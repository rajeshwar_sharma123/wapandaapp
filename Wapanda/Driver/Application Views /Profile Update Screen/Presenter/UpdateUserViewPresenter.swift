
//Notes:- This class is used as presenter for UpdateUserViewPresenter

import Foundation
import ObjectMapper

class UpdateUserViewPresenter: ResponseCallback{
    
//MARK:- UpdateUserViewPresenter local properties
    
    private weak var updateUserViewDelegate          : UpdateUserViewDelegate?
    private lazy var updateUserBusinessLogic         : UpdateUserBusinessLogic = UpdateUserBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:UpdateUserViewDelegate){
        self.updateUserViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.updateUserViewDelegate?.hideLoader()
        self.updateUserViewDelegate?.updatedAgreeTermsAndConditionStatusSuccessfully()
    }
    
    func servicesManagerError(error: ErrorModel){
        self.updateUserViewDelegate?.hideLoader()
        self.updateUserViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call updateUser Api.
    
    func sendUpdateUserRequest(withUpdateUserViewRequestModel updateUserForm1ViewRequestModel:UpdateUserViewRequestModel){
        
        self.updateUserViewDelegate?.showLoader()
        
        var requestModel: UpdateUserRequestModel!
        
        requestModel = UpdateUserRequestModel.Builder()
                            .setAgreeTermsAndConditions(updateUserForm1ViewRequestModel.agreeTermsAndConditions)
                            .setUserId(updateUserForm1ViewRequestModel.userId)
                            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).build()
        
        self.updateUserBusinessLogic.performUpdateUser(withUpdateUserRequestModel: requestModel, presenterDelegate: self)
    }
}
