//
//  BottomMakerView.swift
//  Wapanda
//
//  Created by Daffolapmac on 23/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Charts
class BottomMakerView: UIView
{
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewArrow: UIImageView!
    open override func awakeFromNib()
    {
       imageViewArrow.getImageViewWithImageTintColor(color: UIColor.black)
    }
    
}
