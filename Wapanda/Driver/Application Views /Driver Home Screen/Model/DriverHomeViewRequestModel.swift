
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct DriverHomeViewRequestModel {
        var available             : Bool!
        var ratio                 : Float!
        var ongoingRate           : Float!
        var lat                   : Float!
        var lng                   : Float!
}
