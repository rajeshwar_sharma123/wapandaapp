
//Note :- This class contains OTPPopup Buisness Logic

class PreRegistrationOTPBusinessLogic {
    
    /**
     This method is used for perform OTP With Valid Inputs(OTP text) constructed into a PreRegistrationOTPRequestModel
     
     - parameter inputData: Contains info for OTPPopup
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performPreRegistrationOTP(withPreRegistrationOTPRequestModel otpPopupRequestModel: PreRegistrationOTPRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForOTPPopup()
        PreRegistrationOTPApiRequest().makeAPIRequest(withReqFormData: otpPopupRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForOTPPopup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
        errorResolver.registerErrorCode (ErrorCodes.PHONE_ALREADY_REGISTERED, message  : AppConstants.ErrorMessages.PHONE_ALREADY_REGISTERED)
        errorResolver.registerErrorCode (ErrorCodes.EMAIL_ALREADY_REGISTERED, message  : AppConstants.ErrorMessages.EMAIL_ALREADY_REGISTERED)
        
        return errorResolver
    }
}
