//
//  CabInfo.swift
//
//  Created by  on 15/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class CabInfo: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carType = "carType"
    static let rideType = "rideType"
    static let createdAt = "createdAt"
    static let approved = "approved"
    static let carPlateNumber = "carPlateNumber"
    static let imageId = "imageId"
    static let model = "model"
    static let id = "_id"
    static let dis = "dis"
    static let updatedAt = "updatedAt"
    static let year = "year"
    static let companyName = "companyName"
    static let make = "make"
  }

  // MARK: Properties
  public var carType: CarType?
  public var rideType: String?
  public var createdAt: String?
  public var approved: Bool? = false
  public var carPlateNumber: String?
  public var imageId: String?
  public var model: String?
  public var id: String?
  public var dis: Float?
  public var updatedAt: String?
  public var year: Int?
  public var companyName: String?
  public var make: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    carType <- map[SerializationKeys.carType]
    rideType <- map[SerializationKeys.rideType]
    createdAt <- map[SerializationKeys.createdAt]
    approved <- map[SerializationKeys.approved]
    carPlateNumber <- map[SerializationKeys.carPlateNumber]
    imageId <- map[SerializationKeys.imageId]
    model <- map[SerializationKeys.model]
    id <- map[SerializationKeys.id]
    dis <- map[SerializationKeys.dis]
    updatedAt <- map[SerializationKeys.updatedAt]
    year <- map[SerializationKeys.year]
    companyName <- map[SerializationKeys.companyName]
    make <- map[SerializationKeys.make]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carType { dictionary[SerializationKeys.carType] = value.dictionaryRepresentation() }
    if let value = rideType { dictionary[SerializationKeys.rideType] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    dictionary[SerializationKeys.approved] = approved
    if let value = carPlateNumber { dictionary[SerializationKeys.carPlateNumber] = value }
    if let value = imageId { dictionary[SerializationKeys.imageId] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = dis { dictionary[SerializationKeys.dis] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = year { dictionary[SerializationKeys.year] = value }
    if let value = companyName { dictionary[SerializationKeys.companyName] = value }
    if let value = make { dictionary[SerializationKeys.make] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? CarType
    self.rideType = aDecoder.decodeObject(forKey: SerializationKeys.rideType) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.approved = aDecoder.decodeBool(forKey: SerializationKeys.approved)
    self.carPlateNumber = aDecoder.decodeObject(forKey: SerializationKeys.carPlateNumber) as? String
    self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.imageId) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.dis = aDecoder.decodeObject(forKey: SerializationKeys.dis) as? Float
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.year = aDecoder.decodeObject(forKey: SerializationKeys.year) as? Int
    self.companyName = aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String
    self.make = aDecoder.decodeObject(forKey: SerializationKeys.make) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(rideType, forKey: SerializationKeys.rideType)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(approved, forKey: SerializationKeys.approved)
    aCoder.encode(carPlateNumber, forKey: SerializationKeys.carPlateNumber)
    aCoder.encode(imageId, forKey: SerializationKeys.imageId)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(dis, forKey: SerializationKeys.dis)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(year, forKey: SerializationKeys.year)
    aCoder.encode(companyName, forKey: SerializationKeys.companyName)
    aCoder.encode(make, forKey: SerializationKeys.make)
  }

}
