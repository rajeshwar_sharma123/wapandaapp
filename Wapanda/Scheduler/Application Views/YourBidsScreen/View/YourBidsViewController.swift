//
//  YourBidsViewController.swift
//  Wapanda
//
//  Created by Daffodil on 29/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import HMSegmentedControl
enum YourBidSegmentType: Int{
    case Active = 0
    case InActive = 1
}
class YourBidsViewController: BaseViewController {
    
    @IBOutlet weak var segmentedControl: HMSegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewHeightConstraintConstant: NSLayoutConstraint!
    @IBOutlet weak var currentStatusLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var segmentType: YourBidSegmentType = .Active
    var yourBidsViewPresenter: YourBidsViewPresenter!
    var yourActiveBidsResponse: YourBidsResponseModel!
    var yourInActiveBidsResponse: YourBidsResponseModel!
    let yourBidStatusDropDownView = CustomDropDownView.instanceFromNib()
    var statusListArr: [Any]!
    var selectedStatus: Int = 0
    var selectedStatusName: String = "All"
    var dropDownStatus: DropDownStatus = .Close
    var yourActiveBidStatus: String = TripStatus.Open.rawValue
    var yourInActiveBidStatus: String = TripStatus.Cancelled.rawValue
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        yourBidsViewPresenter = YourBidsViewPresenter(delegate: self)
        //yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourActiveBidStatus))
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
            yourInActiveBidStatus = ""
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        initialSetupForView()
        yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourActiveBidStatus))
    }
    
    //MARK:- Helper Methods
    
    /**
     This function setup the Navigation bar
     */
    private func initialSetupForView(){
        setupNavigationBar()
        initSegmentedProperties()
        setupNavigationBar()
        setupTableView()
        setHeaderView()
        setHeaderViewDetail()
        addPullToRefresh()
    }
    
    
    /**
     This function setup the Navigation bar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.YourBidsScreen.NAVIGATION_TITLE, color: UIColor.appThemeColor(), isTranslucent: false)
        self.customizeNavigationBackButton()
    }
    /**
     Intialise TableView Properties
     */
    private func setupTableView(){
        tableView.registerTableViewCell(tableViewCell: UpComingTripCell.self)
        tableView.registerTableViewCell(tableViewCell: NoResultTableViewCell.self)
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
    /**
     This method is used to get the UpcomingTripListModel
     
     -returns : UpcomingTripListModel
     */
    
    func createYourBidsViewModel(_ skipNum: Int, status: String) -> YourBidsViewModel {
        var upcomingTripListModel = YourBidsViewModel()
        upcomingTripListModel.limit = 10
        upcomingTripListModel.skip = skipNum
        upcomingTripListModel.status = status
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
            upcomingTripListModel.userType = .Driver
        }else{
            upcomingTripListModel.userType = .Rider
        }
        return upcomingTripListModel
    }
    
    /**
     This function setup the Navigation bar
     */
    private func initSegmentedProperties(){
        
        segmentedControl.sectionTitles = [AppConstants.ScreenSpecificConstant.YourBidsScreen.ACTIVE_TITLE,AppConstants.ScreenSpecificConstant.YourBidsScreen.INACTIVE_TITLE]
        segmentedControl.selectionStyle = .fullWidthStripe
        segmentedControl.selectionIndicatorColor = UIColor(red: 45/255.0, green: 80.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        
        segmentedControl.selectionIndicatorHeight = 2.0
        segmentedControl.selectionIndicatorLocation = .down
        segmentedControl.backgroundColor = UIColor.appDarkThemeColor()
        
        segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName:UIFont.getSanFranciscoSemibold( withSize: 14.0)]
        segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName:UIFont.getSanFranciscoRegular( withSize: 14.0)]
        segmentType = YourBidSegmentType(rawValue: segmentedControl.selectedSegmentIndex)!
        if segmentType == .InActive{
            segmentedControl.selectionIndicatorColor = UIColor.white
            
        }else{
            segmentedControl.selectionIndicatorColor = UIColor(red: 45/255.0, green: 80.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        }
        segmentedControl.addTarget(self, action: #selector(TripListingViewController.segmentedControlValueChanged(segment:)), for: .valueChanged)
    }
    
    
    // Action Method called on selecting a segment
    func segmentedControlValueChanged(segment:HMSegmentedControl){
        
        segmentType = YourBidSegmentType(rawValue: segment.selectedSegmentIndex)!
        segmentedControl.selectionIndicatorColor = UIColor(red: 45/255.0, green: 80.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        tableView.reloadData()
        setHeaderView()
        
    }
    /**
     This function setup the Header detail
     */
    func setHeaderViewDetail(){
        yourBidStatusDropDownView.delegate = self
        statusListArr = ["All","Closed","Cancelled"]
        currentStatusLabel.text = selectedStatusName
        setArrowImageViewPosition()
    }
    func setArrowImageViewPosition(){
        let size = selectedStatusName.getStingSize("Titillium-Semibold", fontSize: 16)
        arrowImageView.frame.origin.x = currentStatusLabel.frame.origin.x + size.width + 6
    }
    // This is tap gesture action which shows drop down view
    @IBAction func sortStatusTapAction(_ sender: UITapGestureRecognizer) {
        setDropDownStatus()
        yourBidStatusDropDownView.setDropDown(self.view, belowView: (sender.view?.superview)!, itemList: statusListArr, selectedItem: selectedStatus)
        yourBidStatusDropDownView.toggle()
    }
    /**
     This method is use to setup drop down open status and set arrow icon.
     */
    func setDropDownStatus(){
        if dropDownStatus == .Open{
            dropDownStatus = .Close
            arrowImageView.image = UIImage(named: "ic_arrow_drop_down_white")
        }else{
            dropDownStatus = .Open
            arrowImageView.image = UIImage(named: "ic_arrow_drop_up_white")
        }
    }
    func navigateToBiddingUnderwayViewController(_ auctionId:String, riderFare: Double, isDriverSchedule: Bool){
        let biddingUnderwayVC = UIViewController.getViewController(BiddingUnderwayViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        biddingUnderwayVC.fromScheduleCheck = false
        biddingUnderwayVC.auctionId = auctionId
        biddingUnderwayVC.riderFare = riderFare
        biddingUnderwayVC.isDriverSchedule = isDriverSchedule
        biddingUnderwayVC.delegate = self
        self.navigationController?.pushViewController(biddingUnderwayVC, animated: true)
    }
    // Action method called while doing pull to refresh
    
    func addPullToRefresh(){
        // Add Refresh Control to Table View
        refreshControl = UIRefreshControl()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshUpcomingTripData(_:)), for: .valueChanged)
        
    }
    // Action method called while doing pull to refresh
    
    func refreshUpcomingTripData(_ refreshControl: UIRefreshControl){
        if segmentType == .Active{
            yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourActiveBidStatus))
        }else{
            yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourInActiveBidStatus))
        }
    }
    
    // This method is used to show filter view
    func setHeaderView(){
        if segmentType == .Active{
            hideHeaderView()
            yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourActiveBidStatus))
//            if self.yourActiveBidsResponse == nil || self.yourActiveBidsResponse?.items?.count == 0{
//
//                //
//            }
        }else{
            if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
                
//                if self.yourInActiveBidsResponse == nil || self.yourInActiveBidsResponse?.items?.count == 0{
//                    yourInActiveBidStatus = ""
//                }
                headerViewHeightConstraintConstant.constant = 56
                headerView.isHidden = false
            }else{
                hideHeaderView()
            }
            if self.yourInActiveBidsResponse == nil || self.yourInActiveBidsResponse?.items?.count == 0{
                yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourInActiveBidStatus))
            }
        }
    }
    
    func hideHeaderView(){
        headerViewHeightConstraintConstant.constant = 0
        headerView.isHidden = true
        if yourBidStatusDropDownView.isVisible{
            yourBidStatusDropDownView.toggle()
            setDropDownStatus()
        }
    }
}




//MARK:- Tableview Methods

extension YourBidsViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch segmentType {
            
        case .InActive:
            if let data = self.yourInActiveBidsResponse?.items, data.count > 0{
                return data.count
            }
            return 1
            
        case .Active:
            if let data = self.yourActiveBidsResponse?.items, data.count > 0{
                return data.count
            }
            return 1
            
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch segmentType {
            
        case .Active:
            if let data = self.yourActiveBidsResponse?.items, data.count > 0{
                let cell = tableView.getCell(withCellType: UpComingTripCell.self)
                cell.bindYourBidsDataWithCell(yourBids: yourActiveBidsResponse, indexPath: indexPath)
                return cell
            }else{
                let cell = tableView.getCell(withCellType: NoResultTableViewCell.self)
                cell.bind(title: AppConstants.ScreenSpecificConstant.YourBidsScreen.NO_BIDS)
                cell.selectionStyle = .none
                return cell
            }
            
        case .InActive:
            
            if let data = self.yourInActiveBidsResponse?.items, data.count > 0{
                
                let cell = tableView.getCell(withCellType: UpComingTripCell.self)
                cell.selectionStyle = .none
                cell.bindYourBidsDataWithCell(yourBids: yourInActiveBidsResponse, indexPath: indexPath)
                return cell
                
            }else{
                //No trip Found
                let cell = tableView.getCell(withCellType: NoResultTableViewCell.self)
                cell.bind(title: AppConstants.ScreenSpecificConstant.YourBidsScreen.NO_BIDS)
                cell.selectionStyle = .none
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if segmentType == .InActive{
            if let data = self.yourInActiveBidsResponse?.items, data.count-1 == indexPath.row,(self.yourInActiveBidsResponse?.hasNext!)!{
                
                yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel((self.yourInActiveBidsResponse?.items?.count)!, status: yourInActiveBidStatus))
                
            }
        }else{
            
            if let data = self.yourActiveBidsResponse?.items, data.count-1 == indexPath.row,(self.yourActiveBidsResponse?.hasNext!)!{
                yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel((self.yourActiveBidsResponse?.items?.count)!, status: yourActiveBidStatus))
                
            }
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentType == .InActive{
            
        }else{
            if let data = self.yourActiveBidsResponse?.items, data.count > 0{
                let item =  self.yourActiveBidsResponse?.items?[indexPath.row]
                if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
                    navigateToBiddingUnderwayViewController((item?.id)!, riderFare: (item?.yourBid?.price)!,isDriverSchedule: true)
                }else{
                    navigateToBiddingUnderwayViewController((item?.id)!, riderFare: (item?.riderSelectedFare)!,isDriverSchedule: false)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch segmentType {
            
        case .InActive:
            if let data = self.yourInActiveBidsResponse?.items, data.count > 0{
                return 150
            }else{
                return tableView.frame.size.height
            }
            
        case .Active:
            if let data = self.yourActiveBidsResponse?.items, data.count > 0{
                return 150
            }else{
                return tableView.frame.size.height
            }
            
        }
        
        return 0
    }
}

// MARK: - BiddingUnderwayViewDelegate Methods


extension YourBidsViewController : YourBidsViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.refreshControl.endRefreshing()
        tableView.reloadData()
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    func updateYourBidsList(withResponseModel upcomingTripList: YourBidsResponseModel){
        self.refreshControl.endRefreshing()
        
        if segmentType == .InActive{
            if let trip = self.yourInActiveBidsResponse , trip.hasNext!
            {
                let lastIndex = (self.yourInActiveBidsResponse?.items?.count)! // First Index of newly added items
                
                self.yourInActiveBidsResponse?.items?.append(contentsOf:upcomingTripList.items!)
                self.yourInActiveBidsResponse?.hasNext = upcomingTripList.hasNext
                
                //Index path of newly added items
                var rowsToAdd = [IndexPath]()
                for indexRow in lastIndex ..< (self.yourInActiveBidsResponse?.items?.count)!
                {
                    rowsToAdd.append(IndexPath(row: indexRow, section: 0))
                }
                
                //Insert new added rows
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: rowsToAdd, with: .none)
                self.tableView.endUpdates()
            }else{
                self.yourInActiveBidsResponse = upcomingTripList
                tableView.reloadData()
                
            }
            
        }else{
            if let trip = self.yourActiveBidsResponse , trip.hasNext!
            {
                let lastIndex = (self.yourActiveBidsResponse?.items?.count)! // First Index of newly added items
                
                self.yourActiveBidsResponse?.items?.append(contentsOf:upcomingTripList.items!)
                self.yourActiveBidsResponse?.hasNext = upcomingTripList.hasNext
                
                //Index path of newly added items
                var rowsToAdd = [IndexPath]()
                for indexRow in lastIndex ..< (self.yourActiveBidsResponse?.items?.count)!
                {
                    rowsToAdd.append(IndexPath(row: indexRow, section: 0))
                }
                
                //Insert new added rows
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: rowsToAdd, with: .none)
                self.tableView.endUpdates()
            }else{
                self.yourActiveBidsResponse = upcomingTripList
                tableView.reloadData()
                
            }
        }
    }
}


extension YourBidsViewController: DropDownViewDelegate{
    func updateSelectedItem(selectedItem: Int) {
        selectedStatus = selectedItem
        let statusName = statusListArr[selectedItem] as! String
        selectedStatusName = statusName
        currentStatusLabel.text = selectedStatusName
        setArrowImageViewPosition()
        setDropDownStatus()
        yourInActiveBidStatus = selectedStatusName.uppercased()
        if selectedStatusName.uppercased() == TripStatus.All.rawValue{
            yourInActiveBidStatus = ""
        }else  if selectedStatusName.uppercased() == TripStatus.Closed.rawValue{
            yourInActiveBidStatus = TripStatus.Closed.rawValue
        }else if selectedStatusName.uppercased() == TripStatus.Cancelled.rawValue{
            yourInActiveBidStatus = TripStatus.Cancelled.rawValue
        }
        self.yourInActiveBidsResponse = nil
        yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourInActiveBidStatus))
        tableView.reloadData()
    }
}


extension YourBidsViewController: BiddingUnderwayViewControllerDelegate{
    func updateUpcomingTripList() {
        self.yourActiveBidsResponse = nil
        yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourActiveBidStatus))
        self.yourInActiveBidsResponse = nil
       // yourBidsViewPresenter.sendYourBidsList(withData: createYourBidsViewModel(0, status: yourInActiveBidStatus))
    }
}
