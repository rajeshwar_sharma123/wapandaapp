//
//  DriverRatingScreenViewDelgate.swift
//  Wapanda
//
//  Created by Daffolapmac on 05/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//


import UIKit
protocol DriverRatingScreenViewDelgate: BaseViewProtocol {
    func didReceiveDriverRatingList(withResponseModel:DriverRatingResponseModel)
}
