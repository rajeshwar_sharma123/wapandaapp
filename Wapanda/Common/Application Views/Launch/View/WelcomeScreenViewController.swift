/**
    This screen Handle the designing of welcome screen
 */

import UIKit
import SocketIO
class WelcomeScreenViewController: BaseViewController, WelcomeScreenViewDelegate {


    //MARK: View Life Cycle
    var socket : SocketIOClient!
    var shouldHaveBackButton : Bool!
    override func viewDidLoad() {
        super.viewDidLoad()

        //Setup Initials
        setupInitialUI()
        //Add notification if app comes from background to foreground
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Check for location permissions
        LocationServiceManager.sharedInstance.viewDelegate = self
        LocationServiceManager.sharedInstance.checkForUserPermissions()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func appDidBecomeActive(notif: Notification){
        //Check for location permissions
        LocationServiceManager.sharedInstance.viewDelegate = self
        LocationServiceManager.sharedInstance.checkForUserPermissions()
    }
    
    //MARK: Initial View Methods
    
    /**
     This function setup the Initial UI elements
     - returns :void
     */
    
    private func setupInitialUI() ->Void{
        self.setupNavigationBar()
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    
    private func setupNavigationBar() ->Void{
        self.hideNavigationBar()
    }

    
    //MARK: - Action handling for the view 
    
    /**
     This method calculates the dynamic height of label according to the context.
     
     - parameter sender: SignUp button reference
     
     */
    
    @IBAction func btnSignUpClick(_ sender: UIButton) {
        let singUpVC = UIViewController.getViewController(SignUpForm1ViewController.self, storyboard: UIStoryboard.Storyboard.Main.object)
        self.navigationController?.pushViewController(singUpVC, animated: true)
    }

    /**
     This method calculates the dynamic height of label according to the context.
     
     - parameter sender: Login button reference
     
     */
    
    @IBAction func btnLoginClick(_ sender: UIButton) {
        let loginVC = UIViewController.getViewController(viewController: LoginScreenViewController.self)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
   
    /**
     This method calculates the dynamic height of label according to the context.
     
     - parameter sender: Skip SignUp button reference
     
     */
    
    @IBAction func btnSkipSignUpClick(_ sender: UIButton) {
    UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN, value: false)
       
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER, value: false)
            self.navigateToRiderHomeScreen()
            //navigateToSwitchUserScreen()
    }
    

    //MARK: Welcome screen view delegate
    
    func showControllerWithVC(controller: UIViewController)
    {
        self.present(controller, animated: true, completion: nil)
    }
    
    func locationUpdated(lat: Double, long: Double) {
        
    }

    //MARK: Navigation
    func navigateToRiderHomeScreen(){
        
        let rolesVC = UIViewController.getViewController(RiderHomeViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        self.navigationController?.pushViewController(rolesVC, animated: true)
    }
    private func navigateToSwitchUserScreen(){
        let switchUserVC = UIViewController.getViewController(viewController: SwitchRoleScreenViewController.self)
        self.navigationController?.pushViewController(switchUserVC, animated: true)
    }
}
