//
//  DriverStripeAPIConnectionResponseModel.swift
//
//  Created by  on 27/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DriverStripeAPIConnectionResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let chargesEnabled = "charges_enabled"
  }

  // MARK: Properties
  public var id: String?
  public var chargesEnabled: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    chargesEnabled <- map[SerializationKeys.chargesEnabled]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.chargesEnabled] = chargesEnabled
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.chargesEnabled = aDecoder.decodeBool(forKey: SerializationKeys.chargesEnabled)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(chargesEnabled, forKey: SerializationKeys.chargesEnabled)
  }

}
