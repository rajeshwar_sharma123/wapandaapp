//
//  Vehicle.swift
//
//  Created by  on 27/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class VehicleScheduleTrip: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carType = "carType"
    static let id = "_id"
    static let year = "year"
    static let carPlateNumber = "carPlateNumber"
    static let companyName = "companyName"
    static let make = "make"
    static let model = "model"
    static let imageId = "imageId"
  }

  // MARK: Properties
  public var carType: CarTypeScheduleTrip?
  public var id: String?
  public var year: Int?
  public var carPlateNumber: String?
  public var companyName: String?
  public var make: String?
  public var model: String?
  public var imageId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    carType <- map[SerializationKeys.carType]
    id <- map[SerializationKeys.id]
    year <- map[SerializationKeys.year]
    carPlateNumber <- map[SerializationKeys.carPlateNumber]
    companyName <- map[SerializationKeys.companyName]
    make <- map[SerializationKeys.make]
    model <- map[SerializationKeys.model]
    imageId <- map[SerializationKeys.imageId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carType { dictionary[SerializationKeys.carType] = value.dictionaryRepresentation() }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = year { dictionary[SerializationKeys.year] = value }
    if let value = carPlateNumber { dictionary[SerializationKeys.carPlateNumber] = value }
    if let value = companyName { dictionary[SerializationKeys.companyName] = value }
    if let value = make { dictionary[SerializationKeys.make] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = imageId { dictionary[SerializationKeys.imageId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? CarTypeScheduleTrip
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.year = aDecoder.decodeObject(forKey: SerializationKeys.year) as? Int
    self.carPlateNumber = aDecoder.decodeObject(forKey: SerializationKeys.carPlateNumber) as? String
    self.companyName = aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String
    self.make = aDecoder.decodeObject(forKey: SerializationKeys.make) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.imageId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(year, forKey: SerializationKeys.year)
    aCoder.encode(carPlateNumber, forKey: SerializationKeys.carPlateNumber)
    aCoder.encode(companyName, forKey: SerializationKeys.companyName)
    aCoder.encode(make, forKey: SerializationKeys.make)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(imageId, forKey: SerializationKeys.imageId)
  }

}
