//
//  DocumentTypeTableViewCell.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/26/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DocumentTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDocumentType: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func bindData(WithTitle title: String, isDocUploaded: Bool){
        self.lblDocumentType.text = title
        self.setDocStatusImage(isDocUploaded: isDocUploaded)
    }
    
    func setDocStatusImage(isDocUploaded: Bool){
        let image = isDocUploaded ? #imageLiteral(resourceName: "ic_check_mark"): #imageLiteral(resourceName: "ic_keyboard_arrow_right_white")
        self.imgView.image = image
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
