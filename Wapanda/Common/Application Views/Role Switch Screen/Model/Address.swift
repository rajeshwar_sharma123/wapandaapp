//
//  Address.swift
//  Wapanda
//
//  Created by daffomac-31 on 18/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Address:  NSObject, Mappable, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let lat = "lat"
        static let lng = "lng"
        static let address = "address"
        static let address_type_title =  "address_type_title"  //this variable is added on app side
    }
    
    // MARK: Properties
    public var lat: Double?
    public var lng: Double?
    public var address: String?
    public var address_type_title: String?  //this variable is added on app side
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        lat <- map[SerializationKeys.lat]
        lng <- map[SerializationKeys.lng]
        address <- map[SerializationKeys.address]
        address_type_title <- map[SerializationKeys.address_type_title]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = lat { dictionary[SerializationKeys.lat] = value }
        if let value = lng { dictionary[SerializationKeys.lng] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = address_type_title { dictionary[SerializationKeys.address_type_title] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? Double
        self.lng = aDecoder.decodeObject(forKey: SerializationKeys.lng) as? Double
        self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
        self.address_type_title = aDecoder.decodeObject(forKey: SerializationKeys.address_type_title) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(lat, forKey: SerializationKeys.lat)
        aCoder.encode(lng, forKey: SerializationKeys.lng)
        aCoder.encode(address, forKey: SerializationKeys.address)
        aCoder.encode(address_type_title, forKey: SerializationKeys.address_type_title)
    }
    
}
