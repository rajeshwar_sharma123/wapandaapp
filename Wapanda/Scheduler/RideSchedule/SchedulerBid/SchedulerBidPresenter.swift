import UIKit
import ObjectMapper

class SchedulerBidPresenter: ResponseCallback {
    
    //MARK:- CabSelectionViewPresenter local properties
    
    private weak var view: SchedulerBidViewControllerDelegate?
    private var manager = SchedulerBitManager()
    //MARK:- Constructor
    
    init(delegate: SchedulerBidViewControllerDelegate){
        view = delegate
    }
    
    //MARK:- Methods to make decision and call cabSelection Api.
    /**
     This method is used to make Car Make List request to business layer with valid Request model
     - returns : Void
     */
    func getAllVehicleTypes(latFrom: Double, lngFrom: Double, latTo: Double, lngTo: Double) -> Void{
        
        view?.showLoader()
        
        let vehicleTypesRequestModel : VehicleRequestModel = VehicleRequestModel.Builder()
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setLatTo(Float(latTo))
            .setLngTo(Float(lngTo))
            .setLatFrom(Float(latFrom))
            .setLngFrom(Float(lngFrom))
            .build()
        
        manager.performAllVehicleTypeRequest(withCarMakeRequestModel: vehicleTypesRequestModel, presenterDelegate: self)
    }
    func getCabsList(location: SourceDestinationModel, selectedCab: [String] = [String]()){
        
        self.view?.showLoader()
        
        let requestModel = CabSelectionRequestModel.Builder()
            .setSourceCoordinate(lat: location.source.lat, lng: location.source.long)
            .setDestinationCoordinate(lat: location.destination.lat, lng: location.destination.long)
            .setSortingOrder(order: AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_PRICE)
            .setCarTypes(carTypes: selectedCab)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE).addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).build()
        
        manager.performCabSelection(withCabSelectionRequestModel: requestModel, presenterDelegate: self)
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject: T){
        
        view?.hideLoader()
        if let response = responseObject as? VehicleEstimatedPriceResponseModel {
            //guard let rideList = response.items else {  return }
            
            if response.items.count > 0 {
                
             //   guard let ride = response.items[0] else { return }
                let standardFairValuesArray = response.items.filter({ (vehicleObj) -> Bool in
                    if vehicleObj.carType == "Standard"
                    { return true}
                    return false
                })
                if let cabName = standardFairValuesArray[0].carType, let price = standardFairValuesArray[0].estPrice{
                    let cabType = Type(JSON: [:])
                    cabType?.vehicleType = cabName
                    let cabInfo = getSelectedCab(cabType: cabType!, estPrice: price)
                    UserRideDetail.shared.selectedCabs = [cabInfo]
                }
               self.view?.refreshList(list: response.items)
            }
            else{
                self.view?.noCabsAvailable()
            }
            
        }
        if let response = responseObject as? CabSelectionResponseModel {
            
            guard let rideList = response.rideList else {  return }
            
            if rideList.count > 0 {
                
                guard let ride = response.rideList?[0] else { return }
                
                if let cabType = ride.cabInfo?.type, let price = ride.estPrice{
                    let cabInfo = getSelectedCab(cabType: cabType, estPrice: price)
                    UserRideDetail.shared.selectedCabs = [cabInfo]
                }
                self.view?.renderData(data: ride)
            }
            else{
                self.view?.noCabsAvailable()
            }
        }
    }
    
    
    func getSelectedCab(cabType: Type, estPrice: Double) -> SelectedCabModel {
        let name = cabType.vehicleType ?? ""
        let id = cabType._id ?? ""
        var cabIcon = ""
        if let icon = cabType.icon {
            cabIcon = icon
        }
        
        return SelectedCabModel(name: name, id: id, icon: cabIcon, estPrice: estPrice)
    }
    
    
    func servicesManagerError(error: ErrorModel){
        self.view?.hideLoader()
        self.view?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
}
