//
//  ApplePayHandler.swift
//  Wapanda
//
//  Created by daffomac-31 on 30/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Stripe

protocol ApplePayDelegate: class{
    func applePayPaymentSuccess()
    func applePayPaymentFailure()
    func processPaymentWithSourceId(id: String)
}


class ApplePayHandler: NSObject {
    
    var paymentSucceeded = false
    lazy var applePayAvailable = Stripe.deviceSupportsApplePay()
    weak var delegate: ApplePayDelegate?
    var paymentProcessingCompletionHanlder: ((PKPaymentAuthorizationStatus) -> Void)?
    
    /// This method is used to process apple payment requet
    /// Check for applePayAvailable before calling this method
    ///
    /// - Parameter items: array of items where last item denotes the totoal of all items
    func handleApplePayRequestWithItem(items: [ApplePayItemModel]) {
        
        let locale = Locale.current
        
        let countryCode = locale.regionCode
        let currencyCode = locale.currencyCode
    
        let merchantIdentifier = PListUtility.getValue(forKey: AppConstants.PListKeys.APPLE_PAY_MERCHANT_ID) as! String
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: countryCode!, currency: currencyCode!)
        
        var paymentSummaryItems: [PKPaymentSummaryItem] = []
        var total = 0.0
        
        for item in items{
            let price = NSDecimalNumber(value: item.price)
            let paymentItem = PKPaymentSummaryItem(label: item.label, amount: price)
            paymentSummaryItems.append(paymentItem)
            total = total + item.price
        }
        
        //PAY Wapanda label
        let itemPayWapanda = PKPaymentSummaryItem(label: "Wapanda", amount: NSDecimalNumber(value: total))
        paymentSummaryItems.append(itemPayWapanda)
        
        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = paymentSummaryItems
        
        // Continued in next step
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            // Setup payment authorization view controller
            let paymentAuthorizationViewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            paymentAuthorizationViewController.delegate = self
            
            // Present payment authorization view controller
            UIApplication.shared.visibleViewController?.present(paymentAuthorizationViewController, animated: true, completion: {
            })
            
        }
        else {
            // There is a problem with your Apple Pay configuration
            self.delegate?.applePayPaymentFailure()
        }
    }
    
    func onPaymentProcessingSuccess(){
        self.paymentSucceeded = true
        self.paymentProcessingCompletionHanlder!(.success)
    }
    
    func onPaymentProcessingError(){
        self.paymentSucceeded = false
        self.paymentProcessingCompletionHanlder!(.failure)
    }
}

extension ApplePayHandler: PKPaymentAuthorizationViewControllerDelegate{
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        
        STPAPIClient.shared().createSource(with: payment) { (source, error) in
            guard let _ = source, error == nil else {
                // Present error to user...
                self.delegate?.applePayPaymentFailure()
                completion(.failure)
                return
            }
            
            ///Submit token to backend
            self.delegate?.processPaymentWithSourceId(id: source!.stripeID)
            self.paymentProcessingCompletionHanlder = completion
        }
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        // Dismiss payment authorization view controller
         UIApplication.shared.visibleViewController?.dismiss(animated: true, completion: {
            if (self.paymentSucceeded) {
                self.delegate?.applePayPaymentSuccess()
            }
            else{
                self.delegate?.applePayPaymentFailure()
            }
        })
    }
}
