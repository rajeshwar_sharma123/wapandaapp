//
//  DriverRatingScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DriverRatingScreenRequestModel {
    
    var driverId   : String!
    var skip   : Int!
    var limit : Int!
}
