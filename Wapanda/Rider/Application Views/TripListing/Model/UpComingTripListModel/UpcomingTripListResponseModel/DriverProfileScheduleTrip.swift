//
//  DriverProfile.swift
//
//  Created by  on 27/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DriverProfileScheduleTrip:NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastLoc = "lastLoc"
    static let status = "status"
    static let available = "available"
    static let averageRating = "averageRating"
  }

  // MARK: Properties
  public var lastLoc: LastLoc?
  public var status: String?
  public var available: Bool? = false
  public var averageRating: Int?

    
    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lastLoc <- map[SerializationKeys.lastLoc]
    status <- map[SerializationKeys.status]
    available <- map[SerializationKeys.available]
    averageRating <- map[SerializationKeys.averageRating]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lastLoc { dictionary[SerializationKeys.lastLoc] = value.dictionaryRepresentation() }
    if let value = status { dictionary[SerializationKeys.status] = value }
    dictionary[SerializationKeys.available] = available
    if let value = averageRating { dictionary[SerializationKeys.averageRating] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lastLoc = aDecoder.decodeObject(forKey: SerializationKeys.lastLoc) as? LastLoc
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.available = aDecoder.decodeObject(forKey: SerializationKeys.available) as? Bool
    self.averageRating = aDecoder.decodeObject(forKey: SerializationKeys.averageRating) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lastLoc, forKey: SerializationKeys.lastLoc)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(available, forKey: SerializationKeys.available)
    aCoder.encode(averageRating, forKey: SerializationKeys.averageRating)
  }

}
