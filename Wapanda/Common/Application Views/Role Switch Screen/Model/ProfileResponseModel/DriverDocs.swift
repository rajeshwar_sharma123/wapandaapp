//
//  DriverDocs.swift
//
//  Created by  on 8/30/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DriverDocs:  NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updatedAt"
    static let insuranceDoc = "insuranceDoc"
    static let id = "_id"
    static let vehicleDoc = "vehicleDoc"
    static let createdAt = "createdAt"
    static let taxDoc = "taxDoc"
    static let canApprove = "canApprove"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var insuranceDoc: InsuranceDoc?
  public var id: String?
  public var vehicleDoc: VehicleDoc?
  public var createdAt: String?
  public var taxDoc: TaxDoc?
  public var canApprove: Bool?


  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    insuranceDoc <- map[SerializationKeys.insuranceDoc]
    id <- map[SerializationKeys.id]
    vehicleDoc <- map[SerializationKeys.vehicleDoc]
    createdAt <- map[SerializationKeys.createdAt]
    taxDoc <- map[SerializationKeys.taxDoc]
    canApprove <- map[SerializationKeys.canApprove]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = insuranceDoc { dictionary[SerializationKeys.insuranceDoc] = value.dictionaryRepresentation() }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = vehicleDoc { dictionary[SerializationKeys.vehicleDoc] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = taxDoc { dictionary[SerializationKeys.taxDoc] = value.dictionaryRepresentation() }
    if let value = canApprove { dictionary[SerializationKeys.canApprove] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.insuranceDoc = aDecoder.decodeObject(forKey: SerializationKeys.insuranceDoc) as? InsuranceDoc
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.vehicleDoc = aDecoder.decodeObject(forKey: SerializationKeys.vehicleDoc) as? VehicleDoc
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.taxDoc = aDecoder.decodeObject(forKey: SerializationKeys.taxDoc) as? TaxDoc
    self.canApprove = aDecoder.decodeObject(forKey: SerializationKeys.canApprove) as? Bool
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(insuranceDoc, forKey: SerializationKeys.insuranceDoc)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(vehicleDoc, forKey: SerializationKeys.vehicleDoc)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(taxDoc, forKey: SerializationKeys.taxDoc)
    aCoder.encode(canApprove, forKey: SerializationKeys.canApprove)
  }

}
