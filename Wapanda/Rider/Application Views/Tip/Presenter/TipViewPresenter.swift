
//Notes:- This class is used as presenter for TipViewPresenter

import Foundation
import ObjectMapper

class TipViewPresenter: ResponseCallback{
    
//MARK:- TipViewPresenter local properties
    
    private weak var tipViewDelegate          : TipViewDelegate?
    private lazy var tipBusinessLogic         : TipBusinessLogic = TipBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:TipViewDelegate){
        self.tipViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let _ = responseObject as? CommonResponseModel{
            self.tipViewDelegate?.tipSubmittedSucessfully()
        }
        self.tipViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error: ErrorModel){
        self.tipViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.tipViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call Tip Api.
    
    func sendTipRequest(withTipViewRequestModel tipViewRequestModel:TipViewRequestModel){
        self.tipViewDelegate?.showLoader()
        
        let requestModel = TipRequestModel.Builder().setTripId(tipViewRequestModel.tripId)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.tipBusinessLogic.performTip(withTipRequestModel: requestModel, presenterDelegate: self)
    }
}
