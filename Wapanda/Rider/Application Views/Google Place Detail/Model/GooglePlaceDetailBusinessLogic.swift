
//Note :- This class contains GooglePlaceDetail Buisness Logic

class GooglePlaceDetailBusinessLogic {
    
    
    deinit {
        print("GooglePlaceDetailBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a GooglePlaceDetailRequestModel
     
     - parameter inputData: Contains info for GooglePlace
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performGooglePlaceDetail(withGooglePlaceRequestModel requestModel: GooglePlaceDetailRequestModel, presenterDelegate:ResponseCallbackGeneral) ->Void {
        
        //Adding predefined set of errors
        GooglePlaceDetailServiceManager().makeRequest(withReqFormData: requestModel, responseCallback: presenterDelegate)
    }

}
