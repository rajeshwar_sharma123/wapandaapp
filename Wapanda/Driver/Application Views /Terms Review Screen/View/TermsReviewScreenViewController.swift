//
//  TermsReviewScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/24/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class TermsReviewScreenViewController: BaseViewController {
    
    // MARK: - Local variables
    var addReviewScreenSkipButton : Bool = true
    fileprivate var presenterTermsReview: TermsReviewViewPresenter!
    fileprivate var termsReviewData = TermsReviewResponseModel()
     var screenMode = SCREEN_MODE.NEW
    // MARK: - IBOutlets
    @IBOutlet weak var tableViewDocuments: UITableView!
    
    // MARK: - Terms Review Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
        self.presenterTermsReview = TermsReviewViewPresenter(delegate: self)
        self.presenterTermsReview.getTermsReviewList()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.objProfileTaxInformation = LoginResponseModel.retriveUser()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helper Methods
    private func intialSetupForView(){
        self.setUpTableView()
        self.setUpNavigationBar()
    }
    func setScreenMode(mode: SCREEN_MODE){
        self.screenMode = mode
    }
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.TermsScreen.STEP_NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
    }
    private func setUpTableView(){
        self.tableViewDocuments.delegate = self
        self.tableViewDocuments.dataSource = self
        self.tableViewDocuments.estimatedRowHeight = 60
        self.tableViewDocuments.tableFooterView = UIView()
    }
    // MARK: - IBOutlet Action Methods
    @IBAction func agreeButtonTapped(_ sender: CustomButton) {
        //NAVIGATION
        if self.screenMode == .EDITING{
            self.popViewController()
        }
        else{
             self.navigateToDocumentSetUpController()
        }
        
       
    }
    
    // MARK: - Navigation
    
    func navigateToViewerController(WithSelectedItem item : Item){
        let viewerVC = UIViewController.getViewController(DocumentViewerScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        viewerVC.initializeScreen(WithItemData: item)
        self.navigationController?.pushViewController(viewerVC, animated: true)
    }
    
    func navigateToDocumentSetUpController(){
        let docSetUpVC = UIViewController.getViewController(DriverSetUpScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
          docSetUpVC.screenTitle = AppConstants.ScreenSpecificConstant.DriverSetup.NAVIGATION_TITLE
        self.navigationController?.pushViewController(docSetUpVC, animated: true)
    }
    func popViewController(){
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - UITableView Delegate Methods
extension TermsReviewScreenViewController : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentsListTableViewCell", for: indexPath) as! DocumentsListTableViewCell
        cell.bind(WithData: self.termsReviewData.items[indexPath.row].type)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.navigateToViewerController(WithSelectedItem: self.termsReviewData.items[indexPath.row])
        self.tableViewDocuments.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return termsReviewData.items.count
    }
}

//MARK-  Terms Review Delegate
extension TermsReviewScreenViewController: TermsReviewViewDelegate{
    
    func termsReviewListFetchedSuccessfully(WithResponseModel data: TermsReviewResponseModel){
        self.termsReviewData = data
        self.tableViewDocuments.reloadData()
    }
    
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
