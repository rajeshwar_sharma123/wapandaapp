
import UIKit

extension BidsListViewController: UITableViewDataSource, UITableViewDelegate {

    func registerCells() {
        bidsTableView.registerTableViewCell(tableViewCell: AllBidsCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bids.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.getCell(withCellType: AllBidsCell.self)
        cell.bind(data: bids[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == bids.count - 5 && presenter.bidsList!.hasNext && !presenter.callInProgress {
            getBidsList(skip: bids.count, showLoader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !(AppDelegate.sharedInstance.userInformation.driverProfile?.approved)!{
            presentVerifyInProcessViewController()
            return
        }
        
        if let vc = UIViewController.getViewControllerFromSchedulingStoryboard(viewController: BidDetailViewController.self) {
            
            guard let driverAvailabile = driverObject?.driverProfile?.available else { return }

            vc.bidId = bids[indexPath.row].id
            vc.riderSelectedFare = bids[indexPath.row].riderSelectedFare!
            vc.isOnlineDriver = driverAvailabile
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    func presentVerifyInProcessViewController(){
        let unverified = UIViewController.getViewController(VerifyInProcessViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        self.present(unverified, animated: true, completion: nil)
    }
}
