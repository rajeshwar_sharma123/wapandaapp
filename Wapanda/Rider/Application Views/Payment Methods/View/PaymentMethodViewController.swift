//
//  PaymentMethodViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 02/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class PaymentMethodViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    weak var delegate: AddPaymentScreenProtocol?
    var isComingFromSideMenu = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.intialSetupForView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    override func rightButtonClick() ->Void {
//
//    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        
        self.setupTableView()
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    private func setupNavigationBar() ->Void{
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.PaymentMethodScreen.NAVIGATION_TITLE, color: UIColor.appThemeColor(), isTranslucent: false)
        self.customizeNavigationBackButton()
//        if !isComingFromSideMenu{
//            self.addNavigationRightButtonWithTitle(title: "Continue")
//        }
    }
    override func backButtonClick() {
        
        if (self != self.navigationController?.viewControllers[0])
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    /**
     This method is used to setup initial Bank Info View
     */
    private func setupTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func addNewCreditCardClick(){
        let addNewCardVC = UIViewController.getViewController(AddCreditCardScreenViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        addNewCardVC.isComingFromSideMenu = self.isComingFromSideMenu
        self.navigationController?.pushViewController(addNewCardVC, animated: true)
    }
    
    
    // MARK: - Payment Method View Delegate
    override func showLoader(_ VC: AnyObject?) {
        super.showLoader(self)
    }
    
    override func hideLoader(_ VC: AnyObject?) {
        super.hideLoader(self)
    }
    
    override func showErrorAlert(_ alertTitle: String, alertMessage: String, VC: AnyObject?) {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PaymentMethodViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.getCell(withCellType: PaymentMethodTableViewCell.self)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.addNewCreditCardClick()
    }
}


