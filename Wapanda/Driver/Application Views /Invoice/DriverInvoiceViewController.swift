//
//  DriverInvoiceViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 01/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

enum NotificationStatus{
    case Pending
    case Received
    case Cancelled
}



class DriverInvoiceViewController: BaseViewController {

    //MARK:- Properties/Outlets
    
    @IBOutlet weak var lblStopAddress: UILabel!
    @IBOutlet weak var labelTip: UILabel!
    @IBOutlet weak var imgViewRoute: UIImageView!
    @IBOutlet weak var lblBillAmount: UILabel!
    @IBOutlet weak var lblRideTimeValue: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var labelDriverFee: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblRideDistanceValue: UILabel!
    @IBOutlet weak var lblRideFare: UILabel!
    @IBOutlet weak var lblTipValue: UILabel!
    @IBOutlet weak var lblTotalAmt: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewBillDetail: UIBorderView!
    @IBOutlet weak var lblPending: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var _lblPendingHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var _lblAddStopHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var _imgViewDashedLineHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDriverFee: UILabel!
    @IBOutlet weak var _layoutConstraintTopView: NSLayoutConstraint!
    
    

    var invoiceDataModel: DriverInvoiceViewModel!
    var MILE_PER_METER = 1609.344


   //MARK:- ViewLifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(handleDriverInvoice(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_INVOICE_HANDLE), object: nil)

        customizeViewAppearence()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Helper Methods
    
    func customizeViewAppearence() {

       // self.hideNavigationBar()
        self.automaticallyAdjustsScrollViewInsets = false
        lblPending.text = AppConstants.ScreenSpecificConstant.DriverInvoice.PENDING_MESSAGE
        self.btnSubmit.setCornerCircular(10.0)
        if let viewControllersCount = self.navigationController?.viewControllers.count, viewControllersCount >= 2,(self.navigationController?.viewControllers[viewControllersCount-2] is TripListingViewController)
        {
            self.btnSubmit.setTitle("OK", for: .normal)
        }
        else
        {
            self.btnSubmit.setTitle("Done", for: .normal)
        }
        self.setupNavigationBar()
        
        if self.invoiceDataModel.status == TRIP_STATUS.CANCELLED.rawValue
        {
            togglePendingView(status: .Cancelled)
        }
        else if let payment = self.invoiceDataModel.payment,payment.paid == true
        {
            togglePendingView(status: .Received)
        }
        else
        {
            togglePendingView(status: .Pending)
        }

        
        //Set Route Image
        let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: (self.invoiceDataModel?.routeImageId )!))
        self.imgViewRoute.setImageWith(imageUrl!, placeholderImage: #imageLiteral(resourceName: "map_placeholder"))

        
        // Set Ride  Details
        if self.invoiceDataModel.status == TRIP_STATUS.CANCELLED.rawValue{
            self.lblBillAmount.text = "$0.00"
        }
        else if let payment = self.invoiceDataModel.payment
            {
                var driverFee = 0.0
                if let driverFees = self.invoiceDataModel?.payment?.applicationFee
                {
                    driverFee = Double(driverFees)
                }
                 self.lblBillAmount.text = AppUtility.getFormattedPriceString(withPrice: Double((payment.amountPayable! - driverFee)))
            }
            else
            {
                self.lblBillAmount.text = AppUtility.getFormattedPriceString(withPrice: Double(invoiceDataModel!.agreedPrice))
            }
           
        

        self.lblRideDistanceValue.text = String(format: "%.1f miles", (Double(invoiceDataModel!.distanceTravelled)/MILE_PER_METER))
       // self.lblRideTimeValue.text = AppUtility.getFormattedTimeDuration(withTime: invoiceDataModel!.duration)
        
        var duration = 0
        if let invoiceModel = invoiceDataModel,invoiceModel.duration >= 0
        {
            duration = invoiceModel.duration
        }
        
        self.lblRideTimeValue.text = AppUtility.getTimeInMinutes(fromTimeInMS: UIntMax(duration))

        self.lblSource.text = invoiceDataModel!.fromAddress
        self.lblDestination.text = invoiceDataModel!.toAddress
        self.lblStopAddress.text = invoiceDataModel!.stopAddress
        
        if self.lblStopAddress.text!.isEmptyString() {
            _imgViewDashedLineHeightConstraint.constant = 0
            _lblAddStopHeightConstraint.constant = 0
        
        }else{
            _imgViewDashedLineHeightConstraint.constant = 12
            _lblAddStopHeightConstraint.constant = 24

        }

    }
    /**
     This function setup the Navigation bar
     */
    private func setupNavigationBar(){
        if let viewControllersCount = self.navigationController?.viewControllers.count, viewControllersCount >= 2,(self.navigationController?.viewControllers[viewControllersCount-2] is TripListingViewController)
        {
            self.customizeNavigationBarWithTitleImage(image: UIImage())
            self.customizeNavigationBackButton()
        }
    }



    // Update view based on notification status
    func togglePendingView(status: NotificationStatus){

        switch status {

            case .Pending:
                                viewBillDetail.isHidden = true
                                 _lblPendingHeightConstraint.constant = 18
                              
                                lblPending.text = AppConstants.ScreenSpecificConstant.DriverInvoice.PENDING_MESSAGE
                                scrollView.isScrollEnabled = false

                                break
            case .Received:

                                viewBillDetail.isHidden = false
                                
                                  _layoutConstraintTopView.constant = _layoutConstraintTopView.constant - _lblPendingHeightConstraint.constant
                                
                                _lblPendingHeightConstraint.constant = 0

                                lblPending.text = AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
                                scrollView.isScrollEnabled = true
                                setBillDetails()
                                break
        case .Cancelled:
            
                            viewBillDetail.isHidden = true
                                _lblPendingHeightConstraint.constant = 18
                            lblPending.text = AppConstants.ScreenSpecificConstant.DriverInvoice.CANCELLED_MESSAGE
                            scrollView.isScrollEnabled = false
            break


        }
    }


    //Bind Data model on notification arrival
    func updateViewOnNotificationArrival(obj: DriverInvoiceViewModel){
          self.invoiceDataModel = obj
        togglePendingView(status: .Received)
    }
    func handleDriverInvoice(_ notification:Notification)
    {
        let objNotification = notification.object as! PushNotificationObjectModel
        let viewModel = DriverInvoiceViewModel(routeImageId: (objNotification.trip?.routeImageId)!, toAddress: (objNotification.trip?.toAddress)!, fromAddress: (objNotification.trip?.fromAddress)!, status: (objNotification.trip?.status)!, id: (objNotification.trip?.id)!, agreedPrice: (objNotification.trip?.agreedPrice)!, distanceTravelled: 0, endTime: "", startTime: "", duration: 0, stopAddress: ((objNotification.trip?.stop?.count)! > 0) ?  (objNotification.trip?.stop![0].address!)! : "",payment: objNotification.trip?.payment
        )
        
        self.updateViewOnNotificationArrival(obj: viewModel)
    }

    //Set Bill Details
    private func setBillDetails(){
        
        var totalAmountPayable = 0.0
        var totalRideAmount = 0.0
        var tipPercent  = 0
        var tipAmount  = 0.0
        var driverFee  = 0.0

        if let amountPayable = self.invoiceDataModel?.payment?.stripeAccountAmount
        {
            totalAmountPayable = Double(amountPayable)
           
        }
        if let totalTipPercent = self.invoiceDataModel?.payment?.compTipPercent
        {
            tipPercent = totalTipPercent
        }
        if let totalTipAmount = self.invoiceDataModel?.payment?.compTip
        {
            tipAmount = Double(totalTipAmount)
        }
        if let rideAmount = self.invoiceDataModel?.payment?.amountPayable
        {
            totalRideAmount = Double(rideAmount)
        }
        
        if let driverFees = self.invoiceDataModel?.payment?.applicationFee
        {
            driverFee = Double(driverFees)
        }
        
        if tipPercent != 0
        {
            self.labelTip.text = String(format: "Tip (%d%%)",tipPercent)
        }else
        {
            self.labelTip.text = String(format: "Tip (%d%%)",tipPercent)
        }
        if driverFee != 0
        {
            self.lblDriverFee.text = String(format: "-$%.2f", driverFee)
        }else
        {
           self.lblDriverFee.text = String(format: "$%.2f", driverFee)
        }
        
        
        self.lblTipValue.text = AppUtility.getFormattedPriceString(withPrice: tipAmount)
        
        self.lblRideFare.text = AppUtility.getFormattedPriceString(withPrice: totalRideAmount-tipAmount )
        
        self.lblTotalAmt.text = AppUtility.getFormattedPriceString(withPrice: totalAmountPayable)
        
        self.lblBillAmount.text =  self.lblTotalAmt.text
    }



    //Redirect to homecontroller
    @IBAction func submitBtnAction(_ sender: UIButton) {

       //if  let viewControllersCount = self.navigationController?.viewControllers.count
        
        if let viewControllersCount = self.navigationController?.viewControllers.count, viewControllersCount >= 2,(self.navigationController?.viewControllers[viewControllersCount-2] is TripListingViewController)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
        self.navigatedToSpecifiedController(viewController: DriverHomeViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        }
    }

}
