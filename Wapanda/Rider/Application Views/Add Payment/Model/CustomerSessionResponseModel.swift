//
//  CustomerSessionResponseModel.swift
//
//  Created by Daffolapmac on 30/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class CustomerSessionResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stripeCustomerSession = "stripeCustomerSession"
  }

  // MARK: Properties
  public var stripeCustomerSession: StripeCustomerSession?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    stripeCustomerSession <- map[SerializationKeys.stripeCustomerSession]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stripeCustomerSession { dictionary[SerializationKeys.stripeCustomerSession] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.stripeCustomerSession = aDecoder.decodeObject(forKey: SerializationKeys.stripeCustomerSession) as? StripeCustomerSession
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(stripeCustomerSession, forKey: SerializationKeys.stripeCustomerSession)
  }

}
