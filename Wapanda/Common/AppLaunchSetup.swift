/**
 This class handle all the setup work on project launch
 */


import Foundation
import HockeySDK
import IQKeyboardManager
import AFNetworking
import GoogleMaps
import GooglePlaces
import Stripe


class AppLaunchSetup: NSObject {
    
    /*
     Create Singleton object for the class
     */
    static let shareInstance = AppLaunchSetup()
    
    private override init() {
        
    }
    
    /**
     This method used to setup hockey sdk services on launch
     */
    func setupHockeySDK(){
        let keyHockeyApp = PListUtility.getValue(forKey: AppConstants.PListKeys.KEY_HOCKEY_APP) as! String
        
        if !keyHockeyApp.isEmpty{
            BITHockeyManager.shared().configure(withIdentifier: keyHockeyApp);
            BITHockeyManager.shared().start();
            BITHockeyManager.shared().authenticator.authenticateInstallation();
        }
    }
    
    /**
     This method used to setup hockey sdk services on launch
     */
    func setupStripeSDK(){
        let keyStripeApp = PListUtility.getValue(forKey: AppConstants.PListKeys.STRIPE_KEY_PUBLISHABLE) as! String
        let merchantId = PListUtility.getValue(forKey: AppConstants.PListKeys.APPLE_PAY_MERCHANT_ID) as! String
        STPPaymentConfiguration.shared().publishableKey = keyStripeApp
        STPPaymentConfiguration.shared().appleMerchantIdentifier = merchantId
    }
    
    /**
     This method is used to enable IQKeyabord
     */
    func setupIQKeyboard(){
        IQKeyboardManager.shared().shouldShowTextFieldPlaceholder = false
        IQKeyboardManager.shared().isEnabled = true
    }
    
    /**
     This method is used to enable network rechability monitoring
     */
    func startMonitoringNetworkRechability(){
        AFNetworkReachabilityManager.shared().startMonitoring()
    }
    
    /**
     This method used to setup google map sdk on launch
     */
    func setupGoogleMap(){
        let keyGoogleMap = PListUtility.getValue(forKey: AppConstants.PListKeys.KEY_GOOGLE_MAP) as! String
        GMSServices.provideAPIKey(keyGoogleMap)
        GMSPlacesClient.provideAPIKey(keyGoogleMap)
    }
}
