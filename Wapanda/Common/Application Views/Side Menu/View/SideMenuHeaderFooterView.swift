//
//  SideMenuHeaderFooterView.swift
//  Wapanda
//
//  Created by daffomac-31 on 04/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

protocol SideMenuHeaderFooterViewDelegate: class {
    func profileClicked()
}

class SideMenuHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgStar: UIImageView!
    weak var delegate: SideMenuHeaderFooterViewDelegate?

    @IBOutlet weak var btnLogin: UIButton!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        self.viewImage.setCornerCircular(self.viewImage.bounds.size.width/2)
        self.imgView.setCornerCircular(self.imgView.bounds.size.width/2)
        self.addTapGestureOnName()
    }

    func addTapGestureOnName(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileClicked))
        tapGesture.numberOfTapsRequired = 1
        self.lblName.addGestureRecognizer(tapGesture)
    }
    
    /// Binds header data
    ///
    /// - Parameters:
    ///   - name: String value as user name
    ///   - rating: String value as rating
    func bind(withUserName name: String, rating: String?, imgId: String){
        self.lblName.text = name
        
        if let ratingValue = rating{
            self.lblRating.isHidden = false
            self.imgStar.isHidden = false
            
            self.lblRating.text = ratingValue
        }
        else{
            self.lblRating.isHidden = true
            self.imgStar.isHidden = true
        }
        
        if  !imgId.isEmptyString(){
            imgView.contentMode = .scaleAspectFit
        }else{
            imgView.contentMode = .center
        }
        
        imgView.setImageWith(URLRequest.init(url: URL(string:AppUtility.getImageURL(fromImageId: imgId))!), placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"), success: { (request, response, image) in
            self.imgView.image = image
            self.imgView.contentMode = .scaleAspectFit
        }, failure: { (request, response, error) in
            
            self.imgView.image = #imageLiteral(resourceName: "ic_account_placeholder")
            self.imgView.contentMode = .center
        })
       
    }
    
    @IBAction func profileClick(_ sender: Any) {
        self.profileClicked()
    }
    
    func profileClicked(){
        self.delegate?.profileClicked()
    }
    @IBAction func btnLoginAction(_ sender: UIButton) {
        
        AppInitialViewHandler.sharedInstance.setupInitialViewController()

    }
    
}
