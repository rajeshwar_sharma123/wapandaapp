//
//  DriverWelcomeScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/20/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DriverWelcomeScreenViewController: BaseViewController {

    @IBOutlet weak var lblDriverFirstName: UILabel!
    var objDriverWelcomeProfile : LoginResponseModel!
    // MARK:- Driver Welcome Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpInitialView()
        
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used to setup initial view
     */
    func setUpInitialView(){
        self.setupNavigationBar()
        self.lblDriverFirstName.text = "Hi \(self.objDriverWelcomeProfile.firstName?.capitalizeWordsInSentence() ?? "")!"
    }

    /**
     This method is used to setup navigationBar
     */
    func setupNavigationBar(){
        self.hideNavigationBar()
    }
    
    // MARK : - IBOutlets Action Methods
    
    @IBAction func letsGoButtonTapped(_ sender: UIButton) {
        self.navigateToTaxInformationViewController()
    }

 
    // MARK: - Navigation

    func navigateToTaxInformationViewController(){
        
        let taxInfoVC = UIViewController.getViewController(TaxInformationScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        taxInfoVC.objProfileTaxInformation = self.objDriverWelcomeProfile
        self.navigationController?.pushViewController(taxInfoVC, animated: true)
    }


}
