//
//  ForgetPasswordViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 26/07/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import CountryPicker

class ForgetPasswordViewController: BaseViewController {
    
    //MARK:- ForgetPasswordViewController local properties.
    var forgetPasswordPresenter : ForgetPasswordScreenPresenter!
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var txtEmailOrPhone: UITextField!
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet weak var lblEmailOrPhoneError: UILabel!
    @IBOutlet weak var viewPicker: UIView!
    
    //Country picker view outlets
    @IBOutlet weak var _iLeadingSpaceEmailOrPhoneLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickerCountryCode: CountryPicker!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgViewDropdown: UIImageView!
    var countryCode: String = ""
    
    @IBOutlet var _iLeadingEmailOrPhoneToSuperView: NSLayoutConstraint!
    var isCountryCodeHidden = true
    
    //MARK:- View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
        self.addKeyboardActivityListener()
        self.forgetPasswordPresenter = ForgetPasswordScreenPresenter(delegate: self,textFieldValidationDelegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupCountryPicker()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        if isCountryCodeHidden{
            self.hideCountryCode()
        }
        else{
            self.showPickerView()
        }
    }
    
    //MARK:- IBAction for Buttons
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        self.forgetPasswordPresenter.sendForgetPasswordRequest(withData: self.createForgetPasswordScreenRequestModelWithTextFieldInput())
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.txtEmailOrPhone.delegate = self
        self.setupNavigationBar()
        self.customizeTextFields()
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: "")
        self.customizeNavigationBackButton()
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        self.txtEmailOrPhone.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.ForgotPasswordScreen.EMAIL_OR_PHONE_PLACEHOLDER_TEXT, attributes:attributes)
    }
    
    /**
     This method is used to get the forgetPasswordViewRequestModel from the current input in text fields
     
     -returns : EmailForgetPasswordViewRequestModel with current data in textFields
     */
    func createForgetPasswordScreenRequestModelWithTextFieldInput() -> ForgetPasswordScreenRequestModel {
        var forgetPasswordScreenRequestModel = ForgetPasswordScreenRequestModel()
        forgetPasswordScreenRequestModel.email = self.txtEmailOrPhone.text
        forgetPasswordScreenRequestModel.phoneNumber = self.txtEmailOrPhone.text
        forgetPasswordScreenRequestModel.countryCodeExt = self.countryCode
        return forgetPasswordScreenRequestModel
    }
    
    
    // MARK: - Navigation
    
    func navigateToVerifyOTPViewController(withType type:EntryType){
        let verifyVC = UIViewController.getViewController(viewController: VerifyOTPViewController.self)
        verifyVC.entryType = type
        verifyVC.initializeViewWithForgetPasswordRequestModel(model: self.createForgetPasswordScreenRequestModelWithTextFieldInput())
        self.navigationController?.pushViewController(verifyVC, animated: true)
    }
    
    
    
    
    // MARK: - Error Methods
    
    /**
     This method is used to show error on all the text fields with default messages.
     */
    func showAllTextFieldError(){
        self.lblEmailOrPhoneError.text = AppConstants.ValidationErrors.ENTER_EMAILID_OR_PHONE_NUMBER
        self.lblEmailOrPhoneError.isHidden = false
    }
    
    
    /**
     This method is used to show error on email or phone number text fields
     
     -parameter message: String the message that is needed to diaplay as error.
     */
    func showEmailOrPhoneNoTextFieldError(withMessage message:String) {
        
        self.lblEmailOrPhoneError.text = message
        self.lblEmailOrPhoneError.isHidden = false
    }
    
}

// MARK : UITextField Delegate Methods
extension ForgetPasswordViewController : UITextFieldDelegate
{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.txtEmailOrPhone:
            self.lblEmailOrPhoneError.isHidden = true
            
            if string.isEmpty{
                self.hideCountryCode()
            }
            
            return true
        default:
            self.lblEmailOrPhoneError.isHidden = true
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtEmailOrPhone{
            if let textPhone = textField.text, textPhone.isPhoneNumberValid(){
                self.showPickerView()
            }
            else{
                self.hidePickerView()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

// MARK: - ForgetPasswordViewDelegate Methods
extension ForgetPasswordViewController : ForgetPasswordViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    func otpSentSuccessfully(to type: EntryType, withResponseModel forgetPasswordResponseModel: ForgetPasswordResponseModel) {
        self.hideLoader(self)
        self.navigateToVerifyOTPViewController(withType:type)
    }
    
}

// MARK: - TextFieldValidationDelegate Methods
extension ForgetPasswordViewController : TextFieldValidationDelegate
{
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType) {
        
        switch forTextFields {
        case .EmailOrPhone:
            self.showEmailOrPhoneNoTextFieldError(withMessage: message)
            break
        case .All:
            self.showAllTextFieldError()
            break
        default:
            break
        }
        
    }
}

//MARK: - Keyboard Handling 
extension ForgetPasswordViewController{
    //MARK: Keyboard handling methods
    
    /**
     This function adds listener for keyboard
     */
    func addKeyboardActivityListener(){
        let notifCenter = NotificationCenter.default
        notifCenter.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
    }
    
    /**
     This function handle screen on keyboard show event
     */
    func keyboardWillShow(){
        
        //Hide country code picker if open
        self.pickerCountryCode.isHidden = true
    }
    
    /**
     This function hide keyboard from screen
     */
    func hideKeybaord(){
        self.view.endEditing(true)
    }
}

//MARK: Country Code Picker Handling 
extension ForgetPasswordViewController: CountryPickerDelegate{
    
    func showPickerView(){
        self.isCountryCodeHidden = false
        self._iLeadingSpaceEmailOrPhoneLayoutConstraint.isActive = true
        self._iLeadingEmailOrPhoneToSuperView.isActive = false
        self.viewPicker.isHidden = false
    }
    
    func hidePickerView(){
        self.hideCountryCode()
        self.resetCountryCode()
    }
    
    func hideCountryCode(){
        self.isCountryCodeHidden = true
        self._iLeadingSpaceEmailOrPhoneLayoutConstraint.isActive = false
        self._iLeadingEmailOrPhoneToSuperView.isActive = true
        self.viewPicker.isHidden = true
    }
    
    func resetCountryCode(){
        self.countryCode = ""
    }

    
    //MARK: Country Code Picker
    
    /**
     This function setup country code picker
     */
    func setupCountryPicker(){
        
        //get corrent country
        let code = Locale.countryCode()
        
        //init Picker
        pickerCountryCode.countryPickerDelegate = self
        pickerCountryCode.showPhoneNumbers = true
        pickerCountryCode.setCountry(code)
    }
    
    /**
     This function is used to set the country phone code for selected country
     
     - parameter code: Country Phone code
     - parameter image: Country Flag Image
     
     */
    func setSelectedCountryWithPhoneCode(code: String, image: UIImage, countryCode: String){
        
        //Set Data on the screen
        self.lblCountryCode.text = code
        self.imgCountryFlag.image = image
        
        //Set data in model
        self.countryCode = AppUtility.getFormattedDiallingCodeFromCode(code: code)
    }
    
    /**
     This function is delegate for the picker selection
     */
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage){
        
        //Set Selected Country Code and Image
        setSelectedCountryWithPhoneCode(code: phoneCode, image: flag, countryCode: countryCode)
    }
    
    
    //MARK: Button Actions
    
    /**
     This function is action listener for the show country
     
     - parameter sender: Show country button object
     
     */
    @IBAction func btnShowCountryPicker(_ sender: UIButton) {
        hideKeybaord()
        setPickerViewAppearance()
    }
    
    private func setPickerViewAppearance(){
        
        if pickerCountryCode.isHidden{
            pickerCountryCode.isHidden = false
        }
        else{
            pickerCountryCode.isHidden = true
        }
    }
}

