//
//  RiderTripViewDelegate.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/18/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
protocol RiderTripViewDelegate: BaseViewProtocol {

    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker,bearing:Double)
    func didCancelTrip()
    func didReceiveErrorOnCancelTrip(_ errorMessage:String)
}
