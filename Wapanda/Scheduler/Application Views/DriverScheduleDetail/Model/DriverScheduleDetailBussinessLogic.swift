//
//  DriverScheduleDetailBussinessLogic.swift
//  Wapanda
//
//  Created by MacBook on 12/7/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DriverScheduleDetailBussinessLogic {

    deinit {
        print("DriverScheduleDetailBussinessLogic deinit")
    }
    
    
    /**
     This method is used for perform start pickUp Confirmation With Valid Inputs constructed into a CancelRiderScheduleRideRequestModel
     
     - parameter inputData: Contains info for DriverScheduleDetailRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performStartPickUpConfirmation(withDriverScheduleDetailRequestModel: DriverScheduleDetailRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForCancelAuction()
        DriverScheduleDetailAPIRequest().makeAPIRequest(withReqFormData: withDriverScheduleDetailRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
        
    }
    
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForCancelAuction() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.START_PICKUP_NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_AUTH, message  : AppConstants.ErrorMessages.INVALID_AUTH)
        errorResolver.registerErrorCode(ErrorCodes.PICKUP_ALREADY_STARTED, message  : AppConstants.ErrorMessages.PICKUP_ALREADY_STARTED)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_ON_TRIP, message  : AppConstants.ErrorMessages.START_PICKUP_ALREADY_ON_TRIP)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_TRIP_STATUS, message  : AppConstants.ErrorMessages.INVALID_TRIP_STATUS)

        return errorResolver
        
    }
    
}

