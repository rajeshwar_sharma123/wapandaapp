import UIKit
import ObjectMapper

class AuctionItemModel: Mappable {
    
    var id = ""
    var riderSelectedFare: Double?
    var pickupDateTime = ""
    var riderId = ""
    var carType = ""
    var estDuration: UIntMax?
    var estDistance: Int?
    var toAddress = ""
    var fromAddress = ""
    var startLoc: StartLocation?
    var endLoc: EndLocation?
    var closeBy: String = ""
    
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        
        id <- map["_id"]
        pickupDateTime <- map["pickupDateTime"]
        riderSelectedFare <- map["riderSelectedFare"]
        riderId <- map["riderId"]
        carType <- map["carType"]
        estDuration <- map["estDuration"]
        estDistance <- map["estDistance"]
        startLoc <- map["startLoc"]
        endLoc <- map["endLoc"]
        fromAddress <- map["fromAddress"]
        toAddress <- map["toAddress"]
        closeBy <- map["closeBy"]
    }

}
