
//Notes:-

protocol ResendEmailOTPViewDelegate:BaseViewProtocol {
    func resendEmailOTPSuccessfully() -> Void
    func showToastWithMessage(_ message:String) -> Void
}
