//
//  StripeAuthenticationResponseModel.swift
//
//  Created by  on 27/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class StripeAuthenticationResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stripeUserId = "stripe_user_id"
    static let stripePublishableKey = "stripe_publishable_key"
    static let errorDescription = "error_description"
    static let tokenType = "token_type"
    static let refreshToken = "refresh_token"
    static let livemode = "livemode"
    static let accessToken = "access_token"
    static let scope = "scope"
    static let error = "error"
  }

  // MARK: Properties
  public var stripeUserId: String?
  public var stripePublishableKey: String?
  public var errorDescription: String?
  public var tokenType: String?
  public var refreshToken: String?
  public var livemode: Bool? = false
  public var accessToken: String?
  public var scope: String?
  public var error: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    stripeUserId <- map[SerializationKeys.stripeUserId]
    stripePublishableKey <- map[SerializationKeys.stripePublishableKey]
    errorDescription <- map[SerializationKeys.errorDescription]
    tokenType <- map[SerializationKeys.tokenType]
    refreshToken <- map[SerializationKeys.refreshToken]
    livemode <- map[SerializationKeys.livemode]
    accessToken <- map[SerializationKeys.accessToken]
    scope <- map[SerializationKeys.scope]
    error <- map[SerializationKeys.error]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stripeUserId { dictionary[SerializationKeys.stripeUserId] = value }
    if let value = stripePublishableKey { dictionary[SerializationKeys.stripePublishableKey] = value }
    if let value = errorDescription { dictionary[SerializationKeys.errorDescription] = value }
    if let value = tokenType { dictionary[SerializationKeys.tokenType] = value }
    if let value = refreshToken { dictionary[SerializationKeys.refreshToken] = value }
    dictionary[SerializationKeys.livemode] = livemode
    if let value = accessToken { dictionary[SerializationKeys.accessToken] = value }
    if let value = scope { dictionary[SerializationKeys.scope] = value }
    if let value = error { dictionary[SerializationKeys.error] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.stripeUserId = aDecoder.decodeObject(forKey: SerializationKeys.stripeUserId) as? String
    self.stripePublishableKey = aDecoder.decodeObject(forKey: SerializationKeys.stripePublishableKey) as? String
    self.errorDescription = aDecoder.decodeObject(forKey: SerializationKeys.errorDescription) as? String
    self.tokenType = aDecoder.decodeObject(forKey: SerializationKeys.tokenType) as? String
    self.refreshToken = aDecoder.decodeObject(forKey: SerializationKeys.refreshToken) as? String
    self.livemode = aDecoder.decodeBool(forKey: SerializationKeys.livemode)
    self.accessToken = aDecoder.decodeObject(forKey: SerializationKeys.accessToken) as? String
    self.scope = aDecoder.decodeObject(forKey: SerializationKeys.scope) as? String
    self.error = aDecoder.decodeObject(forKey: SerializationKeys.error) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(stripeUserId, forKey: SerializationKeys.stripeUserId)
    aCoder.encode(stripePublishableKey, forKey: SerializationKeys.stripePublishableKey)
    aCoder.encode(errorDescription, forKey: SerializationKeys.errorDescription)
    aCoder.encode(tokenType, forKey: SerializationKeys.tokenType)
    aCoder.encode(refreshToken, forKey: SerializationKeys.refreshToken)
    aCoder.encode(livemode, forKey: SerializationKeys.livemode)
    aCoder.encode(accessToken, forKey: SerializationKeys.accessToken)
    aCoder.encode(scope, forKey: SerializationKeys.scope)
    aCoder.encode(error, forKey: SerializationKeys.error)
  }

}
