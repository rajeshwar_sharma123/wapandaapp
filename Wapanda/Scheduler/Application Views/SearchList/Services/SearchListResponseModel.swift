import UIKit
import ObjectMapper

class SearchListResponseModel: Mappable {
    
    var skip = 0
    var limit = 0
    var totalCount = 0
    var itemCount = 0
    var hasPrev = false
    var hasNext = false
    var items = [SearchListItemModel]()
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        
        skip <- map["skip"]
        limit <- map["limit"]
        hasPrev <- map["hasPrev"]
        hasNext <- map["hasNext"]
        totalCount <- map["total_count"]
        itemCount <- map["item_count"]
        items <- map["items"]
    }
}
