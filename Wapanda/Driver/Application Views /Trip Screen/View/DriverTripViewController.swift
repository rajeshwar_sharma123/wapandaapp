//
//  DriverTripViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 27/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import JHTAlertController

class DriverTripViewController: BaseViewController {
    
    @IBOutlet weak var labelPickUpTitle: UILabel!
    @IBOutlet weak var labelPickUp: UILabel!
    @IBOutlet weak var viewRiderImageBackground: UIView!
    @IBOutlet weak var imageViewRiderImage: UIImageView!
    @IBOutlet weak var labelRiderName: UILabel!
    @IBOutlet weak var labelEstimationTime: UILabel!
    @IBOutlet weak var btnEndTrip: UIButton!
    @IBOutlet weak var btnStartRide: UIButton!
    @IBOutlet weak var viewMapGoogle: GMSMapView!
    @IBOutlet weak var bottomContraint: NSLayoutConstraint!
    @IBOutlet weak var bottomContraintWithMultiplierOne: NSLayoutConstraint!
    
    var isFirstTime = true
    var driverLocation :CLLocationCoordinate2D!
    var userCurrentLocationCoordinate: CLLocationCoordinate2D?
    var destinationLocationCoordinate: CLLocationCoordinate2D?
    var presenterGoogleDirection:GoogleDirectionDetailViewPresenter!
    var presenterDriverTrip:DriverTripPresenter!
    var pushNotificationModel:PushNotificationObjectModel!
    var cabMarker : GMSMarker!
    var shouldShowLoader = true
    var shouldShowLoaderForDirection = true
    var polyline: GMSPolyline?
    var tripStartLocation: CLLocationCoordinate2D?
    var tripEndLocation: CLLocationCoordinate2D?
    @IBOutlet weak var viewEndTripWithNextStop: UIView!
    @IBOutlet weak var viewEndTripBottomView: UIView!
    var wayPointMarkerAdded = false
    var presenterVisitedStop: VisitedStopViewPresenter!
    var markerAddStop: GMSMarker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewMapGoogle.delegate = self
        self.presenterDriverTrip = DriverTripPresenter(delegate: self)
        
        self.viewMapGoogle.customizeMapAppearance()
        self.googleDirectionSettingOnLoad()
        LocationServiceManager.sharedInstance.viewDelegate = self
        LocationServiceManager.sharedInstance.checkForUserPermissions()
        self.setCornerRadiusOfRiderProfileImage()
        
        self.presenterVisitedStop = VisitedStopViewPresenter(delegate: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(riderCancelTrip(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_CANCELLED_TRIP), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(riderAddedStop(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_ADDED_STOP), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.hideNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setCornerRadiusOfRiderProfileImage()
    {
        self.viewRiderImageBackground.layoutIfNeeded()
        
        self.imageViewRiderImage.clipsToBounds = true
        self.viewRiderImageBackground.clipsToBounds = true
        
        self.imageViewRiderImage.layer.cornerRadius = self.imageViewRiderImage.frame.width/2
        self.viewRiderImageBackground.layer.cornerRadius = self.viewRiderImageBackground.frame.width/2
    }
    fileprivate func populateInitialTripData()
    {
        if let startLoc = self.pushNotificationModel?.trip?.startLoc?.coordinates
        {
            self.destinationLocationCoordinate = CLLocationCoordinate2D(latitude: Double((startLoc[1])), longitude: Double((startLoc[0])))
        }
        self.labelPickUp.text = self.pushNotificationModel.trip?.fromAddress
        self.labelPickUpTitle.text = "Pick up"
        self.labelRiderName.text = "\(self.pushNotificationModel.trip?.rider?.firstName?.capitalizeWordsInSentence() ?? "") \(self.pushNotificationModel.trip?.rider?.lastName?.capitalizeWordsInSentence() ?? "")"
        if let imageId = self.pushNotificationModel.trip?.rider?.profileImageFileId{
            let imgURL = AppUtility.getImageURL(fromImageId: imageId)
            self.imageViewRiderImage.setImageWith(URL(string: imgURL)!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
        }
        self.labelEstimationTime.text = "\((AppUtility.getTimeInMinutes(fromTimeInMS: (self.pushNotificationModel.trip?.estDuration)!)))"
        
    }
    fileprivate func initialiseDriverStartTripView()
    {
        if let stops = self.pushNotificationModel.trip?.stop, stops.count > 0, !stops.first!.visited!{
            self.showNextStopOption()
        }
        else{
            self.viewEndTripBottomView.isHidden = false
        }
        
        self.viewMapGoogle.clear()
        self.cabMarker = nil
        self.polyline = nil
        if let imageId = self.pushNotificationModel.trip?.rider?.profileImageFileId{
            let imgURL = AppUtility.getImageURL(fromImageId: imageId)
            self.imageViewRiderImage.setImageWith(URL(string: imgURL)!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
        }
        if let endLoc = self.pushNotificationModel?.trip?.endLoc?.coordinates
        {
            self.destinationLocationCoordinate = CLLocationCoordinate2D(latitude: Double((endLoc[1])), longitude: Double((endLoc[0])))
        }
        self.labelRiderName.text = "\(self.pushNotificationModel.trip?.rider?.firstName?.capitalizeWordsInSentence() ?? "") \(self.pushNotificationModel.trip?.rider?.lastName?.capitalizeWordsInSentence() ?? "")"
        self.labelPickUp.text = self.pushNotificationModel.trip?.toAddress
        self.labelPickUpTitle.text = "Ride in progress"
        self.labelEstimationTime.text = "\((AppUtility.getTimeInMinutes(fromTimeInMS: (self.pushNotificationModel.trip?.estDuration)!)))"
        self.getDirection()
    }
    @IBAction func navigateViewTapped(_ sender: Any) {
        if let sourceCoordinates =  self.userCurrentLocationCoordinate,let destinationCoordinates = self.destinationLocationCoordinate
        {
            if UIApplication.shared.canOpenURL(URL(string:AppConstants.ScreenSpecificConstant.DriverTripScreen.GOOGLE_MAP_APP_DEEP_LINK)!)
            {
                UIApplication.shared.open(URL(string:String(format:AppConstants.ScreenSpecificConstant.DriverTripScreen.GOOGLE_MAP_APP_URL_FORMAT,(sourceCoordinates.latitude),(sourceCoordinates.longitude),(destinationCoordinates.latitude),(destinationCoordinates.longitude)))!)
            }
            else
            {
                UIApplication.shared.open(URL(string:String(format:AppConstants.ScreenSpecificConstant.DriverTripScreen.GOOGLE_MAP_SITE_URL_FORMAT,(sourceCoordinates.latitude),(sourceCoordinates.longitude),(destinationCoordinates.latitude),(destinationCoordinates.longitude)))!)
            }
        }
    }
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.shouldShowLoader = true
        
        let alertController = self.initialseAlertController(withTitle: AppConstants.ErrorMessages.ALERT_TITLE, andMessage: AppConstants.ErrorMessages.CANCEL_RIDE_CONFIRMATION)
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.NO_TITLE, style: .cancel, handler: nil))
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.YES_TITLE, style: .default, handler: { (alertAction) in
            self.presenterDriverTrip.sendCancelRideRequest(withTripId: (self.pushNotificationModel.trip?.id)!, andCancelby: "DRIVER")
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func riderViewTapped(_ sender: Any) {
        self.bottomContraint.isActive = !self.bottomContraint.isActive
        self.bottomContraintWithMultiplierOne.isActive = !self.bottomContraintWithMultiplierOne.isActive
        UIView.animate(withDuration: 0.2 , delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            
        })
    }
    @IBAction func myLocationTapped(_ sender: Any) {
        if let currentLocation = self.userCurrentLocationCoordinate{
            self.viewMapGoogle.updateCameraLocation(lat: currentLocation.latitude, long: currentLocation.longitude, zoom: 15.0)
        }
        
    }
    @IBAction func endTripButtonTapped(_ sender: Any) {
        
        self.shouldShowLoader = true
        self.presenterDriverTrip.sendRideEndRequest(withStartRideModel: DriverTripScreenRequestModel(latFrom: self.userCurrentLocationCoordinate?.latitude, lngFrom: self.userCurrentLocationCoordinate?.longitude, startTime:AppUtility.getFormattedDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", date: Date()) , tripId: self.pushNotificationModel.trip?.id))
        
    }
    @IBAction func startEndRideButtonTapped(_ sender: Any) {
        
       
        if let sourceCoordinates =  self.userCurrentLocationCoordinate
        {
            self.shouldShowLoader = true
            self.presenterDriverTrip.sendRideStartRequest(withStartRideModel: DriverTripScreenRequestModel(latFrom: sourceCoordinates.latitude, lngFrom: sourceCoordinates.longitude, startTime:AppUtility.getFormattedDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", date: Date()) , tripId: self.pushNotificationModel.trip?.id))
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: "Unable to fetch location, please try again!")
        }
    }
    
    @IBAction func btnContactTapped(_ sender: Any) {
        let contactVC = UIViewController.getViewController(viewController: ContactViewController.self)
        contactVC.modalPresentationStyle = .overCurrentContext
        
        let name = "\(self.pushNotificationModel.trip?.rider?.firstName?.capitalizeWordsInSentence() ?? "") \(self.pushNotificationModel.trip?.rider?.lastName?.capitalizeWordsInSentence() ?? "")"
        let phone = self.pushNotificationModel.trip?.rider?.phone ?? ""
        var imageId = ""
        if let imgId = self.pushNotificationModel.trip?.rider?.profileImageFileId{
            imageId = imgId
        }
        
        
        contactVC.bindScreen(withName: name, phoneNumber: phone, imgId: imageId, pushModelObject: self.pushNotificationModel)
        self.present(contactVC, animated: true, completion: nil)
    }
    
    @IBAction func btnNextStopClick(_ sender: Any) {
        //Next Stop Clicked
        self.presenterVisitedStop.sendVisitedStopRequest(withVisitedStopViewRequestModel: VisitedStopViewRequestModel(tripId: self.pushNotificationModel.trip!.id, stopId: self.pushNotificationModel.trip!.stop!.first!.id))
    }
    func riderCancelTrip(_ notification:Notification)
    {
        let alertController = self.initialseAlertController(withTitle: AppConstants.ErrorMessages.ALERT_TITLE, andMessage: "Rider cancelled the ride")
        alertController.addAction(JHTAlertAction(title:"Ok", style: .cancel, handler: { (alertAction) in
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    func riderAddedStop(_ notification:Notification)
    {
         self.handleScreenOnRiderAddedStop(withResponseModel: notification.object as! PushNotificationObjectModel)
    }
}
//MARK: Map View Delegate
extension DriverTripViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //        if !self.isFirstTime
        //        {
        //            self.bottomContraint.isActive = true
        //            self.bottomContraintWithMultiplierOne.isActive = false
        //            UIView.animate(withDuration: 0.2 , delay: 0, options: .curveEaseOut, animations: {
        //                self.view.layoutIfNeeded()
        //            }, completion: { _ in
        //
        //            })
        //        }
        //         self.isFirstTime = false
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        self.bottomContraint.isActive = true
        self.bottomContraintWithMultiplierOne.isActive = false
        UIView.animate(withDuration: 0.2 , delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            
        })
    }
}
extension DriverTripViewController: WelcomeScreenViewDelegate{
    func showControllerWithVC(controller: UIViewController){
        self.present(controller, animated: true, completion: nil)
    }
    
    func locationUpdated(lat: Double, long: Double){
        self.userCurrentLocationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if let _ = self.cabMarker
        {
            self.presenterDriverTrip.update(cabMarker: self.cabMarker, withNewLocation: self.userCurrentLocationCoordinate!)
        }
        else
        {
            if self.pushNotificationModel.trip?.status == TRIP_STATUS.CONFIRMED.rawValue
            {
                self.populateInitialTripData()
            }
            else
            {
                self.initialiseDriverStartTripView()
                
            }
            
        }
        
        self.getDirection()
        
        if SocketManager.shared.driverSocket.status == .connected {
            
            SocketManager.shared.updateDriverLocation(CLLocation(latitude: lat, longitude: long))
            
        }else if SocketManager.shared.driverSocket.status != .connecting
            
        {
            SocketManager.shared.connectToDriverSocket()
        }
    }
}
extension DriverTripViewController: GoogleDirectionDetailViewDelegate{
    
    func googleDirectionSettingOnLoad(){
        self.presenterGoogleDirection = GoogleDirectionDetailViewPresenter(delegate: self)
    }
    
    func getDirection(){
        if ReachabilityManager.shared.isNetworkAvailable
        {
            var wayPoint: CLLocationCoordinate2D?
            
            
            if let wayPoints = self.pushNotificationModel.trip?.stop?.first, !wayPoints.visited!{
                wayPoint = CLLocationCoordinate2D(latitude: Double(wayPoints.stop!.coordinates!.last!), longitude: Double(wayPoints.stop!.coordinates!.first!))
            }
            
            self.presenterGoogleDirection.sendGoogleDirectionDetailRequest(withGoogleDirectionDetailViewRequestModel:GoogleDirectionDetailViewRequestModel(source: self.userCurrentLocationCoordinate!, destination: self.destinationLocationCoordinate!, wayPoint: wayPoint), shouldShowLoader: self.shouldShowLoaderForDirection)
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
        }
        
    }
    
    //MARK: Direction View Delegate
    func directionDetailWithData(data: GoogleDirectionResponse){
        self.shouldShowLoaderForDirection = false

        if let stop = self.pushNotificationModel.trip?.stop?.first, !stop.visited!{
            if self.markerAddStop == nil{
                self.markerAddStop  = self.viewMapGoogle.addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D(latitude: Double(stop.stop!.coordinates!.last!), longitude: Double(stop.stop!.coordinates!.first!)), markerImage: #imageLiteral(resourceName: "ic_destination"))
            }
        }
        
        if let routes = data.routes, routes.count > 0{
            self.routeAavailableWithData(data: data)
        }
        else{
            //No Route Available Settings
            self.noRouteAvailableSettings()
        }
    }
    
    func routeAavailableWithData(data: GoogleDirectionResponse){
        self.labelEstimationTime.text = AppUtility.getTimeInMinutes(fromTimeInMS: UIntMax(data.routes!.first!.legs!.first!.duration!.value!*1000))
        
        if let polyline = self.polyline{
            self.polyline = self.viewMapGoogle.updateRoute(withPolyline: polyline, newPolyLinePoints: data.routes!.first!.overviewPolyline!.points!)
        }
        else{
            self.tripStartLocation = CLLocationCoordinate2D(latitude: Double(data.routes!.first!.legs!.first!.startLocation!.lat!), longitude: Double(data.routes!.first!.legs!.first!.startLocation!.lng!))
            self.tripEndLocation = CLLocationCoordinate2D(latitude: Double(data.routes!.first!.legs!.first!.endLocation!.lat!), longitude: Double(data.routes!.first!.legs!.first!.endLocation!.lng!))
            
            self.addMarkersForSourceAndDestination(withSource: self.tripStartLocation!, withDestination: self.tripEndLocation!)
            self.polyline = self.viewMapGoogle.drawRoute(withOverViewPolyline: data.routes!.first!.overviewPolyline!.points!)
            self.hideLoader()
        }
    }
    
    
    func noRouteAvailableSettings(){
        AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_ROUTE_AVAILABLE_MESSAGE)
    }
    
    
    func prepareScreenToDrawRoute(){
        self.viewMapGoogle.clear()
    }
    
    func addMarkersForSourceAndDestination(withSource source: CLLocationCoordinate2D, withDestination destination: CLLocationCoordinate2D){
        self.cabMarker = self.viewMapGoogle.addMarkerWithCoordinates(coordinate: source, markerImage: #imageLiteral(resourceName: "ic_car"))
        let _ = self.viewMapGoogle.addMarkerWithCoordinates(coordinate: destination, markerImage: #imageLiteral(resourceName: "ic_destination"))
    }
    
    func showLoader()
    {
        if shouldShowLoader{
            super.showLoader(self)
        }
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
extension DriverTripViewController : CancelScreenViewDelgate
{
    func didCancelTrip() {
          AppInitialViewHandler.sharedInstance.setupInitialViewController()
    }
    func didReceiveErrorOnCancelTrip(_ errorMessage: String) {
        let alertController = self.initialseAlertController(withTitle: AppConstants.ErrorMessages.ALERT_TITLE, andMessage: errorMessage)
        alertController.addAction(JHTAlertAction(title:"OK", style: .default, handler: { (alertAction) in
              AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
extension DriverTripViewController : DriverTripScreenViewDelgate
{
    
    func rideStartedSuccessfully(withResponseModel:Trip)
    {
        self.pushNotificationModel.trip = withResponseModel
        self.initialiseDriverStartTripView()
    }
    func rideEndedSuccessfully(withResponseModel:DriverTripResponseModel)
    {

        let objInvoiceVC = UIViewController.getViewController(DriverInvoiceViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)

        objInvoiceVC.invoiceDataModel = DriverInvoiceViewModel(routeImageId: withResponseModel.routeImageId!, toAddress: withResponseModel.toAddress ?? "", fromAddress: withResponseModel.fromAddress ?? "", status: withResponseModel.status ?? "", id: withResponseModel.id ?? "0", agreedPrice: withResponseModel.agreedPrice ?? 0.0, distanceTravelled: Int(withResponseModel.distanceTravelled ?? 0), endTime: withResponseModel.endTime ?? "", startTime: withResponseModel.startTime ?? "", duration: Int(withResponseModel.duration ?? 0), stopAddress: ((withResponseModel.stops?.count)! > 0) ?  (withResponseModel.stops![0].address!) : "",payment: withResponseModel.payment)

        self.navigationController?.pushViewController(objInvoiceVC, animated: true)
    }
    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker,bearing:Double)
    {
        let _ = self.viewMapGoogle.updateMarkerAtPosition(withPoints: coordinate, marker: marker, bearing: bearing)
    }
    func didReceiveTripSnapShot(_ rideImage: UIImage) {
        
    }
}

//MARK: Next Stop Handling
extension DriverTripViewController: VisitedStopViewDelegate{
    
    func handleScreenOnRiderAddedStop(withResponseModel: PushNotificationObjectModel){
        self.pushNotificationModel = withResponseModel
        self.getDirection()
        self.showNextStopOption()
        AppUtility.showCustomAlert(title: AppConstants.ErrorMessages.ALERT_TITLE, message: AppConstants.ScreenSpecificConstant.DriverTripScreen.RIDER_ADDED_STOP_ALERT_MESSAGE, presentingVC: self)
    }
    
    func showNextStopOption(){
        self.viewEndTripBottomView.isHidden = false
        self.viewEndTripWithNextStop.isHidden = false
        self.btnEndTrip.isHidden = true
    }
    
    func hideNextStopOption(){
        self.viewEndTripBottomView.isHidden = false
        self.viewEndTripWithNextStop.isHidden = true
        self.btnEndTrip.isHidden = false
    }
    
    //MARK: Visited Stop Delegate 
    func driverVisitedStopSuccessfully(withResponseModel: Trip){
        self.pushNotificationModel.trip = withResponseModel
        self.getDirection()
        self.hideNextStopOption()
        let _ = self.viewMapGoogle.removeMarker(marker: self.markerAddStop!)
        self.markerAddStop = nil
    }
}
