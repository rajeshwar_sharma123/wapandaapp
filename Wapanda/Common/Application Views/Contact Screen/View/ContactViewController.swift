//
//  ContactViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 27/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class ContactViewController: BaseViewController {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    private var phoneNumber = ""
    private var name = ""
    private var imageId: String?
    private var pushNotificationModel: PushNotificationObjectModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.customizeView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// This method is used to bind screen with required data
    ///
    /// - Parameters:
    ///   - name: name of the caller
    ///   - phoneNumber: phone number of the caller
    ///   - imgId: imgId as image id of the caller
    func bindScreen(withName name: String, phoneNumber: String, imgId: String?,pushModelObject:PushNotificationObjectModel){
        self.name = name
        self.phoneNumber = phoneNumber
        self.imageId = imgId
        self.pushNotificationModel = pushModelObject
    }
    
    //MARK: Custom View Handling
    
    /// Method is used to do view customization on load
    func customizeView(){
        self.imgView.setCornerCircular(self.imgView.frame.size.width/2)
        self.lblName.text = self.name.capitalizeWordsInSentence()
        self.lblContact.text = self.phoneNumber
        if let imageId = self.imageId{
            let imgURL = AppUtility.getImageURL(fromImageId: imageId)
            self.imgView.setImageWith(URL(string: imgURL)!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
        }
    }
    
    @IBAction func btnCallClick(_ sender: Any) {
        AppUtility.callNumber(phoneNumber: self.phoneNumber)
    }
    @IBAction func driverInfoTapped(_ sender: Any) {
        if !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            self.navigateToDriverRatingList()
        }
    }
    
    //MARK:Navigation
    private func navigateToDriverRatingList()
    {
        let driverRatingVC =  UIViewController.getViewController(DriverRatingViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        driverRatingVC.driverInfoModel = self.getDriverInfoModel()
        self.present(driverRatingVC, animated: true, completion: {})
    }
    private func getDriverInfoModel() -> DriverInfoResponseModel
    {
        let driverInfoModel = DriverInfoResponseModel(JSON: [:])
        driverInfoModel?.firstName = "\(self.pushNotificationModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")"
        driverInfoModel?.id = "\(self.pushNotificationModel.trip?.driver?.id ?? "")"
        driverInfoModel?.profileImageFileId = "\(self.pushNotificationModel.trip?.driver?.profileImageFileId ?? "")"
        driverInfoModel?.driverProfile = self.pushNotificationModel.trip?.driver?.driverProfile
        driverInfoModel?.cabInfo = Cab(JSON: (self.pushNotificationModel.trip?.vehicle?.dictionaryRepresentation())!)
        return driverInfoModel!
    }
    
    @IBAction func btnSendMessageClick(_ sender: Any) {
        AppUtility.messageNumber(phoneNumber: self.phoneNumber)
    }
    
    @IBAction func btnCrossClick(_ sender: Any) {
        self.dismissScreen()
    }
    
    private func dismissScreen(){
        self.dismiss(animated: true, completion: nil)
    }
}

extension ContactViewController{
    
}
