//
//  UserDoc.swift
//
//  Created by  on 25/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class UserDoc: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastName = "lastName"
    static let firstName = "firstName"
    static let id = "_id"
    static let profileImageFileId = "profileImageFileId"
  }

  // MARK: Properties
  public var lastName: String?
  public var firstName: String?
  public var id: String?
  public var profileImageFileId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lastName <- map[SerializationKeys.lastName]
    firstName <- map[SerializationKeys.firstName]
    id <- map[SerializationKeys.id]
    profileImageFileId <- map[SerializationKeys.profileImageFileId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = profileImageFileId { dictionary[SerializationKeys.profileImageFileId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.profileImageFileId = aDecoder.decodeObject(forKey: SerializationKeys.profileImageFileId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(profileImageFileId, forKey: SerializationKeys.profileImageFileId)
  }

}
