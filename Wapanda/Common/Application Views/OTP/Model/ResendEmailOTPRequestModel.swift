

//Notes:- This class is used for constructing ResendEmailOTP Service Request Model

class ResendEmailOTPRequestModel {
    
    //MARK:- ResendEmailOTPRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()

        
        /**
         This method is used for setting email
         
         - parameter phone: String parameter that is going to be set on email
         
         - returns: returning Builder Object
         */
        
        func setEmail(_ email:String) -> Builder{
            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of ResendEmailOTPRequestModel
         and provide ResendEmailOTPRequestModel object.
         
         -returns : ResendEmailOTPRequestModel
         */
        
        func build()->ResendEmailOTPRequestModel{
            return ResendEmailOTPRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting OTPPopup end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  AppConstants.ApiEndPoints.RESEND_EMAIL_OTP
    }
    
}
