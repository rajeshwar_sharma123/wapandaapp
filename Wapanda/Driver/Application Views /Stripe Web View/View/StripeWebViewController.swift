//
//  StripeWebViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 26/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class StripeWebViewController:BaseViewController {
    
    var urlString : String!
    var delegate : StripeWebViewDelegate!
    @IBOutlet weak var webViewDocuments: UIWebView!
    @IBOutlet weak var lblScreenTitle: UILabel!
    var screenTitle = "Stripe Connect"
    
    // MARK: - Document Viewer Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

         self.intialSetupForView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    // MARK: - Helper Methods
    private func intialSetupForView(){
       // self.setTitle(title: "Connect")
        self.setUpWebView()
        self.setUpNavigationBar()
    }
    @objc private func appDidBecomeActive()
    {
         self.webViewDocuments.loadRequest(URLRequest(url: URL(string: self.urlString)!))
    }
    @objc private func appDidEnterBackground()
    {
         self.hideLoader(self)
    }

    
    private func setTitle(title: String){
        self.lblScreenTitle.text = title
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle: self.screenTitle)
        self.customizeNavigationBackButton()
    }
    private func setUpWebView(){
        self.webViewDocuments.delegate = self
        self.webViewDocuments.loadRequest(URLRequest(url: URL(string: self.urlString)!))
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


// MARK: - UIWebView Delegate Methods
extension StripeWebViewController : UIWebViewDelegate
{
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
       if (request.url?.absoluteString.contains(PListUtility.getValue(forKey: AppConstants.PListKeys.STRIPE_REDIRECT_URI) as! String))!
        {
            let stripeConnectResponseModel = StripeConnectionResponseModel(JSON: (request.url?.queryParameters ?? [:]))
             self.delegate.didCompleteStripeConnection(withResponseModel: stripeConnectResponseModel!)
            self.navigationController?.popViewController(animated: true)
            //self.dismiss(animated: true, completion: {
               
            //})
            return false
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.showLoader(self)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideLoader(self)
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.hideLoader(self)
        
    }
}
extension URL {
    
    public var queryParameters: [String: String]? {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
            return nil
        }
        
        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }
        
        return parameters
    }
}
