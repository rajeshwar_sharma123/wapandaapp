//
//  TaxInformationScreenViewDelgate.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

//Notes:- This protocol is used as a interface which is used by TaxInformationScreenViewPresenter to tranfer info to TaxInformationScreenViewController

protocol AddCreditCardScreenViewDelgate: BaseViewProtocol{
    
}
protocol CardTextFieldValidationDelegate:class {
    
    func showErrorMessage(withMessage message: String,forTextFields: CardEntryType)
}

