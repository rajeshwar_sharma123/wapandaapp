

//Notes:- This class is used for constructing ResendEmailOTP Service Request Model

class ResendOTPRequestModel {
    
    //MARK:- ResendOTPRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var entryType : EntryType!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.entryType = builder.entryType
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var entryType : EntryType!
        
        /**
         This method is used for setting email
         
         - parameter phone: String parameter that is going to be set on email
         
         - returns: returning Builder Object
         */
        
        func setEmail(_ email:String) -> Builder{
            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for setting phone
         
         - parameter phone: String parameter that is going to be set on phone
         
         - returns: returning Builder Object
         */
        
        func setPhone(_ phone:String) -> Builder{
            requestBody["phone"] = phone as AnyObject?
            return self
        }
        
        /**
         This method is used for setting countryCodeExt
         
         - parameter countryCodeExt: String parameter that is going to be set on countryCodeExt
         
         - returns: returning Builder Object
         */
        
        func setCountryCodeExt(_ countryCodeExt:String) -> Builder{
            requestBody["countryCodeExt"] = countryCodeExt as AnyObject?
            return self
        }
        /**
         This method is used for setting Phone OTP
         
         - parameter phoneOTP: String parameter that is going to be set on Phone OTP
         
         - returns: returning Builder Object
         */
        func setEntryType(_ entryType:EntryType)->Builder{
            self.entryType = entryType
            return self
        }

        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of ResendOTPRequestModel
         and provide ResendOTPRequestModel object.
         
         -returns : ResendOTPRequestModel
         */
        
        func build()->ResendOTPRequestModel{
            return ResendOTPRequestModel(builderObject: self)
        }
    }
    /**
     This method is used for getting OTPPopup end point
     
     -returns: String containg end point
     */
    func getPhoneEndPoint()->String{
        return  AppConstants.ApiEndPoints.RESEND_PHONE_OTP
    }
    /**
     This method is used for getting OTPPopup end point
     
     -returns: String containg end point
     */
    func getEmailEndPoint()->String{
        return  AppConstants.ApiEndPoints.RESEND_EMAIL_OTP
    }
    
}
