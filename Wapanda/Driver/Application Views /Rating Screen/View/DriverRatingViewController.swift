//
//  DriverRatingViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 27/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import HCSStarRatingView
class DriverRatingViewController: BaseViewController {
    
    @IBOutlet weak var tableViewDriverRating: UITableView!
    var driverRatingList : DriverRatingResponseModel!
    var driverInfoModel : DriverInfoResponseModel!
    fileprivate var previouslySelectedIndex = 0
    fileprivate var selectedIndex = 0
    var presenterDriverRating : DriverRatingPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenterDriverRating = DriverRatingPresenter(delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.initTableView()
        self.presenterDriverRating.sendDriverRatingRequest(withDriverInfoModel: DriverRatingScreenRequestModel(driverId: self.driverInfoModel.id, skip: 0, limit: 10))
    }
    func initTableView(){
        self.tableViewDriverRating.registerTableViewCell(tableViewCell: NoResultTableViewCell.self)
        self.tableViewDriverRating.registerTableViewHeaderFooterView(tableViewHeaderFooter: DriverInfoSectionHeaderView.self)
         self.tableViewDriverRating.registerTableViewHeaderFooterView(tableViewHeaderFooter: VehicleImageSectionHeaderView.self)
        self.tableViewDriverRating.tableHeaderView = self.initialiseRatingTableViewHeader(self.driverInfoModel)
         self.tableViewDriverRating.tableFooterView = UIView()
            self.tableViewDriverRating.delegate = self
        self.tableViewDriverRating.dataSource = self
    }
    fileprivate func getFormattedRatedOnTime(fromTimeStamp timeStamp:String)->String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let calendar = NSCalendar.current
        let ratedOnDate = timeStamp.getDateFromZoneFormate()
        
        if calendar.isDateInToday(ratedOnDate)
        {
            dateFormatter.dateFormat = "h:mm a"
            return String(format:"Today, %@",dateFormatter.string(from: ratedOnDate).lowercased())
        }
        else if calendar.isDateInYesterday(ratedOnDate)
        {
            dateFormatter.dateFormat = "h:mm a"
            return String(format:"Yesterday, %@",dateFormatter.string(from: ratedOnDate).lowercased())
        }
        else
        {
            dateFormatter.dateFormat = "MMM dd yyy"
            let dateString = dateFormatter.string(from: ratedOnDate)
            dateFormatter.dateFormat = "h:mm a"
            let timeString = dateFormatter.string(from: ratedOnDate).lowercased()
            return String(format:"%@, %@",dateString,timeString)
        }
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension DriverRatingViewController : DriverRatingScreenViewDelgate
{
    func didReceiveDriverRatingList(withResponseModel: DriverRatingResponseModel) {
        
       
        if let _ = self.driverRatingList
        {
            let lastIndex = (self.driverRatingList.rateAndReviews?.items?.count)! // First Index of newly added items
            
            self.driverRatingList.rateAndReviews?.items?.append(contentsOf:withResponseModel.rateAndReviews!.items!)
            self.driverRatingList.rateAndReviews?.hasNext = withResponseModel.rateAndReviews?.hasNext
            
            //Index path of newly added items
            var rowsToAdd = [IndexPath]()
            for indexRow in lastIndex ..< (self.driverRatingList.rateAndReviews?.items?.count)!
            {
                rowsToAdd.append(IndexPath(row: indexRow, section: 0))
            }
            
            //Insert new added rows
            self.tableViewDriverRating.beginUpdates()
            self.tableViewDriverRating.insertRows(at: rowsToAdd, with: .none)
            self.tableViewDriverRating.endUpdates()
        }
        else
        {
            self.driverRatingList = withResponseModel
            self.tableViewDriverRating.reloadData()
              self.tableViewDriverRating.selectRow(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .none)
        }
    }
    
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
//MARK: UITable View Delegate & DataSource
extension DriverRatingViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.initialiseDriverInfoSectionViewHeader(self.driverInfoModel)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            return ratings.count
        }
        else{
            //No Rating Found
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            return 50
        }
        else{
            //No Rating Found
            return self.tableViewDriverRating.frame.size.height
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            return UITableViewAutomaticDimension
        }
        else{
            //No Rating Found
            return self.tableViewDriverRating.frame.size.height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            let cell = tableView.getCell(withCellType: DriverRatingTableViewCell.self)
            cell.labelDate.text = self.getFormattedRatedOnTime(fromTimeStamp: ratings[indexPath.row].ratedOn!)
            if let review = ratings[indexPath.row].review,review != "",self.selectedIndex == indexPath.row
            {
                cell.labelFeedback.text = ratings[indexPath.row].review
            }
            else if self.selectedIndex == indexPath.row
            {
                cell.labelFeedback.text = "No review available."
            }
            else
            {
                cell.labelFeedback.text = ""
            }

            cell.driverRatingView.value = CGFloat(ratings[indexPath.row].rating!)
            cell.selectionStyle = .none
            return cell
        }
        else{
            //No Rating Found
            let cell = tableView.getCell(withCellType: NoResultTableViewCell.self)
                cell.bind(title: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count-1 == indexPath.row,(self.driverRatingList.rateAndReviews?.hasNext!)!{
              self.presenterDriverRating.sendDriverRatingRequest(withDriverInfoModel: DriverRatingScreenRequestModel(driverId: self.driverInfoModel.id, skip: self.driverRatingList?.rateAndReviews?.items?.count, limit: 10))
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            
            self.selectedIndex = indexPath.row
            self.tableViewDriverRating.reloadRows(at: [IndexPath(item: self.previouslySelectedIndex, section: 0),indexPath], with: .none)
            self.previouslySelectedIndex = self.selectedIndex
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let frameSectionView = self.tableViewDriverRating.rect(forSection: 0)
        let relativeFrame = self.tableViewDriverRating.convert(frameSectionView, to: self.view)
        
        let initialPositionOfSection = (self.view.frame.height*0.3)
        let currentPositionOfSection = (relativeFrame.origin.y)
        
        // Ratio is the relative position of section with respect to the initial position.
        var ratio = currentPositionOfSection/initialPositionOfSection
        
        if      ratio > 1 { ratio = 1 }
        else if ratio < 0 { ratio = 0 }
        
        self.animateColor(ofTableHeaderViewWithRatio: ratio)
        self.animateColor(ofLabelsOfSectionHeaderViewWithRatio: ratio)
        self.animateColor(ofSectionHeaderViewWithRatio: ratio)
        self.animateColor(ofStartImageViewWithRatio:ratio)

    }
    func animateColor(ofLabelsOfSectionHeaderViewWithRatio ratio:CGFloat)
    {
        // ColorChange = (FinalColorCode * InverseRatio) + (InitialColor*Ratio)
        let topRowColor = (((255*(1-ratio))+(64*ratio))/255.0)
        let bottomRowColor = (((255*(1-ratio))+(166*ratio))/255.0)
        if let sectionView = self.tableViewDriverRating.headerView(forSection: 0) as? DriverInfoSectionHeaderView
        {
        sectionView.labelDriverName.textColor = UIColor(red: topRowColor, green: topRowColor, blue: topRowColor,alpha:1)
        sectionView.labelDriverRating.textColor = UIColor(red: topRowColor, green: topRowColor, blue: topRowColor,alpha:1)
        
        sectionView.labelVehicleNumber.textColor = UIColor(red: bottomRowColor, green: bottomRowColor, blue: bottomRowColor, alpha:1)
        sectionView.labelVehicleServiceName.textColor = UIColor(red: bottomRowColor, green: bottomRowColor, blue: bottomRowColor, alpha:1)
        if ratio == 1 {
            sectionView.imageViewstar.getImageViewWithOutImageTintColor(color: .white)
            sectionView.removeBottomShadow()
        }
        else if ratio == 0 {
            sectionView.imageViewstar.getImageViewWithImageTintColor(color: .white)
            sectionView.addBottomShadow()
        }
        }
    }
    func animateColor(ofSectionHeaderViewWithRatio ratio:CGFloat)
    {
        if let sectionView = self.tableViewDriverRating.headerView(forSection: 0) as?DriverInfoSectionHeaderView
        {
        sectionView.viewBackground.backgroundColor = UIColor(red: 255/255.0, green: 110/255.0, blue: 110/255.0, alpha:1-ratio)
        }
    }
    func animateColor(ofStartImageViewWithRatio ratio:CGFloat)
    {
        if let sectionView = self.tableViewDriverRating.headerView(forSection: 0) as?DriverInfoSectionHeaderView
        {
            let imageStartColor = (((255*(1-ratio))+(0*ratio))/255.0)
            sectionView.imageViewstar.getImageViewWithImageTintColor(color: UIColor(red: imageStartColor, green: imageStartColor, blue: imageStartColor, alpha:1))
        }
    }
    func animateColor(ofTableHeaderViewWithRatio ratio:CGFloat)
    {
        if let tableheaderView = self.tableViewDriverRating.tableHeaderView as? VehicleImageSectionHeaderView
        {
        tableheaderView.viewBackground.backgroundColor = UIColor(red: 255/255.0, green: 110/255.0, blue: 110/255.0, alpha:1-ratio)
        }
    }
    
    func initialiseRatingTableViewHeader(_ objDriverProfile:DriverInfoResponseModel)->VehicleImageSectionHeaderView
    {
        let nib = self.tableViewDriverRating.getHeader(withHeaderType: VehicleImageSectionHeaderView.self)
        nib.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height*0.3)
        if objDriverProfile.profileImageFileId != AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING {
            let imgURL = AppUtility.getImageURL(fromImageId: objDriverProfile.profileImageFileId)
            nib.imageViewDriver.setImageWith(URL(string: imgURL)!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
        }
        if let imageId = objDriverProfile.cabInfo?.imageId {
            let imgURL = AppUtility.getImageURL(fromImageId: imageId)
            nib.imageViewVehicle.setImageWith(URL(string: imgURL)!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
        }
        nib.viewBackgroundDriverImage.clipsToBounds = true
        nib.viewBackgroundDriverImage.layer.cornerRadius = nib.viewBackgroundDriverImage.frame.height/2
        nib.imageViewDriver.clipsToBounds = true
        nib.imageViewDriver.layer.cornerRadius = nib.imageViewDriver.frame.height/2
        return nib
        
    }
    func initialiseDriverInfoSectionViewHeader(_ objDriverProfile:DriverInfoResponseModel)->DriverInfoSectionHeaderView
    {
        let nib = self.tableViewDriverRating.getHeader(withHeaderType: DriverInfoSectionHeaderView.self)
        nib.labelDriverName.text = objDriverProfile.firstName
        nib.labelDriverRating.text = String(format:"%.1f",objDriverProfile.driverProfile?.averageRating ?? 0.0)
        nib.labelVehicleServiceName.text = "\(objDriverProfile.cabInfo?.make ?? "") \(objDriverProfile.cabInfo?.model ?? "")"
        nib.labelVehicleNumber.text = objDriverProfile.cabInfo?.carPlateNumber?.uppercased() ?? ""
        return nib
    }
}
