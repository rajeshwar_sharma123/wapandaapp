import UIKit

protocol PickupFareViewControllerDelegate: class {
    func fareSelected(fare: Float)
}


class PickupFareViewController: UIViewController {
    
    @IBOutlet weak var boundaryView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    var prices = [Float]()
    var delegate: PickupFareViewControllerDelegate?
    
    let MIN_PRICE = 1
    let MAX_PRICE = 200
    let PRICE_INCR = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        
        for i in 0...199 {
            prices.append(Float(MIN_PRICE + i * PRICE_INCR))
        }
        boundaryView.setBorderToView(withWidth: 1.2, color: UIColor.filterPickerBorderColor())
        boundaryView.setCornerCircular(6)
        pickerView.reloadAllComponents()
        setupNavigationBar()
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, color: UIColor.appDarkGrayColor(),isTranslucent: false)
        self.customizeNavigationBackButton()
    }
    
    @IBAction func setFare() {
        let selectedIndex = pickerView.selectedRow(inComponent: 0)
        delegate?.fareSelected(fare: prices[selectedIndex])
        navigationController?.popViewController(animated: true)
    }
}


extension PickupFareViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: picker view datasource methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        pickerView.subviews.forEach({
            $0.isHidden = $0.frame.height < 2.0
        })
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return prices.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel
        
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            pickerLabel?.frame.size.height = 50
            pickerLabel?.textColor = UIColor.white
            pickerLabel?.font = UIFont(name: "Titillium-Semibold", size: 32)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        
        pickerLabel?.text = "$\(prices[row])"
        
        return pickerLabel!
    }
}
