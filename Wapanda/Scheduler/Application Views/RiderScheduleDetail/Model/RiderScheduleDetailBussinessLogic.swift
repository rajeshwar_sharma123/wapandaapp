//
//  RiderScheduleDetailBussinessLogic.swift
//  Wapanda
//
//  Created by Daffodil on 28/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class RiderScheduleDetailBussinessLogic {
    
    
    deinit {
        print("RiderScheduleDetailBussinessLogic deinit")
    }
    
    
    /**
     This method is used for perform accept auction With Valid Inputs constructed into a CancelRiderScheduleRideRequestModel
     
     - parameter inputData: Contains info for CancelRiderScheduleRideRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performCancelRide(withCancelRiderScheduleRideRequestModel cancelRiderScheduleRideRequestModel: CancelRiderScheduleRideRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForCancelAuction()
        RiderScheduleDetailAPIRequest().makeAPIRequest(withReqFormData: cancelRiderScheduleRideRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
      
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
   
    private func registerErrorForCancelAuction() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION_BID)
         errorResolver.registerErrorCode(ErrorCodes.ALREADY_CANCELLED, message  : AppConstants.ErrorMessages.ALREADY_CANCELLED)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_RIDER, message  : AppConstants.ErrorMessages.INVALID_RIDER)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_DRIVER, message  : AppConstants.ErrorMessages.INVALID_DRIVER)
        
        return errorResolver

    }
    
}
