import UIKit

class AllBidsCell: UITableViewCell {

//    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var lblPickUpTime: UILabel!
    @IBOutlet weak var lblRideTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblMiles: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
//        let borderColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
//        borderView.setBorderToView(withWidth: 1, color: borderColor)
//        borderView.setCornerCircular(4)
    }
    
    func bind(data: AuctionItemModel) {
        
        if let estDuration = data.estDuration{
            lblRideTime.text = AppUtility.getTimeInMinutes(fromTimeInMS: estDuration) + " DRIVE"
        }
        
        lblPickUpTime.text = AppUtility.getDateFromUTCFormat(dateStr: data.closeBy, format: "MMM dd")
        
        if let fare = data.riderSelectedFare{
            lblPrice.text =  AppUtility.getFormattedPriceString(withPrice: fare)
        }
        
        lblAddress.text = data.fromAddress
        lblMiles.text = "\(AppUtility.getTripDistanceInMiles(data.estDistance!)) MILES"
        
    }
}
