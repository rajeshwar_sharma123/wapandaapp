//
//  WaitingPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class WaitingPresenter: ResponseCallback{
    
    //MARK:- WaitingPresenter local properties
    private weak var waitingViewDelegate             : WaitingScreenViewDelgate?
    private lazy var waitingBusinessLogic         : WaitingBusinessLogic = WaitingBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:WaitingScreenViewDelgate) {
        self.waitingViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.waitingViewDelegate?.hideLoader()
        if responseObject is NewRideResponseModel
        {
            self.waitingViewDelegate?.didAcceptRidePrice(withResponseModel: responseObject as! NewRideResponseModel)
        }
        if responseObject is RiderRejectResponseModel
        {
            self.waitingViewDelegate?.didRejectRidePrice(withResponseModel: responseObject as! RiderRejectResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.waitingViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.waitingViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendAcceptRideRequest(withBiddingId biddingId: String) -> Void{
        
        self.waitingViewDelegate?.showLoader()
        
        var acceptRequestModel : WaitingRequestModel!
        
        
            acceptRequestModel = WaitingRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setBiddingId(biddingId).build()
        
            self.waitingBusinessLogic.performRideAcceptRequest(withWaitingRequestModel: acceptRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendRejectRideRequest(withBiddingId biddingId: String) -> Void{
        
        self.waitingViewDelegate?.showLoader()
        
        var acceptRequestModel : WaitingRequestModel!
        
        
        acceptRequestModel = WaitingRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setBiddingId(biddingId).build()
        
        self.waitingBusinessLogic.performRideRejectRequest(withWaitingRequestModel: acceptRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData taxScreenRequestModel:WaitingScreenRequestModel) -> Bool {
        
        return true
    }
}
