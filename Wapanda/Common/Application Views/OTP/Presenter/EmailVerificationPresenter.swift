
//Notes:- This class is used as presenter for OTPVerificationPresenter

import Foundation
import ObjectMapper

class EmailVerificationPresenter: ResponseCallback{
    
//MARK:- OTPPopupViewPresenter local properties
    private weak var otpPopupViewDelegate          : EmailVerificationViewDelegate?
    private var otpPopupBusinessLogic              : EmailVerificationBusinessLogic!
    private weak var textFieldValidationDelegate : TextFieldValidationDelegate?
    
//MARK:- Constructor
    init(delegate responseDelegate:EmailVerificationViewDelegate){
        self.otpPopupViewDelegate = responseDelegate
        self.textFieldValidationDelegate = responseDelegate as? TextFieldValidationDelegate
        self.otpPopupBusinessLogic = EmailVerificationBusinessLogic()
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.otpPopupViewDelegate?.hideLoader()
        self.otpPopupViewDelegate?.emailVerifiedSuccessfully()
    }
    
    func servicesManagerError(error : ErrorModel){
        self.otpPopupViewDelegate?.hideLoader()
        self.otpPopupViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- API calls
    
    func verifyEmail(withOTP otp:String, email: String, userID: String){
        
        guard validateInput(withData: otp) else {
            return
        }
        
        self.otpPopupViewDelegate?.showLoader()

        let otpPopupRequestModel:EmailVerificationRequestModel = EmailVerificationRequestModel.Builder()
            .setOTP(otp)
            .setEmail(email)
            .setUserId(userID)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.otpPopupBusinessLogic.performOTPVerification(withEmailVerificationRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
    
//MARK:- Validation for input fields method
    
    func validateInput(withData otp:String) -> Bool {
        
        guard !otp.isEmptyString() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_OTP, forTextFields: .OTP)
            return false
        }
        
        guard otp.isOTPValid() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_OTP, forTextFields: .OTP)
            return false
        }
        
        return true
    }
}
