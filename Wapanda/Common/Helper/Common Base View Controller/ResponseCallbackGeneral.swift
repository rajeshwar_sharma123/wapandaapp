
//Note :- This method is used for Handling Api Response

import ObjectMapper

protocol ResponseCallbackGeneral:class {
    
    func servicesManagerSuccessResponse<T:AnyObject>(responseObject : T)
    func servicesManagerError(error : ErrorModel)
}

