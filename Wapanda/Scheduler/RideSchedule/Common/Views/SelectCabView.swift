import UIKit

protocol SelectCabDelegate: class {
    func selectCabClicked()
}

class SelectCabView: UIView {
    
    @IBOutlet weak var lblCabType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var cabIconImageView: UIImageView!
    
    @IBOutlet weak var dropDownImageView: UIImageView!
    
    weak var delegate: SelectCabDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        let view = Bundle.main.loadNibNamed("SelectCabView", owner: self, options: nil)?[0] as! UIView
        addSubview(view)
        view.frame = self.bounds
    }

    
    func bindData(cabInfo: SelectedCabModel) {
        lblCabType.text = cabInfo.name
        
        let icon = cabInfo.icon
        if let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: icon)){
            cabIconImageView.setImageWith(imageUrl, placeholderImage: #imageLiteral(resourceName: "ic_driver_side").withRenderingMode(UIImageRenderingMode.alwaysTemplate))
        }
        else {
            cabIconImageView.image =  #imageLiteral(resourceName: "ic_driver_side").withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        }
        
        if cabInfo.estPrice == 0{
            lblPrice.text = "--.--"
        }else{
        lblPrice.text = AppUtility.getFormattedPriceString(withPrice: cabInfo.estPrice)
        }
    }
    
    @IBAction func selectedCabClicked(_ sender: Any) {
        
        delegate?.selectCabClicked()
    }
}
