//
//  NewPasswordScreenRequestModel.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
struct NewPasswordScreenRequestModel {
    var otp                 : String!
    var newPassword                 : String!
}
