import UIKit

class BidDetailViewController: BaseViewController, BidDetailViewDelegate {
    
    @IBOutlet weak var btnPlaceBid: UIButton!
    @IBOutlet weak var lblBidRange: UILabel!
    @IBOutlet weak var lblBidStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerBoundaryView: UIView!
    @IBOutlet weak var countDownTimer: SRCountdownTimer!
    private let  COUNTER_TIME : TimeInterval = 60

    private var presenter: BidDetailPresenter?
    fileprivate var bidDetail: BidDetailResponseModel?
    fileprivate var prices = [Double]()
    fileprivate let FARE_INCREMENT = 0.25
    var bidId = ""
    var isOnlineDriver = false
    var riderSelectedFare: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        
        customizeNavigationBarWithTitle(navigationTitle: "", color: UIColor.appDarkThemeColor(), isTranslucent: false)
        customizeNavigationBackButton()

        presenter = BidDetailPresenter(delegate: self)
        presenter?.getBidsDetail(id: bidId)
        let borderColor = UIColor(red: 255/255.0, green:  139/255.0, blue:  139/255.0, alpha:0.5)
        pickerBoundaryView.setBorderToView(withWidth: 1.2, color: borderColor)
        pickerBoundaryView.setCornerCircular(6)
        userImageView.setCornerCircular(userImageView.frame.width/2)
        userImageView.setBorderToView(withWidth: 6, color: UIColor.white)
        initializeCounterTimer()
    }
    
    @IBAction func placeBidClicked(_ sender: UIButton) {
//        guard isOnlineDriver else {
//            super.showErrorAlert("Alert", alertMessage: "To place a bid you have to go Online", VC: self)
//            return
//        }
        
        guard bidDetail?.detail?.riderId != AppDelegate.sharedInstance.userInformation.id! else {
            super.showErrorAlert("Alert", alertMessage: "You cannot bid on your own ride", VC: self)
            return
        }
        
        guard let fairValues = bidDetail?.fares else { return }
        let selectedIndex = pickerView.selectedRow(inComponent: 0)
        presenter?.addDriverQuote(bidId: bidId, price: Float(prices[selectedIndex]), fareValues: fairValues)
    }
    
    
    func showDetail(bidDetail: BidDetailResponseModel){
        
        startCounterTimer(COUNTER_TIME)

        self.bidDetail = bidDetail
        guard let fairValues = bidDetail.fares else { return }

        let minFare = AppUtility.getFormattedPriceString(withPrice: fairValues.minFare)
        let maxFare = String(format: "%.2f", fairValues.maxFare)
        lblBidRange.text = minFare + "-" + maxFare
        lblName.text = bidDetail.riderName
        
        if !bidDetail.riderImage.isEmpty {
            let url = AppUtility.getImageURL(fromImageId: bidDetail.riderImage)
            userImageView.setImageWith(URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_emergency_contact"))
        }
        else {
            userImageView.image = #imageLiteral(resourceName: "ic_placeholder_emergency_contact")
        }
        
        var fare = fairValues.minFare
        
        while (fare <= fairValues.maxFare) {
            prices.append(fare)
            fare += FARE_INCREMENT
        }
        pickerView.reloadAllComponents()

        var midIndex = Int(prices.count/2)
        
        let matchedIndex = prices.index(of: riderSelectedFare)
        if matchedIndex != nil{
            midIndex = matchedIndex!
        }
        setLowHighBidRange(price: prices[midIndex])
        pickerView.selectRow(midIndex, inComponent: 0, animated: false)
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        //super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
        AppUtility.presentToastWithMessage(alertMessage)
    }
    
    func showLoader() {
        self.showLoader(self)
    }
    
    func hideLoader() {
        self.hideLoader(self)
    }
    
    private func initializeCounterTimer()
    {
        self.countDownTimer.lineWidth = 2.0
        self.countDownTimer.backgroundColor = UIColor.clear
        self.countDownTimer.lineColor = UIColor.white
        self.countDownTimer.delegate = self
    }
    private func startCounterTimer(_ counterTotalTime:TimeInterval){
        self.countDownTimer.elapsedTime = 0
        self.countDownTimer.start(beginingValue: Int(counterTotalTime))
    }
    
    func bidPlaced() {
        navigationController?.popViewController(animated: true)
    }
    
}

extension BidDetailViewController: SRCountdownTimerDelegate
{
    func timerDidUpdateCounterValue(newValue: Int){
        if (newValue < 15){
            self.countDownTimer.lineColor = UIColor.white
        }
    }
    
    func timerDidStart(){
        countDownTimer.lineColor = UIColor.white
        countDownTimer.trailLineColor = UIColor.clear
    }
    func timerDidPause(){}
    func timerDidResume(){}
    func timerDidEnd(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension BidDetailViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: picker view datasource methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        pickerView.subviews.forEach({
            $0.isHidden = $0.frame.height < 2.0
        })
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return prices.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard row > 0 else { return}
        setLowHighBidRange(price: self.prices[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel
        
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            pickerLabel?.frame.size.height = 50
            pickerLabel?.textColor = UIColor.white
            pickerLabel?.font = UIFont(name: "Titillium-Semibold", size: 32)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        
        pickerLabel?.text = "$\(prices[row])"
        
        return pickerLabel!
    }
    
    fileprivate func setLowHighBidRange(price: Double) {
        
        guard let fareValues = self.bidDetail?.fares else { return }
        
        if price <= fareValues.fairPriceLow {
            lblBidStatus.text = "Your Bid Looks Low"
        }
        else if price >= fareValues.fairPriceHigh {
            lblBidStatus.text = "Your Bid Looks High"
        }
        else{
            lblBidStatus.text = "Your Bid Looks Good"
        }
    }
}
