//
//  RiderHome+DrawRoute.swift
//  Wapanda
//
//  Created by daffomac-31 on 29/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import GoogleMaps
import IQKeyboardManager
import GooglePlaces
import AFNetworking

//MARK: Draw route on map
extension RiderHomeViewController: GoogleDirectionDetailViewDelegate{
    
    func googleDirectionSettingOnLoad(){
        self.presenterGoogleDirection = GoogleDirectionDetailViewPresenter(delegate: self)
    }
    
    func getDirection(){
        if ReachabilityManager.shared.isNetworkAvailable
        {
            self.presenterGoogleDirection.sendGoogleDirectionDetailRequest(withGoogleDirectionDetailViewRequestModel: GoogleDirectionDetailViewRequestModel(source: self.selectedSourceAddress!.coordinate, destination: self.selectedDestinationAddress!.coordinate, wayPoint: nil))
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
        }
        
    }
    
    //MARK: Direction View Delegate
    func directionDetailWithData(data: GoogleDirectionResponse){
        self.prepareScreenToDrawRoute()
        
        if let routes = data.routes, routes.count > 0{
            self.routeAavailableWithData(data: data)
        }
        else{
            //No Route Available Settings
            self.noRouteAvailableSettings()
        }
    }
    
    func routeAavailableWithData(data: GoogleDirectionResponse){
        
        self.viewMapGoogle.drawRoute(withOverViewPolyline: data.routes!.first!.overviewPolyline!.points!)
//        let path = GMSMutablePath()
//        for legs in (data.routes?.first?.legs!)!
//        {
//            let latitude: CLLocationDegrees? = Double((legs.startLocation?.lat)!)
//            let longitude: CLLocationDegrees? = Double((legs.startLocation?.lng)!)
//            path.addLatitude(latitude!, longitude: longitude!)
//        }
//        var encodedPath: String = path.encodedPath()
        self.addMarkersForSourceAndDestination(withSource: CLLocationCoordinate2D(latitude: Double(data.routes!.first!.legs!.first!.startLocation!.lat!), longitude: Double(data.routes!.first!.legs!.first!.startLocation!.lng!)), withDestination: CLLocationCoordinate2D(latitude: Double(data.routes!.first!.legs!.first!.endLocation!.lat!), longitude: Double(data.routes!.first!.legs!.first!.endLocation!.lng!)))
    }
    
    func moveToCabSelectionScreen(){
        
        let dataModel = CabSelectionViewRequestModel(latFrom: self.selectedSourceAddress!.coordinate.latitude, lngFrom: self.selectedSourceAddress!.coordinate.longitude, latTo: self.selectedDestinationAddress!.coordinate.latitude, lngTo: self.selectedDestinationAddress!.coordinate.longitude, sourceAddress: self.selectedSourceAddress!.title, destinationAddress: self.selectedDestinationAddress!.title, cabList: nil, sortingOrder: nil)
        
        self.performCabSelection(data: dataModel)

    }
    
    func noRouteAvailableSettings(){
        AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_ROUTE_AVAILABLE_MESSAGE)
    }
    
    
    func prepareScreenToDrawRoute(){
        self.viewMapGoogle.clear()
        self.removePickSourcePinFromScreen()
    }
    
    func addMarkersForSourceAndDestination(withSource source: CLLocationCoordinate2D, withDestination destination: CLLocationCoordinate2D){
        
        let _ = self.addCustomInfoViewMarker(withCoordinates: source, title: self.selectedSourceAddress!.title)
        
        
        let _ = self.addCustomInfoViewMarker(withCoordinates: destination, title: self.selectedDestinationAddress!.title!)
    }
    
    func addCustomInfoViewMarker(withCoordinates coordinate: CLLocationCoordinate2D, title: String)->RouteAddressInfoWindow{
       
        //Prepare info window view
        let infoWindowDestination = self.getInfoNibForRouteAddress()
        infoWindowDestination.bind(withLocation: title)
        
        //Show info view window
        return self.viewMapGoogle.addCustomInfoViewMarker(withCoordinates: coordinate, infoWindowView: infoWindowDestination) as! RouteAddressInfoWindow
    }
    
    func getInfoNibForRouteAddress()->RouteAddressInfoWindow{
        let nib = Bundle.main.loadNibNamed("RouteAddressInfoWindow", owner: self, options: nil)?.first as! RouteAddressInfoWindow
        return nib
    }
}

