//
//  RideScheduleTimeViewDelegate.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 12/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

//Notes:- This protocol is used as a interface which is used by ScheduleRideTimeViewPresenter to tranfer info to ScheduleRideTimeViewController

protocol ScheduleRideTimeViewDelegate: BaseViewProtocol{
    
    func rideScheduleTimeSubmittedSuccessfully(withResponseModel scheduleRideTimeResponseModel:ScheduleRideTimeResponseModel)
}
