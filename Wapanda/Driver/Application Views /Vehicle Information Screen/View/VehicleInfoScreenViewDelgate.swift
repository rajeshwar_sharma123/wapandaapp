//
//  VehicleInfoScreenViewDelgate.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

//Notes:- This protocol is used as a interface which is used by VehicleInfoScreenViewPresenter to tranfer info to VehicleInfoScreenViewController

protocol VehicleInfoScreenViewDelgate: BaseViewProtocol{
    func vehicleInfoSubmittedSuccessfully(withResponseModel vehicleInfoResponseModel:VehicleDoc)
    func allVehicleTypesFetchedSuccessfully(withResponseModel allVehicleResponseModel:AllVehicleTypeResponseModel)
    func makesFetchedSuccessfully(withResponseModel makesResponseModel:MakesResponseModel)
    func yearsFetchedSuccessfully(withResponseModel yearsResponseModel:YearResponseModel)
    func modelsFetchedSuccessfully(withResponseModel modelResponseModel:ModelsResponseModel)

    func showErrorMessage(withMessage message: String,forTextFields: VehicleInfoInput)
}

