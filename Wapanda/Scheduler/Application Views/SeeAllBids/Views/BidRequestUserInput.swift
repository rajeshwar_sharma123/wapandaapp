import UIKit

struct BidRequestUserInput {
    
    var latitude: Double?
    var longitude: Double?
    var pickupTime: Double?
    var fare: Float?
    var sortKey = "riderSelectedFare"
    var sortOrder = -1
    var pickupTimeUTC: String?
    var addressTitle: String?
}
