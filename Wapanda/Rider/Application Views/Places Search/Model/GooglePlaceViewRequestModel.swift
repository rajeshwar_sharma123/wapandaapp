
//Notes:- This model is used as a model that holds signup properties from signup view controller.
import GooglePlaces

struct GooglePlaceViewRequestModel {
    var searchString                   : String!
    var userLocationCoordinate: CLLocationCoordinate2D?
}
