//
//  TipViewRequestModel.swift
//  Wapanda
//
//  Created by daffomac-31 on 13/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

struct TipViewRequestModel {
    var driverName: String!  = ""
    var driverImageId: String = ""
    var tripId: String!
    var tripFare: Double!
    var routeImageId: String  = ""
    var pushObjModel : PushNotificationObjectModel!
}
