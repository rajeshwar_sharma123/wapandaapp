//
//  NewRideRequestViewDelegate.swift
//  Wapanda
//
//  Created by Daffolapmac on 19/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

protocol NewRideScreenViewDelgate: BaseViewProtocol {
    func didChangeRidePrice(_ counterPrice : Double)
    func newRideSubmittedSuccessful(withResponseModel:NewRideResponseModel)
}
