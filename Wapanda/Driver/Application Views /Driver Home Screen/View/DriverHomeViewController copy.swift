

import UIKit
import GoogleMaps
import IQKeyboardManager
import GooglePlaces
import AFNetworking

class DriverHomeViewController: BaseViewController {
    
    @IBOutlet weak var viewMapGoogle: GMSMapView!
    @IBOutlet weak var viewDestinationSearch: UIView!
    @IBOutlet weak var lblAreabasePrice: UILabel!
    @IBOutlet weak var lastVisitedWidthLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnTip: UIButton!
    @IBOutlet weak var viewTip: UIView!
    @IBOutlet weak var lblDriverPrice: UILabel!
    @IBOutlet weak var viewSliderBackground: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var lblAvailability: UILabel!
    @IBOutlet weak var viewIndicator: UIView!
    @IBOutlet weak var viewDriverAvailability: UIView!
    
    var priceSlider: JMMarkSlider!
    var currentLocationMarker: GMSMarker?
    var pickLocationPin: UIImageView?
   // var objUserProfile : LoginResponseModel!
    var objDriverHomePresenter : DriverHomeViewPresenter!
    var timerForBasePrice : Timer!
    var driverBasePrice :Float!
    var onGoingPrice :Float = 0.0
    var previousRatio :Float!
    var driverLocation :CLLocationCoordinate2D!
    var isFirstTime = true
    var shouldInitialiseDriverPrice = true
    var shouldReload = true
    var shouldShouldShowVerifyScreeen = false
    var timerEditingChanged: Timer? = nil
    var shouldShowLoader = true
    
    //Source Window
    var sourceInfoWindow: PickSourceInfoWindow!
    var userCurrentLocationCoordinate: CLLocationCoordinate2D?
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.customizeMapAppearance()
        self.customizeNavigationBar()
        
       
        //Source Info Window
        sourceInfoWindow = self.getInfoNibForAddress()
        
        //Add Cab Location Observer
        NotificationCenter.default.addObserver(self, selector: #selector(updateCabLocation(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.UPDATE_CAB_LOCATION), object: nil)
        
        if shouldShouldShowVerifyScreeen
        {
            let unverified = UIViewController.getViewController(VerifyInProcessViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
            self.present(unverified, animated: true, completion: nil)
            self.shouldShouldShowVerifyScreeen = false
            
        }
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       // self.objUserProfile = AppDelegate.sharedInstance.userInformation
      
         self.customizeNavigationBar()
         self.objDriverHomePresenter = DriverHomeViewPresenter(delegate: self)
        
        if AppUtility.isUserLogin(){
            self.objDriverHomePresenter.sendLaunchDataRequest()
            self.initilaseAvailabilityView()
        }
        
        SocketManager.shared.riderSocket.disconnect()
        
        //Check for location permissions
        LocationServiceManager.sharedInstance.viewDelegate = self
        LocationServiceManager.sharedInstance.checkForUserPermissions()
        
        //Socket Connect
        SocketManager.shared.connectToDriverSocket()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if let _ = timerForBasePrice
        {
            self.timerForBasePrice.invalidate()
        }
        self.timerForBasePrice = nil
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if self.shouldReload
        {
            if let _ =  self.priceSlider
            {
                self.priceSlider.removeFromSuperview()
            }
            self.priceSlider = JMMarkSlider(frame: CGRect(x: 0, y: 0, width: self.viewSliderBackground.frame.width, height: self.viewSliderBackground.frame.height))
            if AppUtility.isUserLogin()
            {
                if let driverOnGoingRate = AppDelegate.sharedInstance.userInformation.driverProfile?.ongoingRate
                {
                    let ratio = AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0
                    
                    self.onGoingPrice = driverOnGoingRate
                    self.driverBasePrice = ratio * self.onGoingPrice
                    self.previousRatio = ratio
                    self.lblAreabasePrice.text = String(format:"$%.2f",self.onGoingPrice)
                    self.initialiseSliderView(withAreaBase: self.onGoingPrice,withRatio: ratio)
                }
                else
                {
                    self.driverBasePrice = 0.0
                    self.onGoingPrice = 0.0
                    self.previousRatio = 1
                }
                self.shouldShowLoader = true
                self.getCurrentAreaBasePrice()
                self.startTimerToGetAreaBase()
            }
            else
            {
                self.viewSliderBackground.addSubview(self.priceSlider)
            }
            
            self.driverLocation = self.currentLocationMarker?.position
            
            self.addPinOnMapCenter()
            self.shouldReload = true
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helper Methods
    func initialiseSliderView(withAreaBase areaBasePrice:Float,withRatio ratio:Float){
        self.priceSlider.markColors = [UIColor.gettCabsMarkerColor(),UIColor.liftCabsMarkerColor(),UIColor.uberCabsMarkerColor()]
        self.priceSlider.markPositions = []
        self.priceSlider.markTitles = []
        self.priceSlider.markWidth = 1.0
        self.priceSlider.selectedBarColor = UIColor.sliderColor()
        self.priceSlider.unselectedBarColor = UIColor.sliderColor()
        self.priceSlider.value = (AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0)/2
        self.priceSlider.addTarget(self, action:#selector(sliderChanged(_:)), for: .valueChanged)
        self.priceSlider.addTarget(self, action:#selector(sliderChangedEnd(_:)), for: .touchUpInside)
        self.viewSliderBackground.addSubview(self.priceSlider)
    }
    func initilaseAvailabilityView(){
        self.viewShadow.addShadow()
        
        if AppUtility.isUserLogin()
        {
            guard let _ = AppDelegate.sharedInstance.userInformation.driverProfile?.available else
            {
                //self.viewDriverAvailability.backgroundColor = UIColor.appThemeColor()
                self.lblAvailability.backgroundColor = UIColor.white
                self.lblAvailability.textColor = UIColor.black
                self.lblAvailability.text = AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE
                return
            }
            
           // self.viewDriverAvailability.backgroundColor = (AppDelegate.sharedInstance.userInformation.driverProfile?.available)! ? UIColor.white : UIColor.appThemeColor()
            self.lblAvailability.backgroundColor = (AppDelegate.sharedInstance.userInformation.driverProfile?.available)! ? UIColor.appThemeColor() : UIColor.white
            self.lblAvailability.textColor = (AppDelegate.sharedInstance.userInformation.driverProfile?.available)! ? UIColor.white : UIColor.black
            self.lblAvailability.text = (AppDelegate.sharedInstance.userInformation.driverProfile?.available)! ? AppConstants.ScreenSpecificConstant.DriverHomeScreen.OFFLINE_TITLE : AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE
        }
        else
        {
            self.viewDriverAvailability.backgroundColor = UIColor.appThemeColor()
            self.viewIndicator.backgroundColor = UIColor.white
            self.lblAvailability.textColor = UIColor.white
            self.lblAvailability.text = AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE
        }
    }
    func validateIfUserVerified()->Bool{
        guard let _ = AppDelegate.sharedInstance.userInformation.driverProfile?.available else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
            return false
        }
        guard let _ = AppDelegate.sharedInstance.userInformation.driverProfile?.approved else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
            return false
        }
        guard (AppDelegate.sharedInstance.userInformation.driverProfile?.approved)! else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
            return false
        }
        return true
        
    }
    
    //MARK:- Google Maps Customization Methods
    func customizeMapAppearance(){
        
        viewMapGoogle.isMyLocationEnabled = false
        viewMapGoogle.setMinZoom(1, maxZoom: 18)
        viewMapGoogle.delegate = self
        viewMapGoogle.isBuildingsEnabled = false
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "Style", withExtension: "json") {
                viewMapGoogle?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
    }
    func customizeNavigationBar(){
        self.hideNavigationBar()
    }
    func updateCameraLocation(lat: Double, long: Double,zoom:Double){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15.0)
        viewMapGoogle.animate(to: camera)
    }
    func updateCurrentLocationOnMap(lat: Double, long: Double){
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if let marker = self.currentLocationMarker{
            self.currentLocationMarker = self.updateMarkerAtPosition(withPoints: CLLocationCoordinate2D(latitude: lat, longitude: long), marker: marker, bearing: 0.0)
            self.showSourcePickerPinWithCoordinates(coordinate: coordinate)
        }
        else{
            self.currentLocationMarker = self.addMarkerWithCoordinates(coordinate: coordinate, markerImage: #imageLiteral(resourceName: "ic_current_location"))
        }
    }
    @objc func updateCabLocation(_ notication:Notification){
    }
    
    // Bound all markers of map to fit view
    func boundCameraToFitAllMarkers(){
        var bounds = GMSCoordinateBounds()
        if let _ = self.currentLocationMarker
        {
            bounds = bounds.includingCoordinate((self.currentLocationMarker?.position)!)
        }
        viewMapGoogle.animate(with: GMSCameraUpdate.fit(bounds,withPadding: ScreenSize.size.height * 0.1))
    }
    private func startTimerToGetAreaBase(){
        
        if let _ = timerForBasePrice
        {
            timerForBasePrice.invalidate()
            timerForBasePrice = nil
        }
        timerForBasePrice = Timer.scheduledTimer(timeInterval: TimeInterval(AppConstants.ScreenSpecificConstant.DriverHomeScreen.TIME_TO_FETCH_AREA_PRICE), target: self, selector: #selector(getCurrentAreaBasePrice), userInfo: nil, repeats: true)
    }
    func getCurrentAreaBasePrice(){
        if let _ = self.driverLocation
        {
            self.objDriverHomePresenter.sendAreabasePriceFetchRequest(withAreaViewRequestModel: AreaBasePriceViewRequestModel(latitude: self.driverLocation.latitude, longitude: self.driverLocation.longitude), withLoader: self.shouldShowLoader)
            self.shouldShowLoader = false
        }
    }
    
    //MARK: IBACTION
    @IBAction func btnMyCurrentLocationClick(_ sender: Any) {
        if let currentLocation = self.userCurrentLocationCoordinate{
            self.updateCameraLocation(lat: currentLocation.latitude, long: currentLocation.longitude, zoom: 15.0)
        }
    }
    
    //MARK:- Slider Action Methods
    @IBAction func tipbuttonTapped(_ sender: Any) {
        self.viewTip.isHidden = !self.viewTip.isHidden
        self.btnTip.setImage(self.viewTip.isHidden ? #imageLiteral(resourceName: "toolTip") : #imageLiteral(resourceName: "close"), for: .normal)
    }
    @IBAction func earningsButtonTapped(_ sender: Any) {
        let earningsVC = UIViewController.getViewController(EarningsViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        self.navigationController?.pushViewController(earningsVC, animated: true)
        
    }
    @IBAction func refreshButtonTapped(_ sender: Any) {
         self.shouldShowLoader = true
        self.getCurrentAreaBasePrice()
    }
    @IBAction func sliderChangedEnd(_ sender : JMMarkSlider){
        if AppUtility.isUserLogin()
        {
            if let _ = self.driverLocation
            {
                if let isAvailable = AppDelegate.sharedInstance.userInformation.driverProfile?.available
                {
                    self.previousRatio = AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0
                    self.objDriverHomePresenter.sendDriverUpdatePriceRequest(withDriverHomeViewRequestModel: DriverHomeViewRequestModel(available: isAvailable, ratio: self.priceSlider.value*2.0, ongoingRate: self.driverBasePrice, lat: Float(self.driverLocation.latitude), lng: Float(self.driverLocation.longitude)))
                }
                else
                {
                    self.previousRatio = AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0
                    self.objDriverHomePresenter.sendDriverUpdatePriceRequest(withDriverHomeViewRequestModel: DriverHomeViewRequestModel(available: false, ratio: self.priceSlider.value*2.0, ongoingRate: self.driverBasePrice, lat: Float(self.driverLocation.latitude), lng: Float(self.driverLocation.longitude)))
                }
            }
            else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ScreenSpecificConstant.DriverHomeScreen.WAIT_FOR_LOCATTION_MESSAGE)
            }
        }
        else
        {
            
            self.logOutUser()
        }
    }
    @IBAction func sliderChanged(_ sender : JMMarkSlider){
        let sliderValue = sender.value
        let ratio =  sliderValue * 2.0
        self.driverBasePrice = ratio * self.onGoingPrice
        self.lblDriverPrice.text = self.priceSlider.setDriverText(self.driverBasePrice)
    }
    @IBAction func availabilityViewTapped(_ sender: Any) {
        
        if AppUtility.isUserLogin()
        {
            if (AppDelegate.sharedInstance.userInformation.driverProfile?.available)!
            {
                self.objDriverHomePresenter.sendDriverOfflineRequest(withDriverHomeViewRequestModel: DriverHomeViewRequestModel(available: false, ratio: self.priceSlider.value*2.0, ongoingRate: self.driverBasePrice, lat: Float(self.driverLocation.latitude), lng: Float(self.driverLocation.longitude)))
            }
            else
            {
                self.objDriverHomePresenter.sendDriverOnlineRequest(withDriverHomeViewRequestModel: DriverHomeViewRequestModel(available: true, ratio: self.priceSlider.value*2.0, ongoingRate: self.driverBasePrice, lat: Float(self.driverLocation.latitude), lng: Float(self.driverLocation.longitude)))
            }
        }
        else
        {
            self.logOutUser()
        }
    }
    
    
    //MARK: Navigation Selector
    
    @IBAction func moreButtonTapped(_ sender:Any) {
        
        super.menuButtonClick()
//        self.showApplePayScreen()
    }
    @IBAction func seeAllBidsButtonTapped(_ sender: Any) {
      //  self.showDriverInvoiceScreen()
    }
    
    func showApplePayScreen(){
        let tipVC = UIViewController.getViewController(ApplePayTestViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        self.present(tipVC, animated: false, completion: nil)
    }
    
    func showDriverInvoiceScreen(){
        let tipVC = UIViewController.getViewController(DriverInvoiceViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        self.present(tipVC, animated: false, completion: nil)
    }
    
    //MARK: View Delegates
    
    override func showLoader(_ VC: AnyObject?) {
        super.showLoader(self)
    }
    override func hideLoader(_ VC: AnyObject?) {
        self.hideLoader(self)
    }
    override func showErrorAlert(_ alertTitle: String, alertMessage: String, VC: AnyObject?) {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}

//MARK: Screen Navigation Login Goes Here ..
extension DriverHomeViewController{
    func navigateToUserWelcomeScreen(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func showDocumentVerifiedScreen(){
        let documentVerifiedVC = UIViewController.getViewController(DriverVerifiedViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        documentVerifiedVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(documentVerifiedVC, animated: true, completion: nil)
    }
}

extension DriverHomeViewController: WelcomeScreenViewDelegate{
    
    func showControllerWithVC(controller: UIViewController){
        self.present(controller, animated: true, completion: nil)
    }
    func locationUpdated(lat: Double, long: Double){
        
        self.userCurrentLocationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if isFirstTime
        {
            self.updateCameraLocation(lat: lat, long: long,zoom: 15.0)
            self.updateCurrentLocationOnMap(lat: lat, long: long)
            isFirstTime = false
        }
        else
        {
            self.updateCurrentLocationOnMap(lat: lat, long: long)
        }
        if AppUtility.isUserLogin()
        {
            guard let _ = AppDelegate.sharedInstance.userInformation.driverProfile?.available else
            {
                return
            }
            guard let _ = AppDelegate.sharedInstance.userInformation.driverProfile?.approved else
            {
                return
            }
            guard (AppDelegate.sharedInstance.userInformation.driverProfile?.approved)! else
            {
                return
            }
            if SocketManager.shared.driverSocket.status == .connected
            {
                if (AppDelegate.sharedInstance.userInformation.driverProfile?.available)!
                {
                    SocketManager.shared.updateDriverLocation(CLLocation(latitude: lat, longitude: long))
                }
            }
            else if SocketManager.shared.driverSocket.status != .connecting

            {
                SocketManager.shared.connectToDriverSocket()
            }
        }
    }
}

//MARK: Pick source pin
extension DriverHomeViewController{
    func addPinOnMapCenter(){
        pickLocationPin = UIImageView(image: #imageLiteral(resourceName: "ic_pin"))
        pickLocationPin?.center = CGPoint(x: self.viewMapGoogle.center.x, y: self.viewMapGoogle.center.y-28)
        self.viewMapGoogle.addSubview(pickLocationPin!)
        self.viewMapGoogle.bringSubview(toFront: self.pickLocationPin!)
    }
    func addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D, markerImage image: UIImage)->GMSMarker{
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude:coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.icon = image
        marker.map = viewMapGoogle
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        return marker
    }
    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker,bearing:Double)->GMSMarker{
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(AppConstants.ScreenSpecificConstant.RiderHomeScreen.CAB_ANIMATION_DURATION)
        marker.position = coordinate
        marker.rotation = bearing
        CATransaction.commit()
        return marker
    }
}

//MARK: Map View Delegate
extension DriverHomeViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.sourceInfoWindow.removeFromSuperview()
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        
        self.showSourceInfoViewForLocationPin()
        
    }
    func showSourceInfoViewForLocationPin(){
        if let point = self.pickLocationPin?.center{
            self.addSourceInfoWindow(withPositionPoints: point)
            let location = self.viewMapGoogle.projection.coordinate(for: point)
            self.driverLocation = location
            if AppUtility.isUserLogin()
            {
                self.getCurrentAreaBasePrice()
            }
            self.showSourcePickerPinWithCoordinates(coordinate: location)
        }
    }
    func getInfoNibForAddress()->PickSourceInfoWindow{
        let nib = Bundle.main.loadNibNamed("PickSourceInfoWindow", owner: self, options: nil)?.first as! PickSourceInfoWindow
        return nib
    }
    func addSourceInfoWindow(withPositionPoints point: CGPoint){
        self.sourceInfoWindow.center = CGPoint(x: (self.pickLocationPin?.center.x)!, y: (self.pickLocationPin?.center.y)!-(self.pickLocationPin?.frame.size.height)!-12)
        self.viewMapGoogle.addSubview(self.sourceInfoWindow)
        self.sourceInfoWindow.fetchingAddress()
    }
    func showSourcePickerPinWithCoordinates(coordinate: CLLocationCoordinate2D){
        
        if ReachabilityManager.shared.isNetworkAvailable
        {
            GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (response: GMSReverseGeocodeResponse?, error: Error?) in
                
                self.addSourceAddressInfoWindow(withResponseData: response)
                
            })
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
        }
        
    }
    func addSourceAddressInfoWindow(withResponseData data: GMSReverseGeocodeResponse?){
        guard self.pickLocationPin != nil else{return}
        
        self.sourceInfoWindow.center = CGPoint(x: (self.pickLocationPin?.center.x)!, y: (self.pickLocationPin?.center.y)!-(self.pickLocationPin?.frame.size.height)!-12)
        let titlePickedSource = self.getFormattedSourceLocationAddressTitle(WithGeocodingResponse: data)
        self.sourceInfoWindow.bind(withLocation: titlePickedSource)
        self.viewMapGoogle.addSubview(self.sourceInfoWindow)
    }
    func getFormattedSourceLocationAddressTitle(WithGeocodingResponse response: GMSReverseGeocodeResponse?)->String{
        var strSourceAddress = ""
        
        if var firstResult = response?.firstResult()?.lines{
            
            //Remove Empty Strings from result
            for (index, value) in firstResult.enumerated(){
                if value.isEmpty{
                    firstResult.remove(at: index)
                }
            }
            
            strSourceAddress = firstResult.joined(separator: ", ")
        }
        
        if strSourceAddress.isEmpty{
            strSourceAddress = AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_LOCATION_FOUND
        }
        
        return strSourceAddress
    }
}

extension DriverHomeViewController: DriverHomeViewDelegate
{
    func updateAreaBasePrice(withResponseModel areaResponseModel: AreaPriceResponseModel) {
        
        
        let ratio = AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0
        if let indexArray = areaResponseModel.indexArray,indexArray.count != 0
        {
             self.onGoingPrice = indexArray[0].indexrate ?? 0.0
        }

        self.lblAreabasePrice.text = String(format:"$%.2f",self.onGoingPrice)
        if self.shouldInitialiseDriverPrice
        {
            self.driverBasePrice = ratio * self.onGoingPrice
            self.priceSlider.value = (AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0)/2
            self.lblDriverPrice.text = self.priceSlider.setDriverText(self.driverBasePrice)
            self.shouldInitialiseDriverPrice = false
        }
        self.priceSlider.minimumSliderBaseValue = AppConstants.ScreenSpecificConstant.DriverHomeScreen.MINIMUM_VALUE
        self.initialiseSliderView(withAreaBase: self.onGoingPrice,withRatio: ratio)
        self.hideLoader()
    }
    func driverStatusUpdatedSuccessful(withResponseModel taxResponseModel: LoginResponseModel) {
        AppDelegate.sharedInstance.userInformation.driverProfile?.available = taxResponseModel.driverProfile?.available
        AppDelegate.sharedInstance.userInformation.canRide = taxResponseModel.canRide
        AppDelegate.sharedInstance.userInformation.canDrive = taxResponseModel.canDrive
        AppDelegate.sharedInstance.userInformation.driverProfile?.ongoingRate = taxResponseModel.driverProfile?.ongoingRate
        AppDelegate.sharedInstance.userInformation.driverProfile?.ratio = taxResponseModel.driverProfile?.ratio
        
        if self.previousRatio != AppDelegate.sharedInstance.userInformation.driverProfile?.ratio
        {
            AppUtility.presentToastWithMessage("Your base price updated successfully")
        }
        self.previousRatio = taxResponseModel.driverProfile?.ratio
        self.driverBasePrice = self.onGoingPrice * (AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0)
        self.priceSlider.value = (AppDelegate.sharedInstance.userInformation.driverProfile?.ratio ?? 1.0)/2
        self.lblDriverPrice.text = self.priceSlider.setDriverText(self.driverBasePrice)
       AppDelegate.sharedInstance.userInformation.saveUser()
       
        self.initilaseAvailabilityView()
        if let _ = self.currentLocationMarker
        {
            self.locationUpdated(lat: (self.currentLocationMarker?.position.latitude)!, long: (self.currentLocationMarker?.position.longitude)!)
        }
        // SocketManager.shared.authoriseDriver()
        self.hideLoader()
    }
    
    func driverAPIStripeConnectionSuccessfull(withResponseModel connectResponseModel: DriverStripeAPIConnectionResponseModel) {
        let userInfo = AppDelegate.sharedInstance.userInformation
        userInfo?.stripeProfile?.stripeAccountId = connectResponseModel.id
        userInfo?.stripeProfile?.chargesEnabled = connectResponseModel.chargesEnabled
        AppDelegate.sharedInstance.userInformation = userInfo
        AppDelegate.sharedInstance.userInformation.saveUser()
    }
    func hideLoader() {
        super.hideLoader(self)
    }
    func showLoader() {
        super.showLoader(self)
    }
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        self.shouldReload = false
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    func didReceiveLaunchDetails(withLaunchRequestModel:LaunchResponseModel){
        AppDelegate.sharedInstance.userInformation = withLaunchRequestModel.loginResponseModel
        AppDelegate.sharedInstance.userInformation.saveUser()
        self.initilaseAvailabilityView()
        if let tripDetails = withLaunchRequestModel.trip
        {
            if tripDetails.driver?.id == AppDelegate.sharedInstance.userInformation.id
            {
                let objNewRequestVC = UIViewController.getViewController(DriverTripViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
                objNewRequestVC.pushNotificationModel = PushNotificationObjectModel(JSON: [:])
                objNewRequestVC.pushNotificationModel.trip = tripDetails
                self.navigationController?.pushViewController(objNewRequestVC, animated: true)
            }
        }
    }
}
extension DriverHomeViewController:StripeWebViewDelegate
{
    func didCompleteStripeConnection(withResponseModel stripeConnectRespone: StripeConnectionResponseModel) {
        
        if stripeConnectRespone.error != nil
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: stripeConnectRespone.errorDescription ?? AppConstants.ErrorMessages.SOME_ERROR_OCCURED)
        }
        else
        {
            self.objDriverHomePresenter.sendDriverConnectRequest(withAuthCode: stripeConnectRespone.code!, false)
        }
    }
}

