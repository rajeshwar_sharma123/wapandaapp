
//Notes:- This protocol is used as a interface which is used by AddAddressPresenter to tranfer info to AddAddressViewController

protocol AddAddressViewDelegate:BaseViewProtocol {
    func addAddressSuccessfully(withData user: LoginResponseModel)
}
