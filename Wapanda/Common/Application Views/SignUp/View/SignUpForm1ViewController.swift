/**
 This class handle UI Design for signup form - 1 screen
 */

import UIKit
import CountryPicker

class SignUpForm1ViewController: BaseViewController, CountryPickerDelegate, TextFieldValidationDelegate, UITextFieldDelegate, BaseViewProtocol, SignUpViewDelegate{

    
    //Form view outlets
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var lblErrorEmail: UILabel!
    @IBOutlet weak var lblErrorNumber: UILabel!
    @IBOutlet weak var lblErrorPassword: UILabel!
    @IBOutlet weak var btnSecurePassword: UIButton!
    
    //Country picker view outlets
    @IBOutlet weak var pickerCountryCode: CountryPicker!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgViewDropdown: UIImageView!
    
    //Screen global variables
    fileprivate var isPasswordEncyptionEnabled = true
    fileprivate var signUpPresenter: SignUpViewPresenter!
    fileprivate var signUpViewRequestModel = SignUpViewRequestModel()

    //MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        //Setup Country Picker
        setupInitialUI()
        setupCountryPicker()
        addKeyboardActivityListener()
        self.signUpPresenter    = SignUpViewPresenter(delegate: self as SignUpViewDelegate,textFieldValidationDelegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Initial View Methods
    
    /**
     This function setup the Initial UI elements
     - returns :void
     */
    private func setupInitialUI() ->Void{
        self.setupNavigationBar()
        self.customizeTextFields()
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font]
        self.txtFieldEmail.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.EMAIL_PLACEHOLDER_TEXT,
                                                                      attributes:attributes)
        self.txtFieldPhone.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.PHONE_PLACEHOLDER_TEXT,
                                                                      attributes:attributes)
        self.txtFieldPassword.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.PASSWORD_PLACEHOLDER_TEXT,
                                                                         attributes:attributes)
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    
    private func setupNavigationBar() ->Void{
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.SingUpScreen.FORM1_NAVIGATION_TITLE, withFontSize: 14, andColor: UIColor.clear)
        self.customizeNavigationBackButton()
    }
    
    //MARK: Keyboard handling methods
    
    /**
     This function adds listener for keyboard
     */
    private func addKeyboardActivityListener(){
        let notifCenter = NotificationCenter.default
        notifCenter.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
    }
    
    /**
     This function handle screen on keyboard show event
     */
    func keyboardWillShow(){
        
        //Hide country code picker if open
        self.pickerCountryCode.isHidden = true
    }
    
    /**
     This function hide keyboard from screen
     */
    func hideKeybaord(){
        self.view.endEditing(true)
    }
    
    //MARK: Base View Delegate
    func hideLoader() {
        
    }
    
    func showLoader() {
        
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        
    }
    
    //MARK: UIText Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        switch textField
        {
            case self.txtFieldEmail:
                self.lblErrorEmail.text = ""
            case self.txtFieldPhone:
                self.lblErrorNumber.text = ""
            case self.txtFieldPassword:
                self.lblErrorPassword.text = ""
            default:
                break
        }
        
        return true
    }
    
    //MARK: Text Field Validation Delelgate
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType)
    {
        switch forTextFields
        {
            case TextFieldsType.EmailAddress:
                self.showValidationMessageForEmailWithMessage(msg: message)
            case TextFieldsType.PhoneNumber:
                self.showValidationMessageForPhoneWithMessage(msg: message)
            case TextFieldsType.Password:
                self.showValidationMessageForPasswordWithMessage(msg: message)
            case TextFieldsType.All:
                self.showEmptyFieldValidationForAllFields()
            default: break
        }
    }
    
    //Validation Helper Methods
    private func showValidationMessageForEmailWithMessage(msg: String){
        self.lblErrorEmail.text = msg
    }
    
    private func showValidationMessageForPasswordWithMessage(msg: String){
        self.lblErrorPassword.text = msg
    }
    
    private func showValidationMessageForPhoneWithMessage(msg: String){
        self.lblErrorNumber.text = msg
    }
    
    private func showEmptyFieldValidationForAllFields(){
        self.showValidationMessageForPhoneWithMessage(msg: AppConstants.ValidationErrors.ENTER_PHONE_NUMBER)
        self.showValidationMessageForPasswordWithMessage(msg: AppConstants.ValidationErrors.ENTER_PASSWORD)
    }

    //MARK: Country Code Picker
    
    /**
     This function setup country code picker
     */
    private func setupCountryPicker(){
        
        //get corrent country
        let code = Locale.countryCode()
        //init Picker
        pickerCountryCode.countryPickerDelegate = self
        pickerCountryCode.showPhoneNumbers = true
        pickerCountryCode.setCountry(code)
    }
    
    /**
     This function is used to set the country phone code for selected country
     
     - parameter code: Country Phone code
     - parameter image: Country Flag Image
     
     */
    func setSelectedCountryWithPhoneCode(code: String, image: UIImage, countryCode: String){
        
        //Set Data on the screen
        self.lblCountryCode.text = code
        self.imgCountryFlag.image = image
        
        //Set data in model
        self.signUpViewRequestModel.countryCode = countryCode
        
        self.signUpViewRequestModel.countryCodeExt = AppUtility.getFormattedDiallingCodeFromCode(code: code)
    }
    
    /**
     This function is delegate for the picker selection
     */
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage){
        
        //Set Selected Country Code and Image
        setSelectedCountryWithPhoneCode(code: phoneCode, image: flag, countryCode: countryCode)
    }
    
    
    //MARK: Button Actions
    
    /**
     This function is action listener for the show country
     
     - parameter sender: Show country button object

     */
    @IBAction func btnShowCountryPicker(_ sender: UIButton) {
        hideKeybaord()
        setPickerViewAppearance()
    }
    
    private func setPickerViewAppearance(){
        
        if pickerCountryCode.isHidden{
            pickerCountryCode.isHidden = false
        }
        else{
            pickerCountryCode.isHidden = true
        }
    }
    
    /**
     This function shows country code picker pn screen
     
     - parameter sender: Show or hide password button object reference

     */
    @IBAction func btnSecurePasswordClick() {
        if isPasswordEncyptionEnabled{
            disableSecurePassword()
        }
        else{
            enableSecurePassword()
        }
    }
    
    /**
     These functions work as helper for enabling secure password
     */
    private func enableSecurePassword(){
        self.isPasswordEncyptionEnabled = true
        self.txtFieldPassword.isSecureTextEntry = true
        self.btnSecurePassword.setImage(#imageLiteral(resourceName: "ic_visibility_off_white"), for: .normal)
    }
    
    /**
     These functions work as helper for disabling secure password
     */
    private func disableSecurePassword(){
        self.isPasswordEncyptionEnabled = false
        self.txtFieldPassword.isSecureTextEntry = false
        self.btnSecurePassword.setImage(#imageLiteral(resourceName: "ic_visibility_white"), for: .normal)
    }
    
    /**
     This function work as action handler for navigation on next screen
     
     - parameter sender: Next button object reference
     
     */
    @IBAction func btnNextClick(_ sender: UIButton) {
        self.signUpViewRequestModel = self.createSignUpViewRequestModelWithTextFieldInput()
        
        let validationPassed = self.signUpPresenter.validateAllSignUpForm1Inputs(withData: self.signUpViewRequestModel)
        
        if validationPassed{
            
            //Move To Next Screen
            navigateToSignUpForm2Screen()
        }
    }
    
    /**
     This function work as action handler for back button click
     
     */
    override func backButtonClick() {
        super.backButtonClick()
    }
    
    /**
     This method is used to get the SignUpForm1ViewRequestModel from the current input in text fields for email signup
     
     - returns : SignUpForm1ViewRequestModel with current data in textFields
     */
    func createSignUpViewRequestModelWithTextFieldInput() -> SignUpViewRequestModel {
        
        signUpViewRequestModel.email = self.txtFieldEmail.text?.getWhitespaceTrimmedString()
        signUpViewRequestModel.phoneNo = self.txtFieldPhone.text?.getPhoneNumberFormat()
        signUpViewRequestModel.password = self.txtFieldPassword.text?.getWhitespaceTrimmedString()


        return signUpViewRequestModel
    }
    
    //MARK: Signup Delegate
    func signUpSuccessful(withResponseModel: LoginResponseModel) {
    }
    
    //MARK: Navigation
    
    /**
     This function used to navigate to form screen 2 of signup process
     
     */
    func navigateToSignUpForm2Screen(){
        let signUpForm2VC = UIViewController.getViewController(viewController: SignUpForm2ViewController.self)
        signUpForm2VC.initializeViewWithSignUpRequestModel(model: self.signUpViewRequestModel)
        self.navigationController?.pushViewController(signUpForm2VC, animated: true)
    }
}
