 

import UIKit
import LGSideMenuController
import UserNotifications
import Stripe
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    static let sharedInstance = UIApplication.shared.delegate as! AppDelegate
    var userInformation: LoginResponseModel!
    var kMainViewController : LGSideMenuController?
    var isActivatedFromBackground = false

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        return true
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Start monitoring netowork for any changes
        ReachabilityManager.shared.startMonitoring()
        
        //Setup for hockey app
        AppLaunchSetup.shareInstance.setupHockeySDK()
        
        //Setup for stripe
        AppLaunchSetup.shareInstance.setupStripeSDK()
        
        //Setup for IQKeyabord
        AppLaunchSetup.shareInstance.setupIQKeyboard()
        
        //Setup for Google Map
        AppLaunchSetup.shareInstance.setupGoogleMap()
        
        //Setup Inital View For App
        AppInitialViewHandler.sharedInstance.setupInitialViewController()
        
        
        //Setup APNS Setting
        APNSManager.sharedInstance.registerForPushNotification()
        APNSManager.sharedInstance.handleNotificationPayloadOnLaunch(withLaunchOptions: launchOptions)
        self.isActivatedFromBackground = false
      
        return true
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        self.isActivatedFromBackground = true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.APP_BECOME_ACTIVE_NOTIFICATION), object: nil)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        
        APNSManager.sharedInstance.onRegistrationSuccess(withDeviceToken: token)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        APNSManager.sharedInstance.onRecievingPushNotification(withPayload: userInfo as NSDictionary)
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let stripeHandled = Stripe.handleURLCallback(with: url)
        if (stripeHandled) {
            return true
        } else {
          
        }
        return false
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                let stripeHandled = Stripe.handleURLCallback(with: url)
                if (stripeHandled) {
                    
                    return true
                } else {
                   
                }
            }
        }
        return false
    }

}

