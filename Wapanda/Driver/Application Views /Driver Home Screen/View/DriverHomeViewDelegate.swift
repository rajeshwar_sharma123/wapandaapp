//
//  DriverHomeViewDelegate.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/30/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
protocol DriverHomeViewDelegate: BaseViewProtocol{
    
    func driverStatusUpdatedSuccessful(withResponseModel taxResponseModel:LoginResponseModel)
    func updateAreaBasePrice(withResponseModel areaResponseModel:AreaPriceResponseModel)
    func didReceiveLaunchDetails(withLaunchRequestModel:LaunchResponseModel)
    func driverAPIStripeConnectionSuccessfull(withResponseModel:DriverStripeAPIConnectionResponseModel)
}

protocol DriverBidViewDelegate{
    func driverDidAcceptedRide(_ notification:PushNotificationObjectModel)
    func driverDidCounterRide(_ notification:PushNotificationObjectModel)
    
    func riderDidCreateBid(_ notification:PushNotificationObjectModel)
    func riderDidAcceptBid(_ notification:PushNotificationObjectModel)
    func riderDidRejectBid(_ notification:PushNotificationObjectModel)
}
