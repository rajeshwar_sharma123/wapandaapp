//
//  PaymentMethodTableViewCell.swift
//  Wapanda
//
//  Created by daffomac-31 on 02/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(name: String, img: UIImage){
        self.lblTitle.text = name
        self.imgView.image = img
    }
}
