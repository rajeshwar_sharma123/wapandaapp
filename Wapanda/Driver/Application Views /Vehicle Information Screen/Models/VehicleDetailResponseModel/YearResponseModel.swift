//
//  Year.swift
//
//  Created by Daffodil on 22/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class YearResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let years = "Years"
  }

  // MARK: Properties
  public var years: Years?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    years <- map[SerializationKeys.years]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = years { dictionary[SerializationKeys.years] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.years = aDecoder.decodeObject(forKey: SerializationKeys.years) as? Years
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(years, forKey: SerializationKeys.years)
  }

}
