//
//  ApplePayItemModel.swift
//  Wapanda
//
//  Created by daffomac-31 on 30/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

struct ApplePayItemModel{
    var label: String!
    var price: Double!
}
