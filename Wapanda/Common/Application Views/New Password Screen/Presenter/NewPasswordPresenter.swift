
//Notes:- This class is used as presenter for NewPasswordPresenter

import Foundation
import ObjectMapper

class NewPasswordPresenter: ResponseCallback{
    
//MARK:- OTPPopupViewPresenter local properties
    private weak var newPasswordViewDelegate          : NewPasswordViewDelegate?
    private var newPasswordBusinessLogic              : NewPasswordBusinessLogic!
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?
//MARK:- Constructor
    init(delegate responseDelegate:NewPasswordViewDelegate,textFieldValidationDelegate:TextFieldValidationDelegate){
        self.newPasswordViewDelegate = responseDelegate
        self.textFieldValidationDelegate = textFieldValidationDelegate
        self.newPasswordBusinessLogic = NewPasswordBusinessLogic()
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.newPasswordViewDelegate?.hideLoader()
        self.newPasswordViewDelegate?.passwordChangedSuccessfully()
    }
    
    func servicesManagerError(error : ErrorModel){
        self.newPasswordViewDelegate?.hideLoader()
        self.newPasswordViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- API calls
    
    func sendNewPasswordRequest(withData newPasswordRequestModel:NewPasswordScreenRequestModel){
        
        guard validateInput(withData: newPasswordRequestModel) else {
            return
        }

        self.newPasswordViewDelegate?.showLoader()

        let otpPopupRequestModel:NewPasswordRequestModel = NewPasswordRequestModel.Builder()
            .setOTP(newPasswordRequestModel.otp).setNewPassword(newPasswordRequestModel.newPassword)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.newPasswordBusinessLogic.performOTPVerification(withNewPasswordRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData newPasswordRequestModel:NewPasswordScreenRequestModel) -> Bool {
        
        //Password validaiton
        guard !newPasswordRequestModel.newPassword.isEmpty else {
            //password empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_PASSWORD, forTextFields: .Password)
            return false
        }
        guard newPasswordRequestModel.newPassword!.isPasswordValid() else{
            //pass not valid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_PASSWORD, forTextFields: .Password)
            return false
        }
        
        return true
    }
}


