
//Notes:- This class is used as presenter for UpdatedProfileViewPresenter

import Foundation
import ObjectMapper

class UpdatedProfileViewPresenter: ResponseCallback{
    
//MARK:- UpdatedProfileViewPresenter local properties
    
    private weak var updatedProfileViewDelegate          : UpdatedProfileViewDelegate?
    private lazy var updatedProfileBusinessLogic         : UpdatedProfileBusinessLogic = UpdatedProfileBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:UpdatedProfileViewDelegate){
        self.updatedProfileViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.updatedProfileViewDelegate?.hideLoader()
        
        if let responseData = responseObject as? LoginResponseModel{
            self.updatedProfileViewDelegate?.updatedProfileFetchedSuccessfully(WithResponseModel: responseData)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.updatedProfileViewDelegate?.hideLoader()
        self.updatedProfileViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call updatedProfile Api.
    
    func getUpdatedProfileList(){
        
        self.updatedProfileViewDelegate?.showLoader()
        
        var updatedProfileRequestModel : UpdatedProfileRequestModel!
        
        updatedProfileRequestModel = UpdatedProfileRequestModel.Builder()
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).setUserId(AppDelegate.sharedInstance.userInformation.id!).addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.updatedProfileBusinessLogic.performUpdatedProfile(withLoginRequestModel: updatedProfileRequestModel, presenterDelegate: self)

    }
}
