import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak var lblFare: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func bindData(data: SearchListItemModel) {
        if let fare = data.fareAbove {
            lblFare.text = AppUtility.getFormattedPriceString(withPrice: fare)
        }
        else {
           lblFare.text = "-"
        }
        
        if !data.pickupDateTime.isEmpty {
            lblTime.text = AppUtility.getDateFromUTCFormatSearchList(dateStr: data.pickupDateTime, format: "hh: mm a")
        }
        else{
           lblTime.text = "-"
        }
        
        lblName.text = !data.title.isEmpty ? data.title : ""
        lblPlace.text = !data.destAddress.isEmpty ? data.destAddress : "-"
    }
}
