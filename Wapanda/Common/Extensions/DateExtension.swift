//
//  DateExtension.swift
//  Wapanda
//
//  Created by daffomac-31 on 18/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Foundation

extension Date{
    func getUTCFormateDate() -> String {
        let dateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dateString: String = dateFormatter.string(from: self)
        return dateString
    }
}
