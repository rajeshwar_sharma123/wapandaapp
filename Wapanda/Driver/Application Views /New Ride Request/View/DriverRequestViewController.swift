//
//  DriverRequestViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 13/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
class DriverRequestViewController: BaseViewController {
    //MARK:- Outlets
    @IBOutlet weak var imageViewRider: UIImageView!
    @IBOutlet weak var lblRiderName: UILabel!
    var timerDriver: SRCountdownTimer!
    @IBOutlet weak var riderImageBackgroundView: UIView!
    @IBOutlet weak var timerBackgroundView: UIView!
    @IBOutlet weak var btnCounter: CustomButton!
    @IBOutlet weak var lblPreviousPrice: UILabel!
    @IBOutlet weak var btnAcceptRide: CustomButton!
    @IBOutlet weak var lblRidePrice: UILabel!
    @IBOutlet weak var lblEstminatedTime: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var counterValueLabel: UILabel!
    
    @IBOutlet weak var viewPreviousPrice: UIBorderView!
    @IBOutlet weak var imageViewCheckBox: UIImageView!
    @IBOutlet weak var trailingLblPriceToImageView: NSLayoutConstraint!
    @IBOutlet weak var trailingLblPriceToBorderView: NSLayoutConstraint!
    
    var driverRequestPresenter : NewRidePresenter!
    var driverBidDelegate : DriverBidViewDelegate!
    var objNotification:PushNotificationObjectModel!
    var conterBidVC : CounterPriceViewController!
    //MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.driverRequestPresenter = NewRidePresenter(delegate: self)
        
        //Add notification if app comes from background to foreground
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.initialsePickUpAndDestination()
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.initialiseCounterTimer(60)
        self.initialiseRiderImageView()
        self.initialSetUpView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Helper Methods
    @objc private func appDidBecomeActive()
    {
        if AppDelegate.sharedInstance.isActivatedFromBackground
        {
        self.initialiseCounterTimer(60)
        self.initialiseRiderImageView()
        }
    }
    private func initialSetUpView()
    {
        if let riderProfile = self.objNotification.riderDoc
        {
        self.lblRiderName.text = "\(riderProfile.firstName?.capitalizeWordsInSentence() ?? "") \(riderProfile.lastName?.capitalizeWordsInSentence() ?? "")"
        }
        if (self.objNotification.biddingDoc?.riderCountered!)!
        {
            self.initialiseViewWithCounterEnable()
        }
        else
        {
            self.initialiseViewWithCounterDisable()
        }
        
    }
    
    private func initialsePickUpAndDestination()
    {
        if let fromAddress = self.objNotification.biddingDoc?.fromAddress
        {
            self.lblPickUpAddress.text = "Pickup at : \(fromAddress)"
        }
        if let toAddress = self.objNotification.biddingDoc?.toAddress
        {
         self.lblDestination.text = toAddress
        }
    
    }
    private func initialiseRiderImageView()
    {
        self.riderImageBackgroundView.clipsToBounds = true
        self.riderImageBackgroundView.layer.cornerRadius = self.riderImageBackgroundView.frame.height/2
         self.imageViewRider.kf.setImage(with: self.getImageURL(self.objNotification.riderDoc?.profileImageFileId ?? ""), placeholder: #imageLiteral(resourceName: "ic_account_placeholder"), options: nil, progressBlock:nil) { (image, error, cacheType, url) in
        }
    }
    private func initialiseCounterTimer(_ counterTotalTime:TimeInterval)
    {
        if let _ = timerDriver{}else{
            self.timerDriver = SRCountdownTimer(frame: CGRect(x: 0, y: 0, width: self.timerBackgroundView.frame.width, height: self.timerBackgroundView.frame.width))
        }
        self.timerDriver.lineWidth = 4.0
        self.timerDriver.backgroundColor = UIColor.clear
        self.timerDriver.lineColor = .white
         self.timerDriver.isLabelHidden = true
        self.timerDriver.trailLineColor = UIColor.clear
        self.timerBackgroundView.addSubview(self.timerDriver)
        self.timerDriver.delegate = self
        let closeByTime = (self.objNotification.biddingDoc?.closeBy)!.getDateFromZoneFormate()
        self.timerDriver.elapsedTime = counterTotalTime - closeByTime.timeIntervalSinceNow
        self.timerDriver.start(beginingValue: Int(counterTotalTime))
        
       
    }
    private func initialiseViewWithCounterDisable()
    {
        self.imageViewCheckBox.isHidden = false
         self.imageViewCheckBox.getImageViewWithImageTintColor(color: UIColor(red: 185.0/255.0, green: 254.0/255.0, blue: 10.0/100.0, alpha: 1.0))
        self.viewPreviousPrice.isHidden = true
        self.lblRidePrice.text = String(format:"$%0.2f",(self.objNotification.biddingDoc?.estPrice)!)
        self.lblEstminatedTime.text = self.getEstimateDurationFormat((self.objNotification.biddingDoc?.estDuration)!)
        
//        let closeByTime = (self.objNotification.biddingDoc?.closeBy)!.getDateFromZoneFormate()
//        self.timerDriver.elapsedTime = closeByTime.timeIntervalSinceNow
        self.btnCounter.backgroundColor = UIColor.appCounterGreyColor()
        self.btnCounter.isUserInteractionEnabled = false
    }
    private func initialiseViewWithCounterEnable()
    {
        self.imageViewCheckBox.isHidden = true
       
        self.viewPreviousPrice.isHidden = false
        self.lblRidePrice.text = String(format:"$%0.2f",(self.objNotification.biddingDoc?.riderCounterPrice)!)
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format:"$%0.2f",(self.objNotification.biddingDoc?.estPrice)!))
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
        self.lblPreviousPrice.attributedText = attributeString
        self.lblEstminatedTime.text = self.getEstimateDurationFormat((self.objNotification.biddingDoc?.estDuration)!)
        self.btnCounter.backgroundColor = UIColor.appCounterBlueColor()
        self.btnCounter.isUserInteractionEnabled = true
    }
    private func getEstimateDurationFormat(_ estimateSec:Int)->String
    {
        let (hours,min) = self.secondsToHoursMinutesSeconds(seconds: estimateSec/1000)
        
        if hours != 0
        {
            return ("\(hours) hrs, \(min) min")
        }
        else
        {
            return ("\(min) min")
        }
    
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60)
    }
    //MARK:- IBActions
    
    @IBAction func counterButtonTapped(_ sender: Any) {
        self.conterBidVC = UIViewController.getViewController(CounterPriceViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        self.conterBidVC.riderCounteredPrice = self.objNotification.biddingDoc?.riderCounterPrice ?? 00.00
         self.conterBidVC.counteredPriceDelegate = self
        self.conterBidVC.priceObjNotification = self.objNotification
        self.conterBidVC.modalPresentationStyle = .overCurrentContext
        self.present(conterBidVC, animated: true, completion: nil)
        
    }
    @IBAction func acceptPriceButtonTapped(_ sender: Any) {
     
        if let currentLocation = LocationServiceManager.sharedInstance.currentLocation
        {
            self.driverRequestPresenter.sendAcceptRideRequest(withRideRequestModel:NewRideScreenRequestModel(lat: currentLocation.coordinate.latitude, lng: currentLocation.coordinate.latitude, success: true, message: "", biddingId: (objNotification.biddingDoc?.id)!))
        }
    }
    
    //GeoCode Coordinate Methods
    
    func getAddress(fromCoordinates coordinate: CLLocationCoordinate2D,addressTextField:UILabel,isStartLoc:Bool){
        if ReachabilityManager.shared.isNetworkAvailable
        {
            GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (response: GMSReverseGeocodeResponse?, error: Error?) in
                
                let address = self.getFormattedSourceLocationAddressTitle(WithGeocodingResponse: response)
                DispatchQueue.main.async(execute: {
                    if !isStartLoc
                    {
                    addressTextField.text = address
                    }
                    else
                    {
                        addressTextField.text = "Pickup at : \(address)"
                    }
                })
            })
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
        }
        
    }
    func getFormattedSourceLocationAddressTitle(WithGeocodingResponse response: GMSReverseGeocodeResponse?)->String{
        var strSourceAddress = ""
        
        if var firstResult = response?.firstResult()?.lines{
            
            //Remove Empty Strings from result
            for (index, value) in firstResult.enumerated(){
                if value.isEmpty{
                    firstResult.remove(at: index)
                }
            }
            
            strSourceAddress = firstResult.joined(separator: ", ")
        }
        
        if strSourceAddress.isEmpty{
            strSourceAddress = AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_LOCATION_FOUND
        }
        
        return strSourceAddress
    }
    
}
extension DriverRequestViewController:SRCountdownTimerDelegate
{
    func timerDidUpdateCounterValue(newValue: Int){

        self.counterValueLabel.text="\(newValue)"

    }
    func timerDidStart(){}
    func timerDidPause(){}
    func timerDidResume(){}
    func timerDidEnd(){
        if let _ = self.conterBidVC
        {
           self.conterBidVC!.dismiss(animated: false, completion: {
            self.dismiss(animated: true, completion: {})
           })
        }
        else
        {
            self.dismiss(animated: true, completion: {})
        }
    }
}
extension DriverRequestViewController : NewRideScreenViewDelgate
{
    func newRideSubmittedSuccessful(withResponseModel: NewRideResponseModel) {
        if withResponseModel.success!
        {
            self.dismiss(animated: true, completion: {
                let objPush = PushNotificationObjectModel(JSON: [:])
                objPush?.trip = withResponseModel.trip
                self.driverBidDelegate.driverDidAcceptedRide(objPush!)
            })
        }
    }
    
    func didChangeRidePrice(_ counterPrice: Double) {
        
    }
    
    func showLoader()
    {
        self.showLoader(self)
    }
    
    func hideLoader()
    {
     self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
extension DriverRequestViewController:CounteredPriceViewDelegate
{
    func didCounterPrice(_ objNotification:PushNotificationObjectModel) {
        self.dismiss(animated: true, completion: {
            self.driverBidDelegate.driverDidCounterRide(objNotification)
        })
    }
}

