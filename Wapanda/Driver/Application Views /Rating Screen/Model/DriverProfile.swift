//
//  DriverProfile.swift
//
//  Created by Daffolapmac on 16/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DriverProfile: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let available = "available"
    static let createdAt = "createdAt"
    static let approved = "approved"
    static let lastLoc = "lastLoc"
    static let status = "status"
    static let id = "_id"
    static let aggreeTermsAndConditions = "aggreeTermsAndConditions"
    static let updatedAt = "updatedAt"
    static let ratio = "ratio"
    static let statsTripSuccessCount = "statsTripSuccessCount"
    static let ongoingRate = "ongoingRate"
    static let averageRating = "averageRating"
    static let statsTripCount = "statsTripCount"
  }

  // MARK: Properties
  public var available: Bool? = false
  public var createdAt: String?
  public var approved: Bool? = false
  public var lastLoc: LastLoc?
  public var status: String?
  public var id: String?
  public var aggreeTermsAndConditions: Bool? = false
  public var updatedAt: String?
  public var ratio: Int?
  public var statsTripSuccessCount: Int?
  public var ongoingRate: Int?
  public var averageRating: Int?
  public var statsTripCount: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    available <- map[SerializationKeys.available]
    createdAt <- map[SerializationKeys.createdAt]
    approved <- map[SerializationKeys.approved]
    lastLoc <- map[SerializationKeys.lastLoc]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    aggreeTermsAndConditions <- map[SerializationKeys.aggreeTermsAndConditions]
    updatedAt <- map[SerializationKeys.updatedAt]
    ratio <- map[SerializationKeys.ratio]
    statsTripSuccessCount <- map[SerializationKeys.statsTripSuccessCount]
    ongoingRate <- map[SerializationKeys.ongoingRate]
    averageRating <- map[SerializationKeys.averageRating]
    statsTripCount <- map[SerializationKeys.statsTripCount]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.available] = available
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    dictionary[SerializationKeys.approved] = approved
    if let value = lastLoc { dictionary[SerializationKeys.lastLoc] = value.dictionaryRepresentation() }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.aggreeTermsAndConditions] = aggreeTermsAndConditions
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = ratio { dictionary[SerializationKeys.ratio] = value }
    if let value = statsTripSuccessCount { dictionary[SerializationKeys.statsTripSuccessCount] = value }
    if let value = ongoingRate { dictionary[SerializationKeys.ongoingRate] = value }
    if let value = averageRating { dictionary[SerializationKeys.averageRating] = value }
    if let value = statsTripCount { dictionary[SerializationKeys.statsTripCount] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.available = aDecoder.decodeBool(forKey: SerializationKeys.available)
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.approved = aDecoder.decodeBool(forKey: SerializationKeys.approved)
    self.lastLoc = aDecoder.decodeObject(forKey: SerializationKeys.lastLoc) as? LastLoc
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.aggreeTermsAndConditions = aDecoder.decodeBool(forKey: SerializationKeys.aggreeTermsAndConditions)
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.ratio = aDecoder.decodeObject(forKey: SerializationKeys.ratio) as? Int
    self.statsTripSuccessCount = aDecoder.decodeObject(forKey: SerializationKeys.statsTripSuccessCount) as? Int
    self.ongoingRate = aDecoder.decodeObject(forKey: SerializationKeys.ongoingRate) as? Int
    self.averageRating = aDecoder.decodeObject(forKey: SerializationKeys.averageRating) as? Int
    self.statsTripCount = aDecoder.decodeObject(forKey: SerializationKeys.statsTripCount) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(available, forKey: SerializationKeys.available)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(approved, forKey: SerializationKeys.approved)
    aCoder.encode(lastLoc, forKey: SerializationKeys.lastLoc)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(aggreeTermsAndConditions, forKey: SerializationKeys.aggreeTermsAndConditions)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(ratio, forKey: SerializationKeys.ratio)
    aCoder.encode(statsTripSuccessCount, forKey: SerializationKeys.statsTripSuccessCount)
    aCoder.encode(ongoingRate, forKey: SerializationKeys.ongoingRate)
    aCoder.encode(averageRating, forKey: SerializationKeys.averageRating)
    aCoder.encode(statsTripCount, forKey: SerializationKeys.statsTripCount)
  }

}
