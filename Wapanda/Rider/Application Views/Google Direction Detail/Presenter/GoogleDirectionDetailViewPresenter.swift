
//Notes:- This class is used as presenter for GoogleDirectionDetailViewPresenter

import Foundation
import ObjectMapper

class GoogleDirectionDetailViewPresenter: ResponseCallback{
    
//MARK:- GoogleDirectionDetailViewPresenter local properties
    
    private weak var googleDirectionDetailViewDelegate          : GoogleDirectionDetailViewDelegate?
    private lazy var googleDirectionDetailBusinessLogic         : GoogleDirectionDetailBusinessLogic = GoogleDirectionDetailBusinessLogic()
    private var shouldHideLoader = true

//MARK:- Constructor
    
    init(delegate responseDelegate:GoogleDirectionDetailViewDelegate){
        self.googleDirectionDetailViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        
        if self.shouldHideLoader{
            self.googleDirectionDetailViewDelegate?.hideLoader()
        }

        if let reponse = responseObject as? GoogleDirectionResponse{
            self.googleDirectionDetailViewDelegate?.directionDetailWithData(data: reponse)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        
        if self.shouldHideLoader{
            self.googleDirectionDetailViewDelegate?.hideLoader()
        }
        
        self.googleDirectionDetailViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call GoogleDirectionDetail Api.
    
    func sendGoogleDirectionDetailRequest(withGoogleDirectionDetailViewRequestModel googleDirectionDetailViewRequestModel:GoogleDirectionDetailViewRequestModel, shouldShowLoader status: Bool = true){
        
        if status{
            self.googleDirectionDetailViewDelegate?.showLoader()
        }
        else{
            self.shouldHideLoader = false
        }
        
        let requestModel = GoogleDirectionDetailRequestModel.Builder()
                        .setSourceLatitude(googleDirectionDetailViewRequestModel.source.latitude)
                        .setSourceLongitude(googleDirectionDetailViewRequestModel.source.longitude)
                        .setDestinationLatitude(googleDirectionDetailViewRequestModel.destination.latitude)
                        .setDestinationLongitude(googleDirectionDetailViewRequestModel.destination.longitude)
                        .setWayPoint(googleDirectionDetailViewRequestModel.wayPoint?.latitude, lng: googleDirectionDetailViewRequestModel.wayPoint?.longitude)
                        .build()
        
        
        self.googleDirectionDetailBusinessLogic.performGoogleDirectionDetail(withGoogleDirectionRequestModel: requestModel, presenterDelegate: self)
    }
}
