//
//  RideListFilterViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 06/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import GooglePlaces
import IQKeyboardManager

enum RIDE_FILTER_SCREEN_TYPE{
    case SORT
    case CAR
    case EDIT_ADDRESS
}

protocol RideListFilterProtocol: class {
    func filterSelected(withSortingType sortingType:String, selectedCars: [RideList], updatedAddress: CabSelectionViewRequestModel)
}

class RideListFilterViewController: BaseViewController {
    
    @IBOutlet weak var lblSortingTypeValue: UILabel!
    @IBOutlet weak var viewSort: UIView!
    @IBOutlet weak var viewCabType: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewSearchResult: UITableView!
    @IBOutlet weak var imgViewSortByDropdown: UIImageView!
    @IBOutlet weak var imgViewCartTypeDropdown: UIImageView!
    @IBOutlet weak var imgViewSelectedCar: UIImageView!
    @IBOutlet weak var lblSelectedCarType: UILabel!
    
    var dataSourceSort = [
                            AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_PRICE,
                            AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_DISTANCE
                         ]
    
    var dataSourceSelectedCars: [RideList] = []
    var dataSourcePreSelectedCars: [RideList] = []
    
    var dataSourceSelectedSortingType = UserRideDetail.shared.sortingType
    
    var screenType = RIDE_FILTER_SCREEN_TYPE.SORT
    
    var presenterCabList: CabSelectionViewPresenter!
    var cabListDataSource: CabSelectionResponseModel?
    var cabDataHashMap: [String: [RideList]] = [:]
    var priceSortedCabList: [String: [RideList]] = [:]
    var timeSortedCabList: [String: [RideList]] = [:]
    var selectedCab: [String: RideList] = [:]
    var viewDataSource: CabSelectionViewRequestModel!
    
    weak var delegate: RideListFilterProtocol?
    
    //Edit Source And Destination Screen Global Variables
    
    //Outlets
    @IBOutlet weak var txtFieldSourceAddress: UITextField!
    @IBOutlet weak var txtFieldDestinationAddress: UITextField!
    
    //Global Variables
    var dataSource: [GMSAutocompletePrediction] = []
    var dataSourceUserAddresses: [Address] = []
    var presenterGooglePlaces: GooglePlaceViewPresenter!
    var placeDetailPresenter: GooglePlaceDetailViewPresenter!
    var selectedSourceAddress: SelectedPlaceModel!
    var selectedDestinationAddress: SelectedPlaceModel!
    var selectedAddressTitle: String!
    var editAddressType: ADDRESS_TYPE!
    var addAddressTypeSelected = ADD_ADDRESS_TYPE.NONE
    var searchString = ""
    var searchReultDelegateAndDataSource: SearchAddressTableViewDataSourceAndDelegate!
    
    //Bottom View
    @IBOutlet weak var tblViewAddress: UITableView!
    

    //MARK: View Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.customizeViewAppearance()
        self.initTableView()
        presenterCabList = CabSelectionViewPresenter(delegate: self)
        self.editSourceAndDestinationOnLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.presenterCabList.sendCabSelectionRequest(withCabSelectionViewRequestModel: self.viewDataSource, withLoader: true)
        self.showSelectedCarOnLaunch()
        self.sortFilterDidSet()
    }
    
    internal func customizeTextFields(_ textField:UITextField,andPlaceHolderText text:String){
        
        let font = UIFont.getSanFranciscoSemibold(withSize: 14.0)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        textField.attributedPlaceholder = NSAttributedString(string: text, attributes:attributes)
    }
    
    /// Binds Data With Screen
    ///
    /// - Parameters:
    ///   - cars: Available car list of type [RideList]
    ///   - screenTypeSelected: Selected screen type of type RIDE_FILTER_SCREEN_TYPE
    func bind(withCars cars: [RideList], screenTypeSelected: RIDE_FILTER_SCREEN_TYPE, viewDataSource: CabSelectionViewRequestModel, sortingOrder: String){
        
        self.dataSourcePreSelectedCars = cars
        self.screenType = screenTypeSelected
        self.viewDataSource = viewDataSource
        self.dataSourceSelectedSortingType = sortingOrder
        
        //Selected source and destination handling
        self.selectedSourceAddress = SelectedPlaceModel(title: viewDataSource.sourceAddress, coordinate: CLLocationCoordinate2D(latitude: viewDataSource
            .latFrom, longitude: viewDataSource.lngFrom))
        self.selectedDestinationAddress = SelectedPlaceModel(title: viewDataSource.destinationAddress, coordinate: CLLocationCoordinate2D(latitude: viewDataSource
            .latTo, longitude: viewDataSource.lngTo))
    }
    
    //MARK: Customize View Appearance
    private func customizeViewAppearance(){
        self.setButtonState()
        self.setSourceAndDestination()
        self.customizeTextFields(self.txtFieldSourceAddress, andPlaceHolderText: "Source")
        self.customizeTextFields(self.txtFieldDestinationAddress, andPlaceHolderText: "Destination")
    }
    
    
    /// Set selected option state
    func setButtonState(){
        if screenType == RIDE_FILTER_SCREEN_TYPE.SORT{
           self.setSortViewSelected()
           self.setCarTypeViewUnSelected()
            self.tblView.isHidden = false
            self.tblView.allowsMultipleSelection = false
        }
        else if screenType == RIDE_FILTER_SCREEN_TYPE.CAR{
            self.setCarTypeViewSelected()
            self.setSortViewUnSelected()
            self.tblView.isHidden = false
            self.tblView.allowsMultipleSelection = true
        }
        else{
            self.setSortViewUnSelected()
            self.setCarTypeViewUnSelected()
            self.tblView.isHidden = true
            self.txtFieldSourceAddress.becomeFirstResponder()
        }
    }
    
    /// Method used to show sort type view in selected state
    private func setSortViewSelected(){
        self.viewSort.alpha = 1
        self.imgViewSortByDropdown.isHighlighted = true
    }
    
    /// Method used to show car type view in selected state
    private func setCarTypeViewSelected(){
        self.viewCabType.alpha = 1
        self.imgViewCartTypeDropdown.isHighlighted = true
    }
    
    func setCarTypeViewUnSelected(){
        self.viewCabType.alpha = 0.35
        self.imgViewCartTypeDropdown.isHighlighted = false
    }
    
    func setSortViewUnSelected(){
        self.viewSort.alpha = 0.35
        self.imgViewSortByDropdown.isHighlighted = false
    }
    
    func hideSearchScreen(){
        self.tblViewSearchResult.isHidden = true
        self.view.sendSubview(toBack: self.tblViewSearchResult)
    }
    
    /// Set Source and destination addresess
    func setSourceAndDestination(){
        self.txtFieldSourceAddress.text = self.viewDataSource.sourceAddress
        self.txtFieldDestinationAddress.text = self.viewDataSource.destinationAddress
    }
    
    
    /// Initalize table view
    func initTableView(){
        self.tblView.registerTableViewCell(tableViewCell: SortTypeTableViewCell.self)
        self.tblView.registerTableViewCell(tableViewCell: CarTypeTableViewCell.self)
        self.tblView.registerTableViewCell(tableViewCell: NoResultTableViewCell.self)
        
        if screenType == .SORT{
            self.reloadTableData()
        }
    }
    
    //MARK: Actions
    
    /// Back button action handler
    ///
    /// - Parameter sender: button reference
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.dismiss(animated: false) { 
        }
    }
    
    
    /// Sort button action handler
    ///
    /// - Parameter sender: button reference
    @IBAction func btnSortByClick(_ sender: Any) {
        self.screenType = RIDE_FILTER_SCREEN_TYPE.SORT
        self.customizeViewAppearance()
        
        self.reloadTableData()
    }
    
    
    /// Select cab action handler
    ///
    /// - Parameter sender: button reference 
    @IBAction func btnSelectCabClick(_ sender: Any) {
        self.screenType = RIDE_FILTER_SCREEN_TYPE.CAR
        self.customizeViewAppearance()

        self.reloadTableData()
    }
    
    
    /// Work as single source to apply filter and dismiss screen
    ///
    /// - Parameter sender: sender button reference
    @IBAction func btnApplyClick(_ sender: Any) {
        self.dismiss(animated: false) {
            if self.dataSourceSelectedCars.count > 0 || self.lblSelectedCarType.text == AppConstants.ScreenSpecificConstant.RideListScreen.ALL_CAB_SELECTION_TITLE{
                self.delegate?.filterSelected(withSortingType: self.dataSourceSelectedSortingType, selectedCars: self.dataSourceSelectedCars, updatedAddress: self.viewDataSource)
            }
            else{
                self.delegate?.filterSelected(withSortingType: self.dataSourceSelectedSortingType, selectedCars: self.dataSourcePreSelectedCars, updatedAddress: self.viewDataSource)
            }
        }
    }
    
    //Data Source Manipulating
    func clearDataSources(){
        cabDataHashMap = [:]
        priceSortedCabList = [:]
        timeSortedCabList = [:]
        selectedCab = [:]
    }
    
    func prepareDataSources(){
        self.prepareHashMapFromCabData()
        self.prepareTimeSortedHashMap()
        self.preparePriceSortedHashMap()
    }
    
    func prepareHashMapFromCabData(){
        
        if let cabList = self.cabListDataSource?.rideList{
            for rider in cabList{
                
                if let cabTypeName = rider.cabInfo?.type?.vehicleType{
                    if let _ = self.cabDataHashMap[cabTypeName]{
                        self.cabDataHashMap[cabTypeName]!.append(rider)
                    }
                    else{
                        self.cabDataHashMap[cabTypeName] = [rider]
                    }
                }
            }
        }
    }
    
    func prepareTimeSortedHashMap(){
        for (key, value) in self.cabDataHashMap{
            self.timeSortedCabList[key] = self.getTimeSortedRiderList(data: value)
        }
    }
    
    func preparePriceSortedHashMap(){
        for (key, value) in self.cabDataHashMap{
            self.priceSortedCabList[key] = self.getPriceSortedRiderList(data: value)
        }
    }
    
    func getPriceSortedRiderList(data: [RideList])->[RideList]{
        let sortedData = data.sorted { (rider1, rider2) -> Bool in
            return rider1.rideEstimatedFare! < rider2.rideEstimatedFare!
        }
        
        return sortedData
    }
    
    func getTimeSortedRiderList(data: [RideList])->[RideList]{
        let sortedData = data.sorted { (rider1, rider2) -> Bool in
            return rider1.rideArivalTime! < rider2.rideArivalTime!
        }
        
        return sortedData
    }
    
    //GET PRICE AND TIME
    
    /// This method created price range for selected car type
    ///
    /// - Parameter type: car type as string i.e. standard
    /// - Returns: formatted price range string
    func getPriceRangeForCarType(type: String)->String{
        let priceSortedListForCar = self.priceSortedCabList[type]
        
        if priceSortedListForCar!.count == 1{
            //Give Single Price
            let price = priceSortedListForCar!.first!.rideEstimatedFare
            return String(format: "$%d", UIntMax(price!))
        }
        else{
            //Give Price range
            let lowPrice = priceSortedListForCar!.first!.rideEstimatedFare
            let highPrice = priceSortedListForCar!.last!.rideEstimatedFare
            return String(format: "$%d-%d", UIntMax(lowPrice!), UIntMax(highPrice!))
        }
    }
    
    
    /// Prepare final price with format specified from miliseconds for selected car type
    ///
    /// - Parameter type: car type as string i.e. standard
    /// - Returns: Formatted time to show on screen
    func getTimeForCarType(type: String)->String{
        let timeSortedListOfSelectedCar = self.timeSortedCabList[type]
        return AppUtility.getTimeInMinutes(fromTimeInMS: timeSortedListOfSelectedCar!.first!.rideArivalTime!)
    }
    
    /// This method is used to add selected car in list
    ///
    /// - Parameter cabType: cabType as target car to add
    func addCabAsSelected(withCabType cabType:String){
        let cabResult = cabAlreadyExistsInList(cabType: cabType)
        
        if !cabResult.status{
            let cab = self.cabDataHashMap[cabType]?.first
            self.dataSourceSelectedCars.append(cab!)
        }
        else{
            self.dataSourceSelectedCars.remove(at: cabResult.index)
        }
        
        self.cabDidSetNotification()

    }
    
    
    /// Method is used to check if car exists in selected car list or not
    ///
    /// - Parameter cabType: cabType as target car to add
    /// - Returns: return status of car
    func cabAlreadyExistsInList(cabType: String)->(status: Bool, index: Int){
        
        for  (index, car) in self.dataSourceSelectedCars.enumerated(){
            if cabType == car.cabInfo?.type?.vehicleType{
                return (true, index)
            }
        }
        
        return (false, -1)
    }
    
    func cabAlreadyExistsInPreSelectedCarList(cabType: String)->(status: Bool, index: Int){
        
        for  (index, car) in self.dataSourcePreSelectedCars.enumerated(){
            if cabType == car.cabInfo?.type?.vehicleType{
                return (true, index)
            }
        }
        
        return (false, -1)
    }
    
    func showSelectedCarOnLaunch(){
        if dataSourcePreSelectedCars.count == 0{
            self.imgViewSelectedCar.isHidden = true
            self.lblSelectedCarType.text = AppConstants.ScreenSpecificConstant.RideListScreen.ALL_CAB_SELECTION_TITLE
        }
        else if dataSourcePreSelectedCars.count == 1{
            let selectedCar = dataSourcePreSelectedCars.first!
            self.imgViewSelectedCar.isHidden = false
            self.imgViewSelectedCar.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: selectedCar.cabInfo?.type?.icon ?? ""))!, placeholderImage: #imageLiteral(resourceName: "ic_standard_car"))
            self.lblSelectedCarType.text = selectedCar.cabInfo?.type?.vehicleType?.capitalizeWordsInSentence() ?? ""
            self.imgViewSelectedCar.getImageViewWithImageTintColor(color: UIColor.appPlaceholderColor())
        }
        else if dataSourcePreSelectedCars.count > 1{
            self.imgViewSelectedCar.isHidden = true
            self.lblSelectedCarType.text = AppConstants.ScreenSpecificConstant.RideListScreen.MULTIPLE_CAB_TITLE
        }
    }
    
    func cabDidSetNotification(){
        if dataSourceSelectedCars.count == 0{
            self.imgViewSelectedCar.isHidden = true
            self.lblSelectedCarType.text = AppConstants.ScreenSpecificConstant.RideListScreen.ALL_CAB_SELECTION_TITLE
        }
        else if dataSourceSelectedCars.count == 1{
            let selectedCar = dataSourceSelectedCars.first!
            self.imgViewSelectedCar.isHidden = false
            self.imgViewSelectedCar.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: selectedCar.cabInfo?.type?.icon ?? ""))!, placeholderImage: #imageLiteral(resourceName: "ic_standard_car"))
            self.lblSelectedCarType.text = selectedCar.cabInfo?.type?.vehicleType?.capitalizeWordsInSentence() ?? ""
            self.imgViewSelectedCar.getImageViewWithImageTintColor(color: UIColor.appPlaceholderColor())
        }
        else if dataSourceSelectedCars.count > 1{
            self.imgViewSelectedCar.isHidden = true
            self.lblSelectedCarType.text = AppConstants.ScreenSpecificConstant.RideListScreen.MULTIPLE_CAB_TITLE
        }
    }
    
    func sortFilterDidSet(){
        self.lblSortingTypeValue.text = self.dataSourceSelectedSortingType
    }
    
    func reloadTableData(){
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        self.tblView.reloadData()
    }
}

//MARK: Cab Selection View Delegate
extension RideListFilterViewController: CabSelectionViewDelegate{
    
    //MARK: Cab Selection Delegate
    func cabFetched(data: CabSelectionResponseModel){
        
        if let riderList = data.rideList, riderList.count > 0{
            self.cabListDataSource = data
            
            //Add preselected car in selected car list if available
            for cab in data.rideList!{
                if self.cabAlreadyExistsInPreSelectedCarList(cabType: cab.cabInfo!.type!.vehicleType!).status{
                    self.dataSourceSelectedCars.append(cab)
                }
            }
            //
            
            self.cabDidSetNotification()
            self.prepareDataSources()
            self.reloadTableData()
        }
        else{
            self.reloadTableData()
        }
    }
    
    //MARK: Base View Delegate
    func showLoader() {
        super.showLoader(self)
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.reloadTableData()
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}


//MARK: Table View Delegate & Datasource
extension RideListFilterViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if screenType == RIDE_FILTER_SCREEN_TYPE.SORT{
            return dataSourceSort.count
        }
        else{
            if self.cabDataHashMap.keys.count == 0{
                //Show no cab available message screen in cab list
                return 1
            }
            else{
                return self.cabDataHashMap.keys.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.cabDataHashMap.keys.count == 0{
            return 40.0
        }
        else{
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if screenType == RIDE_FILTER_SCREEN_TYPE.SORT{
            let cell = tableView.getCell(withCellType: SortTypeTableViewCell.self)
            let sortTypeString = self.dataSourceSort[indexPath.row]
            cell.bind(withSortingType: sortTypeString)
            cell.selectionStyle = .none
            
            if dataSourceSelectedSortingType == dataSourceSort[indexPath.row]{
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else{
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            return cell
        }
        else{
            
            if self.cabDataHashMap.keys.count == 0{
                let cellNoCabAvailable = tableView.getCell(withCellType: NoResultTableViewCell.self)
                cellNoCabAvailable.bind(title: AppConstants.ScreenSpecificConstant.RiderSelectCab.NO_CAB_AVAILABLE_MESSAGE)
                cellNoCabAvailable.backgroundColor = UIColor.appThemeColor()
                cellNoCabAvailable.lblMessage.textColor = UIColor.white
                return cellNoCabAvailable
            }
            else{
            
                let cell = tableView.getCell(withCellType: CarTypeTableViewCell.self)
                
                let carType = Array(cabDataHashMap.keys)[indexPath.row]
                let priceRange = self.getPriceRangeForCarType(type: carType)
                let time = self.getTimeForCarType(type: carType)
                let imgId = self.cabDataHashMap[carType]?.first?.cabInfo?.type?.icon ?? ""
                
                cell.bind(withCarType: carType.capitalizeWordsInSentence(), priceRange: priceRange, imageId: imgId, time: time)
                cell.selectionStyle = .none
                
                if cabAlreadyExistsInList(cabType: carType).status{
                    tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                }
                else{
                    tableView.deselectRow(at: indexPath, animated: true)
                }
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if screenType == RIDE_FILTER_SCREEN_TYPE.SORT{
            //handle single click on sorting filter
            UserRideDetail.shared.sortingType = self.dataSourceSort[indexPath.row]
            dataSourceSelectedSortingType = self.dataSourceSort[indexPath.row]
            self.sortFilterDidSet()
        }
        else{
            //handle multiple selection on car type
            guard Array(cabDataHashMap.keys).count != 0 else
            {
                return
            }
            
            let carType = Array(cabDataHashMap.keys)[indexPath.row]
            self.addCabAsSelected(withCabType: carType)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if screenType == RIDE_FILTER_SCREEN_TYPE.CAR{
            //handle single click on sorting filter
            let carType = Array(cabDataHashMap.keys)[indexPath.row]
            self.addCabAsSelected(withCabType: carType)
        }
    }
   
}
