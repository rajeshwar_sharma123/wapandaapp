//
//  DocumentUploadResponseModel.swift
//
//  Created by Daffomac-23 on 8/1/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DocumentUploadResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let contentType = "contentType"
    static let path = "path"
    static let id = "_id"
    static let fileName = "fileName"
    static let contentLength = "contentLength"
    static let createdAt = "createdAt"
  }

  // MARK: Properties
  public var contentType: String?
  public var path: String?
  public var id: String?
  public var fileName: String?
  public var contentLength: Int?
  public var createdAt: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    contentType <- map[SerializationKeys.contentType]
    path <- map[SerializationKeys.path]
    id <- map[SerializationKeys.id]
    fileName <- map[SerializationKeys.fileName]
    contentLength <- map[SerializationKeys.contentLength]
    createdAt <- map[SerializationKeys.createdAt]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = contentType { dictionary[SerializationKeys.contentType] = value }
    if let value = path { dictionary[SerializationKeys.path] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = fileName { dictionary[SerializationKeys.fileName] = value }
    if let value = contentLength { dictionary[SerializationKeys.contentLength] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.contentType = aDecoder.decodeObject(forKey: SerializationKeys.contentType) as? String
    self.path = aDecoder.decodeObject(forKey: SerializationKeys.path) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.fileName = aDecoder.decodeObject(forKey: SerializationKeys.fileName) as? String
    self.contentLength = aDecoder.decodeObject(forKey: SerializationKeys.contentLength) as? Int
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(contentType, forKey: SerializationKeys.contentType)
    aCoder.encode(path, forKey: SerializationKeys.path)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(fileName, forKey: SerializationKeys.fileName)
    aCoder.encode(contentLength, forKey: SerializationKeys.contentLength)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
  }

}
