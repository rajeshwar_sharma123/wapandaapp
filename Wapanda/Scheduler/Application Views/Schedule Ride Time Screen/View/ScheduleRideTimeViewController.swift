//
//  RideScheduleTimeViewController.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Stripe



class ScheduleRideTimeViewController: BaseViewController, SorceAndDestinationViewDelegate, SelectCabDelegate {
    
    var rideScheduleTimeViewPresenter : ScheduleRideTimeViewPresenter!
    
    @IBOutlet weak var selectCabView: SelectCabView!
    @IBOutlet weak var sourceAndDestinationView: SourceAndDestinationView!
    @IBOutlet weak var dateBracketView: UIView!
    @IBOutlet weak var timeBracketView: UIView!
    @IBOutlet weak var datePickerView: UIPickerView!
    @IBOutlet weak var timePickerView: UIPickerView!
    
    @IBOutlet weak var paymentActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var feelingLuckyButton: UIButton!
    var datePickerData: [Any] = [Any]()
    var timePickerData: [String] = [String]()
    var selectedFare: Double = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rideScheduleTimeViewPresenter = ScheduleRideTimeViewPresenter(delegate: self)
        selectCabView.delegate = self
        sourceAndDestinationView.delegate = self

        if let cabInfo = UserRideDetail.shared.selectedCabs.first{
            selectCabView.bindData(cabInfo: cabInfo)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.intialSetupForView()
    }
    
    //MARK:- Helper methods
    /**
     This method is used to set default prperty of Date and Time Picker
     */
    
    func setPickerProperty(){
        datePickerView.delegate = self
        datePickerView.dataSource = self
        datePickerView.backgroundColor = UIColor.clear
        timePickerView.delegate = self
        timePickerView.dataSource = self
        timePickerView.backgroundColor = UIColor.clear
        dateBracketView.layer.cornerRadius = 6
        dateBracketView.layer.borderColor = UIColor.pickerBorderColor().cgColor
        dateBracketView.layer.borderWidth = 1
        timeBracketView.layer.cornerRadius = 6
        timeBracketView.layer.borderColor = UIColor.pickerBorderColor().cgColor
        timeBracketView.layer.borderWidth = 1
    }
    
    /**
     This method is used to set data for Date and Time Picker
     */
    func setDateAndTimePickerData(){
        let today = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE MMM dd"
        datePickerData.append(getDateDetail(today, dateStr: "Today"))
        let maxDate = 13
        for i in 0..<maxDate {
            let date = Calendar.current.date(byAdding: .day, value: i + 1, to: today)
            let dateStr = dateFormatter.string(from: date!)
            //datePickerData.append(dateStr)
            datePickerData.append(getDateDetail(date!, dateStr: dateStr))
        }
        setTimePickerData(dateObj: Date())
    }
    
    func setTimePickerData(dateObj: Date){
        timePickerData.removeAll()
        let today = Date()
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.dateFormat = "dd MM yyyy"
        let futureDateStr = dateFormatter.string(from: dateObj)
        let futureDate = dateFormatter.date(from: futureDateStr)
        let todayDateStr = dateFormatter.string(from: today)
        let todayDate = dateFormatter.date(from: todayDateStr)
        let date: Date!
        if futureDate == todayDate{
            let minutes = calendar.component(.minute, from: today)
            var minuteDiff = 0
            if minutes%10 != 0{
                minuteDiff = 10 - minutes%10
            }
            let extraMinutes = 60 + minuteDiff
            date = Calendar.current.date(byAdding: .minute, value: extraMinutes, to: today)
        }else{
            date = futureDate
        }
        
        var hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        if hour == 0 {
            hour = 24 * 60
        }else{
            hour = 24*60 - hour*60 - minutes
        }
        let minuteInterval:Int = 10
        var minuteIncrease: Int = 0
        let loopIterate: Float =  Float(hour)/Float(minuteInterval)
        let count = Int(loopIterate.rounded())
        
        for _ in 0..<count {
            let dateMinute = Calendar.current.date(byAdding: .minute, value: minuteIncrease, to: date!)
            dateFormatter.locale = Locale(identifier:"en_US_POSIX")
            dateFormatter.dateFormat = "h:mm a"
            let timeStrTmp = dateFormatter.string(from: dateMinute!)
            timePickerData.append(timeStrTmp)
            minuteIncrease = minuteIncrease + minuteInterval
        }
        timePickerView.reloadAllComponents()
    }
    
    /**
     This method is used to create dictionary for date detail
     */
    func getDateDetail(_ date:Any,dateStr: String) -> [String:Any]{
        var dateDic: [String:Any] = [String:Any]()
        dateDic["date"] = date
        dateDic["dateStr"] = dateStr
        return dateDic
    }
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.setupNavigationBar()
        setPickerProperty()
        setDateAndTimePickerData()
        
        if let sourceDestination = UserRideDetail.shared.sourceDestination {
            sourceAndDestinationView.bind(source: sourceDestination.source.address, destination: sourceDestination.destination.address)
        }
        
//        selectCabView.bindData(cabType: type, estPrice: estPrice)
        setDefaultPayment()
    }
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, color: UIColor.schedulingThemeColor(),isTranslucent: false)
        self.customizeNavigationBackButton()
    }
    
    /**
     This method is used to get the scheduleRideTimeRequestModel
     
     -returns : ScheduleRideTimeModel
     */
    func createrideScheduleRequestModelWithTextFieldInput() -> ScheduleRideTimeModel {
        //let pickUpDateTime = getPickupDateTime()
        
        
   // let currentTime = Calendar.current.date(byAdding: .hour, value: 1, to: Date())!
        
        var rideScheduleTimeModel = ScheduleRideTimeModel()
        
        if let location = UserRideDetail.shared.sourceDestination {
            rideScheduleTimeModel.latFrom = Float(location.source.lat)
            rideScheduleTimeModel.lngFrom = Float(location.source.long)
            
            rideScheduleTimeModel.latTo = Float(location.destination.lat)
            rideScheduleTimeModel.lngTo = Float(location.destination.long)
            
            rideScheduleTimeModel.fromAddress = location.source.address
            rideScheduleTimeModel.toAddress = location.destination.address
        }
        
        if let selectedCab = UserRideDetail.shared.selectedCabs.first {
            if selectedCab.name != "Any"
            {
            rideScheduleTimeModel.carType = selectedCab.name
            }
        }
        
        rideScheduleTimeModel.pickupDateTime = getPickupDateTime()
        rideScheduleTimeModel.riderSelectedFare = Float(selectedFare)
        
        return rideScheduleTimeModel
    }
    /**
     This method is used to get the selected Date & Time
     */
    func getPickupDateTime()-> String{
        let dateDic = datePickerData[datePickerView.selectedRow(inComponent: 0)] as! [String:Any]
        let date = dateDic["date"] as! Date
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let selectedDate = dateFormatter.string(from: date)
        let selectedTime = timePickerData[timePickerView.selectedRow(inComponent: 0)]
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        let selectedDateTime = "\(selectedDate) \(selectedTime)"
         let dateTmp = dateFormatter.date(from: selectedDateTime)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(identifier:"UTC")
        let selectedDateTimeString = dateFormatter.string(from: dateTmp!)
        return selectedDateTimeString
    }
    //2017-11-22T19:51:57.491Z
    //Custom method action
    
    @IBAction func feelingLuckyButtonAction(_ sender: Any) {

       if feelingLuckyButton.tag == 1{
        self.rideScheduleTimeViewPresenter.sendScheduleRideTimeRequest(withData: self.createrideScheduleRequestModelWithTextFieldInput())
        }else{
            navigateToPaymentMethodViewController()
        }
    }
    
    
    func navigateToBiddingUnderwayViewController(_ scheduleRideTimeResponseModel: ScheduleRideTimeResponseModel){
        let biddingUnderwayVC = UIViewController.getViewController(BiddingUnderwayViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        biddingUnderwayVC.fromScheduleCheck = true
        biddingUnderwayVC.auctionId = scheduleRideTimeResponseModel.id
        biddingUnderwayVC.riderFare = scheduleRideTimeResponseModel.riderSelectedFare
        biddingUnderwayVC.scheduleRideTimeResponseModel = scheduleRideTimeResponseModel
        self.navigationController?.pushViewController(biddingUnderwayVC, animated: true)
    }
    
    func navigateToPaymentMethodViewController(){
        let paymentMethodVC = UIViewController.getViewController(PaymentMethodViewController.self,storyboard: UIStoryboard.Storyboard.Rider.object)
        self.navigationController?.pushViewController(paymentMethodVC, animated: true)
    }
    
    @objc fileprivate func setDefaultPayment()
    {
        self.feelingLuckyButton.isUserInteractionEnabled = false
        guard let _ = StripePaymentManager.shared.customer else {
            paymentActivityIndicator.isHidden = false
            paymentActivityIndicator.startAnimating()
            self.getDefaultCardFromNewContext()
            return
        }
        if let _ = StripePaymentManager.shared.customer?.defaultSource as? STPSource
        { self.initialseDefaultCardView(enable:true) }
        else
        { self.initialiseNoPayment(enable:true) }
    }

    private func getDefaultCardFromNewContext()
    {
        let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        customerContext.retrieveCustomer { (customer, error) in
            self.paymentActivityIndicator.stopAnimating()
            self.paymentActivityIndicator.isHidden = true
            if error != nil
            {
               // self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: (error?.localizedDescription)!)
                self.initialiseNoPayment(enable:true)
                return
            }
            StripePaymentManager.shared.customer = customer
            if let _ = customer?.defaultSource as? STPSource
            { self.initialseDefaultCardView(enable:true)  }
            else
            { self.initialiseNoPayment(enable:true)   }
        }
    }
    
    func initialiseNoPayment(enable:Bool){
        self.feelingLuckyButton.isUserInteractionEnabled = enable
        self.feelingLuckyButton.setTitle("Add payment method", for: .normal)
        self.feelingLuckyButton.tag = 2
        self.feelingLuckyButton.backgroundColor = UIColor.schedulingThemeColor()

    }
    
    func initialseDefaultCardView(enable:Bool){
        self.feelingLuckyButton.isUserInteractionEnabled = enable
        self.feelingLuckyButton.setTitle("I’m Feelin’ Lucky", for: .normal)
        self.feelingLuckyButton.tag = 1
        self.feelingLuckyButton.backgroundColor = UIColor.appDarkThemeColor()
    }
    
    func selectCabClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnEditSourceOrDestinationClick() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ScheduleRideTimeViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    // The number of columns of data
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        pickerView.subviews.forEach({
            $0.isHidden = $0.frame.height < 1.0
        })
        return 1
    }
    
    // The number of rows of data
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if pickerView == datePickerView{
            return datePickerData.count
        }else{
            return timePickerData.count
        }
    }
    
    // The data to return for the row and component (column) that's being passed in
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var title = ""
        if pickerView == datePickerView{
            let dateDic = datePickerData[row] as! [String:Any]
            title = dateDic["dateStr"] as! String
        }else{
            title = timePickerData[row]
        }
        let attributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName:  UIFont.getTitilliumSemibold(withSize: 18)]
        let mutableString = NSMutableAttributedString(string: title, attributes: attributes)
        return mutableString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView == datePickerView{
            let dateDic = datePickerData[row] as! [String:Any]
            let date = dateDic["date"] as! Date
            setTimePickerData(dateObj: date)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
}

// MARK: - ScheduleRideTimeViewDelegate Methods
extension ScheduleRideTimeViewController : ScheduleRideTimeViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        if alertMessage == AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE{
            self.showErrorAlert(alertTitle, alertMessage: AppConstants.ErrorMessages.RIDE_TIME_GREATER_THAN_NOW, VC: self)
            return
        }
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    func rideScheduleTimeSubmittedSuccessfully(withResponseModel scheduleRideTimeResponseModel: ScheduleRideTimeResponseModel) {
        //print(scheduleRideTimeResponseModel)
        navigateToBiddingUnderwayViewController(scheduleRideTimeResponseModel)
    }
}


