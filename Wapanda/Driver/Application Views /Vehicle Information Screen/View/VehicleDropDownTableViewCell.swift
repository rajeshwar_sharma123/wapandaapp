//
//  VehicleDropDownTableViewCell.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/22/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class VehicleDropDownTableViewCell: UITableViewCell {

    @IBOutlet weak var textFieldDropDown: CustomTextField!
    @IBOutlet weak var lblDropDownTitle: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var lblDropDownError: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
