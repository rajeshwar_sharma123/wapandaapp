//
//  CabLocationsResponseModel.swift
//
//  Created by Daffomac-23 on 8/18/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class CabLocationsResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let cabs = "list"
  }

  // MARK: Properties
  public var cabs: [Cabs]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    cabs <- map[SerializationKeys.cabs]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = cabs { dictionary[SerializationKeys.cabs] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.cabs = aDecoder.decodeObject(forKey: SerializationKeys.cabs) as? [Cabs]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(cabs, forKey: SerializationKeys.cabs)
  }

}
