
//Note :- This class contains Launch Buisness Logic

class LaunchBusinessLogic {
    
    
    deinit {
        print("LaunchBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a LaunchRequestModel
     
     - parameter inputData: Contains info for Launch
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performLaunch(withLaunchRequestModel signUpRequestModel: LaunchRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        LaunchApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        return errorResolver
    }
}
