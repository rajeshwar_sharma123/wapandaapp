//
//  TaxInformationBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class TaxInformationBusinessLogic {
    
    init(){
        print("TaxInformationBusinessLogic init \(self)")
    }
    deinit {
        print("TaxInformationBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform TaxInformation With Valid Inputs constructed into a TaxInformationRequestModel
     
     - parameter inputData: Contains info for Tax Information
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performTaxInformation(withTaxInformationRequestModel taxInformationRequestModel: TaxInformationRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForTaxInformation()
        TaxInformationAPIRequest().makeAPIRequest(withReqFormData: taxInformationRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForTaxInformation() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        
        return errorResolver
    }
}
