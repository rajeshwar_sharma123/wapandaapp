
//Note:- This Extension contains UILabel Extension method

import UIKit


extension UILabel{
    

    /**
     This method calculates the dynamic height of label according to the context.
     
     - parameter str:   text to be set on the label
     - parameter font:  corresponding font for the label
     - parameter width: width of the label
     
     - returns: returns dynamic height for the label.
     */
    
    func calculateHeightForLabel(str:String , forFont font:UIFont, withWidth width:CGFloat) ->CGFloat
    {
        let lblTitle:UILabel = UILabel()
        lblTitle.numberOfLines = 0
        lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping;
        lblTitle.text = str
        lblTitle.font = font
        var size:CGSize = CGSize()
        size = lblTitle.sizeThatFits(CGSize(width: width, height: CGFloat.greatestFiniteMagnitude))
        return size.height
    }
}
