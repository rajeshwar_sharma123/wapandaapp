//
//  CounterPricePresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class CounterPricePresenter: ResponseCallback{
    
    //MARK:- CounterPricePresenter local properties
    private weak var counterPriceViewDelegate             : CounterPriceViewDelegate?
    private lazy var counterPriceBusinessLogic         : CounterPriceBusinessLogic = CounterPriceBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:CounterPriceViewDelegate) {
        self.counterPriceViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.counterPriceViewDelegate?.hideLoader()
        self.counterPriceViewDelegate?.counterPriceSubmittedSuccessful(withResponseModel: responseObject as! DriverCounterResponseModel)
    }
    
    func servicesManagerError(error : ErrorModel){
        self.counterPriceViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.counterPriceViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendcounterPriceSumbitRequest(withData counterScreenRequestModel:CounterPriceScreenRequestModel) -> Void{
        self.counterPriceViewDelegate?.showLoader()
        
        var counterRequestModel : CounterPriceRequestModel!
            counterRequestModel = CounterPriceRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setDriverCounterPrice(counterScreenRequestModel.driverCounterPrice).setUserId(counterScreenRequestModel.bidingId).build()
        self.counterPriceBusinessLogic.performCounterPrice(withCounterPriceRequestModel: counterRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData taxScreenRequestModel:CounterPriceScreenRequestModel) -> Bool {
        
      
        
        return true
    }
}
