
import UIKit
import FFCircularProgressView


class CustomLoader: NSObject {
    
    private var circularProgressView: FFCircularProgressView!
    override init() {

        //Customize the Loader
        
        //Set loader Frame
        let screenSize: CGRect = UIScreen.main.bounds
        let centerPoint = CGPoint(x: screenSize.width / 2, y: screenSize.height / 2)
        self.circularProgressView = FFCircularProgressView(frame: CGRect(x: 40.0, y: 40.0, width: 91.0, height: 90.0))
        self.circularProgressView.center = centerPoint

        //Set the loader Image and Image frame
        let iconFrame = UIImageView()
        iconFrame.image = #imageLiteral(resourceName: "ic_panda_face")
        iconFrame.frame = CGRect(x: 20.5, y: 20, width: 50.0, height: 50.0)
        
        //Customize other loader properties
        self.circularProgressView.iconView = iconFrame
        self.circularProgressView.tintColor = UIColor.white
        self.circularProgressView.lineWidth = 4
       
    }
   

    func showLoader(OnView : UIView) {
        OnView.addSubview(circularProgressView)
        circularProgressView.startSpinProgressBackgroundLayer()
    }

    func hideLoader() {
        circularProgressView.removeFromSuperview()
        circularProgressView.stopSpinProgressBackgroundLayer()
    }

}
