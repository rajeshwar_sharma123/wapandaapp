
//Notes:- This protocol is used as a interface which is used by WapandaPresenter to tranfer info to WapandaViewController
import GooglePlaces

protocol GooglePlaceViewDelegate:BaseViewProtocol {
    func showSearchList(list: [GMSAutocompletePrediction])
}
