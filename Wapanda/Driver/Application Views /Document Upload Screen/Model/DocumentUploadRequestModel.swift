//
//  DocumentUploadRequestModel
//  Wapanda

enum EndPoints:Int
{
    case Insurance  = 0
    case Vehicle    = 1
    case Selfie     = 2
    case Upload     = 3
}
import Foundation
class DocumentUploadRequestModel {
    
    //MARK:- DocumentUploadRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var userId: String!
    var imageData: Data!
    var endPoint: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.userId     = builder.userID
        self.imageData  = builder.imageData
        self.endPoint  = builder.endPoint
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var userID: String!
        var imageData: Data!
        var endPoint: String!
        /**
         This method is used for setting Image Ids for Insurance
         
         - parameter imageIds: String parameter that is going to be set on Image Ids for Insurance
         
         - returns: returning Builder Object
         */
        func setImageIds(_ imageIds:[String]) -> Builder{
            requestBody["imageIds"] = imageIds as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Image Id for vehicle
         
         - parameter imageId: String parameter that is going to be set on Image Id for vehicle
         
         - returns: returning Builder Object
         */
        func setImageId(_ imageId:String)->Builder{
            requestBody["imageId"] = imageId as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Profile Image Id for Selfie
         
         - parameter profileImageFileId: String parameter that is going to be set on Profile Image Id for Selfie
         
         - returns: returning Builder Object
         */
        func setProfileImageFileId(_ profileImageFileId:String)->Builder{
            requestBody["profileImageFileId"] = profileImageFileId as AnyObject?
            return self
        }
        /**
         This method is used for setting User id
         
         - parameter userID: String parameter that is going to be set on userID
         
         - returns: returning Builder Object
         */
        func setUserId(_ userID:String)->Builder{
            self.userID = userID
            return self
        }
        /**
         This method is used for setting User id
         
         - parameter userID: String parameter that is going to be set on userID
         
         - returns: returning Builder Object
         */
        func setEndPoint(forType type:EndPoints)->Builder{
            switch type {
            case .Insurance:
                self.endPoint = String(format: AppConstants.ApiEndPoints.DRIVER_INSURANCE_UPDATE, self.userID)
                break
            case .Vehicle:
                self.endPoint = String(format: AppConstants.ApiEndPoints.DRIVER_VEHCILE_UPDATE, self.userID)
                break
            case .Selfie:
                self.endPoint = String(format: AppConstants.ApiEndPoints.USER_PROFILE_UPDATE, self.userID)
                break
            case .Upload:
                self.endPoint = AppConstants.ApiEndPoints.DRIVER_DOCUMENT_UPLOAD
                break
            }
            return self
        }
        /**
         This method is used for setting Image Binary Data
         
         - parameter imageData: String parameter that is going to be set on imageData
         
         - returns: returning Builder Object
         */
        func setImageData(_ imageData:Data)->Builder{
            self.imageData = imageData
            return self
        }

        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of DocumentUploadRequestModel
         and provide DocumentUploadRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->DocumentUploadRequestModel{
            return DocumentUploadRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Document Upload end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        
        return self.endPoint
    }
    
}
