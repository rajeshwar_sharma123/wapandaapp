

import UIKit

class AddressSearchResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblStateCountry: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func layoutSubviews() {
    }
    
    
    func bind(withTitle title: NSAttributedString, WithStateAndCountry stateAndCountryText: NSAttributedString, img: UIImage = #imageLiteral(resourceName: "ic_location")){
        
        self.lblPlace.attributedText = title
        self.lblStateCountry.attributedText = stateAndCountryText
        self.imgView.image = img
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
