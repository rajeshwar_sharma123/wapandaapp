//
//  HistoryAPIRequestModel.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 13/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

enum UserType:String{
    
    case Rider = "RIDER"
    case Driver = "DRIVER"
  }


//Notes:- This class is used for constructing History Service Request Model

class HistoryRequestModel {
    
    //MARK:- HistoryRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var skip : Int!
    var limit : Int!
    var userType: UserType!
    var sort: String = "endTime"
    var order: Int!
    

    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.limit = builder.limit
        self.skip = builder.skip
        self.userType = builder.userType
        self.sort = builder.sort
        self.order = builder.order

    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        var skip : Int!
        var limit : Int!
        var userType: UserType!
        var sort: String!
        var order: Int!
        

        
        
        /**
         This method is used for setting user role
         
         - parameter limit: Int parameter that is going to be set on limit
         
         - returns: returning Builder Object
         */
        func setUserType(withType type: UserType)->Builder{
            self.userType = type
            return self
        }
        
        
        /**
         This method is used for setting limit of item to be fetched
         
         - parameter limit: Int parameter that is going to be set on limit
         
         - returns: returning Builder Object
         */
        func setSortingType(withType type: String)->Builder{
            self.sort = type
            return self
        }
        
        /**
         This method is used for setting limit of item to be fetched
         
         - parameter limit: Int parameter that is going to be set on limit
         
         - returns: returning Builder Object
         */
        func setLimit(_ limit:Int)->Builder{
            self.limit = limit
            return self
        }
        
        /**
         This method is used for setting number of items to skipped
         
         - parameter skip: Int parameter that is going to be set on skip
         
         - returns: returning Builder Object
         */
        func setSkip(_ skip:Int)->Builder{
            self.skip = skip
            return self
        }
        
        /**
         This method is used for setting number of items to skipped
         
         - parameter skip: Int parameter that is going to be set on skip
         
         - returns: returning Builder Object
         */
        func setOrder(_ order:Int)->Builder{
            self.order = order
            return self
        }


        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of HistoryRequestModel
         and provide HistoryRequestModel object.
         
         -returns : HistoryRequestModel
         */
        func build()->HistoryRequestModel{
            return HistoryRequestModel(builderObject: self)
        }
    }
    /**
     This method is used for getting UpdateUser end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        
        return String(format: AppConstants.ApiEndPoints.PAST_TRIPS, self.userType.rawValue,self.skip,self.limit,self.sort,self.order)
    }

}
