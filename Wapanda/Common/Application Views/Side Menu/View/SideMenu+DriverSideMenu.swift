//
//  SideMenu+DriverSideMenu.swift
//  Wapanda
//
//  Created by daffomac-31 on 23/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit


extension SideMenuViewController{
    
    enum SIDE_MENU_OPTION_DRIVER: Int{
        case PAYMENTS
        case TRIP
        case BID
        case RATING
        case HELP
        case DOCUMENTS
        case RIDER_SIDE
        case SETTINGS
    }
    
    func navigateToDriverScreenWithSelectedIndex(index: Int){
        
        
        
        switch index {
        case SIDE_MENU_OPTION_DRIVER.PAYMENTS.rawValue:
            self.closeSideMenu()
            self.driverPaymentClicked()
        case SIDE_MENU_OPTION_DRIVER.TRIP.rawValue:
            self.closeSideMenu()
            self.driverTripClicked()
        case SIDE_MENU_OPTION_DRIVER.BID.rawValue:
            self.closeSideMenu()
            self.yourBidsClicked()
        case SIDE_MENU_OPTION_DRIVER.RATING.rawValue:
            self.closeSideMenu()
            self.driverRatingClicked()
        case SIDE_MENU_OPTION_DRIVER.HELP.rawValue:
            self.closeSideMenu()
            self.driverHelpClicked()
        case SIDE_MENU_OPTION_DRIVER.DOCUMENTS.rawValue:
            self.closeSideMenu()
            self.driverDocumentClicked()
        case SIDE_MENU_OPTION_DRIVER.RIDER_SIDE.rawValue:
            self.closeSideMenu()
            self.navigateToScreenBasedOnConfirmation()
        case SIDE_MENU_OPTION_DRIVER.SETTINGS.rawValue:
            self.closeSideMenu()
            self.driverSettingClicked()   
        default:
            break
        }
    }
    
    func driverPaymentClicked(){
        let objNewRequestVC = UIViewController.getViewController(StripeWebViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        objNewRequestVC.delegate = UIApplication.shared.visibleViewController as! StripeWebViewDelegate
        objNewRequestVC.urlString = "\(AppConstants.ApiEndPoints.STRIPE_CONNECT_AUTH)?response_type=code&client_id=\(PListUtility.getValue(forKey: AppConstants.PListKeys.STRIPE_STANDARD_CLIENT_ID) as! String)&scope=read_write"
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(objNewRequestVC, animated: true)
        
    }
    
    func driverTripClicked(){
        let tripsVC = UIViewController.getViewController(TripListingViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(tripsVC, animated: true)
    }
    
    func driverRatingClicked(){
        let driverRatingVC =  UIViewController.getViewController(RatingViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        driverRatingVC.driverProfileModel = AppDelegate.sharedInstance.userInformation.driverProfile
         UIApplication.shared.visibleViewController?.navigationController?.pushViewController(driverRatingVC, animated: true)
    }
    
    func driverHelpClicked(){
        let riderHelpVC = UIViewController.getViewController(HelpViewController.self,storyboard: UIStoryboard.Storyboard.Rider.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(riderHelpVC, animated: true)
    }
    
    func driverDocumentClicked(){
        let docSetUpVC = UIViewController.getViewController(DriverSetUpScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        docSetUpVC.screenTitle = "Documents"
        docSetUpVC.shouldShowVerifyScreeen = false
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(docSetUpVC, animated: true)
    }
    func riderSideClicked(){
        
        if AppUtility.isUserLogin(){
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER, value: false)
            self.devicePresenter.sendDriverOfflineRequest()
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }
        else{
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.NON_LOGGED_IS_DRIVER, value: false)
            AppInitialViewHandler.sharedInstance.setupInitialViewControllerForNonLoggedUser()
           
            //self.navigateToRiderHomeScreen()
        }
    }
    
    private func navigateToRiderHomeScreen(){
        let riderHomeVC = UIViewController.getViewController(RiderHomeViewController.self,storyboard: UIStoryboard.Storyboard.Rider.object)
        self.navigationController?.pushViewController(riderHomeVC, animated: true)
    }
    
    func driverSettingClicked(){
        let resetPasswordVC = UIViewController.getViewController(ResetPasswordViewController.self,storyboard: UIStoryboard.Storyboard.Main.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(resetPasswordVC, animated: true)
        
    }
    /// Show your bids for user
    private func yourBidsClicked(){
        let yourBidVC = UIViewController.getViewController(YourBidsViewController.self, storyboard: UIStoryboard.Storyboard.Scheduling.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(yourBidVC, animated: true)
        
    }
    
    func driverLogoutClicked(){
        self.logout()
    }
    
    func moveToDriverProfile(){
        let profile = UIViewController.getViewController(DriverProfileViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(profile, animated: true)
    }
}
extension SideMenuViewController:StripeWebViewDelegate
{
    func didCompleteStripeConnection(withResponseModel stripeConnectRespone: StripeConnectionResponseModel) {
        
        if stripeConnectRespone.error != nil
        {
            self.closeSideMenu()
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: stripeConnectRespone.errorDescription ?? AppConstants.ErrorMessages.SOME_ERROR_OCCURED)
        }
        else
        {
            self.presenterBankInfo.sendDriverConnectRequest(withAuthCode: stripeConnectRespone.code!, false)
        }
    }
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
}
extension SideMenuViewController:BankInfoScreenViewDelgate
{
    func stripeAuthenticationSuccessfull(withResponseModel stripeResponseModel:StripeAuthenticationResponseModel)
    {
        self.closeSideMenu()
    }
    func driverAPIStripeConnectionSuccessfull(withResponseModel connectResponseModel:DriverStripeAPIConnectionResponseModel){
    }
}
