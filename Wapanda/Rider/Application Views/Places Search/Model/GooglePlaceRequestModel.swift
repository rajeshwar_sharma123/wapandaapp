

//Notes:- This class is used for constructing GooglePlace Service Request Model
import GooglePlaces
import UIKit

class GooglePlaceRequestModel {
    
    var query: String
    var bounds: GMSCoordinateBounds?
    var filter: GMSAutocompleteFilter?
    var placeClient: GMSPlacesClient
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.query = builderObject.query
        self.bounds = builderObject.bounds
        self.filter = builderObject.filter
        self.placeClient = builderObject.placeClient
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var query: String = ""
        var bounds: GMSCoordinateBounds?
        var filter: GMSAutocompleteFilter?
        var placeClient = GMSPlacesClient()
        
        func buildWithQuery(queryString: String) -> Builder{
            self.query = queryString
            return self
        }
        
        func buildWithBounds(bounds: GMSCoordinateBounds?) -> Builder{
            self.bounds = bounds
            return self
        }
        
        func buildWithFilter(filter: GMSAutocompleteFilter) -> Builder{
            self.filter = filter
            return self
        }
        
        func buildWithPlaceClient(placeClient: GMSPlacesClient) -> Builder{
            self.placeClient = placeClient
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having.
         */
        
        func build() -> GooglePlaceRequestModel{
            return GooglePlaceRequestModel(builderObject: self)
        }
        
    }
}
