
//Notes:- This protocol is used as a interface which is used by SignUpPresenter to tranfer info to SignUpViewController

protocol SwitchRoleViewDelegate:BaseViewProtocol {
    
    func didReceiveUserProfile(withResponseModel: SignUpResponseModel)
}
