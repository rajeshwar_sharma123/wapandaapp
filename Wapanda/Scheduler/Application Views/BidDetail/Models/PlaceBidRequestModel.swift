import UIKit

class PlaceBidRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var bidId = ""
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        bidId = builder.bidId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var bidId = ""
        
        /**
         This method is used for setting price selected by the driver
         
         - parameter typicalPrice: String parameter that is going to be set on price
         
         - returns: returning Builder Object
         */
        func setPrice(price: Float) -> Builder{
            requestBody["price"] = price as AnyObject
            return self
        }
        
        /**
         This method is used for setting typicalPrice
         
         - parameter typicalPrice: String parameter that is going to be set on typicalPrice
         
         - returns: returning Builder Object
         */
        func setTypicalPrice(typicalPrice: Float) -> Builder{
            requestBody["typicalPrice"] = typicalPrice as AnyObject
            return self
        }
        
        /**
         This method is used for setting fairPrice
         
         - parameter fairPrice: String parameter that is going to be set on fairPrice in request dictionary
         
         - returns: returning Builder Object
         */
        func setFairPrice(fairPrice: Float) -> Builder{
            requestBody["fairPrice"] = fairPrice as AnyObject
            return self
        }
        
        /**
         This method is used for setting fairPriceLow
         
         - parameter fairPriceLow: String parameter that is going to be set on fairPriceLow in request dictionary
         
         - returns: returning Builder Object
         */
        func setFairPriceLow(fairPriceLow: Float) -> Builder{
            requestBody["fairPriceLow"] = fairPriceLow as AnyObject
            return self
        }
        
        /**
         This method is used for setting fairPriceHigh
         
         - parameter fairPriceHigh: String parameter that is going to be set on fairPriceHigh
         
         - returns: returning Builder Object
         */
        func setFairPriceHigh(fairPriceHigh: Float) -> Builder{
            requestBody["fairPriceHigh"] = fairPriceHigh as AnyObject
            return self
        }
        
        /**
         This method is used for setting minimumPrice
         
         - parameter typicalPrice: String parameter that is going to be set on minimumPrice
         
         - returns: returning Builder Object
         */
        func setMinimumPrice(minimumPrice: Float) -> Builder{
            requestBody["minimumPrice"] = minimumPrice as AnyObject
            return self
        }
        
        /**
         This method is used for setting maximumPrice
         
         - parameter maximumPrice: String parameter that is going to be set on maximumPrice
         
         - returns: returning Builder Object
         */
        func setMaximumPrice(maximumPrice: Float) -> Builder{
            requestBody["maximumPrice"] = maximumPrice as AnyObject
            return self
        }
        
        /**
         This method is used for setting bidId
         
         - parameter fare: String parameter that is going to be set on bidId whose detail we have to fetch
         
         - returns: returning Builder Object
         */
        func setBidId(bidId: String) -> Builder{
            self.bidId = bidId
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of PlaceBidAPIRequest
         and provide PlaceBidAPIRequest object.
         
         -returns : PlaceBidAPIRequest
         */
        func build() -> PlaceBidRequestModel {
            return PlaceBidRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting VisitedStop end point
     
     -returns: String containg end point
     */
    func getEndPoint() -> String{
        return "/auctions/quotes/" + bidId 
    }

}
