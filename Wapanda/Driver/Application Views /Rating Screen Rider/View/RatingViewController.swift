//
//  RatingViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 27/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import HCSStarRatingView
class RatingViewController: BaseViewController {
    
    @IBOutlet weak var tableViewRating: UITableView!
    var driverRatingList : DriverRatingResponseModel!
    var driverProfileModel : DriverProfile!
    fileprivate var previouslySelectedIndex = 0
    fileprivate var selectedIndex = 0
    var presenterRating : DriverRatingPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenterRating = DriverRatingPresenter(delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.DriverRatingScreen.NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableViewRating.tableFooterView = UIView()
        self.setUpNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //self.initTableView()
        
        self.presenterRating.sendDriverRatingRequest(withDriverInfoModel: DriverRatingScreenRequestModel(driverId: AppDelegate.sharedInstance.userInformation.id, skip: 0, limit: 10))
    }
    func initTableView(){
        self.tableViewRating.registerTableViewHeaderFooterView(tableViewHeaderFooter: DriverInfoSectionHeaderView.self)
        self.tableViewRating.registerTableViewHeaderFooterView(tableViewHeaderFooter: RatingImageSectionHeaderView.self)
        self.tableViewRating.registerTableViewCell(tableViewCell: NoResultTableViewCell.self)
        
        
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            
            self.tableViewRating.tableHeaderView = self.initialiseRatingTableViewHeader(self.driverRatingList.driverProfile!)
            self.tableViewRating.isScrollEnabled = true
            
        }else{
            self.tableViewRating.tableHeaderView = self.initRatingTableViewHeader()
            self.tableViewRating.isScrollEnabled = false
            
        }
        
        self.tableViewRating.delegate = self
        self.tableViewRating.dataSource = self
    }
    fileprivate func getFormattedRatedOnTime(fromTimeStamp timeStamp:String)->String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let calendar = NSCalendar.current
        let ratedOnDate = timeStamp.getDateFromZoneFormate()
        
        if calendar.isDateInToday(ratedOnDate)
        {
            dateFormatter.dateFormat = "h:mm a"
            return String(format:"Today, %@",dateFormatter.string(from: ratedOnDate).lowercased())
        }
        else if calendar.isDateInYesterday(ratedOnDate)
        {
            dateFormatter.dateFormat = "h:mm a"
            return String(format:"Yesterday, %@",dateFormatter.string(from: ratedOnDate).lowercased())
        }
        else
        {
            dateFormatter.dateFormat = "MMM dd yyy"
            let dateString = dateFormatter.string(from: ratedOnDate)
            dateFormatter.dateFormat = "h:mm a"
            let timeString = dateFormatter.string(from: ratedOnDate).lowercased()
            return String(format:"%@, %@",dateString,timeString)
        }
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension RatingViewController : DriverRatingScreenViewDelgate
{
    func didReceiveDriverRatingList(withResponseModel: DriverRatingResponseModel) {
        
        
        if let _ = self.driverRatingList
        {
            let lastIndex = (self.driverRatingList.rateAndReviews?.items?.count)! // First Index of newly added items
            
            self.driverRatingList.rateAndReviews?.items?.append(contentsOf:withResponseModel.rateAndReviews!.items!)
            self.driverRatingList.rateAndReviews?.hasNext = withResponseModel.rateAndReviews?.hasNext
            
            
            //Index path of newly added items
            var rowsToAdd = [IndexPath]()
            for indexRow in lastIndex ..< (self.driverRatingList.rateAndReviews?.items?.count)!
            {
                rowsToAdd.append(IndexPath(row: indexRow, section: 0))
            }
            
            self.handleTableViewSeperator()
            
            //Insert new added rows
            self.tableViewRating.beginUpdates()
            self.tableViewRating.insertRows(at: rowsToAdd, with: .none)
            self.tableViewRating.endUpdates()
        }
        else
        {

            self.driverRatingList = withResponseModel
            self.handleTableViewSeperator()
            self.initTableView()
            self.tableViewRating.selectRow(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .none)
        }
    }
    
    func handleTableViewSeperator(){
        if let ratingList = self.driverRatingList, let ratingListItems = ratingList.rateAndReviews?.items{
            
            if ratingListItems.count > 0{
                self.tableViewRating.separatorStyle = .singleLine
            }
            else{
                self.tableViewRating.separatorStyle = .none
            }
        }
        else{
            self.tableViewRating.separatorStyle = .none
        }
    }
    
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
//MARK: UITable View Delegate & DataSource
extension RatingViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            return ratings.count
        }
        else{
            //No Rating Found
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            return UITableViewAutomaticDimension
        }
        else{
            //No Rating Found
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            let cell = tableView.getCell(withCellType: DriverRatingTableViewCell.self)
            cell.labelDate.text = self.getFormattedRatedOnTime(fromTimeStamp: ratings[indexPath.row].ratedOn!)
            if let review = ratings[indexPath.row].review,review != "",self.selectedIndex == indexPath.row
            {
                cell.labelFeedback.text = ratings[indexPath.row].review
            }
            else if self.selectedIndex == indexPath.row
            {
                cell.labelFeedback.text = "No review available."
            }
            else
            {
                cell.labelFeedback.text = ""
            }
            cell.driverRatingView.value = CGFloat(ratings[indexPath.row].rating!)
            cell.selectionStyle = .none
            return cell
        }
        else{
            //No Rating Found
            let cell = tableView.getCell(withCellType: NoResultTableViewCell.self)
            cell.contentView.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            cell.bind(title: "No review available.")
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count-1 == indexPath.row,(self.driverRatingList.rateAndReviews?.hasNext!)!{
            self.presenterRating.sendDriverRatingRequest(withDriverInfoModel: DriverRatingScreenRequestModel(driverId: AppDelegate.sharedInstance.userInformation.id, skip: self.driverRatingList?.rateAndReviews?.items?.count, limit: 10))
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let ratings = self.driverRatingList?.rateAndReviews?.items, ratings.count > 0{
            
            self.selectedIndex = indexPath.row
            self.tableViewRating.reloadRows(at: [IndexPath(item: self.previouslySelectedIndex, section: 0),indexPath], with: .none)
            self.previouslySelectedIndex = self.selectedIndex
        }
    }
    
    
    func initialiseRatingTableViewHeader(_ objDriverProfile:DriverProfile)->RatingImageSectionHeaderView
    {
        let nib = self.tableViewRating.getHeader(withHeaderType: RatingImageSectionHeaderView.self)
        nib.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height*0.45)
        
        nib.labelAverageRating.text = String(format:"%.1f",objDriverProfile.averageRating!)
        
        let acceptedPercentage = self.getAcceptancePercentage(totalAcceptedRides: objDriverProfile.statsTripSuccessCount!, totalRides: objDriverProfile.statsTripCount!)
        nib.labelAcceptancePrecent.text = String(format:"%d%%",acceptedPercentage)
        nib.labelAcceptanceStatus.text = self.getStatus(fromPercentage: acceptedPercentage).status
        nib.labelAcceptanceStatus.textColor = self.getStatus(fromPercentage:acceptedPercentage).color
        
        
        nib.labelCancellationPercent.text = String(format:"%d%%",100-acceptedPercentage)
        nib.labelCancellationStatus.text = self.getStatus(fromPercentage: acceptedPercentage).status
        nib.labelCancellationStatus.textColor = self.getStatus(fromPercentage: acceptedPercentage).color
        
        
        return nib
        
    }
    
    
    func initRatingTableViewHeader()->RatingImageSectionHeaderView{
        
        let nib = self.tableViewRating.getHeader(withHeaderType: RatingImageSectionHeaderView.self)
        nib.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height*0.45)
        nib.viewBottomBorder.isHidden = true
        
        nib.labelAverageRating.text = String(format:"%.1f",5.0)
        nib.labelAcceptancePrecent.text = String(format:"%d%%",0)
        nib.labelAcceptanceStatus.text = ""
        nib.labelCancellationStatus.text = ""
        nib.labelAcceptanceStatus.textColor = self.getStatus(fromPercentage:0).color
        nib.labelCancellationPercent.text = String(format:"%d%%",0)
        nib.labelCancellationStatus.textColor = self.getStatus(fromPercentage: 0).color
        
        return nib
        
    }
    
    private func getAcceptancePercentage(totalAcceptedRides:Int,totalRides:Int)->Int
    {
        guard totalRides != 0 else {
            return 0
        }
        return Int(((Double(totalAcceptedRides)/Double(totalRides))*100).rounded())
    }
    private func getStatus(fromPercentage percentage:Int) -> (status: String, color: UIColor)
    {
        if percentage > 90
        {
            return ("Good",UIColor.driverRatingGreenStatus())
        }
        else if percentage >= 80
        {
            return ("Average",UIColor.gray)
        }
        else
        {
            return ("Needs Improvement",UIColor.appDarkThemeColor())
        }
    }
}


