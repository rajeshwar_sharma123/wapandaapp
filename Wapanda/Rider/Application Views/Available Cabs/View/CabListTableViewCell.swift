

import UIKit

class CabListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgViewSelectedCabType: UIImageView!
    @IBOutlet weak var lblSelectedCabTypeName: UILabel!
    @IBOutlet weak var lblSelectedCabTripPrice: UILabel!
    @IBOutlet weak var lblSelectedCabReachingTime: UILabel!
    @IBOutlet weak var viewCellContent: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected
        {
            self.lblSelectedCabTypeName.textColor = UIColor.white
            self.lblSelectedCabReachingTime.textColor = UIColor.white
            self.lblSelectedCabTripPrice.textColor = UIColor.white
            self.imgViewSelectedCabType.getImageViewWithImageTintColor(color: UIColor.white)
            self.contentView.backgroundColor = UIColor.appThemeColor()
        }
        else
        {
            self.lblSelectedCabTypeName.textColor = UIColor.black
            self.lblSelectedCabReachingTime.textColor = UIColor.black
            self.lblSelectedCabTripPrice.textColor = UIColor.appThemeColor()
            self.imgViewSelectedCabType.tintColor = UIColor.clear
           self.imgViewSelectedCabType.getImageViewWithOutImageTintColor(color: UIColor.white)
        }
    }
    
    
    /// Bind data with cell
    ///
    /// - Parameters:
    ///   - name: name of the car type
    ///   - price: price  in UIntMax
    ///   - time: time in miliseconds
    ///   - imgURL: image id for car
    
    func bind(withCabType name: String, price: Double, time: UIntMax, imgURL: String){
        self.lblSelectedCabTypeName.text = name.capitalizeWordsInSentence()
        self.lblSelectedCabReachingTime.text = AppUtility.getTimeInMinutes(fromTimeInMS: time)
        self.lblSelectedCabTripPrice.text = String(format: "$%0.2f", price)
        self.imgViewSelectedCabType.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: imgURL))!, placeholderImage: #imageLiteral(resourceName: "ic_standard_car"))
        
    }
}
