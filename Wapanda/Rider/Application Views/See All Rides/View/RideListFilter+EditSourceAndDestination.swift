//
//  RideListFilter+EditSourceAndDestination.swift
//  Wapanda
//
//  Created by daffomac-31 on 19/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces

extension RideListFilterViewController{
    
    //MARK: View Life Cycle Methods
    func editSourceAndDestinationOnLoad() {
        
        self.searchReultDelegateAndDataSource = SearchAddressTableViewDataSourceAndDelegate()
        self.searchReultDelegateAndDataSource.delegate = self
        
        self.presenterGooglePlaces = GooglePlaceViewPresenter(delegate: self)
        self.txtFieldSourceAddress.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.txtFieldDestinationAddress.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.initalizeUserAddresess()
        self.registerTableViewCells()
        self.initialViewCustomization()
//        self.addTapGestureOnBackgroundClick()
    }
    
//    func addTapGestureOnBackgroundClick(){
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(backgroundClick))
//        gesture.numberOfTouchesRequired = 1
//        self.view.addGestureRecognizer(gesture)
//    }
//    
//    func backgroundClick(){
//        self.hideSearchScreen()
//    }
    
    func registerTableViewCells(){
        self.tblViewSearchResult.registerTableViewCell(tableViewCell: ImageViewWithTitleTableViewCell.self)
        self.tblViewSearchResult.registerTableViewCell(tableViewCell: AddressSearchResultTableViewCell.self)
        
        self.tblViewSearchResult.delegate = self.searchReultDelegateAndDataSource
        self.tblViewSearchResult.dataSource = self.searchReultDelegateAndDataSource
    }
    
    func initScreen(withSourceAddress source: SelectedPlaceModel!, withDestinationAddress destination: SelectedPlaceModel!, withEditAddressType type: ADDRESS_TYPE){
        self.selectedSourceAddress = source
        self.selectedDestinationAddress = destination
        self.editAddressType = type
    }
    
    //MARK: View Customization Helper Methods
    func initialViewCustomization(){
    }
    
    func initalizeUserAddresess(){
        
        if let home = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
            home.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME
            self.dataSourceUserAddresses.append(home)
        }
        
        if let work = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
            work.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_WORK
            self.dataSourceUserAddresses.append(work)
        }
        
        if let recent = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.recentlyVisited{
            recent.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT
            self.dataSourceUserAddresses.append(recent)
        }
    }
}

//MARK: UITextField Delegate ...
extension RideListFilterViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //Setting view for editing address
        self.setSortViewUnSelected()
        self.setCarTypeViewUnSelected()
        self.tblView.isHidden = true
        
        //Clear data source
        self.clearSearchResults()
        
        if textField == self.txtFieldSourceAddress{
            self.editAddressType = .SOURCE
        }
        else{
            self.editAddressType = .DESTINATION
        }
        
        tblViewSearchResult.isHidden = false
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.startSearchForPlaces()
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let userEnteredString = textField.text
        
        let newString = (userEnteredString! as NSString).replacingCharacters(in: range, with: string) as NSString
        
        self.searchString = newString as String
        
        print(newString)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.searchString = ""
        tblViewSearchResult.isHidden = false
        return true
    }
    
    //MARK: Helper Methods
    func textFieldDidChange(){
        self.startSearchForPlaces()
    }
    
    func startSearchForPlaces(){
        
        if !searchString.isEmpty{
            let requestModel = GooglePlaceViewRequestModel(searchString: searchString.getWhitespaceTrimmedString(), userLocationCoordinate: LocationServiceManager.sharedInstance.currentLocation?.coordinate)
            self.presenterGooglePlaces.sendGooglePlaceRequest(withGooglePlaceViewRequestModel: requestModel)
        }
        else{
            self.clearSearchResults()
        }
    }
    
    func clearSearchResults(){
        self.dataSource = []
        
        self.reloadDataOnSearchScreen()
    }
    
    func reloadDataOnSearchScreen(){
        self.searchReultDelegateAndDataSource.bind(dataSourceSearchResults: self.dataSource, userAddress: self.dataSourceUserAddresses, searchText: self.searchString)
        self.tblViewSearchResult.reloadData()
        self.view.bringSubview(toFront: self.tblViewSearchResult)
    }
}

//MARK: Google Place Delegate ...
extension RideListFilterViewController: GooglePlaceViewDelegate{
    
    func showSearchList(list: [GMSAutocompletePrediction]){
        self.dataSource = list
    
        self.reloadDataOnSearchScreen()
    }
}

extension RideListFilterViewController: SearchAddressTableViewDelegate{
    func addressSelected(withAddress address: SelectedPlaceModel){
        if self.editAddressType == .SOURCE{
            self.viewDataSource.sourceAddress = address.title
            self.viewDataSource.latFrom = address.coordinate.latitude
            self.viewDataSource.lngFrom = address.coordinate.longitude
            self.txtFieldSourceAddress.text = address.title
        }
        else{
            self.viewDataSource.destinationAddress = address.title
            self.viewDataSource.latTo = address.coordinate.latitude
            self.viewDataSource.lngTo = address.coordinate.longitude
            self.txtFieldDestinationAddress.text = address.title
        }
    }
}
