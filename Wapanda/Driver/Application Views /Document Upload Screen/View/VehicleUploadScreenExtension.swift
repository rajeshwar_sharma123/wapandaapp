//
//  VehicleUploadScreenExtension.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/21/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension DocumentsUploadScreenViewController
{
    static let VEHICLE_SCREEN_DESCRIPTION = "Take a clear photo of your vehicle.Riders will see this when they check out your reviews."
    static let VEHICLE_SCREEN_TITLE = "Take a Photo of Your Vehicle"
    static let VEHICLE_SCREEN_STEP = "Step 4 of 7"
    static let VEHICLE_SCREEN_PLACEHOLDER_IMAGE = #imageLiteral(resourceName: "car_placeholder")
    static let VEHICLE_SCREEN_PLACEHOLDER_BACKGROUND = UIColor.clear
    
    func navigateToBankInfoController(){
        self.shouldUpdateProfile = true
        let bankInfoVC = UIViewController.getViewController(BankInfoScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        self.navigationController?.pushViewController(bankInfoVC, animated: true)
    }
    func navigateToVehicleController(){
        
        let bankInfoVC = UIViewController.getViewController(VehicleInformationViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        self.navigationController?.pushViewController(bankInfoVC, animated: true)
    }
    
    func initialiseVehicleUploadCellOfCollectionView(_ cell : DocumentPreviewCollectionViewCell,index:Int) -> DocumentPreviewCollectionViewCell
    {
        cell.imageViewPlaceHolder.backgroundColor = DocumentsUploadScreenViewController.VEHICLE_SCREEN_PLACEHOLDER_BACKGROUND
        cell.imageViewPlaceHolder.image = DocumentsUploadScreenViewController.VEHICLE_SCREEN_PLACEHOLDER_IMAGE
        cell.imageViewPreview.isHidden = false
        cell.btnAddImage.isHidden = false
        cell.imageViewPlaceHolder.isHidden = false
        cell.imageViewPreview.clipsToBounds = true
        if let image = self.documentScreenRequestModel.addedImage
        {
            cell.btnAddImage.isHidden = true
            cell.imageViewPlaceHolder.isHidden = true
            cell.imageViewPreview.image = image
            return cell
        }
        guard let _ = self.objProfileUploadDocument.driverDocs?.vehicleDoc?.imageId else
        {
            return cell
        }
        cell.btnAddImage.isHidden = true
        cell.imageViewPlaceHolder.isHidden = false
        cell.imageViewPreview.kf.indicatorType = .activity
         cell.imageViewPreview.kf.setImage(with: URL(string:"\(AppConstants.URL.BASE_URL)/files/\(String(describing: (self.objProfileUploadDocument.driverDocs?.vehicleDoc?.imageId!)!))?api_key=\(AppConstants.APIRequestHeaders.API_KEY_VALUE)"), placeholder: UIImage(), options: nil, progressBlock:nil) { (image, error, cacheType, url) in
            cell.imageViewPlaceHolder.isHidden = true
            
        }
        return cell
        
    }
}
