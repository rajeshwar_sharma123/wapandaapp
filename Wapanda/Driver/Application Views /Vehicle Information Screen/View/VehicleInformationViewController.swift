//
//  VehicleInformationViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/22/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//
enum VehicleInfoInput:Int{
    case Year = 0
    case Make = 1
    case Model = 2
    case Company = 3
    case PlateNumber = 4
    case CarType = 5
}
import UIKit

class VehicleInformationViewController: BaseViewController {
    
    // MARK: - IBOutlets/Local variables
    
    var addReviewScreenSkipButton : Bool = true
    let inputTitles = ["YEAR OF VEHICLE","MAKE OF VEHICLE","MODEL OF VEHICLE","COMPANY/CORPORATE NAME","LICENCE PLATE NUMBER","CAR TYPE"]
    let inputTitlesPlaceholders = ["Year Of Vehicle","Make Of Vehicle","Model Of Vehicle","Company/Corporate Name","Licence Plate Number","Car Type"]
    var inputTitlesError = ["","","","","",""]
    let inputTypes : [VehicleInfoInput] = [.Year,.Make,.Model,.Company,.PlateNumber,.CarType]
    var addVehicleInfoScreenSkipButton : Bool = true
    var dropDownPicker : UIPickerView = UIPickerView()
    var numberOfRows = 0
     var objProfileVehicleDocument:LoginResponseModel!
    
    //======Added====
    var allMakesTypes : MakesResponseModel?
    var yearResponseModel: YearResponseModel?
    var modelsResponseModel: ModelsResponseModel?

    //===========
    var allVehicleTypes : AllVehicleTypeResponseModel!
    var selectedMake : Makes!
    var selectedIndex : Int!
    var vehicleRequestModel = VehicleInfoScreenRequestModel()
    var screenMode = SCREEN_MODE.NEW
    fileprivate var presenterVehicleInformation: VehicleInfoPresenter!
    
    @IBOutlet weak var tableViewVehicleInfo: UITableView!
    
    // MARK: - Terms Review Screen Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenterVehicleInformation = VehicleInfoPresenter(delegate: self)
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.objProfileVehicleDocument = AppDelegate.sharedInstance.userInformation
        self.intialSetupForView()
        
        var model = VehicleInfoScreenRequestModel()
        model.type = ParameterType.Year
        model.year = self.vehicleRequestModel.year
            self.presenterVehicleInformation.getVehicleInfoList(withModel: model)
            self.presenterVehicleInformation.getAllVehicleTypes()

        
    }
    

    
    // MARK: - Helper Methods
    private func intialSetupForView(){
        self.setUpVehicleViewModel()
        self.setUpTableView()
        self.setUpNavigationBar()
        self.setUpPickerView()
        
    }
    
    
    func setUpVehicleViewModel()
    {
        self.vehicleRequestModel.year = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.year ?? 0
        self.vehicleRequestModel.make = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.make ?? ""
        self.vehicleRequestModel.makeId = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.make ?? ""

        self.vehicleRequestModel.model = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.model ?? ""
        self.vehicleRequestModel.companyName = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.companyName ?? ""
        self.vehicleRequestModel.carType = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.carType?.vehicleType ?? ""
        self.vehicleRequestModel.carPlateNumber = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.carPlateNumber ?? ""
    }
    
    
    func setScreenMode(mode: SCREEN_MODE){
        self.screenMode = mode
    }
    
    
    func getCarType(fromCarId carId: String)->String
    {
        let carTypeIdIndex = self.allVehicleTypes.items?.index(where: { (objItems) -> Bool in
            return objItems.id == carId
        })
        if let _ = carTypeIdIndex
        {
            return self.allVehicleTypes.items?[(carTypeIdIndex)!].vehicleType ?? ""
        }
        else
        {
             return AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
        }
    }
    
    
    func getCarId(fromCarType carType: String)->String
    {
        let carTypeIdIndex = self.allVehicleTypes.items?.index(where: { (objItems) -> Bool in
            return objItems.vehicleType == carType
        })
        if let _ = carTypeIdIndex
        {
            return self.allVehicleTypes.items?[(carTypeIdIndex)!].id ?? ""
        }
        else
        {
            return AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
        }
        
    }
    
    
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.VehicleInfo.STEP_NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
        if addVehicleInfoScreenSkipButton
        {
            self.addNavigationSkipButton()
        }
        
    }
    
    
    private func setUpTableView(){
        self.tableViewVehicleInfo.delegate = self
        self.tableViewVehicleInfo.dataSource = self
        self.tableViewVehicleInfo.tableFooterView = UIView()
    }
    
    
    private func setUpPickerView(){
        self.dropDownPicker.delegate = self
        self.dropDownPicker.dataSource = self
        self.dropDownPicker.backgroundColor = UIColor.white
    }
    
    
    internal func dropDownButtonTapped(_ sender:UIButton)
    {
        self.selectedIndex = sender.tag
        let cell = self.tableViewVehicleInfo.cellForRow(at: IndexPath(row: self.selectedIndex, section: 0)) as! VehicleDropDownTableViewCell
//        if (self.selectedIndex == VehicleInfoInput.Model.rawValue) && (self.selectedMake == nil)
//        {
//             AppUtility.presentToastWithMessage(AppConstants.ValidationErrors.SELECT_MAKE_FIRST)
//            return
//        }
        
    switch selectedIndex{
    case VehicleInfoInput.Make.rawValue: if let make = self.allMakesTypes?.makes , make.count > 0{
                                                self.numberOfRows = make.count
                                                }else{
                                                    AppUtility.presentToastWithMessage("Makes are not available.")
                                                        cell.textFieldDropDown.text = ""
        

                                                        return
                                                        }
        
    case VehicleInfoInput.Model.rawValue: if let model = self.modelsResponseModel?.models , model.count > 0{
                                                self.numberOfRows = model.count
                                                }else{
                                                    AppUtility.presentToastWithMessage("Models are not available.")
                                                    cell.textFieldDropDown.text = ""
        
                                                    return
                                                }
    case VehicleInfoInput.Year.rawValue: if yearResponseModel == nil || yearResponseModel?.years == nil{
                                                AppUtility.presentToastWithMessage("Years are not available.")
                                                    cell.textFieldDropDown.text = ""
                                                        return
                                            }
        
    default : self.numberOfRows = 0
        
        }
            cell.textFieldDropDown.becomeFirstResponder()
    }
    
    
    // MARK: - Textfield Delegate Methods
    

    
    //====== TextField DidEndEditing ======
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        self.selectedIndex = textField.tag
        
        switch selectedIndex{
            
            case VehicleInfoInput.Make.rawValue:
                
                if !(textField.text?.getWhitespaceTrimmedString().isEmptyString())!{
                    
                    if let make = self.allMakesTypes?.makes , make.count > 0{
                        self.vehicleRequestModel.makeId = make[dropDownPicker.selectedRow(inComponent: 0)].makeId  ?? ""
                        self.vehicleRequestModel.make = textField.text  ?? ""


                    }
                    
                    
                    var model = VehicleInfoScreenRequestModel()
                    model.year = self.vehicleRequestModel.year
                    model.type = ParameterType.Model
                    model.make =  self.vehicleRequestModel.makeId
                    self.presenterVehicleInformation.getVehicleInfoList(withModel: model)
            }
            
            
        
       case VehicleInfoInput.Year.rawValue:
        
        
                                if !(textField.text?.getWhitespaceTrimmedString().isEmptyString())!
                                    {
                                        
                                        if  yearResponseModel == nil || (yearResponseModel?.years == nil){
                                            
                                            self.vehicleRequestModel.year = Int(textField.text ?? "0") ?? 2000
                                            var model = VehicleInfoScreenRequestModel()
                                            model.type = ParameterType.Year
                                            model.year = self.vehicleRequestModel.year
                                            
                                            self.presenterVehicleInformation.getVehicleInfoList(withModel: model)

                                            return
                                        }

                                        self.vehicleRequestModel.year = Int(textField.text ?? "0") ?? 2000
                                            var model = VehicleInfoScreenRequestModel()
                                            model.year = self.vehicleRequestModel.year
                                            model.type = .Make
                                            self.presenterVehicleInformation.getVehicleInfoList(withModel: model)
                                        }
        default : print("")
        
    }
           }
    

    
    //====== TextField DidBeginEditing ======

    internal func textFieldDidBeginEditing(_ textField:UITextField)
    {
        self.selectedIndex = textField.tag
        let cell = self.tableViewVehicleInfo.cellForRow(at: IndexPath(row: self.selectedIndex, section: 0)) as! VehicleDropDownTableViewCell
        cell.lblDropDownError.text = ""
        self.inputTitlesError[self.selectedIndex] = ""
        
        switch textField.tag {
            
        case VehicleInfoInput.CarType.rawValue:
            if let _ = self.allVehicleTypes
            {
                self.numberOfRows = (self.allVehicleTypes.items?.count)!
                self.vehicleRequestModel.carType = (self.allVehicleTypes.items?[0].vehicleType)!
                textField.text = self.vehicleRequestModel.carType
            }
            else
            {
                self.presenterVehicleInformation.getAllVehicleTypes()
            }
            break
        case VehicleInfoInput.Make.rawValue:
            if let _ = self.allVehicleTypes
            {
                if let makes = self.allMakesTypes?.makes , makes.count > 0{

                self.numberOfRows = makes.count
                self.vehicleRequestModel.make = makes[0].makeDisplay!
                self.vehicleRequestModel.makeId = makes[0].makeId!

                textField.text = makes[0].makeDisplay!
                let cellModel = self.tableViewVehicleInfo.cellForRow(at: IndexPath(row: self.selectedIndex+1, section: 0)) as! VehicleDropDownTableViewCell
                 cellModel.textFieldDropDown.text = ""
                cellModel.textFieldDropDown.isUserInteractionEnabled = true
                self.vehicleRequestModel.model = ""
                self.dropDownPicker.selectRow(0, inComponent: 0, animated: true)
                    self.dropDownPicker.reloadAllComponents()

                }else{
                    cell.textFieldDropDown.inputView = nil
                    cell.textFieldDropDown.becomeFirstResponder()
                }

            }
            else
            {
                //self.presenterVehicleInformation.getCarMakeList(withModel: MakesViewModel(year: 2000, type: .Make))
            }
            break
        case VehicleInfoInput.Model.rawValue:

            if let models = modelsResponseModel?.models,models.count > 0{
                    
                    self.numberOfRows = models.count

                    cell.textFieldDropDown.isUserInteractionEnabled = true

                    cell.textFieldDropDown.inputView = self.dropDownPicker

                    self.vehicleRequestModel.model = (models[0].modelName) ?? ""
                    textField.text = self.vehicleRequestModel.model
                    self.dropDownPicker.selectRow(0, inComponent: 0, animated: true)
                self.dropDownPicker.reloadAllComponents()


            }else{
                cell.textFieldDropDown.inputView = nil
                cell.textFieldDropDown.becomeFirstResponder()
            }
            
            break
        case VehicleInfoInput.Year.rawValue:
            
            if let year = yearResponseModel,(year.years != nil){

                self.numberOfRows = (Int((year.years?.maxYear)!)! - Int((year.years?.minYear)!)!) + 1
          
                textField.text = "\(self.vehicleRequestModel.year)"
                self.dropDownPicker.selectRow(0, inComponent: 0, animated: true)
                self.dropDownPicker.reloadAllComponents()

            }else{
                cell.textFieldDropDown.inputView = nil
                cell.textFieldDropDown.becomeFirstResponder()

            }

            
        default: break
            
        }
        textField.textColor = UIColor.white
    }
    
    
    internal func textFieldDidBeginChanged(_ textField:UITextField)
    {
        
        self.selectedIndex = textField.tag
        
        if self.tableViewVehicleInfo.cellForRow(at: IndexPath(row: self.selectedIndex, section: 0)) is VehicleInfoTableViewCell
        {
            let cell = self.tableViewVehicleInfo.cellForRow(at: IndexPath(row: self.selectedIndex, section: 0)) as! VehicleInfoTableViewCell
            switch textField.tag {
            case VehicleInfoInput.Company.rawValue:
                self.vehicleRequestModel.companyName = textField.text!
                cell.lblTextError.text = ""
                break
            case VehicleInfoInput.PlateNumber.rawValue:
                if textField.text!.characters.count > 8{
                    textField.deleteBackward()
                }else{
                    self.vehicleRequestModel.carPlateNumber = textField.text!
                    cell.lblTextError.text = ""
                }
               
                break
            default: break
                
        }
        self.inputTitlesError[self.selectedIndex] = ""
        textField.textColor = UIColor.white
        }
    }
    
    

    /**
     This function customizes the text field properties
     - returns :void
     */
    internal func customizeTextFields(_ textField:UITextField,andPlaceHolderText text:String){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        textField.attributedPlaceholder = NSAttributedString(string: text, attributes:attributes)
    }
    
    
    
    // MARK: - IBOutlet Action Methods
    
    @IBAction func submitButtonTapped(_ sender: CustomButton) {
        
        self.vehicleRequestModel.carTypeId = self.getCarId(fromCarType: self.vehicleRequestModel.carType)
        if !self.presenterVehicleInformation.sendVehicleInfoSumbitRequest(withData: self.vehicleRequestModel)
        {
            self.tableViewVehicleInfo.reloadData()
        }
    }
    
    // MARK: - Navigation
    
    func navigateToViewerController(WithSelectedItem item : Item){
        let viewerVC = UIViewController.getViewController(DocumentViewerScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        viewerVC.initializeScreen(WithItemData: item)
        self.navigationController?.pushViewController(viewerVC, animated: true)
    }
    
    func navigateToBankInfoController(){
        let bankInfoUpVC = UIViewController.getViewController(BankInfoScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        self.navigationController?.pushViewController(bankInfoUpVC, animated: true)
    }
    func popViewController(){
        self.navigationController?.popViewController(animated: true)
    }
    override func skipButtonClick() {
        self.navigateToBankInfoController()
    }
}




// MARK: - UITableView Delegate Methods

extension VehicleInformationViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.inputTypes[indexPath.row] == .Year || self.inputTypes[indexPath.row] == .Make || self.inputTypes[indexPath.row] == .Model || self.inputTypes[indexPath.row] == .CarType
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleDropDownTableViewCell", for: indexPath) as! VehicleDropDownTableViewCell
            cell.textFieldDropDown.textColor = UIColor.white
            cell.lblDropDownError.text = self.inputTitlesError[indexPath.row]
            cell.textFieldDropDown.addTarget(self, action: #selector(textFieldDidBeginEditing(_:)), for: .editingDidBegin)
            cell.btnDropDown.addTarget(self, action: #selector(dropDownButtonTapped(_:)), for: .touchUpInside)
            cell.textFieldDropDown.addTarget(self, action: #selector(textFieldDidBeginChanged(_:)), for: .editingChanged)
            cell.textFieldDropDown.addTarget(self, action: #selector(textFieldDidEndEditing(textField:)), for: .editingDidEnd)
            // cell.textFieldDropDown.addTarget(self, action: #selector(textField(textField:)), for: .)

            cell.textFieldDropDown.isUserInteractionEnabled = true
            
            switch indexPath.row {
            case VehicleInfoInput.CarType.rawValue:
                cell.textFieldDropDown.text = self.vehicleRequestModel.carType
                break
                
            case VehicleInfoInput.Make.rawValue:
                    cell.textFieldDropDown.text = self.vehicleRequestModel.make
                break
                
            case VehicleInfoInput.Model.rawValue:
                cell.textFieldDropDown.text = self.vehicleRequestModel.model

                
                break
                
            case VehicleInfoInput.Year.rawValue:
                cell.textFieldDropDown.keyboardType = .numberPad
                cell.textFieldDropDown.text = self.vehicleRequestModel.year != 0 ? "\(self.vehicleRequestModel.year)" : ""
                break
            default: break
                
            }
            cell.lblDropDownTitle.text = self.inputTitles[indexPath.row]
            cell.textFieldDropDown.tag = indexPath.row
            cell.btnDropDown.tag = indexPath.row
            cell.textFieldDropDown.inputView = dropDownPicker
            self.customizeTextFields(cell.textFieldDropDown,andPlaceHolderText:self.inputTitlesPlaceholders[indexPath.row])
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleInfoTableViewCell", for: indexPath) as! VehicleInfoTableViewCell
            cell.txtFieldInput.textColor = UIColor.white
            cell.lblTextError.text = self.inputTitlesError[indexPath.row]
            cell.lblInputTitle.text = self.inputTitles[indexPath.row]
            cell.txtFieldInput.tag = indexPath.row
            switch indexPath.row {
            case VehicleInfoInput.PlateNumber.rawValue:
                cell.txtFieldInput.text = self.vehicleRequestModel.carPlateNumber
                break
            case VehicleInfoInput.Company.rawValue:
                cell.txtFieldInput.text = self.vehicleRequestModel.companyName
                break
            default: break
                
            }
            cell.txtFieldInput.addTarget(self, action: #selector(textFieldDidBeginChanged(_:)), for: .editingChanged)
            cell.txtFieldInput.placeholder = self.inputTitlesPlaceholders[indexPath.row]
            self.customizeTextFields(cell.txtFieldInput,andPlaceHolderText:self.inputTitlesPlaceholders[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 94
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableViewVehicleInfo.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
}


// MARK:- UIPicker Delegate Methods

extension VehicleInformationViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.numberOfRows
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let cell = self.tableViewVehicleInfo.cellForRow(at: IndexPath(row: self.selectedIndex, section: 0)) as! VehicleDropDownTableViewCell
        
        switch self.selectedIndex {
            
        case VehicleInfoInput.CarType.rawValue:
                                               self.vehicleRequestModel.carType = (self.allVehicleTypes.items?[row].vehicleType)!
                                                cell.textFieldDropDown.text = self.vehicleRequestModel.carType
        case VehicleInfoInput.Make.rawValue:
                                                self.vehicleRequestModel.make = (self.allMakesTypes?.makes?[row].makeDisplay)!
                                                self.vehicleRequestModel.makeId = (self.allMakesTypes?.makes?[row].makeId)!

                                                cell.textFieldDropDown.text = (self.allMakesTypes?.makes?[row].makeDisplay)!
            
        case VehicleInfoInput.Model.rawValue:
                                                self.vehicleRequestModel.model = (self.modelsResponseModel?.models?[row].modelName)!
                                                cell.textFieldDropDown.text = self.vehicleRequestModel.model
        case VehicleInfoInput.Year.rawValue:
            
                                                self.vehicleRequestModel.year = Int(self.yearResponseModel?.years?.maxYear ?? "0")! - row
                                                cell.textFieldDropDown.text = "\(self.vehicleRequestModel.year)"
        default: break
            
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        switch self.selectedIndex {
            
        case VehicleInfoInput.CarType.rawValue:
                                                return NSAttributedString(string: (self.allVehicleTypes.items?[row].vehicleType)!, attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
            
        case VehicleInfoInput.Make.rawValue:
                                                return NSAttributedString(string: (self.allMakesTypes?.makes?[row].makeDisplay) ?? "", attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
            
            
        case VehicleInfoInput.Model.rawValue:
                                                return NSAttributedString(string: (self.modelsResponseModel?.models?[row].modelName) ?? "", attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
            
        case VehicleInfoInput.Year.rawValue:
            
                                                let year = Int(self.yearResponseModel?.years?.maxYear ?? "2000") ?? 0
            
                                                if row == 0{
                                                    return NSAttributedString(string: "\(year)", attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
                
                                                }else{
                                                    return NSAttributedString(string: "\(year - row)", attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
                
                                                }
             default: break
            
        }
        
                return NSAttributedString(string: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
    }
}


//MARK-  Vehicle Information Delegate

extension VehicleInformationViewController: VehicleInfoScreenViewDelgate{
    
    func showErrorMessage(withMessage message: String, forTextFields: VehicleInfoInput) {
        self.inputTitlesError[forTextFields.rawValue] = message
    }
    
    
    func vehicleInfoSubmittedSuccessfully(withResponseModel vehicleInfoResponseModel: VehicleDoc) {
        //NAVIGATION
        if let _ = self.objProfileVehicleDocument.driverDocs
        {
            self.objProfileVehicleDocument.driverDocs?.vehicleDoc = vehicleInfoResponseModel
        }
        else
        {
            self.objProfileVehicleDocument.driverDocs = DriverDocs(JSON: [:])
            self.objProfileVehicleDocument.driverDocs?.vehicleDoc = vehicleInfoResponseModel
        }
        AppDelegate.sharedInstance.userInformation = self.objProfileVehicleDocument
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        if self.screenMode == SCREEN_MODE.EDITING{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.navigateToBankInfoController()
        }
    }
    
    func allVehicleTypesFetchedSuccessfully(withResponseModel vehicleInfoResponseModel: AllVehicleTypeResponseModel){
        self.allVehicleTypes = vehicleInfoResponseModel
    }
    
    func makesFetchedSuccessfully(withResponseModel vehicleInfoResponseModel: MakesResponseModel) {
        
        self.allMakesTypes = vehicleInfoResponseModel
        
        if let makes = vehicleInfoResponseModel.makes,makes.count > 0{
            self.numberOfRows = makes.count
//            if let year = self.objProfileVehicleDocument.driverDocs?.vehicleDoc?.year , year != self.vehicleRequestModel.year{
                self.vehicleRequestModel.makeId = makes[0].makeId!
                self.vehicleRequestModel.make = makes[0].makeDisplay!
            //}
       
        
        var model = VehicleInfoScreenRequestModel()
        model.year = self.vehicleRequestModel.year
        model.type = ParameterType.Model
        model.make =  self.vehicleRequestModel.makeId
         self.presenterVehicleInformation.getVehicleInfoList(withModel: model)
        }
        
    }
    
    func modelsFetchedSuccessfully(withResponseModel modelResponseModel: ModelsResponseModel) {
        
        self.modelsResponseModel = modelResponseModel
        
        if let models = modelResponseModel.models,models.count > 0{
            self.numberOfRows = models.count
            self.vehicleRequestModel.model = models[0].modelName ?? ""
            
        
            tableViewVehicleInfo.reloadRows(at: [IndexPath(row: VehicleInfoInput.Model.rawValue, section: 0),IndexPath(row: VehicleInfoInput.Make.rawValue, section: 0),IndexPath(row: VehicleInfoInput.Year.rawValue, section: 0)], with: .automatic)
            dropDownPicker.reloadAllComponents()
        }
        
    }
    
   
    func yearsFetchedSuccessfully(withResponseModel yearsResponseModel: YearResponseModel) {
        
        self.yearResponseModel = yearsResponseModel
        
        var model = VehicleInfoScreenRequestModel()
        model.year = self.vehicleRequestModel.year
        model.type = .Make
        self.presenterVehicleInformation.getVehicleInfoList(withModel: model)
    }
    
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
