//
//  DocumentUploadDelegate.swift
//  Wapanda
//

import Foundation

//Notes:- This protocol is used as a interface which is used by DocumentUploadViewPresenter to tranfer info to DocumentUploadViewController

protocol DocumentUploadDelegate: BaseViewProtocol{
    
    func documentUploadedSuccessfully(withResponseModel objDocModel:DocumentUploadResponseModel)
    func insuranceDocUpdatedSuccessfully(withResponseModel objInsurance:InsuranceDoc)
    func vehicleDocUpdatedSuccessfully(withResponseModel objVehicelModel:VehicleDoc)
    func selfieUploadedSuccessfully(withResponseModel objProfileModel:LoginResponseModel)
}

 
