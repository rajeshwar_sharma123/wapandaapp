
//Notes:-

protocol NewPasswordViewDelegate:BaseViewProtocol {
    func passwordChangedSuccessfully() -> Void
    func showToastWithMessage(_ message:String) -> Void
}
