
//Notes:- This class is used as presenter for CabLocationViewPresenter

import Foundation
import ObjectMapper
import GoogleMaps
class CabLocationViewPresenter: ResponseCallback{
    
//MARK:- CabLocationViewPresenter local properties
    
    private weak var cabLocationViewDelegate          : CabLocationViewDelegate?
    private lazy var cabLocationBusinessLogic         : CabLocationBusinessLogic = CabLocationBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:CabLocationViewDelegate){
        self.cabLocationViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let response = responseObject as? CabLocationsResponseModel{
            self.cabLocationViewDelegate?.fetchNearByCabListSuccessful(response)
        }
        if responseObject is LaunchResponseModel
        {
            self.cabLocationViewDelegate?.didReceiveLaunchDetails(withLaunchRequestModel : responseObject as! LaunchResponseModel)
        }
        if responseObject is ProcessPaymentResponseModel{
           AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }
        self.cabLocationViewDelegate?.hideLoader()

    }
    
    func servicesManagerError(error: ErrorModel){
        self.cabLocationViewDelegate?.hideLoader()
        self.cabLocationViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    //MARK:- Methods to make decision and call updateUser Api.
    
    func fetchCabList(withCabViewRequestModel cabViewRequestModel:CabLocationViewRequestModel){
        
        self.cabLocationViewDelegate?.showLoader()
        
        var requestModel: CabLocationRequestModel!
        
        requestModel = CabLocationRequestModel.Builder().setLatitude(cabViewRequestModel.lat).setLongitude(cabViewRequestModel.lng)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).build()
        
        self.cabLocationBusinessLogic.performCabLocationListFetch(withCabLocationRequestModel: requestModel, presenterDelegate: self)
    }

    //MARK : Helper Methods
    
    func updateMarkersLocation(onCabLocationList cabList:[Cabs],andMarkerList markerList:[GMSMarker],with newLoaction:Cabs)
    {
        var cabsList = cabList
        let cabIndex = cabsList.index { (objCab) -> Bool in
            objCab.id == newLoaction.id
        }
        if let index = cabIndex
        {
            let lastCLLocation = CLLocation(latitude: Double(cabsList[index].lat!), longitude: Double(cabsList[index].lng!))
            let newCLLocation = CLLocation(latitude: Double(newLoaction.lat!), longitude: Double(newLoaction.lng!))
            
            cabsList.remove(at: index)
            cabsList.insert(newLoaction, at: index)
            
            self.cabLocationViewDelegate?.updateCabLocationList(cabsList)
            
           let _ = self.cabLocationViewDelegate?.updateMarkerAtPosition(withPoints: CLLocationCoordinate2D(latitude: Double(newLoaction.lat!), longitude: Double(newLoaction.lng!)), marker: markerList[index],bearing: self.getBearingBetweenTwoLocation(oldLocation: lastCLLocation, newLocation: newCLLocation))
        }
        else
        {
            if newLoaction.id != AppDelegate.sharedInstance.userInformation.id
            {
            cabsList.append(newLoaction)
            self.cabLocationViewDelegate?.updateCabLocationList(cabsList)
            let _ = self.cabLocationViewDelegate?.addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D(latitude: Double(newLoaction.lat!), longitude: Double(newLoaction.lng!)), markerImage: #imageLiteral(resourceName: "ic_car"))
            }
        }
    }
    
    func addMarkersLocation(onCabLocationList cabList:[Cabs],andMarkerList markerList:[GMSMarker],with newLoaction:Cabs)
    {
            var cabsList = cabList
        let cabIndex = cabsList.index { (objCab) -> Bool in
            objCab.id == newLoaction.id
        }
        if let index = cabIndex
        {
            let lastCLLocation = CLLocation(latitude: Double(cabsList[index].lat!), longitude: Double(cabsList[index].lng!))
            let newCLLocation = CLLocation(latitude: Double(newLoaction.lat!), longitude: Double(newLoaction.lng!))
            
            cabsList.remove(at: index)
            cabsList.insert(newLoaction, at: index)
            
            self.cabLocationViewDelegate?.updateCabLocationList(cabsList)
            
            let _ = self.cabLocationViewDelegate?.updateMarkerAtPosition(withPoints: CLLocationCoordinate2D(latitude: Double(newLoaction.lat!), longitude: Double(newLoaction.lng!)), marker: markerList[index],bearing: self.getBearingBetweenTwoLocation(oldLocation: lastCLLocation, newLocation: newCLLocation))
        }
        else
        {
        
            if newLoaction.id != AppDelegate.sharedInstance.userInformation.id
            {
                cabsList.append(newLoaction)
                self.cabLocationViewDelegate?.updateCabLocationList(cabsList)
                let _ = self.cabLocationViewDelegate?.addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D(latitude: Double(newLoaction.lat!), longitude: Double(newLoaction.lng!)), markerImage: #imageLiteral(resourceName: "ic_car"))
            }
            
        }

        
    }
    func sendLaunchDataRequest() -> Void{
        
        var launchRequestModel : LaunchRequestModel!
        launchRequestModel = LaunchRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).build()
        self.cabLocationBusinessLogic.performLaunch(withLaunchRequestModel: launchRequestModel, presenterDelegate: self)
    }

    func removeMarkersLocation(onCabLocationList cabList:[Cabs],andMarkerList markerList:[GMSMarker],with newLoaction:Cabs)
    {
        var cabsList = cabList
        var markersList = markerList
        let cabIndex = cabsList.index { (objCab) -> Bool in
            objCab.id == newLoaction.id
        }
        if let index = cabIndex
        {
            cabsList.remove(at: index)
            
            self.cabLocationViewDelegate?.updateCabLocationList(cabsList)
            let _ = self.cabLocationViewDelegate?.removeMarkerAtPosition(marker: markerList[index])
                markersList.remove(at: index)
             self.cabLocationViewDelegate?.updateMarkerList(markersList)

        }
    }
    
    //MARK:- Bearing Calculating methods
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoLocation(oldLocation : CLLocation, newLocation : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: oldLocation.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: oldLocation.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: newLocation.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: newLocation.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
}
