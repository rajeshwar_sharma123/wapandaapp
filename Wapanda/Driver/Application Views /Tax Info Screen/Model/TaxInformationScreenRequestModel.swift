//
//  TaxInformationScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct TaxInformationScreenRequestModel {
    
    var socialSecurityNumber    : String!
    var taxId                   : String!
    var taxClassification       : String!
}
