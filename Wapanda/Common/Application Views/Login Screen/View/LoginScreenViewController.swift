//
//  LoginScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import CountryPicker

class LoginScreenViewController: BaseViewController {

    //MARK:- LoginViewController local properties.
    var loginPresenter : LoginScreenPresenter!
    //MARK:- IBOutlets

    @IBOutlet weak var txtEmailOrPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var lblEmailOrPhoneError: UILabel!
    @IBOutlet weak var lblPasswordError: UILabel!
    
    //Country picker view outlets
    @IBOutlet weak var viewCountryCode: UIView!
    @IBOutlet weak var pickerCountryCode: CountryPicker!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgViewDropdown: UIImageView!
    @IBOutlet weak var _iLeadingEmailOrPhoneLayoutConstraint: NSLayoutConstraint!
    var countryCode: String = ""
    @IBOutlet var _iLeadingEmailOrPhoneToSuperView: NSLayoutConstraint!
    @IBOutlet var _iLeadingEmailOrPhoneToCountryPicker: NSLayoutConstraint!
    var isCountryCodeHidden = true
    
    //MARK:- View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.intialSetupForView()
        addKeyboardActivityListener()
        self.loginPresenter = LoginScreenPresenter(delegate: self,textFieldValidationDelegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupCountryPicker()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewDidLayoutSubviews() {
        if isCountryCodeHidden{
            self.hideCountryCode()
        }
        else{
            self.showCountryCodeView()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction for Buttons
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        self.loginPresenter.sendLoginRequest(withData: self.createLoginScreenRequestModelWithTextFieldInput())
    }
    
    @IBAction func showHidePasswordButtonTapped(_ sender: UIButton) {
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
        self.btnShowHidePassword.setImage(self.txtPassword.isSecureTextEntry ? #imageLiteral(resourceName: "ic_visibility_off_white"): #imageLiteral(resourceName: "ic_visibility_white"), for: .normal)
        
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
     private func intialSetupForView(){
        self.txtEmailOrPhone.delegate = self
        self.txtPassword.delegate = self
        self.setupNavigationBar()
        self.customizeTextFields()
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: "")
        self.customizeNavigationBackButton()
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        self.txtEmailOrPhone.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.LoginScreen.EMAIL_OR_PHONE_PLACEHOLDER_TEXT, attributes:attributes)
        self.txtPassword.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.LoginScreen.PASSWORD_PLACEHOLDER_TEXT, attributes:attributes)
    }

    /**
     This method is used to get the loginViewRequestModel from the current input in text fields
     
     -returns : EmailLoginViewRequestModel with current data in textFields
     */
    func createLoginScreenRequestModelWithTextFieldInput() -> LoginScreenRequestModel {
        var loginScreenRequestModel = LoginScreenRequestModel()
        loginScreenRequestModel.email = self.txtEmailOrPhone.text?.getWhitespaceTrimmedString()
        loginScreenRequestModel.phoneNumber = self.txtEmailOrPhone.text
        loginScreenRequestModel.password = self.txtPassword.text
        loginScreenRequestModel.countryCode = self.countryCode
        
        return loginScreenRequestModel
    }

    
    // MARK: - Navigation
    
    func navigateToUserOptionViewController(){
        let rolesVC = UIViewController.getViewController(viewController: SwitchRoleScreenViewController.self)
        rolesVC.initializeScreenWithDataAs(hideBackButton: true)
        self.navigationController?.pushViewController(rolesVC, animated: true)
    }
    
    
    
    
    // MARK: - Error Methods

    /**
     This method is used to show error on all the text fields with default messages.
     */
    func showAllTextFieldError(){
        
        self.lblEmailOrPhoneError.text = AppConstants.ValidationErrors.ENTER_EMAILID_OR_PHONE_NUMBER
        self.lblPasswordError.text = AppConstants.ValidationErrors.ENTER_PASSWORD
        self.lblEmailOrPhoneError.isHidden = false
        self.lblPasswordError.isHidden = false
    }
    
    
    /**
     This method is used to show error on email or phone number text fields
     
     -parameter message: String the message that is needed to diaplay as error.
     */
    func showEmailOrPhoneNoTextFieldError(withMessage message:String) {
        
        self.lblEmailOrPhoneError.text = message
        self.lblEmailOrPhoneError.isHidden = false
    }
    
    /**
     This method is used to show error on password text fields
     
     -parameter message: String the message that is needed to diaplay as error.
     */
    func showPasswordTextFieldError(withMessage message:String) {
        
        self.lblPasswordError.text = message
        self.lblPasswordError.isHidden = false
    }
    
    override func backButtonClick() ->Void {
        self.navigatedToSpecifiedController(viewController: WelcomeScreenViewController.self, storyboard: UIStoryboard.Storyboard.Main.object)
    }
    
    @IBAction func btnForgetPasswordClick(_ sender: UIButton) {
        self.navigateToForgetPasswordScreen()
    }
    
    private func navigateToForgetPasswordScreen(){
        let forgetPasswordVC = UIViewController.getViewController(ForgetPasswordViewController.self, storyboard: UIStoryboard.Storyboard.Main.object)
        self.navigationController?.pushViewController(forgetPasswordVC, animated: true)
    }
}

// MARK : UITextField Delegate Methods
extension LoginScreenViewController : UITextFieldDelegate
{


    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.txtEmailOrPhone:
            self.lblEmailOrPhoneError.isHidden = true
            if string.isEmpty{
                self.hideCountryCode()
            }
            return true
        case self.txtPassword:
            self.lblPasswordError.isHidden = true
            return true
        default:
            self.lblEmailOrPhoneError.isHidden = true
            self.lblPasswordError.isHidden = true
            return true
        }
      }
 
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtEmailOrPhone{
            if let phoneText = textField.text, phoneText.isPhoneNumberValid(){
                self.showCountryCodeView()
            }
            else{
                self.hideCountryCodeView()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
}

// MARK: - LoginViewDelegate Methods
extension LoginScreenViewController : LoginViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    func loginSuccessfull(withResponseModel loginResponseModel:LoginResponseModel)
    {
        AppDelegate.sharedInstance.userInformation = loginResponseModel
        AppDelegate.sharedInstance.userInformation.saveUser()
        self.registerDeviceToken()
        self.hideLoader(self)
        self.navigateToUserOptionViewController()
    }
    
}

// MARK: - TextFieldValidationDelegate Methods
extension LoginScreenViewController : TextFieldValidationDelegate
{
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType) {
        
        switch forTextFields {
            
        case .EmailOrPhone:
            self.showEmailOrPhoneNoTextFieldError(withMessage: message)
            break
        case .Password:
            self.showPasswordTextFieldError(withMessage: message)
            break
        case .All:
            self.showAllTextFieldError()
            break
        default:
            break
        }
        
    }
}

//MARK: Country Code Handling
extension LoginScreenViewController: CountryPickerDelegate{
    
    func hideCountryCodeView(){
        self.hideCountryCode()
    }
    
    func hideCountryCode(){
        self.isCountryCodeHidden = true
        self._iLeadingEmailOrPhoneToCountryPicker.isActive = false
        self._iLeadingEmailOrPhoneToSuperView.isActive = true
        self.viewCountryCode.isHidden = true
    }
    
    func resetCountryCode(){
        self.countryCode = ""
    }
    
    func showCountryCodeView(){
        self.isCountryCodeHidden = false
        self._iLeadingEmailOrPhoneToCountryPicker.isActive = true
        self._iLeadingEmailOrPhoneToSuperView.isActive = false
        self.viewCountryCode.isHidden = false
    }
    
    /**
     This function is action listener for the show country
     
     - parameter sender: Show country button object
     
     */
    @IBAction func btnShowCountryPicker(_ sender: UIButton) {
        hideKeybaord()
        setPickerViewAppearance()
    }
    
    private func setPickerViewAppearance(){
        
        if pickerCountryCode.isHidden{
            pickerCountryCode.isHidden = false
        }
        else{
            pickerCountryCode.isHidden = true
        }
    }
    
    //MARK: Country Code Picker
    
    /**
     This function setup country code picker
     */
    func setupCountryPicker(){
        
        //get corrent country
        let code = Locale.countryCode()
        
        //init Picker
        pickerCountryCode.countryPickerDelegate = self
        pickerCountryCode.showPhoneNumbers = true
        pickerCountryCode.setCountry(code)
    }
    
    /**
     This function is used to set the country phone code for selected country
     
     - parameter code: Country Phone code
     - parameter image: Country Flag Image
     
     */
    func setSelectedCountryWithPhoneCode(code: String, image: UIImage, countryCode: String){
        
        //Set Data on the screen
        self.lblCountryCode.text = code
        self.imgCountryFlag.image = image
        
        self.countryCode = AppUtility.getFormattedDiallingCodeFromCode(code: code)
    }
    
    /**
     This function is delegate for the picker selection
     */
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage){
        
        //Set Selected Country Code and Image
        setSelectedCountryWithPhoneCode(code: phoneCode, image: flag, countryCode: countryCode)
    }
}

//MARK: Keyboard Handling
extension LoginScreenViewController{
    //MARK: Keyboard handling methods
    
    /**
     This function adds listener for keyboard
     */
    func addKeyboardActivityListener(){
        let notifCenter = NotificationCenter.default
        notifCenter.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
    }
    
    /**
     This function handle screen on keyboard show event
     */
    func keyboardWillShow(){
        
        //Hide country code picker if open
        self.pickerCountryCode.isHidden = true
    }
    
    /**
     This function hide keyboard from screen
     */
    func hideKeybaord(){
        self.view.endEditing(true)
    }
}
