//
//  LoginScreenPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginScreenPresenter: ResponseCallback{
    
    //MARK:- LoginScreenPresenter local properties
    private weak var loginViewDelegate             : LoginViewDelegate?
    private var loginBusinessLogin                 =  LoginBusinessLogic()
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?

    //MARK:- Constructor
    init(delegate loginDelegate: LoginViewDelegate) {
        self.loginViewDelegate = loginDelegate
    }
    
    convenience init(delegate responseDelegate:LoginViewDelegate,textFieldValidationDelegate: TextFieldValidationDelegate) {
        self.init(delegate: responseDelegate)
        self.textFieldValidationDelegate = textFieldValidationDelegate
    }
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN, value: true)
        self.loginViewDelegate?.hideLoader()
        self.loginViewDelegate?.loginSuccessfull(withResponseModel: responseObject as! LoginResponseModel)
    }
    
    func servicesManagerError(error : ErrorModel){
        self.loginViewDelegate?.hideLoader()
        self.loginViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendLoginRequest(withData loginViewRequestModel:LoginScreenRequestModel) -> Void{
        
        guard self.validateInput(withData: loginViewRequestModel) else{
            return
        }
        
        self.loginViewDelegate?.showLoader()
        
        var loginRequestModel : LoginRequestModel!
        
        if loginViewRequestModel.email.isValidEmailId() {
            let userName = loginViewRequestModel.email
                
            loginRequestModel = LoginRequestModel.Builder()
                .setUserName(userName!)
                .setPassword(loginViewRequestModel.password)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .build()
        }else{
            let userName = loginViewRequestModel.countryCode + loginViewRequestModel.phoneNumber

            loginRequestModel = LoginRequestModel.Builder()
                .setUserName(userName)
                .setPassword(loginViewRequestModel.password)
                .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .build()
        }
        
        self.loginBusinessLogin.performLogin(withLoginRequestModel: loginRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData loginRequestModel:LoginScreenRequestModel) -> Bool {
        
        //All fields not empty
        guard self.validateAllFieldsEmpty(withData: loginRequestModel) else{
            //All fields empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: "", forTextFields: .All)
            return false
        }
        
        //email validation
        guard !loginRequestModel.email.isEmptyString() || !loginRequestModel.phoneNumber.isEmptyString() else{
            //email or phone empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_EMAILID_OR_PHONE_NUMBER, forTextFields: .EmailOrPhone)
            return false
        }
        
        guard loginRequestModel.email.isValidEmailId() || loginRequestModel.phoneNumber.isPhoneNumberValid() else {
            //email or phone not valid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_EMAILID_OR_PHONE_NUMBER, forTextFields: .EmailOrPhone)
            return false
        }
        
        //Password validaiton
        guard !loginRequestModel.password.isEmpty else {
            //password empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_PASSWORD, forTextFields: .Password)
            return false
        }
        
        return true
    }
    
    /**
     Validate that all fields are empty or not
     - parameter loginRequestModel : LoginScreenRequestModel
     */
    func validateAllFieldsEmpty(withData loginRequestModel:LoginScreenRequestModel) -> Bool{
        guard !loginRequestModel.email.isEmptyString() || !loginRequestModel.phoneNumber.isEmptyString() || !loginRequestModel.password.isEmpty else {
            return false
        }
        return true
    }
}
