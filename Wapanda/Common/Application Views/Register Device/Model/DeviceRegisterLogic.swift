//
//  DeviceRegisterBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class DeviceRegisterBusinessLogic {
    
    init(){
        print("DeviceRegisterBusinessLogic init \(self)")
    }
    deinit {
        print("DeviceRegisterBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform DeviceRegister With Valid Inputs constructed into a DeviceRegisterRequestModel
     
     - parameter inputData: Contains info for Device Registration
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performDeviceRegister(withDeviceRegisterRequestModel DeviceRegisterRequestModel: DeviceRegisterRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForDeviceRegister()
        DeviceRegisterAPIRequest().makeAPIRequest(withReqFormData: DeviceRegisterRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    func performDeviceDeRegister(withDeviceRegisterRequestModel DeviceRegisterRequestModel: DeviceRegisterRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForDeviceRegister()
        DeviceRegisterAPIRequest().makeDeleteAPIRequest(withReqFormData: DeviceRegisterRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    func performDriverOfflineRequest(withDriverHomeRequestModel signUpRequestModel: DriverHomeRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForDeviceRegister()
        DeviceRegisterAPIRequest().makeOfflineAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForDeviceRegister() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        return errorResolver
    }
}
