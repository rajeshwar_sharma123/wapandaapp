//
//  CabInfo.swift
//
//  Created by  on 8/25/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Cab: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let year = "year"
    static let make = "make"
    static let model = "model"
    static let type = "carType"
    static let companyName = "companyName"
   
    static let id = "_id"
    static let approved = "approved"
    static let imageId = "imageId"
    static let carPlateNumber = "carPlateNumber"
  }

  // MARK: Properties
  public var year: Int?
  public var make: String?
  public var model: String?
  public var type: Type?
  public var companyName: String?
  public var id: String?
  public var approved: Bool? = false
  public var imageId: String?
  public var carPlateNumber: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    year <- map[SerializationKeys.year]
    make <- map[SerializationKeys.make]
    model <- map[SerializationKeys.model]
    type <- map[SerializationKeys.type]
    companyName <- map[SerializationKeys.companyName]
    id <- map[SerializationKeys.id]
    approved <- map[SerializationKeys.approved]
    imageId <- map[SerializationKeys.imageId]
    carPlateNumber <- map[SerializationKeys.carPlateNumber]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = year { dictionary[SerializationKeys.year] = value }
    if let value = make { dictionary[SerializationKeys.make] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = type { dictionary[SerializationKeys.type] = value.dictionaryRepresentation() }
    if let value = companyName { dictionary[SerializationKeys.companyName] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = approved { dictionary[SerializationKeys.approved] = value }
    if let value = imageId { dictionary[SerializationKeys.imageId] = value }
    if let value = carPlateNumber { dictionary[SerializationKeys.carPlateNumber] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.year = aDecoder.decodeObject(forKey: SerializationKeys.year) as? Int
    self.make = aDecoder.decodeObject(forKey: SerializationKeys.make) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? Type
    self.companyName = aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.approved = aDecoder.decodeObject(forKey: SerializationKeys.approved) as? Bool
    self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String
    self.carPlateNumber = aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(year, forKey: SerializationKeys.year)
    aCoder.encode(make, forKey: SerializationKeys.make)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(companyName, forKey: SerializationKeys.companyName)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(approved, forKey: SerializationKeys.approved)
    aCoder.encode(imageId, forKey: SerializationKeys.imageId)
    aCoder.encode(carPlateNumber, forKey: SerializationKeys.carPlateNumber)
  }

}
