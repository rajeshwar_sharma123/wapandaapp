
//Notes:- This class is used as presenter for TermsReviewViewPresenter

import Foundation
import ObjectMapper

class TermsReviewViewPresenter: ResponseCallback{
    
//MARK:- TermsReviewViewPresenter local properties
    
    private weak var termsReviewViewDelegate          : TermsReviewViewDelegate?
    private lazy var termsReviewBusinessLogic         : TermsReviewBusinessLogic = TermsReviewBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:TermsReviewViewDelegate){
        self.termsReviewViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.termsReviewViewDelegate?.hideLoader()
        
        if let responseData = responseObject as? TermsReviewResponseModel{
            self.termsReviewViewDelegate?.termsReviewListFetchedSuccessfully(WithResponseModel: responseData)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.termsReviewViewDelegate?.hideLoader()
        self.termsReviewViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call termsReview Api.
    
    func getTermsReviewList(){
        
        self.termsReviewViewDelegate?.showLoader()
        
        var termsReviewRequestModel : TermsReviewRequestModel!
        
        termsReviewRequestModel = TermsReviewRequestModel.Builder()
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.termsReviewBusinessLogic.performTermsReview(withLoginRequestModel: termsReviewRequestModel, presenterDelegate: self)

    }
}
