//
//  StripeCustomerSession.swift
//
//  Created by Daffolapmac on 30/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class StripeCustomerSession: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let object = "object"
    static let id = "id"
    static let livemode = "livemode"
    static let created = "created"
    static let secret = "secret"
    static let expires = "expires"
    static let associatedObjects = "associated_objects"
  }

  // MARK: Properties
  public var object: String?
  public var id: String?
  public var livemode: Bool? = false
  public var created: Int?
  public var secret: String?
  public var expires: Int?
  public var associatedObjects: [AssociatedObjects]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    object <- map[SerializationKeys.object]
    id <- map[SerializationKeys.id]
    livemode <- map[SerializationKeys.livemode]
    created <- map[SerializationKeys.created]
    secret <- map[SerializationKeys.secret]
    expires <- map[SerializationKeys.expires]
    associatedObjects <- map[SerializationKeys.associatedObjects]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = object { dictionary[SerializationKeys.object] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.livemode] = livemode
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = secret { dictionary[SerializationKeys.secret] = value }
    if let value = expires { dictionary[SerializationKeys.expires] = value }
    if let value = associatedObjects { dictionary[SerializationKeys.associatedObjects] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.object = aDecoder.decodeObject(forKey: SerializationKeys.object) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.livemode = aDecoder.decodeBool(forKey: SerializationKeys.livemode)
    self.created = aDecoder.decodeObject(forKey: SerializationKeys.created) as? Int
    self.secret = aDecoder.decodeObject(forKey: SerializationKeys.secret) as? String
    self.expires = aDecoder.decodeObject(forKey: SerializationKeys.expires) as? Int
    self.associatedObjects = aDecoder.decodeObject(forKey: SerializationKeys.associatedObjects) as? [AssociatedObjects]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(object, forKey: SerializationKeys.object)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(livemode, forKey: SerializationKeys.livemode)
    aCoder.encode(created, forKey: SerializationKeys.created)
    aCoder.encode(secret, forKey: SerializationKeys.secret)
    aCoder.encode(expires, forKey: SerializationKeys.expires)
    aCoder.encode(associatedObjects, forKey: SerializationKeys.associatedObjects)
  }

}
