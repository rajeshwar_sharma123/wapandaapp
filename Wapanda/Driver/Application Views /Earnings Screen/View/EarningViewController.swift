//
//  EarningsViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 27/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Charts
class EarningsViewController: BaseViewController {
    
    @IBOutlet weak var barChartView: BarChartView!
    var weeklyEarnings = [Double](repeating: 0.0, count: 7)
    var weeklyEarningsDetail = [WeeklyEarnings](repeating: WeeklyEarnings(JSON: [:])!, count: 7)
    @IBOutlet weak var buttonLeftArrow: UIButton!
    @IBOutlet weak var buttonRightArrow: UIButton!
    @IBOutlet weak var labelEarnings: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var labelWeekToDate: UILabel!
    @IBOutlet weak var labelLastRideAmount: UILabel!
    @IBOutlet weak var labelMonthToDate: UILabel!
    @IBOutlet weak var labelTotalToDate: UILabel!
    @IBOutlet weak var lblAvailability: UILabel!
    
    let bottomMarker = Bundle.main.loadNibNamed("BottomMarkerView", owner: self, options: nil)?.first as! BottomMakerView
    let marker = Bundle.main.loadNibNamed("PriceMarkerView", owner: self, options: nil)?.first as! PriceMakerView
    var presenterEarnings : EarningsPresenter!
    
    
    let currentDate = Date()
    var currentWeekDate : Date = Date().startOfWeek
    var currentWeekEndDate : Date!
    var currentMonthDate : Date!
    let timeZoneOffSetMiliSeconds = TimeZone.current.secondsFromGMT()*1000
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenterEarnings = EarningsPresenter(delegate: self)
        
        self.setUpChartView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.initilaseAvailabilityView()
        self.setUpNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.shadowView.addShadow()

    }
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle: "Earnings")
        
        self.customizeNavigationBackButton()
    }
    private func setUpChartView()
    {
        self.barChartView.noDataText = "No earnings in this week."
        self.barChartView.noDataFont = UIFont.getSanFranciscoMedium(withSize: 18)
        self.barChartView.noDataTextColor = UIColor.appDarkGrayColor()
        
        self.barChartView.fitBars = false
        self.barChartView.xAxis.drawAxisLineEnabled = false
        self.barChartView.xAxis.drawLabelsEnabled = false
        self.barChartView.xAxis.drawGridLinesEnabled = false
        self.barChartView.leftAxis.drawAxisLineEnabled = false
        self.barChartView.leftAxis.drawZeroLineEnabled = false
        self.barChartView.leftAxis.drawAxisLineEnabled = false
        self.barChartView.leftAxis.gridColor = UIColor(red: 224/255.0, green:  228/255.0, blue:  237/255.0, alpha:1.0)
        
        self.barChartView.rightAxis.drawGridLinesEnabled = false
        self.barChartView.rightAxis.drawZeroLineEnabled = false
        self.barChartView.rightAxis.drawAxisLineEnabled = false
        self.barChartView.leftAxis.labelFont = UIFont.getSanFranciscoMedium(withSize: 14)
        self.barChartView.leftAxis.labelTextColor = UIColor.appDarkGrayColor()
        self.barChartView.leftAxis.valueFormatter = YAxisValueFormatter()
        self.barChartView.rightAxis.drawAxisLineEnabled = false
        self.barChartView.rightAxis.drawLabelsEnabled = false
        
        self.barChartView.backgroundColor = UIColor.clear
        
        self.barChartView.scaleXEnabled = false
        self.barChartView.scaleYEnabled = false
        
        self.barChartView.chartDescription?.text = ""
        
        self.barChartView.legend.enabled = false
        
        self.bottomMarker.isHidden = true
        self.bottomMarker.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        self.barChartView.addSubview(self.bottomMarker)
        self.barChartView.marker = marker
        self.barChartView.delegate = self
        
        self.getCurrentEarningData()
    }
    private func getCurrentEarningData()
    {
        self.currentWeekDate = Date().startOfWeek
        self.currentWeekEndDate = Date().startOfWeek.addingTimeInterval(6*24*60*60)
        self.hideShowLeftRightArrow()
        self.setInitialDatesForEarnings()
         self.labelEarnings.text = "\(self.getWeekSpanTitle())"
        self.presenterEarnings.sendEarningsRequest(withDriverInfoModel: self.intialiseEarningsRequestModel())
    }
    fileprivate func hideShowLeftRightArrow()
    {
        self.buttonRightArrow.isHidden = self.currentDate.timeIntervalSince(self.currentWeekDate) < (7*24*60*60) ? true : false

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = AppConstants.DateConstants.DOB_FORMAT_FROM_SERVER

        let accountCreatedDate = dateFormatter.date(from: AppDelegate.sharedInstance.userInformation.createdAt!)

        self.buttonLeftArrow.isHidden = accountCreatedDate!.timeIntervalSince(self.currentWeekDate) > (7*24*60*60) ? true : false
    }
    
    private func getWeekSpanTitle()->String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd"
       return "\(dateFormatter.string(from: self.currentWeekDate)) - \(dateFormatter.string(from: self.currentWeekEndDate))"
    }
    fileprivate func setChartData()
    {
        var dataEntries: [BarChartDataEntry] = []
        let data = BarChartData()
         data.barWidth = 0.99
        for i in 0..<weeklyEarnings.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: weeklyEarnings[i])
            dataEntries.append(dataEntry)
        }

        
        let dataset = BarChartDataSet(values: dataEntries, label: "")
        dataset.colors = [UIColor.appThemeColor()]
        dataset.highlightColor = UIColor.appThemeColor()
        dataset.barBorderWidth = 1.0
        dataset.barBorderColor = UIColor(white: 0, alpha: 0.15)
        dataset.valueTextColor = UIColor.clear
        data.setValueFont(UIFont.getSanFranciscoSemibold(withSize: 12))
        
        data.addDataSet(dataset)
        self.barChartView.data = data
    }
    @IBAction func rightArrowTapped(_ sender: Any) {
        self.buttonRightArrow.isUserInteractionEnabled = false
        self.currentWeekDate = self.currentWeekDate.addingTimeInterval(7*24*60*60)
        self.currentWeekEndDate = self.currentWeekDate.addingTimeInterval(6*24*60*60)
        self.labelEarnings.text = "\(self.getWeekSpanTitle())"
        self.setInitialDatesForEarnings()
        self.presenterEarnings.sendEarningsRequest(withDriverInfoModel: self.intialiseEarningsRequestModel())
        self.buttonRightArrow.isUserInteractionEnabled = true
    }
    
    @IBAction func leftArrowTapped(_ sender: Any) {
        self.buttonLeftArrow.isUserInteractionEnabled = false
        self.currentWeekDate = self.currentWeekDate.addingTimeInterval(-7*24*60*60)
        self.currentWeekEndDate = self.currentWeekDate.addingTimeInterval(6*24*60*60)
        self.labelEarnings.text = "\(self.getWeekSpanTitle())"
        self.setInitialDatesForEarnings()
        self.presenterEarnings.sendEarningsRequest(withDriverInfoModel: self.intialiseEarningsRequestModel())
        self.buttonLeftArrow.isUserInteractionEnabled = true
        
    }
    @IBAction func availabilityViewTapped(_ sender: Any) {
        
        if AppUtility.isUserLogin()
        {
            if let driverLocation = LocationServiceManager.sharedInstance.currentLocation
            {
            if (AppDelegate.sharedInstance.userInformation.driverProfile?.available)!
            {
             self.presenterEarnings.sendDriverOfflineRequest(withDriverHomeViewRequestModel: DriverHomeViewRequestModel(available: false, ratio: 2.0, ongoingRate: 0.0, lat: Float(driverLocation.coordinate.latitude), lng: Float(driverLocation.coordinate.longitude)))
            }
            else
            {
               self.presenterEarnings.sendDriverOnlineRequest(withDriverHomeViewRequestModel: DriverHomeViewRequestModel(available: true, ratio: 2.0, ongoingRate: 0.0, lat: Float(driverLocation.coordinate.latitude), lng: Float(driverLocation.coordinate.longitude)))
            }
            }
        }
        else
        {
            self.logOutUser()
        }
    }
    
    fileprivate func setInitialDatesForEarnings()
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd-MM-yyyy ,EE"
        
        print("Week Day:\(dateFormatter.string(from: self.currentWeekDate))")
        print("Week End Day:\(dateFormatter.string(from: self.currentWeekDate.addingTimeInterval(6*24*60*60)))")
        
        dateFormatter.dateFormat = "01-MM-yyyy"
        let monthDayString = dateFormatter.string(from: self.currentWeekDate)
        self.currentMonthDate = dateFormatter.date(from: monthDayString)
        print(self.currentWeekDate.addingTimeInterval(7*24*60*60).timeIntervalSince1970)
        print("Months date:\(monthDayString)")
    }
    private func intialiseEarningsRequestModel()->EarningsScreenRequestModel
    {
        
        var objEarningsScreen = EarningsScreenRequestModel()
        objEarningsScreen.currentDate = Int(self.currentDate.timeIntervalSince1970*1000)+timeZoneOffSetMiliSeconds
        objEarningsScreen.toWeekDate = Int(self.currentWeekDate.addingTimeInterval(6*24*60*60).timeIntervalSince1970*1000)+timeZoneOffSetMiliSeconds
        objEarningsScreen.fromMonth = Int(self.currentMonthDate.timeIntervalSince1970*1000)+timeZoneOffSetMiliSeconds
        objEarningsScreen.fromWeekDate = Int(self.currentWeekDate.timeIntervalSince1970*1000)+timeZoneOffSetMiliSeconds
        return objEarningsScreen
    }
    fileprivate func getWeekIndexFromDate(_ utcDate:String)->Int
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = AppConstants.DateConstants.DOB_FORMAT_FROM_SERVER
   
        let localDate = dateFormatter.date(from: utcDate)
        return localDate!.indexOfWeekday
    }
    fileprivate func initilaseAvailabilityView(){
        
        if AppUtility.isUserLogin()
        {
            guard let _ =  AppDelegate.sharedInstance.userInformation.driverProfile?.available else
            {
                //self.viewDriverAvailability.backgroundColor = UIColor.appThemeColor()
                self.lblAvailability.backgroundColor = UIColor.white
                self.lblAvailability.textColor = UIColor.black
                self.lblAvailability.text = AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE
                return
            }
            
            // self.viewDriverAvailability.backgroundColor = (self.objUserProfile.driverProfile?.available)! ? UIColor.white : UIColor.appThemeColor()
            self.lblAvailability.backgroundColor = (AppDelegate.sharedInstance.userInformation.driverProfile?.available)! ? UIColor.appThemeColor() : UIColor.white
            self.lblAvailability.textColor = (AppDelegate.sharedInstance.userInformation.driverProfile?.available)! ? UIColor.white : UIColor.black
            self.lblAvailability.text = (AppDelegate.sharedInstance.userInformation.driverProfile?.available)! ? AppConstants.ScreenSpecificConstant.DriverHomeScreen.OFFLINE_TITLE : AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE
        }
        else
        {
            self.lblAvailability.backgroundColor = UIColor.white
            self.lblAvailability.textColor = UIColor.black
            self.lblAvailability.text = AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE
        }
    }
    fileprivate func setMarkersInfo(_ indexSelected:Int)
    {
        if let _ = self.weeklyEarningsDetail[indexSelected].date
        {
        self.bottomMarker.isHidden = false
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = AppConstants.DateConstants.DOB_FORMAT_FROM_SERVER
        
        let localDate = dateFormatter.date(from: self.weeklyEarningsDetail[indexSelected].date!)

        dateFormatter.dateFormat = "E, MMM dd"
        dateFormatter.string(from: localDate!)
        self.bottomMarker.labelDate.text = "\(dateFormatter.string(from: localDate!))"
        let markerText = String(format:"$%.2f",(self.weeklyEarningsDetail[indexSelected].totalEarning)!)
        self.marker.label?.text = markerText//"$\((self.weeklyEarningsDetail[indexSelected].totalEarning)!)"
        }

    }
}
extension EarningsViewController : EarningsScreenViewDelgate
{
    func didReceiveEarningsList(withResponseModel: EarningResponseModel) {
        
        self.labelWeekToDate.text       =       "$\(withResponseModel.weekToDateEarning ?? 0.0)"
        self.labelMonthToDate.text      =       "$\(withResponseModel.monthToDateEarning ?? 0.0)"
        self.labelTotalToDate.text      =       "$\(withResponseModel.totalEarningTillNow ?? 0.0)"
        self.labelLastRideAmount.text   =       "$\(withResponseModel.lastTripAmount ?? 0.0)"
        
        self.hideShowLeftRightArrow()
        if withResponseModel.weeklyEarnings?.count == 0
        {
            self.weeklyEarnings = []
            self.weeklyEarningsDetail = []
            self.barChartView.clear()
            self.bottomMarker.isHidden = true
            return
        }
        
        self.weeklyEarnings = [Double](repeating: 0.0, count: 7)
         self.weeklyEarningsDetail = [WeeklyEarnings](repeating: WeeklyEarnings(JSON: [:])!, count: 7)
        for dayEarning in withResponseModel.weeklyEarnings!
        {
            self.barChartView.clear()
            self.bottomMarker.isHidden = true
            let index =  self.getWeekIndexFromDate(dayEarning.date!)-1
            self.weeklyEarnings.remove(at: index)
            self.weeklyEarnings.insert(Double(dayEarning.totalEarning ?? 0.0), at: index)
            
            self.weeklyEarningsDetail.remove(at:index)
            self.weeklyEarningsDetail.insert(dayEarning, at: index)
            self.setChartData()
            self.barChartView.notifyDataSetChanged()
        }
    }
    
    func driverStatusUpdatedSuccessful(withResponseModel taxResponseModel:LoginResponseModel) {
        AppDelegate.sharedInstance.userInformation.driverProfile?.available = taxResponseModel.driverProfile?.available
        AppDelegate.sharedInstance.userInformation.canRide = taxResponseModel.canRide
        AppDelegate.sharedInstance.userInformation.canDrive = taxResponseModel.canDrive
        AppDelegate.sharedInstance.userInformation.driverProfile?.ongoingRate = taxResponseModel.driverProfile?.ongoingRate
        AppDelegate.sharedInstance.userInformation.driverProfile?.ratio = taxResponseModel.driverProfile?.ratio
        AppDelegate.sharedInstance.userInformation.saveUser()
        self.initilaseAvailabilityView()

        self.hideLoader()

    }
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
extension EarningsViewController : ChartViewDelegate
{
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let _ = self.weeklyEarningsDetail[Int(entry.x)].date
        {
        self.bottomMarker.frame = CGRect(x: highlight.xPx-50, y: (self.barChartView.frame.size.height)-(self.barChartView.frame.size.height)*0.12, width:  100, height: 30)
        self.barChartView.marker = self.marker
        self.currentWeekDate = self.currentWeekDate.startOfWeek
        self.currentWeekEndDate = self.currentWeekDate.addingTimeInterval(6*24*60*60)
        self.setInitialDatesForEarnings()
        self.setMarkersInfo(Int(entry.x))
        }
        else
        {
            self.bottomMarker.isHidden = true
            self.barChartView.marker = nil
        }
    }
}
extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}
extension Date {
    var startOfWeek: Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    var startOfMonth : Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year, .month, .day], from: self))!
    }
    var indexOfWeekday : Int {
        return Calendar.gregorian.component(.weekday, from: self)
    }
    var weekOfMonth : Int {
        return Calendar.gregorian.component(.weekOfMonth, from: self)
    }
    var currentMonth : String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        return dateFormatter.string(from: self)
    }
}
class YAxisValueFormatter: NSObject, IAxisValueFormatter {
    
    let numFormatter: NumberFormatter
    
    override init() {
        numFormatter = NumberFormatter()
        numFormatter.minimumFractionDigits = 0
        numFormatter.maximumFractionDigits = 0
        
        // if number is less than 1 add 0 before decimal
        numFormatter.minimumIntegerDigits = 1 // how many digits do want before decimal
        numFormatter.positivePrefix = "$ "
        numFormatter.negativePrefix = "$ "
    }
    
    /// Called when a value from an axis is formatted before being drawn.
    ///
    /// For performance reasons, avoid excessive calculations and memory allocations inside this method.
    ///
    /// - returns: The customized label that is drawn on the axis.
    /// - parameter value:           the value that is currently being drawn
    /// - parameter axis:            the axis that the value belongs to
    ///
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return numFormatter.string(from: NSNumber(floatLiteral: value))!
    }
}
