
//Notes:- This class is used as presenter for SignUpViewPresenter

import Foundation
import ObjectMapper

class SignUpViewPresenter: ResponseCallback{
    
//MARK:- SignUpViewPresenter local properties
    
    private weak var signUpViewDelegate          : SignUpViewDelegate?
    private weak var textFieldValidationDelegate : TextFieldValidationDelegate?
    private lazy var signUpBusinessLogic         : SignUpBusinessLogic = SignUpBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:SignUpViewDelegate){
        self.signUpViewDelegate = responseDelegate
    }
    
    convenience init(delegate responseDelegate:SignUpViewDelegate,textFieldValidationDelegate: TextFieldValidationDelegate){
        self.init(delegate: responseDelegate)
        self.textFieldValidationDelegate = textFieldValidationDelegate
    }

    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN, value: true)
        
        self.signUpViewDelegate?.hideLoader()
        
        if let response = responseObject as? LoginResponseModel{
            print(responseObject)
            self.signUpViewDelegate?.signUpSuccessful(withResponseModel: response)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.signUpViewDelegate?.hideLoader()
        self.signUpViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call signUp Api.
    
    func sendSignUpRequest(withSignUpViewRequestModel signUpForm1ViewRequestModel:SignUpViewRequestModel){
        
        guard self.validateOTPInput(withData: signUpForm1ViewRequestModel.phoneOTP) else {
            return
        }
        
        self.signUpViewDelegate?.showLoader()
        
        let signUpRequestModel : SignUpRequestModel = SignUpRequestModel.Builder()
            .setEmail(signUpForm1ViewRequestModel.email)
            .setPhoneNumber(signUpForm1ViewRequestModel.phoneNo)
            .setPassword(signUpForm1ViewRequestModel.password)
            .setFirstName(signUpForm1ViewRequestModel.firstName)
            .setLastName(signUpForm1ViewRequestModel.lastName)
            .setCountryCode(signUpForm1ViewRequestModel.countryCode)
            .setDiallingCode(signUpForm1ViewRequestModel.countryCodeExt)
            .setPhoneOTP(signUpForm1ViewRequestModel.phoneOTP)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).build()
        
        print(signUpRequestModel.requestBody)
        
        self.signUpBusinessLogic.performSignUp(withSignUpRequestModel: signUpRequestModel, presenterDelegate: self)
    }
    
//MARK:- Methods to validate Email SignUp Form 1 Input.
    
    func validateAllSignUpForm1Inputs(withData signUpModel:SignUpViewRequestModel) -> Bool {
        
        // All Fields Empty Validation
        guard self.validateAllFieldNotEmptyForSignUp(withData: signUpModel) else{
            //All fields are empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: "", forTextFields: .All)
            return false
        }
        
        //Email validations
        if let email = signUpModel.email, !email.isEmpty{
            
            guard signUpModel.email!.isValidEmailId() else{
                //pass not valid
                self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_EMAIL_ID, forTextFields: .EmailAddress)
                return false
            }
        }
        
        
        //Phone number validations
        guard !signUpModel.phoneNo!.isEmpty else {
            //password empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_PHONE_NUMBER, forTextFields: .PhoneNumber)
            return false
        }
        
        guard signUpModel.phoneNo!.isPhoneNumberValid() else{
            //pass not valid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_PHONE_NUMBER, forTextFields: .PhoneNumber)
            return false
        }
        
        //Password validations
        guard !signUpModel.password!.isEmpty else {
            //password empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_PASSWORD, forTextFields: .Password)
            return false
        }
        
        guard signUpModel.password!.isPasswordValid() else{
            //pass not valid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_PASSWORD, forTextFields: .Password)
            return false
        }
        
        return true
    }
    
    //MARK:- Methods to validate Email SignUp Form 2 Input.
    
    func validateAllSignUpForm2Inputs(withData signUpModel:SignUpViewRequestModel) -> Bool {
        
        //First Name validation
        guard !signUpModel.firstName.isEmpty else {
            //first name empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_FIRST_NAME, forTextFields: .FirstName)
            return false
        }
        
        return true
    }
    
    private func validateAllFieldNotEmptyForSignUp(withData signUpModel:SignUpViewRequestModel) -> Bool{
        guard !signUpModel.phoneNo.isEmptyString() || !signUpModel.password.isEmpty else {
            return false
        }
        return true
    }
    
    //MARK: Phone OTP Validations
    
    func validateOTPInput(withData otp:String) -> Bool {
        
        guard !otp.isEmptyString() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_OTP, forTextFields: .OTP)
            return false
        }
        
        guard otp.isOTPValid() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_OTP, forTextFields: .OTP)
            return false
        }
        
        return true
    }
}
