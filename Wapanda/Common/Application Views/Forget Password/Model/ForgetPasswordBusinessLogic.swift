//
//  ForgetPasswordBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class ForgetPasswordBusinessLogic {
    
    init(){
        print("ForgetPasswordBusinessLogic init \(self)")
    }
    deinit {
        print("ForgetPasswordBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform ForgetPassword With Valid Inputs constructed into a ForgetPasswordRequestModel
     
     - parameter inputData: Contains info for Forgot Password
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performForgetPassword(withForgetPasswordRequestModel ForgetPasswordRequestModel: ForgetPasswordRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForForgetPassword()
        ForgetPasswordAPIRequest().makeAPIRequest(withReqFormData: ForgetPasswordRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForForgetPassword() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.PHONE_NOT_REGISTERED, message: AppConstants.ErrorMessages.PHONE_NOT_REGISTERED)
        errorResolver.registerErrorCode(ErrorCodes.EMAIL_NOT_REGISTERED, message: AppConstants.ErrorMessages.EMAIL_NOT_REGISTERED)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message: AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        
        return errorResolver
    }
}
