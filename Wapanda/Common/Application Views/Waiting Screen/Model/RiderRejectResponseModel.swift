//
//  RiderRejectResponseModel.swift
//
//  Created by  on 21/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RiderRejectResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carType = "carType"
    static let endLoc = "endLoc"
    static let createdAt = "createdAt"
    static let closeBy = "closeBy"
    static let startLoc = "startLoc"
    static let riderId = "riderId"
    static let driverRate = "driverRate"
    static let success = "success"
    static let closed = "closed"
    static let id = "_id"
    static let estDistance = "estDistance"
    static let updatedAt = "updatedAt"
    static let driverId = "driverId"
    static let estPrice = "estPrice"
    static let estDuration = "estDuration"
  }

  // MARK: Properties
  public var carType: String?
  public var endLoc: EndLoc?
  public var createdAt: String?
  public var closeBy: String?
  public var startLoc: StartLoc?
  public var riderId: String?
  public var driverRate: Int?
  public var success: Bool? = false
  public var closed: Bool? = false
  public var id: String?
  public var estDistance: Int?
  public var updatedAt: String?
  public var driverId: String?
  public var estPrice: Float?
  public var estDuration: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    carType <- map[SerializationKeys.carType]
    endLoc <- map[SerializationKeys.endLoc]
    createdAt <- map[SerializationKeys.createdAt]
    closeBy <- map[SerializationKeys.closeBy]
    startLoc <- map[SerializationKeys.startLoc]
    riderId <- map[SerializationKeys.riderId]
    driverRate <- map[SerializationKeys.driverRate]
    success <- map[SerializationKeys.success]
    closed <- map[SerializationKeys.closed]
    id <- map[SerializationKeys.id]
    estDistance <- map[SerializationKeys.estDistance]
    updatedAt <- map[SerializationKeys.updatedAt]
    driverId <- map[SerializationKeys.driverId]
    estPrice <- map[SerializationKeys.estPrice]
    estDuration <- map[SerializationKeys.estDuration]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = endLoc { dictionary[SerializationKeys.endLoc] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = closeBy { dictionary[SerializationKeys.closeBy] = value }
    if let value = startLoc { dictionary[SerializationKeys.startLoc] = value.dictionaryRepresentation() }
    if let value = riderId { dictionary[SerializationKeys.riderId] = value }
    if let value = driverRate { dictionary[SerializationKeys.driverRate] = value }
    dictionary[SerializationKeys.success] = success
    dictionary[SerializationKeys.closed] = closed
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = estDistance { dictionary[SerializationKeys.estDistance] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    if let value = estPrice { dictionary[SerializationKeys.estPrice] = value }
    if let value = estDuration { dictionary[SerializationKeys.estDuration] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.endLoc = aDecoder.decodeObject(forKey: SerializationKeys.endLoc) as? EndLoc
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.closeBy = aDecoder.decodeObject(forKey: SerializationKeys.closeBy) as? String
    self.startLoc = aDecoder.decodeObject(forKey: SerializationKeys.startLoc) as? StartLoc
    self.riderId = aDecoder.decodeObject(forKey: SerializationKeys.riderId) as? String
    self.driverRate = aDecoder.decodeObject(forKey: SerializationKeys.driverRate) as? Int
    self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
    self.closed = aDecoder.decodeBool(forKey: SerializationKeys.closed)
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.estDistance = aDecoder.decodeObject(forKey: SerializationKeys.estDistance) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.driverId = aDecoder.decodeObject(forKey: SerializationKeys.driverId) as? String
    self.estPrice = aDecoder.decodeObject(forKey: SerializationKeys.estPrice) as? Float
    self.estDuration = aDecoder.decodeObject(forKey: SerializationKeys.estDuration) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(endLoc, forKey: SerializationKeys.endLoc)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(closeBy, forKey: SerializationKeys.closeBy)
    aCoder.encode(startLoc, forKey: SerializationKeys.startLoc)
    aCoder.encode(riderId, forKey: SerializationKeys.riderId)
    aCoder.encode(driverRate, forKey: SerializationKeys.driverRate)
    aCoder.encode(success, forKey: SerializationKeys.success)
    aCoder.encode(closed, forKey: SerializationKeys.closed)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(estDistance, forKey: SerializationKeys.estDistance)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(driverId, forKey: SerializationKeys.driverId)
    aCoder.encode(estPrice, forKey: SerializationKeys.estPrice)
    aCoder.encode(estDuration, forKey: SerializationKeys.estDuration)
  }

}
