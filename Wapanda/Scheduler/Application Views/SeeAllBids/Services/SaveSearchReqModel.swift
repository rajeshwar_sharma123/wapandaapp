import UIKit

class SaveSearchReqModel {
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var sortKey: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        sortKey = builder.sortKey
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var sortKey: String?
        
        /**
         This method is used for setting dest Locations
         
         - parameter destLoc: String parameter that is going to be set on destination locations
         
         - returns: returning Builder Object
         */
        func setDestLoc(_ destLoc: [Float]) -> Builder{
            if destLoc.count > 0 {
                requestBody["destLoc"] = destLoc as AnyObject
            }
            return self
        }
        
        
        /**
         This method is used for setting title
         
         - parameter title: String parameter that is going to be set on title
         
         - returns: returning Builder Object
         */
        func setTitle(_ title: String?) -> Builder{
            
            if let title = title {
                requestBody["title"] = title as AnyObject?
            }
            return self
        }
        
        /**
         This method is used for setting pickupDateTime
         
         - parameter pickupDateTime: String parameter that is going to be set on pickupDateTime
         
         - returns: returning Builder Object
         */
        func setPickupDateTime(_ pickupDateTime: String?) -> Builder {
            
            if let time = pickupDateTime {
                requestBody["pickupDateTime"] = time as AnyObject?
            }
            return self
        }
        
        /**
         This method is used for setting destination address
         
         - parameter address: String parameter that is going to be set on  destination address
         
         - returns: returning Builder Object
         */
        func setDestAddress(_ address: String?)->Builder{
            if let address = address {
                requestBody["destAddress"] = address as AnyObject?
            }
            return self
        }
        
        /**
         This method is used for setting fare
         
         - parameter fare: String parameter that is going to be set on fare
         
         - returns: returning Builder Object
         */
        func setFare(_ fare: Float?)->Builder{
            if let fare = fare {
                requestBody["fareAbove"] = fare as AnyObject?
            }
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of BidsListRequestModel
         and provide BidsListRequestModel object.
         
         -returns : VisitedStopRequestModel
         */
        func build() -> SaveSearchReqModel{
            return SaveSearchReqModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting VisitedStop end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return "/auctions/criteria"
    }
}
