import UIKit
import MapKit

protocol SearchBidViewDelegate {
    func searchBid(with parameters: BidRequestUserInput)
    func saveSearch(with parameters: BidRequestUserInput)
    func pickUpTimeClicked(delegate: PickupTimeViewControllerDelegate)
    func fareClicked(delegate: PickupFareViewControllerDelegate)
    func locationClicked(delegate: GooglePlaceSearchDelegate)
    func showClear()
    func hideClear()
}

class SearchBidView: UIView, UITextViewDelegate, PickupTimeViewControllerDelegate, PickupFareViewControllerDelegate, GooglePlaceSearchDelegate {

    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnSaveSearch: UIButton!
    @IBOutlet weak var fareTxtFld: UITextField!
    @IBOutlet weak var timeTxtFld: UITextField!
    @IBOutlet weak var locationTxtFld: UITextField!
    
    var selectedDate: String?
    var selectedPrice: Float?
    var selectedPlace: SelectedPlaceModel?
    
    var userInput = BidRequestUserInput()
    
    var delegate: SearchBidViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        let view = Bundle.main.loadNibNamed("SearchBidView", owner: self, options: nil)?[0] as! UIView
        addSubview(view)
        view.frame = self.bounds
    }
    
    func bindView(userInput: BidRequestUserInput) {
        
        self.userInput = userInput
        selectedPrice = userInput.fare
        selectedDate = userInput.pickupTimeUTC

        if let fare = userInput.fare {
            fareTxtFld.text = AppUtility.getFormattedPriceString(withPrice: Double(fare))
        }
        else {
            fareTxtFld.text = ""
        }
        if let pickupTime = userInput.pickupTimeUTC {
            timeTxtFld.text = AppUtility.getDateForFilter(dateStr: pickupTime)
        }
        else {
            timeTxtFld.text = ""
        }
        
        if let address = userInput.addressTitle {
            
            selectedPlace = SelectedPlaceModel()
            locationTxtFld.text = address
            selectedPlace!.title = address
            selectedPlace!.coordinate = CLLocationCoordinate2D()
            
            if let latitude = userInput.latitude {
                selectedPlace!.coordinate.latitude = Double(latitude)
            }
            if let longitude = userInput.longitude {
                selectedPlace!.coordinate.longitude = Double(longitude)
            }

        }
        else {
            selectedPlace = nil
            locationTxtFld.text = ""
        }
        setupSaveButton()
    }
    
    @IBAction func btnUpdateClicked(_ sender: UIButton) {
        delegate?.searchBid(with: getUpdatedUserInput())
    }
    
    @IBAction func btnSaveSearchClicked(_ sender: UIButton) {
        delegate?.saveSearch(with: getUpdatedUserInput())
    }
    
    func getUpdatedUserInput() -> BidRequestUserInput {
        
        var serachParam = BidRequestUserInput()
        
        if let dateString = selectedDate {
            let date = dateString.getDateFromZoneFormate()
            serachParam.pickupTime = date.timeIntervalSince1970 * 1000 //converted date in milliseconds
            serachParam.pickupTimeUTC = dateString
        }
        
        if let address = selectedPlace {
            serachParam.latitude = Double(address.coordinate.latitude)
            serachParam.longitude = Double(address.coordinate.longitude)
            serachParam.addressTitle = address.title
        }
        
        if let fare = selectedPrice {
            serachParam.fare = fare
        }
        return serachParam
    }
    
    @IBAction func fareClicked(_ sender: UIButton) {
        delegate?.fareClicked(delegate: self)
    }
    
    @IBAction func timeClicked(_ sender: UIButton) {
        delegate?.pickUpTimeClicked(delegate: self)
    }
    
    @IBAction func locationClicked(_ sender: UIButton) {
        delegate?.locationClicked(delegate: self)
    }
    
    //MARK: PickupTimeViewController Delegate method

    func updatePickupTime(_ date: String) {
        
        timeTxtFld.text = AppUtility.getDateForFilter(dateStr: date)
        selectedDate = date
        setupSaveButton()
    }
    
    //MARK: PickupFareViewController Delegate method
    func fareSelected(fare: Float) {
        selectedPrice = fare
        fareTxtFld.text = AppUtility.getFormattedPriceString(withPrice: Double(fare))
        setupSaveButton()
    }
    
    func placeSelected(withDetail place: SelectedPlaceModel){
        locationTxtFld.text = place.title
        selectedPlace = place
        setupSaveButton()
    }
    
    func placeSearchScreenDidDismiss(){
    }
    
    func setupSaveButton() {
        if selectedPlace != nil || selectedPrice != nil || selectedDate != nil {
            btnSaveSearch.backgroundColor = UIColor.appCounterBlueColor()
            btnUpdate.alpha = 1
            btnSaveSearch.isUserInteractionEnabled = true
            btnUpdate.isUserInteractionEnabled = true
            delegate?.showClear()
        }
        else {
            btnSaveSearch.backgroundColor = UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1)
            btnUpdate.alpha = 0.5
            btnSaveSearch.isUserInteractionEnabled = false
            btnUpdate.isUserInteractionEnabled = false
            delegate?.hideClear()
        }
    }
}
