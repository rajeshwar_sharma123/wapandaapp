
//Notes:- This class is used as presenter for GooglePlaceDetailViewPresenter

import Foundation
import ObjectMapper
import GooglePlaces

class GooglePlaceDetailViewPresenter: ResponseCallbackGeneral{
    
//MARK:- GooglePlaceDetailViewPresenter local properties
    
    private weak var googlePlaceDetailViewDelegate          : GooglePlaceDetailViewDelegate?
    private lazy var googlePlaceDetailBusinessLogic         : GooglePlaceDetailBusinessLogic = GooglePlaceDetailBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:GooglePlaceDetailViewDelegate){
        self.googlePlaceDetailViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:AnyObject>(responseObject : T){
        self.googlePlaceDetailViewDelegate?.hideLoader()
        
        if let place = responseObject as? GMSPlace{
            self.googlePlaceDetailViewDelegate?.placeDetailWithData(data: place)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.googlePlaceDetailViewDelegate?.hideLoader()
        
        self.googlePlaceDetailViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call GooglePlaceDetail Api.
    
    func sendGooglePlaceDetailRequest(withGooglePlaceDetailViewRequestModel googlePlaceDetailViewRequestModel:GooglePlaceDetailViewRequestModel){
        
        self.googlePlaceDetailViewDelegate?.showLoader()
        
        let requestModel = GooglePlaceDetailRequestModel.Builder()
                        .buildWithPlaceId(placeId: googlePlaceDetailViewRequestModel.placeId)
                        .buildWithPlaceClient(placeClient: GMSPlacesClient())
                        .build()
        
        self.googlePlaceDetailBusinessLogic.performGooglePlaceDetail(withGooglePlaceRequestModel: requestModel, presenterDelegate: self)
    }
}
