import UIKit
import ObjectMapper

class VehicleListResponseModel: Mappable {

    var carType: String?
    var estPrice: Double?
    var fairValues: FairValuesModel?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    required init?(map: Map){
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        carType <- map["driverRatio.cartype"]
        estPrice <- map["estPrice"]
        fairValues <- map["fairValues"]
 
    }
}
