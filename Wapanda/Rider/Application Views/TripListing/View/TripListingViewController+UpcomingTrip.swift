//
//  TripListingViewController+UpcomingTrip.swift
//  Wapanda
//
//  Created by Daffodil on 23/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import UIKit
extension TripListingViewController{
    
    
    func initialSetupForUpcomingTrip(){
        presenterhistory.sendUpcomingTripList(withData: createUpcomingTripListModel(0, status: ""))
        addPullToRefresh()
        tableView.registerTableViewCell(tableViewCell: UpComingTripCell.self)
        
        
    }
    
    // Action method called while doing pull to refresh
    
    func addPullToRefresh(){
        if segmentType == .History{
            refreshControl.removeFromSuperview()
            refreshControl = nil
            
        }else{
            // Add Refresh Control to Table View
            refreshControl = UIRefreshControl()
            if #available(iOS 10.0, *) {
                tableView.refreshControl = refreshControl
            } else {
                tableView.addSubview(refreshControl)
            }
            refreshControl.addTarget(self, action: #selector(refreshUpcomingTripData(_:)), for: .valueChanged)
        }
        
    }
    // Action method called while doing pull to refresh
    
    func refreshUpcomingTripData(_ refreshControl: UIRefreshControl){
        presenterhistory.sendUpcomingTripList(withData: createUpcomingTripListModel(0,status: "" ))
    }
   
    /**
     This method is used to get the UpcomingTripListModel
     
     -returns : UpcomingTripListModel
     */
    
    func createUpcomingTripListModel(_ skipNum: Int, status: String) -> UpcomingTripListModel {
        var upcomingTripListModel = UpcomingTripListModel()
        upcomingTripListModel.limit = 10
        upcomingTripListModel.skip = skipNum
        upcomingTripListModel.status = status
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
            upcomingTripListModel.userType = .Driver
        }else{
            upcomingTripListModel.userType = .Rider
        }
        return upcomingTripListModel
    }
    
    func navigateToBiddingUnderwayViewController(_ auctionId:String, riderFare: Double, isDriverSchedule: Bool){
        let biddingUnderwayVC = UIViewController.getViewController(BiddingUnderwayViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        biddingUnderwayVC.fromScheduleCheck = false
        biddingUnderwayVC.auctionId = auctionId
        biddingUnderwayVC.riderFare = riderFare
        biddingUnderwayVC.isDriverSchedule = isDriverSchedule
        biddingUnderwayVC.delegate = self
        self.navigationController?.pushViewController(biddingUnderwayVC, animated: true)
    }
    
    func navigateToDriverScheduleDetailViewController(tripItem: Trip){
        let driverScheduleVC = UIViewController.getViewController(DriverScheduleDetailViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        driverScheduleVC.tripItem = tripItem
        self.navigationController?.pushViewController(driverScheduleVC, animated: true)
    }
    
    func navigateToRiderScheduleDetailViewController(tripItem: Trip){
        let riderScheduleVC = UIViewController.getViewController(RiderScheduleDetailViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        riderScheduleVC.tripItem = tripItem
        riderScheduleVC.delegate = self
        self.navigationController?.pushViewController(riderScheduleVC, animated: true)
    }
    
    
    func updateUpcomingTripListInExtensionClass(withResponseModel upcomingTripList:UpcomingTripListResponseModel){
        self.refreshControl.endRefreshing()
        print(upcomingTripList)
        if let trip = self.upcomingTripListResponse , trip.hasNext!
        {
            let lastIndex = (self.upcomingTripListResponse?.items?.count)! // First Index of newly added items
            
            self.upcomingTripListResponse?.items?.append(contentsOf:upcomingTripList.items!)
            self.upcomingTripListResponse?.hasNext = upcomingTripList.hasNext
            
            //Index path of newly added items
            var rowsToAdd = [IndexPath]()
            for indexRow in lastIndex ..< (self.upcomingTripListResponse?.items?.count)!
            {
                rowsToAdd.append(IndexPath(row: indexRow, section: 0))
            }
            
            //Insert new added rows
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: rowsToAdd, with: .none)
            self.tableView.endUpdates()
        }else{
            self.upcomingTripListResponse = upcomingTripList
            tableView.reloadData()
            
        }
    }
    /**
     This method is use to handel UpcomingTripCell click action
     */
    func handelUpcomingTripCellAction(indexPath: IndexPath){
        if let data = self.upcomingTripListResponse?.items, data.count > 0{
            let item =  self.upcomingTripListResponse?.items?[indexPath.row]
            if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
                navigateToDriverScheduleDetailViewController(tripItem: item!)
                
            }else{
                navigateToRiderScheduleDetailViewController(tripItem: item!)
            }
        }
    }
    
}

extension String{
    func getStingSize(_ fontName: String, fontSize: CGFloat)-> CGSize{
        if let font = UIFont(name: fontName, size: fontSize)
        {
            let fontAttributes = [NSFontAttributeName: font]
            return self.size(attributes: fontAttributes)
        }
        return CGSize()
    }
    
}

extension TripListingViewController: BiddingUnderwayViewControllerDelegate{
    func updateUpcomingTripList() {
        upcomingTripListResponse = nil
        presenterhistory.sendUpcomingTripList(withData: createUpcomingTripListModel(0,status: ""))
    }
}


