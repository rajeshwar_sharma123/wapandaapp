
//Note :- This method is used for Handling Api Response

import ObjectMapper

protocol ResponseCallback:class {
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T)
    func servicesManagerError(error : ErrorModel)
}

