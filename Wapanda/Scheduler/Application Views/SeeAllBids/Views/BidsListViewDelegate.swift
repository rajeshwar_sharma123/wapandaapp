import UIKit

protocol BidsListViewDelegate: BaseViewProtocol {
    
    func refreshList(list: [AuctionItemModel])
    func searchSaved()
    func showErrorAlert(_ alertTitle: String, alertMessage: String)
    func driverStatusUpdatedSuccessful(reponse: LoginResponseModel) 
}
