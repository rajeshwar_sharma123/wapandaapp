//
//  LoginResponseModel.swift
//
//  Created by  on 8/30/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class LoginResponseModel:  NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let phoneVerified = "phoneVerified"
    static let profileImageFileId = "profileImageFileId"
    static let emailVerified = "emailVerified"
    static let email = "email"
    static let createdAt = "createdAt"
    static let riderProfile = "riderProfile"
    static let driverDocs = "driverDocs"
    static let countryCodeExt = "countryCodeExt"
    static let accountDisabled = "accountDisabled"
    static let id = "_id"
    static let countryCode = "countryCode"
    static let updatedAt = "updatedAt"
    static let phone = "phone"
    static let firstName = "firstName"
    static let lastName = "lastName"
    static let driverProfile = "driverProfile"
    static let canRide = "canRide"
    static let canDrive = "canDrive"
    static let emergencyContact = "emergencyContact"
    static let emergencyContactName = "emergencyContactName"
    static let emergencyContactImageId = "emergencyContactImageId"
    static let stripeProfile = "stripeProfile"
  }
  // MARK: Properties
  public var phoneVerified: Bool? = false
  public var profileImageFileId: String?
  public var emailVerified: Bool? = false
  public var email: String?
  public var createdAt: String?
  public var riderProfile: RiderProfile?
  public var driverDocs: DriverDocs?
  public var countryCodeExt: String?
  public var accountDisabled: Bool? = false
  public var id: String?
  public var countryCode: String?
  public var updatedAt: String?
  public var phone: String?
  public var firstName: String?
  public var lastName: String?
  public var driverProfile: DriverProfile?
  public var canRide: Bool? = false
  public var canDrive: Bool? = false
  public var emergencyContact: String?
  public var emergencyContactName: String?
  public var emergencyContactImageId: String?
  public var stripeProfile: StripeProfile?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    phoneVerified <- map[SerializationKeys.phoneVerified]
    profileImageFileId <- map[SerializationKeys.profileImageFileId]
    emailVerified <- map[SerializationKeys.emailVerified]
    email <- map[SerializationKeys.email]
    createdAt <- map[SerializationKeys.createdAt]
    riderProfile <- map[SerializationKeys.riderProfile]
    driverDocs <- map[SerializationKeys.driverDocs]
    countryCodeExt <- map[SerializationKeys.countryCodeExt]
    accountDisabled <- map[SerializationKeys.accountDisabled]
    id <- map[SerializationKeys.id]
    countryCode <- map[SerializationKeys.countryCode]
    updatedAt <- map[SerializationKeys.updatedAt]
    phone <- map[SerializationKeys.phone]
    firstName <- map[SerializationKeys.firstName]
    lastName <- map[SerializationKeys.lastName]
    driverProfile <- map[SerializationKeys.driverProfile]
    canRide <- map[SerializationKeys.canRide]
    canDrive <- map[SerializationKeys.canDrive]
    emergencyContact <- map[SerializationKeys.emergencyContact]
    emergencyContactName <- map[SerializationKeys.emergencyContactName]
    emergencyContactImageId <- map[SerializationKeys.emergencyContactImageId]
    stripeProfile <- map[SerializationKeys.stripeProfile]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.phoneVerified] = phoneVerified
    if let value = profileImageFileId { dictionary[SerializationKeys.profileImageFileId] = value }
    dictionary[SerializationKeys.emailVerified] = emailVerified
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = riderProfile { dictionary[SerializationKeys.riderProfile] = value.dictionaryRepresentation() }
    if let value = driverDocs { dictionary[SerializationKeys.driverDocs] = value.dictionaryRepresentation() }
    if let value = countryCodeExt { dictionary[SerializationKeys.countryCodeExt] = value }
    dictionary[SerializationKeys.accountDisabled] = accountDisabled
    dictionary[SerializationKeys.canDrive] = canDrive
    dictionary[SerializationKeys.canRide] = canRide
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = countryCode { dictionary[SerializationKeys.countryCode] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = driverProfile { dictionary[SerializationKeys.driverProfile] = value.dictionaryRepresentation() }
    if let value = emergencyContact { dictionary[SerializationKeys.emergencyContact] = value }
    if let value = emergencyContactName { dictionary[SerializationKeys.emergencyContactName] = value }
    if let value = emergencyContactImageId { dictionary[SerializationKeys.emergencyContactImageId] = value }
      if let value = stripeProfile { dictionary[SerializationKeys.stripeProfile] = value.dictionaryRepresentation() }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.phoneVerified = aDecoder.decodeObject(forKey: SerializationKeys.phoneVerified) as? Bool
    self.profileImageFileId = aDecoder.decodeObject(forKey: SerializationKeys.profileImageFileId) as? String
    self.emailVerified = aDecoder.decodeObject(forKey: SerializationKeys.emailVerified) as? Bool
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.riderProfile = aDecoder.decodeObject(forKey: SerializationKeys.riderProfile) as? RiderProfile
    self.driverDocs = aDecoder.decodeObject(forKey: SerializationKeys.driverDocs) as? DriverDocs
    self.countryCodeExt = aDecoder.decodeObject(forKey: SerializationKeys.countryCodeExt) as? String
    self.accountDisabled = aDecoder.decodeObject(forKey: SerializationKeys.accountDisabled) as? Bool
    self.canRide = aDecoder.decodeObject(forKey: SerializationKeys.canRide) as? Bool
    self.canDrive = aDecoder.decodeObject(forKey: SerializationKeys.canDrive) as? Bool
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.countryCode = aDecoder.decodeObject(forKey: SerializationKeys.countryCode) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.driverProfile = aDecoder.decodeObject(forKey: SerializationKeys.driverProfile) as? DriverProfile
    self.emergencyContact = aDecoder.decodeObject(forKey: SerializationKeys.emergencyContact) as? String
    self.emergencyContactName = aDecoder.decodeObject(forKey: SerializationKeys.emergencyContactName) as? String
    self.emergencyContactImageId = aDecoder.decodeObject(forKey: SerializationKeys.emergencyContactImageId) as? String
     self.stripeProfile = aDecoder.decodeObject(forKey: SerializationKeys.stripeProfile) as? StripeProfile
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(phoneVerified, forKey: SerializationKeys.phoneVerified)
    aCoder.encode(profileImageFileId, forKey: SerializationKeys.profileImageFileId)
    aCoder.encode(emailVerified, forKey: SerializationKeys.emailVerified)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(riderProfile, forKey: SerializationKeys.riderProfile)
    aCoder.encode(driverDocs, forKey: SerializationKeys.driverDocs)
    aCoder.encode(countryCodeExt, forKey: SerializationKeys.countryCodeExt)
    aCoder.encode(accountDisabled, forKey: SerializationKeys.accountDisabled)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(countryCode, forKey: SerializationKeys.countryCode)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(driverProfile, forKey: SerializationKeys.driverProfile)
    aCoder.encode(canDrive, forKey: SerializationKeys.canDrive)
    aCoder.encode(canRide, forKey: SerializationKeys.canRide)
    aCoder.encode(emergencyContact, forKey: SerializationKeys.emergencyContact)
    aCoder.encode(emergencyContactName, forKey: SerializationKeys.emergencyContactName)
    aCoder.encode(emergencyContactImageId, forKey: SerializationKeys.emergencyContactImageId)
    aCoder.encode(stripeProfile, forKey: SerializationKeys.stripeProfile)
  }
    //MARK:- Helper methods to retrive and save user information.
    
    func saveUser(){
        let archiver = NSKeyedArchiver.archivedData(withRootObject: self)
        
        UserDefaultUtility.saveObjectWithKey(archiver as AnyObject, key: AppConstants.UserInfoConstants.USER_INFO)
    }
    
    class func retriveUser()->LoginResponseModel?{
        if(UserDefaultUtility.objectAlreadyExist(AppConstants.UserInfoConstants.USER_INFO)){
            let data = UserDefaultUtility.retrievObjectWithKey(AppConstants.UserInfoConstants.USER_INFO) as! Data
            let unarchiver = NSKeyedUnarchiver.unarchiveObject(with: data) as! LoginResponseModel
            return unarchiver
        }
        return nil
    }
    
    
    class func deleteUserInformationObject(){
        UserDefaultUtility.removeObjectWithKey(AppConstants.UserInfoConstants.USER_INFO)
    }
    
}
