//
//  JMMarkSlider.swift
//  Wapanda
//
//  Created by Daffomac-23 on 9/1/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class JMMarkSlider: UISlider {
    var markColors = [UIColor]()
    var markTitles = [String]()
    var markWidth: CGFloat = 0.0
    var markPositions = [CGFloat]()
    var selectedBarColor: UIColor?
    var unselectedBarColor: UIColor?
    var handlerImage: UIImage = #imageLiteral(resourceName: "sliderHandler")
    var handlerColor: UIColor?
    var minimumSliderBaseValue: Float = 0.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Default configuration
        markColors = []
        markPositions = []
        markWidth = 1.0
        selectedBarColor = UIColor.sliderColor()
        unselectedBarColor = UIColor.sliderColor()
        value = 0.5
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Default configuration
        markColors = []
        markPositions = []
        markWidth = 1.0
        value = 0.5
        selectedBarColor = UIColor.sliderColor()
        unselectedBarColor = UIColor.sliderColor()        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.modifyMarkPositions(rect)
    }
    func modifyMarkPositions(_ rect:CGRect )
    {
        let innerRect: CGRect = rect.insetBy(dx: 0.0, dy: 0.0)
        UIGraphicsBeginImageContextWithOptions(innerRect.size, false, 0)
        
        let context: CGContext? = UIGraphicsGetCurrentContext()
        // Selected side
        context!.setLineCap(.round)
        context?.setLineWidth(12.0)
        context?.move(to: CGPoint(x: 6, y: innerRect.height / 2))
        context?.addLine(to: CGPoint(x: innerRect.size.width - 10, y: innerRect.height / 2))
        context?.setStrokeColor((selectedBarColor?.cgColor)!)
        context?.strokePath()
        
        
        let selectedSide: UIImage? = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero)
        // Set trips on selected side
        selectedSide?.draw(at: CGPoint(x: 0, y: 0))
        for i in 0..<markPositions.count {
            let position = markPositions[i] * innerRect.size.width / 100.0
            context?.beginPath()
            context?.move(to: CGPoint(x: CGFloat(position), y: innerRect.height / 2 - 7))
            // top left
            context?.addLine(to: CGPoint(x: CGFloat(position)-3, y: innerRect.height / 2 - 7 - 10))
            // mid right
            context?.addLine(to: CGPoint(x: CGFloat(position)+3, y: innerRect.height / 2 - 7 - 10))
            // bottom left
            context?.closePath()
            context?.setFillColor(markColors[i].cgColor)
            context?.fillPath()
            self.drawCabTitle(title: markTitles[i], textColor: self.markColors[i], inRect: CGRect(x: CGFloat(position), y:innerRect.height / 2 - 7 - 22 , width: 30, height: 10))
            context?.setLineWidth(markWidth)
            context?.move(to: CGPoint(x: CGFloat(position), y: innerRect.height / 2 - 5))
            context?.addLine(to: CGPoint(x: CGFloat(position), y: innerRect.height / 2 + 5))
            context?.setStrokeColor(markColors[i].cgColor)
            context?.strokePath()
            
        }
        context?.setLineWidth(markWidth)
        context?.move(to: CGPoint(x: CGFloat(innerRect.size.width / 2), y: rect.origin.y+5))
        context?.addLine(to: CGPoint(x: CGFloat(innerRect.size.width / 2), y: innerRect.height / 2 + 5))
        context?.setStrokeColor(UIColor.centerMarkColor().cgColor)
        context?.strokePath()
        
        let selectedStripSide: UIImage? = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero)
        context?.clear(rect)
        // Unselected side
        context!.setLineCap(.round)
        context?.setLineWidth(12.0)
        context?.move(to: CGPoint(x: 6, y: innerRect.height / 2))
        context?.addLine(to: CGPoint(x: innerRect.size.width - 10, y: innerRect.height / 2))
        context?.setStrokeColor((unselectedBarColor?.cgColor)!)
        context?.strokePath()
        
        
        let unselectedSide: UIImage? = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero)
        // Set trips on unselected side
        unselectedSide?.draw(at: CGPoint(x: 0, y: 0))
        for i in 0..<markPositions.count {
            let position = markPositions[i] * innerRect.size.width / 100.0
            
            context?.beginPath()
            context?.move(to: CGPoint(x: CGFloat(position), y: innerRect.height / 2 - 7))
            // top left
            context?.addLine(to: CGPoint(x: CGFloat(position)-3, y: innerRect.height / 2 - 7 - 10))
            // mid right
            context?.addLine(to: CGPoint(x: CGFloat(position)+3, y: innerRect.height / 2 - 7 - 10))
            // bottom left
            context?.closePath()
            context?.setFillColor(markColors[i].cgColor)
            context?.fillPath()
            self.drawCabTitle(title: markTitles[i], textColor: self.markColors[i], inRect: CGRect(x: CGFloat(position), y:innerRect.height / 2 - 7 - 22 , width: 30, height: 10))
            context?.setLineWidth(markWidth)
            context?.move(to: CGPoint(x: CGFloat(position), y: innerRect.height / 2 - 5))
            context?.addLine(to: CGPoint(x: CGFloat(position), y: innerRect.height / 2 + 5))
            context?.setStrokeColor(markColors[i].cgColor)
            context?.strokePath()
            
        }
        context?.setLineWidth(markWidth)
        context?.move(to: CGPoint(x: CGFloat(innerRect.size.width / 2), y: rect.origin.y+5))
        context?.addLine(to: CGPoint(x: CGFloat(innerRect.size.width / 2), y: innerRect.height / 2 + 5))
        context?.setStrokeColor(UIColor.centerMarkColor().cgColor)
        context?.strokePath()
        
        let unselectedStripSide: UIImage? = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero)
        context?.clear(rect)
        UIGraphicsEndImageContext()
        
        setMinimumTrackImage(selectedStripSide, for: .normal)
        setMaximumTrackImage(unselectedStripSide, for: .normal)
        if handlerImage != nil {
            setThumbImage(handlerImage, for: .normal)
        }
        else if handlerColor != nil {
            setThumbImage(UIImage(), for: .normal)
            thumbTintColor = handlerColor
        }

    }
    func getBaseValue(fromSliderValue sliderValue:Float,andMinimumValue minimumValue:Float)->Float
    {
        var baseValue = (sliderValue*2)*AppConstants.ScreenSpecificConstant.DriverHomeScreen.MULTIPLE_FACTOR + minimumValue
        if baseValue < 0
        {
            baseValue = 0.25
        }
        return self.roundValueToNearestQuater(baseValue.rounded(toPlaces: 2))
    }
    func getSliderValue(fromBaseValue baseValue:Float,andMinimumValue minimumValue:Float)->Float
    {
        let sliderValue = (baseValue-minimumValue)/AppConstants.ScreenSpecificConstant.DriverHomeScreen.MULTIPLE_FACTOR
        return sliderValue
    }
    func roundValueToNearestQuater(_ value:Float)->Float
    {
        let fractionNum = Float(value) / 0.25
        let roundedNum = Float(floor(fractionNum))
        return roundedNum * 0.25
    }
    
    func drawCabTitle(title:String,textColor:UIColor,inRect:CGRect){
        
        let textFont = UIFont.getSanFranciscoLight(withSize: 10)
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
           let stringSize = title.size(attributes: textFontAttributes)
        let centerRect = CGRect(x:inRect.origin.x - (stringSize.width / 2),
                                y:inRect.origin.y,
                                width:stringSize.width,
                                height:stringSize.height)
        title.draw(in: centerRect, withAttributes: textFontAttributes)
    }
    func setDriverText(_ price:Float)->String
    {
        return String(format:"YOUR PRICE : $%.2f",price)
    }
}
extension Float {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
    func roundValueToNearestQuater()->Float
    {
        let fractionNum = Float(self) / 0.25
        let roundedNum = Float(floor(fractionNum))
        return roundedNum * 0.25
    }
}
