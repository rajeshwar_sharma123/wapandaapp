//
//  TipViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 12/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Stripe
class TipViewController: BaseViewController {
    
    @IBOutlet weak var imgViewDriverImage: UIImageView!
    @IBOutlet weak var viewBgDriverImage: UIView!
    @IBOutlet weak var btnNoTip: UIButton!
    @IBOutlet weak var btnFifteenPerTip: UIButton!
    @IBOutlet weak var imgViewBrand: UIImageView!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var btnTwentyPerTip: UIButton!
    @IBOutlet weak var btnTwentyFivePerTip: UIButton!
    @IBOutlet weak var lblRideFareValue: UILabel!
    @IBOutlet weak var lblTipValue: UILabel!
    @IBOutlet weak var lblTotalBillValue: UILabel!
    var totalBillValue: Double!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblTipTitle: UILabel!
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var btnApplePay: UIButton!
    var viewRequestModel: TipViewRequestModel!
    var presenterTip : TipViewPresenter!
    var appplePayHandler = ApplePayHandler()
    var amountPayable = 0.0
    var presenterProcessPayment : ProcessPaymentViewPresenter!
    var selectedTipPercent = 0.0
    
    var isApplePayDefault = false
    var isProcessingApplePay = false
    @IBOutlet weak var imgViewRoute: UIImageView!
    var customerContext: STPCustomerContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenterTip = TipViewPresenter(delegate: self)
        self.presenterProcessPayment = ProcessPaymentViewPresenter(delegate:self)
        self.appplePayHandler.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(addPaymentScreenDidDismiss(_:)), name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.REFRESH_PAYMENT_OPTION), object: nil)
        
        self.viewCustomization()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setDefaultPayment(false)
        self.btnApplePay.isHidden = !appplePayHandler.applePayAvailable
    }
    
    func bind(withViewModel model: TipViewRequestModel){
        self.viewRequestModel = model
    }
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func viewCustomization(){
        //Set Driver
        self.setDriver()
        
        //Set Inital Tip
        self.setInitialTip()
        
        self.btnSubmit.setCornerCircular(10.0)
        self.btnApplePay.setCornerCircular(10.0)
        
        //Set Route Image
        let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: self.viewRequestModel.routeImageId))
        self.imgViewRoute.setImageWith(imageUrl!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
    }
    @objc fileprivate func addPaymentScreenDidDismiss(_ notification:NSNotification)
    {
        let isContextRefresh = (notification.object as! NSNumber).boolValue
        self.setDefaultPayment(isContextRefresh)
    }
    @objc fileprivate func setDefaultPayment(_ shouldLoadContext:Bool)
    {
               self.activityLoader.isHidden = false
        guard let _ = StripePaymentManager.shared.customer else {
            self.getDefaultCardFromNewContext()
            return
        }

        if !shouldLoadContext
        {
            if let defaultSource = StripePaymentManager.shared.customer?.defaultSource as? STPSource,defaultSource.type == .card
            {
                self.initialseDefaultCardView(defaultSource)
                
            }
            else
            {
                self.initialiseNoPaymentAddedView()
            }
        }
        else
        {
            self.getDefaultCardFromNewContext()
        }
        
    }
    private func getDefaultCardFromNewContext()
    {
        customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        customerContext.retrieveCustomer { (customer, error) in
            if error != nil
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: (error?.localizedDescription)!)
                self.activityLoader.isHidden = true
                return
            }
            StripePaymentManager.shared.customer = customer
            if let defaultSource = StripePaymentManager.shared.customer?.defaultSource as? STPSource,defaultSource.type == .card
            {
                self.initialseDefaultCardView(defaultSource)
            }
            else
            {
                self.initialiseNoPaymentAddedView()
            }
        }
        
    }
    private func initialseDefaultCardView(_ defaultCard : STPSource)
    {
        self.imgViewBrand.image = StripePaymentManager.shared.getBrandImage(basedOnBrandType: (defaultCard.cardDetails?.brand)!)
        self.lblCardNumber.text = defaultCard.cardDetails?.last4
         self.activityLoader.isHidden = true
    }
    private func initialiseNoPaymentAddedView()
    {
        self.imgViewBrand.image = StripePaymentManager.shared.getBrandImage(basedOnBrandType: .unknown)
        self.lblCardNumber.text = ""
        self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: "Please select a default payment method.")
         self.activityLoader.isHidden = true
    }

    private func setDriver(){
        
        //Image & Image Bg View Customization
        self.imgViewDriverImage.clipsToBounds = true
        self.viewBgDriverImage.clipsToBounds = true
        self.imgViewDriverImage.layer.cornerRadius = self.imgViewDriverImage.frame.width/2
        self.viewBgDriverImage.layer.cornerRadius = self.viewBgDriverImage.frame.width/2
        
        //Set Driver Image
        let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: self.viewRequestModel.driverImageId))
        self.imgViewDriverImage.setImageWith(imageUrl!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
        
        //Set Driver Name
        self.lblDriverName.text = self.viewRequestModel.driverName
    }
    
    private func setInitialTip(){
        var compTip = 0
        if let selectedTip = self.viewRequestModel.pushObjModel.trip?.payment?.compTipPercent
        {
            compTip = Int(selectedTip)
        }
        switch compTip {
        case 0:
            self.setTipOptionSelectedWithOption(sender: self.btnNoTip)
            break
        case 15:
            self.setTipOptionSelectedWithOption(sender: self.btnFifteenPerTip)
            break
        case 20:
            self.setTipOptionSelectedWithOption(sender: self.btnTwentyPerTip)
            break
        case 25:
            self.setTipOptionSelectedWithOption(sender: self.btnTwentyFivePerTip)
            break
        default:
            break
        }
        
    }
    
    private func setBill(){
        self.amountPayable = self.viewRequestModel.tripFare
        self.lblRideFareValue.text = AppUtility.getFormattedPriceString(withPrice: self.viewRequestModel.tripFare)
        self.lblTipValue.text = AppUtility.getFormattedPriceString(withPrice: self.viewRequestModel.tripFare*self.selectedTipPercent)
        let selectedPercent = self.selectedTipPercent*100
        self.lblTipTitle.text = String(format: "Tip (%d%%)",selectedPercent)
        self.lblTotalBillValue.text = AppUtility.getFormattedPriceString(withPrice: self.viewRequestModel.tripFare+self.viewRequestModel.tripFare*self.selectedTipPercent)
        self.totalBillValue = (self.viewRequestModel.tripFare! + self.viewRequestModel.tripFare*self.selectedTipPercent)
    }
    
    private func setTipOptionSelectedWithOption(sender: UIButton){
        switch sender {
        case self.btnNoTip:
            self.selectedTipPercent = 0.0
            self.setTipButtonSelected(sender: self.btnNoTip)
            self.setTipButtonDeselected(sender: self.btnFifteenPerTip)
            self.setTipButtonDeselected(sender: self.btnTwentyPerTip)
            self.setTipButtonDeselected(sender: self.btnTwentyFivePerTip)
            break
        case self.btnFifteenPerTip:
            self.selectedTipPercent = 0.15
            self.setTipButtonSelected(sender: self.btnFifteenPerTip)
            self.setTipButtonDeselected(sender: self.btnNoTip)
            self.setTipButtonDeselected(sender: self.btnTwentyPerTip)
            self.setTipButtonDeselected(sender: self.btnTwentyFivePerTip)
            break
        case self.btnTwentyPerTip:
            self.selectedTipPercent = 0.20
            self.setTipButtonSelected(sender: self.btnTwentyPerTip)
            self.setTipButtonDeselected(sender: self.btnFifteenPerTip)
            self.setTipButtonDeselected(sender: self.btnNoTip)
            self.setTipButtonDeselected(sender: self.btnTwentyFivePerTip)
            break
        case self.btnTwentyFivePerTip:
            self.selectedTipPercent = 0.25
            self.setTipButtonSelected(sender: self.btnTwentyFivePerTip)
            self.setTipButtonDeselected(sender: self.btnFifteenPerTip)
            self.setTipButtonDeselected(sender: self.btnTwentyPerTip)
            self.setTipButtonDeselected(sender: self.btnNoTip)
            break
        default:
            break
        }
        
        self.setBill()
    }
    
    private func setTipButtonSelected(sender: UIButton){
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.backgroundColor = UIColor.appThemeColor()
    }
    
    private func setTipButtonDeselected(sender: UIButton){
        sender.setTitleColor(UIColor.appDarkGrayColor(), for: .normal)
        sender.backgroundColor = UIColor.white
    }
    
    @IBAction func btnSubmitClick(_ sender: UIButton) {
      //  self.presenterTip.sendTipRequest(withTipViewRequestModel: TipViewRequestModel(driverName: self.viewRequestModel.driverName, driverImageId: self.viewRequestModel.driverImageId, tripId: self.viewRequestModel.tripId, tripFare: self.viewRequestModel.tripFare, routeImageId: self.viewRequestModel.routeImageId, pushObjModel: self.viewRequestModel.pushObjModel))
       self.presenterProcessPayment.sendAddTipRequest(withTipPercent: self.selectedTipPercent*100, andPaymentId: (self.viewRequestModel.pushObjModel.trip?.paymentId)!)
    }
    
    fileprivate func getDefaultSource(_ amountPayable:Double)
    {
        self.showLoader()
        let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        customerContext.retrieveCustomer { (customer, error) in
            
            if error != nil
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: (error?.localizedDescription)!)
                self.hideLoader()
                return
            }
            
            StripePaymentManager.shared.customer = customer
            if let defaultSource = StripePaymentManager.shared.customer?.defaultSource as? STPSource,defaultSource.type == .card
            {
                self.initiatePayment(defaultSource,amountPayable: amountPayable)
               // self.createSourceFromCard(defaultCard)
               // self.processPayment(defaultCard.stripeID)
            }
            else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: "Please select a default payment method.")
                self.hideLoader()
            }
        }
        
    }
    
//    private func createSourceFromCard(_ defaultCard :STPSource)
//    {
//        
//            let sourceParams = STPSourceParams.cardParams(
//                withCard: STPCardParams())
//            sourceParams.redirect = ["return_url":"wapandaapp://stripe-redirect"]
//
//            STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
//                if let errorObj = error {
//                    self.hideLoader()
//                    self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: errorObj.localizedDescription)
//                    return
//                }
//              self.initiatePayment(source!)
//            }
//    }
    private func initiatePayment(_ defaultSource :STPSource,amountPayable:Double)
    {
        if defaultSource.cardDetails?.threeDSecure == .required
        {
            self.initiale3DSecureFlow(defaultSource,withAmountPayable:amountPayable)
        }
        else if defaultSource.status == .chargeable
        {
            self.processPayment("")
        }
    }
    private func initiale3DSecureFlow(_ defaultSource :STPSource,withAmountPayable:Double)
    {
        let amount =  NSDecimalNumber(string:String(format:"%.2f",withAmountPayable))
        let sourceParams = STPSourceParams.threeDSecureParams(withAmount: UInt( amount.multiplying(by: 100)), currency: "usd", returnURL:"wapandaapp://stripe-redirect" , card: defaultSource.stripeID)
      
        STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
            if let errorObj = error {
                self.hideLoader()
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: errorObj.localizedDescription)
                return
            }
            let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
            customerContext.attachSource(toCustomer: source!, completion: { (error) in
            let redirectContext = STPRedirectContext(source: source!) { (sourceID, clientSecret, error) in
                
                STPAPIClient.shared().retrieveSource(withId: sourceID, clientSecret: clientSecret, completion: { (src, error) in
                    
                    if let _ = error    { self.hideLoader() }
                    else                {
                        self.checkSourceStatus(src!,orginalSource:source!)
                    }
                
                })
            }
            redirectContext?.startSafariAppRedirectFlow()
            })
        }
        
    }
    private func checkSourceStatus(_ source:STPSource,orginalSource:STPSource)
    {
        switch source.status {
        case .chargeable:
            self.processPayment(source.stripeID)
            break
        case .failed:
            self.hideLoader()
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PAYMENT_AUTH_FAILED)
            break
        default:
             self.processPayment(source.stripeID)
            self.hideLoader()
            break
        }
    
    }
    fileprivate func processPayment(_ sourceId:String)
    {
        self.hideLoader()
        self.presenterProcessPayment.sendProcessPaymentRequest(withProcessPaymentViewRequestModel: ProcessPaymentViewRequestModel(paymentId: self.viewRequestModel.pushObjModel.trip?.paymentId,sourceId:sourceId))
    }
    @IBAction func btnTipClick(_ sender: UIButton) {
        self.setTipOptionSelectedWithOption(sender: sender)
    }
    @IBAction func driverProfileImageTapped(_ sender: Any) {
        self.navigateToDriverRatingList()
    }
    /// This method handle click on add payment button
    ///
    /// - Parameter sender: button reference
    @IBAction func btnAddPaymentMethodClick(_ sender: Any) {
        
        if let _ = StripePaymentManager.shared.customer?.defaultSource
        {
            
            let paymentVC = UIViewController.getViewController(AddPaymentScreenViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            let navigationController = UINavigationController(rootViewController: paymentVC)
            UIApplication.shared.visibleViewController?.present(navigationController, animated: true)
        }
        else
        {
            let paymentMethodVC = UIViewController.getViewController(PaymentMethodViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            let navigationController = UINavigationController(rootViewController: paymentMethodVC)
            UIApplication.shared.visibleViewController?.present(navigationController, animated: true)
        }
    }
    //MARK:Navigation
    private func navigateToDriverRatingList()
    {
        let driverRatingVC =  UIViewController.getViewController(DriverRatingViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        driverRatingVC.driverInfoModel = self.getDriverInfoModel()
        self.present(driverRatingVC, animated: true, completion: {})
    }
    private func getDriverInfoModel() -> DriverInfoResponseModel
    {
        let driverInfoModel = DriverInfoResponseModel(JSON: [:])
        driverInfoModel?.firstName = "\(self.viewRequestModel.pushObjModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")"
        driverInfoModel?.id = "\(self.viewRequestModel.pushObjModel.trip?.driver?.id ?? "")"
        driverInfoModel?.profileImageFileId = "\(self.viewRequestModel.pushObjModel.trip?.driver?.profileImageFileId ?? "")"
        driverInfoModel?.driverProfile = self.viewRequestModel.pushObjModel.trip?.driver?.driverProfile
        driverInfoModel?.cabInfo = Cab(JSON: (self.viewRequestModel.pushObjModel.trip?.vehicle?.dictionaryRepresentation())!)
        return driverInfoModel!
    }
    
    @IBAction func btnApplePayClick(_ sender: Any) {
        self.isApplePayDefault = true
        self.presenterProcessPayment.sendAddTipRequest(withTipPercent: self.selectedTipPercent*100, andPaymentId: (self.viewRequestModel.pushObjModel.trip?.paymentId)!)
    }
}

//MARK: BaseViewProtocol Methods
extension TipViewController : TipViewDelegate{
    func tipSubmittedSucessfully(){
        self.dismiss(animated: true) {
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }
    }
    
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        if self.isProcessingApplePay{
            self.appplePayHandler.onPaymentProcessingError()
        }
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
extension TipViewController:ApplePayDelegate{
    func applePayPaymentSuccess()
    {
       
    }
    func applePayPaymentFailure()
    {
    
    }
    func processPaymentWithSourceId(id: String)
    {
            self.isProcessingApplePay = true
            self.processPayment(id)
    }
}
extension TipViewController:ProcessPaymentViewDelegate{
    func processPaymentSuccess(withResponseModel data: ProcessPaymentResponseModel)
    {
        if isApplePayDefault{
            self.appplePayHandler.onPaymentProcessingSuccess()
        }
        
        UIApplication.shared.visibleViewController?.dismiss(animated: true, completion: nil)
        AppInitialViewHandler.sharedInstance.setupInitialViewController()
    }
    func addTipSuccess(withResponseModel data: AddTipResponseModel)
    {
        self.amountPayable = Double(data.amountPayable ?? 0)
        if isApplePayDefault
        {
            self.appplePayHandler.handleApplePayRequestWithItem(items: [ApplePayItemModel(label: "Fare", price:self.amountPayable)])
        }
        else
        {
            self.getDefaultSource(self.amountPayable)
        }
        
    }
}
extension TipViewController:AddPaymentViewDelgate{
    func setDefaultCard() {
        self.getDefaultSource(self.amountPayable)
    }
}
