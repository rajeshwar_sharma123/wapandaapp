//
//  DriverInfo+CounterBid.swift
//  Wapanda
//
//  Created by daffomac-31 on 15/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

//MARK: Counter Bid View

extension DriverInfoViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    //Helper Methods
    
    func initialisePickerView()
    {
        self.riderCounteredPrice = self.rideInfo.rideEstimatedFare
        self.prepareCounterPriceArray()
        self.setCounteredPriceWithSelectedComponentIndex(index: self.getIndexForEstimatedFare(fare: self.riderCounteredPrice))
        self.pickerCounterPrice.delegate = self
        self.pickerCounterPrice.dataSource = self
        
        if self.riderCounteredPrice > 0{
            self.pickerCounterPrice.selectRow(self.getIndexForEstimatedFare(fare: self.riderCounteredPrice), inComponent: 0, animated: true)
        }
        else{
            self.pickerCounterPrice.selectRow(0, inComponent: 0, animated: true)
        }
        
        self.initCounterBidView()
        self.removeSelectorFromPickerView()
        self.updateBidPriceStatus()
    }
    
    func prepareCounterPriceArray(){
        var price = self.rideInfo.fareValue!.fairValues!.minimumPrice!
        
        while Double(price) <= self.rideInfo.rideEstimatedFare!{
            self.arrCounterBidPrices.append(Double(price))
            price = price + rideInfo.fareValue!.moneyview!.increment!
        }
        
//        while (Double(price + rideInfo.fareValue!.moneyview!.increment!)) <= self.rideInfo.rideEstimatedFare!{
//            price = price + rideInfo.fareValue!.moneyview!.increment!
//            self.arrCounterBidPrices.append(Double(price))
//        }
        
        //In case nothing prepared to show in the list
        if self.arrCounterBidPrices.count == 0{
            self.arrCounterBidPrices.append(Double(self.rideInfo.rideEstimatedFare!))
        }
    }
    
    func getIndexForEstimatedFare(fare: Double)->Int{
        
        for (index, price) in self.arrCounterBidPrices.enumerated(){
            if fare == price{
                return index
            }
        }
        
        return 0
    }
    
    func setCounteredPriceWithSelectedComponentIndex(index: Int){
        let stringValue = String(format:"%0.2f", self.arrCounterBidPrices[index])
        
        if let myDouble = NumberFormatter().number(from: stringValue)?.doubleValue {
            self.riderCounteredPrice = myDouble
            if self.rideInfo.rideEstimatedFare == self.riderCounteredPrice
            {
                self.letsGoButton.backgroundColor = UIColor.appCounterGreyColor()
                self.letsGoButton.isUserInteractionEnabled = false
            }
            else
            {
                self.letsGoButton.backgroundColor = UIColor.appCounterBlueColor()
                self.letsGoButton.isUserInteractionEnabled = true
            }
        }
        
    }
    
    func initCounterBidView(){
        
        //Driver Name
        self.lblDriverNameCounterBid.text = (self.driverInfo?.firstName ?? "").capitalizeWordsInSentence() + " " + (self.driverInfo?.lastName ?? "").capitalizeWordsInSentence()
        //Company Name
        self.lblCompanyNameCounterBid.text  = (self.rideInfo.cabInfo?.companyName ?? "")
        //Make & Model
        self.lblMakeModelCounterBid.text = (self.rideInfo.cabInfo?.make?.capitalizeWordsInSentence() ?? "") + " " + (self.rideInfo.cabInfo?.model?.capitalizeWordsInSentence() ?? "")
        //Rating
        if let rating = self.driverInfo?.driverProfile?.averageRating{
            self.lblRatingCounterBid.text = String(describing: rating)
        }
        //Star Image Tint
        self.imgViewRatingCounterBid.getImageViewWithImageTintColor(color: UIColor.appDarkGrayColor())
        if self.rideInfo.rideEstimatedFare == self.riderCounteredPrice
        {
            self.letsGoButton.backgroundColor = UIColor.appCounterGreyColor()
            self.letsGoButton.isUserInteractionEnabled = false
        }
        else
        {
            self.letsGoButton.backgroundColor = UIColor.appCounterBlueColor()
            self.letsGoButton.isUserInteractionEnabled = true
        }
    }
    
    private func removeSelectorFromPickerView()
    {
        for view: UIView in pickerCounterPrice.subviews {
            if view.bounds.size.height < 2.0 {
                view.backgroundColor = UIColor.clear
            }
        }
    }
    
    /// This method handle click on lets go on counter price
    ///
    /// - Parameter sender: button reference
    @IBAction func btnLetsGoFromCounterScreenClick(_ sender: Any) {
        self.bidType = .COUNTER
        let requestModel = CreateBidViewRequestModel(latFrom: rideDetailModel.latFrom, lngFrom: rideDetailModel.lngFrom, latTo: rideDetailModel.latTo, lngTo: rideDetailModel.lngTo, driverId: self.driverInfo!.id, riderId: AppDelegate.sharedInstance.userInformation.id, estDistance: rideInfo.rideEstimatedDistance, estDuration: rideInfo.rideEstimatedTime, startTime: Date().getUTCFormateDate(), riderCounterPrice: self.riderCounteredPrice,fromAddress:self.rideDetailModel.sourceAddress,toAddress:self.rideDetailModel.destinationAddress)
        self.presenterCreateBid.sendCreateBidRequest(withCreateBidViewRequestModel: requestModel)
    }

    func updateBidPriceStatus(){
        if (self.riderCounteredPrice > Double(self.rideInfo.fareValue!.fairValues!.fairPriceLow!)) && (self.riderCounteredPrice < Double(self.rideInfo.fareValue!.fairValues!.fairPriceHigh!)){
            self.lblBidStatus.text = AppConstants.ScreenSpecificConstant.CounterBidScreen.BID_LOOKS_GOOD_MESSAGE
            self.lblBidStatus.textColor = UIColor.driverRatingGreenStatus()
        }
        else if self.riderCounteredPrice >= Double(self.rideInfo.fareValue!.fairValues!.fairPriceHigh!){
            self.lblBidStatus.text = AppConstants.ScreenSpecificConstant.CounterBidScreen.BID_HIGH_MESSAGE
            self.lblBidStatus.textColor = UIColor.appDarkThemeColor()
        }
        else {
            self.lblBidStatus.text = AppConstants.ScreenSpecificConstant.CounterBidScreen.BID_LOW_MESSAGE
            self.lblBidStatus.textColor = UIColor.appCounterGreyColor()
        }
    }
    
    //MARK: Picker View Data Source & Delegate
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrCounterBidPrices.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat{
        return 40.0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.setCounteredPriceWithSelectedComponentIndex(index: row)
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: String(format:"$%0.2f",self.arrCounterBidPrices[row]), attributes: [NSForegroundColorAttributeName:UIColor.appDarkGrayColor(), NSFontAttributeName: UIFont.getSanFranciscoMedium(withSize: 32.0)])
    }
}
