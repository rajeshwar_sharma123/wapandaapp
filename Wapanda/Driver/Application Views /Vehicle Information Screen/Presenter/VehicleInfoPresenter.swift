//
//  VehicleInfoPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper




class VehicleInfoPresenter: ResponseCallback{
    
    //MARK:- VehicleInfoPresenter local properties
    private weak var vehicleInfoInfoViewDelegate             : VehicleInfoScreenViewDelgate?
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?
    private lazy var vehicleInfoInfoBusinessLogic         : VehicleInfoBusinessLogic = VehicleInfoBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:VehicleInfoScreenViewDelgate) {
        self.vehicleInfoInfoViewDelegate = responseDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.vehicleInfoInfoViewDelegate?.hideLoader()
        
        if responseObject is MakesResponseModel{
            self.vehicleInfoInfoViewDelegate?.makesFetchedSuccessfully(withResponseModel: responseObject as! MakesResponseModel)
            
        }
        
        if responseObject is YearResponseModel{
            self.vehicleInfoInfoViewDelegate?.yearsFetchedSuccessfully(withResponseModel: responseObject as! YearResponseModel)
            
        }
        
        if responseObject is ModelsResponseModel{
            self.vehicleInfoInfoViewDelegate?.modelsFetchedSuccessfully(withResponseModel: responseObject as! ModelsResponseModel)
            
        }
        
        
        if responseObject is AllVehicleTypeResponseModel{
            self.vehicleInfoInfoViewDelegate?.allVehicleTypesFetchedSuccessfully(withResponseModel:responseObject as! AllVehicleTypeResponseModel)
        }
        
        if responseObject is VehicleDoc{
            self.vehicleInfoInfoViewDelegate?.vehicleInfoSubmittedSuccessfully(withResponseModel: responseObject as! VehicleDoc)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.vehicleInfoInfoViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.vehicleInfoInfoViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendVehicleInfoSumbitRequest(withData vehicleInfoScreenRequestModel:VehicleInfoScreenRequestModel) -> Bool{
        
        
        guard self.validateInput(withData: vehicleInfoScreenRequestModel) else{
            self.vehicleInfoInfoViewDelegate?.hideLoader()
            return false
        }
        
        self.vehicleInfoInfoViewDelegate?.showLoader()
        
        var vehicleInfoRequestModel : VehicleInfoRequestModel!
        
        
        vehicleInfoRequestModel = VehicleInfoRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setMake(vehicleInfoScreenRequestModel.make).setCompanyName(vehicleInfoScreenRequestModel.companyName).setCarPlateNumber(vehicleInfoScreenRequestModel.carPlateNumber).setCarType(vehicleInfoScreenRequestModel.carTypeId).setModel(vehicleInfoScreenRequestModel.model).setYear(vehicleInfoScreenRequestModel.year).setUserId(AppDelegate.sharedInstance.userInformation.id!).build()

        self.vehicleInfoInfoBusinessLogic.performVehicleInfoSubmitRequest(withVehicleInfoRequestModel: vehicleInfoRequestModel, presenterDelegate: self)
        return true
    }
    
    /**
     This method is used to make Car Make List request to business layer with valid Request model
     - returns : Void
     */
    func getVehicleInfoList(withModel vehicleRequest: VehicleInfoScreenRequestModel) -> Void{
        
        self.vehicleInfoInfoViewDelegate?.showLoader()
        
        let carMakeRequestModel : CarMakeRequestModel = CarMakeRequestModel.Builder().setType(vehicleRequest.type).setYear(vehicleRequest.year).setMake(vehicleRequest.make).build()
        
       // self.vehicleInfoInfoBusinessLogic.performCarMakeRequest(withCarMakeRequestModel: carMakeRequestModel, presenterDelegate: self)
        self.vehicleInfoInfoBusinessLogic.performVehicleInfoRequest(withRequestModel: carMakeRequestModel, presenterDelegate: self)
    }
    
    
    
   
    /**
     This method is used to make Car Make List request to business layer with valid Request model
     - returns : Void
     */
    func getAllVehicleTypes() -> Void{
        
       self.vehicleInfoInfoViewDelegate?.showLoader()
        
        let vehicleTypesRequestModel : VehicleTypeRequestModel = VehicleTypeRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").setLimit(10).setSkip(0).setSort("vehicleType").setOrder(1).build()
        
        self.vehicleInfoInfoBusinessLogic.performAllVehicleTypeRequest(withCarMakeRequestModel: vehicleTypesRequestModel, presenterDelegate: self)
    }
    
    private func readJson(_ fileName:String) -> [String: Any] {
        do {
            if let file = Bundle.main.url(forResource: fileName, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData vehicleInfoScreenRequestModel:VehicleInfoScreenRequestModel) -> Bool {
        
        var isValid = true
        if (vehicleInfoScreenRequestModel.year == 0) {
            //Year empty
            self.vehicleInfoInfoViewDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_YEAR, forTextFields: .Year)
            isValid = false
        }
        if (vehicleInfoScreenRequestModel.carPlateNumber.isEmptyString()) {
            //Car Plate Number empty
            self.vehicleInfoInfoViewDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_PLATE_NUMBER, forTextFields: .PlateNumber)
            isValid = false
        }
        if (vehicleInfoScreenRequestModel.carType.isEmptyString()) {
            //Car Type Number empty
            self.vehicleInfoInfoViewDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_TYPE, forTextFields: .CarType)
            isValid = false
        }
        if (vehicleInfoScreenRequestModel.companyName.isEmptyString()) {
            //Company Name empty
            self.vehicleInfoInfoViewDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_COMPANY, forTextFields: .Company)
            isValid = false
        }
        if (vehicleInfoScreenRequestModel.make.isEmptyString()) {
            //Make empty
            self.vehicleInfoInfoViewDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_MAKE, forTextFields: .Make)
            isValid = false
        }
        if (vehicleInfoScreenRequestModel.model.isEmptyString()) {
            //Model empty
            self.vehicleInfoInfoViewDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_MODEL, forTextFields: .Model)
            isValid = false
        }
        
        
        if (vehicleInfoScreenRequestModel.carPlateNumber.count > 8) {
            //Car Plate Number Length validation
            self.vehicleInfoInfoViewDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_CAR_PLATE_NUMBER, forTextFields: .PlateNumber)
            isValid = false
        }
        return isValid
    }
}
