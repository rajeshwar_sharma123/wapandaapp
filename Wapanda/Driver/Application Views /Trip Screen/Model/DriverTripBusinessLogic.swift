//
//  DriverTripBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class DriverTripBusinessLogic {
    
    init(){
        print("DriverTripBusinessLogic init \(self)")
    }
    deinit {
        print("DriverTripBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform DriverTrip With Valid Inputs constructed into a DriverTripRequestModel
     
     - parameter inputData: Contains info for Driver Start Trip
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performStartRideRequest(withDriverTripRequestModel DriverTripRequestModel: DriverTripRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForDriverStartTrip()
        DriverTripAPIRequest().makeAPIRequest(withReqFormData: DriverTripRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for perform End Trip With Valid Inputs constructed into a DriverTripRequestModel
     
     - parameter inputData: Contains info for Driver End Trip
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performEndRideRequest(withDriverTripRequestModel DriverTripRequestModel: DriverTripRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForDriverEndTrip()
        DriverTripAPIRequest().makeEndTripAPIRequest(withReqFormData: DriverTripRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    /**
     This method is used for perform Cancel With Valid Inputs constructed into a CancelRequestModel
     
     - parameter inputData: Contains info for Cancel Trip
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performCancelRequest(withCancelRequestModel CancelRequestModel: CancelRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForDriverCancelTrip()
        CancelAPIRequest().makeAPIRequest(withReqFormData: CancelRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
        /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForDriverCancelTrip() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_RIDER, message  : AppConstants.ErrorMessages.INVALID_RIDER)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_DRIVER, message: AppConstants.ErrorMessages.INVALID_DRIVER)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_CANCELLED, message: AppConstants.ErrorMessages.ALREADY_CANCELLED)
        return errorResolver
    }
    private func registerErrorForDriverEndTrip() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_RIDER, message  : AppConstants.ErrorMessages.INVALID_RIDER)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_AUTH, message  : AppConstants.ErrorMessages.INVALID_AUTH)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_DRIVER, message: AppConstants.ErrorMessages.INVALID_DRIVER)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_CANCELLED, message: AppConstants.ErrorMessages.ALREADY_CANCELLED)
         errorResolver.registerErrorCode(ErrorCodes.INVALID_TRIP_STATUS, message: AppConstants.ErrorMessages.INVALID_TRIP_STATUS)
        return errorResolver
    }
    private func registerErrorForDriverStartTrip() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_AUTH, message  : AppConstants.ErrorMessages.INVALID_AUTH)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_TRIP_STATUS, message: AppConstants.ErrorMessages.INVALID_TRIP_STATUS)
        return errorResolver
    }
}
