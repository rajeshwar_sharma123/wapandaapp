
//Notes:- This protocol is used as a interface which is used by SendEmergencyAlertPresenter to tranfer info to SendEmergencyAlertViewController

protocol SendEmergencyAlertViewDelegate:BaseViewProtocol {
    func sentEmergencyAlertSucessfully()
}
