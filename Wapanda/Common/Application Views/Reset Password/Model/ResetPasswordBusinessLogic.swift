//
//  ResetPasswordBusinessLogic.swift
//  Wapanda
//
//  Created by Daffodil on 21/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation


class ResetPasswordBusinessLogic {
    
    /**
     This method is used for perform password change request
     
     - parameter inputData: Contains password info
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performPasswordChangeRequest(withResetPasswordRequestModel resetRequestModel: ResetPasswordRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests()
        
        ResetPasswordAPIRequest().makeAPIRequest(withReqFormData: resetRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForResetPassword() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.INVALID_OLD_PASSWORD, message  : AppConstants.ErrorMessages.INVALID_OLD_PASSWORD)
        errorResolver.registerErrorCode (ErrorCodes.NOT_REGISTERED, message  : AppConstants.ErrorMessages.NOT_REGISTERED)
        
        return errorResolver
    }
}
