//
//  LoginViewDelgate.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

//Notes:- This protocol is used as a interface which is used by LoginViewPresenter to tranfer info to LoginViewController

protocol DeviceRegisterDelegate: class{
    
   func registerDeviceSuccessfull(withResponseModel loginResponseModel:DeviceRegisterResponseModel)
    func deRegisterDeviceSuccessfull(withResponseModel loginResponseModel:DeviceRegisterResponseModel)
     func driverOffilineSuccessful(withResponseModel taxResponseModel:LoginResponseModel)
}
