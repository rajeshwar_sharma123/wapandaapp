
//Notes:- This class is used as presenter for UpdateUserProfileViewPresenter

import Foundation
import ObjectMapper

class UpdateUserProfileViewPresenter: ResponseCallback{
    
//MARK:- UpdateUserProfileViewPresenter local properties
    
    private weak var updateUserProfileViewDelegate          : UpdateUserProfileViewDelegate?
    private lazy var updateUserProfileBusinessLogic         : UpdateUserProfileBusinessLogic = UpdateUserProfileBusinessLogic()
    private weak var textFieldValidationDelegate : TextFieldValidationDelegate?

//MARK:- Constructor
    
    init(delegate responseDelegate:UpdateUserProfileViewDelegate){
        self.updateUserProfileViewDelegate = responseDelegate
        self.textFieldValidationDelegate = responseDelegate as? TextFieldValidationDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let value = responseObject as? LoginResponseModel{
            self.updateUserProfileViewDelegate?.userProfileUpdatedSuccessfully(withResponse: value)
        }
        
        self.updateUserProfileViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error: ErrorModel){
        self.updateUserProfileViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.updateUserProfileViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call UpdateUserProfile Api.
    
    func sendUpdateUserProfileRequest(withUpdateUserProfileViewRequestModel updateUserProfileViewRequestModel:UpdateUserProfileViewRequestModel){
        
        guard validateInput(withData: updateUserProfileViewRequestModel) else {
            return
        }
        
        self.updateUserProfileViewDelegate?.showLoader()
        
        let viewModel = UpdateUserProfileRequestModel.Builder()
            .setUserId(AppDelegate.sharedInstance.userInformation.id!)
            .setRequestBody(updateUserProfileViewRequestModel)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).build()
        self.updateUserProfileBusinessLogic.performUpdateUserProfile(withUpdateUserProfileRequestModel: viewModel, presenterDelegate: self)
    }
    
    //MARK:- Validation for input fields method
    
    func validateInput(withData data:UpdateUserProfileViewRequestModel) -> Bool {
        
        guard let firstName = data.firstName, !firstName.isEmptyString() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_FIRST_NAME, forTextFields: .FirstName)
            return false
        }
        
//        guard let email = data.email, !email.isEmptyString() else{
//            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_EMAIL_ID, forTextFields: .EmailAddress)
//            return false
//        }
//
        guard  data.firstName!.isNameValid() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_FIRST_NAME, forTextFields: .FirstName)
            return false
        }
        
        
         if let email = data.email,!email.isEmptyString() {
            guard  data.email!.isValidEmailId() else{
                self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_EMAIL, forTextFields: .EmailAddress)
                return false
            }
        }
    
        
        if let lastName = data.lastName, !lastName.isEmptyString(){
            guard data.lastName!.isNameValid() else{
                self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_LAST_NAME, forTextFields: .LastName)
                return false
            }
        }
        
        return true
    }
}
