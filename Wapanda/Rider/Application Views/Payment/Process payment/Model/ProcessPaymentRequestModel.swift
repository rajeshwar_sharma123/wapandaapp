

//Notes:- This class is used for constructing ProcessPayment Service Request Model

class ProcessPaymentRequestModel {
    
    //MARK:- ProcessPaymentRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var paymentID: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.paymentID = builder.paymentID
        
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
         var paymentID: String = ""
        /**
         This method is used for setting Paymeny Id
         
         - parameter userName: String parameter that is going to be set on Paymeny Id
         
         - returns: returning Builder Object
         */
        func setPaymentId(_ id:String) -> Builder{
            requestBody["paymentId"] = id as AnyObject?
            self.paymentID = id
            return self
        }
        /**
         This method is used for setting Paymeny Id
         
         - parameter userName: String parameter that is going to be set on Paymeny Id
         
         - returns: returning Builder Object
         */
        func setSourceId(_ sourceId:String) -> Builder{
            if sourceId != ""
            {
            requestBody["sourceId"] = sourceId as AnyObject?
            }
            return self
        }
        /**
         This method is used for setting tipPercent Id
         
         - parameter tipPercent: String parameter that is going to be set on Paymeny Id
         
         - returns: returning Builder Object
         */
        func setTipPercent(_ tipPercent:Double) -> Builder{
            requestBody["tipPercent"] = tipPercent as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of ProcessPaymentRequestModel
         and provide ProcessPaymentForm1ViewViewRequestModel object.
         
         -returns : ProcessPaymentRequestModel
         */
        func build()->ProcessPaymentRequestModel{
            return ProcessPaymentRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting ProcessPayment end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return AppConstants.ApiEndPoints.PROCESS_PAYMENT
    }
    /**
     This method is used for getting ProcessPayment end point
     
     -returns: String containg end point
     */
    func getAddTipEndPoint()->String{
        return String(format:AppConstants.ApiEndPoints.ADD_TIP_PERCENT,self.paymentID)
    }
    
}
