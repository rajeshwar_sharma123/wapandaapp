//
//  YourBidsViewPresenter.swift
//  Wapanda
//
//  Created by Daffodil on 29/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class YourBidsViewPresenter: ResponseCallback{
    //MARK:- YourBidsViewPresenter properties
    
    weak var yourBidsViewDelegate   : YourBidsViewDelegate?
    lazy var yourBidsBussinessLogic : YourBidsBussinessLogic = YourBidsBussinessLogic()
   
    //MARK:- Constructor
    
    init(delegate responseDelegate: YourBidsViewDelegate){
        self.yourBidsViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        
        self.yourBidsViewDelegate?.hideLoader()
        
     if responseObject is YourBidsResponseModel{
            yourBidsViewDelegate?.updateYourBidsList(withResponseModel: responseObject as! YourBidsResponseModel)
        }
        
    }
    
    func servicesManagerError(error: ErrorModel){
        
        self.yourBidsViewDelegate?.hideLoader()
        self.yourBidsViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        
    }
    
    //if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
    /**
     This method is used to send upcoming trip list request to business layer
     - returns : Void
     */
    func sendYourBidsList(withData yourBidsViewModel: YourBidsViewModel) -> Void{
        
        self.yourBidsViewDelegate?.showLoader()
        
        var yourBidsRequestModel: YourBidsRequestModel!
        if yourBidsViewModel.status != ""{
            yourBidsRequestModel = YourBidsRequestModel.Builder()
                .setLimit(yourBidsViewModel.limit)
                .setSkip(yourBidsViewModel.skip)
                .setUserType(yourBidsViewModel.userType)
                .setOrder(1)
                .setSortingType(withType: "pickupDateTime")
                .setStatus(yourBidsViewModel.status)
                .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                .build()
        }else{
            yourBidsRequestModel = YourBidsRequestModel.Builder()
                .setLimit(yourBidsViewModel.limit)
                .setSkip(yourBidsViewModel.skip)
                .setUserType(yourBidsViewModel.userType)
                .setOrder(1)
                .setSortingType(withType: "pickupDateTime")
                .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                .build()
        }
        self.yourBidsBussinessLogic.performYourBidsList(withYourBidsRequestModel: yourBidsRequestModel, presenterDelegate: self)
        
    }
    
    //MARK:- Methods to make decision and call Feedback Api.
    
}

