
//Note :- This class contains DriverHome Buisness Logic

class DriverHomeBusinessLogic {
    
    
    deinit {
        print("DriverHomeBusinessLogic deinit")
    }
    
    /**
     This method is used for perform BankInfo With Valid Inputs constructed into a BankInfoRequestModel
     
     - parameter inputData: Contains info for Authentication Received from Stripe
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performBankInfo(withBankInfoRequestModel bankInfoRequestModel: BankInfoRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForBankInfo()
        BankInfoAPIRequest().makeAPIRequest(withReqFormData: bankInfoRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    /**
     This method is used for perform sign Up With Valid Inputs constructed into a DriverHomeRequestModel
     
     - parameter inputData: Contains info for DriverHome
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performDriverAvailabiltyRequest(withDriverHomeRequestModel signUpRequestModel: DriverHomeRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        DriverHomeApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    /**
     This method is used for perform sign Up With Valid Inputs constructed into a DriverHomeRequestModel
     
     - parameter inputData: Contains info for DriverHome
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performAreaBasePriceRequest(withAreaBasePriceRequestModel areaRequestModel: AreaBasePriceRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForAreaBasePrice()
        DriverHomeApiRequest().makeAPIRequestToGetBasePrice(withReqFormData: areaRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    func performLaunch(withLaunchRequestModel signUpRequestModel: LaunchRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        LaunchApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForAreaBasePrice() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
         errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
         errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
        
        return errorResolver
    }
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        return errorResolver
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForBankInfo() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        
        return errorResolver
    }
    
}
