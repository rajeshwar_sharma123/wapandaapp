//
//  TripDriverUpdateModel.swift
//
//  Created by  on 05/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class TripDriverUpdateModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let tripId = "tripId"
    static let driverLoc = "driverLoc"
  }

  // MARK: Properties
  public var tripId: String?
  public var driverLoc: DriverLoc?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    tripId <- map[SerializationKeys.tripId]
    driverLoc <- map[SerializationKeys.driverLoc]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = tripId { dictionary[SerializationKeys.tripId] = value }
    if let value = driverLoc { dictionary[SerializationKeys.driverLoc] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.tripId = aDecoder.decodeObject(forKey: SerializationKeys.tripId) as? String
    self.driverLoc = aDecoder.decodeObject(forKey: SerializationKeys.driverLoc) as? DriverLoc
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(tripId, forKey: SerializationKeys.tripId)
    aCoder.encode(driverLoc, forKey: SerializationKeys.driverLoc)
  }

}
