//
//  HelpViewController.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 08/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

enum CellState{
    
    case Expanded
    case Collapsed
}



class HelpViewController: BaseViewController {
    
    //MARK:- Properties/Outlets
    
    @IBOutlet weak var tableView: UITableView!
    var presenterHelp: HelpViewPresenter!
    
    fileprivate var helpDataSource: HelpViewResponseModel?
    
    let minAnswerLabelHeight:CGFloat = 29.0
    
    
    //MARK:- Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        intialSetupForView()
        
        //Initialize help presenter and call API
        self.presenterHelp = HelpViewPresenter(delegate: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenterHelp.makeHelpApiRequest()
    }
    
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        
        setupTableView()
        setupNavigationBar()
    }
    
    /**
     This function setup the Navigation bar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.HelpScreen.NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
    }
    
    /**
     Intialise TableView Properties
     */
    private func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    
    
    // Setting Properties on state of cell(i.e. Expand/Collapse)
    
    func reloadTableBasedOnCellState(withState state: CellState,withIndex index: Int){
        
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! HelpTableViewCell
        
        let answerItem = helpDataSource?.items?[index].answer
        
        switch state {
            
        case .Expanded:
            helpDataSource?.items?[index].isExpanded = !(helpDataSource?.items?[index].isExpanded)!
            
            //toggleStateOfOtherRows(withIndex: index)
            
            
            break
            
        case .Collapsed:
            if ((answerItem?.heightForView(font: UIFont.getSanFranciscoRegular(withSize: 12.0), width: cell.lblDescription.frame.width))! > minAnswerLabelHeight){
                self.helpDataSource?.items?[index].isExpanded = !(helpDataSource?.items?[index].isExpanded)!
                
                //toggleStateOfOtherRows(withIndex: index)
                
                break
            }
            
        }
        let selectedRowIndexPath = IndexPath(row: index, section: 0)
        tableView.reloadRows(at: [selectedRowIndexPath], with: .automatic)
    }
    
    // Set state of non selected rows
    func toggleStateOfOtherRows(withIndex index: Int) {
        
        for i in 0 ..< (helpDataSource?.items?.count)! {
            
            if(index != i){
                helpDataSource?.items?[i].isExpanded = false
            }
        }
        
        tableView.reloadData()
        
    }
}


//MARK:- HelpViewDelegate Methods

extension HelpViewController: HelpViewDelegate{
    
    //MARK: Base View Delegate
    
    /**
     This function shows the activity for the screen
     */
    func showLoader(){
        super.showLoader(self)
    }
    
    /**
     This function hide the activity
     */
    func hideLoader(){
        super.hideLoader(self)
    }

    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
        
    }
    
    
    func dataFetchedSuccessfully(withResponseModel responseData: HelpViewResponseModel) {
        
        self.helpDataSource = responseData
        self.tableView.reloadData()
    }
    
}



// MARK: - TableView Methods
extension HelpViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let data = self.helpDataSource?.items, data.count > 0{
            return data.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.getCell(withCellType: HelpTableViewCell.self)
        
        if let data = self.helpDataSource?.items, data.count > 0{
            
            cell.bindCellData(withResponse: helpDataSource!, withIndexPath: indexPath)
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = helpDataSource?.items?[indexPath.row]
        
        if let data = self.helpDataSource?.items, data.count > 0{
            
            (item?.isExpanded)! ?  self.reloadTableBasedOnCellState(withState: .Expanded, withIndex: indexPath.row) : self.reloadTableBasedOnCellState(withState: .Collapsed, withIndex: indexPath.row)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let data = self.helpDataSource?.items, data.count > 0{
            return UITableViewAutomaticDimension
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let data = self.helpDataSource?.items, data.count > 0{
            return 100.0
        }
        return 0
    }
}


