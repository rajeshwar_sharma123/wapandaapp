import UIKit

protocol SorceAndDestinationViewDelegate {
    
    func btnEditSourceOrDestinationClick()
}

class SourceAndDestinationView: UIView {

    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    var delegate: SorceAndDestinationViewDelegate?

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        let view = Bundle.main.loadNibNamed("SourceAndDestinationView", owner: self, options: nil)?[0] as! UIView
        addSubview(view)
        view.frame = self.bounds
    }
    
    func bind(source: String, destination: String){
        
        lblSource.text = source
        lblDestination.text = destination
    }
    
    
    @IBAction func btnEditSourceOrDestinationClick(_ sender: Any) {
        delegate?.btnEditSourceOrDestinationClick()
    }
}
