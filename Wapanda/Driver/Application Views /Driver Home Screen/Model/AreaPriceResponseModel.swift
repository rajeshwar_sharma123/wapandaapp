//
//  AreaPriceResponseModel.swift
//
//  Created by Daffolapmac on 02/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class AreaPriceResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let moneyview = "moneyview"
    static let validRadius = "validRadius"
    static let ratio = "ratio"
    static let indexArray = "indexArray"
    static let expirytime = "expirytime"
    static let ongoingRate = "ongoingRate"
    static let localization = "localization"
  }

  // MARK: Properties
  public var moneyview: Moneyview?
  public var validRadius: Int?
  public var ratio: Float?
  public var indexArray: [IndexArray]?
  public var expirytime: String?
  public var ongoingRate: Float?
  public var localization: Localization?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    moneyview <- map[SerializationKeys.moneyview]
    validRadius <- map[SerializationKeys.validRadius]
    ratio <- map[SerializationKeys.ratio]
    indexArray <- map[SerializationKeys.indexArray]
    expirytime <- map[SerializationKeys.expirytime]
    ongoingRate <- map[SerializationKeys.ongoingRate]
    localization <- map[SerializationKeys.localization]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = moneyview { dictionary[SerializationKeys.moneyview] = value.dictionaryRepresentation() }
    if let value = validRadius { dictionary[SerializationKeys.validRadius] = value }
    if let value = ratio { dictionary[SerializationKeys.ratio] = value }
    if let value = indexArray { dictionary[SerializationKeys.indexArray] = value.map { $0.dictionaryRepresentation() } }
    if let value = expirytime { dictionary[SerializationKeys.expirytime] = value }
    if let value = ongoingRate { dictionary[SerializationKeys.ongoingRate] = value }
    if let value = localization { dictionary[SerializationKeys.localization] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.moneyview = aDecoder.decodeObject(forKey: SerializationKeys.moneyview) as? Moneyview
    self.validRadius = aDecoder.decodeObject(forKey: SerializationKeys.validRadius) as? Int
    self.ratio = aDecoder.decodeObject(forKey: SerializationKeys.ratio) as? Float
    self.indexArray = aDecoder.decodeObject(forKey: SerializationKeys.indexArray) as? [IndexArray]
    self.expirytime = aDecoder.decodeObject(forKey: SerializationKeys.expirytime) as? String
    self.ongoingRate = aDecoder.decodeObject(forKey: SerializationKeys.ongoingRate) as? Float
    self.localization = aDecoder.decodeObject(forKey: SerializationKeys.localization) as? Localization
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(moneyview, forKey: SerializationKeys.moneyview)
    aCoder.encode(validRadius, forKey: SerializationKeys.validRadius)
    aCoder.encode(ratio, forKey: SerializationKeys.ratio)
    aCoder.encode(indexArray, forKey: SerializationKeys.indexArray)
    aCoder.encode(expirytime, forKey: SerializationKeys.expirytime)
    aCoder.encode(ongoingRate, forKey: SerializationKeys.ongoingRate)
    aCoder.encode(localization, forKey: SerializationKeys.localization)
  }

}
