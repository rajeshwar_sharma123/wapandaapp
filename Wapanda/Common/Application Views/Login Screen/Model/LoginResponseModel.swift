//
//  LoginResponseModel.swift
//  Wapanda
//
//  Created by daffomac-31 on 27/07/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginResponseModel: NSObject, Mappable, NSCoding {
    
    var _id: String!
    var firstName: String!
    var phone: String!
    var email: String = ""
    var countryCode: String!
    var countryCodeExt: String!
    var accountDisabled: Bool!
    var emailVerified: Bool!
    var phoneVerified: Bool!
    var driverDocs: DriverDocs?
    var driverProfile: DriverProfile?
    var riderProfile: RiderProfile?
    
    required internal init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    override init() {
        
    }
    
    internal func mapping(map: Map) {
        _id                        <- map["_id"]
        firstName                  <- map["firstName"]
        phone                      <- map["phone"]
        email                      <- map["email"]
        countryCode                <- map["countryCode"]
        countryCodeExt             <- map["countryCodeExt"]
        accountDisabled            <- map["accountDisabled"]
        emailVerified              <- map["emailVerified"]
        phoneVerified              <- map["phoneVerified"]
        driverDocs              <- map["driverDocs"]
        driverProfile              <- map["driverProfile"]
        riderProfile              <- map["riderProfile"]
    }
    
    //MARK:- NSCoding Delegate methods
    
    func encode(with aCoder: NSCoder){
        
        aCoder.encode(_id,forKey:AppConstants.UserInfoConstants.USER_ID)
        aCoder.encode(firstName,forKey:AppConstants.UserInfoConstants.FIRST_NAME)
        aCoder.encode(phone,forKey:AppConstants.UserInfoConstants.PHONE)
        aCoder.encode(email,forKey:AppConstants.UserInfoConstants.EMAIL)
        aCoder.encode(countryCode,forKey:AppConstants.UserInfoConstants.COUNTRY_CODE)
        aCoder.encode(countryCodeExt,forKey:AppConstants.UserInfoConstants.COUNTRY_CODE_EXTENSION)
        aCoder.encode(accountDisabled, forKey: AppConstants.UserInfoConstants.ACCOUNT_DISABLED)
        aCoder.encode(emailVerified, forKey: AppConstants.UserInfoConstants.EMAIL_VERIFIED)
        aCoder.encode(phoneVerified, forKey: AppConstants.UserInfoConstants.PHONE_VERIFIED)
        aCoder.encode(driverDocs,forKey: AppConstants.UserInfoConstants.DRIVER_DOC)
        aCoder.encode(driverProfile,forKey: AppConstants.UserInfoConstants.DRIVER_PROFILE)
        aCoder.encode(riderProfile,forKey: AppConstants.UserInfoConstants.RIDER_PROFILE)
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        
        self.init()
        
        _id = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.USER_ID) as! String
        firstName = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.FIRST_NAME) as! String
        phone = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.PHONE) as! String
        email = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.EMAIL) as! String
        countryCode = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.COUNTRY_CODE) as! String
        countryCodeExt = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.COUNTRY_CODE_EXTENSION) as! String
        accountDisabled = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.ACCOUNT_DISABLED) as! Bool
        emailVerified = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.EMAIL_VERIFIED) as! Bool
        phoneVerified = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.PHONE_VERIFIED) as! Bool
        driverDocs = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.DRIVER_DOC) as? DriverDocs
        riderProfile = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.RIDER_PROFILE) as? RiderProfile
        driverProfile = aDecoder.decodeObject(forKey: AppConstants.UserInfoConstants.DRIVER_PROFILE) as? DriverProfile
    }
    
    //MARK:- Helper methods to retrive and save user information.
    
    func saveUser(){
        let archiver = NSKeyedArchiver.archivedData(withRootObject: self)
        
        UserDefaultUtility.saveObjectWithKey(archiver as AnyObject, key: AppConstants.UserInfoConstants.USER_INFO)
    }
    
    class func retriveUser()->LoginResponseModel?{
        if(UserDefaultUtility.objectAlreadyExist(AppConstants.UserInfoConstants.USER_INFO)){
            let data = UserDefaultUtility.retrievObjectWithKey(AppConstants.UserInfoConstants.USER_INFO) as! Data
            let unarchiver = NSKeyedUnarchiver.unarchiveObject(with: data) as! LoginResponseModel
            return unarchiver
        }
        return nil
    }
    
    
    class func deleteUserInformationObject(){
        UserDefaultUtility.removeObjectWithKey(AppConstants.UserInfoConstants.USER_INFO)
    }
}
