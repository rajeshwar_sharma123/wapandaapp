
//Note :- This class contains CabLocation Buisness Logic

class CabLocationBusinessLogic {
    
    
    deinit {
        print("CabLocationBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a CabLocationRequestModel
     
     - parameter inputData: Contains info for CabLocation
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performCabLocationListFetch(withCabLocationRequestModel cabRequestModel: CabLocationRequestModel, presenterDelegate:CabLocationViewPresenter) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForNearByCabs()
        CabLocationApiRequest().makeAPIRequest(withReqFormData: cabRequestModel,errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    func performLaunch(withLaunchRequestModel signUpRequestModel: LaunchRequestModel, presenterDelegate:CabLocationViewPresenter) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForLaunch()
        LaunchApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForNearByCabs() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
        return errorResolver
    }
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForLaunch() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        return errorResolver
    }
}
