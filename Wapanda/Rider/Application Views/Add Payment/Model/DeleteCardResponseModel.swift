//
//  DeleteCardResponseModel.swift
//
//  Created by Daffolapmac on 01/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DeleteCardResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let deleted = "deleted"
  }

  // MARK: Properties
  public var id: String?
  public var deleted: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    deleted <- map[SerializationKeys.deleted]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.deleted] = deleted
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.deleted = aDecoder.decodeBool(forKey: SerializationKeys.deleted)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(deleted, forKey: SerializationKeys.deleted)
  }

}
