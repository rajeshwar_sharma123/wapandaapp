//
//  DriverHome+Slider.swift
//  Wapanda
//
//  Created by Daffolapmac on 05/12/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import UIKit
extension DriverHomeViewController
{
    func setGradientOnBackgroundView()
    {
         //self.viewGradient.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 392)
        gradient.colors = [UIColor(red: 54/255.0, green:  54/255.0, blue:  54/255.0, alpha: 1).cgColor, UIColor(red: 57/255.0, green:  57/255.0, blue:  57/255.0, alpha: 0.83).cgColor]
        gradient.locations = [0.0, 1.0]

        self.viewGradient.layer.insertSublayer(gradient, at: 0)
        self.viewGradient.layer.masksToBounds = true
    }
    func initialiseSliderView(_ ratio : Float)
    {
        let carTypeObj = self.getUserCarTypePriceObj()

        self.lblCarTypePrice.attributedText = self.attributedStringForCarType(self.getUserCarTypePriceObj().cartype ?? "")
        self.labelBaseArea.text = String(format:"$ %.2f",carTypeObj.basefare ?? 0.0)
        self.labelMile.text = String(format:"$ %.2f",(carTypeObj.milefare ?? 0.0) * ratio)
        self.labelMinutes.text = String(format:"$ %.2f",(carTypeObj.minutefare ?? 0.0) * ratio)
    }
    
    func attributedStringForCarType(_ carType:String)->NSMutableAttributedString
    {
        let carName = carType == "BlackCar" ? "BLACK CAR" : carType.uppercased()
    
        let firstHalf = NSMutableAttributedString(string: "( \(carName) ")
        
        let carImageAttachment = NSTextAttachment()
        carImageAttachment.image = #imageLiteral(resourceName: "ic_carType")
        
        let carImageString = NSAttributedString(attachment: carImageAttachment)

        firstHalf.append(carImageString)
        firstHalf.append(NSAttributedString(string: " )"))
        return firstHalf
    }
    func initialiseSliderScrollView()
    {
        self.scrollViewSlider.delegate = self
        self.scrollViewSlider.contentSize = CGSize(width: self.view.frame.width*2, height: self.scrollViewSlider.frame.height)
        
        self.viewSlider.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width*2, height: self.scrollViewSlider.frame.height)
//        self.scaleView.frame =  CGRect(x: 0, y: (self.viewSlider.frame.height/2.0-4.0), width: self.view.frame.width*2, height: 8)
        self.viewSlider.backgroundColor = UIColor.clear
        self.viewSlider.addSubview(self.viewSUVMarker)
        self.viewSlider.addSubview(self.viewTaxiMarker)
        self.viewSlider.addSubview(self.viewBlackCarMarker)
        self.viewSlider.addSubview(self.viewXLMarker)
        self.viewSlider.addSubview(self.viewStandardMarker)
        //self.scrollViewSlider.addSubview(viewSlider)
        
    }
    
    func setSliderWidth(_ maxPrice:Float)
    {
        var sliderWidth : Float = 0.0
        if (self.getScaleWidth(maxPrice) >= (Float(self.view.frame.width) * 2.0))
        {
            sliderWidth = self.getScaleWidth(maxPrice)
        }
        else
        {
            self.currentFactor = (Float(self.view.frame.width)) / (maxPrice * 2)
            sliderWidth = (Float(self.view.frame.width * 2.0))
        }
        self.additionFactor =  (Float(self.view.frame.width/2) / 100) / self.currentFactor
        self.scrollViewSlider.contentSize = CGSize(width: CGFloat(sliderWidth), height: self.scrollViewSlider.frame.height)
        self.viewSlider.frame = CGRect(x: 0, y: 0, width: CGFloat(sliderWidth), height: self.scrollViewSlider.frame.height)
         //self.scaleView.frame =  CGRect(x: 0, y: (self.viewSlider.frame.height/2.0-4.0), width: CGFloat(sliderWidth), height: 8)
    }
    func setMarkersOnSlider()
    {
        self.setXLMarkersOnSlider()
        self.setSUVMarkersOnSlider()
        self.setTaxiMarkersOnSlider()
        self.setStandardMarkersOnSlider()
        self.setBlackMarkersOnSlider()
    }
    /**
     This method is used to calculate the total width of the scale.
     - parameter maxPrice: The max base price among the car type and the driver.
     
     Formula = (Price in cents) * (How many cents will cover in 1 point of width) + (Width of view to give left and right space to get minimum and max value in the center)
     
     */
    func getScaleWidth(_ maxPrice:Float)->Float
    {
        let contentWidth = (maxPrice * 2 * 100) * AppConstants.ScreenSpecificConstant.DriverHomeScreen.POINTS_PER_CENT_FACTOR + Float(self.view.frame.width)
        return contentWidth
    }
    func initialiseCarTypeMakers(_ carTypePrices :[IndexArray])
    {
        
    }
    func getMaxPrice(_ carPrices:[IndexArray],_ userPrice: Float)->Float
    {
        var priceArray = carPrices.flatMap { (obj) -> Float? in
            return obj.indexrate
        }
        priceArray.append(userPrice)
        return priceArray.max()!
        
    }
    func getPostionForTheMarker(_ basePrice:Float,factor:Float)->Float
    {
        return factor * (basePrice * 100) + Float(self.view.frame.width/2)
    }
    func setSUVMarkersOnSlider()
    {
         let suvPriceObj = CarTypeMakers.getCarTypePriceInformation(self.onGoingPrices, carType: .SUV)
        if let suvPrice = suvPriceObj.indexrate
        {
            let postion = (((suvPrice+self.additionFactor) * 100) * self.currentFactor)
            self.viewSUVMarker.frame = CGRect(x: (Double(postion)-Double(self.viewSUVMarker.frame.width/2)), y: (Double(self.viewSlider.frame.height/2.0+4.0)), width:100.0, height: 80)
            let markerUI = self.getMarkerViewWithRespectToDriverPrice(Double(self.driverBasePrice), carPriceObject: suvPriceObj)
            self.viewSUVMarker.imageViewArrow.image = markerUI.2
            self.viewSUVMarker.labelPercent.text = String(format:"%d%%",markerUI.0)
            self.viewSUVMarker.labelCarType.textColor = markerUI.1
            self.viewSUVMarker.isHidden = false
            //print("SUV \(suvPrice) \((Double(postion)-Double(self.viewSUVMarker.frame.width/2))) \(self.currentFactor)")
        }
    }
    func setTaxiMarkersOnSlider()
    {
        let taxiPriceObj = CarTypeMakers.getCarTypePriceInformation(self.onGoingPrices, carType: .Taxi)
        if let taxiPrice = taxiPriceObj.indexrate
        {
            let postion = (((taxiPrice+self.additionFactor) * 100) * self.currentFactor)
            self.viewTaxiMarker.frame = CGRect(x: (Double(postion)-Double(self.viewTaxiMarker.frame.width/2)), y: (Double((self.viewSlider.frame.height/2.0)-4.0-80.0)), width:100.0, height: 80)
            
            let markerUI = self.getMarkerViewWithRespectToDriverPrice(Double(self.driverBasePrice), carPriceObject: taxiPriceObj)
            self.viewTaxiMarker.imageViewArrow.image = markerUI.2
            self.viewTaxiMarker.labelPercent.text = String(format:"%d%%",markerUI.0)
            self.viewTaxiMarker.labelCarType.textColor = markerUI.1
            self.viewTaxiMarker.isHidden = false
            //print("TAXI \(taxiPrice) \((Double(postion)-Double(self.viewTaxiMarker.frame.width/2))) \(self.currentFactor)")
        }
    }
    func setBlackMarkersOnSlider()
    {
         let blackPriceObj = CarTypeMakers.getCarTypePriceInformation(self.onGoingPrices, carType: .BlackCar)
        if let blackPrice = blackPriceObj.indexrate
        {
            let postion = (((blackPrice+self.additionFactor) * 100) * self.currentFactor)
            self.viewBlackCarMarker.frame = CGRect(x: (Double(postion)-Double(self.viewBlackCarMarker.frame.width/2)), y: (Double(self.viewSlider.frame.height/2.0-4.0-80.0)), width:100.0, height: 80)
            
            let markerUI = self.getMarkerViewWithRespectToDriverPrice(Double(self.driverBasePrice), carPriceObject: blackPriceObj)
            self.viewBlackCarMarker.imageViewArrow.image = markerUI.2
            self.viewBlackCarMarker.labelPercent.text = String(format:"%d%%",markerUI.0)
            self.viewBlackCarMarker.labelCarType.textColor = markerUI.1
            self.viewBlackCarMarker.isHidden = false
            //print("BLACK \(blackPrice) \((Double(postion)-Double(self.viewBlackCarMarker.frame.width/2))) \(self.currentFactor)")
        }
    }
    func setStandardMarkersOnSlider()
    {
        let standardPriceObj = CarTypeMakers.getCarTypePriceInformation(self.onGoingPrices, carType: .Standard)

        if let standardPrice = standardPriceObj.indexrate
        {
            let postion = (((standardPrice+self.additionFactor) * 100) * self.currentFactor)
            self.viewStandardMarker.frame = CGRect(x: (Double(postion)-Double(self.viewStandardMarker.frame.width/2)), y: (Double(self.viewSlider.frame.height/2.0+4.0)), width:100.0, height: 80)
            
            let markerUI = self.getMarkerViewWithRespectToDriverPrice(Double(self.driverBasePrice), carPriceObject: standardPriceObj)
            self.viewStandardMarker.imageViewArrow.image = markerUI.2
            self.viewStandardMarker.labelPercent.text = String(format:"%d%%",markerUI.0)
            self.viewStandardMarker.labelCarType.textColor = markerUI.1
            self.viewStandardMarker.isHidden = false
            //print("STANDARD \(standardPrice) \((Double(postion)-Double(self.viewStandardMarker.frame.width/2))) \(self.currentFactor)")
        }
    }
    func setXLMarkersOnSlider()
    {
        let xlPriceObj = CarTypeMakers.getCarTypePriceInformation(self.onGoingPrices, carType: .XL)
        if let xlPrice = xlPriceObj.indexrate
        {
            let postion = (((xlPrice+self.additionFactor) * 100) * self.currentFactor)
            self.viewXLMarker.frame = CGRect(x: (Double(postion)-Double(self.viewXLMarker.frame.width/2)), y: (Double(self.viewSlider.frame.height/2.0+4.0)), width:100.0, height: 80)
            let markerUI = self.getMarkerViewWithRespectToDriverPrice(Double(self.driverBasePrice), carPriceObject: xlPriceObj)
            self.viewXLMarker.imageViewArrow.image = markerUI.2
            self.viewXLMarker.labelPercent.text = String(format:"%d%%",markerUI.0)
            self.viewXLMarker.labelCarType.textColor = markerUI.1
            self.viewXLMarker.isHidden = false
            //print("XL : \(xlPrice) \((Double(postion)-Double(self.viewXLMarker.frame.width/2))) \(self.currentFactor)")
        }
    }
    func setSelfPositionOnSlider()
    {
        let postion = (((self.driverBasePrice) * 100) * self.currentFactor)
        self.scrollViewSlider.scrollRectToVisible(CGRect(x: Double(postion), y: 0, width:Double(self.view.frame.width), height: Double(self.scrollViewSlider.frame.height)), animated: true)
        //print("SELF : \(self.driverBasePrice) \(postion) \(self.currentFactor)")
        
    }
    
    func getMarkerViewWithRespectToDriverPrice(_ driverPrice:Double,carPriceObject:IndexArray) -> (Int,UIColor,UIImage)
    {
        var percent = 0
        var color = UIColor.white
        var markerImage = #imageLiteral(resourceName: "ic_driverMarker")
        let carPrice = Double(carPriceObject.indexrate ?? 0)
        
        guard carPrice != 0 else {
             return (percent,color,#imageLiteral(resourceName: "ic_driverMarker"))
        }
        percent     = Int(((abs(driverPrice-carPrice)/carPrice)*100.0).rounded())
        
        markerImage = percent == 0 ? UIImage() : (driverPrice > carPrice ? #imageLiteral(resourceName: "ic_driverMarker") : #imageLiteral(resourceName: "ic_ArrowUp"))
        
        
        color = carPriceObject.cartype == AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.carType?.vehicleType ? UIColor(red: 255/255.0, green: 167/255.0, blue: 0/255.0, alpha: 1) : UIColor.white
        
        
        return (percent,color,markerImage)
    }
    
    func getRatio(_ scrollOffsetX:Float, carTypeRate:Float)->Float
    {
        guard carTypeRate != 0 else {
            return 1
        }
        let newPrice = (scrollOffsetX / 100) / self.currentFactor
        let ratio = newPrice/carTypeRate
        return ratio
    }
    func getUserCarTypePriceObj()->IndexArray
    {
        let userCarType = self.onGoingPrices.filter({ (obj) -> Bool in
            if obj.cartype == AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.carType?.vehicleType
            {
                return true
            }
            return false
        })
        
        if userCarType.count != 0
        {
            return userCarType[0]
        }
        return IndexArray(JSON:[:])!
    }
}
extension DriverHomeViewController:UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let carTypeObj = self.getUserCarTypePriceObj()
       self.driverBasePrice = self.getRatio(Float(scrollView.contentOffset.x), carTypeRate: (carTypeObj.indexrate ?? 0.0)) * (carTypeObj.indexrate ?? 0.0)
        let maxRate = self.getMaxPrice(self.onGoingPrices, self.driverBasePrice)
        self.setSliderWidth(maxRate)
        self.setMarkersOnSlider()
        let ratio = self.getRatio(Float(scrollView.contentOffset.x), carTypeRate: (carTypeObj.indexrate ?? 0.0))
        self.initialiseSliderView(ratio)
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if !decelerate
        {
            let carTypeObj = self.getUserCarTypePriceObj()
            self.updateDriverPrice(ratio: self.getRatio(Float(scrollView.contentOffset.x), carTypeRate: (carTypeObj.indexrate ?? 0.0)))
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let carTypeObj = self.getUserCarTypePriceObj()
        let ratio = self.getRatio(Float(scrollView.contentOffset.x), carTypeRate: (carTypeObj.indexrate ?? 0.0))
        self.updateDriverPrice(ratio: ratio)
    }
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        scrollView.setContentOffset(scrollView.contentOffset, animated: true)
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
         //print(" scrollViewDidEndScrollingAnimation")
    }
}
