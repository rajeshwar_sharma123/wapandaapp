//
//  HelpBusinessLogic.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation


class HelpBusinessLogic {
    
    
    deinit {
        print("HelpBusinessLogic deinit")
    }
    
    /**
     Note:- Perform Help With Valid Inputs constructed into a HelpRequestModel
     
     - parameter inputData: Contains info for Help
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performHelpRequest(withHelpRequestModel helpRequestModel: HelpViewRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForhelp()
        
        HelpApiRequest().makeAPIRequest(withReqFormData: helpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     Registering set of Predefined Error coming from server
     */
    private func registerErrorForhelp() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        
        return errorResolver
    }
}
