
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct FeedbackViewRequestModel {
    var driverName: String!
    var driverImageId: String?
    var tripId: String!
    var rate                   : Int!
    var reivew: String?
    var pushObjModel : PushNotificationObjectModel!
}
