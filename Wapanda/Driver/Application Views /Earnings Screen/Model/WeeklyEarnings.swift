//
//  WeeklyEarnings.swift
//
//  Created by Daffolapmac on 23/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class WeeklyEarnings: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalEarning = "totalEarning"
    static let date = "date"
  }

  // MARK: Properties
  public var totalEarning: Float?
  public var date: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    totalEarning <- map[SerializationKeys.totalEarning]
    date <- map[SerializationKeys.date]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = totalEarning { dictionary[SerializationKeys.totalEarning] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.totalEarning = aDecoder.decodeObject(forKey: SerializationKeys.totalEarning) as? Float
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(totalEarning, forKey: SerializationKeys.totalEarning)
    aCoder.encode(date, forKey: SerializationKeys.date)
  }

}
