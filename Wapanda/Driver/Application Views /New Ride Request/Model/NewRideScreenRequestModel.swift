//
//  NewRideScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NewRideScreenRequestModel {
    var lat : Double = 0.0
    var lng : Double = 0.0
    var success   = true
    var message   = "Ride accepted."
    var biddingId   = ""
}
