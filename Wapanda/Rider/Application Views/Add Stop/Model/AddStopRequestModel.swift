

//Notes:- This class is used for constructing AddStop Service Request Model

class AddStopRequestModel {
    
    //MARK:- AddStopRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var tripId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.tripId = builder.tripId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var tripId: String!
        
        /**
         This method is used for setting lat
         
         - parameter userName: String parameter that is going to be set on lat
         
         - returns: returning Builder Object
         */
        func setLat(_ lat:Double) -> Builder{
            requestBody["lat"] = lat as AnyObject?
            return self
        }
        
        
        /**
         This method is used for setting lat
         
         - parameter userName: String parameter that is going to be set on lat
         
         - returns: returning Builder Object
         */
        func setAddress(_ address:String) -> Builder{
            requestBody["address"] = address as AnyObject?
            return self
        }
        
        
        /**
         This method is used for setting lng
         
         - parameter userName: String parameter that is going to be set on lng
         
         - returns: returning Builder Object
         */
        func setLng(_ lng:Double) -> Builder{
            requestBody["lng"] = lng as AnyObject?
            return self
        }
        
        /**
         This method is used for setting trip id
         
         - parameter userName: String parameter that is going to be set trip id
         
         - returns: returning Builder Object
         */
        func setTripId(_ id:String) -> Builder{
            self.tripId = id
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of AddStopRequestModel
         and provide AddStopForm1ViewViewRequestModel object.
         
         -returns : AddStopRequestModel
         */
        func build()->AddStopRequestModel{
            return AddStopRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting AddStop end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.ADD_STOP, self.tripId)
    }
    
}
