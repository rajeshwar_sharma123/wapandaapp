import UIKit
import GooglePlaces
import GoogleMaps
import IQKeyboardManager

protocol SelectCabViewControllerDelegate: class {
    func updateList(cabType: String)
}

class SelectCabViewController: BaseViewController, SelectCabViewDelegate, SelectCabDelegate {
    
    @IBOutlet weak var selectCabView: SelectCabView!
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var sourceTxtFld: UITextField!
    @IBOutlet weak var cabsTableView: UITableView!
    @IBOutlet weak var sourceAndDestinationView: UIBorderView!
    
    //to track if a user clicked on address or the cab selection from previous screen
    var editAddress = true
    var presenter: SelectCabPresenter!
    var vehicleList = [VehicleListResponseModel]()
    var editAddressType: ADDRESS_TYPE!
    var selectedCabName: String = ""
    var selectedCabPrice = 0.0
    var previouslySelectedIndex = -1
    var searchString = ""
    var selectedCab: Type?
    var estPrice: Double = 0.0
    var dataSource: [GMSAutocompletePrediction] = []
    var dataSourceUserAddresses: [Address] = []
    var presenterGooglePlaces: GooglePlaceViewPresenter!
    var placeDetailPresenter: GooglePlaceDetailViewPresenter!
    var selectedSourceAddress: SelectedPlaceModel!
    var selectedDestinationAddress: SelectedPlaceModel!
    var selectedAddressTitle: String!
    var addAddressTypeSelected = ADD_ADDRESS_TYPE.NONE
    
    weak var delegate: SelectCabViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        
        presenter = SelectCabPresenter(delegate: self)
        setInitialSourceAndDestinationTitle()
        selectCabView.delegate = self

        registerTableViewCells()
        setupTextFields()
        self.initalizeUserAddresess()
        
        self.placeDetailPresenter = GooglePlaceDetailViewPresenter(delegate: self)
        self.presenterGooglePlaces = GooglePlaceViewPresenter(delegate: self)
        
        if let selectedRideInfo = UserRideDetail.shared.selectedCabs.first {
            selectCabView.bindData(cabInfo: selectedRideInfo)
        }
        
        addressTableView.isHidden = !editAddress
        cabsTableView.isHidden = editAddress
        setDropDownArrow()
        
        if editAddress {
            sourceTxtFld.becomeFirstResponder()
        }
        selectedCabName = selectedCab?.vehicleType ?? ""
        
        presenter.getAllVehicleTypes(latFrom: selectedSourceAddress.coordinate.latitude,
                                     lngFrom: selectedSourceAddress.coordinate.longitude,
                                     latTo: selectedDestinationAddress.coordinate.latitude,
                                     lngTo: selectedDestinationAddress.coordinate.longitude)
    }
    
    
    func setupTextFields() {
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        sourceTxtFld.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        destinationTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    func initalizeUserAddresess(){
        
        if let home = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
            home.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME
            self.dataSourceUserAddresses.append(home)
        }
        
        if let work = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
            work.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_WORK
            self.dataSourceUserAddresses.append(work)
        }
        
        if let recent = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.recentlyVisited{
            recent.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT
            self.dataSourceUserAddresses.append(recent)
        }
    }
    
    func setInitialSourceAndDestinationTitle(){
        if let sourceDestination = UserRideDetail.shared.sourceDestination {
            sourceTxtFld.text = sourceDestination.source.address
            destinationTextField.text = sourceDestination.destination.address
            
            selectedSourceAddress = SelectedPlaceModel(title: sourceDestination.source.address, coordinate: CLLocationCoordinate2D(latitude: sourceDestination.source.lat, longitude: sourceDestination.source.long))
                
            selectedDestinationAddress = SelectedPlaceModel(title: sourceDestination.destination.address, coordinate: CLLocationCoordinate2D(latitude: sourceDestination.destination.lat, longitude: sourceDestination.destination.long))
        }
    }
    
    
    func showErrorAlert(_ errorTitle: String, alertMessage errorMessage: String) {
        super.showErrorAlert(errorTitle, alertMessage: errorMessage, VC: self)
    }
    
    func showLoader() {
        self.showLoader(self)
    }
    
    func hideLoader() {
        self.hideLoader(self)
    }
    
    func refreshList(list: [VehicleListResponseModel]) {
        
        vehicleList = list
        vehicleList.insert(self.createAnyCarTypeModel(vehicleList), at: 0)
        cabsTableView.reloadData()
    }
    func createAnyCarTypeModel(_ list: [VehicleListResponseModel])-> VehicleListResponseModel
    {
        let anyType =  VehicleListResponseModel(JSON:[:])
        anyType?.carType = "Any"
        anyType?.estPrice = 0.0
        anyType?.fairValues = FairValuesModel(JSON:[:])
        anyType?.fairValues?.fairPriceLow = list.flatMap({ (obj) -> Double? in obj.fairValues?.fairPriceLow }).min() ?? 0.0
        anyType?.fairValues?.fairPriceHigh = list.flatMap({ (obj) -> Double? in obj.fairValues?.fairPriceHigh }).max() ?? 0.0
        anyType?.fairValues?.maxFare = list.flatMap({ (obj) -> Double? in obj.fairValues?.maxFare }).max() ?? 0.0
        anyType?.fairValues?.minFare = list.flatMap({ (obj) -> Double? in obj.fairValues?.minFare }).min() ?? 0.0
        return anyType!
    }
    //ic_arrow_drop_down_white
    func selectCabClicked() {
        cabsTableView.isHidden = !cabsTableView.isHidden
        setDropDownArrow()
        addressTableView.isHidden = true
        sourceTxtFld.endEditing(true)
        destinationTextField.endEditing(true)
    }
    func setDropDownArrow(){
        if cabsTableView.isHidden{
            selectCabView.dropDownImageView.image = #imageLiteral(resourceName: "icDownArrow")
        }else{
            selectCabView.dropDownImageView.image = #imageLiteral(resourceName: "ic_upArrow")
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyClicked(_ sender: Any) {
        

        UserRideDetail.shared.sourceDestination = SourceDestinationModel
            .Builder().setDestination(lat: selectedDestinationAddress.coordinate.latitude, long: selectedDestinationAddress.coordinate.longitude, address: selectedDestinationAddress.title)
            .setSource(lat: selectedSourceAddress.coordinate.latitude, long: selectedSourceAddress.coordinate.longitude, address: selectedSourceAddress.title).build()
        
        if previouslySelectedIndex != -1{
            
            let name = vehicleList[previouslySelectedIndex].carType ?? ""
//            let icon = vehicleList[previouslySelectedIndex].icon ?? ""
            let estPrice = vehicleList[previouslySelectedIndex].estPrice
            let selectedCab = SelectedCabModel(name: name, id: name, icon: "", estPrice: estPrice!)
            UserRideDetail.shared.selectedCabs =  [selectedCab]
            delegate?.updateList(cabType: name)
        }
        else{
            delegate?.updateList(cabType: selectedCabName)
        }
        
        dismiss(animated: true, completion: nil)
    }
}


//MARK: UITextField Delegate ...
extension SelectCabViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.clearSearchResults()
        searchString = textField.text!
        self.startSearchForPlaces()

        if textField == sourceTxtFld {
            self.editAddressType = .SOURCE
        }
        else{
            self.editAddressType = .DESTINATION
        }
        addressTableView.isHidden = false
        cabsTableView.isHidden = true
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //addressTableView.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.startSearchForPlaces()
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if (range.location == 0 && string.characters.count == 0){
            //Empty String handling
            self.searchString = ""
        }
        
        if !string.isEmpty{
            self.searchString = textField.text! + string
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.searchString = ""
        addressTableView.isHidden = false
        cabsTableView.isHidden = true
        return true
    }
    
    //MARK: Helper Methods
    func textFieldDidChange(){
        self.startSearchForPlaces()
    }
    
    func startSearchForPlaces(){
        
        if !searchString.isEmpty{
            let requestModel = GooglePlaceViewRequestModel(searchString: searchString.getWhitespaceTrimmedString(), userLocationCoordinate: LocationServiceManager.sharedInstance.currentLocation?.coordinate)
            self.presenterGooglePlaces.sendGooglePlaceRequest(withGooglePlaceViewRequestModel: requestModel)
        }
        else{
            self.clearSearchResults()
        }
    }
    
    func clearSearchResults(){
        self.dataSource = []
        self.addressTableView.reloadData()
    }
    
}

//MARK: Google Place Delegate ...
extension SelectCabViewController: GooglePlaceViewDelegate{
    
    func showSearchList(list: [GMSAutocompletePrediction]){
        dataSource = list
        addressTableView.reloadData()
        addressTableView.isHidden = false
        cabsTableView.isHidden = true
    }
}

//MARK: Google Place Detail Delegate ...
extension SelectCabViewController: GooglePlaceDetailViewDelegate{
    
    func placeDetailWithData(data: GMSPlace){
        
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedAddressTitle, coordinate: data.coordinate)
        self.onAddressSelection(withAddress: model)
    }
}

//MARK: UITableView Delegate & Datasource...
extension SelectCabViewController: UITableViewDelegate, UITableViewDataSource {
    
    func registerTableViewCells(){
        addressTableView.register(UINib(nibName: String(describing: AddressSearchResultTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: AddressSearchResultTableViewCell.self))
        addressTableView.register(UINib(nibName: String(describing: ImageViewWithTitleTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ImageViewWithTitleTableViewCell.self))
        cabsTableView.registerTableViewCell(tableViewCell: SelectCabCell.self)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == cabsTableView {
            return vehicleList.count
        }
        if dataSource.count == 0 && !searchString.isEmpty {
            return 1
        }
        else if dataSource.count == 0 {
            return self.dataSourceUserAddresses.count
        }
        else{
            return self.dataSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == cabsTableView {
            let cell = tableView.getCell(withCellType: SelectCabCell.self)
            let vehicle = vehicleList[indexPath.row]
            
            if (vehicle.carType == selectedCabName) {
                previouslySelectedIndex = indexPath.row
            }
            
            cell.bindData(cabType: vehicle.carType, price: vehicle.estPrice, icon: nil, currentlySelected: (vehicle.carType == selectedCabName))
            return cell
        }
        else if dataSource.count == 0 && !searchString.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddressSearchResultTableViewCell.self)) as! AddressSearchResultTableViewCell
            cell.bind(withTitle: NSAttributedString(string: AppConstants.ScreenSpecificConstant.RideListScreen.LOCATION_NOT_FOUND) , WithStateAndCountry: NSAttributedString(string: AppConstants.ScreenSpecificConstant.RideListScreen.LOCATION_NOT_FOUND_MSG))
            cell.selectionStyle = .none
            return cell
        }
        else if dataSource.count == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageViewWithTitleTableViewCell.self)) as! ImageViewWithTitleTableViewCell
            
            let selectedAddress = self.dataSourceUserAddresses[indexPath.row]
            
            if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT{
                cell.bind(withTitle: selectedAddress.address!)
            }
            else if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME{
                cell.bind(withTitle: selectedAddress.address_type_title!,andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_home"))
            }
            else{
                cell.bind(withTitle: selectedAddress.address_type_title!, andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_work"))
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddressSearchResultTableViewCell.self)) as! AddressSearchResultTableViewCell
            let selectedAddress = self.dataSource[indexPath.row]
            cell.bind(withTitle: selectedAddress.attributedPrimaryText , WithStateAndCountry: selectedAddress.attributedSecondaryText ?? NSAttributedString(string: ""))
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == cabsTableView {
            if previouslySelectedIndex != -1{
                let cell = tableView.cellForRow(at: IndexPath(row: previouslySelectedIndex, section: 0)) as!SelectCabCell
                cell.makeCellDeselected()
            
            }
            let cell = tableView.cellForRow(at: indexPath) as!SelectCabCell
            cell.makeCellSelected()
            previouslySelectedIndex = indexPath.row
    
        }
        else if dataSource.count == 0 && !searchString.isEmpty{
            //Do nothing while tap on no location cell
        }
        else if dataSource.count == 0{
            self.selectedAddressTitle = self.dataSourceUserAddresses[indexPath.row].address
            self.handleUserAddressSelection(withAddress: self.dataSourceUserAddresses[indexPath.row])
            tableView.isHidden = true
        }
        else{
            
            //Get Place Detail
            let selectedPlace = self.dataSource[indexPath.row]
            self.selectedAddressTitle = selectedPlace.attributedFullText.string
            self.placeDetailPresenter.sendGooglePlaceDetailRequest(withGooglePlaceDetailViewRequestModel: GooglePlaceDetailViewRequestModel(placeId: selectedPlace.placeID ?? ""))
            tableView.isHidden = true
            setDropDownArrow()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func handleUserAddressSelection(withAddress address: Address){
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedAddressTitle, coordinate: CLLocationCoordinate2D(latitude: address.lat!, longitude: address.lng!))
        
        //Set Address
        if self.editAddressType == .DESTINATION{
            destinationTextField.text = self.selectedAddressTitle
        }
        else{
            sourceTxtFld.text = self.selectedAddressTitle
        }
        
        self.onAddressSelection(withAddress: model)
    }
    
    func onAddressSelection(withAddress address: SelectedPlaceModel){
        if editAddressType == .DESTINATION{
            self.selectedDestinationAddress = address
        }
        else{
            self.selectedSourceAddress = address
        }
        
        self.updateSourceAndDestinationAddress()
    }
    
    func updateSourceAndDestinationAddress(){
        sourceTxtFld.text =  selectedSourceAddress.title ?? ""
        destinationTextField.text = selectedDestinationAddress.title ?? ""
        presenter.getAllVehicleTypes(latFrom: selectedSourceAddress.coordinate.latitude,
                                     lngFrom: selectedSourceAddress.coordinate.longitude,
                                     latTo: selectedDestinationAddress.coordinate.latitude,
                                     lngTo: selectedDestinationAddress.coordinate.longitude)
    }
    
}
