
//Note :- This class contains CabSelection Buisness Logic

class CabSelectionBusinessLogic {
    
    
    deinit {
        print("CabSelectionBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a CabSelectionRequestModel
     
     - parameter inputData: Contains info for CabSelection
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performCabSelection(withCabSelectionRequestModel signUpRequestModel: CabSelectionRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        CabSelectionApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)

        return errorResolver
    }
}
