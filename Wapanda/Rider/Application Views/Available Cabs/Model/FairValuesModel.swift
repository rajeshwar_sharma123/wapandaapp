import UIKit
import ObjectMapper

public class FairValuesModel: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let minFare = "minimumPrice" 
        static let maxFare = "maximumPrice"
        static let fairPrice = "fairPrice"
        static let fairPriceLow = "fairPriceLow"
        static let fairPriceHigh = "fairPriceHigh"
        static let typicalPrice = "typicalPrice"
    }
    
    // MARK: Properties
    public var minFare: Double = 0.0
    public var maxFare: Double = 0.0
    public var fairPrice: Double = 0.0
    public var fairPriceLow: Double = 0.0
    public var fairPriceHigh: Double = 0.0
    public var typicalPrice: Double = 0.0

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        minFare <- map[SerializationKeys.minFare]
        maxFare <- map[SerializationKeys.maxFare]
        fairPrice <- map[SerializationKeys.fairPrice]
        fairPriceLow <- map[SerializationKeys.fairPriceLow]
        fairPriceHigh <- map[SerializationKeys.fairPriceHigh]
        typicalPrice <- map[SerializationKeys.typicalPrice]
    }
}
