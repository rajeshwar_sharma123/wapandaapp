

import UIKit



class PickSourceInfoWindow: UIView {

    
    /// Screen Outlets
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var _iWidthLayoutActivityIndicator: NSLayoutConstraint!
   
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    
    /// Data binding on cell
    ///
    /// - Parameter address: string title for address
    func bind(withLocation address: String){
        self.lblAddress.text = address
        self.hideActivityView()
    }
    
    
    /// Prepare view for fetching address
    func fetchingAddress(){
        self.bind(withLocation: AppConstants.ScreenSpecificConstant.RiderHomeScreen.FETCHING_ADDRESS_TITLE)
        self.showActivityView()
    }
    
    
    /// Shows activity view on view
    private func showActivityView(){
        self._iWidthLayoutActivityIndicator.constant = 20
        self.activityView.isHidden = false
    }
    
    
    /// hides activity view from view
    private func hideActivityView(){
        self.activityView.isHidden = true
        self._iWidthLayoutActivityIndicator.constant = 0
    }
    
}
