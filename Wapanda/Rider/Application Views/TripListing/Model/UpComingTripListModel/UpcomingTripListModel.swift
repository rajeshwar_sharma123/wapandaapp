//
//  UpcomingTripListModel.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 21/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
struct UpcomingTripListModel {
    var limit                      : Int!
    var skip                       : Int!
    var status                     : String!
    var userType                   :UserType!
}
