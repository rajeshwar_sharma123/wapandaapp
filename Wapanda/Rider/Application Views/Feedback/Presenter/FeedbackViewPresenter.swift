
//Notes:- This class is used as presenter for FeedbackViewPresenter

import Foundation
import ObjectMapper

class FeedbackViewPresenter: ResponseCallback{
    
//MARK:- FeedbackViewPresenter local properties
    
    private weak var feedbackViewDelegate          : FeedbackViewDelegate?
    private lazy var feedbackBusinessLogic         : FeedbackBusinessLogic = FeedbackBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:FeedbackViewDelegate){
        self.feedbackViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let _ = responseObject as? CommonResponseModel{
            self.feedbackViewDelegate?.feedbackSubmittedSucessfully()
        }
        self.feedbackViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error: ErrorModel){
        self.feedbackViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.feedbackViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call Feedback Api.
    
    func sendFeedbackRequest(withFeedbackViewRequestModel feedbackViewRequestModel:FeedbackViewRequestModel){
        self.feedbackViewDelegate?.showLoader()
        
        let requestModel = FeedbackRequestModel.Builder()
            .setTrip(feedbackViewRequestModel.tripId)
            .setRate(feedbackViewRequestModel.rate)
            .setReview(feedbackViewRequestModel.reivew)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.feedbackBusinessLogic.performFeedback(withFeedbackRequestModel: requestModel, presenterDelegate: self)
    }
}
