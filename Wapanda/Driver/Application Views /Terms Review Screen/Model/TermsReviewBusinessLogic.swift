
//Note :- This class contains TermsReview Buisness Logic

class TermsReviewBusinessLogic {
    
    
    deinit {
        print("TermsReviewBusinessLogic deinit")
    }

    /**
     */
    
    func performTermsReview(withLoginRequestModel termsReviewRequestModel: TermsReviewRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForTermsReview()
        TermsReviewApiRequest().makeAPIRequest(withReqFormData: termsReviewRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForTermsReview() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        return errorResolver
    }
}
