//
//  EarningsScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct EarningsScreenRequestModel {
    var fromWeekDate : Int = 0
    var toWeekDate : Int = 0
    var fromMonth : Int = 0
    var currentDate : Int = 0
}
