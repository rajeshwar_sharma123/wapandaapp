

//Notes:- This class is used for constructing UpdatedProfile Service Request Model

class UpdatedProfileRequestModel {
    
    //MARK:- UpdatedProfileRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var userId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.userId     = builder.userID
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var userID: String!
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        /**
         This method is used for setting User id
         
         - parameter userID: String parameter that is going to be set on userID
         
         - returns: returning Builder Object
         */
        func setUserId(_ userID:String)->Builder{
            self.userID = userID
            return self
        }
        
        /**
         This method is used to set properties in upper class of UpdatedProfileRequestModel
         and provide UpdatedProfileForm1ViewViewRequestModel object.
         
         -returns : UpdatedProfileRequestModel
         */
        func build()->UpdatedProfileRequestModel{
            return UpdatedProfileRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting UpdatedProfile end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.USER_PROFILE_UPDATE, self.userId)
    }
    
}
