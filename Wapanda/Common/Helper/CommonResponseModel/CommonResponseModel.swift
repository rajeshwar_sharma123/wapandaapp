
//Note :- This class is used for creating common Response Model

import ObjectMapper

class CommonResponseModel:Mappable {
    
    var message       : String = ""
    
    
    required internal init?(map: Map)
    {
        mapping(map: map)
    }
    
    init()
    {
        
    }
    
    internal func mapping(map: Map)
    {
        message         <- map["message"]
    }
}
