//
//  GetAuctionDetailResponseModel.swift
//
//  Created by Daffolapmac on 14/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class AuctionDetailResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lowestQuote = "lowestQuote"
    static let carType = "carType"
    static let endLoc = "endLoc"
    static let createdAt = "createdAt"
    static let startLoc = "startLoc"
    static let toAddress = "toAddress"
    static let riderId = "riderId"
    static let fromAddress = "fromAddress"
    static let riderSelectedFare = "riderSelectedFare"
    static let closed = "closed"
    static let id = "_id"
    static let estDistance = "estDistance"
    static let status = "status"
    static let updatedAt = "updatedAt"
    static let pickupDateTime = "pickupDateTime"
    static let estDuration = "estDuration"
    
    static let selectedDriverId = "selectedDriverId"
    
    
  }

  // MARK: Properties
  public var lowestQuote: LowestQuote?
  public var carType: String?
  public var endLoc: EndLocation?
  public var createdAt: String?
  public var startLoc: StartLocation?
  public var toAddress: String?
  public var riderId: String?
  public var fromAddress: String?
  public var riderSelectedFare: Double?
  public var closed: Bool? = false
  public var id: String?
  public var estDistance: Int?
  public var status: String?
  public var updatedAt: String?
  public var pickupDateTime: String?
  public var estDuration: Int?
    
  public var selectedDriverId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lowestQuote <- map[SerializationKeys.lowestQuote]
    carType <- map[SerializationKeys.carType]
    endLoc <- map[SerializationKeys.endLoc]
    createdAt <- map[SerializationKeys.createdAt]
    startLoc <- map[SerializationKeys.startLoc]
    toAddress <- map[SerializationKeys.toAddress]
    riderId <- map[SerializationKeys.riderId]
    fromAddress <- map[SerializationKeys.fromAddress]
    riderSelectedFare <- map[SerializationKeys.riderSelectedFare]
    closed <- map[SerializationKeys.closed]
    id <- map[SerializationKeys.id]
    estDistance <- map[SerializationKeys.estDistance]
    status <- map[SerializationKeys.status]
    updatedAt <- map[SerializationKeys.updatedAt]
    pickupDateTime <- map[SerializationKeys.pickupDateTime]
    estDuration <- map[SerializationKeys.estDuration]
    selectedDriverId <- map[SerializationKeys.selectedDriverId]
    
    
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lowestQuote { dictionary[SerializationKeys.lowestQuote] = value.dictionaryRepresentation() }
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = endLoc { dictionary[SerializationKeys.endLoc] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = startLoc { dictionary[SerializationKeys.startLoc] = value.dictionaryRepresentation() }
    if let value = toAddress { dictionary[SerializationKeys.toAddress] = value }
    if let value = riderId { dictionary[SerializationKeys.riderId] = value }
    if let value = fromAddress { dictionary[SerializationKeys.fromAddress] = value }
    if let value = riderSelectedFare { dictionary[SerializationKeys.riderSelectedFare] = value }
    dictionary[SerializationKeys.closed] = closed
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = estDistance { dictionary[SerializationKeys.estDistance] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = pickupDateTime { dictionary[SerializationKeys.pickupDateTime] = value }
    if let value = estDuration { dictionary[SerializationKeys.estDuration] = value }
    
    if let value = selectedDriverId { dictionary[SerializationKeys.selectedDriverId] = value }
    
    
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lowestQuote = aDecoder.decodeObject(forKey: SerializationKeys.lowestQuote) as? LowestQuote
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.endLoc = aDecoder.decodeObject(forKey: SerializationKeys.endLoc) as? EndLocation
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.startLoc = aDecoder.decodeObject(forKey: SerializationKeys.startLoc) as? StartLocation
    self.toAddress = aDecoder.decodeObject(forKey: SerializationKeys.toAddress) as? String
    self.riderId = aDecoder.decodeObject(forKey: SerializationKeys.riderId) as? String
    self.fromAddress = aDecoder.decodeObject(forKey: SerializationKeys.fromAddress) as? String
    self.riderSelectedFare = aDecoder.decodeObject(forKey: SerializationKeys.riderSelectedFare) as? Double
    self.closed = aDecoder.decodeBool(forKey: SerializationKeys.closed)
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.estDistance = aDecoder.decodeObject(forKey: SerializationKeys.estDistance) as? Int
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.pickupDateTime = aDecoder.decodeObject(forKey: SerializationKeys.pickupDateTime) as? String
    self.estDuration = aDecoder.decodeObject(forKey: SerializationKeys.estDuration) as? Int
     self.selectedDriverId = aDecoder.decodeObject(forKey: SerializationKeys.selectedDriverId) as? String
    
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lowestQuote, forKey: SerializationKeys.lowestQuote)
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(endLoc, forKey: SerializationKeys.endLoc)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(startLoc, forKey: SerializationKeys.startLoc)
    aCoder.encode(toAddress, forKey: SerializationKeys.toAddress)
    aCoder.encode(riderId, forKey: SerializationKeys.riderId)
    aCoder.encode(fromAddress, forKey: SerializationKeys.fromAddress)
    aCoder.encode(riderSelectedFare, forKey: SerializationKeys.riderSelectedFare)
    aCoder.encode(closed, forKey: SerializationKeys.closed)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(estDistance, forKey: SerializationKeys.estDistance)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(pickupDateTime, forKey: SerializationKeys.pickupDateTime)
    aCoder.encode(estDuration, forKey: SerializationKeys.estDuration)
    aCoder.encode(selectedDriverId, forKey: SerializationKeys.selectedDriverId)
    
  }

}
