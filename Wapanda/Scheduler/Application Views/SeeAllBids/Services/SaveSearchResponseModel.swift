import UIKit
import ObjectMapper

class SaveSearchResponseModel: Mappable {
    
    var fareAbove: Int64 = 0
    var destAddress = ""
    var driverId = ""
    var id = ""
    var title = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        
        id <- map["_id"]
        fareAbove <- map["fareAbove"]
        destAddress <- map["destAddress"]
        driverId <- map["driverId"]
        title <- map["title"]
    }
}
