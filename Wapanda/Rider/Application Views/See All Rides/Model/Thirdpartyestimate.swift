//
//  Thirdpartyestimate.swift
//
//  Created by  on 08/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Thirdpartyestimate: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fairPrice = "fairPrice"
    static let typicalPrice = "typicalPrice"
    static let minimumPrice = "minimumPrice"
    static let maximumPrice = "maximumPrice"
    static let fairPriceHigh = "fairPriceHigh"
    static let fairPriceLow = "fairPriceLow"
  }

  // MARK: Properties
  public var fairPrice: Float?
  public var typicalPrice: Float?
  public var minimumPrice: Float?
  public var maximumPrice: Float?
  public var fairPriceHigh: Float?
  public var fairPriceLow: Float?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    fairPrice <- map[SerializationKeys.fairPrice]
    typicalPrice <- map[SerializationKeys.typicalPrice]
    minimumPrice <- map[SerializationKeys.minimumPrice]
    maximumPrice <- map[SerializationKeys.maximumPrice]
    fairPriceHigh <- map[SerializationKeys.fairPriceHigh]
    fairPriceLow <- map[SerializationKeys.fairPriceLow]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fairPrice { dictionary[SerializationKeys.fairPrice] = value }
    if let value = typicalPrice { dictionary[SerializationKeys.typicalPrice] = value }
    if let value = minimumPrice { dictionary[SerializationKeys.minimumPrice] = value }
    if let value = maximumPrice { dictionary[SerializationKeys.maximumPrice] = value }
    if let value = fairPriceHigh { dictionary[SerializationKeys.fairPriceHigh] = value }
    if let value = fairPriceLow { dictionary[SerializationKeys.fairPriceLow] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.fairPrice = aDecoder.decodeObject(forKey: SerializationKeys.fairPrice) as? Float
    self.typicalPrice = aDecoder.decodeObject(forKey: SerializationKeys.typicalPrice) as? Float
    self.minimumPrice = aDecoder.decodeObject(forKey: SerializationKeys.minimumPrice) as? Float
    self.maximumPrice = aDecoder.decodeObject(forKey: SerializationKeys.maximumPrice) as? Float
    self.fairPriceHigh = aDecoder.decodeObject(forKey: SerializationKeys.fairPriceHigh) as? Float
    self.fairPriceLow = aDecoder.decodeObject(forKey: SerializationKeys.fairPriceLow) as? Float
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(fairPrice, forKey: SerializationKeys.fairPrice)
    aCoder.encode(typicalPrice, forKey: SerializationKeys.typicalPrice)
    aCoder.encode(minimumPrice, forKey: SerializationKeys.minimumPrice)
    aCoder.encode(maximumPrice, forKey: SerializationKeys.maximumPrice)
    aCoder.encode(fairPriceHigh, forKey: SerializationKeys.fairPriceHigh)
    aCoder.encode(fairPriceLow, forKey: SerializationKeys.fairPriceLow)
  }

}
