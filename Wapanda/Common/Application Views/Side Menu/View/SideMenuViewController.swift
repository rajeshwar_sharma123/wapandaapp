

import UIKit
import Stripe
import JHTAlertController

class SideMenuViewController: BaseViewController {
    
    enum SIDE_MENU_OPTIONS: Int{
        case PAYMENTS
        case TRIP
        case BID
        case HELP
        case EMERGENCY
        case DRIVER_SIDE
        case SETTINGS
        case LOGOUT
    }
    
    @IBOutlet weak var tblView: UITableView!
    var presenterBankInfo : BankInfoPresenter!
    //Side Menu Data Source
    var dataSource: [(name: String, image: UIImage, subTitle: String)] = []
    
    //MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppUtility.isUserLogin() && !AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER){
            super.showLoader(self)
            
            StripePaymentManager.shared.refreshCustomerContextWithCompletionBlock { (flag) in
                StripePaymentManager.shared.isCardAdded = flag
                super.hideLoader(self)
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.presenterBankInfo = BankInfoPresenter(delegate: self)
        if AppUtility.isUserLogin(){
            self.prepareDataSource()

        }else{
            self.prepareDataSourceForNonLogged()
        }
        self.viewCustomization()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Customize View
    
    /// Method used to customize view appearance
    func viewCustomization(){
        self.tableViewInitalization()
    }
    
   
    func prepareDataSource(){
        if AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER){
            //DRIVER SIDE MENU DATA SOURCE
            self.dataSource = [
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_PAYMENT_TITLE, #imageLiteral(resourceName: "ic_payment"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_TRIP_TITLE, #imageLiteral(resourceName: "ic_trip"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_BID_TITLE,#imageLiteral(resourceName: "allBids"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_RATING_TITLE, #imageLiteral(resourceName: "ic_star"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_HELP_TITLE, #imageLiteral(resourceName: "ic_help"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_DOCUMENTS_TITLE, #imageLiteral(resourceName: "ic_documents"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_RIDER_SIDE_TITLE, #imageLiteral(resourceName: "ic_rider_side"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_SETTINGS_TITLE, #imageLiteral(resourceName: "ic_settings"), ""),
            ]
        }
        else{
            //RIDER SIDE MENU DATA SOURCE 
            
            
            print(StripePaymentManager.shared.isCardAdded)
            
            self.dataSource = [
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_PAYMENT_TITLE, #imageLiteral(resourceName: "ic_payment"), StripePaymentManager.shared.isCardAdded ? "" : AppConstants.ScreenSpecificConstant.SideMenuScreen.PAYMENT_SUBTITLE),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_TRIP_TITLE, #imageLiteral(resourceName: "ic_trip"), ""),(AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_BID_TITLE, #imageLiteral(resourceName: "ic_trip"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_HELP_TITLE, #imageLiteral(resourceName: "ic_help"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_EMERGENCY_TITLE, #imageLiteral(resourceName: "ic_emergency"), (AppUtility.isUserLogin() && AppDelegate.sharedInstance.userInformation.emergencyContact != nil) ? "" : AppConstants.ScreenSpecificConstant.SideMenuScreen.SETUP_EMERGENCY_TITLE),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_DRIVER_SIDE_TITLE, #imageLiteral(resourceName: "ic_driver_side"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_SETTINGS_TITLE, #imageLiteral(resourceName: "ic_settings"), "")
            ]
        }
        
    }
    
    
    
    func prepareDataSourceForNonLogged(){
        if AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.NON_LOGGED_IS_DRIVER){
            //DRIVER SIDE MENU DATA SOURCE
            self.dataSource = [
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_PAYMENT_TITLE, #imageLiteral(resourceName: "ic_payment"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_TRIP_TITLE, #imageLiteral(resourceName: "ic_trip"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_BID_TITLE, #imageLiteral(resourceName: "ic_trip"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_RATING_TITLE, #imageLiteral(resourceName: "ic_star"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_HELP_TITLE, #imageLiteral(resourceName: "ic_help"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_DOCUMENTS_TITLE, #imageLiteral(resourceName: "ic_documents"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_RIDER_SIDE_TITLE, #imageLiteral(resourceName: "ic_rider_side"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreenDriver.ITEM_SETTINGS_TITLE, #imageLiteral(resourceName: "ic_settings"), ""),
            ]
        }
        else{
            //RIDER SIDE MENU DATA SOURCE
            self.dataSource = [
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_PAYMENT_TITLE, #imageLiteral(resourceName: "ic_payment"),""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_TRIP_TITLE, #imageLiteral(resourceName: "ic_trip"), ""),
                   (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_BID_TITLE, #imageLiteral(resourceName: "ic_trip"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_HELP_TITLE, #imageLiteral(resourceName: "ic_help"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_EMERGENCY_TITLE, #imageLiteral(resourceName: "ic_emergency"),""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_DRIVER_SIDE_TITLE, #imageLiteral(resourceName: "ic_driver_side"), ""),
                (AppConstants.ScreenSpecificConstant.SideMenuScreen.ITEM_SETTINGS_TITLE, #imageLiteral(resourceName: "ic_settings"), "")
            ]
        }
    }

    
    
    /// Method to initalize table view
    func tableViewInitalization(){
        self.tblView.registerTableViewCell(tableViewCell: SideMenuItemTableViewCell.self)
        self.tblView.registerTableViewHeaderFooterView(tableViewHeaderFooter: SideMenuHeaderFooterView.self)
        
        self.tblView.reloadData()
    }
    
    @IBAction func btnDismissSideMenuClick(_ sender: Any) {
        self.closeSideMenu()
    }
    
    func closeSideMenu(){
        AppDelegate.sharedInstance.kMainViewController?.toggleRightView(animated: true, completionHandler: nil)
    }
}

//MARK: UITable View Delegate and datasource
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.getHeader(withHeaderType: SideMenuHeaderFooterView.self)
        let name = (AppDelegate.sharedInstance.userInformation?.firstName?.capitalizeWordsInSentence() ?? AppConstants.ScreenSpecificConstant.SideMenuScreen.ANONYMOUS_USER_TITLE) + " " + (AppDelegate.sharedInstance.userInformation?.lastName?.capitalizeWordsInSentence() ?? "")
        
        if AppUtility.isUserLogin(){
            header.btnLogin.isHidden = true
            let rating = AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER) ? (AppDelegate.sharedInstance.userInformation.driverProfile?.averageRating != nil ? String(format: "%.1f", AppDelegate.sharedInstance.userInformation.driverProfile!.averageRating!): "0.0"): nil
            header.bind(withUserName: name, rating: rating, imgId: (AppUtility.isUserLogin() && (AppDelegate.sharedInstance.userInformation.profileImageFileId != nil)) ? AppDelegate.sharedInstance.userInformation.profileImageFileId! : "")

        }else{
            
            header.btnLogin.isHidden = false

            header.bind(withUserName: AppConstants.ScreenSpecificConstant.SideMenuScreen.ANONYMOUS_USER_TITLE, rating: nil, imgId: (AppUtility.isUserLogin() && (AppDelegate.sharedInstance.userInformation.profileImageFileId != nil)) ? AppDelegate.sharedInstance.userInformation.profileImageFileId! : "")
        
        }
        header.delegate = self
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.getCell(withCellType: SideMenuItemTableViewCell.self)
        let dataSourceCell = self.dataSource[indexPath.row]
        cell.bind(withItemName: dataSourceCell.name, imgItem: dataSourceCell.image, subTitle: dataSourceCell.subTitle)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if AppUtility.isUserLogin(){
            if AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER){
                
                    self.navigateToDriverScreenWithSelectedIndex(index: indexPath.row)
            }
            else{
                    self.navigateToScreenWithSelectedIndex(index: indexPath.row)
                }
            }
        else{
            //AppInitialViewHandler.sharedInstance.setupInitialViewController()
            

            if AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.NON_LOGGED_IS_DRIVER){
                if SIDE_MENU_OPTION_DRIVER(rawValue: indexPath.row) == .HELP{
                self.navigateToDriverScreenWithSelectedIndex(index: indexPath.row)
                }else{
                    AppInitialViewHandler.sharedInstance.setupInitialViewController()

                }
            }
            else{
                if SIDE_MENU_OPTIONS(rawValue: indexPath.row) == .HELP {
                    self.navigateToScreenWithSelectedIndex(index: indexPath.row)
                }else{
                    AppInitialViewHandler.sharedInstance.setupInitialViewController()
                }
            }
        }
    }
    
    func navigateToScreenWithSelectedIndex(index: Int){
        //        case SIDE_MENU_OPTIONS.CONNECTION.rawValue:
        //self.connectionsClicked()
        switch index {
        case SIDE_MENU_OPTIONS.PAYMENTS.rawValue:
           
            self.paymentsClicked()
        case SIDE_MENU_OPTIONS.TRIP.rawValue:
            self.closeSideMenu()
            self.tripsClicked()
        case SIDE_MENU_OPTIONS.BID.rawValue:
            self.closeSideMenu()
            self.yourBidsClicked()
        case SIDE_MENU_OPTIONS.HELP.rawValue:
            self.closeSideMenu()
            self.helpClicked()
        case SIDE_MENU_OPTIONS.EMERGENCY.rawValue:
            self.closeSideMenu()
            self.emergencyClicked()
        case SIDE_MENU_OPTIONS.DRIVER_SIDE.rawValue:
            self.closeSideMenu()
            self.navigateToScreenBasedOnConfirmation()
        case SIDE_MENU_OPTIONS.SETTINGS.rawValue:
            self.closeSideMenu()
            self.settingsClicked()
        case SIDE_MENU_OPTIONS.LOGOUT.rawValue:
            self.closeSideMenu()
            self.logOutClicked()
        default:
            break
        }
    }
    
    
    /// Show payments for user
    private func paymentsClicked(){
        
        if let customer = StripePaymentManager.shared.customer
        {
             self.closeSideMenu()
            if let _ = customer.defaultSource
            {
                self.navigateToAddPayment()
            }
            else
            {
                self.navigateToPaymentMethod()
            }
        }
        else
        {
            self.showLoader()
            let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
            customerContext.retrieveCustomer { (customer, error) in
                self.hideLoader()
                 self.closeSideMenu()
                if let _ = customer,customer?.defaultSource != nil
                {
                    StripePaymentManager.shared.customer = customer
                    if let defaultSource = StripePaymentManager.shared.customer?.defaultSource as? STPSource,defaultSource.type == .card
                    {
                          StripePaymentManager.shared.isCardAdded = true
                    }
                    else
                    {
                          StripePaymentManager.shared.isCardAdded = false
                    }
                  
                    self.navigateToAddPayment()
                    
                }
                else
                {
                    self.navigateToPaymentMethod()
                }
        
        }
    }
}
    
    private func navigateToPaymentMethod()
    {
        
        let paymentMethodVC = UIViewController.getViewController(PaymentMethodViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        paymentMethodVC.isComingFromSideMenu = true

        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(paymentMethodVC, animated: true)
    }
    
    private func navigateToAddPayment()
    {
        let paymentVC = UIViewController.getViewController(AddPaymentScreenViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
           paymentVC.isComingFromSideMenu = true

        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    /// Show trips for user
    private func tripsClicked(){
        let tripsVC = UIViewController.getViewController(TripListingViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(tripsVC, animated: true)
        
    }
    
    /// Show your bids for user
    private func yourBidsClicked(){
        let yourBidVC = UIViewController.getViewController(YourBidsViewController.self, storyboard: UIStoryboard.Storyboard.Scheduling.object)
  UIApplication.shared.visibleViewController?.navigationController?.pushViewController(yourBidVC, animated: true)
        
    }
    
    /// Show connections for user
    private func connectionsClicked(){
        
    }
    
    /// Show help for user
    private func helpClicked(){
        
        let riderHelpVC = UIViewController.getViewController(HelpViewController.self,storyboard: UIStoryboard.Storyboard.Rider.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(riderHelpVC, animated: true)
        
    }
    
    /// Show emergency for user
    private func emergencyClicked(){
        
        let emergencyVC = UIViewController.getViewController(EmergencyContactViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        
        if let _ = AppDelegate.sharedInstance.userInformation.emergencyContact{
            let viewModelEmegency = EmergencyContactViewRequestModel(emergencyContact: AppDelegate.sharedInstance.userInformation.emergencyContact, userId: AppDelegate.sharedInstance.userInformation.id!, emergencyContactName: AppDelegate.sharedInstance.userInformation.emergencyContactName, emergencyContactImageId: AppDelegate.sharedInstance.userInformation.emergencyContactImageId)
            emergencyVC.bind(withViewData: viewModelEmegency)
        }
        
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(emergencyVC, animated: true)
    }
    
    /// Show driver side for user
    private func driverSideClicked(){

        if AppUtility.isUserLogin(){
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER, value: true)
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
            LocationServiceManager.sharedInstance.startDriverUpdateTimer(withtimeInterVal: 3)
        }
        else{
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.NON_LOGGED_IS_DRIVER, value: true)
            AppInitialViewHandler.sharedInstance.setupInitialViewControllerForNonLoggedUser()

           // self.navigateToDriverHomeScreen()
        }
        
        
    }
    
    //String(format:AppConstants.ApiEndPoints.HELP,"DRIVER")
    
     func navigateToScreenBasedOnConfirmation(){
        
        let alertController = self.initialseAlertController(withTitle: "Alert", andMessage: (AppUtility.isUserLogin() ? ((AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER)) ? String(format:AppConstants.ScreenSpecificConstant.SideMenuScreen.SWITCH_ROLE_MESSAGE,"Rider"): String(format:AppConstants.ScreenSpecificConstant.SideMenuScreen.SWITCH_ROLE_MESSAGE,"Driver")): ""))
        
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.YES_TITLE, style: .default, handler: { (alertAction) in
            
            if AppUtility.isUserLogin(){
                if AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER){
                    self.riderSideClicked()
                    
                }else{
                    self.driverSideClicked()
                }
            }
        }))
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.NO_TITLE, style: .default, handler: { (alertAction) in
                self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func navigateToDriverWelcomeViewController(){
        
        let driverWelcomeVC = UIViewController.getViewController(DriverWelcomeScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        driverWelcomeVC.objDriverWelcomeProfile = LoginResponseModel.retriveUser()
        self.navigationController?.pushViewController(driverWelcomeVC, animated: true)
    }
    
    /// Show settings for user
    private func settingsClicked(){
        let resetPasswordVC = UIViewController.getViewController(ResetPasswordViewController.self,storyboard: UIStoryboard.Storyboard.Main.object)
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(resetPasswordVC, animated: true)
    }
    
    /// Perform logout for user
    private func logOutClicked(){
        self.logout()
    }
    
    func moveToRiderProfile(){
        if AppUtility.isUserLogin(){
            let profile = UIViewController.getViewController(RiderProfileViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            UIApplication.shared.visibleViewController?.navigationController?.pushViewController(profile, animated: true)
        }
    }
}

extension SideMenuViewController: SideMenuHeaderFooterViewDelegate{
    func profileClicked(){
        self.closeSideMenu()
        
        if AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER){
            self.moveToDriverProfile()
        }
        else{
            self.moveToRiderProfile()
        }
        
    }
}
