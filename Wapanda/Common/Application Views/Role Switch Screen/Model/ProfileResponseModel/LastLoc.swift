//
//  LastLoc.swift
//
//  Created by  on 8/30/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class LastLoc:  NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updatedAt"
    static let coordinates = "coordinates"
    static let id = "_id"
    static let type = "type"
    static let createdAt = "createdAt"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var coordinates: [Float]?
  public var id: String?
  public var type: String?
  public var createdAt: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    coordinates <- map[SerializationKeys.coordinates]
    id <- map[SerializationKeys.id]
    type <- map[SerializationKeys.type]
    createdAt <- map[SerializationKeys.createdAt]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.coordinates = aDecoder.decodeObject(forKey: SerializationKeys.coordinates) as? [Float]
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(coordinates, forKey: SerializationKeys.coordinates)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
  }

}
