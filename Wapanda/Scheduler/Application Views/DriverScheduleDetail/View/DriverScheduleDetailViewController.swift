//
//  DriverScheduleDetailViewController.swift
//  Wapanda
//
//  Created by Daffodil on 24/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DriverScheduleDetailViewController: BaseViewController {

    
    @IBOutlet weak var tableView: UITableView!
    var tripItem: Trip!
    private var driverScheduleDetailViewPresenter: DriverScheduleDetailPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        intialSetupForView()
    }

    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.setupNavigationBar()
        self.driverScheduleDetailViewPresenter = DriverScheduleDetailPresenter(delegate: self)
        //setData()
    }
    /**
     This method is used to setup navigationBar
     */
    
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, color: UIColor.schedulingThemeColor(),isTranslucent: false)
        self.customizeNavigationBackButton()
    }
    /**
     This method is used to map data
     */
    func setData(){
        
//        if let routeImgId = tripItem.tripId?.routeImageId{
//            mapImageView.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: routeImgId))!, placeholderImage:#imageLiteral(resourceName: "map_placeholder"))
//        }
//        priceLabel.text = AppUtility.getFormattedPriceString(withPrice: (tripItem.lowestQuote?.price)!)
//        travelTimeLabel.text = AppUtility.getDriverApproxDriveTime(tripItem.estDuration!,hourStyle:"h", mintStyle: "minutes")
    }
    
    override func errorAlertConfirmButtonClicked(){
        //call service for the confirmation
        self.driverScheduleDetailViewPresenter.sendStartPickUpConfirmationRequest(tripId: tripItem.id!, latFrom:LocationServiceManager.sharedInstance.currentLocation?.coordinate.latitude, lngFrom: LocationServiceManager.sharedInstance.currentLocation?.coordinate.longitude )
    }
}
extension DriverScheduleDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell: DriverScheduleDetailCell!
        switch indexPath.row {
        case 0:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "YourBidDetailCell") as! DriverScheduleDetailCell!
            if let routeImgId = tripItem.routeImageId{
                cell.mapImageView.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: routeImgId))!, placeholderImage:#imageLiteral(resourceName: "map_placeholder"))
            }
            cell.priceLabel.text = AppUtility.getFormattedPriceString(withPrice: (tripItem.agreedPrice)!)
            cell.travelTimeLabel.text = AppUtility.getDriverApproxDriveTime(tripItem.estDuration!,hourStyle:"h", mintStyle: "minutes")
            cell.distanceLabel.text = "\(AppUtility.getTripDistanceInMiles(Int(tripItem.estDistance ?? 0.0))) miles"
           
        case 1:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "SourceDestinationDetailCell") as! DriverScheduleDetailCell!
            cell.sourceLabel.text = tripItem.fromAddress!
            cell.destinationLabel.text = tripItem.toAddress!
            //cell.currentBidLabel.text = AppUtility.getFormattedPriceString(withPrice: (tripItem.agreedPrice)!)
        case 2:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "DateDetailCell") as! DriverScheduleDetailCell!
            cell.dateTimeLabel.text = AppUtility.getTripDateFromDateString((tripItem.pickupByTime)!)
            
        case 3:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "PickupRiderCell") as! DriverScheduleDetailCell!
            cell.pickupRiderButton.addTarget(self, action:#selector(self.pickupRiderButtonAction), for: .touchUpInside)
            let pickupEnabled:Bool = self.isDriverAllowedForPickUp(forTime:(tripItem.pickupByTime)!)
            if pickupEnabled{
                cell.pickupRiderButton.isEnabled = true
            }else{
                cell.pickupRiderButton.isEnabled = false
                cell.pickupRiderButton.backgroundColor = UIColor.gray
            }
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 290
        case 1:
            return 100
        case 2:
            return 65
        case 3:
            return 110
        default:
            return 0
        }
    }
    
    func pickupRiderButtonAction() {
        self.showErrorAlertWithCancelAndConfirmButtons(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.READY_FOR_PICKUP, VC: self)
    }

   
    func isDriverAllowedForPickUp(forTime : String) -> Bool {
        if let pickUpDate : Date = AppUtility.getDateFromString(dateStr: forTime){
            let currentDate:Date = Date()
            let twoHrBackOfPickUpDate: Date = Calendar.current.date(byAdding: .hour, value: -2, to: pickUpDate)!
            if currentDate >= twoHrBackOfPickUpDate
            {
                return true
            }
        }else{
            print("Not Able to convert")
        }
        return false
    }
}


// MARK: - DriverScheduleDetailViewDelegate Methods
extension DriverScheduleDetailViewController: DriverScheduleDetailViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func pickUpConfirmed(withResponseModel response: CancelResponseModel){
        self.navigateToDriverTripViewController(withTrip : response.trip!)
    }
    
    func navigateToDriverTripViewController(withTrip:Trip){
        let driverTripVC: DriverTripViewController = UIViewController.getViewController(DriverTripViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        driverTripVC.pushNotificationModel = getPushNotificationObject(withTrip:withTrip)
        self.navigationController?.pushViewController(driverTripVC, animated: true)
    }
    
    func getPushNotificationObject(withTrip:Trip)-> PushNotificationObjectModel{
        let pushNotificationModel = PushNotificationObjectModel(JSON:[:])
        pushNotificationModel?.trip = withTrip
        return pushNotificationModel!
    }
    
    
}


