//
//  Items.swift
//
//  Created by  on 21/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class TripItems:NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carType = "carType"
    static let lowestQuote = "lowestQuote"
    static let tripId = "tripId"
    static let endLoc = "endLoc"
    static let selectedDriverId = "selectedDriverId"
    static let createdAt = "createdAt"
    static let closeBy = "closeBy"
    static let startLoc = "startLoc"
    static let toAddress = "toAddress"
    static let riderId = "riderId"
    static let fromAddress = "fromAddress"
    static let status = "status"
    static let closed = "closed"
    static let id = "_id"
    static let yourBid = "yourBid"
    static let riderSelectedFare = "riderSelectedFare"
    static let estDistance = "estDistance"
    static let updatedAt = "updatedAt"
    static let selectedDriverFair = "selectedDriverFair"
    static let pickupDateTime = "pickupDateTime"
    static let estDuration = "estDuration"
    
    
  }

  // MARK: Properties
  public var carType: String?
  public var lowestQuote: LowestQuote?
  public var tripId: TripIdResponseModel?
  public var endLoc: EndLocation?
  public var selectedDriverId: String?
  public var createdAt: String?
  public var closeBy: String?
  public var startLoc: StartLocation?
  public var toAddress: String?
  public var riderId: String?
  public var fromAddress: String?
  public var status: String?
  public var closed: Bool? = false
  public var id: String?
  public var yourBid: YourBid?
  public var riderSelectedFare: Double?
  public var estDistance: Int?
  public var updatedAt: String?
  public var selectedDriverFair: Double?
  public var pickupDateTime: String?
  public var estDuration: UIntMax?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    carType <- map[SerializationKeys.carType]
    lowestQuote <- map[SerializationKeys.lowestQuote]
    tripId <- map[SerializationKeys.tripId]
    endLoc <- map[SerializationKeys.endLoc]
    selectedDriverId <- map[SerializationKeys.selectedDriverId]
    createdAt <- map[SerializationKeys.createdAt]
    closeBy <- map[SerializationKeys.closeBy]
    startLoc <- map[SerializationKeys.startLoc]
    toAddress <- map[SerializationKeys.toAddress]
    riderId <- map[SerializationKeys.riderId]
    fromAddress <- map[SerializationKeys.fromAddress]
    status <- map[SerializationKeys.status]
    closed <- map[SerializationKeys.closed]
    id <- map[SerializationKeys.id]
    yourBid <- map[SerializationKeys.yourBid]
    riderSelectedFare <- map[SerializationKeys.riderSelectedFare]
    estDistance <- map[SerializationKeys.estDistance]
    updatedAt <- map[SerializationKeys.updatedAt]
    selectedDriverFair <- map[SerializationKeys.selectedDriverFair]
    pickupDateTime <- map[SerializationKeys.pickupDateTime]
    estDuration <- map[SerializationKeys.estDuration]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = lowestQuote { dictionary[SerializationKeys.lowestQuote] = value.dictionaryRepresentation() }
    if let value = tripId { dictionary[SerializationKeys.tripId] = value }
    if let value = endLoc { dictionary[SerializationKeys.endLoc] = value.dictionaryRepresentation() }
    if let value = selectedDriverId { dictionary[SerializationKeys.selectedDriverId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = closeBy { dictionary[SerializationKeys.closeBy] = value }
    if let value = startLoc { dictionary[SerializationKeys.startLoc] = value.dictionaryRepresentation() }
    if let value = toAddress { dictionary[SerializationKeys.toAddress] = value }
    if let value = riderId { dictionary[SerializationKeys.riderId] = value }
    if let value = fromAddress { dictionary[SerializationKeys.fromAddress] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    dictionary[SerializationKeys.closed] = closed
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = yourBid { dictionary[SerializationKeys.yourBid] = value.dictionaryRepresentation() }
    if let value = riderSelectedFare { dictionary[SerializationKeys.riderSelectedFare] = value }
    if let value = estDistance { dictionary[SerializationKeys.estDistance] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = selectedDriverFair { dictionary[SerializationKeys.selectedDriverFair] = value }
    if let value = pickupDateTime { dictionary[SerializationKeys.pickupDateTime] = value }
    if let value = estDuration { dictionary[SerializationKeys.estDuration] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.lowestQuote = aDecoder.decodeObject(forKey: SerializationKeys.lowestQuote) as? LowestQuote
    self.tripId = aDecoder.decodeObject(forKey: SerializationKeys.tripId) as? TripIdResponseModel
    self.endLoc = aDecoder.decodeObject(forKey: SerializationKeys.endLoc) as? EndLocation
    self.selectedDriverId = aDecoder.decodeObject(forKey: SerializationKeys.selectedDriverId) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.closeBy = aDecoder.decodeObject(forKey: SerializationKeys.closeBy) as? String
    self.startLoc = aDecoder.decodeObject(forKey: SerializationKeys.startLoc) as? StartLocation
    self.toAddress = aDecoder.decodeObject(forKey: SerializationKeys.toAddress) as? String
    self.riderId = aDecoder.decodeObject(forKey: SerializationKeys.riderId) as? String
    self.fromAddress = aDecoder.decodeObject(forKey: SerializationKeys.fromAddress) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.closed = aDecoder.decodeObject(forKey: SerializationKeys.closed) as? Bool
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.yourBid = aDecoder.decodeObject(forKey: SerializationKeys.yourBid) as? YourBid
    self.riderSelectedFare = aDecoder.decodeObject(forKey: SerializationKeys.riderSelectedFare) as? Double
    self.estDistance = aDecoder.decodeObject(forKey: SerializationKeys.estDistance) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.selectedDriverFair = aDecoder.decodeObject(forKey: SerializationKeys.selectedDriverFair) as? Double
    self.pickupDateTime = aDecoder.decodeObject(forKey: SerializationKeys.pickupDateTime) as? String
    self.estDuration = aDecoder.decodeObject(forKey: SerializationKeys.estDuration) as? UIntMax
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(lowestQuote, forKey: SerializationKeys.lowestQuote)
    aCoder.encode(tripId, forKey: SerializationKeys.tripId)
    aCoder.encode(endLoc, forKey: SerializationKeys.endLoc)
    aCoder.encode(selectedDriverId, forKey: SerializationKeys.selectedDriverId)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(closeBy, forKey: SerializationKeys.closeBy)
    aCoder.encode(startLoc, forKey: SerializationKeys.startLoc)
    aCoder.encode(toAddress, forKey: SerializationKeys.toAddress)
    aCoder.encode(riderId, forKey: SerializationKeys.riderId)
    aCoder.encode(fromAddress, forKey: SerializationKeys.fromAddress)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(closed, forKey: SerializationKeys.closed)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(yourBid, forKey: SerializationKeys.yourBid)
    aCoder.encode(riderSelectedFare, forKey: SerializationKeys.riderSelectedFare)
    aCoder.encode(estDistance, forKey: SerializationKeys.estDistance)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(selectedDriverFair, forKey: SerializationKeys.selectedDriverFair)
    aCoder.encode(pickupDateTime, forKey: SerializationKeys.pickupDateTime)
    aCoder.encode(estDuration, forKey: SerializationKeys.estDuration)
  }

}
