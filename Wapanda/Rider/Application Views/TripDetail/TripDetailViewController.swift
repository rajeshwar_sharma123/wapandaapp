//
//  TripDetailViewController.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 14/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class TripDetailViewController: BaseViewController {
    
    var tripDetailSource : HistoryItems!
    @IBOutlet weak var imgViewRoute: UIImageView!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var imgViewDriver: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTotalBillAmt: UILabel!
    @IBOutlet weak var lblTipPercent: UILabel!
    @IBOutlet weak var lblTipAmt: UILabel!
    @IBOutlet weak var lblRideFare: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblBillAmt: UILabel!
    @IBOutlet weak var lblRating: UILabel!
   // @IBOutlet weak var lblPlateNo: UILabel!
    @IBOutlet weak var lblDriverNAme: UILabel!
    @IBOutlet weak var lblStop: UILabel!
    @IBOutlet weak var _imgViewDashedLineHeightConstraint: NSLayoutConstraint!
   
    
    @IBOutlet weak var _lblAddStopHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetupForView()
    }
    
    
    //MARK:- Helper Methods
    
    /**
     This function setup the Navigation bar
     */
    private func initialSetupForView(){
        setupNavigationBar()
        setTripDetails()
    }
    
    
    /**
     This function setup the Navigation bar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitleImage(image: #imageLiteral(resourceName: "ic_panda_navigation"))
        self.customizeNavigationBackButton(color: UIColor.black,image:#imageLiteral(resourceName: "ic_chevron_left"))
    }
    
    /**
     This function setup the Navigation bar
     */
    private func setTripDetails(){
        
        //Set Route Image
        if let routeID = tripDetailSource?.routeImageId {
            let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: routeID))!
            imgViewRoute.setImageWith(imageUrl, placeholderImage: #imageLiteral(resourceName: "map_placeholder"))
            
        }
        
        if let profileID = tripDetailSource?.driver?.profileImageFileId {
            
            let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: profileID))!
                        
            imgViewDriver.setImageWith(URLRequest.init(url: imageUrl), placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"), success: { (request, response, image) in
                self.imgViewDriver.image = image
                self.imgViewDriver.contentMode = .scaleAspectFit
            }, failure: { (request, response, error) in
                
                self.imgViewDriver.image = #imageLiteral(resourceName: "ic_account_placeholder")
                self.imgViewDriver.contentMode = .center
            })
            
            //Image & Image Bg View Customization
            self.imgViewDriver.clipsToBounds = true
            self.imgViewDriver.clipsToBounds = true
            self.imgViewDriver.layer.cornerRadius = self.imgViewDriver.frame.width/2
            self.imgViewDriver.layer.cornerRadius = self.imgViewDriver.frame.width/2
            self.imgViewDriver.layer.borderColor = UIColor.white.cgColor
            self.imgViewDriver.layer.borderWidth = 2.0
          
        }
        
        if let rating = tripDetailSource?.driver?.driverProfile?.averageRating{
            lblRating.text = String(format:"%.1f", rating)
        }else{
            lblRating.text = String(format:"%.1f", 0)
        }
        
        if let stopAddress = tripDetailSource.stops,stopAddress.count > 0{
            
            lblStop.text = stopAddress[0].address ?? ""
            _imgViewDashedLineHeightConstraint.constant = 18
            _lblAddStopHeightConstraint.constant = 24

        }else{
            lblStop.text =  ""
            _imgViewDashedLineHeightConstraint.constant = 0

            _lblAddStopHeightConstraint.constant = 0


        }
        

        
        lblDestination.text = tripDetailSource.toAddress ?? ""
        lblSource.text = tripDetailSource.fromAddress ?? ""
      //  lblStop.text
        lblCarType.text = tripDetailSource?.vehicle?.carType?.vehicleType?.capitalizeWordsInSentence() ?? ""
      //  lblPlateNo.text = tripDetailSource?.vehicle?.carPlateNumber?.uppercased() ?? ""
        lblDriverNAme.text = "\(tripDetailSource?.driver?.firstName?.capitalizeWordsInSentence() ?? "") \(tripDetailSource?.driver?.lastName?.capitalizeWordsInSentence() ?? "")"
        
        if let startTime = tripDetailSource?.startTime{
            lblTime.text =  AppUtility.getFormattedTimeDuration(fromTimeStamp: startTime)
            
        }else{
            lblTime.text =  ""
        }
        
        setBillDetails()
    }
    
    
    
    //Set Bill Details
    private func setBillDetails(){
        
        var totalAmountPayable = 0.0
        var totalRideAmount = 0.0
        var tipPercent  = 0
        var tipAmount  = 0.0
        
        if let amountPayable = self.tripDetailSource?.payment?.amountPayable
        {
            totalAmountPayable = Double(amountPayable)
            
        }
        if let totalTipPercent = self.tripDetailSource?.payment?.compTipPercent
        {
            tipPercent = totalTipPercent
        }
        if let totalTipAmount = self.tripDetailSource?.payment?.compTip
        {
            tipAmount = Double(totalTipAmount)
        }
        if let rideAmount = self.tripDetailSource?.payment?.compFare
        {
            totalRideAmount = Double(rideAmount)
        }
        if tipPercent != 0
        {
            self.lblTipPercent.text = String(format: "Tip (%d%%)",tipPercent)
        }
        else
        {
            self.lblTipAmt.text = String(format: "Tip (%d%%)",tipPercent)
        }
        self.lblRideFare.text = AppUtility.getFormattedPriceString(withPrice: totalRideAmount)
        self.lblTipAmt.text = AppUtility.getFormattedPriceString(withPrice: tipAmount)
        self.lblTotalBillAmt.text = AppUtility.getFormattedPriceString(withPrice: totalAmountPayable)
        self.lblBillAmt.text  = self.lblTotalBillAmt.text
        
    }
    
}
