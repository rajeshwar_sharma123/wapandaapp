//
//  ThirdPartyRideListTableViewCell.swift
//  Wapanda
//
//  Created by Daffodil on 11/12/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class ThirdPartyRideListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCabName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    /// Binds the data with cell
    ///
    /// - Parameters:
    ///   - cabName: string value as driver name
    ///   - companyName: string value as company name
    ///   - price: double value as price
    func bind(withCabName cabName: String, companyName: String,price: Double,imgCompany: UIImage){
        self.lblCabName.text = cabName.capitalizeWordsInSentence()
        self.lblPrice.text = String(format: "$%0.2f", price)
        self.lblCompanyName.text = companyName
    }

}
