
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct CreateBidViewRequestModel {
    
    var latFrom                   : Double!
    var lngFrom                   : Double!
    var latTo                   : Double!
    var lngTo                   : Double!
    var driverId                   : String!
    var riderId                   : String!
    var estDistance                   : UIntMax!
    var estDuration                   : UIntMax!
    var startTime                   : String!
    var riderCounterPrice                   : Double? //Send while counter bidding but not in case of create bid
    var fromAddress                   : String!
    var toAddress                   : String!
}
