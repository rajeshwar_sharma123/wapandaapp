

import UIKit
import GoogleMaps

protocol CabSelectionBottomSheetViewDelegate: class {
    func viewDidDismiss()
    func drawRoute(withSource source: SelectedPlaceModel?, withDestination destination: SelectedPlaceModel?)
    func pushVC(withVC vc: UIViewController)
    func showSideMenuButton()
    func hideSideMenuButton()
}

class CabSelectionBottomSheetViewController: BaseViewController, UserRideDetailProtocol {
    
    weak var delegate: CabSelectionBottomSheetViewDelegate?
    var presenterCabSelection: CabSelectionViewPresenter!
    var isCabSelectionListViewOpen = false
    var cabSelectionResponseModel: CabSelectionResponseModel?
    var cabDataHashMap: [String: [RideList]] = [:]
    var priceSortedCabList: [String: [RideList]] = [:]
    var timeSortedCabList: [String: [RideList]] = [:]
    var selectedCab: [String: RideList] = [:]
    var cabSelectionViewModel: CabSelectionViewRequestModel!
    var timerToGetCabs : Timer!
    var shouldShowLoader : Bool!
    
    //View Bottom - Source, Destination, Schedule, All Rides
    @IBOutlet weak var viewShadowBottomView: UIView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblSourceAddress: UILabel!
    @IBOutlet weak var lblSourceReachingTime: UILabel!
    @IBOutlet weak var lblDestinationAddress: UILabel!
    @IBOutlet weak var lblDestinationReachingTime: UILabel!
    @IBOutlet weak var viewSource: UIView!
    @IBOutlet weak var viewDestination: UIView!
    
    //View - Selected Cab Type
    @IBOutlet weak var viewShadowSelectedCabType: UIView!
    @IBOutlet weak var imgViewSelectedCabType: UIImageView!
    @IBOutlet weak var lblSelectedCabTypeName: UILabel!
    @IBOutlet weak var lblSelectedCabTripPrice: UILabel!
    @IBOutlet weak var lblSelectedCabReachingTime: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewSelectedCabType: UIView!
    @IBOutlet weak var viewNoCab: UIView!
    
    //View-Cab List
    @IBOutlet weak var viewCabList: UIView!
    @IBOutlet weak var tblViewCabList: UITableView!
    @IBOutlet weak var _iwidthAutoLayoutConstraintForCabListView: NSLayoutConstraint!
    let CAB_LIST_ANIMATION_DURATION = 1.0
    let CAB_LIST_CELL_HEIGHT: CGFloat = 60.0
    let CAB_LIST_HEADER_HEIGHT: CGFloat = 40.0
    @IBOutlet weak var viewCabType: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenterCabSelection = CabSelectionViewPresenter(delegate: self)
        addLocationManagerDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initalCabSelectionViewSettings()
        NotificationCenter.default.addObserver(self, selector: #selector(getCabs), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.GET_CAB_LIST), object: nil)
        
        self.hideNavigationBar()
        
        self.shouldShowLoader = true
        self.getCabs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        if let _ = timerToGetCabs
        {
            self.timerToGetCabs.invalidate()
        }
        self.timerToGetCabs = nil
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bind(withDelegate delegate: CabSelectionBottomSheetViewDelegate, viewDataModel data: CabSelectionViewRequestModel){
        self.delegate = delegate
        self.cabSelectionViewModel = data
        
    }
    
    func addLocationManagerDelegate() {
        
        let delegates = UserRideDetail.shared.delegates
        for (index, delegate) in delegates.enumerated() {
            
            if delegate is CabSelectionBottomSheetViewController {
                UserRideDetail.shared.delegates.remove(at: index)
                break
            }
        }
        UserRideDetail.shared.delegates.append(self)
    }
    
    func clearDataSources(){
        cabDataHashMap = [:]
        priceSortedCabList = [:]
        timeSortedCabList = [:]
        selectedCab = [:]
    }
    
    //MARK: Timer Methods
    private func startTimerToGetCabs(){
        
        if let _ = self.timerToGetCabs
        {
            self.timerToGetCabs.invalidate()
            self.timerToGetCabs = nil
        }
        timerToGetCabs = Timer.scheduledTimer(timeInterval: TimeInterval(AppConstants.ScreenSpecificConstant.RiderHomeScreen.TIME_TO_FETCH_CABS), target: self, selector: #selector(getCabs), userInfo: nil, repeats: true)
    }
    //MARK: View Customization Methods
    
    func prepareScreenUI(){
        self.addTapGestureOnMainView()
        self.addTapGestureOnCabSelectionView()
        self.addTapGestureOnSourceView()
        self.addTapGestureOnDestinationView()
        self.updateSourceAndDestinationAddress()
    }
    
    func updateSourceAndDestinationAddress(){
        self.lblSourceAddress.text = self.cabSelectionViewModel.sourceAddress
        self.lblDestinationAddress.text = self.cabSelectionViewModel.destinationAddress
    }
    
    func setSourceAndDestinationReachTime(){
        
        guard self.selectedCab.count > 0 else {return}
        
        let selectedCabKey = self.selectedCab.keys.first!
        let riderTimeSorted = self.timeSortedCabList[selectedCabKey]!.first
        let sourceReachDate = Date().addingTimeInterval(Double((riderTimeSorted!.rideArivalTime)!/1000))
        let destinationReachDate = sourceReachDate.addingTimeInterval(Double(riderTimeSorted!.rideEstimatedTime!/1000))
        self.lblSourceReachingTime.text = AppUtility.getFormattedDate(withFormat: AppConstants.DateConstants.TIME_FORMAT_IN_12_HOUR, date: sourceReachDate)
        self.lblDestinationReachingTime.text = AppUtility.getFormattedDate(withFormat: AppConstants.DateConstants.TIME_FORMAT_IN_12_HOUR, date: destinationReachDate)
    }
    
    func setPlaceholderTextForSourceAndDestinationReachTime(){
        self.lblSourceReachingTime.text = AppConstants.ScreenSpecificConstant.RiderSelectCab.CAR_REACH_TIME_PLACEHODLER
        self.lblDestinationReachingTime.text = AppConstants.ScreenSpecificConstant.RiderSelectCab.CAR_REACH_TIME_PLACEHODLER
    }
    
    func defaultCabSetting(){
        self.getDefaultSelectedCab()
//        self.setdefaultCab()
        self.setSourceAndDestinationReachTime()
    }
    
    func getDefaultSelectedCab(){
        
        if UserRideDetail.shared.isMostSeatAvailableSelected{
            if let suv = self.cabDataHashMap[AppConstants.ScreenSpecificConstant.RiderSelectCab.CAR_TÁPE_SUV_KEY]{
                self.selectedCab[AppConstants.ScreenSpecificConstant.RiderSelectCab.CAR_TÁPE_SUV_KEY] = suv.first
            }
            
            if let xl = self.cabDataHashMap[AppConstants.ScreenSpecificConstant.RiderSelectCab.CART_TYPE_XL_KEY]{
                self.selectedCab[AppConstants.ScreenSpecificConstant.RiderSelectCab.CART_TYPE_XL_KEY] = xl.first
            }
        }
        
        guard self.selectedCab.count == 0 else {return}
            
        //Get Selected Cab
        if let standarCab = self.cabDataHashMap[AppConstants.ScreenSpecificConstant.RiderSelectCab.CAR_TYPE_STANDARD_KEY]{
            self.selectedCab[AppConstants.ScreenSpecificConstant.RiderSelectCab.CAR_TYPE_STANDARD_KEY] = standarCab.first
        }
        else{
            let firstCabKey = self.cabDataHashMap.keys.first
            self.selectedCab[firstCabKey!] = self.cabDataHashMap[firstCabKey!]?.first
        }
    }
    
    func setdefaultCab(){
        guard self.selectedCab.count > 0 else {return}
        
        let key = self.selectedCab.keys.first
        let name = key!.capitalizeWordsInSentence()
        let price = self.priceSortedCabList[key!]!.first!.rideEstimatedFare!
        let arrivalTime = self.timeSortedCabList[key!]!.first!.rideArivalTime!
        let imgUrl = self.cabDataHashMap[key!]!.first!.cabInfo!.type!.icon
        
        self.lblSelectedCabTypeName.text = name
        self.lblSelectedCabReachingTime.text = AppUtility.getTimeInMinutes(fromTimeInMS: arrivalTime)
        self.lblSelectedCabTripPrice.text = String(format: "$%0.2f", price)
        self.imgViewSelectedCabType.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: imgUrl!))!, placeholderImage: #imageLiteral(resourceName: "ic_standard_car"))
    }
    
    func showNoCabAvailable(){
        self.settingsForNoCabAvailable()
    }
    
    func settingsForCabAvailable(){
        self.viewCabType.isHidden = true
        self.viewSelectedCabType.isHidden = false
        self.viewNoCab.isHidden = true
        self.view.bringSubview(toFront: self.viewSelectedCabType)
    }
    
    func settingsForNoCabAvailable(){
        self.clearDataSources()
        self.viewCabType.isHidden = false
        self.viewSelectedCabType.isHidden = true
        self.viewNoCab.isHidden = false
        self.setPlaceholderTextForSourceAndDestinationReachTime()
        self.view.bringSubview(toFront: self.viewNoCab)
    }
    
    func addTapGestureOnMainView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideBottomSheet))
        tapGesture.numberOfTouchesRequired = 1
        self.viewBg.addGestureRecognizer(tapGesture)
    }
    
    func addTapGestureOnCabSelectionView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showCabSelectionListWithAnimation))
        tapGesture.numberOfTapsRequired = 1
        self.viewSelectedCabType.addGestureRecognizer(tapGesture)
    }
    
    func addTapGestureOnSourceView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(editSourceClick))
        tapGesture.numberOfTapsRequired = 1
        self.viewSource.addGestureRecognizer(tapGesture)
    }
    
    func addTapGestureOnDestinationView(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(editDestinationClick))
        tapGesture.numberOfTapsRequired = 1
        self.viewDestination.addGestureRecognizer(tapGesture)
    }
    
    func hideBottomSheet(){
        
        self.dismiss(animated: true) {
            //On dismiss completion...
            guard let _ = self.delegate else{return}
            
            self.delegate!.viewDidDismiss()
            self.delegate?.showSideMenuButton()
        }
    }
    
    func hideSelectedCabView(){
        self.viewSelectedCabType.isHidden = true
    }
    
    func showSelectedCabView(){
        self.viewSelectedCabType.isHidden = false
    }
    
    func editSourceClick(){
        
        self.showEditAddressScreen(withEditType: .SOURCE)
    }
    
    func editDestinationClick(){
        
        self.showEditAddressScreen(withEditType: .DESTINATION)
    }
    
    func showEditAddressScreen(withEditType type: ADDRESS_TYPE){
        
        //Show Edit source/destination screen
        let source = SelectedPlaceModel(title: self.cabSelectionViewModel.sourceAddress, coordinate: CLLocationCoordinate2D(latitude: self.cabSelectionViewModel.latFrom, longitude: self.cabSelectionViewModel.lngFrom))
        let destination = SelectedPlaceModel(title: self.cabSelectionViewModel.destinationAddress, coordinate: CLLocationCoordinate2D(latitude: self.cabSelectionViewModel.latTo, longitude: self.cabSelectionViewModel.lngTo))
        
        let editSourceVC = UIViewController.getViewController(EditSourceAndDestinationViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        editSourceVC.initScreen(withSourceAddress: source, withDestinationAddress: destination, withEditAddressType: type)
        editSourceVC.delegate = self
        editSourceVC.modalPresentationStyle = .overCurrentContext
        self.present(editSourceVC, animated: true, completion: nil)
        
        //Hide navigation button while showing edit address screen
        self.delegate?.hideSideMenuButton()
    }
    
    func getCabs(){
        print(self.shouldShowLoader)
        presenterCabSelection.sendCabSelectionRequest(withCabSelectionViewRequestModel: self.cabSelectionViewModel, withLoader: shouldShowLoader)
        self.shouldShowLoader = false
    }
    
    
    /// This method is used to draw route on map
    func drawRoute(){
        //Draw Route
        let source = SelectedPlaceModel(title: cabSelectionViewModel.sourceAddress, coordinate: CLLocationCoordinate2D(latitude: cabSelectionViewModel.latFrom, longitude: cabSelectionViewModel.lngFrom))
        let destination = SelectedPlaceModel(title: cabSelectionViewModel.destinationAddress, coordinate: CLLocationCoordinate2D(latitude: cabSelectionViewModel.latTo, longitude: cabSelectionViewModel.lngTo))
        self.delegate?.drawRoute(withSource: source, withDestination: destination)
    }

    
    
    //MARK: Actions
    @IBAction func btnScheduleClick(_ sender: Any) {
        
        
//        guard !self.selectedCab.isEmpty else{
//            
//            AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderSelectCab.NO_CAB_AVAILABLE_MESSAGE)
//            return
//        }
        if let vc = UIViewController.getViewControllerFromSchedulingStoryboard(viewController: SchedulingIntroViewController.self) {
            
            let sourceDest = SourceDestinationModel.Builder().setSource(lat: cabSelectionViewModel.latFrom, long: cabSelectionViewModel.lngFrom, address: cabSelectionViewModel.sourceAddress).setDestination(lat: cabSelectionViewModel.latTo, long: cabSelectionViewModel.lngTo, address: cabSelectionViewModel.destinationAddress)
                .build()
            
            UserRideDetail.shared.sourceDestination = sourceDest
//            self.delegate?.pushVC(withVC: vc)
            self.show(vc, sender: self)

        }
    }
    
    
    /// Method is used to navigate user on sell all rides screen with selected cab
    ///
    /// - Parameter sender: referece to called button
    @IBAction func btnSeeAllRidesClick(_ sender: Any) {
//        if !self.selectedCab.isEmpty{
            let vc = UIViewController.getViewController(RideListingViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            
            let modelRequest = CabSelectionViewRequestModel(latFrom: self.cabSelectionViewModel.latFrom, lngFrom: self.cabSelectionViewModel.lngFrom, latTo: self.cabSelectionViewModel.latTo, lngTo: self.cabSelectionViewModel.lngTo, sourceAddress: self.cabSelectionViewModel.sourceAddress, destinationAddress: self.cabSelectionViewModel.destinationAddress, cabList: self.getSelectedCabListIds(), sortingOrder: CAB_SORTING_TYPE.ARRIVAL_TIME.rawValue)
          let cabs = Array(self.selectedCab.values)
          vc.bind(viewModel: modelRequest, selectedCar: cabs)
            
            vc.delegate = self
        
            self.show(vc, sender: self)
            //self.delegate?.pushVC(withVC: vc)
//        }
//        else{
//            AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderSelectCab.NO_CAB_AVAILABLE_MESSAGE)
//        }
    }
    
    func getSelectedCabListIds()->[String]{
        var ids: [String] = []
        
        for cab in self.selectedCab.values{
            ids.append(cab.cabInfo!.type!._id!)
        }
        
        return ids
    }
    
    func sourceDestinationUpdated() {
        guard let sourceDestination = UserRideDetail.shared.sourceDestination else { return }

        self.cabSelectionViewModel.sourceAddress = sourceDestination.source.address
        self.cabSelectionViewModel.destinationAddress = sourceDestination.destination.address
        updateSourceAndDestinationAddress()
    }
    
    func selectedCabChanged() { }

}

//MARK: Available Cab Type List View
extension CabSelectionBottomSheetViewController: UITableViewDelegate, UITableViewDataSource{
    
    func initalCabSelectionViewSettings(){
        self.prepareScreenUI()
        self.hideCabSelectionListView()
        self.configureTableView()
        self.showSelectedCabView()
        
        self.tblViewCabList.setCornerCircular(10)
    }
    
    func configureTableView(){
        self.tblViewCabList.delegate = self
        self.tblViewCabList.dataSource = self
        self.tblViewCabList.separatorStyle = .none
        self.tblViewCabList.bounces = false
        
        self.registerTableViewCells()
    }
    
    func registerTableViewCells(){
        self.tblViewCabList.registerTableViewCell(tableViewCell: CabListTableViewCell.self)
        self.tblViewCabList.registerTableViewHeaderFooterView(tableViewHeaderFooter: CabSelectionListHeaderFooterView.self)
    }
    
    func showCabSelectionListWithAnimation(){
        UIView.animate(withDuration: CAB_LIST_ANIMATION_DURATION, animations: {
            
            //Animation Block...
            self.showCabSelectionListView()
            
        }) { (boolCompletedAnimation) in
            //Animation Finish Block...
            
            self.hideSelectedCabView()
        }
    }
    
    func hideCabSelectonListWithAnimation(){
        UIView.animate(withDuration: CAB_LIST_ANIMATION_DURATION, animations: {
            
            //Animation Block...
            self.hideCabSelectionListView()
            
        }) { (boolCompletedAnimation) in
            //Animation Finish Block...
            
            self.showSelectedCabView()
        }
    }
    
    func showCabSelectionListView(){
        self.isCabSelectionListViewOpen = true
        self._iwidthAutoLayoutConstraintForCabListView.constant = CGFloat(self.cabDataHashMap.keys.count)*CAB_LIST_CELL_HEIGHT + CAB_LIST_HEADER_HEIGHT
    }
    
    func hideCabSelectionListView(){
        self.isCabSelectionListViewOpen = false
        self._iwidthAutoLayoutConstraintForCabListView.constant = 0
    }
    
    //MARK: TableView Delegate & DataSources
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return CAB_LIST_HEADER_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CAB_LIST_HEADER_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.getHeader(withHeaderType: CabSelectionListHeaderFooterView.self)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cabDataHashMap.keys.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return CAB_LIST_CELL_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CAB_LIST_CELL_HEIGHT
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.getCell(withCellType: CabListTableViewCell.self)
        
        let key = Array(cabDataHashMap.keys)[indexPath.row]
        if let _ = self.selectedCab[key]
        {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
        let price = self.priceSortedCabList[key]?.first?.rideEstimatedFare
        let arrivalTime = self.timeSortedCabList[key]?.first?.rideArivalTime
        let imgUrl = self.cabDataHashMap[key]?.first?.cabInfo?.type?.icon ?? ""
        
        cell.bind(withCabType: key, price: price!, time: arrivalTime!, imgURL: imgUrl)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let key = Array(cabDataHashMap.keys)[indexPath.row]
        
        self.selectedCab = [:]
        self.selectedCab[key] = self.cabDataHashMap[key]?.first
        self.setdefaultCab()
        self.setSourceAndDestinationReachTime()
        
        self.hideCabSelectonListWithAnimation()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}

//MARK: Cab Selection View Delegate
extension CabSelectionBottomSheetViewController: CabSelectionViewDelegate{
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.drawRoute()
        
        self.showNoCabAvailable()
        
        if alertMessage == AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION{
            super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
        }
    }
    
    func cabFetched(data: CabSelectionResponseModel) {
        //Start Drawing Route
        self.drawRoute()
        
        //Handle fetched cabs
        self.clearDataSources()
        self.cabSelectionResponseModel = data
        
        if let riderList = data.rideList, riderList.count > 0{
            self.settingsForCabAvailable()
            self.prepareDataSources()
            
            self.defaultCabSetting()
            
            self.tblViewCabList.reloadData()
            
            if isCabSelectionListViewOpen
            {
                UIView.animate(withDuration: CAB_LIST_ANIMATION_DURATION, animations: {
                    self.showCabSelectionListView()
                }) { (boolCompletedAnimation) in
                    self.hideSelectedCabView()
                }
            }
            else
            {
                if self.selectedCab.keys.count != 0
                {
                    let firstCabKey = self.selectedCab.keys.first
                    if let _ = self.cabDataHashMap[firstCabKey!]
                    {}
                    else
                    {
                        self.getDefaultSelectedCab()
                        self.setdefaultCab()
                    }
                    
                }}
        }
        else{
            self.showNoCabAvailable()
        }
        
        }
        
        func prepareDataSources(){
            self.prepareHashMapFromCabData()
            self.prepareTimeSortedHashMap()
            self.preparePriceSortedHashMap()
        }
        
        func prepareHashMapFromCabData(){
            
            if let cabList = self.cabSelectionResponseModel?.rideList{
                for rider in cabList{
                    
                    if let cabTypeName = rider.cabInfo?.type?.vehicleType{
                        if let _ = self.cabDataHashMap[cabTypeName]{
                            self.cabDataHashMap[cabTypeName]!.append(rider)
                        }
                        else{
                            self.cabDataHashMap[cabTypeName] = [rider]
                        }
                    }
                }
            }
        }
        
        func prepareTimeSortedHashMap(){
            for (key, value) in self.cabDataHashMap{
                self.timeSortedCabList[key] = self.getTimeSortedRiderList(data: value)
            }
        }
        
        func preparePriceSortedHashMap(){
            for (key, value) in self.cabDataHashMap{
                self.priceSortedCabList[key] = self.getPriceSortedRiderList(data: value)
            }
        }
        
        func getPriceSortedRiderList(data: [RideList])->[RideList]{
            let sortedData = data.sorted { (rider1, rider2) -> Bool in
                return rider1.rideEstimatedFare! < rider2.rideEstimatedFare!
            }
            
            return sortedData
        }
        
        func getTimeSortedRiderList(data: [RideList])->[RideList]{
            let sortedData = data.sorted { (rider1, rider2) -> Bool in
                return rider1.rideArivalTime! < rider2.rideArivalTime!
            }
            
            return sortedData
        }
}

extension CabSelectionBottomSheetViewController: EditSourceAndDestinationViewProtocol{
    func editAddressScreenDidDismiss(withSourceAddress source: SelectedPlaceModel!, withDestinationAddress destination: SelectedPlaceModel!){
        
        //Update data
        self.cabSelectionViewModel = CabSelectionViewRequestModel(latFrom: source.coordinate.latitude, lngFrom: source.coordinate.longitude, latTo: destination.coordinate.latitude, lngTo: destination.coordinate.longitude, sourceAddress: source.title, destinationAddress: destination.title, cabList: nil, sortingOrder: nil)
        
        //Updated address
        self.updateSourceAndDestinationAddress()
        
        self.viewWillAppear(false)
    }
    
    func editSourceDestinationScreenDidDismiss(){
        self.delegate?.showSideMenuButton()
    }
}

extension CabSelectionBottomSheetViewController: RideListingViewDelegate{
    func updatedAddress(withAddressData data: CabSelectionViewRequestModel){
        
        //Update data
        self.cabSelectionViewModel = CabSelectionViewRequestModel(latFrom: data.latFrom, lngFrom: data.lngFrom, latTo: data.latTo, lngTo: data.lngTo, sourceAddress: data.sourceAddress, destinationAddress: data.destinationAddress, cabList: nil, sortingOrder: nil)
        
        //Updated address
        self.updateSourceAndDestinationAddress()
    }
}
