//
//  GoogleMapsExtension.swift
//  Wapanda
//
//  Created by daffomac-31 on 27/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import GoogleMaps

extension GMSMapView{
    
    
    /// This method is used to customize map view appearance with custom color and settings
    func customizeMapAppearance(){
        
        self.isMyLocationEnabled = false
        self.setMinZoom(1, maxZoom: 18)
        self.isBuildingsEnabled = false
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "Style", withExtension: "json") {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    
    /// This method is used to update the camera location with animation to the new location
    ///
    /// - Parameters:
    ///   - lat: Lat for the new location coordinate
    ///   - long: long for the new location coordinate
    ///   - zoom: zoom level for the map
    func updateCameraLocation(lat: Double, long: Double,zoom:Float){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: zoom)
        self.animate(to: camera)
    }
    
    //MARK: Marker Addition and customization goes here...
    
    
    /// This method is used
    ///
    /// - Parameters:
    ///   - coordinate: new updated coordinate for the marker
    ///   - marker: marker of type GMSMarker
    ///   - bearing: bearing for the marker
    /// - Returns: return updated marker reference
    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker, bearing:Double)->GMSMarker{
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(AppConstants.ScreenSpecificConstant.RiderHomeScreen.CAB_ANIMATION_DURATION)
        if bearing != 0
        {
            marker.rotation = bearing
        }
        marker.position = coordinate
        CATransaction.commit()
        
        return marker
    }
    
    
    /// This method is used to
    ///
    /// - Parameters:
    ///   - coordinate: new marker coordinate on the map
    ///   - image: image for the marker of type UIImage
    /// - Returns: return added marker reference of type GMSMarker
    func addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D, markerImage image: UIImage)->GMSMarker{
        
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude:coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.icon = image
        marker.map = self
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)

        return marker
    }
    
    
    /// This method is used to Remove marker from the map
    ///
    /// - Parameter marker: marker to delete
    /// - Returns: return deleted marker reference
    func removeMarker(marker: GMSMarker)->GMSMarker{
        CATransaction.begin()
        CATransaction.setAnimationDuration(AppConstants.ScreenSpecificConstant.RiderHomeScreen.CAB_ANIMATION_DURATION)
        marker.map = nil
        CATransaction.commit()
        return marker
    }
    
    //MARK: Draw polyline
    
    /// This method is used to draw polyline on map
    ///
    /// - Parameter polyLine: overview polyLine description string
    func drawRoute(withOverViewPolyline polyLine: String)->GMSPolyline{
        
        //DRAW ROUTE
        let path: GMSMutablePath = GMSMutablePath(fromEncodedPath: polyLine)!
        let routePolyline = GMSPolyline(path: path)
        routePolyline.strokeColor = UIColor.appDarkThemeColor()
        routePolyline.strokeWidth = 3.0
        routePolyline.map = self
        
        //BOUND MAP FOR POLYLINE
        let polyLineCoordinates = self.getCoordinatesFromPolyline(polyline: routePolyline)
        self.boundMapWithCoordinates(coordinates: polyLineCoordinates)
        
        //RETURN POLYLINE
        return routePolyline
    }
    
    func updateRoute(withPolyline polyline: GMSPolyline, newPolyLinePoints points: String)->GMSPolyline{
        let path: GMSMutablePath = GMSMutablePath(fromEncodedPath: points)!
        polyline.path = path
        polyline.strokeColor = UIColor.appDarkThemeColor()
        polyline.strokeWidth = 3.0
        polyline.map = self
        
        return polyline
    }
    
    
    func getCoordinatesFromPolyline(polyline: GMSPolyline)->[CLLocationCoordinate2D]{
        
        let count = UInt(polyline.path!.count())
        var arrayCoordinates: [CLLocationCoordinate2D] = []
        
        for i in 0..<count{
            arrayCoordinates.append(polyline.path!.coordinate(at: i))
        }
        
        return arrayCoordinates
    }

    func boundMapWithCoordinates(coordinates: [CLLocationCoordinate2D]){
        var bounds = GMSCoordinateBounds()
        
        for cord in coordinates{
            bounds = bounds.includingCoordinate(cord)
        }
        
        self.animate(with: GMSCameraUpdate.fit(bounds,withPadding: ScreenSize.size.height * 0.1))
    }
    
    //MARK: Custom info window on map
    
    
    /// This method is used add custom info window marker on the map
    ///
    /// - Parameters:
    ///   - coordinate: coordinate for the info window to show on map
    ///   - infoWindowView: custom view for the info window
    /// - Returns: info window marker reference
    func addCustomInfoViewMarker(withCoordinates coordinate: CLLocationCoordinate2D, infoWindowView: UIView)->UIView{
        let marker = GMSMarker(position: coordinate)
        
        marker.iconView = infoWindowView
        marker.tracksInfoWindowChanges = false
        marker.map = self
        
        return infoWindowView
    }
}
