//
//  TripStatusDropDownView.swift
//  DropDownSwift
//
//  Created by Daffolapmac-33 on 18/11/17.
//  Copyright © 2017 Daffolapmac-33. All rights reserved.
//

import UIKit
enum DropDownStatus: Int{
    case Open = 1
    case Close = 0
}

protocol DropDownViewDelegate {
    func updateSelectedItem(selectedItem: Int)
}

class CustomDropDownView: UIView {

    @IBOutlet weak var tableView: UITableView!
    var delegate: DropDownViewDelegate!
    var dataArray: [Any] = []
    var selectedItem: Int = 0
    var isVisible = false
    var rowHeight: CGFloat = 48
    var rowNormalColor = UIColor.appDarkThemeColor()
    var rowSelectedColor = UIColor.appThemeColor()
    var radioNormalImage: String = "ic_radio_oval_Unseleted"
    var radioSelectedImage: String = "ic_radio_oval_seleted"
    
    class func instanceFromNib() -> CustomDropDownView {
        return UINib(nibName: "CustomDropDownView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomDropDownView
    }
    override func awakeFromNib() {
        tableView.register(UINib(nibName: "CustomDropDownCell", bundle: nil), forCellReuseIdentifier: "CustomDropDownCell")
        self.backgroundColor = UIColor.transparentBackgroundColor()
    }

    func setRowColorAndHeightWithRadioImage(_ normalColor: UIColor, selectedColor: UIColor, height: CGFloat, radioNormalImage: String, radioSelectedImage: String){
        rowNormalColor = normalColor
        rowSelectedColor = selectedColor
        rowHeight = height
        self.radioNormalImage = radioNormalImage
        self.radioSelectedImage = radioSelectedImage
    }

    func setDropDown(_ onView: UIView, belowView: UIView, itemList:[Any], selectedItem: Int){
        
        dataArray = itemList
        self.selectedItem = selectedItem
        self.frame = CGRect(x: belowView.frame.minX, y: belowView.frame.maxY, width: belowView.frame.width, height:onView.frame.maxY - belowView.frame.maxY)
        tableView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat(itemList.count) * rowHeight)
        onView.addSubview(self)
        tableView.reloadData()
    }
    func toggle(){
        if isVisible{
            isVisible = false
            hide()
        }else{
            isVisible = true
            show()
        }
    }
    func hide(){
        self.isHidden = true
    }
    func show(){
        self.isHidden = false
    }
}

extension CustomDropDownView: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomDropDownCell", for: indexPath) as! CustomDropDownCell
        let itemName = dataArray[indexPath.row] as! String
        cell.statusLabel.text = itemName
        //cell.radioImageView.layer.cornerRadius = cell.radioImageView.frame.width/2
        cell.radioImageView.layer.masksToBounds = true
        if indexPath.row == selectedItem{
            cell.radioImageView.image = UIImage(named:radioSelectedImage)
            cell.backgroundColor = rowSelectedColor
        }else{
            cell.radioImageView.image = UIImage(named: radioNormalImage)
            cell.backgroundColor = rowNormalColor
        }
        
        return cell;
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        if delegate != nil {
            delegate.updateSelectedItem(selectedItem: indexPath.row)
        }
        
       toggle()
    }
}
