//
//  UpcomingTripListRequestModel.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 19/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

enum TripStatus: String{
    case Accepted = "ACCEPTED"
    case AcceptedCamelCase = "Accepted"
    case Cancelled = "CANCELLED"
    case Open = "OPEN"
    case Scheduled = "SCHEDULED"
    case All = "ALL"
    case CurrentBid = "Current Bid"
    case Expired = "EXPIRED"
    case Closed = "CLOSED"
}

class UpcomingTripListRequestModel {
    
    //MARK:- UpcomingTripListRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var skip : Int!
    var limit : Int!
    var userType: UserType!
    var sort: String = "pickupDateTime"
    var order: Int!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.limit = builder.limit
        self.skip = builder.skip
        self.userType = builder.userType
        self.sort = builder.sort
        self.order = builder.order
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var skip : Int!
        var limit : Int!
        var userType: UserType!
        var sort: String!
        var order: Int!
        
        
        /**
        This method is used for setting user role
        
        - parameter limit: Int parameter that is going to be set on limit
        
        - returns: returning Builder Object
        */
        func setUserType(_ type: UserType)->Builder{
            self.userType = type
            return self
        }
        
        /**
         This method is used for setting limit of item to be fetched
         
         - parameter limit: Int parameter that is going to be set on limit
         
         - returns: returning Builder Object
         */
        func setLimit(_ limit:Int)->Builder{
            self.limit = limit
            return self
        }
        
        /**
         This method is used for setting number of items to skipped
         
         - parameter skip: Int parameter that is going to be set on skip
         
         - returns: returning Builder Object
         */
        func setSkip(_ skip:Int)->Builder{
            self.skip = skip
            return self
        }
        
        
        /**
         This method is used for setting status
         
         - parameter auctionId: String parameter that is going to be set on status
         
         - returns: returning Builder Object
         */
        func setStatus(_ status: String)->Builder{
            requestBody["status"] = status as AnyObject?
            return self
        }
        
        /**
         This method is used for setting limit of item to be fetched
         
         - parameter limit: Int parameter that is going to be set on limit
         
         - returns: returning Builder Object
         */
        func setSortingType(withType type: String)->Builder{
            self.sort = type
            return self
        }
        /**
         This method is used for setting order of list ascending or descending
         
         - parameter order: Int parameter that is going to be set on order
         
         - returns: returning Builder Object
         */
        func setOrder(_ order:Int)->Builder{
            self.order = order
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of UpcomingTripListRequestModel
         and provide UpcomingTripListRequestModel object.
         
         -returns : UpcomingTripListRequestModel
         */
        func build()->UpcomingTripListRequestModel{
            return UpcomingTripListRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting auction detail end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
       // return  String(format: AppConstants.ApiEndPoints.AUCTION_DETAIL, self.requestBody["auctionId"] as! String)
        return String(format: AppConstants.ApiEndPoints.UPCOMING_TRIP, self.userType.rawValue,self.skip,self.limit,self.sort,self.order)
    }
    
}
