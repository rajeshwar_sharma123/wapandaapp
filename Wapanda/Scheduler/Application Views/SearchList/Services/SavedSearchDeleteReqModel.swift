import UIKit

class SavedSearchDeleteReqModel {
    //Note :- Property Name must be same as key used in request API
    var requestHeader: [String:AnyObject]!
    var requestBody: [String:AnyObject]!
    var searchId = ""

    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestHeader = builder.requestHeader
        self.requestBody = builder.requestBody
        searchId = builder.searchId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var searchId = ""

        /**
         This method is used for setting Search Id
         
         - parameter destLoc: String parameter that is going to be set on destination locations
         
         - returns: returning Builder Object
         */
        func setSearchId(_ searchId: String) -> Builder{
            self.searchId = searchId
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of BidsListRequestModel
         and provide BidsListRequestModel object.
         
         -returns : VisitedStopRequestModel
         */
        func build() -> SavedSearchDeleteReqModel{
            return SavedSearchDeleteReqModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting VisitedStop end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return "/auctions/criteria/\(searchId)"
    }
}
