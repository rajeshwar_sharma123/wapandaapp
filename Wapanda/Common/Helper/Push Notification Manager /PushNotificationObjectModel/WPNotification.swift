//
//  Notification.swift
//
//  Created by  on 19/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class WPNotification: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let alert = "alert"
  }

  // MARK: Properties
  public var alert: Alert?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    alert <- map[SerializationKeys.alert]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = alert { dictionary[SerializationKeys.alert] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.alert = aDecoder.decodeObject(forKey: SerializationKeys.alert) as? Alert
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(alert, forKey: SerializationKeys.alert)
  }

}
