import UIKit
import CoreLocation

class SchedulingIntroViewController: UIViewController, UserRideDetailProtocol {

    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var sourceAndDestinationView: SourceAndDestinationView!
    private var sourceDestination : SourceDestinationModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    func initView() {
        
        customizeNavigationBarWithTitle(navigationTitle: "", color: UIColor.schedulerNavColor() , isTranslucent: false)
        customizeNavigationBackButton()

//        lblInfo.text = """
//        Name your price and
//        we’ll send your bid to
//        every driver in the
//        area.
//        """
        
        lblInfo.font = UIFont(name:"Titillium-Light", size: 26.0)
        
        if let sourceDestination = UserRideDetail.shared.sourceDestination {
            sourceAndDestinationView.bind(source: sourceDestination.source.address, destination: sourceDestination.destination.address)
        }
        addLocationManagerDelegate()
    }
    
    @IBAction func soundsGoodClicked(_ sender: UIButton) {
        if let vc = UIViewController.getViewControllerFromSchedulingStoryboard(viewController: SchedulerBidViewController.self) {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setLocation(sourceDestination: SourceDestinationModel) {
        self.sourceDestination = sourceDestination
    }

    func filterSelected(withSortingType sortingType:String, selectedCars: [RideList], updatedAddress: CabSelectionViewRequestModel) {
    }
    
    func addLocationManagerDelegate() {
        
        let delegates = UserRideDetail.shared.delegates
        for (index, delegate) in delegates.enumerated() {
            
            if delegate is SchedulerBidViewController {
                UserRideDetail.shared.delegates.remove(at: index)
                break
            }
        }
        UserRideDetail.shared.delegates.append(self)
    }
    
    func selectedCabChanged() {
        
    }
    
    func sourceDestinationUpdated() {
        upldateLocation()
    }
    
    func upldateLocation() {
        if let sourceDestination = UserRideDetail.shared.sourceDestination {
            sourceAndDestinationView.bind(source: sourceDestination.source.address, destination: sourceDestination.destination.address)
        }
    }
}
