//
//  RideScheduleTimeRequestModel.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 12/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class ScheduleRideTimeRequestModel {
    
    //MARK:- ScheduleRideTimeRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        
        /**
         This method is used for setting latFrom
         
         - parameter latFrom: Float parameter that is going to be set on latFrom
         
         - returns: returning Builder Object
         */
        func setLatFrom(_ latFrom:Float)->Builder{
            requestBody["latFrom"] = latFrom as AnyObject?
            return self
        }
        
        /**
         This method is used for setting lngFrom
         
         - parameter lngFrom: Float parameter that is going to be set on lngFrom
         
         - returns: returning Builder Object
         */
        func setLngFrom(_ lngFrom:Float)->Builder{
            requestBody["lngFrom"] = lngFrom as AnyObject?
            return self
        }
        
        
        /**
         This method is used for setting latTo
         
         - parameter latTo: Float parameter that is going to be set on latTo
         
         - returns: returning Builder Object
         */
        func setLatTo(_ latTo:Float)->Builder{
            requestBody["latTo"] = latTo as AnyObject?
            return self
        }
       
        /**
         This method is used for setting lngTo
         
         - parameter lngTo: Float parameter that is going to be set on lngTo
         
         - returns: returning Builder Object
         */
        func setLngTo(_ lngTo:Float)->Builder{
            requestBody["lngTo"] = lngTo as AnyObject?
            return self
        }
        
        /**
         This method is used for setting pickupDateTime
         
         - parameter pickupDateTime: String parameter that is going to be set on pickupDateTime
         
         - returns: returning Builder Object
         */
        func setPickupDateTime(_ pickupDateTime:String)->Builder{
            requestBody["pickupDateTime"] = pickupDateTime as AnyObject?
            return self
        }
        
        /**
         This method is used for setting fromAddress
         
         - parameter fromAddress: String parameter that is going to be set on fromAddress
         
         - returns: returning Builder Object
         */
        func setFromAddress(_ fromAddress:String)->Builder{
            requestBody["fromAddress"] = fromAddress as AnyObject?
            return self
        }
        
        /**
         This method is used for setting toAddress
         
         - parameter toAddress: String parameter that is going to be set on toAddress
         
         - returns: returning Builder Object
         */
        func setToAddress(_ toAddress:String)->Builder{
            requestBody["toAddress"] = toAddress as AnyObject?
            return self
        }
        
        /**
         This method is used for setting carType
         
         - parameter carType: String parameter that is going to be set on carType
         
         - returns: returning Builder Object
         */
        func setCarType(_ carType:String)->Builder{
            if carType != ""
            {
                 requestBody["carType"] = carType as AnyObject?
            }
           
            return self
        }
        
        /**
         This method is used for setting riderSelectedFare
         
         - parameter riderSelectedFare: Float parameter that is going to be set on riderSelectedFare
         
         - returns: returning Builder Object
         */
        func setRiderSelectedFare(_ riderSelectedFare:Float)->Builder{
            requestBody["riderSelectedFare"] = riderSelectedFare as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of ScheduleRideTimeRequestModel
         and provide ScheduleRideTimeRequestModel object.
         
         -returns : ScheduleRideTimeRequestModel
         */
        func build()->ScheduleRideTimeRequestModel{
            return ScheduleRideTimeRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting login end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return AppConstants.ApiEndPoints.SCHEDULE_RIDE_AUCTION
    }
    
}
