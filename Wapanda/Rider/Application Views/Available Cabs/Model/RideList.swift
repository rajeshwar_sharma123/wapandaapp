//
//  RideList.swift
//
//  Created by  on 8/25/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RideList: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let rideArivalTime = "rideArivalTime"
    static let rideEstimatedTime = "rideEstimatedTime"
    static let cabInfo = "cabInfo"
    static let rideEstimatedFare = "rideEstimatedFare"
    static let _id = "_id"
    static let driverProfile = "driverProfile"
    static let rideEstimatedDistance = "rideEstimatedDistance"
    static let distanceFromDriver = "distanceFromDriver"
    static let fareValue = "fareValue"
    static let fairValues = "fareValue.fairValues"
    static let estPrice = "fareValue.estPrice"
  }

  // MARK: Properties
  public var rideArivalTime: UIntMax?
  public var rideEstimatedTime: UIntMax?
  public var rideEstimatedDistance: UIntMax?
  public var distanceFromDriver: UIntMax?
  public var cabInfo: Cab?
  public var rideEstimatedFare: Double?
  public var _id: String?
  public var driverProfile: DriverProfile?
  public var fareValue: FareValue?
  public var estPrice: Double?
  public var fairValues: FairValuesModel?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    rideArivalTime <- map[SerializationKeys.rideArivalTime]
    rideEstimatedTime <- map[SerializationKeys.rideEstimatedTime]
    rideEstimatedDistance <- map[SerializationKeys.rideEstimatedDistance]
    distanceFromDriver <- map[SerializationKeys.distanceFromDriver]
    cabInfo <- map[SerializationKeys.cabInfo]
    rideEstimatedFare <- map[SerializationKeys.rideEstimatedFare]
    _id <- map[SerializationKeys._id]
    driverProfile <- map[SerializationKeys.driverProfile]
    fareValue <- map[SerializationKeys.fareValue]
    fairValues <- map[SerializationKeys.fairValues]
    estPrice <- map[SerializationKeys.estPrice]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = rideArivalTime { dictionary[SerializationKeys.rideArivalTime] = value }
    if let value = rideEstimatedTime { dictionary[SerializationKeys.rideEstimatedTime] = value }
    if let value = rideEstimatedDistance { dictionary[SerializationKeys.rideEstimatedDistance] = value }
    if let value = distanceFromDriver { dictionary[SerializationKeys.distanceFromDriver] = value }
    if let value = cabInfo { dictionary[SerializationKeys.cabInfo] = value.dictionaryRepresentation() }
    if let value = rideEstimatedFare { dictionary[SerializationKeys.rideEstimatedFare] = value }
    if let value = _id { dictionary[SerializationKeys._id] = value }
    if let value = driverProfile { dictionary[SerializationKeys.driverProfile] = value.dictionaryRepresentation() }
    if let value = fareValue { dictionary[SerializationKeys.fareValue] = value.dictionaryRepresentation() }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.rideArivalTime = aDecoder.decodeObject(forKey: SerializationKeys.rideArivalTime) as? UIntMax
    self.rideEstimatedTime = aDecoder.decodeObject(forKey: SerializationKeys.rideEstimatedTime) as? UIntMax
    self.rideEstimatedDistance = aDecoder.decodeObject(forKey: SerializationKeys.rideEstimatedDistance) as? UIntMax
    self.distanceFromDriver = aDecoder.decodeObject(forKey: SerializationKeys.distanceFromDriver) as? UIntMax
    self.cabInfo = aDecoder.decodeObject(forKey: SerializationKeys.cabInfo) as? Cab
    self.rideEstimatedFare = aDecoder.decodeObject(forKey: SerializationKeys.rideEstimatedFare) as? Double
    self._id = aDecoder.decodeObject(forKey: SerializationKeys._id) as? String
    self.driverProfile = aDecoder.decodeObject(forKey: SerializationKeys.driverProfile) as? DriverProfile
    self.fareValue = aDecoder.decodeObject(forKey: SerializationKeys.fareValue) as? FareValue

  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(rideArivalTime, forKey: SerializationKeys.rideArivalTime)
    aCoder.encode(rideEstimatedTime, forKey: SerializationKeys.rideEstimatedTime)
    aCoder.encode(rideEstimatedDistance, forKey: SerializationKeys.rideEstimatedDistance)
    aCoder.encode(distanceFromDriver, forKey: SerializationKeys.distanceFromDriver)
    aCoder.encode(cabInfo, forKey: SerializationKeys.cabInfo)
    aCoder.encode(rideEstimatedFare, forKey: SerializationKeys.rideEstimatedFare)
    aCoder.encode(_id, forKey: SerializationKeys._id)
    aCoder.encode(driverProfile, forKey: SerializationKeys.driverProfile)
    aCoder.encode(fareValue, forKey: SerializationKeys.fareValue)
  }

}
