/**
    This class handles the OTP Verification process for phone and email
 */

import UIKit
import Foundation

class PhoneOTPVerificationViewController:BaseViewController{
    
    //Screen Outlets
    @IBOutlet weak var lblNumberWithDots: UILabel!
    @IBOutlet weak var txtFieldOTPCode: UITextField!
    @IBOutlet weak var lblErrorOTPCode: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    
    //Global Variables
    fileprivate var signUpPresenter: SignUpViewPresenter!
    fileprivate var loginPresenter: LoginScreenPresenter!
    fileprivate var registerDevicePresenter: DeviceRegisterScreenPresenter!
    fileprivate var presenterResendPhoneOTP : ResendPhoneOTPPresenter!
    fileprivate var signUpViewRequestModel : SignUpViewRequestModel?
    fileprivate var timerEnableResendOTP: Timer!
    
    //MARK: Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Setup Initial UI
        self.setupInitialUI()
        
        //Prepare resend phone OTP presenter
        self.presenterResendPhoneOTP = ResendPhoneOTPPresenter(delegate: self)
        
        //Prepare signup presenter
        self.signUpPresenter = SignUpViewPresenter(delegate: self, textFieldValidationDelegate: self)
        
        //Prepare login presenter
        self.loginPresenter = LoginScreenPresenter(delegate: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //Disable Resend OTP for 60 seconds 
        self.disableResendOTP()
        self.startTimerForEnableOTP()
    }
    
    /**
     This method initalizes class with prefilled values in model from form 2 screen
     */
    func initializeViewWithSignUpRequestModel(model: SignUpViewRequestModel){
        self.signUpViewRequestModel = model
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Initial View Methods
    
    /**
     This function setup the Initial UI elements
     - returns :void
     */
    private func setupInitialUI() ->Void{
        //Customize navigation bar style
        self.setupNavigationBar()
        
        //Customize the text field properties
        self.customizeTextFields()
        
        //Set Screen Message
        self.setScreenMessage()
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        self.txtFieldOTPCode.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.OTP_PLACEHOLDER_TEXT, attributes:attributes)
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    private func setupNavigationBar() ->Void{
        self.customizeNavigationBackButton()
    }
    
    @objc private func enableResendOTP(){
        self.btnResend.isEnabled = true
        self.btnResend.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    private func disableResendOTP(){
        self.btnResend.isEnabled = false
        self.btnResend.titleLabel?.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    private func startTimerForEnableOTP(){
      
        timerEnableResendOTP = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(enableResendOTP), userInfo: nil, repeats: false)
    }
    
    //MARK: Button Actions
    
    @IBAction func btnNextClick(_ sender: Any) {
        //Set OTP in model
        self.signUpViewRequestModel?.phoneOTP = self.txtFieldOTPCode.text
        
        //
        self.signUpPresenter.sendSignUpRequest(withSignUpViewRequestModel: self.signUpViewRequestModel!)
    }
    
    @IBAction func btnResendClick(_ sender: Any) {
        
        //Disable Resend button for next 60 seconds
        self.disableResendOTP()
        self.startTimerForEnableOTP()
        
        //Resend OTP
        self.resendPhoneOTP()
    }
    
    func resendPhoneOTP(){
        self.presenterResendPhoneOTP.resendPhoneOTP(withPhone: self.signUpViewRequestModel!.phoneNo, countryCode: self.signUpViewRequestModel!.countryCode, diallingCode: self.signUpViewRequestModel!.countryCodeExt)
    }
    
    //MARK: Screen Helper Methods
    
    private func setScreenMessage(){
        
        //Get Phone number
        guard let phoneNumber = self.signUpViewRequestModel?.phoneNo else {return}
        
        //Make phone number hint text
        let lastTwoCharactersFromPhnNumber = phoneNumber.substring(from: phoneNumber.index(phoneNumber.endIndex, offsetBy: -2))
        let hintTextPhoneNumber = AppConstants.ScreenSpecificConstant.SingUpScreen.HINT_PHONE_NUMBER_TEXT + lastTwoCharactersFromPhnNumber
        
        //Set hint text
        self.lblNumberWithDots.text = hintTextPhoneNumber
    }
    
    
    //MARK: Base View Delegate
    
    /**
     This function shows the activity for the screen
     - returns :void
     */
    func showLoader(){
        super.showLoader(self)
    }
    
    /**
     This function hide the activity
     - returns :void
     */
    func hideLoader(){
        super.hideLoader(self)
    }
    
    /**
     This function show error in alert on screen
     - returns :void
     */
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    //MARK: Navigation 

    /**
     This function navigate to the email verification screen
     - returns :void
     */
    func navigateToEmailVerificationScreenWithResponseData(data: LoginResponseModel){
        let emailOTPVerificationVC = UIViewController.getViewController(viewController: EmailOTPVerificationViewController.self)
        emailOTPVerificationVC.initializeViewWithSignUpRequestModel(model: data)
        self.navigationController?.pushViewController(emailOTPVerificationVC, animated: true)
    }
    
    /**
     This function navigate to the Switch Role screen
     - returns :void
     */
    func navigateToSwitchUserScreen(){
        let switchUserScreen = UIViewController.getViewController(viewController: SwitchRoleScreenViewController.self)
        switchUserScreen.initializeScreenWithDataAs(hideBackButton: true)
        self.navigationController?.pushViewController(switchUserScreen, animated: true)
    }
}

//MARK: Sign Up View Delegate
extension PhoneOTPVerificationViewController: SignUpViewDelegate{
    
    func signUpSuccessful(withResponseModel: LoginResponseModel){
        self.performLogin()
    }
}

extension PhoneOTPVerificationViewController: ResendPhoneOTPViewDelegate{
    
    func resendPhoneOTPSuccessfully() -> Void{
    }
    
    func showToastWithMessage(_ message:String) -> Void{
        AppUtility.presentToastWithMessage(message)
    }
}

//MARK: Text Field Delegate

extension PhoneOTPVerificationViewController: TextFieldValidationDelegate, UITextFieldDelegate {
    
    //MARK: Validation Delegate
    
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType)
    {
        switch forTextFields {
        case .OTP:
            self.lblErrorOTPCode.text = message
        default:
            break
        }
    }
    
    //MARK: UITextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField{
        case self.txtFieldOTPCode:
            self.lblErrorOTPCode.text = ""
            
            let str = (textField.text! + string)
            if (str.characters.count <= 6){
                return true
            }
            else{
                return false
            }
        default:
            break
        }
        
        
        return true
    }

}

extension PhoneOTPVerificationViewController: LoginViewDelegate{
    
    func performLogin(){
        
        let loginRequestModel = LoginScreenRequestModel(email: "", phoneNumber: (self.signUpViewRequestModel?.phoneNo)!, countryCode: (self.signUpViewRequestModel?.countryCodeExt)! , password: self.signUpViewRequestModel?.password)
        
        self.loginPresenter.sendLoginRequest(withData: loginRequestModel)
    }
    
    func loginSuccessfull(withResponseModel loginResponseModel:LoginResponseModel){
        //Save Login user data
        
        AppDelegate.sharedInstance.userInformation = loginResponseModel
        AppDelegate.sharedInstance.userInformation.saveUser()
        
         self.registerDeviceToken()
        
        if let email = loginResponseModel.email, !email.isEmpty{
            
            //Move to the email verification screen
            self.navigateToEmailVerificationScreenWithResponseData(data: loginResponseModel)
            
        }
        else{
            //Move to the user role screen
            navigateToSwitchUserScreen()
        }

    }
}








