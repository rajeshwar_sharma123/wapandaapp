    //
    //  HistoryViewPresenter.swift
    //  Wapanda
    //
    //  Created by Daffodilmac-20 on 13/11/17.
    //  Copyright © 2017 Wapanda. All rights reserved.
    //
    
    import Foundation
    import ObjectMapper
    
    class HistoryViewPresenter: ResponseCallback{
        //MARK:- HelpViewPresenter properties
        
         weak var historyViewDelegate   : HistoryViewDelegate?
         lazy var historyBusinessLogic  : HistoryBusinessLogic = HistoryBusinessLogic()
         lazy var upcomingTripListBussinessLogic: UpcomingTripListBussinessLogic = UpcomingTripListBussinessLogic()
        
        //MARK:- Constructor
        
        init(delegate responseDelegate: HistoryViewDelegate){
            self.historyViewDelegate = responseDelegate
        }
        
        
        
        //MARK:- ResponseCallback delegate methods
        
        func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
            
            self.historyViewDelegate?.hideLoader()
            
            if let data = responseObject as? HistoryResponseModel{
                self.historyViewDelegate?.historyListingSuccess(withResponseModel: data )
            }
            if responseObject is UpcomingTripListResponseModel{
                historyViewDelegate?.updateUpcomingTripList(withResponseModel: responseObject as! UpcomingTripListResponseModel)
            }
            
        }
        
        func servicesManagerError(error: ErrorModel){
            
            self.historyViewDelegate?.hideLoader()
            self.historyViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
            
        }
        
        
        
        //MARK:- Methods to make decision and call Feedback Api.
        
        func makeHistoryApiRequest(withHistoryViewModel historyScreenViewModel:HistoryScreenViewModel){
            
            self.historyViewDelegate?.showLoader()
            var userType = UserType.Rider
            if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
            {
                userType = .Driver
            }
            else
            {
                userType = .Rider
            }
            let historyRequestModel = HistoryRequestModel.Builder().setLimit(historyScreenViewModel.limit).setSkip(historyScreenViewModel.skip).setUserType(withType: userType).setOrder(-1).setSortingType(withType: "startTime").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).build()
            self.historyBusinessLogic.performHistoryRequest(withHistoryRequestModel: historyRequestModel, presenterDelegate: self)
            
        }
           
    }
