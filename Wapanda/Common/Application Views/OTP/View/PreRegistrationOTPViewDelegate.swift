
//Notes:-

protocol PreRegistrationOTPViewDelegate:BaseViewProtocol {
    func preRegistrationOTPSentSuccessfully() -> Void
    func showToastWithMessage(_ message:String) -> Void
}
