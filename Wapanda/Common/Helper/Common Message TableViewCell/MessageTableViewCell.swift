//
//  MessageTableViewCell.swift
//  Miavina
//
//  Created by daffomac-31 on 10/05/17.
//  Copyright © 2017 Daffodilsw Applications. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMsg: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindWithData(message: String)
    {
        self.lblMsg.text = message
    }
}
