//
//  RiderScheduleDetailViewDelegate.swift
//  Wapanda
//
//  Created by Daffodil on 28/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
//Notes:- This protocol is used as a interface which is used by RiderScheduleDetailViewDelegate to tranfer info to RiderScheduleDetailViewDelegate

protocol RiderScheduleDetailViewDelegate: BaseViewProtocol{
    
    func updateCancelRide(withResponseModel response: CancelResponseModel)
    
}
