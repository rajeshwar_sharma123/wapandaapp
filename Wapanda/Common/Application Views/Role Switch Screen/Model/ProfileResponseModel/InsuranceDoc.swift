//
//  InsuranceDoc.swift
//
//  Created by  on 8/30/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class InsuranceDoc:  NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let approved = "approved"
    static let updatedAt = "updatedAt"
    static let imageIds = "imageIds"
    static let id = "_id"
    static let createdAt = "createdAt"
  }

  // MARK: Properties
  public var approved: Bool? = false
  public var updatedAt: String?
  public var imageIds: [String]?
  public var id: String?
  public var createdAt: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    approved <- map[SerializationKeys.approved]
    updatedAt <- map[SerializationKeys.updatedAt]
    imageIds <- map[SerializationKeys.imageIds]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.approved] = approved
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = imageIds { dictionary[SerializationKeys.imageIds] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.approved = aDecoder.decodeObject(forKey: SerializationKeys.approved) as? Bool
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.imageIds = aDecoder.decodeObject(forKey: SerializationKeys.imageIds) as? [String]
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(approved, forKey: SerializationKeys.approved)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(imageIds, forKey: SerializationKeys.imageIds)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
  }

}
