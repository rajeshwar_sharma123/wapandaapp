//
//  Thirdpartyestimators.swift
//
//  Created by  on 08/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Thirdpartyestimators: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let thirdpartyname = "thirdpartyname"
    static let thirdpartycartype = "thirdpartycartype"
    static let thirdpartyestimate = "thirdpartyestimate"
  }

  // MARK: Properties
  public var thirdpartyname: String?
  public var thirdpartycartype: String?
  public var thirdpartyestimate: Thirdpartyestimate?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    thirdpartyname <- map[SerializationKeys.thirdpartyname]
    thirdpartycartype <- map[SerializationKeys.thirdpartycartype]
    thirdpartyestimate <- map[SerializationKeys.thirdpartyestimate]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = thirdpartyname { dictionary[SerializationKeys.thirdpartyname] = value }
    if let value = thirdpartycartype { dictionary[SerializationKeys.thirdpartycartype] = value }
    if let value = thirdpartyestimate { dictionary[SerializationKeys.thirdpartyestimate] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.thirdpartyname = aDecoder.decodeObject(forKey: SerializationKeys.thirdpartyname) as? String
    self.thirdpartycartype = aDecoder.decodeObject(forKey: SerializationKeys.thirdpartycartype) as? String
    self.thirdpartyestimate = aDecoder.decodeObject(forKey: SerializationKeys.thirdpartyestimate) as? Thirdpartyestimate
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(thirdpartyname, forKey: SerializationKeys.thirdpartyname)
    aCoder.encode(thirdpartycartype, forKey: SerializationKeys.thirdpartycartype)
    aCoder.encode(thirdpartyestimate, forKey: SerializationKeys.thirdpartyestimate)
  }

}
