//
//  UpcomingTripListResponseModel.swift
//
//  Created by  on 21/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class UpcomingTripListResponseModel:NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let limit = "limit"
    static let itemCount = "item_count"
    static let skip = "skip"
    static let totalCount = "total_count"
    static let items = "items"
    static let hasNext = "hasNext"
    static let hasPrev = "hasPrev"
  }

  // MARK: Properties
  public var limit: Int?
  public var itemCount: Int?
  public var skip: Int?
  public var totalCount: Int?
 // public var items: [TripItems]?
    public var items: [Trip]?
    
  public var hasNext: Bool? = false
  public var hasPrev: Bool? = false
    

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    limit <- map[SerializationKeys.limit]
    itemCount <- map[SerializationKeys.itemCount]
    skip <- map[SerializationKeys.skip]
    totalCount <- map[SerializationKeys.totalCount]
    items <- map[SerializationKeys.items]
    hasNext <- map[SerializationKeys.hasNext]
    hasPrev <- map[SerializationKeys.hasPrev]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = limit { dictionary[SerializationKeys.limit] = value }
    if let value = itemCount { dictionary[SerializationKeys.itemCount] = value }
    if let value = skip { dictionary[SerializationKeys.skip] = value }
    if let value = totalCount { dictionary[SerializationKeys.totalCount] = value }
    if let value = items { dictionary[SerializationKeys.items] = value.map { $0.dictionaryRepresentation() } }
    dictionary[SerializationKeys.hasNext] = hasNext
    dictionary[SerializationKeys.hasPrev] = hasPrev
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.limit = aDecoder.decodeObject(forKey: SerializationKeys.limit) as? Int
    self.itemCount = aDecoder.decodeObject(forKey: SerializationKeys.itemCount) as? Int
    self.skip = aDecoder.decodeObject(forKey: SerializationKeys.skip) as? Int
    self.totalCount = aDecoder.decodeObject(forKey: SerializationKeys.totalCount) as? Int
    self.items = aDecoder.decodeObject(forKey: SerializationKeys.items) as? [Trip]
    self.hasNext = aDecoder.decodeObject(forKey: SerializationKeys.hasNext) as? Bool
    self.hasPrev = aDecoder.decodeObject(forKey: SerializationKeys.hasPrev) as? Bool
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(limit, forKey: SerializationKeys.limit)
    aCoder.encode(itemCount, forKey: SerializationKeys.itemCount)
    aCoder.encode(skip, forKey: SerializationKeys.skip)
    aCoder.encode(totalCount, forKey: SerializationKeys.totalCount)
    aCoder.encode(items, forKey: SerializationKeys.items)
    aCoder.encode(hasNext, forKey: SerializationKeys.hasNext)
    aCoder.encode(hasPrev, forKey: SerializationKeys.hasPrev)
  }

}
