

//Notes:- This class is used for constructing SignUp Service Request Model

class SignUpRequestModel {
    
    //MARK:- SignUpRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        /**
         This method is used for setting first Name
         
         - parameter userName: String parameter that is going to be set on first Name
         
         - returns: returning Builder Object
         */
        func setFirstName(_ firstName:String) -> Builder{
            requestBody["firstName"] = firstName as AnyObject?
            return self
        }
        
        /**
         This method is used for setting last Name
         
         - parameter userName: String parameter that is going to be set on last Name
         
         - returns: returning Builder Object
         */
        func setLastName(_ lastName:String?) -> Builder{
            guard lastName != nil && !lastName!.isEmpty else{return self}
            
            requestBody["lastName"] = lastName! as AnyObject?
            return self
        }
        
        /**
         This method is used for setting email
         
         - parameter email: String parameter that is going to be set on email
         
         - returns: returning Builder Object
         */
        func setEmail(_ email:String?) -> Builder{
            guard email != nil && !email!.isEmpty else{return self}

            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for setting phoneNumber and country code
         
         - parameter phoneNumber: String parameter that is going to be set on phoneNumber
         
         - parameter countryCode: String parameter that is going to be set on countryCode i.e. +91
         
         - returns: returning Builder Object
         */
        func setPhoneNumber(_ phoneNumber:String)->Builder{
            requestBody["phone"] =  phoneNumber as AnyObject
            return self
        }
        
        /**
         This method is used for setting Phone OTP
         
         - parameter otp: String parameter that is going to be set on otp i.e. 6 digit number
         
         - returns: returning Builder Object
         */
        func setPhoneOTP(_ otp:String)->Builder{
            requestBody["phoneOTP"] = otp as AnyObject?
            return self
        }

        
        /**
         This method is used for setting zipCode
         
         - parameter code: String parameter that is going to be set on Dialling code i.e. +91
         
         - returns: returning Builder Object
         */
        func setDiallingCode(_ code:String)->Builder{
            requestBody["countryCodeExt"] = code as AnyObject?
            return self
        }
        
        /**
         This method is used for setting password
         
         - parameter password: String parameter that is going to be set on password
         
         - returns: returning Builder Object
         */
        func setPassword(_ password:String)->Builder{
            requestBody["password"] = password as AnyObject?
            return self
        }
        
        /**
         This method is used for setting country code
         
         - parameter code: String parameter that is going to be set on country code i.e. IN, AU
         
         - returns: returning Builder Object
         */
        func setCountryCode(_ code:String)->Builder{
            requestBody["countryCode"] = code as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of SignUpRequestModel
         and provide SignUpForm1ViewViewRequestModel object.
         
         -returns : SignUpRequestModel
         */
        func build()->SignUpRequestModel{
            return SignUpRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting SignUp end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return AppConstants.ApiEndPoints.USER_SIGNUP
    }
    
}
