//
//  HelpTableViewCell.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 08/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class HelpTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnExpand: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    // Bind Cell Data with Response Model
    
    func bindCellData(withResponse response: HelpViewResponseModel,withIndexPath indexPath: IndexPath){
        
        let item = response.items?[indexPath.row]
        
        lblTitle.text = item?.question
        lblDescription.text = item?.answer
        btnExpand.tag = indexPath.row
        
        if (item?.isExpanded)! {
            lblDescription.numberOfLines = 0
            btnExpand.setImage(#imageLiteral(resourceName: "ic_expand"), for: .normal)
        }else{
            lblDescription.numberOfLines = 2
            btnExpand.setImage(#imageLiteral(resourceName: "ic_collapse"), for: .normal)
        }
        
        selectionStyle = .none
        
    }
}
