
//Notes:- UITextField an d UIView Extension.s

import Foundation
import UIKit

extension UITextField{
    
    func getBorderBottom(){
        let sizeTextField = self.bounds.size
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0, y: sizeTextField.height-1.0, width: sizeTextField.width, height: 1.0)
        bottomBorder.backgroundColor = UIColor.black.cgColor
        self.layer.addSublayer(bottomBorder)
    }
    
    
    func phoneNumberFieldFormatterShouldReturn(withReplacementString string:String)-> Bool {
        
        guard let text = self.text else{
            return false
        }
        if text.characters.count < 12 {
            if let formattedStr = self.setInputInPhoneNumberFormat(withString: text,appendedString: string){
                self.text = formattedStr
            }
        }else if string != "" {
            return false
        }
        return true
    }
    
    
    fileprivate func setInputInPhoneNumberFormat(withString inputStr: String,appendedString:String) ->String? {
        var str = inputStr
        if appendedString != ""{
            if str.characters.count == 3 {
                str.characters.append("-")
                return str
            }else if str.characters.count == 7{
                str.characters.append("-")
                return str
            }
        }
        return nil
    }

}

extension UIView{
    
    /**
     This method is used to make the view circular
    */
    func makeViewCircular(){
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = true
    }
    
    /**
     This method is used to set corner radius to a view with given width
     
     -parameter width : CGFloat
     
     -returns : Void
     */
    func setCornerCircular(_ width:CGFloat) -> Void {
        self.layer.cornerRadius = width
        self.layer.masksToBounds = true
    }
    
    
    /**
     This method is used to border to the view with given color and broder width
     
     -parameter width: CGFloat
     -parameter color: UIColor default value .white
     
     - returns : Void
     */
    
    func setBorderToView(withWidth width:CGFloat,color:UIColor = .white) -> Void {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func applyHorizontalGradient(){
        let diagonalMode = false
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.bounds.size
        gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        gradientLayer.colors = [UIColor.purple.cgColor,UIColor.blue.cgColor]
        gradientLayer.locations = [0.5,1.0]
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func applyVerticalGradient(){
        let diagonalMode = false
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
        gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        gradientLayer.colors = [UIColor.white.cgColor,UIColor.clear.cgColor]
        gradientLayer.locations = [0.4,1.0]
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = true
    }
    
    /**
     This method is used to end the superview editing.
     
     - returns : Void
     */
    func endSuperViewEditing() ->Void{
        self.superview!.endEditing(true)
    }

    /**
     This method is used to end the view editing.
     
     - returns : Void
     */
    func endViewEditing() ->Void{
        self.endEditing(true)
    }
}
