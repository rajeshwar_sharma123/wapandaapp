//
//  CancelBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class CancelBusinessLogic {
    
    init(){
        print("CancelBusinessLogic init \(self)")
    }
    deinit {
        print("CancelBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform Cancel With Valid Inputs constructed into a CancelRequestModel
     
     - parameter inputData: Contains info for Cancel Trip
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performCancelRequest(withCancelRequestModel CancelRequestModel: CancelRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForCancel()
        CancelAPIRequest().makeAPIRequest(withReqFormData: CancelRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForCancel() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.INVALID_RIDER, message  : AppConstants.ErrorMessages.INVALID_RIDER)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_DRIVER, message  : AppConstants.ErrorMessages.INVALID_DRIVER)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message: AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_CANCELLED, message: AppConstants.ErrorMessages.ALREADY_CANCELLED)
        return errorResolver
    }
}
