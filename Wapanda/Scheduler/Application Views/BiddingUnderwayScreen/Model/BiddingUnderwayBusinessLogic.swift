//
//  BiddingUnderwayBusinessLogic.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 14/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
//Note :- This class contains BiddingUnderway Buisness Logic

class BiddingUnderwayBusinessLogic {
    
    
    deinit {
        print("FeedbackBusinessLogic deinit")
    }
    
    /**
     This method is used for perform auctionDetail With Valid Inputs constructed into a BiddingUnderwayRequestModel
     
     - parameter inputData: Contains info for BiddingUnderwayRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performAuctionDetail(withAuctionDetailRequestModel auctionDetailRequestModel: AuctionDetailRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForAuctionDetail()
        BiddingUnderwayAPIRequest().makeAPIRequestToGetAuctionDetail(withReqFormData: auctionDetailRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for perform accept auction With Valid Inputs constructed into a AcceptAuctionRequestModel
     
     - parameter inputData: Contains info for AcceptAuctionRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performAcceptAuction(withAcceptAuctionRequestModel acceptAuctionRequestModel: AcceptAuctionRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForAcceptAuction()
        BiddingUnderwayAPIRequest().makeAPIRequestToAcceptAuction(withReqFormData: acceptAuctionRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for perform accept auction With Valid Inputs constructed into a AcceptAuctionRequestModel
     
     - parameter inputData: Contains info for AcceptAuctionRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performCancelAuction(withCancelAuctionRequestModel cancelAuctionRequestModel: CancelAuctionRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForCancelAuction()
        BiddingUnderwayAPIRequest().makeAPIRequestToCancelAuction(withReqFormData: cancelAuctionRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForAuctionDetail() ->ErrorResolver{
        
    let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.EMPTY_QUOTES, message  : AppConstants.ErrorMessages.EMPTY_QUOTES_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION_BID)
        errorResolver.registerErrorCode(ErrorCodes.ACCESS_DENIED, message  : AppConstants.ErrorMessages.ACCESS_DENIED)
         return errorResolver
    }
    
    private func registerErrorForAcceptAuction() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION_BID)
        errorResolver.registerErrorCode(ErrorCodes.EMPTY_QUOTES, message  : AppConstants.ErrorMessages.EMPTY_QUOTES_MESSAGE)
       errorResolver.registerErrorCode(ErrorCodes.ACCESS_DENIED, message  : AppConstants.ErrorMessages.ACCESS_DENIED)
        errorResolver.registerErrorCode(ErrorCodes.AUCTION_CLOSED, message  : AppConstants.ErrorMessages.AUCTION_CLOSED)
         return errorResolver
    }
    
    private func registerErrorForCancelAuction() ->ErrorResolver{

        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION_BID)
         errorResolver.registerErrorCode(ErrorCodes.ACCESS_DENIED, message  : AppConstants.ErrorMessages.ACCESS_DENIED)
        errorResolver.registerErrorCode(ErrorCodes.AUCTION_CLOSED, message  : AppConstants.ErrorMessages.AUCTION_CLOSED)
        return errorResolver
    }
   
}
