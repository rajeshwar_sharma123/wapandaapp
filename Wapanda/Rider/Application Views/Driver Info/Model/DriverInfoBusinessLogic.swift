
//Note :- This class contains DriverInfo Buisness Logic

class DriverInfoBusinessLogic {
    
    
    deinit {
        print("DriverInfoBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a DriverInfoRequestModel
     
     - parameter inputData: Contains info for DriverInfo
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performDriverInfo(withDriverInfoRequestModel signUpRequestModel: DriverInfoRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        DriverInfoApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_BIDDING, message  : AppConstants.ErrorMessages.ALREADY_BIDDING)
        errorResolver.registerErrorCode(ErrorCodes.DRIVER_ON_TRIP, message  : AppConstants.ErrorMessages.DRIVER_ON_TRIP)
        errorResolver.registerErrorCode(ErrorCodes.DRIVER_OFFLINE, message  : AppConstants.ErrorMessages.DRIVER_OFFLINE)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.DRIVER_NOT_FOUND)
        return errorResolver
    }
}
