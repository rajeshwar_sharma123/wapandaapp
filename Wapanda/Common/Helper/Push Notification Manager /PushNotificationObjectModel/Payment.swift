//
//  Payment1.swift
//
//  Created by Vipul Sharma on 06/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class Payment: Mappable, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let tripId = "tripId"
        static let paidByChargeId = "paidByChargeId"
        static let stripeAccountAmount = "stripeAccountAmount"
        static let createdAt = "createdAt"
        static let stripeCustomerId = "stripeCustomerId"
        static let paid = "paid"
        static let riderId = "riderId"
        static let amountPayable = "amountPayable"
        static let compTip = "compTip"
        static let stripeAccountId = "stripeAccountId"
        static let compFare = "compFare"
        static let id = "_id"
        static let currency = "currency"
        static let appFeeRatio = "appFeeRatio"
        static let updatedAt = "updatedAt"
        static let driverId = "driverId"
        static let compTipPercent = "compTipPercent"
        static let chargeIds = "chargeIds"
        static let applicationFee = "applicationFee"
    }

    // MARK: Properties
    public var tripId: String?
    public var paidByChargeId: String?
    public var stripeAccountAmount: Double?
    public var createdAt: String?
    public var stripeCustomerId: String?
    public var paid: Bool? = false
    public var riderId: String?
    public var amountPayable: Double?
    public var compTip: Double?
    public var stripeAccountId: String?
    public var compFare: Double?
    public var id: String?
    public var currency: String?
    public var appFeeRatio: Double?
    public var updatedAt: String?
    public var driverId: String?
    public var compTipPercent: Int?
    public var chargeIds: [String]?
    public var applicationFee: Double?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){

    }

    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        tripId <- map[SerializationKeys.tripId]
        paidByChargeId <- map[SerializationKeys.paidByChargeId]
        stripeAccountAmount <- map[SerializationKeys.stripeAccountAmount]
        createdAt <- map[SerializationKeys.createdAt]
        stripeCustomerId <- map[SerializationKeys.stripeCustomerId]
        paid <- map[SerializationKeys.paid]
        riderId <- map[SerializationKeys.riderId]
        amountPayable <- map[SerializationKeys.amountPayable]
        compTip <- map[SerializationKeys.compTip]
        stripeAccountId <- map[SerializationKeys.stripeAccountId]
        compFare <- map[SerializationKeys.compFare]
        id <- map[SerializationKeys.id]
        currency <- map[SerializationKeys.currency]
        appFeeRatio <- map[SerializationKeys.appFeeRatio]
        updatedAt <- map[SerializationKeys.updatedAt]
        driverId <- map[SerializationKeys.driverId]
        compTipPercent <- map[SerializationKeys.compTipPercent]
        chargeIds <- map[SerializationKeys.chargeIds]
        applicationFee <- map[SerializationKeys.applicationFee]
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = tripId { dictionary[SerializationKeys.tripId] = value }
        if let value = paidByChargeId { dictionary[SerializationKeys.paidByChargeId] = value }
        if let value = stripeAccountAmount { dictionary[SerializationKeys.stripeAccountAmount] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = stripeCustomerId { dictionary[SerializationKeys.stripeCustomerId] = value }
        dictionary[SerializationKeys.paid] = paid
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = amountPayable { dictionary[SerializationKeys.amountPayable] = value }
        if let value = compTip { dictionary[SerializationKeys.compTip] = value }
        if let value = stripeAccountId { dictionary[SerializationKeys.stripeAccountId] = value }
        if let value = compFare { dictionary[SerializationKeys.compFare] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = currency { dictionary[SerializationKeys.currency] = value }
        if let value = appFeeRatio { dictionary[SerializationKeys.appFeeRatio] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = driverId { dictionary[SerializationKeys.driverId] = value }
        if let value = compTipPercent { dictionary[SerializationKeys.compTipPercent] = value }
        if let value = chargeIds { dictionary[SerializationKeys.chargeIds] = value }
        if let value = applicationFee { dictionary[SerializationKeys.applicationFee] = value }
        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.tripId = aDecoder.decodeObject(forKey: SerializationKeys.tripId) as? String
        self.paidByChargeId = aDecoder.decodeObject(forKey: SerializationKeys.paidByChargeId) as? String
        self.stripeAccountAmount = aDecoder.decodeObject(forKey: SerializationKeys.stripeAccountAmount) as? Double
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.stripeCustomerId = aDecoder.decodeObject(forKey: SerializationKeys.stripeCustomerId) as? String
        self.paid = aDecoder.decodeBool(forKey: SerializationKeys.paid)
        self.riderId = aDecoder.decodeObject(forKey: SerializationKeys.riderId) as? String
        self.amountPayable = aDecoder.decodeObject(forKey: SerializationKeys.amountPayable) as? Double
        self.compTip = aDecoder.decodeObject(forKey: SerializationKeys.compTip) as? Double
        self.stripeAccountId = aDecoder.decodeObject(forKey: SerializationKeys.stripeAccountId) as? String
        self.compFare = aDecoder.decodeObject(forKey: SerializationKeys.compFare) as? Double
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.currency = aDecoder.decodeObject(forKey: SerializationKeys.currency) as? String
        self.appFeeRatio = aDecoder.decodeObject(forKey: SerializationKeys.appFeeRatio) as? Double
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.driverId = aDecoder.decodeObject(forKey: SerializationKeys.driverId) as? String
        self.compTipPercent = aDecoder.decodeObject(forKey: SerializationKeys.compTipPercent) as? Int
        self.chargeIds = aDecoder.decodeObject(forKey: SerializationKeys.chargeIds) as? [String]
        self.applicationFee = aDecoder.decodeObject(forKey: SerializationKeys.applicationFee) as? Double
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(tripId, forKey: SerializationKeys.tripId)
        aCoder.encode(paidByChargeId, forKey: SerializationKeys.paidByChargeId)
        aCoder.encode(stripeAccountAmount, forKey: SerializationKeys.stripeAccountAmount)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(stripeCustomerId, forKey: SerializationKeys.stripeCustomerId)
        aCoder.encode(paid, forKey: SerializationKeys.paid)
        aCoder.encode(riderId, forKey: SerializationKeys.riderId)
        aCoder.encode(amountPayable, forKey: SerializationKeys.amountPayable)
        aCoder.encode(compTip, forKey: SerializationKeys.compTip)
        aCoder.encode(stripeAccountId, forKey: SerializationKeys.stripeAccountId)
        aCoder.encode(compFare, forKey: SerializationKeys.compFare)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(currency, forKey: SerializationKeys.currency)
        aCoder.encode(appFeeRatio, forKey: SerializationKeys.appFeeRatio)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(driverId, forKey: SerializationKeys.driverId)
        aCoder.encode(compTipPercent, forKey: SerializationKeys.compTipPercent)
        aCoder.encode(chargeIds, forKey: SerializationKeys.chargeIds)
        aCoder.encode(applicationFee, forKey: SerializationKeys.applicationFee)
    }
    
}
