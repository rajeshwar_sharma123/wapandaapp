//
//  AddPaymentRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class AddPaymentRequestModel {
    
    //MARK:- AddPaymentRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var customerId: String!
    var sourceId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.customerId = builder.customerId
        self.sourceId = builder.sourceId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var customerId: String!
        var sourceId: String!

        
        /**
         This method is used for setting sourceId
         
         - parameter sourceId: String parameter that is going to be set on sourceId
         
         - returns: returning Builder Object
         */
        func setSourceId(_ sourceId:String)->Builder{
            self.sourceId = sourceId
            return self
        }
        /**
         This method is used for setting customerId
         
         - parameter customerId: String parameter that is going to be set on customerId
         
         - returns: returning Builder Object
         */
        func setCustomerId(_ customerId:String)->Builder{
            self.customerId = customerId
            return self
        }
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->AddPaymentRequestModel{
            return AddPaymentRequestModel(builderObject: self)
        }
    }
    /**
     This method is used for getting auth stripe end point
     
     -returns: String containg end point
     */
    func getCustomerIdEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.CUSTOMER_ID)
    }
    
    /**
     This method is used for getting auth stripe end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.DELETE_SOURCE_STRIPE,self.customerId,self.sourceId)
    }
    
}
