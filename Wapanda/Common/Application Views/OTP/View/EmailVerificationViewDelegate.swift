
//Notes:-

protocol EmailVerificationViewDelegate:BaseViewProtocol {
    func emailVerifiedSuccessfully() -> Void
    func showToastWithMessage(_ message:String) -> Void
}
