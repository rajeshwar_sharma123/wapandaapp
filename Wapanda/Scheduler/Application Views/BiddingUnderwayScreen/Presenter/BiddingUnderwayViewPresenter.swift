//
//  BiddingUnderwayViewPresenter.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 14/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
class BiddingUnderwayViewPresenter: ResponseCallback{
    
    //MARK:- BiddingUnderwayViewPresenter local properties
    
    private weak var biddingUnderwayViewDelegate: BiddingUnderwayViewDelegate?
    private lazy var biddingUnderwayBusinessLogic         : BiddingUnderwayBusinessLogic = BiddingUnderwayBusinessLogic()
    //MARK:- Constructor
    
    init(delegate responseDelegate:BiddingUnderwayViewDelegate){
        self.biddingUnderwayViewDelegate = responseDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.biddingUnderwayViewDelegate?.hideLoader()
        
        //        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
        //
        //            self.biddingUnderwayViewDelegate?.updateDriverAuctionDetail(withResponseModel: responseObject as! BidDetailResponseModel)
        //        }else{
        //
        //        if responseObject is AuctionDetailResponseModel{
        //            self.biddingUnderwayViewDelegate?.updateAuctionDetail(withResponseModel: responseObject as! AuctionDetailResponseModel)
        //        }
        //    }
        if responseObject is BidDetailResponseModel{
            self.biddingUnderwayViewDelegate?.updateDriverAuctionDetail(withResponseModel: responseObject as! BidDetailResponseModel)
        }
        
        if responseObject is AuctionDetailResponseModel{
            self.biddingUnderwayViewDelegate?.updateAuctionDetail(withResponseModel: responseObject as! AuctionDetailResponseModel)
        }
        
        
        if responseObject is CommonResponseModel{
            self.biddingUnderwayViewDelegate?.updateAcceptOrCancelAuction(withResponseModel: responseObject as! CommonResponseModel)
        }
        
    }
    
    func servicesManagerError(error : ErrorModel){
        self.biddingUnderwayViewDelegate?.hideLoader()
        self.biddingUnderwayViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        
    }
    //MARK:- Methods to call server
    
    /**
     This method is used to send auction fetch request to bussiness layer
     - returns : Void
     */
    func sendAuctionDetailFetchRequest(withAuctionId auctionId: String, shouldShowLoader: Bool) -> Void{
        if shouldShowLoader{
            self.biddingUnderwayViewDelegate?.showLoader()
        }
        var userType = UserType.Rider.rawValue
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
            userType = UserType.Driver.rawValue
        }
        
        let biddingUnderwayRequestModel = AuctionDetailRequestModel.Builder()
            .setAuctionId(auctionId)
            .setUserType(userType)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.biddingUnderwayBusinessLogic.performAuctionDetail(withAuctionDetailRequestModel: biddingUnderwayRequestModel, presenterDelegate: self)
    }
    
    /**
     This method is used to send accept auction request to bussiness layer
     - returns : Void
     */
    func sendAcceptAuctionRequest(withAuctionId auctionId: String, shouldShowLoader: Bool) -> Void{
        if shouldShowLoader{
            self.biddingUnderwayViewDelegate?.showLoader()
        }
        
        let acceptAuctionRequestModel = AcceptAuctionRequestModel.Builder()
            .setAuctionId(auctionId)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        self.biddingUnderwayBusinessLogic.performAcceptAuction(withAcceptAuctionRequestModel: acceptAuctionRequestModel, presenterDelegate: self)
    }
    
    /**
     This method is used to send reject auction request to bussiness layer
     - returns : Void
     */
    func sendCancelAuctionRequest(withAuctionId auctionId: String, shouldShowLoader: Bool) -> Void{
        if shouldShowLoader{
            self.biddingUnderwayViewDelegate?.showLoader()
        }
        
        var userType = UserType.Rider.rawValue
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
            userType = UserType.Driver.rawValue
        }
        
        let cancelAuctionRequestModel = CancelAuctionRequestModel.Builder()
            .setAuctionId(auctionId)
            .setUserType(userType)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        self.biddingUnderwayBusinessLogic.performCancelAuction(withCancelAuctionRequestModel: cancelAuctionRequestModel, presenterDelegate: self)
    }
    
    
}
