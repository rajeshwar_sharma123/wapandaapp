
//Notes:- This class is used as presenter for CreateBidViewPresenter

import Foundation
import ObjectMapper

class CreateBidViewPresenter: ResponseCallback{
    
//MARK:- CreateBidViewPresenter local properties
    
    private weak var createBidViewDelegate          : CreateBidViewDelegate?
    private lazy var createBidBusinessLogic         : CreateBidBusinessLogic = CreateBidBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:CreateBidViewDelegate){
        self.createBidViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.createBidViewDelegate?.hideLoader()
        if let response = responseObject as? CreateBidResponseModel{
            self.createBidViewDelegate?.bidCreated(withResponseData: response)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.createBidViewDelegate?.hideLoader()
        self.createBidViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call createBid Api.
    
    func sendCreateBidRequest(withCreateBidViewRequestModel createBidViewRequestModel:CreateBidViewRequestModel){
        
        self.createBidViewDelegate?.showLoader()
        
        let requestModel: CreateBidRequestModel!
        
        requestModel = CreateBidRequestModel.Builder()
                        .setSourceCoordinate(lat: createBidViewRequestModel.latFrom, lng: createBidViewRequestModel.lngFrom)
                        .setDestinationCoordinate(lat: createBidViewRequestModel.latTo, lng: createBidViewRequestModel.lngTo)
                        .setDriverId(createBidViewRequestModel.driverId)
                        .setRiderId(createBidViewRequestModel.riderId)
                        .setEstDistance(createBidViewRequestModel.estDistance)
                        .setEstDuration(createBidViewRequestModel.estDuration)
                        .setStartTime(createBidViewRequestModel.startTime).setToAddress(createBidViewRequestModel.toAddress).setFromAddress(createBidViewRequestModel.fromAddress)
                        .setRiderCounterPrice(createBidViewRequestModel.riderCounterPrice)
                        .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                        .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                        .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                        .build()
        
        self.createBidBusinessLogic.performCreateBid(withCreateBidRequestModel: requestModel, presenterDelegate: self)
    }
}
