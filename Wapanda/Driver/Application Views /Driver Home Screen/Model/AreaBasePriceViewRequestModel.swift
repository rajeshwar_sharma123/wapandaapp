//
//  AreaBasePriceViewRequestModel.swift
//  Wapanda
//
//  Created by Daffolapmac on 05/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

struct AreaBasePriceViewRequestModel {
    var latitude                   : Double!
    var longitude                   : Double!
}

