
//Note :- This class contains ProcessPayment Buisness Logic

class ProcessPaymentBusinessLogic {
    
    
    deinit {
        print("ProcessPaymentBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs(Email , password) construceted into a ProcessPaymentRequestModel
     
     - parameter inputData: Contains info for ProcessPayment
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performProcessPayment(withProcessPaymentRequestModel signUpRequestModel: ProcessPaymentRequestModel, presenterDelegate:ProcessPaymentViewPresenter) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        ProcessPaymentApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    /**
     This method is used for perform sign Up With Valid Inputs(Email , password) construceted into a ProcessPaymentRequestModel
     
     - parameter inputData: Contains info for ProcessPayment
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performAddTip(withProcessPaymentRequestModel signUpRequestModel: ProcessPaymentRequestModel, presenterDelegate:ProcessPaymentViewPresenter) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        ProcessPaymentApiRequest().makeAddTipAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.PAYMENT_FAILED, message  : AppConstants.ErrorMessages.PAYMENT_FAILED)
        errorResolver.registerErrorCode(ErrorCodes.PAYMENT_ALREADY_PAID, message  : AppConstants.ErrorMessages.PAYMENT_ALREADY_PAID)
        errorResolver.registerErrorCode(ErrorCodes.PAYMENT_RIDER_NO_SOURCE, message  : AppConstants.ErrorMessages.PAYMENT_RIDER_NO_SOURCE)
        errorResolver.registerErrorCode(ErrorCodes.PAYMENT_DRIVER_NO_ACCOUNT, message  : AppConstants.ErrorMessages.PAYMENT_DRIVER_NO_ACCOUNT)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.STRIPE_ACCOUNT_IN_USE, message  : AppConstants.ErrorMessages.STRIPE_ACCOUNT_IN_USE)
        
        
        
        
        
        return errorResolver
    }
}
