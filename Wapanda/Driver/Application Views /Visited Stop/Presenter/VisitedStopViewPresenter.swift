
//Notes:- This class is used as presenter for VisitedStopViewPresenter

import Foundation
import ObjectMapper

class VisitedStopViewPresenter: ResponseCallback{
    
//MARK:- VisitedStopViewPresenter local properties
    
    private weak var visitedStopViewDelegate          : VisitedStopViewDelegate?
    private lazy var visitedStopBusinessLogic         : VisitedStopBusinessLogic = VisitedStopBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:VisitedStopViewDelegate){
        self.visitedStopViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let dataModel = responseObject as? Trip{
            self.visitedStopViewDelegate?.driverVisitedStopSuccessfully(withResponseModel: dataModel)
        }
        
        self.visitedStopViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error: ErrorModel){
        self.visitedStopViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.visitedStopViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call VisitedStop Api.
    
    func sendVisitedStopRequest(withVisitedStopViewRequestModel visitedStopViewRequestModel:VisitedStopViewRequestModel){
        
        self.visitedStopViewDelegate?.showLoader()
        
        let requestModel = VisitedStopRequestModel.Builder()
                            .setStopId(visitedStopViewRequestModel.stopId)
                            .setTripId(visitedStopViewRequestModel.tripId)
                            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                            .build()
        self.visitedStopBusinessLogic.performVisitedStop(withVisitedStopRequestModel: requestModel, presenterDelegate: self)
    }
}
