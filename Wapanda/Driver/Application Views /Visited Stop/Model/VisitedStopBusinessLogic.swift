
//Note :- This class contains VisitedStop Buisness Logic

class VisitedStopBusinessLogic {
    
    
    deinit {
        print("VisitedStopBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs(Email , password) construceted into a VisitedStopRequestModel
     
     - parameter inputData: Contains info for VisitedStop
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performVisitedStop(withVisitedStopRequestModel visitedStopRequestModel: VisitedStopRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        VisitedStopApiRequest().makeAPIRequest(withReqFormData: visitedStopRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_VISITED, message  : AppConstants.ErrorMessages.ALREADY_VISITED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.TRIP_NOT_STARTED_YET, message  : AppConstants.ErrorMessages.TRIP_NOT_STARTED_YET)
        errorResolver.registerErrorCode(ErrorCodes.ACCESS_DENIED, message  : AppConstants.ErrorMessages.ACCESS_DENIED)

        return errorResolver
    }
}
