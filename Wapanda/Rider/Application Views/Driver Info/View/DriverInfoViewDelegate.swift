
//Notes:- This protocol is used as a interface which is used by DriverInfoPresenter to tranfer info to DriverInfoViewController

protocol DriverInfoViewDelegate:BaseViewProtocol {
    func driverInfoFetched(withResponseModel data: DriverInfoResponseModel)
}
