import UIKit
import ObjectMapper

class BidDetailResponseModel: Mappable {
    
    var detail: AuctionDetailResponseModel?
    var fares: FairValuesModel?
    var riderName = ""
    var riderImage = ""

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        
        detail <- map["auctionDetail"]
        fares <- map["fares"]
        riderName <- map["auctionDetail.rider.firstName"]
        riderImage <- map["auctionDetail.rider.profileImageFileId"]
    }

}
