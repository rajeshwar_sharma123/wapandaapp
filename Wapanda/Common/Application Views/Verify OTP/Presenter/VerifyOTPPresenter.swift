
//Notes:- This class is used as presenter for OTPVerificationPresenter

import Foundation
import ObjectMapper

class VerifyOTPPresenter: ResponseCallback{
    
//MARK:- OTPPopupViewPresenter local properties
    private weak var otpPopupViewDelegate          : VerifyOTPViewDelegate?
    private var otpPopupBusinessLogic              : VerifyOTPBusinessLogic!
    private weak var textFieldValidationDelegate : TextFieldValidationDelegate?
    
//MARK:- Constructor
    init(delegate responseDelegate:VerifyOTPViewDelegate){
        self.otpPopupViewDelegate = responseDelegate
        self.textFieldValidationDelegate = responseDelegate as? TextFieldValidationDelegate
        self.otpPopupBusinessLogic = VerifyOTPBusinessLogic()
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.otpPopupViewDelegate?.hideLoader()
        self.otpPopupViewDelegate?.otpVerifiedSuccessfully()
    }
    
    func servicesManagerError(error : ErrorModel){
        self.otpPopupViewDelegate?.hideLoader()
        self.otpPopupViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- API calls
    
    func verify(withOTP otp:String, email: String){
        
        guard validateInput(withData: otp) else {
            return
        }
        
        self.otpPopupViewDelegate?.showLoader()

        let otpPopupRequestModel:VerifyOTPRequestModel = VerifyOTPRequestModel.Builder()
            .setOTP(otp)
            .setEmail(email).setEntryType(EntryType.Email)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.otpPopupBusinessLogic.performOTPVerification(withVerifyOTPRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
    
    func verify(withOTP otp:String, phone: String,andCountryCode code:String){
        
        guard validateInput(withData: otp) else {
            return
        }
        
        self.otpPopupViewDelegate?.showLoader()
        
        let otpPopupRequestModel:VerifyOTPRequestModel = VerifyOTPRequestModel.Builder()
            .setPhone(phone).setPhoneOTP(otp).setCountryCodeExt(code).setEntryType(EntryType.Phone)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.otpPopupBusinessLogic.performOTPVerification(withVerifyOTPRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
    
//MARK:- Validation for input fields method
    
    func validateInput(withData otp:String) -> Bool {
        
        guard !otp.isEmptyString() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_OTP, forTextFields: .OTP)
            return false
        }
        
        guard otp.isOTPValid() else{
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_OTP, forTextFields: .OTP)
            return false
        }
        
        return true
    }
}
