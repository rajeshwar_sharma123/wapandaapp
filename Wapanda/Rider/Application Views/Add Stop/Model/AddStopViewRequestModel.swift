
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct AddStopViewRequestModel {
    
    var lat                   : Double!
    var lng                   : Double!
    var tripId                : String!
    var address               : String!
}
