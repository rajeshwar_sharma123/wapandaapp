//
//  DriverScheduleDetailPresenter.swift
//  Wapanda
//
//  Created by MacBook on 12/7/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

protocol DriverScheduleDetailViewDelegate: BaseViewProtocol{
    func pickUpConfirmed(withResponseModel response: CancelResponseModel)
}

class DriverScheduleDetailPresenter: ResponseCallback {
    //MARK:- RiderScheduleDetailViewPresenter local properties
    
    private weak var driverScheduleDetailViewDelegate: DriverScheduleDetailViewDelegate?
    private lazy var driverScheduleDetailBussinessLogic: DriverScheduleDetailBussinessLogic = DriverScheduleDetailBussinessLogic()
    
    
    //MARK:- Constructor
    init(delegate responseDelegate:DriverScheduleDetailViewDelegate){
        self.driverScheduleDetailViewDelegate = responseDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.driverScheduleDetailViewDelegate?.hideLoader()
        if responseObject is CancelResponseModel{
            self.driverScheduleDetailViewDelegate?.pickUpConfirmed(withResponseModel: responseObject as! CancelResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.driverScheduleDetailViewDelegate?.hideLoader()
        self.driverScheduleDetailViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        
    }
    
    
    //MARK:- Methods to call server
    /**
     This method is used to send start pick up request to bussiness layer
     - returns : Void
     */
    func sendStartPickUpConfirmationRequest(tripId: String,latFrom : Double?, lngFrom: Double?) -> Void{
        
        self.driverScheduleDetailViewDelegate?.showLoader()
        
        let driverScheduleDetailRequestModel = DriverScheduleDetailRequestModel.Builder()
            .setTripId(tripId)
            .setLatitude(latFrom)
            .setLongitude(lngFrom)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        self.driverScheduleDetailBussinessLogic.performStartPickUpConfirmation(withDriverScheduleDetailRequestModel: driverScheduleDetailRequestModel, presenterDelegate: self)
    }
}
