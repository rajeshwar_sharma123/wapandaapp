

//Notes:- This class is used for constructing Feedback Service Request Model

class FeedbackRequestModel {
    
    //MARK:- FeedbackRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var tripId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.tripId = builder.tripId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var tripId: String!
        
        /**
         This method is used for setting trip id
         
         - parameter tripId: trip id as string parameter
         
         - returns: returning Builder Object
         */
        func setTrip(_ tripId:String) -> Builder{
            self.tripId = tripId
            return self
        }
        
        /**
         This method is used for setting trip id
         
         - parameter tripId: trip id as string parameter
         
         - returns: returning Builder Object
         */
        func setRate(_ rate:Int) -> Builder{
            self.requestBody["rate"] = rate as AnyObject
            return self
        }

        
        /**
         This method is used for setting trip id
         
         - parameter tripId: trip id as string parameter
         
         - returns: returning Builder Object
         */
        func setReview(_ review:String?) -> Builder{
            if let value = review{
                self.requestBody["review"] = value as AnyObject
            }
            
            return self
        }

        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of FeedbackRequestModel
         and provide FeedbackForm1ViewViewRequestModel object.
         
         -returns : FeedbackRequestModel
         */
        func build()->FeedbackRequestModel{
            return FeedbackRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Feedback end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.WRITE_REVIEW, self.tripId)
    }
    
}
