

import UIKit
import GooglePlaces
import GoogleMaps
import IQKeyboardManager

enum ADDRESS_TYPE{
    case SOURCE
    case DESTINATION
}


protocol EditSourceAndDestinationViewProtocol {
    func editAddressScreenDidDismiss(withSourceAddress source: SelectedPlaceModel!, withDestinationAddress destination: SelectedPlaceModel!)
    func editSourceDestinationScreenDidDismiss()
}

class EditSourceAndDestinationViewController: BaseViewController {
    
    //Top view...
    
    //Outlets
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var txtFieldSourceAddress: UITextField!
    @IBOutlet weak var txtFieldDestinationAddress: UITextField!
    
    //Global Variables
    var dataSource: [GMSAutocompletePrediction] = []
    var dataSourceUserAddresses: [Address] = []
    var presenterGooglePlaces: GooglePlaceViewPresenter!
    var placeDetailPresenter: GooglePlaceDetailViewPresenter!
    var selectedSourceAddress: SelectedPlaceModel!
    var selectedDestinationAddress: SelectedPlaceModel!
     var selectedAddressTitle: String!
    var editAddressType: ADDRESS_TYPE!
    var addAddressTypeSelected = ADD_ADDRESS_TYPE.NONE
    var searchString = ""
    var delegate: EditSourceAndDestinationViewProtocol?
    
    //Bottom View
    @IBOutlet weak var tblView: UITableView!

    //MARK: View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.placeDetailPresenter = GooglePlaceDetailViewPresenter(delegate: self)
        self.presenterGooglePlaces = GooglePlaceViewPresenter(delegate: self)
        self.txtFieldSourceAddress.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.txtFieldDestinationAddress.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.initalizeUserAddresess()
        self.registerTableViewCells()
    }
    
    func initScreen(withSourceAddress source: SelectedPlaceModel!, withDestinationAddress destination: SelectedPlaceModel!, withEditAddressType type: ADDRESS_TYPE){
        self.selectedSourceAddress = source
        self.selectedDestinationAddress = destination
        self.editAddressType = type
    }
    
    func setInitialSourceAndDestinationTitle(){
        if self.editAddressType == .DESTINATION{
            self.txtFieldSourceAddress.text = self.selectedSourceAddress.title
            self.txtFieldDestinationAddress.text = ""
        }
        else{
            self.txtFieldSourceAddress.text = ""
            self.txtFieldDestinationAddress.text = self.selectedDestinationAddress.title
        }
    }
    
    func setFirstResponderInitial(){
        if self.selectedSourceAddress == nil{
            self.txtFieldSourceAddress.becomeFirstResponder()
        }
        
        if self.selectedDestinationAddress == nil{
            self.txtFieldDestinationAddress.becomeFirstResponder()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialViewCustomization()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewTop.addShadow()
        self.addBlurrEffectOnView()
        self.setFirstResponderInitial()
        IQKeyboardManager.shared().isEnableAutoToolbar = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: View Customization Helper Methods
    func initialViewCustomization(){
        self.setInitialSourceAndDestinationTitle()
    }
    
    func addBlurrEffectOnView(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 0.3
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubview(toBack: blurEffectView)
    }
    
    func initalizeUserAddresess(){
        
            if let home = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
                home.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME
                self.dataSourceUserAddresses.append(home)
            }
            
            if let work = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
                work.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_WORK
                self.dataSourceUserAddresses.append(work)
            }
            
            if let recent = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.recentlyVisited{
                recent.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT
                self.dataSourceUserAddresses.append(recent)
            }
    }

    
    //MARK: Actions
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.editSourceDestinationScreenDidDismiss()
        }
    }
    
    @IBAction func btnGoClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate!.editAddressScreenDidDismiss(withSourceAddress: self.selectedSourceAddress, withDestinationAddress: self.selectedDestinationAddress)
            self.delegate?.editSourceDestinationScreenDidDismiss()
        }
    }
}

//MARK: UITextField Delegate ...
extension EditSourceAndDestinationViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.clearSearchResults()
        
        if textField == self.txtFieldSourceAddress{
            self.editAddressType = .SOURCE
        }
        else{
            self.editAddressType = .DESTINATION
        }
        tblView.isHidden = false
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.startSearchForPlaces()
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if (range.location == 0 && string.characters.count == 0){
            //Empty String handling
            self.searchString = ""
        }

        if !string.isEmpty{
            self.searchString = textField.text! + string
        }
       
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.searchString = ""
        tblView.isHidden = false
        return true
    }
    
    //MARK: Helper Methods
    func textFieldDidChange(){
        self.startSearchForPlaces()
    }

    func startSearchForPlaces(){
        
        if !searchString.isEmpty{
            let requestModel = GooglePlaceViewRequestModel(searchString: searchString.getWhitespaceTrimmedString(), userLocationCoordinate: LocationServiceManager.sharedInstance.currentLocation?.coordinate)
            self.presenterGooglePlaces.sendGooglePlaceRequest(withGooglePlaceViewRequestModel: requestModel)
        }
        else{
            self.clearSearchResults()
        }
    }
    
    func clearSearchResults(){
        self.dataSource = []
        
        self.tblView.reloadData()
    }
}

//MARK: Google Place Delegate ...
extension EditSourceAndDestinationViewController: GooglePlaceViewDelegate{
    
    func showSearchList(list: [GMSAutocompletePrediction]){
        self.dataSource = list
        self.tblView.reloadData()
    }
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
}

//MARK: Google Place Detail Delegate ...
extension EditSourceAndDestinationViewController: GooglePlaceDetailViewDelegate{
    
    func placeDetailWithData(data: GMSPlace){
        
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedAddressTitle, coordinate: data.coordinate)
        self.onAddressSelection(withAddress: model)
    }
}

//MARK: UITableView Delegate & Datasource...
extension EditSourceAndDestinationViewController: UITableViewDelegate, UITableViewDataSource{
    
    func registerTableViewCells(){
        self.tblView.register(UINib(nibName: String(describing: AddressSearchResultTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: AddressSearchResultTableViewCell.self))
        self.tblView.register(UINib(nibName: String(describing: ImageViewWithTitleTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ImageViewWithTitleTableViewCell.self))
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource.count == 0{
            return self.dataSourceUserAddresses.count
        }
        else{
            return self.dataSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if dataSource.count == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageViewWithTitleTableViewCell.self)) as! ImageViewWithTitleTableViewCell
            
            let selectedAddress = self.dataSourceUserAddresses[indexPath.row]
            
            if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT{
                cell.bind(withTitle: selectedAddress.address!)
            }
            else if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME{
                cell.bind(withTitle: selectedAddress.address_type_title!,andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_home"))
            }
            else{
                cell.bind(withTitle: selectedAddress.address_type_title!, andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_work"))
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddressSearchResultTableViewCell.self)) as! AddressSearchResultTableViewCell
            let selectedAddress = self.dataSource[indexPath.row]
            cell.bind(withTitle: selectedAddress.attributedPrimaryText , WithStateAndCountry: selectedAddress.attributedSecondaryText ?? NSAttributedString(string: ""))
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0{
            
            cell.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
        else if indexPath.row == (self.dataSource.count - 1){
            
            cell.roundCorners([.bottomLeft, .bottomRight], radius: 10.0)
            
        }
        else if self.dataSource.count == 0 && indexPath.row == (self.dataSourceUserAddresses.count - 1){
            cell.roundCorners([.bottomLeft, .bottomRight], radius: 10.0)
        }
        
        //When we have only 1 object
        if self.dataSource.count == 0 && self.dataSourceUserAddresses.count == 1{
            cell.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10.0)
        }
        else if self.dataSource.count == 1 && self.dataSourceUserAddresses.count == 0{
            cell.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10.0)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataSource.count == 0{
            
             self.selectedAddressTitle = self.dataSourceUserAddresses[indexPath.row].address
            self.handleUserAddressSelection(withAddress: self.dataSourceUserAddresses[indexPath.row])
            
        }
        else{
            
            //Get Place Detail
            let selectedPlace = self.dataSource[indexPath.row]
            self.selectedAddressTitle = self.dataSource[indexPath.row].attributedFullText.string
            self.placeDetailPresenter.sendGooglePlaceDetailRequest(withGooglePlaceDetailViewRequestModel: GooglePlaceDetailViewRequestModel(placeId: selectedPlace.placeID ?? ""))
        }
        tableView.isHidden = true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    func handleUserAddressSelection(withAddress address: Address){
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedAddressTitle, coordinate: CLLocationCoordinate2D(latitude: address.lat!, longitude: address.lng!))
        
        //Set Address
        if self.editAddressType == .DESTINATION{
            self.txtFieldDestinationAddress.text = self.selectedAddressTitle
        }
        else{
            self.txtFieldSourceAddress.text = self.selectedAddressTitle
        }
        
        self.onAddressSelection(withAddress: model)
    }
    
    func onAddressSelection(withAddress address: SelectedPlaceModel){
        if editAddressType == .DESTINATION{
            self.selectedDestinationAddress = address
        }
        else{
            self.selectedSourceAddress = address
        }
        
        self.updateSourceAndDestinationAddress()
    }
    
    func updateSourceAndDestinationAddress(){
        self.txtFieldSourceAddress.text = self.selectedSourceAddress.title
        self.txtFieldDestinationAddress.text = self.selectedDestinationAddress.title
    }
}


