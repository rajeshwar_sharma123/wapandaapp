//
//  ImageViewWithTitleTableViewCell.swift
//  Wapanda
//
//  Created by daffomac-31 on 18/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class ImageViewWithTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(withTitle title: String,andDescription desc:String = "", img: UIImage = #imageLiteral(resourceName: "ic_location")){
        self.lblTitle.text = title
        self.lblDetail.text = desc
        self.imgView.image = img
    }
}
