//
//  CarTypeMakers.swift
//  Wapanda
//
//  Created by Daffolapmac on 04/12/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
enum CarTypes : String
{
    case Taxi                   = "Taxi"
    case Standard               = "Standard"
    case XL                     = "XL"
    case BlackCar               = "BlackCar"
    case SUV                    = "SUV"
}
class CarTypeMakers: UIView {

    @IBOutlet weak var labelCarType: UILabel!
    @IBOutlet weak var imageViewArrow: UIImageView!
    @IBOutlet weak var labelPercent: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    class func initialiseTaxiMarker()->CarTypeMakers
    {
        let marker = Bundle.main.loadNibNamed("CarTypeMakers", owner: self, options: nil)?[1] as! CarTypeMakers
        marker.labelCarType.text = "TAXI"
        marker.frame = CGRect(x: 0, y: 0, width: 100, height: 80)
        marker.isHidden = true
        return marker
    }
    class func initialiseSUVMarker()->CarTypeMakers
    {
        let marker = Bundle.main.loadNibNamed("CarTypeMakers", owner: self, options: nil)?.first as! CarTypeMakers
        marker.labelCarType.text = "SUV"
        marker.frame = CGRect(x: 0, y: 0, width: 100, height: 80)
        marker.isHidden = true
        return marker
    }
    class func initialiseStandardMarker()->CarTypeMakers
    {
        let marker = Bundle.main.loadNibNamed("CarTypeMakers", owner: self, options: nil)?.first as! CarTypeMakers
        marker.labelCarType.text = "STANDARD"
        marker.frame = CGRect(x: 0, y: 0, width: 100, height: 80)
        marker.isHidden = true
        return marker
    }
    class func initialiseXLMarker()->CarTypeMakers
    {
        let marker = Bundle.main.loadNibNamed("CarTypeMakers", owner: self, options: nil)?.first as! CarTypeMakers
        marker.labelCarType.text = "XL"
        marker.frame = CGRect(x: 0, y: 0, width: 100, height: 80)
        marker.isHidden = true
        return marker
    }
    class func initialiseBlackCarMarker()->CarTypeMakers
    {
        let marker = Bundle.main.loadNibNamed("CarTypeMakers", owner: self, options: nil)?[1] as! CarTypeMakers
        marker.labelCarType.text = "BLACK CAR"
        marker.frame = CGRect(x: 0, y: 0, width: 100, height: 80)
        marker.isHidden = true
        return marker
    }
    
    class func getCarTypePriceInformation(_ carTypePrices:[IndexArray],carType:CarTypes)->IndexArray
    {
        let userCarType = carTypePrices.filter({ (obj) -> Bool in
            if obj.cartype == carType.rawValue
            {
                return true
            }
            return false
        })
        if userCarType.count != 0
        {
            return userCarType[0]
        }
        return IndexArray(JSON:[:])!
        
    }

}
