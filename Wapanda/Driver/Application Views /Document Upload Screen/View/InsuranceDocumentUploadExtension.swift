//
//  InsuranceDocumentUploadExtension.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/21/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
extension DocumentsUploadScreenViewController
{
    static let INSURANCE_SCREEN_DESCRIPTION = "Take a photo of your Insurance Certificate. Make sure your name, VIN number, insurance company, and expiration date are clearly visible."
    static let INSURANCE_SCREEN_TITLE = "Insurance Information"
    static let INSURANCE_SCREEN_STEP = "Step 2 of 7"
    static let INSURANCE_SCREEN_PLACEHOLDER_BACKGROUND = UIColor.appDocumentPlaceholderColor()
    
    func navigateToSelfieController(){
        self.shouldUpdateProfile = true
        let selfieVC = UIViewController.getViewController(DocumentsUploadScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        self.documentScreenRequestModel.addedImages = [UIImage?](repeating: nil, count: AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD)
        selfieVC.uploadScreenType = UploadScreenType.Selfie
      //  selfieVC.objProfileUploadDocument = self.objProfileUploadDocument
        self.navigationController?.pushViewController(selfieVC, animated: true)
    }
    
    func initialiseInsuranceUploadCellOfCollectionView(_ cell : DocumentPreviewCollectionViewCell,index:Int) -> DocumentPreviewCollectionViewCell
    {
        cell.imageViewPlaceHolder.backgroundColor = DocumentsUploadScreenViewController.INSURANCE_SCREEN_PLACEHOLDER_BACKGROUND
        cell.imageViewPreview.isHidden = true
        cell.btnAddImage.isHidden = false
        cell.imageViewPlaceHolder.isHidden = false
        cell.imageViewPlaceHolder.contentMode = .scaleAspectFit
        if let image = self.documentScreenRequestModel.addedImages[index]
        {
            cell.btnAddImage.isHidden = true
            cell.imageViewPlaceHolder.isHidden = true
            cell.imageViewPreview.image = image
            cell.imageViewPreview.isHidden = false
            cell.imageViewPreview.clipsToBounds = false
            cell.viewBackground.layer.masksToBounds = false
            return cell
        }
        guard let _ = self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds else
        {
            return cell
        }
        
        guard index < (self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds?.count)!
            else {
                return cell
        }
        cell.viewBackground.layer.cornerRadius = 0
        cell.viewBackground.layer.masksToBounds = false
        cell.btnAddImage.isHidden = true
        cell.imageViewPreview.isHidden = false
        cell.imageViewPlaceHolder.isHidden = false
        cell.imageViewPreview.kf.indicatorType = .activity
        cell.imageViewPreview.kf.setImage(with: URL(string:"\(AppConstants.URL.BASE_URL)/files/\(String(describing: (self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds?[index])!))?api_key=\(AppConstants.APIRequestHeaders.API_KEY_VALUE)"), placeholder: UIImage(), options: nil, progressBlock:nil) { (image, error, cacheType, url) in
            cell.imageViewPlaceHolder.isHidden = true

        }

        return cell
        
    }
    
}
