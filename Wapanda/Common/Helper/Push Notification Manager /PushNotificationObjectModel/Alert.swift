//
//  Alert.swift
//
//  Created by  on 19/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Alert: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let locKey = "loc-key"
    static let locArgs = "loc-args"
  }

  // MARK: Properties
  public var locKey: String?
  public var locArgs: [String]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    locKey <- map[SerializationKeys.locKey]
    locArgs <- map[SerializationKeys.locArgs]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = locKey { dictionary[SerializationKeys.locKey] = value }
    if let value = locArgs { dictionary[SerializationKeys.locArgs] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.locKey = aDecoder.decodeObject(forKey: SerializationKeys.locKey) as? String
    self.locArgs = aDecoder.decodeObject(forKey: SerializationKeys.locArgs) as? [String]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(locKey, forKey: SerializationKeys.locKey)
    aCoder.encode(locArgs, forKey: SerializationKeys.locArgs)
  }

}
