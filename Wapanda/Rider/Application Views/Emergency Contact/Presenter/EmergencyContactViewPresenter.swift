
//Notes:- This class is used as presenter for EmergencyContactViewPresenter

import Foundation
import ObjectMapper

class EmergencyContactViewPresenter: ResponseCallback{
    
//MARK:- EmergencyContactViewPresenter local properties
    
    private weak var emergencyContactViewDelegate          : EmergencyContactViewDelegate?
    private lazy var emergencyContactBusinessLogic         : UpdateUserBusinessLogic = UpdateUserBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:EmergencyContactViewDelegate){
        self.emergencyContactViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let response = responseObject as? LoginResponseModel{
            self.emergencyContactViewDelegate?.emergencyContactUpdatedSuccessfully(withUpdatedUserProfile: response)
        }
        
        self.emergencyContactViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error: ErrorModel){
       self.emergencyContactViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.emergencyContactViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call EmergencyContact Api.
    
    func sendEmergencyContactRequest(withEmergencyContactViewRequestModel emergencyContactViewRequestModel:EmergencyContactViewRequestModel){
        
        self.emergencyContactViewDelegate?.showLoader()
        
        let viewModel = UpdateUserRequestModel.Builder()
                        .setEmergencyContact(emergencyContactViewRequestModel.emergencyContact)
                        .setEmergencyContactName(emergencyContactViewRequestModel.emergencyContactName)
                        .setEmergencyContactImageId(emergencyContactViewRequestModel.emergencyContactImageId)
                        .setUserId(emergencyContactViewRequestModel.userId)
                        .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                        .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                        .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).build()
        
        self.emergencyContactBusinessLogic.performUpdateUser(withUpdateUserRequestModel: viewModel, presenterDelegate: self)
    }
}
