
//Notes:- This protocol is used as a interface which is used by WapandaPresenter to tranfer info to WapandaViewController

protocol FeedbackViewDelegate:BaseViewProtocol {
    func feedbackSubmittedSucessfully()
}
