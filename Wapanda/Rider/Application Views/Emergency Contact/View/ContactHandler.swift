//
//  ContactHandler.swift
//  Wapanda
//
//  Created by daffomac-31 on 13/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import JHTAlertController

protocol ContactViewDelegate: class{
    func contactPickerDidSelectContact(withContact contact: CNContact)
    func contactPickerDismissed()
    func deviceContactAccessDenied()
}

class ContactsHandler: NSObject,CNContactPickerDelegate {
    static let sharedInstance = ContactsHandler()
    
    var contactStore = CNContactStore()
    var parentVC: UIViewController!
    weak var delegate: ContactViewDelegate?
    
    
    /// This method is used to check if the user approved permissions for entering his Phonebook
    ///
    /// - Parameter completionHandler: completionHandler with status for permission
    func requestForAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
            
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        self.delegate?.deviceContactAccessDenied()
                    }
                }
            })
            
        default:
            completionHandler(false)
        }
    }
    
    
    /// This method is used for presenting the native phonebook using CNContactsPickerController
    ///
    /// - Parameter vc: target view controller that is presenting phonebook
    func openAddressbook(vc: UIViewController){
        requestForAccess { (accessGranted) in
            if accessGranted == true{
                self.parentVC = vc;
                let controller = CNContactPickerViewController()
                controller.delegate = self
                vc.present(controller,animated: true, completion: nil)
            }
        }
    }
    
    //MARK: Contact Picker Delegate 
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        if contact.isKeyAvailable(CNContactPhoneNumbersKey){
            // handle the selected contact
            self.delegate?.contactPickerDidSelectContact(withContact: contact)
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancelled picking a contact")
        self.delegate?.contactPickerDismissed()
    }
}
