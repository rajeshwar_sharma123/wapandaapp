//
//  CounterPriceScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CounterPriceScreenRequestModel {
    
    var driverCounterPrice    : Double!
    var bidingId    : String!
}
