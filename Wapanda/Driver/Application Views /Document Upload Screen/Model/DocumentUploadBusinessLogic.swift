//
//  DocumentUploadBusinessLogic
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
class DocumentUploadBusinessLogic {
    
    init(){
        print("DocumentUploadBusinessLogic init \(self)")
    }
    deinit {
        print("DocumentUploadBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for update Driver Info With Valid profileId, imageids, constructed into a DocumentUploadRequestModel
     
     - parameter inputData: Contains info for Document Upload
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func uploadDocument(withImage docUploadModel:DocumentUploadRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForTaxInformation()
        DocumentUploadAPIRequest().makeAPIRequest(withImageData: docUploadModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for update Driver Info With Valid profileId, imageids, constructed into a DocumentUploadRequestModel
     
     - parameter inputData: Contains info for Document Upload
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func updateDriverDocuments<T:Mappable>(withImage docUploadModel:DocumentUploadRequestModel, presenterDelegate:ResponseCallback,returningClass:T.Type) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForTaxInformation()
        DocumentUploadAPIRequest().makeAPIRequest(withReqFormData: docUploadModel, errorResolver: errorResolver, responseCallback: presenterDelegate,returningClass:T.self)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForTaxInformation() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        
        return errorResolver
    }
}
