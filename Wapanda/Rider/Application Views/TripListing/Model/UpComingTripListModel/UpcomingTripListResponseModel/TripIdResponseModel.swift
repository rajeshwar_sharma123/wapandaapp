//
//  TripIdResponseModel.swift
//
//  Created by  on 27/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class TripIdResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let routeImageId = "routeImageId"
    static let endLoc = "endLoc"
    static let vehicle = "vehicle"
    static let createdAt = "createdAt"
    static let stops = "stops"
    static let toAddress = "toAddress"
    static let driver = "driver"
    static let startLoc = "startLoc"
    static let cancelledBy = "cancelledBy"
    static let fromAddress = "fromAddress"
    static let auction = "auction"
    static let status = "status"
    static let id = "_id"
    static let estDistance = "estDistance"
    static let pickupByTime = "pickupByTime"
    static let rider = "rider"
    static let updatedAt = "updatedAt"
    static let cancelReason = "cancelReason"
    static let agreedPrice = "agreedPrice"
    static let startTime = "startTime"
    static let estDuration = "estDuration"
  }

  // MARK: Properties
  public var routeImageId: String?
  public var endLoc: EndLocation?
  public var vehicle: Vehicle?
  public var createdAt: String?
  public var stops: [Any]?
  public var toAddress: String?
  public var driver: DriverScheduleTrip?
  public var startLoc: StartLocation?
  public var cancelledBy: String?
  public var fromAddress: String?
  public var auction: String?
  public var status: String?
  public var id: String?
  public var estDistance: Int?
  public var pickupByTime: String?
  public var rider: RiderScheduleTrip?
  public var updatedAt: String?
  public var cancelReason: String?
  public var agreedPrice: Int?
  public var startTime: String?
  public var estDuration: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    routeImageId <- map[SerializationKeys.routeImageId]
    endLoc <- map[SerializationKeys.endLoc]
    vehicle <- map[SerializationKeys.vehicle]
    createdAt <- map[SerializationKeys.createdAt]
    stops <- map[SerializationKeys.stops]
    toAddress <- map[SerializationKeys.toAddress]
    driver <- map[SerializationKeys.driver]
    startLoc <- map[SerializationKeys.startLoc]
    cancelledBy <- map[SerializationKeys.cancelledBy]
    fromAddress <- map[SerializationKeys.fromAddress]
    auction <- map[SerializationKeys.auction]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    estDistance <- map[SerializationKeys.estDistance]
    pickupByTime <- map[SerializationKeys.pickupByTime]
    rider <- map[SerializationKeys.rider]
    updatedAt <- map[SerializationKeys.updatedAt]
    cancelReason <- map[SerializationKeys.cancelReason]
    agreedPrice <- map[SerializationKeys.agreedPrice]
    startTime <- map[SerializationKeys.startTime]
    estDuration <- map[SerializationKeys.estDuration]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = routeImageId { dictionary[SerializationKeys.routeImageId] = value }
    if let value = endLoc { dictionary[SerializationKeys.endLoc] = value.dictionaryRepresentation() }
    if let value = vehicle { dictionary[SerializationKeys.vehicle] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = stops { dictionary[SerializationKeys.stops] = value }
    if let value = toAddress { dictionary[SerializationKeys.toAddress] = value }
    if let value = driver { dictionary[SerializationKeys.driver] = value.dictionaryRepresentation() }
    if let value = startLoc { dictionary[SerializationKeys.startLoc] = value.dictionaryRepresentation() }
    if let value = cancelledBy { dictionary[SerializationKeys.cancelledBy] = value }
    if let value = fromAddress { dictionary[SerializationKeys.fromAddress] = value }
    if let value = auction { dictionary[SerializationKeys.auction] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = estDistance { dictionary[SerializationKeys.estDistance] = value }
    if let value = pickupByTime { dictionary[SerializationKeys.pickupByTime] = value }
    if let value = rider { dictionary[SerializationKeys.rider] = value.dictionaryRepresentation() }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = cancelReason { dictionary[SerializationKeys.cancelReason] = value }
    if let value = agreedPrice { dictionary[SerializationKeys.agreedPrice] = value }
    if let value = startTime { dictionary[SerializationKeys.startTime] = value }
    if let value = estDuration { dictionary[SerializationKeys.estDuration] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.routeImageId = aDecoder.decodeObject(forKey: SerializationKeys.routeImageId) as? String
    self.endLoc = aDecoder.decodeObject(forKey: SerializationKeys.endLoc) as? EndLocation
    self.vehicle = aDecoder.decodeObject(forKey: SerializationKeys.vehicle) as? Vehicle
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.stops = aDecoder.decodeObject(forKey: SerializationKeys.stops) as? [Any]
    self.toAddress = aDecoder.decodeObject(forKey: SerializationKeys.toAddress) as? String
    self.driver = aDecoder.decodeObject(forKey: SerializationKeys.driver) as? DriverScheduleTrip
    self.startLoc = aDecoder.decodeObject(forKey: SerializationKeys.startLoc) as? StartLocation
    self.cancelledBy = aDecoder.decodeObject(forKey: SerializationKeys.cancelledBy) as? String
    self.fromAddress = aDecoder.decodeObject(forKey: SerializationKeys.fromAddress) as? String
    self.auction = aDecoder.decodeObject(forKey: SerializationKeys.auction) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.estDistance = aDecoder.decodeObject(forKey: SerializationKeys.estDistance) as? Int
    self.pickupByTime = aDecoder.decodeObject(forKey: SerializationKeys.pickupByTime) as? String
    self.rider = aDecoder.decodeObject(forKey: SerializationKeys.rider) as? RiderScheduleTrip
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.cancelReason = aDecoder.decodeObject(forKey: SerializationKeys.cancelReason) as? String
    self.agreedPrice = aDecoder.decodeObject(forKey: SerializationKeys.agreedPrice) as? Int
    self.startTime = aDecoder.decodeObject(forKey: SerializationKeys.startTime) as? String
    self.estDuration = aDecoder.decodeObject(forKey: SerializationKeys.estDuration) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(routeImageId, forKey: SerializationKeys.routeImageId)
    aCoder.encode(endLoc, forKey: SerializationKeys.endLoc)
    aCoder.encode(vehicle, forKey: SerializationKeys.vehicle)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(stops, forKey: SerializationKeys.stops)
    aCoder.encode(toAddress, forKey: SerializationKeys.toAddress)
    aCoder.encode(driver, forKey: SerializationKeys.driver)
    aCoder.encode(startLoc, forKey: SerializationKeys.startLoc)
    aCoder.encode(cancelledBy, forKey: SerializationKeys.cancelledBy)
    aCoder.encode(fromAddress, forKey: SerializationKeys.fromAddress)
    aCoder.encode(auction, forKey: SerializationKeys.auction)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(estDistance, forKey: SerializationKeys.estDistance)
    aCoder.encode(pickupByTime, forKey: SerializationKeys.pickupByTime)
    aCoder.encode(rider, forKey: SerializationKeys.rider)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(cancelReason, forKey: SerializationKeys.cancelReason)
    aCoder.encode(agreedPrice, forKey: SerializationKeys.agreedPrice)
    aCoder.encode(startTime, forKey: SerializationKeys.startTime)
    aCoder.encode(estDuration, forKey: SerializationKeys.estDuration)
  }

}
