//
//  DocumentUploadPresenter.swift
//  Wapanda
//


import Foundation
import ObjectMapper

class DocumentUploadPresenter: ResponseCallback{
    
    //MARK:- TaxInformationPresenter local properties
    private weak var documentUploadViewDelegate             : documentUploadViewDelegate?
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?
    private lazy var taxInfoBusinessLogic         : TaxInformationBusinessLogic = TaxInformationBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:TaxInformationScreenViewDelgate,textFieldValidationDelegate: TextFieldValidationDelegate) {
        self.taxInfoViewDelegate = responseDelegate
        self.textFieldValidationDelegate = textFieldValidationDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.taxInfoViewDelegate?.hideLoader()
        self.taxInfoViewDelegate?.taxInfoSubmittedSuccessful(withResponseModel: responseObject as! TaxDoc)
    }
    
    func servicesManagerError(error : ErrorModel){
        self.taxInfoViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.taxInfoViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendTaxInfoSumbitRequest(withData taxScreenRequestModel:TaxInformationScreenRequestModel) -> Void{
        
        
        guard self.validateInput(withData: taxScreenRequestModel) else{
            self.taxInfoViewDelegate?.hideLoader()
            return
        }
        
        self.taxInfoViewDelegate?.showLoader()
        
        var taxRequestModel : TaxInformationRequestModel!
        
        if taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE
        {
            taxRequestModel = TaxInformationRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").setSocialSecurityNumber(taxScreenRequestModel.socialSecurityNumber).setTaxClassification(taxScreenRequestModel.taxClassification).setUserId("59781863cff1905d92b815c6").build()
        }
        else
        {
            taxRequestModel = TaxInformationRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").setTaxId(taxScreenRequestModel.taxId).setTaxClassification(taxScreenRequestModel.taxClassification).setUserId("59781863cff1905d92b815c6").build()
        }
        
    self.taxInfoBusinessLogic.performTaxInformation(withTaxInformationRequestModel: taxRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData taxScreenRequestModel:TaxInformationScreenRequestModel) -> Bool {
        
        //Social Security Number validation
        guard (taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE && !taxScreenRequestModel.socialSecurityNumber.isEmpty) || taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_VALUE else{
            //Social Security Number empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_SECURITY_NUMBER, forTextFields: .Indiviual)
            return false
        }
        
        //Social Security Number validation
        guard taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE && (taxScreenRequestModel.socialSecurityNumber.characters.count == 9) || taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_VALUE else{
            //Social Security Number is invalid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_SECURITY_NUMBER, forTextFields: .Indiviual)
            return false
        }
        
        //Tax ID validation
        guard (taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_VALUE && !taxScreenRequestModel.taxId.isEmpty) || taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE else {
            //Tax ID empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_TAX_ID, forTextFields: .Partnership)
            return false
        }
        
        //Tax ID validation
        guard taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_VALUE && (taxScreenRequestModel.taxId.characters.count == 9) || taxScreenRequestModel.taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE else {
            //Tax ID empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_TAX_ID, forTextFields: .Partnership)
            return false
        }
        
        return true
    }
}
