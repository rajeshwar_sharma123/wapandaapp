//
//  RiderScheduleDetailViewController.swift
//  Wapanda
//
//  Created by Daffodil on 24/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class RiderScheduleDetailViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tripItem: Trip!
    var riderScheduleDetailViewPresenter: RiderScheduleDetailViewPresenter!
    var delegate: BiddingUnderwayViewControllerDelegate!
    var pushNotificationModel: PushNotificationObjectModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.riderScheduleDetailViewPresenter = RiderScheduleDetailViewPresenter(delegate: self)
        intialSetupForView()
    }
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.setupNavigationBar()
        //setData()
    }
    /**
     This method is used to setup navigationBar
     */
    
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, color: UIColor.schedulingThemeColor(),isTranslucent: false)
        self.customizeNavigationBackButton()
    }

    override func errorAlertConfirmButtonClicked() {
        
        self.riderScheduleDetailViewPresenter.sendCancelRideRequest(withRideId: getRiderScheduleDetailModel())
        
    }
    func getRiderScheduleDetailModel() -> RiderScheduleDetailModel{
        var riderScheduleDetailModel = RiderScheduleDetailModel()
        riderScheduleDetailModel.tripId = tripItem.id
        riderScheduleDetailModel.cancelledBy = UserType.Rider.rawValue
        return riderScheduleDetailModel
    }
    
}

extension RiderScheduleDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell: RiderScheduleDetailCell!
        switch indexPath.row {
        case 0:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "DriverDetailCell") as! RiderScheduleDetailCell!
            if let imgId = tripItem.driver?.profileImageFileId{
            cell.driverImgView.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: imgId))!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
            }
            
            if let routeImgId = tripItem.routeImageId{
                cell.mapImgView.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: routeImgId))!, placeholderImage:#imageLiteral(resourceName: "map_placeholder"))
            }
            
            cell.driverName.text = "\(tripItem.driver!.firstName!) \(tripItem.driver!.lastName!)"
            cell.carTypeLabel.text = tripItem.vehicle?.carType?.vehicleType
            cell.driverRatingLabel.text = "\(tripItem.driver!.driverProfile!.averageRating!)"
            cell.plateNumLabel.text = "\(tripItem.vehicle!.carPlateNumber!)"
            cell.totalBillLabel.text =  AppUtility.getFormattedPriceString(withPrice: tripItem.agreedPrice!)
            cell.driverImgView.setCornerCircular(cell.driverImgView.bounds.size.width/2)
            cell.driverImgView.setBorderToView(withWidth: 3, color: UIColor.white)
        case 1:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "SourceDestinationDetailCell") as! RiderScheduleDetailCell!
            cell.sourceLabel.text = tripItem.fromAddress!
            cell.destinationLabel.text = tripItem.toAddress!
        case 2:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "DateDetailCell") as! RiderScheduleDetailCell!
            cell.dateLabel.text = AppUtility.getTripDateFromDateString((tripItem.pickupByTime)!)
        case 3:
            cell = self.tableView.dequeueReusableCell(withIdentifier: "CancelContactCell") as! RiderScheduleDetailCell!
            let cancelTap = UITapGestureRecognizer(target: self, action: #selector(self.cancelTapAction(_:)))
            cell.cancelView.addGestureRecognizer(cancelTap)
            let contactTap = UITapGestureRecognizer(target: self, action: #selector(self.contactTapAction(_:)))
            cell.contactView.addGestureRecognizer(contactTap)
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 307
        case 1:
            return 96
        case 2:
            return 55
        case 3:
            return 72
        default:
            return 0
        }
    }
    
    func cancelTapAction(_ sender: UITapGestureRecognizer) {
        showErrorAlertWithCancelAndConfirmButtons(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.CANCEL_RIDE_CONFIRMATION, VC: self)
    }
    
    func contactTapAction(_ sender: UITapGestureRecognizer) {
        let contactVC = UIViewController.getViewController(viewController: ContactViewController.self)
        contactVC.modalPresentationStyle = .overCurrentContext
        pushNotificationModel = getPushNotificationObjeect()
   
        let name = "\(tripItem.driver!.firstName!) \(tripItem.driver!.lastName!)"
        let phone = tripItem.driver?.phone ?? ""
        var imageId = ""
        if let imgId = tripItem.driver?.profileImageFileId{
            imageId = imgId
        }
        
        contactVC.bindScreen(withName: name, phoneNumber: phone, imgId: imageId, pushModelObject:self.pushNotificationModel)
        self.present(contactVC, animated: true, completion: nil)
    }
    
    func getPushNotificationObjeect()-> PushNotificationObjectModel{
       let pushNotificationModel = PushNotificationObjectModel(JSON:[:])
//        let trip = Trip(JSON:[:])
//        let driver = Driver(JSON:[:])
//
//        let vehicle = Vehicle(JSON:[:])
//        let carType = CarType(JSON:[:])
//        vehicle?.carPlateNumber = tripItem.tripId!.vehicle?.carPlateNumber!
//        vehicle?.id = tripItem.tripId!.vehicle?.id!
//        vehicle?.year = tripItem.tripId!.vehicle?.year!
//        vehicle?.imageId = tripItem.tripId!.vehicle?.imageId!
//        vehicle?.make = tripItem.tripId!.vehicle?.make!
//        vehicle?.model = tripItem.tripId!.vehicle?.model!
//
//        carType?.vehicleType = tripItem.tripId!.vehicle?.carType?.vehicleType
//        vehicle?.carType = carType
//        driver?.firstName = tripItem.tripId!.driver!.firstName!
//        driver?.id = tripItem.tripId!.driver!.id
//        var imageId = ""
//        if let imgId = tripItem.tripId?.driver?.profileImageFileId{
//            imageId = imgId
//        }
//        driver?.profileImageFileId = imageId
//        driver?.driverProfile = tripItem.tripId!.driver!.driverProfile
//        trip?.vehicle = vehicle!
//        trip?.driver = driver
        pushNotificationModel?.trip = tripItem
        return pushNotificationModel!
    }
}


// MARK: - BiddingUnderwayViewDelegate Methods
extension RiderScheduleDetailViewController: RiderScheduleDetailViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
       self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func updateCancelRide(withResponseModel response: CancelResponseModel){
        delegate.updateUpcomingTripList()
        _ = self.navigationController?.popViewController(animated: true)
    }
   
}
