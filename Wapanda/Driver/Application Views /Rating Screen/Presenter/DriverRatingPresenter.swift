//
//  DriverRatingPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps
import Kingfisher
class DriverRatingPresenter: ResponseCallback{
    
    //MARK:- DriverRatingPresenter local properties
    private weak var driverRatingViewDelegate             : DriverRatingScreenViewDelgate?
    private lazy var driverRatingBusinessLogic         : DriverRatingBusinessLogic = DriverRatingBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:DriverRatingScreenViewDelgate) {
        self.driverRatingViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.driverRatingViewDelegate?.hideLoader()
        if responseObject is DriverRatingResponseModel
        {
            self.driverRatingViewDelegate?.didReceiveDriverRatingList(withResponseModel: responseObject as! DriverRatingResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.driverRatingViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.driverRatingViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendDriverRatingRequest(withDriverInfoModel requestModel: DriverRatingScreenRequestModel) -> Void{
        
        self.driverRatingViewDelegate?.showLoader()
        
        var driverRatingRequestModel : DriverRatingRequestModel!
        
            driverRatingRequestModel = DriverRatingRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setDriverId(requestModel.driverId).setSkip(requestModel.skip).setLimit(requestModel.limit).build()
            self.driverRatingBusinessLogic.performGetDriveRatingRequest(withDriverRatingRequestModel: driverRatingRequestModel, presenterDelegate: self)
    }
}
