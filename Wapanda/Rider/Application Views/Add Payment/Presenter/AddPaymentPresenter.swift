//
//  AddPaymentPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class AddPaymentPresenter: ResponseCallback{
    
    //MARK:- AddPaymentPresenter local properties
    private weak var addPaymentViewDelegate             : AddPaymentScreenViewDelgate?
    private lazy var addPaymentBusinessLogic         : AddPaymentBusinessLogic = AddPaymentBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:AddPaymentScreenViewDelgate) {
        self.addPaymentViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.addPaymentViewDelegate?.hideLoader()
        if responseObject is DeleteCardResponseModel {
            self.addPaymentViewDelegate?.cardDeleteSuccessfully(withResponseModel: responseObject as! DeleteCardResponseModel)
        }
        if responseObject is CustomerIdResponseModel {
            self.addPaymentViewDelegate?.customerIdFectchedSuccessfully(withResponseModel: responseObject as! CustomerIdResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.addPaymentViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.addPaymentViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendCardDeleteRequest(withData addPaymentRequestModel:AddPaymentScreenRequestModel) -> Void{
        self.addPaymentViewDelegate?.showLoader()
        
        var addRequestModel : AddPaymentRequestModel!
        addRequestModel = AddPaymentRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json") .addRequestHeader(key: "Authorization", value:"Bearer \(PListUtility.getValue(forKey: AppConstants.PListKeys.STRIPE_SECRET_KEY) as! String)").setCustomerId(addPaymentRequestModel.customerId).setSourceId(addPaymentRequestModel.sourceId)
            .build()
        
    self.addPaymentBusinessLogic.performDeleteSourceRequest(withAddPaymentRequestModel: addRequestModel, presenterDelegate: self)
    }
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendCustomerIdFecthRequest(withData bankScreenRequestModel:AddPaymentScreenRequestModel) -> Void{
        self.addPaymentViewDelegate?.showLoader()
        
        var addRequestModel : AddPaymentRequestModel!
        addRequestModel = AddPaymentRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.addPaymentBusinessLogic.performCustomerIdFetchRequest(withAddPaymentRequestModel: addRequestModel, presenterDelegate: self)
    }
}
