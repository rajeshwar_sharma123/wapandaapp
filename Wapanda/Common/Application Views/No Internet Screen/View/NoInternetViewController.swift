//
//  NoInternetViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/30/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class NoInternetViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openSettingsButtonTapped(_ sender: Any) {
       
            if let url = NSURL(string: "App-Prefs:root=Settings") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
            }
        }
             self.dismiss(animated: true) {}
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true) { 
        }
    }
}
