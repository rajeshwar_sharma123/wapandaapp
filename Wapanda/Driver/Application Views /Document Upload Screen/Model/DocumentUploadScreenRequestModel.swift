//
//  TaxInformationScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import UIKit
struct DocumentUploadScreenRequestModel {
    
    var imageIds                =  [String?](repeating: nil, count: AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD)
    var addedImages             =  [UIImage?](repeating: nil, count: AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD)
    var addedImage              :   UIImage!
    var imageId                   : String!
    var profileImageFileId        : String!
}
