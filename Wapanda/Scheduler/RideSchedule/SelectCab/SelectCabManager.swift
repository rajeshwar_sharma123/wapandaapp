import UIKit

class SelectCabManager {
    
    /**
     This method is used for perform VehicleInfo With Valid Inputs constructed into a VehicleInfoRequestModel
     
     - parameter inputData: Contains info for vehicle type
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performAllVehicleTypeRequest(withCarMakeRequestModel request: VehicleRequestModel, presenterDelegate: ResponseCallback) {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForVehicleInfo()
        VehicleAPIRequest().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForVehicleInfo() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message: AppConstants.ErrorMessages.ADDRESS_NOT_FOUND)

        return errorResolver
    }
    

}
