
//Note :- This class contains GoogleDirectionDetail Buisness Logic

class GoogleDirectionDetailBusinessLogic {
    
    
    deinit {
        print("GoogleDirectionDetailBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a GoogleDirectionDetailRequestModel
     
     - parameter inputData: Contains info for GoogleDirection
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performGoogleDirectionDetail(withGoogleDirectionRequestModel requestModel: GoogleDirectionDetailRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForOTPPopup()
        GoogleDirectionDetailApiRequest().makeAPIRequest(withReqFormData: requestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForOTPPopup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        return errorResolver
    }
}
