
//
//  APNSManager.swift
//  Wapanda
//
//  Created by daffomac-31 on 15/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications
import LGSideMenuController
import JHTAlertController
enum PUSH_EVENTS : String
{
    case EVT_BID_NEW                = "EVT_BID_NEW"
    case EVT_BID_DRIVER_COUNTER     = "EVT_BID_DRIVER_COUNTER"
    case EVT_BID_DRIVER_ACCEPT      = "EVT_BID_DRIVER_ACCEPT"
    case EVT_BID_DRIVER_REJECT      = "EVT_BID_DRIVER_REJECT"
    case EVT_BID_RIDER_ACCEPT       = "EVT_BID_RIDER_ACCEPT"
    case EVT_BID_RIDER_REJECT       = "EVT_BID_RIDER_REJECT"
    case EVT_BID_RIDER_COUNTER      = "EVT_BID_RIDER_COUNTER"
    case EVT_DRIVER_APPROVED        = "EVT_DRIVER_APPROVED"
    case EVT_DRIVER_DISAPPROVED     = "EVT_DRIVER_DISAPPROVED"
    case EVT_USER_ENABLE            = "EVT_USER_ENABLE"
    case EVT_USER_DISABLE           = "EVT_USER_DISABLE"
    case EVT_DRIVER_TRIP_CANCELLED  = "EVT_DRIVER_TRIP_CANCELLED"
    case EVT_RIDER_TRIP_CANCELLED   = "EVT_RIDER_TRIP_CANCELLED"
    case EVT_DRIVER_TRIP_STARTED    = "EVT_DRIVER_TRIP_STARTED"
    case EVT_DRIVER_ARRIVED         = "EVT_DRIVER_ARRIVED"
    case DRIVER_TRIP_ENDED          = "EVT_DRIVER_TRIP_ENDED"
    case RIDER_ADDED_STOP           = "EVT_RIDER_ADDED_STOP"
    case DRIVER_VISITED_STOP        = "EVT_DRIVER_VISITED_STOP"
    case PAYMENT_SUCCESS            = "EVT_PAYMENT_SUCCESS"
}

enum TRIP_STATUS : String
{
    case STARTED                = "STARTED"
    case CONFIRMED              = "CONFIRMED"
    case CANCELLED              = "CANCELLED"
    case ENDED                  = "ENDED"
    case EXPIRED                = "EXPIRED"
}
class APNSManager: NSObject{
    
    static let sharedInstance = APNSManager()
    
    private override init(){}
    
    
    /// This method is used to register app for recieving push notification with settings
    func registerForPushNotification(){
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    /// This method is used to handle notification payload if user launch app from backgroud or from new instance
    ///
    /// - Parameter options: launch options dictionary
    func handleNotificationPayloadOnLaunch(withLaunchOptions options: [UIApplicationLaunchOptionsKey: Any]?){
        if let dict = options?[UIApplicationLaunchOptionsKey.remoteNotification]{
            self.onRecievingPushNotification(withPayload: dict as! NSDictionary)
        }
    }
    
    
    /// This method is called when device is successfully registred with APNS server and server responds with device token for the app
    ///
    /// - Parameter withDeviceToken: Device token in string format
    func onRegistrationSuccess(withDeviceToken deviceToken: String){
        UserDefaultUtility.saveStringWithKey(deviceToken, key: AppConstants.UserDefaultKeys.DEVICE_TOKEN)
    }
    
    
    /// This method is called when device recieve any push notifcation from APNS server
    ///
    /// - Parameter data: data representing payload of recieved notification
    func onRecievingPushNotification(withPayload data: NSDictionary){
        if let objNotification = PushNotificationObjectModel(JSON: data as! [String : Any])
        {
            if let jhtAlertController = UIApplication.shared.visibleViewController as? JHTAlertController
            {
                DispatchQueue.main.async {
                    jhtAlertController.dismiss(animated: true, completion: {
                         self.handleNotificationEvent(objNotification)
                    })
                }
            }
            else
            {
                self.handleNotificationEvent(objNotification)
            }
        }
    }
    
    func handleNotificationEvent(_ objNotification:PushNotificationObjectModel)
    {
        switch objNotification.eventName! {
        case PUSH_EVENTS.EVT_BID_NEW.rawValue:
            self.showNewRideRequestScreen(objNotification)
            break
        case PUSH_EVENTS.EVT_BID_DRIVER_COUNTER.rawValue:
            self.handleDriverCounterEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_BID_DRIVER_ACCEPT.rawValue:
            self.handleDriverAcceptEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_BID_DRIVER_REJECT.rawValue:
            break
        case PUSH_EVENTS.EVT_BID_RIDER_ACCEPT.rawValue:
            self.handleRiderAcceptEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_BID_RIDER_REJECT.rawValue:
            self.handleRiderRejectEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_BID_RIDER_COUNTER.rawValue:
            self.showCounteredPriceRequestScreen(objNotification)
            break
        case PUSH_EVENTS.EVT_DRIVER_APPROVED.rawValue:
            self.handleDriverVerifiedEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_DRIVER_DISAPPROVED.rawValue:
            
            break
        case PUSH_EVENTS.EVT_USER_ENABLE.rawValue:
            
            break
        case PUSH_EVENTS.EVT_USER_DISABLE.rawValue:
            self.handleUserDisabledEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_RIDER_TRIP_CANCELLED.rawValue:
           self.handleRiderCancelTripEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_DRIVER_TRIP_CANCELLED.rawValue:
            self.handleDriverCancelTripEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_DRIVER_TRIP_STARTED.rawValue:
            self.handleDriverStartedTripEvent(objNotification)
            break
        case PUSH_EVENTS.EVT_DRIVER_ARRIVED.rawValue:
            self.handleDriverArrivedEvent(objNotification)
        case PUSH_EVENTS.DRIVER_TRIP_ENDED.rawValue:
            self.handleDriverEndEvent(objNotification)
            break
        case PUSH_EVENTS.RIDER_ADDED_STOP.rawValue:
            self.handleDriverScreenOnRiderAddedStopEvent(objNotification)
        case PUSH_EVENTS.DRIVER_VISITED_STOP.rawValue:
            self.handleRiderScreenOnDriverVisitedStopEvent(objNotification)
        case PUSH_EVENTS.PAYMENT_SUCCESS.rawValue:
            self.handleDriverInvoiceScreen(objNotification)
            break
        default:
            break
        }
    }
    private func handleDriverScreenOnRiderAddedStopEvent(_ objNotification:PushNotificationObjectModel){
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_ADDED_STOP), object: objNotification, userInfo: nil)
//            if let rideVC = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: DriverTripViewController.self) as? DriverTripViewController
//            {
//                rideVC.handleScreenOnRiderAddedStop(withResponseModel: objNotification)
//            }
        }
    }
    private func handleRiderScreenOnDriverVisitedStopEvent(_ objNotification:PushNotificationObjectModel){
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            if let rideVC = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: RiderTripViewController.self) as? RiderTripViewController
            {
                rideVC.handleScreenOnNextStopNotification(withReponseModel: objNotification)
            }
        }
    }
    private func handleUserDisabledEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            if let rideVC = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: BaseViewController.self) as? BaseViewController
            {
                rideVC.showErrorAlertForAccountDissable(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE, VC: rideVC)
            }
        }
        else if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            if let driverVC = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: BaseViewController.self) as? BaseViewController
            {
                driverVC.showErrorAlertForAccountDissable(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE, VC: driverVC)
            }
        }
    }
    private func handleUserEnabledEvent(_ objNotification:PushNotificationObjectModel)
    {
    }
    private func handleDriverVerifiedEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin()
        {
            if let _ = AppDelegate.sharedInstance.userInformation
            {
                AppDelegate.sharedInstance.userInformation.driverProfile?.approved = true
                AppDelegate.sharedInstance.userInformation.saveUser()
            }
            
            if UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
            {
                if let rideVC = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: BaseViewController.self) as? BaseViewController
                {
                    let objNewRequestVC = UIViewController.getViewController(DriverVerifiedViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
                    rideVC.present(objNewRequestVC, animated: true, completion: {})
                }
            }
            else if UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
            {
                if let driverVC = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: BaseViewController.self) as? BaseViewController
                {
                    let objNewRequestVC = UIViewController.getViewController(DriverVerifiedViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
                    driverVC.present(objNewRequestVC, animated: true, completion: {})
                }
            }
        }
    }
    private func handleDriverUnverifiedEvent(_ objNotification:PushNotificationObjectModel)
    {
    }
    private func handleDriverCounterEvent(_ objNotification:PushNotificationObjectModel)
    {
        
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            if let rideVC = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: BaseViewController.self) as? BaseViewController
            {
                if  let topController = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: WaitingViewController.self) as? WaitingViewController
                {
                    
                    topController.setNotificationObject(objNotification)
                    topController.initialiseCounterTimer(30)
                    topController.initialiseViewWithRiderLetsGoEnable()
                }
                else
                {
                    let objNewRequestVC = UIViewController.getViewController(WaitingViewController.self, storyboard: UIStoryboard.Storyboard.Main.object)
                    objNewRequestVC.objNotificationWaiting = objNotification
                    objNewRequestVC.driverBidDelegate = rideVC
                    objNewRequestVC.isDriver = false
                    objNewRequestVC.counterTotalTime = 30
                    rideVC.present(objNewRequestVC, animated: true, completion: nil)
                }
                
            }
        }
    }
    private func handleDriverAcceptEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_ACCEPT_EVENT), object: objNotification, userInfo: nil)
        }
        
    }
    private func handleRiderAcceptEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_ACCEPT_EVENT), object: objNotification, userInfo: nil)
        }
    }
    private func handleRiderRejectEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_REJECT_EVENT), object: objNotification, userInfo: nil)
        }
    }
    private func showNewRideRequestScreen(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            guard let _ = UIApplication.shared.getViewControllerToUpdate(UIApplication.shared.rootViewController!, toUpdate: BaseViewController.self) as? BaseViewController else {
                return
            }
            let objNewRequestVC = UIViewController.getViewController(DriverRequestViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
            objNewRequestVC.objNotification = objNotification
            objNewRequestVC.driverBidDelegate = UIApplication.shared.visibleViewController as! BaseViewController
            UIApplication.shared.visibleViewController?.present(objNewRequestVC, animated: true, completion: nil)
        }
        
    }
    private func showCounteredPriceRequestScreen(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            let objNewRequestVC = UIViewController.getViewController(DriverRequestViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
            objNewRequestVC.objNotification = objNotification
            objNewRequestVC.driverBidDelegate = UIApplication.shared.visibleViewController as! BaseViewController
            UIApplication.shared.visibleViewController?.present(objNewRequestVC, animated: true, completion: nil)
        }
        
    }
    private func handleDriverCancelTripEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_CANCELLED_TRIP), object: objNotification, userInfo: nil)
        }
    }
    private func handleRiderCancelTripEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_CANCELLED_TRIP), object: objNotification, userInfo: nil)
        }
    }
    private func handleDriverStartedTripEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_STARTED_TRIP), object:objNotification, userInfo: nil)
        }
    }
    private func handleDriverArrivedEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_ARRIVED), object:objNotification, userInfo: nil)
        }
    }
    private func handleDriverEndEvent(_ objNotification:PushNotificationObjectModel)
    {
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            if let rideVC = UIApplication.shared.visibleViewController
            {
                if  let topController = (rideVC as? BaseViewController)
                {
                    let tipVC = UIViewController.getViewController(TipViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
                    //Prepare tip screen model
                    let driverName = (objNotification.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")
                    let driverImageId = (objNotification.trip?.driver?.profileImageFileId ?? "")
                    let routeImageId = (objNotification.trip?.routeImageId ?? "")
            
                    let requestModel = TipViewRequestModel(driverName: driverName, driverImageId: driverImageId, tripId: (objNotification.trip?.id), tripFare: Double(objNotification.trip?.payment?.amountPayable ?? 0), routeImageId: routeImageId,pushObjModel:objNotification)
                    tipVC.bind(withViewModel: requestModel)
                    topController.present(tipVC, animated: false, completion: nil)
                }
                else{
                    //Push Rider Trip Screen
                    //                    let objNewRequestVC = UIViewController.getViewController(RiderTripViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
                    //                    objNewRequestVC.pushNotificationModel = objNotification
                    //                    rideVC.navigationController?.pushViewController(objNewRequestVC, animated: true)
                }
            }
        }
    }
    private func handleDriverInvoiceScreen(_ objNotification:PushNotificationObjectModel){
        
        if self.isUserLogin() && UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_DRIVER) && UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_INVOICE_HANDLE), object: objNotification, userInfo: nil)
        }
    }
    private func isUserLogin()->Bool
    {
        guard UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN) else {
            return false
        }
        guard UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN) else {
            return false
        }
        return true
    }}
