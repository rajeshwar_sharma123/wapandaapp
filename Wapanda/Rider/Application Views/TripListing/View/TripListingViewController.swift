//
//  TripListingViewController.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//


enum SegmentType:Int{
    
    case History = 1
    case Upcoming = 0
}



import UIKit
import HMSegmentedControl

class TripListingViewController: BaseViewController{
    
    //MARK:- Properties/Outlets
    
    @IBOutlet weak var segmentedControl: HMSegmentedControl!
    @IBOutlet weak var tableView: UITableView!

    
    var presenterhistory: HistoryViewPresenter!
    var historyListingSource: HistoryResponseModel?
    var segmentType: SegmentType = .Upcoming
    let tripStatusDropDownView = CustomDropDownView.instanceFromNib()

    var upcomingTripListResponse: UpcomingTripListResponseModel!
    //var tripStatus: String = ""
    var refreshControl: UIRefreshControl!
    
    //MARK:- ViewLifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        presenterhistory = HistoryViewPresenter(delegate: self)
        
        initialSetupForUpcomingTrip()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        
        initialSetupForView()

        
        if let historyDataAvailable = self.historyListingSource,(historyDataAvailable.items?.count)! > 0{
            self.historyListingSource = nil
        
        }
        
        presenterhistory.makeHistoryApiRequest(withHistoryViewModel: HistoryScreenViewModel(limit: 10, skip: 0))
        
    }
    
    //MARK:- Helper Methods
    
    /**
     This function setup the Navigation bar
     */
    private func initialSetupForView(){
        setupNavigationBar()
        initSegmentedProperties()
        setupNavigationBar()
        setupTableView()
    }
    
    
    /**
     This function setup the Navigation bar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.RiderTripListing.NAVIGATION_TITLE, color: UIColor.appThemeColor(), isTranslucent: false)
        self.customizeNavigationBackButton()
    }
    
    /**
     Intialise TableView Properties
     */
    private func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerTableViewCell(tableViewCell: HistoryTableViewCell.self)
        tableView.registerTableViewCell(tableViewCell: NoResultTableViewCell.self)
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
    
    
    /**
     This function setup the Navigation bar
     */
    private func initSegmentedProperties(){
        
        segmentedControl.sectionTitles = [AppConstants.ScreenSpecificConstant.RiderTripListing.UPCOMING_TITLE,AppConstants.ScreenSpecificConstant.RiderTripListing.HISTORY_TITLE]
        segmentedControl.selectionStyle = .fullWidthStripe
        segmentedControl.selectionIndicatorColor = UIColor(red: 45/255.0, green: 80.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        
        segmentedControl.selectionIndicatorHeight = 2.0
        segmentedControl.selectionIndicatorLocation = .down
        segmentedControl.backgroundColor = UIColor.appDarkThemeColor()
        
        segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName:UIFont.getSanFranciscoSemibold( withSize: 14.0)]
        segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName:UIFont.getSanFranciscoRegular( withSize: 14.0)]
        segmentType = SegmentType(rawValue: segmentedControl.selectedSegmentIndex)!
        if segmentType == .History{
            segmentedControl.selectionIndicatorColor = UIColor.white
            
        }else{
            segmentedControl.selectionIndicatorColor = UIColor(red: 45/255.0, green: 80.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        }
        segmentedControl.addTarget(self, action: #selector(TripListingViewController.segmentedControlValueChanged(segment:)), for: .valueChanged)
    }
    
    
    // Action Method called on selecting a segment
    func segmentedControlValueChanged(segment:HMSegmentedControl){
        
        segmentType = SegmentType(rawValue: segment.selectedSegmentIndex)!
        if segmentType == .History{
            segmentedControl.selectionIndicatorColor = UIColor.white
            
        }else{
            segmentedControl.selectionIndicatorColor = UIColor(red: 45/255.0, green: 80.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        }
        tableView.reloadData()
        //setHeaderView()
        addPullToRefresh()
    }
    
    
}



//MARK:-  HistoryViewDelegate Methods Handling

extension TripListingViewController: HistoryViewDelegate{
    
    func historyListingSuccess(withResponseModel data: HistoryResponseModel) {
        
        //tableView.refreshControl?.endRefreshing()
        
        
        if let listing = self.historyListingSource , listing.hasNext == true
        {
            
            let lastIndex = (self.historyListingSource?.items?.count)! // First Index of newly added items
            
            self.historyListingSource?.items?.append(contentsOf:data.items!)
            self.historyListingSource?.hasNext = data.hasNext
            
            //Index path of newly added items
            var rowsToAdd = [IndexPath]()
            for indexRow in lastIndex ..< (self.historyListingSource?.items?.count)!
            {
                rowsToAdd.append(IndexPath(row: indexRow, section: 0))
            }
            
            //Insert new added rows
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: rowsToAdd, with: .none)
            self.tableView.endUpdates()
        }else{
            self.historyListingSource = data
            tableView.reloadData()
        }
        
        
    }
    
    
    func updateUpcomingTripList(withResponseModel upcomingTripList:UpcomingTripListResponseModel){
        self.updateUpcomingTripListInExtensionClass(withResponseModel: upcomingTripList)
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        self.refreshControl.endRefreshing()
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}



//MARK:- Tableview Methods

extension TripListingViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch segmentType {
            
        case .History:
            if let data = self.historyListingSource?.items, data.count > 0{
                return data.count
            }
            
            return 1
            
        case .Upcoming:
            
            if let data = self.upcomingTripListResponse?.items, data.count > 0{
                return data.count
            }
            
            return 1
            
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch segmentType {
            
        case .Upcoming:
            if let data = self.upcomingTripListResponse?.items, data.count > 0{
                let cell = tableView.getCell(withCellType: UpComingTripCell.self)
                cell.bindDataWithCell(upcomingTripListResponse: self.upcomingTripListResponse, indexPath: indexPath)
                return cell
            }else{
                let cell = tableView.getCell(withCellType: NoResultTableViewCell.self)
                cell.bind(title: AppConstants.ScreenSpecificConstant.RiderTripListing.No_Schedule_Trip)
                cell.selectionStyle = .none
                return cell
            }
            
        case .History:
            
            if let data = self.historyListingSource?.items, data.count > 0{
                
                let cell = tableView.getCell(withCellType: HistoryTableViewCell.self)
                cell.selectionStyle = .none
                cell.bindData(withResponse: historyListingSource!,withIndexPath: indexPath)
                
                return cell
                
            }else{
                //No trip Found
                let cell = tableView.getCell(withCellType: NoResultTableViewCell.self)
                cell.bind(title: AppConstants.ScreenSpecificConstant.RiderTripListing.No_Trips)
                cell.selectionStyle = .none
                
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if segmentType == .History{
            if let historyListing = self.historyListingSource?.items, historyListing.count-1 == indexPath.row,(self.historyListingSource?.hasNext!)!{
                
                self.presenterhistory.makeHistoryApiRequest(withHistoryViewModel: HistoryScreenViewModel(limit: 10, skip: self.historyListingSource?.items?.count))
                
            }
        }else{
            
            if let tripListing = self.upcomingTripListResponse?.items, tripListing.count-1 == indexPath.row,(self.upcomingTripListResponse?.hasNext!)!{
                
                self.presenterhistory.sendUpcomingTripList(withData: createUpcomingTripListModel((self.upcomingTripListResponse?.items?.count)!,status: ""))
            }
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentType == .History{
        if let data = self.historyListingSource?.items, data.count > 0{
            if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
            {
                let tripDetail = self.historyListingSource?.items?[indexPath.row]
                let tripDetailVC = UIViewController.getViewController(DriverInvoiceViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
                tripDetailVC.invoiceDataModel = DriverInvoiceViewModel(routeImageId: tripDetail?.routeImageId ?? "", toAddress: tripDetail?.toAddress ?? "", fromAddress: tripDetail?.fromAddress ?? "", status: tripDetail?.status ?? "", id: tripDetail?.id ?? "", agreedPrice: tripDetail?.agreedPrice ?? 0.0, distanceTravelled: tripDetail?.distanceTravelled ?? 0, endTime: tripDetail?.endTime ?? "", startTime: tripDetail?.startTime ?? "", duration: tripDetail?.duration ?? 0, stopAddress: ((tripDetail?.stops?.count)! > 0) ?  (tripDetail?.stops![0].address!)! : "",payment: tripDetail?.payment)
                UIApplication.shared.visibleViewController?.navigationController?.pushViewController(tripDetailVC, animated: true)
            }
            else
            {
                let tripDetailVC = UIViewController.getViewController(TripDetailViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
                tripDetailVC.tripDetailSource = self.historyListingSource?.items?[indexPath.row]
                UIApplication.shared.visibleViewController?.navigationController?.pushViewController(tripDetailVC, animated: true)
            }
        }else{
            //Code for Trip selection
            //handelUpcomingTripCellAction(indexPath: indexPath)
            }
        }else{
         handelUpcomingTripCellAction(indexPath: indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch segmentType {
            
        case .History:
            if let data = self.historyListingSource?.items, data.count > 0{
                return self.tableView.frame.height/2
            }else{
                return tableView.frame.size.height
            }
            
        case .Upcoming:
            if let data = self.upcomingTripListResponse?.items, data.count > 0{
                return 150
            }else{
                return tableView.frame.size.height
            }
            
        }
        
        return 0
    }
}







