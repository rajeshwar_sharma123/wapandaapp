//
//  VehicleTypeRequestModel.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/24/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class VehicleTypeRequestModel{
    
    //MARK:- VehicleInfoRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        /**
         This method is used for setting TaxClassification
         
         - parameter taxClassification: String parameter that is going to be set on TaxClassification
         
         - returns: returning Builder Object
         */
        func setSkip(_ skip:Int) -> Builder{
            requestBody["skip"] = skip as AnyObject?
            return self
        }
        
        /**
         This method is used for setting SocialSecurityNumber
         
         - parameter socialSecurityNumber: String parameter that is going to be set on SocialSecurityNumber
         
         - returns: returning Builder Object
         */
        func setLimit(_ limit:Int)->Builder{
            requestBody["limit"] = limit as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Tax Id
         
         - parameter taxId: String parameter that is going to be set on Tax Id
         
         - returns: returning Builder Object
         */
        func setSort(_ sort:String)->Builder{
            requestBody["sort"] = sort as AnyObject?
            return self
        }
       
        /**
         This method is used for setting Tax Id
         
         - parameter taxId: String parameter that is going to be set on Tax Id
         
         - returns: returning Builder Object
         */
        func setOrder(_ order:Int)->Builder{
            requestBody["order"] = order as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->VehicleTypeRequestModel{
            return VehicleTypeRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting login end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.ALL_VEHICLE_TYPES)
    }
    
}
