import UIKit

class SearchListRequestModel {
    //Note :- Property Name must be same as key used in request API
    var requestHeader: [String:AnyObject]!
    var skip: Int?
    var limit: Int?

    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestHeader = builder.requestHeader
        skip = builder.skip
        limit = builder.limit
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var skip: Int?
        var limit: Int?

        /**
         This method is used for setting dest Locations
         
         - parameter destLoc: String parameter that is going to be set on destination locations
         
         - returns: returning Builder Object
         */
        func setLimit(_ limit: Int) -> Builder{
            self.limit = limit
            return self
        }
        
        
        /**
         This method is used for setting skip
         
         - parameter skip: Int parameter that indicates how many records we want to skip from starting
         
         - returns: returning Builder Object
         */
        func setSkip(_ skip: Int) -> Builder{
            
            self.skip = skip
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of BidsListRequestModel
         and provide BidsListRequestModel object.
         
         -returns : VisitedStopRequestModel
         */
        func build() -> SearchListRequestModel{
            return SearchListRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting VisitedStop end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return "/all/auctions/criteria?skip=\(skip)&limit=\(limit)"
    }
}
