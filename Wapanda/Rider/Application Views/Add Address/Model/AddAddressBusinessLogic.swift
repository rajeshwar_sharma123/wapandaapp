
//Note :- This class contains AddAddress Buisness Logic

class AddAddressBusinessLogic {
    
    
    deinit {
        print("AddAddressBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a AddAddressRequestModel
     
     - parameter inputData: Contains info for AddAddress
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performAddAddress(withAddAddressRequestModel signUpRequestModel: AddAddressRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver.registerErrorForSignup()
        AddAddressApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)

        return errorResolver
    }
}
