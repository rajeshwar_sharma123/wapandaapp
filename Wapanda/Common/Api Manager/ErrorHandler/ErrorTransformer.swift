
//Notes:- This class will be used as error transformers

import Foundation

class ErrorTransformer {
    
    class func getErrorModel(fromErrorObject errorObject:Any,errorResponse: NSError ,errorResolver:ErrorResolver)->ErrorModel {
        
        let errorModel = ErrorModel()
        if let errorDict = errorObject as? Dictionary<String,Any> {
            errorModel.setErrorMessage(errorResolver.getErrorObjectForCode(errorDict[AppConstants.ErrorHandlingKeys.ERROR_KEY] as! String))
            errorModel.setErrorTitle(AppConstants.ErrorMessages.ERROR_TITLE)
            errorModel.setErrorPayloadInfo(errorDict)
        }else{
            
                if ErrorTransformer.errorMessageForUnknownError(errorResponse.code) != ""{
                    errorModel.setErrorMessage(self.errorMessageForUnknownError(errorResponse.code))
                }
                else{
                 
                    errorModel.setErrorMessage(errorResponse.debugDescription)
              //    errorModel.setErrorMessage(AppConstants.ErrorMessages.SOME_ERROR_OCCURED)

                }
            
            errorModel.setErrorTitle(AppConstants.ErrorMessages.ERROR_TITLE)
        }
        return errorModel
    }
    class func errorMessageForUnknownError(_ errorResponseCode : Int)->String
    {
        switch errorResponseCode {
        case -1001:
            return AppConstants.ErrorMessages.REQUEST_TIME_OUT
        case -1005: fallthrough
        case -1009:
            return AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION
        default:
            return ""
            
        }
    
    }
}
