
//Notes:- This class is used as presenter for AddStopViewPresenter

import Foundation
import ObjectMapper

class AddStopViewPresenter: ResponseCallback{
    
//MARK:- AddStopViewPresenter local properties
    
    private weak var addStopViewDelegate          : AddStopViewDelegate?
    private lazy var addStopBusinessLogic         : AddStopBusinessLogic = AddStopBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:AddStopViewDelegate){
        self.addStopViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let data = responseObject as? Trip{
            self.addStopViewDelegate?.addStopSuccessfully(withTripModel: data)
        }
        
        self.addStopViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error: ErrorModel){
        self.addStopViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.addStopViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call Add Stop Api.
    
    func sendAddStopRequest(withAddStopViewRequestModel addStopViewRequestModel:AddStopViewRequestModel){
        
        self.addStopViewDelegate?.showLoader()
        
        let requestModel = AddStopRequestModel.Builder()
                            .setLat(addStopViewRequestModel.lat)
                            .setLng(addStopViewRequestModel.lng)
                            .setTripId(addStopViewRequestModel.tripId).setAddress(addStopViewRequestModel.address)
                            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                            .build()
        
        self.addStopBusinessLogic.performAddStop(withAddStopRequestModel: requestModel, presenterDelegate: self)
    }
}
