//
//  DriverTripScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DriverTripScreenRequestModel {
    
    var latFrom   : Double!
    var lngFrom   : Double!
    var startTime : String!
    var tripId : String!
}
