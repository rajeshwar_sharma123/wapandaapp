//
//  RatingImageSectionHeaderView.swift
//  Wapanda
//
//  Created by Daffolapmac on 16/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class RatingImageSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var labelAverageRating: UILabel!
    
    @IBOutlet weak var labelCancellationPercent: UILabel!
    @IBOutlet weak var labelAcceptancePrecent: UILabel!
    @IBOutlet weak var labelCancellationStatus: UILabel!
    
    @IBOutlet weak var viewBottomBorder: UIView!
    @IBOutlet weak var lblRatingView: UILabel!
    @IBOutlet weak var labelAcceptanceStatus: UILabel!


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
