
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct CabLocationViewRequestModel {
    
    var lat  : Double!
    var lng  : Double!
}
