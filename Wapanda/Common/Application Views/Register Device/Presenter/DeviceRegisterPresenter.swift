//
//  DeviceRegisterScreenPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class DeviceRegisterScreenPresenter: ResponseCallback{
    
    //MARK:- DeviceRegisterScreenPresenter local properties
    private weak var deviceRegisterViewDelegate             : DeviceRegisterDelegate?
    private var deviceRegisterBusinessLogic               =  DeviceRegisterBusinessLogic()

    //MARK:- Constructor
    init(delegate deviceRegisterDelegate: DeviceRegisterDelegate) {
        self.deviceRegisterViewDelegate = deviceRegisterDelegate
    }
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        
    }
    
    func servicesManagerError(error : ErrorModel){
      
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send deviceRegister request to business layer with valid Request model
     - returns : Void
     */
    func sendDeviceRegisterRequest(withData deviceRegisterViewRequestModel:DeviceRegisterScreenRequestModel) -> Void{
        var deviceRegisterRequestModel : DeviceRegisterRequestModel!

            deviceRegisterRequestModel = DeviceRegisterRequestModel.Builder()
                .setType(deviceRegisterViewRequestModel.type).setPushId(deviceRegisterViewRequestModel.pushId).addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).build()
        
        self.deviceRegisterBusinessLogic.performDeviceRegister(withDeviceRegisterRequestModel: deviceRegisterRequestModel, presenterDelegate: self)
    }
    func sendDeviceDeregisterRequest() -> Void{
        var deviceRegisterRequestModel : DeviceRegisterRequestModel!
        if AppUtility.isUserLogin()
        {
        deviceRegisterRequestModel = DeviceRegisterRequestModel.Builder()
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).build()
        self.deviceRegisterBusinessLogic.performDeviceDeRegister(withDeviceRegisterRequestModel: deviceRegisterRequestModel, presenterDelegate: self)
        }
    }
    
    func sendDriverOfflineRequest(){
        if let authToken = UserDefaults.standard.string(forKey: AppConstants.APIRequestHeaders.AUTH_TOKEN)
        {
        
        let driverHomeRequestModel = DriverHomeRequestModel.Builder()
            .setAvailable(false)
            .setDriverId(AppDelegate.sharedInstance.userInformation.id!)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: authToken)
            .build()
        
        self.deviceRegisterBusinessLogic.performDriverOfflineRequest(withDriverHomeRequestModel: driverHomeRequestModel, presenterDelegate: self)
        }
    }
    
}
