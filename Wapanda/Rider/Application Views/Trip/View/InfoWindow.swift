

import UIKit

class InfoWindow: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func bind(withTitle title: String){
        self.lblTitle.text = title
    }
}
