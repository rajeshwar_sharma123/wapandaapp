//
//  TaxDoc.swift
//
//  Created by  on 8/30/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class TaxDoc:  NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "_id"
    static let createdAt = "createdAt"
    static let taxClassification = "taxClassification"
    static let approved = "approved"
    static let taxId = "taxId"
    static let updatedAt = "updatedAt"
    static let socialSecurityNumber = "socialSecurityNumber"
  }

  // MARK: Properties
  public var id: String?
  public var createdAt: String?
  public var taxClassification: String?
  public var approved: Bool? = false
  public var taxId: String?
  public var updatedAt: String?
  public var socialSecurityNumber: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    taxClassification <- map[SerializationKeys.taxClassification]
    approved <- map[SerializationKeys.approved]
    taxId <- map[SerializationKeys.taxId]
    updatedAt <- map[SerializationKeys.updatedAt]
    socialSecurityNumber <- map[SerializationKeys.socialSecurityNumber]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = taxClassification { dictionary[SerializationKeys.taxClassification] = value }
    dictionary[SerializationKeys.approved] = approved
    if let value = taxId { dictionary[SerializationKeys.taxId] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = socialSecurityNumber { dictionary[SerializationKeys.socialSecurityNumber] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.taxClassification = aDecoder.decodeObject(forKey: SerializationKeys.taxClassification) as? String
    self.approved = aDecoder.decodeObject(forKey: SerializationKeys.approved) as? Bool
    self.taxId = aDecoder.decodeObject(forKey: SerializationKeys.taxId) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.socialSecurityNumber = aDecoder.decodeObject(forKey: SerializationKeys.socialSecurityNumber) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(taxClassification, forKey: SerializationKeys.taxClassification)
    aCoder.encode(approved, forKey: SerializationKeys.approved)
    aCoder.encode(taxId, forKey: SerializationKeys.taxId)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(socialSecurityNumber, forKey: SerializationKeys.socialSecurityNumber)
  }

}
