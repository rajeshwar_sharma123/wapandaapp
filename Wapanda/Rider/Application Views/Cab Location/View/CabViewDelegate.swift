//
//  CabLocationViewDelegate.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/18/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
protocol CabLocationViewDelegate: BaseViewProtocol {
    func addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D, markerImage image: UIImage)->GMSMarker
    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker,bearing:Double)->GMSMarker
    func removeMarkerAtPosition(marker: GMSMarker)->GMSMarker
    func updateCabLocationList(_ cabLocationList:[Cabs])
    func updateMarkerList(_ markersList:[GMSMarker])
    func fetchNearByCabListSuccessful(_ cabLocationList:CabLocationsResponseModel)
    func didReceiveLaunchDetails(withLaunchRequestModel:LaunchResponseModel)

}
