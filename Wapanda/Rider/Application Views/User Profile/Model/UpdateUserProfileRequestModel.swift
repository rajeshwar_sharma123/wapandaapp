

//Notes:- This class is used for constructing UpdateUserProfile Service Request Model

class UpdateUserProfileRequestModel {
    
    //MARK:- UpdateUserProfileRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var userId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.userId = builder.userId
        
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var userId: String!
        
//        /**
//         This method is used for setting Profile image id 
//         
//         - parameter userName: String parameter that is going to be set on Profile image id 
//         
//         - returns: returning Builder Object
//         */
//        func setProfileImageId(_ id:String?) -> Builder{
//            guard let _ = id else{return self}
//            
//            requestBody["profileImageFileId"] = id as AnyObject?
//            return self
//        }
//        
//        /**
//         This method is used for setting First Name
//         
//         - parameter userName: String parameter that is going to be set on First Name
//         
//         - returns: returning Builder Object
//         */
//        func setFirstName(_ firstName:String?) -> Builder{
//            guard let _ = firstName else{return self}
//
//            requestBody["firstName"] = firstName as AnyObject?
//            return self
//        }
//        
//        /**
//         This method is used for setting Last Name
//         
//         - parameter userName: String parameter that is going to be set on Last Name
//         
//         - returns: returning Builder Object
//         */
//        func setLastName(_ lastName:String?) -> Builder{
//            guard let _ = lastName else{return self}
//            
//            requestBody["lastName"] = lastName as AnyObject?
//            return self
//        }
//        
//        /**
//         This method is used for setting Addresses
//         
//         - parameter userName: String parameter that is going to be set on Addresses
//         
//         - returns: returning Builder Object
//         */
//        func setAddresses(_ addresses: [Dictionary<String, Any>]?) -> Builder{
//            guard let _ = addresses else {return self}
//            
//            requestBody["addresses"] = addresses as AnyObject?
//            return self
//        }
        
        
        /**
         This method is used for setting Addresses
         
         - parameter userName: String parameter that is going to be set on Addresses
         
         - returns: returning Builder Object
         */
        func setRequestBody(_ body: UpdateUserProfileViewRequestModel) -> Builder{
            
            requestBody = body.dictionaryRepresentation() as [String : AnyObject]
            
            return self
        }
        
        /**
         This method is used for setting Addresses
         
         - parameter userName: String parameter that is going to be set on Addresses
         
         - returns: returning Builder Object
         */
        func setUserId(_ userId: String) -> Builder{
            self.userId = userId
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of UpdateUserProfileRequestModel
         and provide UpdateUserProfileForm1ViewViewRequestModel object.
         
         -returns : UpdateUserProfileRequestModel
         */
        func build()->UpdateUserProfileRequestModel{
            return UpdateUserProfileRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting UpdateUserProfile end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.UPDATE_USER_PROFILE, self.userId)
    }
    
}
