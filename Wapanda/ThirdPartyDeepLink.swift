import UIKit

class ThirdPartyDeepLink {
    
    func navigateToUberApp() {
        
        guard let sourceDestination = UserRideDetail.shared.sourceDestination else { return }
        
        let sourceLat = sourceDestination.source.lat
        let sourceLng = sourceDestination.source.long
        let destLat = sourceDestination.destination.lat
        let destLng = sourceDestination.destination.long
        guard let sourceAddress = sourceDestination.source.address.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
        guard let destAddress = sourceDestination.destination.address.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
        
        
        if uberInstalled() {
            open(scheme: "uber://?client_id=BFpwewfzzerYdiMp0Q-nujEzB6yTynpf&action=setPickup&pickup[latitude]=\(sourceLat)&pickup[longitude]=\(sourceLng)&pickup[formatted_address]=\(sourceAddress)&dropoff[latitude]=\(destLat)&dropoff[longitude]=\(destLng)&dropoff[formatted_address]=\(destAddress)&link_text=View%20team%20roster&partner_deeplink=partner%3A%2F%2Fteam%2F9383")
        } else {
            open(scheme: "https://m.uber.com/ul/?action=setPickup&pickup[latitude]=\(sourceLat)&pickup[longitude]=\(sourceLng)&pickup[formatted_address]=\(sourceAddress)&dropoff[latitude]=\(destLat)&dropoff[longitude]=\(destLng)&dropoff[formatted_address]=\(destAddress)&link_text=View%20team%20roster&partner_deeplink=partner%3A%2F%2Fteam%2F9383")
        }
        
    }
    
    func navigateToLyftApp() {
        
        guard let sourceDestination = UserRideDetail.shared.sourceDestination else { return }
            
        
        let sourceLat = sourceDestination.source.lat
        let sourceLng = sourceDestination.source.long
        let destLat = sourceDestination.destination.lat
        let destLng = sourceDestination.destination.long

            
        if lyftInstalled() {

            open(scheme: "lyft://id=lyft&partner=alhbZclMpYFM&pickup[latitude]=\(sourceLat)&pickup[longitude]=\(sourceLng)&destination[latitude]=\(destLat)&destination[longitude]=\(destLng)")
        } else {
            open(scheme: "https://www.lyft.com/signup/SDKSIGNUP?clientId=alhbZclMpYFM&sdkName=iOS_direct")
        }
    }
    

    private func uberInstalled() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: "uber://")!)
    }
    
    private func lyftInstalled() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: "lyft://")!)
    }
    
    private func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
