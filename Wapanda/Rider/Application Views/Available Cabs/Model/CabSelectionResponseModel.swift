//
//  CabSelectionResponseModel.swift
//  CabSelection
//
//  Created by daffomac-31 on 24/07/17.
//  Copyright © 2017 CabSelection. All rights reserved.
//

import Foundation
import ObjectMapper

class CabSelectionResponseModel:Mappable, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let rideList = "cabs"
        static let thirdpartyestimators = "thirdpartyestimators"
    }
    
    // MARK: Properties
    public var rideList: [RideList]?
    public var thirdpartyestimatorsList: [Thirdpartyestimators]?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        rideList <- map[SerializationKeys.rideList]
        thirdpartyestimatorsList <- map[SerializationKeys.thirdpartyestimators]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = rideList { dictionary[SerializationKeys.rideList] = value.map { $0.dictionaryRepresentation() } }
        if let value = thirdpartyestimatorsList { dictionary[SerializationKeys.thirdpartyestimators] = value.map { $0.dictionaryRepresentation() } }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.rideList = aDecoder.decodeObject(forKey: SerializationKeys.rideList) as? [RideList]
        self.thirdpartyestimatorsList = aDecoder.decodeObject(forKey: SerializationKeys.thirdpartyestimators) as? [Thirdpartyestimators]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(rideList, forKey: SerializationKeys.rideList)
        aCoder.encode(thirdpartyestimatorsList, forKey: SerializationKeys.thirdpartyestimators)
    }

}
