//
//  DriverScheduleDetailRequestModel.swift
//  Wapanda
//
//  Created by MacBook on 12/7/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DriverScheduleDetailRequestModel{
    
    //MARK:- DriverScheduleDetailRequestModel properties
    
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var tripId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.tripId = builder.tripId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        //MARK:- Builder class Properties
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var tripId: String!
        
        /**
         This method is used for setting tripId
         
         - parameter tripId: String parameter that is going to be set on tripId
         
         - returns: returning Builder Object
         */
        func setTripId(_ tripId: String)->Builder{
            self.tripId = tripId
            return self
        }

        func setLatitude(_ latitude: Double?)->Builder{
            if let _ = latitude{
               requestBody["latFrom"] = latitude as AnyObject?
            }
            
            return self
        }
        
        
        func setLongitude(_ longitude: Double?)->Builder{
            if let _ = longitude{
             requestBody["lngFrom"] = longitude as AnyObject?
            }
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of CancelRiderScheduleRideRequestModel
         and provide CancelRiderScheduleRideRequestModel object.
         
         -returns : CancelRiderScheduleRideRequestModel
         */
        func build()->DriverScheduleDetailRequestModel{
            return DriverScheduleDetailRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting end point of Drive Start PickUp for Scheduled ride
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.CONFIRM_PICK_UP, self.tripId)
    }
    
}
