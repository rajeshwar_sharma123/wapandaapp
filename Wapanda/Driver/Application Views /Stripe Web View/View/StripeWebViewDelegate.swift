//
//  StripeWebViewDelegate.swift
//  Wapanda
//
//  Created by Daffolapmac on 27/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
protocol StripeWebViewDelegate: BaseViewProtocol {
    func didCompleteStripeConnection(withResponseModel stripeConnectRespone:StripeConnectionResponseModel)
}

