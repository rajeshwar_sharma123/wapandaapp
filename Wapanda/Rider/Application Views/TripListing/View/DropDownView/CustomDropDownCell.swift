//
//  TripStatusDropDownCell.swift
//  DropDownSwift
//
//  Created by Daffolapmac-33 on 18/11/17.
//  Copyright © 2017 Daffolapmac-33. All rights reserved.
//

import UIKit

class CustomDropDownCell: UITableViewCell {

    @IBOutlet weak var radioImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
