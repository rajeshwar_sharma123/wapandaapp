
//Notes:- This protocol is used as a interface which is used by VisitedStopPresenter to tranfer info to VisitedStopViewController

protocol VisitedStopViewDelegate:BaseViewProtocol {
    func driverVisitedStopSuccessfully(withResponseModel: Trip)
}
