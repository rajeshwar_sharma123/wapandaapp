import UIKit
import MapKit

class BidsListViewController: BaseViewController, SearchBidViewDelegate, SavedSearchDelegate {

    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var lblNoBidsFound: UILabel!
    @IBOutlet weak var upDownArrowImage: UIImageView!
    @IBOutlet weak var lblTitleAllBids: UILabel!
    @IBOutlet weak var btnOnlineOffline: UIButton!
    @IBOutlet weak var bidsTableView: UITableView!
    @IBOutlet weak var btnSearchForm: UIButton!
    @IBOutlet weak var searchBidView: SearchBidView!
    @IBOutlet weak var sortByLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var userRequest = BidRequestUserInput()
    
    var SORT_KEYS = [["riderSelectedFare": -1], ["closeBy": 1], ["estDistance": 1], ["estDuration": 1]]
    
    var bids = [AuctionItemModel]()
    var refreshControl =  UIRefreshControl()
    var presenter: BidsListPresenter!
    let dropDownView = CustomDropDownView.instanceFromNib()
    
    
    //Driver Data used to update availability status
    
    var driverObject: LoginResponseModel?
    var driverLocation:CLLocationCoordinate2D?
    var driverBasePrice: Float?
    var sortBytArray: [String] = ["Fare", "Expiration Time", "Distance", "Drive Time"]
    var selectedStatus: Int = 0
    var isRefreshEnabled = false

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        
        initializeUserLocation()
        customizeNavigationBackButton()
        registerCells()
        presenter = BidsListPresenter(delegate: self)
        //getBidsList(skip: 0, showLoader: true)
        addPullToRefreshControl()
        searchBidView.delegate = self
        toggleSearchView()
        setupAvailbilityView()
        setDropDownView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        customizeNavigationBarWithTitle(navigationTitle: "", color: UIColor.appDarkGrayColor() , isTranslucent: false)
        getBidsList(skip: 0, showLoader: true)
    }
    
    func initializeUserLocation() {
        
        guard let location = driverLocation else {
        AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderSelectCab.NO_CAB_AVAILABLE_MESSAGE)
            return
        }

        userRequest.latitude = Double(location.latitude)
        userRequest.longitude = Double(location.longitude)
    }

    func addPullToRefreshControl(){
        refreshControl.tintColor = UIColor.appThemeColor()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            bidsTableView.refreshControl = refreshControl
        } else {
            bidsTableView.backgroundView = refreshControl
        }
    }
    
    
    func refresh(_ refreshControl: UIRefreshControl) {
        isRefreshEnabled = true
        getBidsList(skip: 0, showLoader: false)
    }
    
    func endRefresh(){
        self.refreshControl.endRefreshing()
        self.isRefreshEnabled = false
    }
    
    func getBidsList(skip: Int, showLoader: Bool) {
        
        let sortOrderDict = SORT_KEYS[selectedStatus]
        
        userRequest.sortOrder = sortOrderDict.values.first!
        userRequest.sortKey = sortOrderDict.keys.first!
        
        if userRequest.latitude == nil && userRequest.longitude == nil {
            initializeUserLocation()
        }
        
        presenter.getBidsList(with: userRequest, skip: skip, showLoader: showLoader)
    }
    
    @IBAction func onlineOfflineClicked(_ sender: UIButton) {
        
        if AppUtility.isUserLogin() {
            
            guard let available = driverObject?.driverProfile?.available else { return }
            guard let location = driverLocation else { return }
            if available {
                presenter.updateDriverAvailabilitySataus(isAvailable: false, driverLocation: location)
            }
            else {
                presenter.updateDriverAvailabilitySataus(isAvailable: true, driverLocation: location)
            }
        }
        else {
            self.logOutUser()
        }
    }
    
    
    @IBAction func showSearchFormClicked(_ sender: UIButton) {

        toggleSearchView()
    }
    
    func searchBid(with parameters: BidRequestUserInput) {
        userRequest = parameters
        toggleSearchView()
        getBidsList(skip: 0, showLoader: true)
        lblTitleAllBids.text = "Custom"
    }
    
    func saveSearch(with parameters: BidRequestUserInput) {
        
        
        var serachParam = SaveSearchUserInput()
        
        if let dateString = parameters.pickupTimeUTC {
            serachParam.pickupDateTime = dateString
        }
        if let address = parameters.addressTitle {
            serachParam.destinationAddress = address
        }
        if let latitude = parameters.latitude, let longitude = parameters.longitude {
            serachParam.destLoc = [Float(latitude), Float(longitude)]
        }
        if let fare = parameters.fare {
            serachParam.fare = fare
        }
        showAlertForSaveSearchName(input: serachParam)
    }
    
    func showAlertForSaveSearchName(input: SaveSearchUserInput) {
        
        var userInput = input
        
        let alert = UIAlertController(title: "", message: "Do you want to name your search?", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "Enter Name"
        })
        
        let cancelAction = UIAlertAction(title: "No, Thanks", style: .default, handler: { [weak self] (action) -> Void in
            self?.presenter.saveSearch(with: userInput)
        })
        
        alert.addAction(cancelAction)
        
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak self] (action) -> Void in
            if let searchName = alert.textFields![0].text {
                userInput.title = searchName
            }
            self?.presenter.saveSearch(with: userInput)
            
        }))
        
        
        present(alert, animated: true, completion: nil)
    }
    
    func fareClicked(delegate: PickupFareViewControllerDelegate) {
        let pickupFareVC = UIViewController.getViewController(PickupFareViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        pickupFareVC.delegate = delegate
        navigationController?.pushViewController(pickupFareVC, animated: true)
    }
    
    func locationClicked(delegate: GooglePlaceSearchDelegate) {
        let vcSearch = UIViewController.getViewController(GooglePlaceSearchViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        vcSearch.modalPresentationStyle = .overCurrentContext
        vcSearch.delegate = delegate
        self.present(vcSearch, animated: true, completion: nil)
    }

    
    func pickUpTimeClicked(delegate: PickupTimeViewControllerDelegate) {
        let pickupTimeVC = UIViewController.getViewController(PickupTimeViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        pickupTimeVC.delegate = delegate
        navigationController?.pushViewController(pickupTimeVC, animated: true)
    }
    
    func searchSaved() {
//     searchBidView.isHidden = true
        toggleSearchView()
    }
    
    
    @IBAction func sortByViewTapAction(_ sender: UITapGestureRecognizer) {
        
        setDropDownStatus()
        dropDownView.setDropDown(self.view, belowView: (sender.view?.superview)!, itemList: sortBytArray, selectedItem: selectedStatus)
        dropDownView.toggle()
    }
    
    
    
    func driverStatusUpdatedSuccessful(reponse: LoginResponseModel) {
        
        driverObject?.driverProfile?.available = reponse.driverProfile?.available
        driverObject?.canRide = reponse.canRide
        driverObject?.canDrive = reponse.canDrive
        driverObject?.driverProfile?.ongoingRate = reponse.driverProfile?.ongoingRate
        driverObject?.driverProfile?.ratio = reponse.driverProfile?.ratio        
        AppDelegate.sharedInstance.userInformation = driverObject
        AppDelegate.sharedInstance.userInformation.saveUser()
        setupAvailbilityView()
        self.hideLoader()
    }
    
    func setupAvailbilityView() {
        
        guard let driverAvailabile = driverObject?.driverProfile?.available else { return }
        btnOnlineOffline.addShadow()
        
        if AppUtility.isUserLogin()
        {
            
            if driverAvailabile {
                btnOnlineOffline.backgroundColor = UIColor.appThemeColor()
                btnOnlineOffline.setTitleColor(UIColor.white, for: .normal)
                
                btnOnlineOffline.setTitle(AppConstants.ScreenSpecificConstant.DriverHomeScreen.OFFLINE_TITLE, for: .normal)
            }
            else {
                btnOnlineOffline.backgroundColor = UIColor.white
                btnOnlineOffline.setTitleColor(UIColor.black, for: .normal)
                btnOnlineOffline.setTitle(AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE, for: .normal)
            }
        }
        else {
            btnOnlineOffline.backgroundColor = UIColor.white
            btnOnlineOffline.setTitleColor(UIColor.black, for: .normal)
            btnOnlineOffline.setTitle(AppConstants.ScreenSpecificConstant.DriverHomeScreen.ONLINE_TITLE, for: .normal)
        }
    }
    
    @IBAction func clearClicked(_ sender: Any) {
        userRequest.latitude = nil
        userRequest.longitude = nil
        userRequest.pickupTimeUTC = nil
        userRequest.pickupTime = nil
        userRequest.fare = nil
        userRequest.addressTitle = nil
        searchBidView.bindView(userInput: userRequest)
        toggleSearchView()
        lblTitleAllBids.text = "All Bids"
        getBidsList(skip: 0, showLoader: true)
    }
    
    func toggleSearchView() {
        
        searchBidView.isHidden = !searchBidView.isHidden
        
        if !dropDownView.isHidden{
            setDropDownStatus()
        }
        
        if searchBidView.isHidden {
            upDownArrowImage.image = #imageLiteral(resourceName: "ic_downArrow")
            hideClear()
        }
        else{
            searchBidView.bindView(userInput: userRequest)
            upDownArrowImage.image = #imageLiteral(resourceName: "ic_upArrow")
        }
    }
    
    func showClear() {
        btnClear.isHidden = false
    }
    
    func hideClear() {
        btnClear.isHidden = true
    }
    
    @IBAction func savedSearchClicked(_ sender: UIButton) {
        
        let vc = UIViewController.getViewController(SearchListViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func savedSearchesClicked(params: BidRequestUserInput) {
        userRequest = params
        getBidsList(skip: 0, showLoader: true)
        lblTitleAllBids.text = "Custom"
    }
}


extension BidsListViewController: BidsListViewDelegate {
    func showLoader() {
        if !self.isRefreshEnabled{
            super.showLoader(self)
        }
    }
    
    func hideLoader() {
        hideLoader(self)
    }
    
    
    func refreshList(list: [AuctionItemModel]) {
        bids = list
        endRefresh()
        bidsTableView.reloadData()
        
        lblNoBidsFound.isHidden = bids.count != 0
        
        guard let bidsResopnse = presenter.bidsList else { return }
        
        if bidsResopnse.hasNext && bids.count != 0 {
            bidsTableView.tableFooterView = Bundle.main.loadNibNamed("PaginationFooter", owner: self, options: nil)![0] as? UIView
        }
        else{
            self.bidsTableView.tableFooterView = nil
        }
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        endRefresh()
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    
}

// This extension contain drop drown related functionality
extension BidsListViewController{
    /**
     This function setup the Drop down view
     */
    func setDropDownView(){
        dropDownView.delegate = self
        dropDownView.setRowColorAndHeightWithRadioImage(UIColor.appDarkGrayColor(), selectedColor: UIColor.appDarkGrayColor(), height: 56, radioNormalImage: "icn_radio_gray", radioSelectedImage: "icn_radio_gray_selected")
        sortByLabel.text = sortBytArray[selectedStatus]
    }
    
    func setDropDownStatus(){
        
        dropDownView.isHidden = !dropDownView.isHidden
        if dropDownView.isHidden {
            arrowImageView.image = #imageLiteral(resourceName: "ic_downArrow")
        }
        else{
            arrowImageView.image = #imageLiteral(resourceName: "ic_upArrow")
        }
    }
}

extension BidsListViewController: DropDownViewDelegate{
    
    func updateSelectedItem(selectedItem: Int) {
        selectedStatus = selectedItem
        sortByLabel.text = sortBytArray[selectedItem]
        setDropDownStatus()
        getBidsList(skip: 0, showLoader: true)
    }
}

