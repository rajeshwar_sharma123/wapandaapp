//
//  RiderProfile.swift
//
//  Created by  on 8/30/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RiderProfile: NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updatedAt"
    static let rideCount = "rideCount"
    static let id = "_id"
    static let addresses = "addresses"
    static let createdAt = "createdAt"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var rideCount: Int?
  public var id: String?
  public var addresses: Addresses?
  public var createdAt: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    rideCount <- map[SerializationKeys.rideCount]
    id <- map[SerializationKeys.id]
    addresses <- map[SerializationKeys.addresses]
    createdAt <- map[SerializationKeys.createdAt]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = rideCount { dictionary[SerializationKeys.rideCount] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = addresses { dictionary[SerializationKeys.addresses] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.rideCount = aDecoder.decodeObject(forKey: SerializationKeys.rideCount) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.addresses = aDecoder.decodeObject(forKey: SerializationKeys.addresses) as? Addresses
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(rideCount, forKey: SerializationKeys.rideCount)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(addresses, forKey: SerializationKeys.addresses)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
  }

}
