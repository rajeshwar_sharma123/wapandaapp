
//Notes:- This protocol is used as a interface which is used by UpdateUserPresenter to tranfer info to UpdateUserViewController

protocol UpdateUserViewDelegate:BaseViewProtocol {
    func updatedAgreeTermsAndConditionStatusSuccessfully()
}
