

import Foundation
import ObjectMapper

class DriverInfoResponseModel:Mappable {
    
    private struct SerializationKeys {
        static let lastName = "lastName"
        static let firstName = "firstName"
        static let id = "_id"
        static let cabInfo = "cabInfo"
        static let profileImageFileId = "profileImageFileId"
        static let driverProfile = "driverProfile"
    }
    
    var firstName       : String = ""
    var lastName       : String = ""
    var id       : String = ""
    var cabInfo       : Cab?
    var profileImageFileId       : String = ""
    var driverProfile       : DriverProfile?
    
    required internal init?(map: Map) {
        mapping(map: map)
    }
    
    init() {
        
    }
    
    public func mapping(map: Map) {
        firstName         <- map[SerializationKeys.firstName]
        lastName         <- map[SerializationKeys.lastName]
        id         <- map[SerializationKeys.id]
        cabInfo         <- map[SerializationKeys.cabInfo]
        profileImageFileId         <- map[SerializationKeys.profileImageFileId]
        driverProfile         <- map[SerializationKeys.driverProfile]
    }
}
