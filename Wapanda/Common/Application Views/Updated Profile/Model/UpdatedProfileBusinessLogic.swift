
//Note :- This class contains UpdatedProfile Buisness Logic

class UpdatedProfileBusinessLogic {
    
    
    deinit {
        print("UpdatedProfileBusinessLogic deinit")
    }

    /**
     */
    
    func performUpdatedProfile(withLoginRequestModel termsReviewRequestModel: UpdatedProfileRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForUpdatedProfile()
        UpdatedProfileApiRequest().makeAPIRequest(withReqFormData: termsReviewRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForUpdatedProfile() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_AUTH, message  : AppConstants.ErrorMessages.INVALID_AUTH)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        return errorResolver
    }
}
