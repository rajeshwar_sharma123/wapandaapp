//
//  CustomerIdResponseModel.swift
//
//  Created by Daffolapmac on 30/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class CustomerIdResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stripeCustomerId = "stripeCustomerId"
  }

  // MARK: Properties
  public var stripeCustomerId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    stripeCustomerId <- map[SerializationKeys.stripeCustomerId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stripeCustomerId { dictionary[SerializationKeys.stripeCustomerId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.stripeCustomerId = aDecoder.decodeObject(forKey: SerializationKeys.stripeCustomerId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(stripeCustomerId, forKey: SerializationKeys.stripeCustomerId)
  }

}
