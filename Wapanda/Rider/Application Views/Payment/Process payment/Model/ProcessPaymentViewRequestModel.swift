
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct ProcessPaymentViewRequestModel {
    
    var paymentId                   : String!
    var sourceId                    : String!
}
