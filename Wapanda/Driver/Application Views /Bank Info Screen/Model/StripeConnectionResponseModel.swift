//
//  StripeConnectionResponseModel.swift
//
//  Created by  on 27/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class StripeConnectionResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorDescription = "error_description"
    static let scope = "scope"
    static let code = "code"
    static let error = "error"
  }

  // MARK: Properties
  public var errorDescription: String?
  public var scope: String?
  public var code: String?
  public var error: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    errorDescription <- map[SerializationKeys.errorDescription]
    scope <- map[SerializationKeys.scope]
    code <- map[SerializationKeys.code]
    error <- map[SerializationKeys.error]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorDescription { dictionary[SerializationKeys.errorDescription] = value }
    if let value = scope { dictionary[SerializationKeys.scope] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = error { dictionary[SerializationKeys.error] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorDescription = aDecoder.decodeObject(forKey: SerializationKeys.errorDescription) as? String
    self.scope = aDecoder.decodeObject(forKey: SerializationKeys.scope) as? String
    self.code = aDecoder.decodeObject(forKey: SerializationKeys.code) as? String
    self.error = aDecoder.decodeObject(forKey: SerializationKeys.error) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorDescription, forKey: SerializationKeys.errorDescription)
    aCoder.encode(scope, forKey: SerializationKeys.scope)
    aCoder.encode(code, forKey: SerializationKeys.code)
    aCoder.encode(error, forKey: SerializationKeys.error)
  }

}
