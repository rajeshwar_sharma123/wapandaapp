//
//  AddPaymentScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import OAuthSwift
import Stripe
import AFNetworking
import JHTAlertController

protocol AddPaymentScreenProtocol: class{
    func addPaymentScreenDidDismiss()
}

class AddPaymentScreenViewController: BaseViewController {
    
    @IBOutlet weak var labelNoCardsMessage: UILabel!
    @IBOutlet weak var tableViewCards: UITableView!
    @IBOutlet weak var labelDefault: UILabel!
    @IBOutlet weak var defaultBorderView: UIView!
    
    //MARK : Local Variables
    var isContextRefreshed = false
    var presenterAddPayment : AddPaymentPresenter!
    var cardsList = [STPSource]()
    var customerContext : STPCustomerContext!
    var customer : STPCustomer!
    var deletedIndex = -1
    var isComingFromSideMenu = false
    
    //MARK : Add Payment Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setupNavigationBar()
        
        self.getAddedCardsList()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.presenterAddPayment = AddPaymentPresenter(delegate: self)
        self.setupTableView()
    }
    
    /**
     This method is used to setup initial Bank Info View
     */
    private func setupTableView(){
        self.tableViewCards.delegate = self
        self.tableViewCards.dataSource = self
        self.tableViewCards.tableFooterView = UIView()
    }
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.AddPaymentScreen.NAVIGATION_TITLE)
        self.customizeNavigationBackButton()

    }
    
    
   override func rightButtonClick() ->Void {
    
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.REFRESH_PAYMENT_OPTION), object: NSNumber.init(booleanLiteral: self.isContextRefreshed), userInfo: nil)
    
    let viewControllers: [UIViewController]? = self.navigationController?.viewControllers
    
    if ( self != viewControllers![0]) && !(viewControllers![0] is PaymentMethodViewController)
    {
        
        for aViewController in viewControllers!.reversed() where (!(aViewController is PaymentMethodViewController) && !(aViewController is AddCreditCardScreenViewController) && !(aViewController is AddPaymentScreenViewController)){
            self.navigationController!.popToViewController(aViewController, animated: true)
            return
        }
    }
    else
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
 }
    
    
   private func setUpCustomerContext()
    {
      
        if let _ = LoginResponseModel.retriveUser()?.stripeProfile?.stripeCustomerId
        {
            self.getAddedCardsList()
        }
        else
        {
            self.presenterAddPayment.sendCustomerIdFecthRequest(withData: AddPaymentScreenRequestModel())
        }
    }
    fileprivate func getAddedCardsList()
    {
        self.showLoader()
        self.customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        
//        let paymentContext = STPPaymentContext(customerContext: self.customerContext)
//        paymentContext.delegate = self
//        paymentContext.hostViewController = self
//        print(paymentContext.paymentMethods)
//        print(paymentContext.selectedPaymentMethod)
        
        self.customerContext.retrieveCustomer { (customer, error) in
            self.hideLoader()
            if let _ = customer
            {
                StripePaymentManager.shared.customer = customer
                if let defaultSource = StripePaymentManager.shared.customer?.defaultSource as? STPSource,defaultSource.type == .card
                {
                    StripePaymentManager.shared.isCardAdded = true
                }
                else
                {
                    StripePaymentManager.shared.isCardAdded = false
                }
                
                self.customer = customer
                self.cardsList = customer?.sources.filter({ (source) -> Bool in
                    if let sourceCard = source as? STPSource, sourceCard.type == .card { return true }
                    else{
                        return false
                    }
                }) as! [STPSource]
                
                if !self.isComingFromSideMenu && self.cardsList.count > 0{
                    self.addNavigationRightButtonWithTitle(title: "Continue")
                }
                self.initialseCardListView()
                self.tableViewCards.reloadData()
            }
            else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: (error?.localizedDescription)!)
            }
        }
    }
    fileprivate func initialseCardListView()
    {
        if self.cardsList.count == 0
        {
            self.labelNoCardsMessage.isHidden = false
            self.tableViewCards.isHidden = true
            self.labelDefault.isHidden = true
            self.defaultBorderView.isHidden = true
            
        }
        else {
            self.labelNoCardsMessage.isHidden = true
            self.tableViewCards.isHidden = false
            self.labelDefault.isHidden = false
            self.defaultBorderView.isHidden = false
        }
    
    }
    fileprivate func deleteCard(onRow row:Int)
    {
        let alertController = JHTAlertController(title: AppConstants.ErrorMessages.ALERT_TITLE , message:
            AppConstants.ScreenSpecificConstant.AddPaymentScreen.DELETE_CONFIRM_MESSAGE, preferredStyle: .alert)
        
        alertController.alertBackgroundColor = .white
        alertController.titleViewBackgroundColor = .white
        
        alertController.messageTextColor = .black
        alertController.titleTextColor = .black
        
        alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
        alertController.hasRoundedCorners = true
        
        alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
        alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
        
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.NO_TITLE, style: .cancel, handler: nil))
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.YES_TITLE, style: .default, handler: { (alertAction) in
            self.deletedIndex = row
            self.presenterAddPayment.sendCardDeleteRequest(withData: AddPaymentScreenRequestModel(customerId: AppDelegate.sharedInstance.userInformation.stripeProfile?.stripeCustomerId, sourceId: self.cardsList[row].stripeID))
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    //MARK : IBOutlest Action Methods
    @IBAction func addNewCardButtonTapped(_ sender: Any) {
        let paymentMethodVC = UIViewController.getViewController(PaymentMethodViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        paymentMethodVC.isComingFromSideMenu = self.isComingFromSideMenu
        self.navigationController?.pushViewController(paymentMethodVC, animated: true)
    }
    
    @IBAction func setDefaultCard(_ sender: Any)
    {
        self.setDefaultToCardAtIndex(index: (sender as! UIButton).tag)
    }
    
    func setDefaultToCardAtIndex(index: Int){
        guard index != 0 else{return}
        
        let alertController = JHTAlertController(title: AppConstants.ErrorMessages.ALERT_TITLE , message:
            AppConstants.ScreenSpecificConstant.AddPaymentScreen.DEFAULT_CARD_CONFIRM_MESSAGE, preferredStyle: .alert)
        
        alertController.alertBackgroundColor = .white
        alertController.titleViewBackgroundColor = .white
        
        alertController.messageTextColor = .black
        alertController.titleTextColor = .black
        
        alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
        alertController.hasRoundedCorners = true
        
        alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
        alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
        
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.NO_TITLE, style: .cancel, handler: nil))
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.YES_TITLE, style: .default, handler: { (alertAction) in
            self.showLoader()
            self.customerContext.selectDefaultCustomerSource(self.cardsList[index], completion: { (error) in
                if let _ = error
                {}
                else{
                    self.isContextRefreshed = true
                    StripePaymentManager.shared.refreshCustomerContext()
                    let defaultCard =  self.cardsList.remove(at: index)
                    self.cardsList.insert(defaultCard, at: 0)
                    self.tableViewCards.reloadData()
                }
                self.hideLoader()
            })
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    override func backButtonClick() {
        
     NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.REFRESH_PAYMENT_OPTION), object: NSNumber.init(booleanLiteral: self.isContextRefreshed), userInfo: nil)
     
        let viewControllers: [UIViewController]? = self.navigationController?.viewControllers
     
        if ( self != viewControllers![0]) && !(viewControllers![0] is PaymentMethodViewController)
        {
            
            for aViewController in viewControllers!.reversed() where (!(aViewController is PaymentMethodViewController) && !(aViewController is AddCreditCardScreenViewController) && !(aViewController is AddPaymentScreenViewController)){
                    self.navigationController!.popToViewController(aViewController, animated: true)
                    return
            }
        }
        else
        {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    func navigateToTermsReviewController(){
        
    }
    
}

extension AddPaymentScreenViewController:AddPaymentScreenViewDelgate
{
    func cardDeleteSuccessfully(withResponseModel stripeResponseModel: DeleteCardResponseModel) {
        AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.AddPaymentScreen.CARD_DELETED_SUCCESS_MESSAGE)
        
        // Remove from the local card list
        self.cardsList.remove(at: self.deletedIndex)
        self.initialseCardListView()
        self.tableViewCards.reloadData()
        
        guard self.cardsList.count != 0 else {
            self.isContextRefreshed = true
            StripePaymentManager.shared.refreshCustomerContext()
            return
        }
        
        //Set the card at zeroth index as default
        self.customerContext.selectDefaultCustomerSource(self.cardsList[0], completion: { (error) in
            if let _ = error
            {}
            else{
                
            }
        })
        self.isContextRefreshed = true
        StripePaymentManager.shared.refreshCustomerContext()
        // self.getAddedCardsList()
    }
    func customerIdFectchedSuccessfully(withResponseModel stripeResponseModel:CustomerIdResponseModel)
    {
        AppDelegate.sharedInstance.userInformation.stripeProfile?.stripeCustomerId = stripeResponseModel.stripeCustomerId
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        self.getAddedCardsList()
    }
    
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
//MARK: UITable View Delegate & DataSource
extension AddPaymentScreenViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardsList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.getCell(withCellType: CreditCardTableViewCell.self)
        cell.labelCardNumber.text = "XXXX-XXXX-XXXX-\(((self.cardsList[indexPath.row].cardDetails?.last4)!))"
        cell.labelValidThrough.text = "\(((self.cardsList[indexPath.row].cardDetails?.expMonth)!))/\((self.cardsList[indexPath.row].cardDetails?.expYear)!)"
        cell.imageViewBrand.image = StripePaymentManager.shared.getBrandImage(basedOnBrandType: (self.cardsList[indexPath.row].cardDetails?.brand)!)
       // if let defaultCard = self.customer.defaultSource as? STPCard,defaultCard.stripeID == self.cardsList[indexPath.row].stripeID
        
        //As Stripe always provides default card at zeroth index
        if indexPath.row == 0
        {
            cell.buttonDefault.setImage(#imageLiteral(resourceName: "ic_radio_on"), for: .normal)
        }
        else
        {
            cell.buttonDefault.setImage(#imageLiteral(resourceName: "ic_radio_off"), for: .normal)
        }
        cell.buttonDefault.tag = indexPath.row
        cell.buttonDefault.addTarget(self, action:#selector(setDefaultCard(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .default, title: "          ", handler: { (action, indexPath) in
            self.deleteCard(onRow: indexPath.row)
        })
        deleteAction.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "ic_swipe_delete"))
        
        return [deleteAction]
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0
        {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.setDefaultToCardAtIndex(index: indexPath.row)
    }
}


//extension AddPaymentScreenViewController: STPPaymentContextDelegate{
//
//    // MARK: - STPPaymentContextDelegate
//
//    // Called when the payment context's contents change, such as when the user selects a new payment method or enters shipping info:
//    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
//        print("PaymentViewController paymentContextDidChange")
//
//        print(paymentContext.paymentMethods)
//        print(paymentContext.selectedPaymentMethod)
//
//        let card = paymentContext.paymentMethods?.last as! STPCard
//        print(card.stripeID)
//
//        self.customerContext.selectDefaultCustomerSource(paymentContext.paymentMethods?.last as! STPSourceProtocol) { (error) in
//            if let error = error{
//                print(error.localizedDescription)
//            }
//        }
//        paymentContext.requestPayment()
//    }
//
//    // Called when the user has successfully selected a payment method and completed their purchase; client should pass the contents of the paymentResult object to backend, which should then finish charging the user using the stripe.charges.create API:
//    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping (STPErrorBlock)) {
//        print("PaymentViewController paymentContext didCreatePaymentResult")
//    }
//
//    // Called after the previous method, when any auxiliary UI that has been displayed (such as the Apple Pay dialog) has been dismissed; client should inspect the returned status and show an appropriate message to the user:
//    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
//        print("PaymentViewController paymentContext didFinishWith")
//    }
//
//    // Called in the rare case that the payment context's initial loading call fails, usually due to lack of internet connectivity; client should dismiss the checkout page when this occurs and/or invite the user to try again:
//    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
//        print("Payment didFailToLoadWithError")
//        print(error)
//    }
//
//    // MARK: PKPaymentAuthorizationViewControllerDelegate
//
//    // When using Apple Pay, tells the delegate that the user has authorized the payment request (when using Credit Card, similar to paymentContext:didCreatePaymentResult:completion:); client should pass the contents of the paymentResult object to backend, which should then finish charging the user using the stripe.charges.create API:
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
//        print("paymentAuthorizationViewController didAuthorizePayment payment")
//
//        STPAPIClient.shared().createToken(with: payment) { (token, error) in
//
//            guard let token = token else {
//                print(error!)
//                return
//            }
//
////            Customers().attachSource(toCustomer: token) {_ in
////
////                Charges.post(coin: self.item.coin, source: token.card!.stripeID) {
////
////                    completion(.success)
////
////                }
////
////            }
//
//        }
//
//    }
//
//    // When using Apple Pay, tells the delegate that payment authorization has completed:
//    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
//        print("paymentAuthorizationViewControllerDidFinish")
//        self.dismiss(animated: true, completion: nil)
//    }
//}





