//
//  WaitingViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 21/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class WaitingViewController: BaseViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var imageViewPanda: UIImageView!
    @IBOutlet weak var lblTopMessage: UILabel!
    var timerDriver: SRCountdownTimer!
    @IBOutlet weak var pandaImageBackgroundView: UIView!
    @IBOutlet weak var timerBackgroundView: UIView!
    @IBOutlet weak var lblPreviousPrice: UILabel!
    @IBOutlet weak var btnLetsGo: CustomButton!
    @IBOutlet weak var labelCounterValue: UILabel!
    @IBOutlet weak var lblRidePrice: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var viewPreviousPrice: UIBorderView!
    @IBOutlet weak var lblCounteredPrice: UILabel!
    var waitingPresenter : WaitingPresenter!
    var driverBidDelegate : DriverBidViewDelegate!
    var isDriver = true
    var  counterTotalTime : TimeInterval = 30
    var objNotificationWaiting : PushNotificationObjectModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.waitingPresenter = WaitingPresenter(delegate: self)
        //Add notification if app comes from background to foreground
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(riderRejectNotify(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_REJECT_EVENT), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(riderAcceptNotify(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.RIDER_ACCEPT_EVENT), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(driverAcceptNotify(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_ACCEPT_EVENT), object: nil)
        

    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        if let _ = timerDriver
//        {
//            self.timerDriver.end()
//            self.timerDriver.removeFromSuperview()
//             self.timerDriver = nil
//        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.initialiseCounterTimer(counterTotalTime)
        self.initialiseRiderImageView()
        self.initialSetUpView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc private func appDidBecomeActive()
    {if AppDelegate.sharedInstance.isActivatedFromBackground
    {
        self.initialiseCounterTimer(counterTotalTime)
        self.initialiseRiderImageView()
        }
    }
    
    private func initialSetUpView()
    {
        if !isDriver
        {
            if objNotificationWaiting.eventName == PUSH_EVENTS.EVT_BID_DRIVER_COUNTER.rawValue
            {
                self.initialiseViewWithRiderLetsGoEnable()
            }
            else
            {
                self.initialiseViewWithRiderLetsGoDisable()
            }
        }
        else
        {
             self.initialiseViewWithDriver()
        }
    }
    func driverAcceptNotify(_ notification:Notification)
    {
        let objNotification = notification.object as! PushNotificationObjectModel
        self.dismiss(animated: true, completion: {
            self.driverBidDelegate.driverDidAcceptedRide(objNotification)
        })
    }
    func riderAcceptNotify(_ notification:Notification)
    {
        let objNotification = notification.object as! PushNotificationObjectModel
        self.dismiss(animated: true, completion: {
            self.driverBidDelegate.riderDidAcceptBid(objNotification)
        })
    }
    func riderRejectNotify(_ notification:Notification)
    {
        let objNotification = notification.object as! PushNotificationObjectModel
        self.setNotificationObject(objNotification)
        self.dismiss(animated: true, completion: {
            self.driverBidDelegate.riderDidRejectBid(objNotification)
        })
    }
    private func initialiseRiderImageView()
    {
//        self.pandaImageBackgroundView.clipsToBounds = true
//        self.pandaImageBackgroundView.layer.cornerRadius = self.pandaImageBackgroundView.frame.height/2
    }
    func setNotificationObject(_ objNotification:PushNotificationObjectModel)
    {
        self.objNotificationWaiting = objNotification
    }
    func initialiseCounterTimer(_ counterTotalTime:TimeInterval)
    {
        if let _ = timerDriver
        {
//            self.timerDriver.end()
//            self.timerDriver.removeFromSuperview()
//            self.timerDriver = nil
        }
        else
        {
        self.timerDriver = SRCountdownTimer(frame: CGRect(x: 0, y: 0, width: self.timerBackgroundView.frame.width, height: self.timerBackgroundView.frame.width))
        }
        self.timerDriver.lineWidth = 4.0
        self.timerDriver.backgroundColor = UIColor.clear
        self.timerDriver.lineColor = .black
        self.timerDriver.trailLineColor = UIColor.clear
        self.timerBackgroundView.addSubview(self.timerDriver)
        self.timerDriver.delegate = self
        let closeByTime = (self.objNotificationWaiting.biddingDoc?.closeBy)!.getDateFromZoneFormate()
        self.timerDriver.elapsedTime = counterTotalTime - closeByTime.timeIntervalSinceNow
        if self.timerDriver.elapsedTime < 0
        {
            self.timerDriver.elapsedTime = 0.0
        }
        self.timerDriver.start(beginingValue: Int(counterTotalTime))
    }
    
     func initialiseViewWithRiderLetsGoDisable()
    {
       
         if (self.objNotificationWaiting.biddingDoc?.riderCountered!)!
        {
             self.viewPreviousPrice.isHidden = false
            self.lblCounteredPrice.text = "Your Price"
            self.lblRidePrice.text = String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.riderCounterPrice ?? 00.00)!)
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.estPrice ?? 00.00)!))
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            self.lblPreviousPrice.attributedText = attributeString

        }
        else
        {
            self.viewPreviousPrice.isHidden = true
               self.lblCounteredPrice.text = "Your Price"
            self.lblRidePrice.text = String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.estPrice ?? 00.00)!)
        }
       
        self.lblTopMessage.text = "Waiting for Driver to respond!"
       
        self.btnLetsGo.isHidden = true
        self.cancelButton.isHidden = true
    }
    
     func initialiseViewWithRiderLetsGoEnable()
    {
        self.viewPreviousPrice.isHidden = false
        self.lblTopMessage.text = "Driver countered new price for your ride."
        self.lblCounteredPrice.text = "Countered Price"
        self.lblRidePrice.text = String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.driverCounterPrice ?? 00.00))
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.riderCounterPrice ?? 00.00)))
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
        self.lblPreviousPrice.attributedText = attributeString
        self.btnLetsGo.isHidden = false
        self.cancelButton.isHidden = false
    }
    
     func initialiseViewWithDriver()
    {
        
        self.lblTopMessage.text = "Waiting for Rider to respond"
        self.lblCounteredPrice.text = "Your Price"
        if (self.objNotificationWaiting.biddingDoc?.driverCountered)!
        {
            self.viewPreviousPrice.isHidden = false
            self.lblRidePrice.text = String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.driverCounterPrice ?? 00.00))
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.riderCounterPrice ?? 00.00)))
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            self.lblPreviousPrice.attributedText = attributeString
        }
        else
        {
            self.viewPreviousPrice.isHidden = true
            self.lblRidePrice.text = String(format:"$%0.2f",(self.objNotificationWaiting.biddingDoc?.estPrice ?? 00.00))
        }
        
        
        self.btnLetsGo.isHidden = true
        self.cancelButton.isHidden = true
    }
    
    private func getEstimateDurationFormat(_ estimateSec:Int)->String
    {
        let (hours,min) = self.secondsToHoursMinutesSeconds(seconds: estimateSec/1000)
        
        if hours != 0
        {
            return ("\(hours) hrs, \(min) min")
        }
        else
        {
            return ("\(min) min")
        }
        
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60)
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.waitingPresenter.sendRejectRideRequest(withBiddingId: (objNotificationWaiting.biddingDoc?.id)!)
    }
    
    @IBAction func letsGoTapped(_ sender: Any) {
        self.waitingPresenter.sendAcceptRideRequest(withBiddingId: (objNotificationWaiting.biddingDoc?.id)!)
    }
}
extension WaitingViewController:SRCountdownTimerDelegate
{
    func timerDidUpdateCounterValue(newValue: Int){
        self.labelCounterValue.text="\(newValue)"
        //DispatchQueue.main.async(execute: {
            self.timerDriver.lineColor = UIColor(red: CGFloat(((Int(self.counterTotalTime)-newValue)*255)/Int(self.counterTotalTime))/255.0, green: 0, blue: 0, alpha: 1)
       // })
    }
    
    func timerDidStart(){}
    func timerDidPause(){}
    func timerDidResume(){}
    func timerDidEnd(){
            self.dismiss(animated: true, completion: {
                if self.isDriver
                {
                    AppUtility.presentToastWithMessage("Rider seems to be unavailable at this time")
                }
                else
                {
                    AppUtility.presentToastWithMessage("Driver seems to be unavailable at this time")
                }
            })
    }
}
extension WaitingViewController:WaitingScreenViewDelgate
{
    func didAcceptRidePrice(withResponseModel: NewRideResponseModel)
    {
        if withResponseModel.success!
        {
            self.dismiss(animated: true, completion: {
                let objPush = PushNotificationObjectModel(JSON: [:])
                objPush?.trip = withResponseModel.trip
                self.driverBidDelegate.riderDidAcceptBid(objPush!)
            })
        }
    
    }
    
    func didRejectRidePrice(withResponseModel:RiderRejectResponseModel)
    {
            self.dismiss(animated: true, completion: {
                AppUtility.presentToastWithMessage("Ride Rejected")
            })
    
    }
    func showLoader()
    {
        self.showLoader(self)
    }
    
    func hideLoader()
    {
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }

    
}
