//
//  ResetPasswordViewDelegate.swift
//  Wapanda
//
//  Created by Daffodil on 21/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation


//Notes:- This protocol is used as a delegate for Text Field validation

protocol ResetPasswordViewDelegate:BaseViewProtocol {
    func passwordChangedSuccessfully(withData response: ResetPasswordResponseModel) -> Void
    //func showToastWithMessage(_ message:String) -> Void
}
