import UIKit

class SearchListManager {
    
    /**
     This method is used for perform request for all the saved searched for user
     
     - parameter inputData: Contains info for SearchListRequestModel
     
     */
    func getSavedSearches(with request: SearchListRequestModel, presenterDelegate: ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = registerErrorForSearchList()
        SearchListAPIRequest().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for perform request to delete a saved search
     
     - parameter inputData: Contains info for SearchListRequestModel
     
     */
    func deleteSavedSearch(with request: SavedSearchDeleteReqModel, presenterDelegate: ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = registerErrorForDeleteSavedSearch()
        SavedSearchDeleteAPIRequest().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error for search list API
     */
    private func registerErrorForSearchList() -> ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
        return errorResolver
    }
    
    /**
     This method is used for adding set of Predefined Error for delete saved search API
     */
    private func registerErrorForDeleteSavedSearch() -> ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
        return errorResolver
    }
        
}
