
//Note :- This class contains UpdateUserProfile Buisness Logic

class UpdateUserProfileBusinessLogic {
    
    
    deinit {
        print("UpdateUserProfileBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs(Email , password) construceted into a UpdateUserProfileRequestModel
     
     - parameter inputData: Contains info for UpdateUserProfile
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performUpdateUserProfile(withUpdateUserProfileRequestModel signUpRequestModel: UpdateUserProfileRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        UpdateUserProfileApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)

        return errorResolver
    }
}
