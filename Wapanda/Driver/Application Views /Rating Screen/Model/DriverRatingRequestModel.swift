//
//  DriverRatingRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class DriverRatingRequestModel {
    
    //MARK:- DriverRatingRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var driverId: String!
    var skip : Int!
    var limit : Int!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.driverId = builder.driverId
        self.skip = builder.skip
        self.limit = builder.limit
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var driverId: String!
        var skip : Int!
        var limit : Int!
        /**
         This method is used for setting Driver id
         
         - parameter driverId: String parameter that is going to be set on Driver Id
         
         - returns: returning Builder Object
         */
        func setDriverId(_ driverId:String)->Builder{
            self.driverId = driverId
            return self
        }
        /**
         This method is used for setting limit of item to be fetched
         
         - parameter limit: Int parameter that is going to be set on limit
         
         - returns: returning Builder Object
         */
        func setLimit(_ limit:Int)->Builder{
            self.limit = limit
            return self
        }
        /**
         This method is used for setting number of items to skipped
         
         - parameter skip: Int parameter that is going to be set on skip
         
         - returns: returning Builder Object
         */
        func setSkip(_ skip:Int)->Builder{
            self.skip = skip
            return self
        }
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of DriverRatingRequestModel
         and provide LoginRequestModel object.
         
         -returns : DriverRatingRequestModel
         */
        func build()->DriverRatingRequestModel{
            return DriverRatingRequestModel(builderObject: self)
        }
    }
    /**
     This method is used for getting End Driver Rating End point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.DRIVER_RATING_LIST, self.driverId) + "?skip=\((self.skip)!)&limit=\((self.limit)!)&order=-1&sort=ratedOn"
    }
    
}
