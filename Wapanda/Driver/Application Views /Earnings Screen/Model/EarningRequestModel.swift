//
//  EarningsRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class EarningsRequestModel {
    
    //MARK:- EarningsRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader

    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
   
        
        func setFromWeekDate(_ fromWeekDate:Int)->Builder{
            requestBody["fromWeekDate"] = fromWeekDate as AnyObject?
            return self
        }
        func setToWeekDate(_ toWeekDate:Int)->Builder{
            requestBody["toWeekDate"] = toWeekDate as AnyObject?
            return self
        }
        func setFromMonth(_ fromMonth:Int)->Builder{
            requestBody["fromMonth"] = fromMonth as AnyObject?
            return self
        }
        func setCurrentDate(_ currentDate:Int)->Builder{
            requestBody["currentDate"] = currentDate as AnyObject?
            return self
        }
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of EarningsRequestModel
         and provide LoginRequestModel object.
         
         -returns : EarningsRequestModel
         */
        func build()->EarningsRequestModel{
            return EarningsRequestModel(builderObject: self)
        }
    }
    /**
     This method is used for getting End Driver Rating End point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return AppConstants.ApiEndPoints.EARNINGS_DATA
    }
    
}
