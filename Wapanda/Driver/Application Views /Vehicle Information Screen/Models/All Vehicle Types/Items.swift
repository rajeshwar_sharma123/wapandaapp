//
//  Items.swift
//
//  Created by Daffomac-23 on 8/24/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Items: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let vehicleType = "vehicleType"
    static let icon = "icon"
    static let isActive = "isActive"
    static let id = "_id"
    static let createdAt = "createdAt"
    static let updatedAt = "updatedAt"
    static let price = "price"
  }

  // MARK: Properties
  public var vehicleType: String?
  public var icon: String?
  public var isActive: Bool? = false
  public var id: String?
  public var createdAt: String?
  public var updatedAt: String?
  public var price: Float?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    vehicleType <- map[SerializationKeys.vehicleType]
    icon <- map[SerializationKeys.icon]
    isActive <- map[SerializationKeys.isActive]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    updatedAt <- map[SerializationKeys.updatedAt]
    price <- map[SerializationKeys.price]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
    if let value = icon { dictionary[SerializationKeys.icon] = value }
    dictionary[SerializationKeys.isActive] = isActive
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
    self.icon = aDecoder.decodeObject(forKey: SerializationKeys.icon) as? String
    self.isActive = aDecoder.decodeObject(forKey: SerializationKeys.isActive) as? Bool
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Float
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
    aCoder.encode(icon, forKey: SerializationKeys.icon)
    aCoder.encode(isActive, forKey: SerializationKeys.isActive)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(price, forKey: SerializationKeys.price)
  }

}
