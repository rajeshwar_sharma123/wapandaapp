//
//  EarningsScreenViewDelgate.swift
//  Wapanda
//
//  Created by Daffolapmac on 05/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//


import UIKit
protocol EarningsScreenViewDelgate: BaseViewProtocol {
    func didReceiveEarningsList(withResponseModel:EarningResponseModel)
    func driverStatusUpdatedSuccessful(withResponseModel taxResponseModel:LoginResponseModel)
}
