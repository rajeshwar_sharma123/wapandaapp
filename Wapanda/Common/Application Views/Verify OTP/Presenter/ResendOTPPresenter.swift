
//Notes:- This class is used as presenter for ResendOTPPresenter

import Foundation
import ObjectMapper

class ResendOTPPresenter: ResponseCallback{
    
//MARK:- OTPPopupViewPresenter local properties
    private weak var resendOTPViewDelegate          : ResendOTPViewDelegate?
    private var resendOTPBusinessLogic              : ResendOTPBusinessLogic!
    
//MARK:- Constructor
    init(delegate responseDelegate:ResendOTPViewDelegate){
        self.resendOTPViewDelegate = responseDelegate
        self.resendOTPBusinessLogic = ResendOTPBusinessLogic()
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.resendOTPViewDelegate?.hideLoader()
        self.resendOTPViewDelegate?.resendEmailOTPSuccessfully()
    }
    
    func servicesManagerError(error : ErrorModel){
        self.resendOTPViewDelegate?.hideLoader()
        self.resendOTPViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- API calls
    
    func resendEmailOTP(withEmail email:String){
        
        self.resendOTPViewDelegate?.showLoader()

        let otpPopupRequestModel:ResendOTPRequestModel = ResendOTPRequestModel.Builder()
            .setEmail(email).setEntryType(.Email)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.resendOTPBusinessLogic.performResendOTP(withResendOTPRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
    func resendPhoneOTP(withPhone phone:String,andCountryCode countryCode:String){
        
        self.resendOTPViewDelegate?.showLoader()
        
        let otpPopupRequestModel:ResendOTPRequestModel = ResendOTPRequestModel.Builder()
            .setPhone(phone).setCountryCodeExt(countryCode).setEntryType(.Phone)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.resendOTPBusinessLogic.performResendOTP(withResendOTPRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
}
