//
//  Addresses.swift
//
//  Created by daffomac-31 on 10/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class UserAddresses: NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let addressType = "addressType"
    static let address = "address"
  }

  // MARK: Properties
  public var addressType: String?
  public var address: UserAddress?
    
    public override init() {
        super.init()
    }
    
    public convenience init(addresType: String, address: UserAddress) {
        self.init()
        
        self.addressType = addresType
        self.address = address
    }

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    addressType <- map[SerializationKeys.addressType]
    address <- map[SerializationKeys.address]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = addressType { dictionary[SerializationKeys.addressType] = value }
    if let value = address { dictionary[SerializationKeys.address] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.addressType = aDecoder.decodeObject(forKey: SerializationKeys.addressType) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? UserAddress
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(addressType, forKey: SerializationKeys.addressType)
    aCoder.encode(address, forKey: SerializationKeys.address)
  }

}
