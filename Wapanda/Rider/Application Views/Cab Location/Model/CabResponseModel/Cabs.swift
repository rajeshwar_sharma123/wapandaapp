//
//  Cabs.swift
//
//  Created by  on 15/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Cabs: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lng = "lng"
    static let movement = "movement"
    static let lat = "lat"
    static let cabInfo = "cabInfo"
    static let id = "_id"
  }

  // MARK: Properties
  public var lng: Float?
  public var movement: String?
  public var lat: Float?
  public var cabInfo: CabInfo?
  public var id: String?
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lng <- map[SerializationKeys.lng]
    movement <- map[SerializationKeys.movement]
    lat <- map[SerializationKeys.lat]
    cabInfo <- map[SerializationKeys.cabInfo]
    id <- map[SerializationKeys.id]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lng { dictionary[SerializationKeys.lng] = value }
    if let value = movement { dictionary[SerializationKeys.movement] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = cabInfo { dictionary[SerializationKeys.cabInfo] = value.dictionaryRepresentation() }
    if let value = id { dictionary[SerializationKeys.id] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lng = aDecoder.decodeObject(forKey: SerializationKeys.lng) as? Float
    self.movement = aDecoder.decodeObject(forKey: SerializationKeys.movement) as? String
    self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? Float
    self.cabInfo = aDecoder.decodeObject(forKey: SerializationKeys.cabInfo) as? CabInfo
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lng, forKey: SerializationKeys.lng)
    aCoder.encode(movement, forKey: SerializationKeys.movement)
    aCoder.encode(lat, forKey: SerializationKeys.lat)
    aCoder.encode(cabInfo, forKey: SerializationKeys.cabInfo)
    aCoder.encode(id, forKey: SerializationKeys.id)
  }

}
