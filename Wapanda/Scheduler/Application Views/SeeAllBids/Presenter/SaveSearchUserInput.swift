
import UIKit

struct SaveSearchUserInput {
    
    var fare: Float?
    var destinationAddress: String?
    var pickupDateTime: String?
    var title: String?
    var destLoc = [Float]()
}
