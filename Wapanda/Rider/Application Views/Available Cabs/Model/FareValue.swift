//
//  FareValue.swift
//
//  Created by  on 06/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class FareValue: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let driverRatio = "driverRatio"
    static let moneyview = "moneyview"
    static let estPrice = "estPrice"
    static let fairValues = "fairValues"
  }

  // MARK: Properties
  public var driverRatio: DriverRatio?
  public var moneyview: Moneyview?
  public var estPrice: Float?
  public var fairValues: FairValues?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    driverRatio <- map[SerializationKeys.driverRatio]
    moneyview <- map[SerializationKeys.moneyview]
    estPrice <- map[SerializationKeys.estPrice]
    fairValues <- map[SerializationKeys.fairValues]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = driverRatio { dictionary[SerializationKeys.driverRatio] = value.dictionaryRepresentation() }
    if let value = moneyview { dictionary[SerializationKeys.moneyview] = value.dictionaryRepresentation() }
    if let value = estPrice { dictionary[SerializationKeys.estPrice] = value }
    if let value = fairValues { dictionary[SerializationKeys.fairValues] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.driverRatio = aDecoder.decodeObject(forKey: SerializationKeys.driverRatio) as? DriverRatio
    self.moneyview = aDecoder.decodeObject(forKey: SerializationKeys.moneyview) as? Moneyview
    self.estPrice = aDecoder.decodeObject(forKey: SerializationKeys.estPrice) as? Float
    self.fairValues = aDecoder.decodeObject(forKey: SerializationKeys.fairValues) as? FairValues
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(driverRatio, forKey: SerializationKeys.driverRatio)
    aCoder.encode(moneyview, forKey: SerializationKeys.moneyview)
    aCoder.encode(estPrice, forKey: SerializationKeys.estPrice)
    aCoder.encode(fairValues, forKey: SerializationKeys.fairValues)
  }

}
