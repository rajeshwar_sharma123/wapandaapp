//
//  PriceMakerView.swift
//  Wapanda
//
//  Created by Daffolapmac on 23/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Charts
class PriceMakerView: MarkerView
{
    @IBOutlet var label: UILabel?
    
    @IBOutlet weak var imageViewArrow: UIImageView!
    open override func awakeFromNib()
    {
        imageViewArrow.getImageViewWithImageTintColor(color: UIColor.black)
        self.offset.x = -self.frame.size.width / 2.0
        self.offset.y = -self.frame.size.height - 7.0
    }
    
    open override func refreshContent(entry: ChartDataEntry, highlight: Highlight)
    {
        layoutIfNeeded()
    }
}
