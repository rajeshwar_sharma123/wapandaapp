import UIKit

class BidDetailRequestModel: NSObject {
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var bidId = ""

    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        bidId = builder.bidId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var bidId = ""
        
        /**
         This method is used for setting bidId
         
         - parameter bidId: String parameter that is going to be set on bidId whose detail we have to fetch
         
         - returns: returning Builder Object
         */
        func setBidId(bidId: String) -> Builder{
            self.bidId = bidId
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of BidDetailRequestModel
         and provide BidDetailRequestModel object.
         
         -returns : BidDetailRequestModel
         */
        func build() -> BidDetailRequestModel {
            return BidDetailRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting VisitedStop end point
     
     -returns: String containg end point
     */
    func getEndPoint() -> String{
        return "/auctions/detail/" + bidId + "/DRIVER"
    }
    
}
