//
//  Years.swift
//
//  Created by Daffodil on 22/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Years: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let maxYear = "max_year"
    static let minYear = "min_year"
  }

  // MARK: Properties
  public var maxYear: String?
  public var minYear: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    maxYear <- map[SerializationKeys.maxYear]
    minYear <- map[SerializationKeys.minYear]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = maxYear { dictionary[SerializationKeys.maxYear] = value }
    if let value = minYear { dictionary[SerializationKeys.minYear] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.maxYear = aDecoder.decodeObject(forKey: SerializationKeys.maxYear) as? String
    self.minYear = aDecoder.decodeObject(forKey: SerializationKeys.minYear) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(maxYear, forKey: SerializationKeys.maxYear)
    aCoder.encode(minYear, forKey: SerializationKeys.minYear)
  }

}
