//
//  Trip.swift
//
//  Created by daffomac-31 on 29/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class Trip: NSObject,Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let endLoc = "endLoc"
    static let vehicle = "vehicle"
    static let createdAt = "createdAt"
    static let startLoc = "startLoc"
    static let toAddress = "toAddress"
    static let driver = "driver"
    static let cancelledBy = "cancelledBy"
    static let fromAddress = "fromAddress"
    static let status = "status"
    static let id = "_id"
    static let estDistance = "estDistance"
    static let pickupByTime = "pickupByTime"
    static let rider = "rider"
    static let updatedAt = "updatedAt"
    static let agreedPrice = "agreedPrice"
    static let estDuration = "estDuration"
    static let fromLoc = "fromLoc"
    static let arrivingNotified = "arrivingNotified"
    static let tripRating = "tripRating"
    static let tripComment = "tripComment"
    static let ratedOn = "ratedOn"
    static let paid = "paid"
    static let routeImageId = "routeImageId"
    static let stop = "stops"
    static let paymentId = "paymentId"

    static let payment = "payment"

  }

  // MARK: Properties
  public var endLoc: EndLocation?
  public var vehicle: Vehicle?
  public var createdAt: String?
  public var startLoc: StartLocation?
  public var fromLoc: StartLocation?
  public var toAddress: String?
  public var driver: Driver?
  public var cancelledBy: String?
  public var fromAddress: String?
  public var status: String?
  public var id: String?
  public var estDistance: Double?
  public var pickupByTime: String?
  public var rider: Rider?
  public var updatedAt: String?
  public var agreedPrice: Double?
  public var estDuration: UIntMax?
  public var arrivingNotified: String?
  public var tripRating: Double?
  public var tripComment: String?
  public var ratedOn: String?
  public var stop: [AddStopModel]?
  public var paid: Bool? = false
  public var routeImageId: String?
  public var paymentId: String?
    public var payment: Payment?

      
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    endLoc <- map[SerializationKeys.endLoc]
    vehicle <- map[SerializationKeys.vehicle]
    createdAt <- map[SerializationKeys.createdAt]
    startLoc <- map[SerializationKeys.startLoc]
    fromLoc <- map[SerializationKeys.fromLoc]
    toAddress <- map[SerializationKeys.toAddress]
    driver <- map[SerializationKeys.driver]
    cancelledBy <- map[SerializationKeys.cancelledBy]
    fromAddress <- map[SerializationKeys.fromAddress]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    estDistance <- map[SerializationKeys.estDistance]
    pickupByTime <- map[SerializationKeys.pickupByTime]
    rider <- map[SerializationKeys.rider]
    updatedAt <- map[SerializationKeys.updatedAt]
    agreedPrice <- map[SerializationKeys.agreedPrice]
    estDuration <- map[SerializationKeys.estDuration]
    arrivingNotified <- map[SerializationKeys.arrivingNotified]
    tripRating <- map[SerializationKeys.tripRating]
    tripComment <- map[SerializationKeys.tripComment]
    ratedOn <- map[SerializationKeys.ratedOn]
    stop <- map[SerializationKeys.stop]
    paid <- map[SerializationKeys.paid]
    routeImageId <- map[SerializationKeys.routeImageId]
    paymentId <- map[SerializationKeys.paymentId]

    payment <- map[SerializationKeys.payment]


  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = endLoc { dictionary[SerializationKeys.endLoc] = value.dictionaryRepresentation() }
    if let value = fromLoc { dictionary[SerializationKeys.fromLoc] = value.dictionaryRepresentation() }
    if let value = vehicle { dictionary[SerializationKeys.vehicle] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = startLoc { dictionary[SerializationKeys.startLoc] = value.dictionaryRepresentation() }
    if let value = toAddress { dictionary[SerializationKeys.toAddress] = value }
    if let value = driver { dictionary[SerializationKeys.driver] = value.dictionaryRepresentation() }
    if let value = cancelledBy { dictionary[SerializationKeys.cancelledBy] = value }
    if let value = fromAddress { dictionary[SerializationKeys.fromAddress] = value }
    if let value = arrivingNotified { dictionary[SerializationKeys.arrivingNotified] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = estDistance { dictionary[SerializationKeys.estDistance] = value }
    if let value = pickupByTime { dictionary[SerializationKeys.pickupByTime] = value }
    if let value = rider { dictionary[SerializationKeys.rider] = value.dictionaryRepresentation() }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = agreedPrice { dictionary[SerializationKeys.agreedPrice] = value }
    if let value = estDuration { dictionary[SerializationKeys.estDuration] = value }
    if let value = tripRating { dictionary[SerializationKeys.tripRating] = value }
    if let value = tripComment { dictionary[SerializationKeys.tripComment] = value }
    if let value = ratedOn { dictionary[SerializationKeys.ratedOn] = value }
    if let value = stop { dictionary[SerializationKeys.stop] = value }
    if let value = paid { dictionary[SerializationKeys.paid] = value }
    if let value = routeImageId { dictionary[SerializationKeys.routeImageId] = value }
    if let value = paymentId { dictionary[SerializationKeys.paymentId] = value }
    if let value = payment { dictionary[SerializationKeys.payment] = value.dictionaryRepresentation() }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.endLoc = aDecoder.decodeObject(forKey: SerializationKeys.endLoc) as? EndLocation
    self.vehicle = aDecoder.decodeObject(forKey: SerializationKeys.vehicle) as? Vehicle
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.startLoc = aDecoder.decodeObject(forKey: SerializationKeys.startLoc) as? StartLocation
    self.fromLoc = aDecoder.decodeObject(forKey: SerializationKeys.fromLoc) as? StartLocation
    self.toAddress = aDecoder.decodeObject(forKey: SerializationKeys.toAddress) as? String
    self.driver = aDecoder.decodeObject(forKey: SerializationKeys.driver) as? Driver
    self.cancelledBy = aDecoder.decodeObject(forKey: SerializationKeys.cancelledBy) as? String
    self.fromAddress = aDecoder.decodeObject(forKey: SerializationKeys.fromAddress) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.estDistance = aDecoder.decodeObject(forKey: SerializationKeys.estDistance) as? Double
    self.pickupByTime = aDecoder.decodeObject(forKey: SerializationKeys.pickupByTime) as? String
    self.rider = aDecoder.decodeObject(forKey: SerializationKeys.rider) as? Rider
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.arrivingNotified = aDecoder.decodeObject(forKey: SerializationKeys.arrivingNotified) as? String
    self.agreedPrice = aDecoder.decodeObject(forKey: SerializationKeys.agreedPrice) as? Double
    self.estDuration = aDecoder.decodeObject(forKey: SerializationKeys.estDuration) as? UIntMax
    self.tripRating = aDecoder.decodeObject(forKey: SerializationKeys.tripRating) as? Double
    self.tripComment = aDecoder.decodeObject(forKey: SerializationKeys.tripComment) as? String
    self.ratedOn = aDecoder.decodeObject(forKey: SerializationKeys.ratedOn) as? String
    self.stop = aDecoder.decodeObject(forKey: SerializationKeys.stop) as? [AddStopModel]
    self.paid = aDecoder.decodeObject(forKey: SerializationKeys.paid) as? Bool
    self.routeImageId = aDecoder.decodeObject(forKey: SerializationKeys.routeImageId) as? String
    self.paymentId = aDecoder.decodeObject(forKey: SerializationKeys.paymentId) as? String

    self.payment = aDecoder.decodeObject(forKey: SerializationKeys.payment) as? Payment

  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(endLoc, forKey: SerializationKeys.endLoc)
    aCoder.encode(vehicle, forKey: SerializationKeys.vehicle)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(startLoc, forKey: SerializationKeys.startLoc)
    aCoder.encode(fromLoc, forKey: SerializationKeys.fromLoc)
    aCoder.encode(toAddress, forKey: SerializationKeys.toAddress)
    aCoder.encode(driver, forKey: SerializationKeys.driver)
    aCoder.encode(cancelledBy, forKey: SerializationKeys.cancelledBy)
    aCoder.encode(fromAddress, forKey: SerializationKeys.fromAddress)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(estDistance, forKey: SerializationKeys.estDistance)
    aCoder.encode(pickupByTime, forKey: SerializationKeys.pickupByTime)
    aCoder.encode(rider, forKey: SerializationKeys.rider)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(agreedPrice, forKey: SerializationKeys.agreedPrice)
    aCoder.encode(estDuration, forKey: SerializationKeys.estDuration)
    aCoder.encode(arrivingNotified, forKey: SerializationKeys.arrivingNotified)
    aCoder.encode(tripRating, forKey: SerializationKeys.tripRating)
    aCoder.encode(tripComment, forKey: SerializationKeys.tripComment)
    aCoder.encode(ratedOn, forKey: SerializationKeys.ratedOn)
    aCoder.encode(stop, forKey: SerializationKeys.stop)
    aCoder.encode(paid, forKey: SerializationKeys.paid)
    aCoder.encode(routeImageId, forKey: SerializationKeys.routeImageId)
    aCoder.encode(paymentId, forKey: SerializationKeys.paymentId)

    aCoder.encode(payment, forKey: SerializationKeys.payment)

  }

}
