
//Note :- This Class is used for GooglePlace Service means it is used for handling GooglePlace Api

import GooglePlaces

class GooglePlaceServiceManager {
    
    //MARK:- Helper methods
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: GooglePlaceRequestModel which contains Request header and request body for the signup api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeRequest(withReqFormData reqFromData: GooglePlaceRequestModel, responseCallback: ResponseCallbackGeneral) {
        
        reqFromData.placeClient.autocompleteQuery(reqFromData.query, bounds: reqFromData.bounds, filter: reqFromData.filter, callback: {(results, error) -> Void in
            if let _ = error {
                let errorModel = ErrorModel()
                errorModel.setErrorTitle((error?.localizedDescription)!)
                responseCallback.servicesManagerError(error: errorModel)
            }else if let _ = results {
                responseCallback.servicesManagerSuccessResponse(responseObject: results as AnyObject)
            }
        })
    }
}

