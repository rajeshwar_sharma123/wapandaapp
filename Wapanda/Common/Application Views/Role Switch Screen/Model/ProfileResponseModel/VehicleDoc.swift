//
//  VehicleDoc.swift
//
//  Created by  on 8/30/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class VehicleDoc:  NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carType = "carType"
    static let id = "_id"
    static let createdAt = "createdAt"
    static let approved = "approved"
    static let imageId = "imageId"
    static let carPlateNumber = "carPlateNumber"
    static let make = "make"
    static let model = "model"
    static let companyName = "companyName"
    static let updatedAt = "updatedAt"
    static let year = "year"
  }

  // MARK: Properties
  public var carType: CarType?
  public var id: String?
  public var createdAt: String?
  public var approved: Bool? = false
  public var imageId: String?
  public var carPlateNumber: String?
  public var make: String?
  public var model: String?
  public var companyName: String?
  public var updatedAt: String?
  public var year: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    carType <- map[SerializationKeys.carType]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    approved <- map[SerializationKeys.approved]
    imageId <- map[SerializationKeys.imageId]
    carPlateNumber <- map[SerializationKeys.carPlateNumber]
    make <- map[SerializationKeys.make]
    model <- map[SerializationKeys.model]
    companyName <- map[SerializationKeys.companyName]
    updatedAt <- map[SerializationKeys.updatedAt]
    year <- map[SerializationKeys.year]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carType { dictionary[SerializationKeys.carType] = value.dictionaryRepresentation() }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    dictionary[SerializationKeys.approved] = approved
    if let value = imageId { dictionary[SerializationKeys.imageId] = value }
    if let value = carPlateNumber { dictionary[SerializationKeys.carPlateNumber] = value }
    if let value = make { dictionary[SerializationKeys.make] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = companyName { dictionary[SerializationKeys.companyName] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = year { dictionary[SerializationKeys.year] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? CarType
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.approved = aDecoder.decodeObject(forKey: SerializationKeys.approved) as? Bool
    self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.imageId) as? String
    self.carPlateNumber = aDecoder.decodeObject(forKey: SerializationKeys.carPlateNumber) as? String
    self.make = aDecoder.decodeObject(forKey: SerializationKeys.make) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.companyName = aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.year = aDecoder.decodeObject(forKey: SerializationKeys.year) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(approved, forKey: SerializationKeys.approved)
    aCoder.encode(imageId, forKey: SerializationKeys.imageId)
    aCoder.encode(carPlateNumber, forKey: SerializationKeys.carPlateNumber)
    aCoder.encode(make, forKey: SerializationKeys.make)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(companyName, forKey: SerializationKeys.companyName)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(year, forKey: SerializationKeys.year)
  }

}
