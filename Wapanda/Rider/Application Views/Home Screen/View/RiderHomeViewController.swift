

import UIKit
import GoogleMaps
import IQKeyboardManager
import GooglePlaces
import AFNetworking
import SwiftMessages
import Stripe

enum ADD_ADDRESS_TYPE{
    case NONE
    case HOME
    case WORK
    case LAST_VISITED
}

class RiderHomeViewController: BaseViewController {
    
    @IBOutlet weak var viewMapGoogle: GMSMapView!
    @IBOutlet weak var viewDestinationSearch: UIView!
    var currentLocationMarker: GMSMarker?
    var pickLocationPin: UIImageView?
    @IBOutlet weak var lastVisitedWidthLayoutConstraint: NSLayoutConstraint!
    var oldValue = 0{
        didSet{
            self.updateNumberOfAvailableCabs()
        }
    }
    var cabLocationInfo : [Cabs] = []{
        willSet {
        }
        didSet {
           if (self.oldValue != cabLocationInfo.count)
            {
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.GET_CAB_LIST), object: nil, userInfo: nil)
                self.oldValue = cabLocationInfo.count
            }
        }
    }

    var cabMarkers      : [GMSMarker] = []     //SEARCH SCREEN OUTLETS AND VARIABLES
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewLastVisited: UIView!
    @IBOutlet weak var viewOptions: UIView!
    @IBOutlet weak var shadowViewHome: UIView!
    @IBOutlet weak var shadowViewWork: UIView!
    @IBOutlet weak var shadowViewLastVisited: UIView!
    fileprivate var blurEffectView: UIVisualEffectView!
    @IBOutlet weak var txtFieldEnterDestination: UITextField!
    var dataSource: [GMSAutocompletePrediction] = []
    var dataSourceUserAddresses: [Address] = []
    var presenterGooglePlaces: GooglePlaceViewPresenter!
    var presenterLaunch: LaunchPresenter!
    var isMinimized = true
    var isFirstTime = true
    var shouldBounce = true
    @IBOutlet weak var bgViewSearch: UIView!
    @IBOutlet weak var btnBack: UIButton!
    var timerEditingChanged: Timer? = nil
    var cabLocationViewPresenter : CabLocationViewPresenter!
    
    //ADD ADDRESS
    var addDestinationTypeSelected = ADD_ADDRESS_TYPE.NONE
    var presenterAddAddress: AddAddressViewPresenter!
    var placeDetailPresenter: GooglePlaceDetailViewPresenter!
    var selectedDestinationAddress: SelectedPlaceModel?
    var selectedDestinationAddressTitle: String?
    
    //Source Window
    var sourceInfoWindow: PickSourceInfoWindow!
    var userCurrentLocationCoordinate: CLLocationCoordinate2D?
    var selectedSourceAddress: SelectedPlaceModel?
    @IBOutlet weak var btnMyCurrentLocation: UIButton!
    //Set in case of not opening cab selection view while drawing path on map
    
    var isCabSelectionScreenNotPresent = true
    var cabSelectionVC: CabSelectionBottomSheetViewController?
    
    //Draw Route
    var presenterGoogleDirection: GoogleDirectionDetailViewPresenter!
    var presenterAddPayment: AddPaymentPresenter!

    @IBOutlet weak var btnScrollUpCabView: UIButton!
    
    //New Screen Variabls
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var _iLayoutConstraintClosedAddressSelectionView: NSLayoutConstraint!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblNumberOfRidesAvailable: UILabel!
    @IBOutlet weak var imgViewCloseAddressView: UIImageView!
    @IBOutlet weak var _iLayoutConstaintHeightForLastVisited: NSLayoutConstraint!
    @IBOutlet weak var lblHomeAddress: UILabel!
    @IBOutlet weak var lblWorkAddress: UILabel!
    @IBOutlet weak var lblLastVisitedAddress: UILabel!
    @IBOutlet weak var viewLowestPrice: UIView!
    @IBOutlet weak var viewClosestRide: UIView!
    @IBOutlet weak var viewMostSeats: UIView!
    @IBOutlet weak var viewScreenContent: UIView!
    var isAddressShortcutOpen = false
    @IBOutlet weak var viewFilterOptions: UIView!
    @IBOutlet weak var viewPlaces: UIView!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var viewLastVisit: UIView!
    @IBOutlet weak var lblLowestPrice: UILabel!
    @IBOutlet weak var lblClosestRide: UILabel!
    @IBOutlet weak var lblMostSeats: UILabel!
    @IBOutlet weak var _iLayoutConstraintWorkAddressTrailingSpaceToView: NSLayoutConstraint!
    @IBOutlet weak var _iLayoutConstraintHomeAddressTrailingSpaceToView: NSLayoutConstraint!
    @IBOutlet weak var viewAddWork: UIBorderView!
    @IBOutlet weak var viewAddHome: UIBorderView!
    let userProfileUpdate = Notification.Name(AppConstants.NSNotificationNames.USER_PROFILE_UPDATED)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.viewMapGoogle.customizeMapAppearance()
        self.viewMapGoogle.delegate = self
        
        //Check for location permissions
        LocationServiceManager.sharedInstance.viewDelegate = self
        LocationServiceManager.sharedInstance.checkForUserPermissions()
        
        //Search Screen
//        self.loadSetupSearchScreen()
        
        //Add Address screen
        self.presenterAddAddress = AddAddressViewPresenter(delegate: self)
        self.placeDetailPresenter = GooglePlaceDetailViewPresenter(delegate: self)
        self.cabLocationViewPresenter = CabLocationViewPresenter(delegate: self)
        
        //Source Info Window
        sourceInfoWindow = self.getInfoNibForAddress()
        
        //Socket Observers
        NotificationCenter.default.addObserver(self, selector: #selector(updateCabLocation(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.UPDATE_CAB_LOCATION), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(cabEntersRegion(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.CAB_ENTERS), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(cabExitsRegion(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.CAB_EXITS), object: nil)
        
        self.hideScrollUpButton()
        
        //Draw Route 
        self.googleDirectionSettingOnLoad()
                
        //Stripe Customer Id Presenter
        self.presenterAddPayment = AddPaymentPresenter(delegate: self)
        
        self.initScreenUI()
        self.appLaunchDataRequst()
        NotificationCenter.default.addObserver(self, selector: #selector(appLaunchDataRequst), name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.APP_BECOME_ACTIVE_NOTIFICATION), object: nil)
    }
    
    func appLaunchDataRequst(){
        if AppUtility.isUserLogin(){
            self.cabLocationViewPresenter.sendLaunchDataRequest()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
       NotificationCenter.default.removeObserver(self)
    }
    
    
    override func viewDidLayoutSubviews() {
        if self.isAddressShortcutOpen{
            self.openAddressShortcuts()
        }
        else{
            self.closeAddressShortcuts()
        }
     
        self.handleAddAdressViewLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SocketManager.shared.driverSocket.disconnect()
        //Check for location permissions
        LocationServiceManager.sharedInstance.viewDelegate = self
        LocationServiceManager.sharedInstance.checkForUserPermissions()
        self.customizeNavigationBar()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(initScreenUI), name: self.userProfileUpdate, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.isFirstTime = false
    }
    
    func customizeNavigationBar(){
        self.customizeNavigationBarWithTitleImage(image: #imageLiteral(resourceName: "ic_panda_navigation"))
        self.addNavigationMenuButton()
    }
    
    //MARK: Navigation Selector
    override func menuButtonClick() ->Void {
        super.menuButtonClick()
    }
    
    func updateCurrentLocationOnMap(lat: Double, long: Double){
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if let marker = self.currentLocationMarker{
            self.currentLocationMarker = self.updateMarkerAtPosition(withPoints: CLLocationCoordinate2D(latitude: lat, longitude: long), marker: marker, bearing: 0.0)
            self.showSourcePickerPinWithCoordinates(coordinate: coordinate)
        }
        else{
            self.currentLocationMarker = self.addMarkerWithCoordinates(coordinate: coordinate, markerImage: #imageLiteral(resourceName: "ic_current_location_with_shadow"))
        }
    }
    
    // Bound all markers of map to fit view
    func boundCameraToFitAllMarkers()
    {
        var bounds = GMSCoordinateBounds()
        for marker in self.cabMarkers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        if let _ = self.currentLocationMarker
        {
            bounds = bounds.includingCoordinate((self.currentLocationMarker?.position)!)
        }
        viewMapGoogle.moveCamera(GMSCameraUpdate.fit(bounds, withPadding: ScreenSize.size.height * 0.26))
        if let _ = self.currentLocationMarker
        {
            viewMapGoogle.moveCamera(GMSCameraUpdate.setTarget((self.currentLocationMarker?.position)!))
        }
        let newBounds = GMSCoordinateBounds.init(region: viewMapGoogle.projection.visibleRegion())
        viewMapGoogle.animate(with: GMSCameraUpdate.fit(newBounds,withPadding: ScreenSize.size.height * 0.26  ))
    }
    
    //MARK: Button Actions
    
    @IBAction func btnWorkClick(_ sender: Any) {
        if AppUtility.isUserLogin(){
            if let workAddress = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
                //Move to cab selection if both address available
               
                self.selectedDestinationAddress = SelectedPlaceModel(title: workAddress.address!, coordinate: CLLocationCoordinate2D(latitude: workAddress.lat!, longitude: workAddress.lng!))
                
                if let _ = self.selectedDestinationAddress, let _ = self.selectedSourceAddress{
                    self.moveToCabSelectionScreen()
                }
                else{
                    AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_SOURCE_ADDRESS)
                }
                
            }
            else{
                self.clearUserAddressess()
                self.addDestinationTypeSelected = .WORK
                self.showSearchAddressView()
            }
        }
        else{
            //Move To Signup/Login Screen
            self.navigateToUserWelcomeScreen()
        }
    }
    
    @IBAction func btnLastVisitedLinkClick(_ sender: Any) {
    }
    
    @IBAction func btnHomeClick(_ sender: Any) {
        if AppUtility.isUserLogin(){
            if let workAddress = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
                //Move to cab selection if both address available
                self.selectedDestinationAddress = SelectedPlaceModel(title: workAddress.address!, coordinate: CLLocationCoordinate2D(latitude: workAddress.lat!, longitude: workAddress.lng!))
                
                if let _ = self.selectedDestinationAddress, let _ = self.selectedSourceAddress{
                    self.moveToCabSelectionScreen()
                }
                else{
                    AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_SOURCE_ADDRESS)
                }
                
            }
            else{
                self.clearUserAddressess()
                self.addDestinationTypeSelected = .HOME
                self.showSearchAddressView()
            }
        }
        else{
            //Move To Signup/Login Screen
            self.navigateToUserWelcomeScreen()
        }
    }
    
    @IBAction func btnScrollUpCabViewClick(_ sender: Any) {
        
        self.moveToCabSelectionScreen()
        self.hideScrollUpButton()
    }
    
    @IBAction func btnMyCurrentLocationClick(_ sender: Any) {
        
//        navigateToScheduleTimeViewController()
//        return
//
        if let currentLocation = self.userCurrentLocationCoordinate{
            self.viewMapGoogle.updateCameraLocation(lat: currentLocation.latitude, long: currentLocation.longitude, zoom: 15.0)
        }
    }
    
    
    func navigateToScheduleTimeViewController(){
        
        let rideTimeVC = UIViewController.getViewController(ScheduleRideTimeViewController.self,storyboard: UIStoryboard.Storyboard.Scheduling.object)
        // rolesVC.initializeScreenWithDataAs(hideBackButton: true)
        self.navigationController?.pushViewController(rideTimeVC, animated: true)
    }
    
    //MARK: View Delegates
    
    override func showLoader(_ VC: AnyObject?) {
        super.showLoader(self)
    }
    
    override func hideLoader(_ VC: AnyObject?) {
        self.hideLoader()
    }
    
    override func showErrorAlert(_ alertTitle: String, alertMessage: String, VC: AnyObject?) {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    //MARK : Socket handlers
    @objc func updateCabLocation(_ notication:Notification)
    {
        self.cabLocationViewPresenter.updateMarkersLocation(onCabLocationList: cabLocationInfo, andMarkerList: cabMarkers, with: (notication.object as! Cabs))
    }
    
    @objc func cabEntersRegion(_ notication:Notification)
    {
        self.cabLocationViewPresenter.addMarkersLocation(onCabLocationList: cabLocationInfo, andMarkerList: cabMarkers, with: (notication.object as! Cabs))
    }
    
    @objc func cabExitsRegion(_ notication:Notification)
    {
        self.cabLocationViewPresenter.removeMarkersLocation(onCabLocationList: cabLocationInfo, andMarkerList: cabMarkers, with: (notication.object as! Cabs))
    }
    
    func checkSocketConnection(lat: Double, long: Double)
    {
        if SocketManager.shared.riderSocket.status == .connected
        {
            SocketManager.shared.sendRiderLocation(withLat:lat, andLong: long)
        }
        else
        {
            SocketManager.shared.connectToRiderSocket(withLat:lat, andLong: long)
        }
    }
    
    //MARK: New Screen Impplementation
    @IBAction func enterDestinationClicked(_ sender: Any) {
        if AppUtility.isUserLogin(){
            self.addDestinationTypeSelected = .NONE
            self.showSearchPlaceScreenWithTitle(title: "Enter Destination Address")
        }
        else{
            //Move To Signup/Login Screen
            self.navigateToUserWelcomeScreen()

        }
    }
    
    @IBAction func homeAddressClicked(_ sender: Any) {
        if AppUtility.isUserLogin(){
            if let workAddress = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
                //Move to cab selection if both address available
                self.selectedDestinationAddress = SelectedPlaceModel(title: workAddress.address!, coordinate: CLLocationCoordinate2D(latitude: workAddress.lat!, longitude: workAddress.lng!))
                self.getSourceAddressAndMoveToCabSelectionScreen()
            }
            else{
                self.addDestinationTypeSelected = .HOME
                self.showSearchPlaceScreenWithTitle(title: "Enter home Address")
            }
        }
        else{
            //Move To Signup/Login Screen
            self.navigateToUserWelcomeScreen()
        }
    }
    
    @IBAction func workAddressClicked(_ sender: Any) {
        
        if AppUtility.isUserLogin(){
            if let workAddress = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
                //Move to cab selection if both address available
                self.selectedDestinationAddress = SelectedPlaceModel(title: workAddress.address!, coordinate: CLLocationCoordinate2D(latitude: workAddress.lat!, longitude: workAddress.lng!))
                self.getSourceAddressAndMoveToCabSelectionScreen()
            }
            else{
                self.addDestinationTypeSelected = .WORK
                self.showSearchPlaceScreenWithTitle(title: "Enter work Address")
            }
        }
        else{
            //Move To Signup/Login Screen
            self.navigateToUserWelcomeScreen()
        }
        
    }
    
    @IBAction func lastVisitedClicked(_ sender: Any) {
        if AppUtility.isUserLogin(){
            if let workAddress = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.recentlyVisited{
                //Move to cab selection if both address available
                self.selectedDestinationAddress = SelectedPlaceModel(title: workAddress.address!, coordinate: CLLocationCoordinate2D(latitude: workAddress.lat!, longitude: workAddress.lng!))
                self.getSourceAddressAndMoveToCabSelectionScreen()
            }
        }
    }
    
    @IBAction func lowestPriceFilterClick(_ sender: UITapGestureRecognizer) {
        self.toggleFilter(sender: sender.view!)
    }
    
    @IBAction func closestRideFilterClick(_ sender: UITapGestureRecognizer) {
        self.toggleFilter(sender: sender.view!)
    }
    
    @IBAction func mostSeatsFilterClick(_ sender: UITapGestureRecognizer) {
        if UserRideDetail.shared.isMostSeatAvailableSelected{
            self.setFilterUnselected(withFilter: sender.view!)
        }
        else{
            self.setFilterSelected(withFilter: sender.view!)
        }
        
        UserRideDetail.shared.isMostSeatAvailableSelected = !UserRideDetail.shared.isMostSeatAvailableSelected
    }
    
    @IBAction func placesClick(_ sender: Any) {
        if AppUtility.isUserLogin(){
            self.isAddressShortcutOpen =  !self.isAddressShortcutOpen
            self.viewDidLayoutSubviews()
        }
        else{
            self.navigateToUserWelcomeScreen()
        }
        
    }
}

//MARK: Screen Navigation Login Goes Here ....
extension RiderHomeViewController{
    func navigateToUserWelcomeScreen(){
        self.navigationController?.popToRootViewController(animated: true)
    }
}

//MARK: Search View Handling
extension RiderHomeViewController{
    
    
    func maximizeAddressSearchView(){
        self.hideMenuButton()
        
        UIView.animate(withDuration: 0.4, animations: {
            self.setViewFrameToMax()
        }) { (animationEndded) in
            self.addBlurrEffect()
            self.showBackButton()
            self.tableView.reloadData()
        }
    }
    
    func minimizeAddressSearchView(){
        self.hideBlurrEffect()
        self.endEditing()
        self.addDestinationTypeSelected = .NONE
        
        UIView.animate(withDuration: 0.4, animations: {
            self.setViewFrameMin()
        }) { (animationEndded) in
            self.showSearchButton()
            self.showOptionBar()
            self.setViewMin()
            self.addNavigationMenuButton()
        }
        
    }
    
    func addBlurrEffect(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 0.5
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.viewMapGoogle.addSubview(blurEffectView)
    }
    
    func hideBlurrEffect(){
        blurEffectView.removeFromSuperview()
    }
}

extension RiderHomeViewController: WelcomeScreenViewDelegate{
    func showControllerWithVC(controller: UIViewController){
        self.present(controller, animated: true, completion: nil)
    }
    
    func locationUpdated(lat: Double, long: Double){
        guard AppUtility.isUserLogin() != false else{return}
        
        if isFirstTime
        {
			self.userCurrentLocationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            self.viewMapGoogle.updateCameraLocation(lat: lat, long: long,zoom:15.0)
            self.updateCurrentLocationOnMap(lat: lat, long: long)
            self.cabLocationViewPresenter.fetchCabList(withCabViewRequestModel: CabLocationViewRequestModel(lat: lat, lng: long))
            isFirstTime = false
        }
    }
}

//MARK: Pick source pin
extension RiderHomeViewController{
    func addPinOnMapCenter(){
        if  pickLocationPin == nil{
            pickLocationPin = UIImageView(image: #imageLiteral(resourceName: "ic_pin"))
            pickLocationPin?.center = CGPoint(x: self.viewMapGoogle.center.x, y: self.viewMapGoogle.center.y-10)
            self.viewMapGoogle.addSubview(pickLocationPin!)
            self.viewMapGoogle.bringSubview(toFront: self.pickLocationPin!)
            
            self.showSourceInfoViewForLocationPin()
        }
    }
    
    func removePickSourcePinFromScreen(){
        self.pickLocationPin?.isHidden = true
        self.sourceInfoWindow.removeFromSuperview()
    }
}

//MARK:- Rider Home View Delegate Methods
extension RiderHomeViewController:CabLocationViewDelegate{
    func updateCabLocationList(_ cabLocationList:[Cabs])
    {
        self.cabLocationInfo = cabLocationList
    }
    func updateMarkerList(_ markersList: [GMSMarker]) {
        self.cabMarkers = markersList
    }
   
    func fetchNearByCabListSuccessful(_ cabLocationList: CabLocationsResponseModel) {
        
        self.checkSocketConnection(lat: (self.currentLocationMarker?.position.latitude)!, long: (self.currentLocationMarker?.position.longitude)!)
        if cabLocationList.cabs?.count == 0
        {
            AppUtility.presentToastWithMessage(AppConstants.ErrorMessages.NO_CABS_FOUND)
        }
        else
        {
            for cabMarker in self.cabMarkers
            {
                cabMarker.map = nil
            }
            self.cabMarkers = []
            
            self.cabLocationInfo = cabLocationList.cabs!
            
            //Remove cab with same User ID as Rider.
            let userCabIndex = self.cabLocationInfo.index { (objCab) -> Bool in
                objCab.id == AppDelegate.sharedInstance.userInformation.id
            }
            if let cabIndex = userCabIndex
            {
                self.cabLocationInfo.remove(at: cabIndex)
            }
            for cabLocation in self.cabLocationInfo
            {
                let _ = self.addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D(latitude: Double(cabLocation.lat!), longitude: Double(cabLocation.lng!)), markerImage: #imageLiteral(resourceName: "ic_car"))
            }
            if  SocketManager.shared.riderSocket.status != .connected
            {
                SocketManager.shared.connectToRiderSocket(withLat: (self.currentLocationMarker?.position.latitude)!, andLong: (self.currentLocationMarker?.position.longitude)!)
            }
        }
    }
    
    func addMarkerWithCoordinates(coordinate: CLLocationCoordinate2D, markerImage image: UIImage)->GMSMarker{
        
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude:coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.icon = image
        if let _ = self.selectedDestinationAddress, let _ = self.selectedSourceAddress{
        }
        else
        {
        marker.map = viewMapGoogle
        }
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        if image != #imageLiteral(resourceName: "ic_current_location_with_shadow")
        {
            self.cabMarkers.append(marker)
            if self.shouldBounce
            {
                self.boundCameraToFitAllMarkers()
                self.shouldBounce = false
            }
        }
        return marker
    }
    func removeMarkerAtPosition(marker: GMSMarker)->GMSMarker{
        return self.viewMapGoogle.removeMarker(marker: marker)
    }
    
    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker,bearing:Double)->GMSMarker{
       return self.viewMapGoogle.updateMarkerAtPosition(withPoints: coordinate, marker: marker, bearing: bearing)
    }
    func didReceiveLaunchDetails(withLaunchRequestModel:LaunchResponseModel)
    {
        AppDelegate.sharedInstance.userInformation = withLaunchRequestModel.loginResponseModel
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        self.view.layoutSubviews()
        self.initalizeViewWithSavedAddresses()
        
        //Update Stripe Customer
        self.setUpStripeCustomer()
        
        guard let _ = withLaunchRequestModel.trip,withLaunchRequestModel.trip?.rider?.id == AppDelegate.sharedInstance.userInformation.id else { return }
        
        if withLaunchRequestModel.trip?.status != TRIP_STATUS.ENDED.rawValue
        {
            let objNewRequestVC = UIViewController.getViewController(RiderTripViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            objNewRequestVC.pushNotificationModel = PushNotificationObjectModel(JSON: [:])
            objNewRequestVC.pushNotificationModel.trip = withLaunchRequestModel.trip
            
            guard (UIApplication.shared.visibleViewController?.isKind(of: RiderTripViewController.self))! == false else{return}
            
            if let _ = UIApplication.shared.visibleViewController!.navigationController{
              UIApplication.shared.visibleViewController!.navigationController?.pushViewController(objNewRequestVC, animated: true)
            }
            else{
               let navController = UINavigationController(rootViewController: objNewRequestVC)
                UIApplication.shared.visibleViewController!.present(navController, animated: true, completion: nil)
            }
        }
        else if withLaunchRequestModel.trip?.status == TRIP_STATUS.ENDED.rawValue && !(withLaunchRequestModel.trip?.paid)!
        {
            let tipVC = UIViewController.getViewController(TipViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            //Prepare tip screen model
            let driverName = (withLaunchRequestModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")
            let driverImageId = (withLaunchRequestModel.trip?.driver?.profileImageFileId ?? "")
            let routeImageId = (withLaunchRequestModel.trip?.routeImageId ?? "")
            
            let pushNotificationModel = PushNotificationObjectModel(JSON: [:])
            pushNotificationModel?.trip = withLaunchRequestModel.trip
            
            let amountPayable = Double(withLaunchRequestModel.trip?.payment?.amountPayable ?? 0)
            let compTip = Double(withLaunchRequestModel.trip?.payment?.compTip ?? 0)
            let requestModel = TipViewRequestModel(driverName: driverName, driverImageId: driverImageId, tripId: (withLaunchRequestModel.trip?.id),tripFare: amountPayable-compTip,routeImageId: routeImageId,pushObjModel:pushNotificationModel)
            tipVC.bind(withViewModel: requestModel)
            
            if !(UIApplication.shared.visibleViewController?.isKind(of: TipViewController.self))!{
                self.present(tipVC, animated: true, completion: nil)
            }
        }
        else if withLaunchRequestModel.trip?.status == TRIP_STATUS.ENDED.rawValue && withLaunchRequestModel.trip?.tripRating == nil
        {
            let feedbackVC = UIViewController.getViewController(FeedbackViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            
            //Prepare feedback screen model
            let driverName = (withLaunchRequestModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")
            let driverImageId = (withLaunchRequestModel.trip?.driver?.profileImageFileId ?? "")
            let pushNotificationModel = PushNotificationObjectModel(JSON: [:])
            pushNotificationModel?.trip = withLaunchRequestModel.trip
            
            let requestModel = FeedbackViewRequestModel(driverName: driverName, driverImageId: driverImageId, tripId: (withLaunchRequestModel.trip?.id ?? ""), rate: 5, reivew: nil,pushObjModel:pushNotificationModel)
            
            feedbackVC.bind(withRequestModel: requestModel)
            feedbackVC.delegate = self
            feedbackVC.modalPresentationStyle = .overCurrentContext
            
            if !(UIApplication.shared.visibleViewController?.isKind(of: FeedbackViewController.self))!{
                self.present(feedbackVC, animated: true, completion: {})
            }
            
            self.hideMenuButton()
        }
    }
}

//MARK: Map View Delegate
extension RiderHomeViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.sourceInfoWindow.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        self.showSourceInfoViewForLocationPin()
    }
    
    func showSourceInfoViewForLocationPin(){
        if let point = self.pickLocationPin?.center, !pickLocationPin!.isHidden{
            self.addSourceInfoWindow(withPositionPoints: point)
            let location = self.viewMapGoogle.projection.coordinate(for: point)
            self.showSourcePickerPinWithCoordinates(coordinate: location)
            self.checkSocketConnection(lat: location.latitude, long: location.longitude)
        }
    }
    
    func getInfoNibForAddress()->PickSourceInfoWindow{
        let nib = Bundle.main.loadNibNamed("PickSourceInfoWindow", owner: self, options: nil)?.first as! PickSourceInfoWindow
        return nib
    }
    
    func showSourcePickerPinWithCoordinates(coordinate: CLLocationCoordinate2D){
        
        if ReachabilityManager.shared.isNetworkAvailable
        {
            GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (response: GMSReverseGeocodeResponse?, error: Error?) in
                
                self.updateSourceAddressInfoWindow(withResponseData: response)
                
            })
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
        }
        
    }
    
    func updateSourceAddressInfoWindow(withResponseData data: GMSReverseGeocodeResponse?){
        if let _ = self.pickLocationPin, !pickLocationPin!.isHidden{
            
            let titlePickedSource = self.getFormattedSourceLocationAddressTitle(WithGeocodingResponse: data)
            self.sourceInfoWindow.bind(withLocation: titlePickedSource)
            
            if let _ = data?.firstResult(){
                let firstResult = data?.firstResult()
                self.selectedSourceAddress = SelectedPlaceModel(title: firstResult!.lines!.joined(separator: ", "), coordinate: firstResult!.coordinate)
            }
            else{
                self.selectedSourceAddress = nil
            }
            
        }
    }
    
    func addSourceInfoWindow(withPositionPoints point: CGPoint){
        self.sourceInfoWindow.center = CGPoint(x: (self.pickLocationPin?.center.x)!, y: (self.pickLocationPin?.center.y)!-(self.pickLocationPin?.frame.size.height)!-12)
        self.viewMapGoogle.addSubview(self.sourceInfoWindow)
        self.sourceInfoWindow.fetchingAddress()
    }
    
    func getFormattedSourceLocationAddressTitle(WithGeocodingResponse response: GMSReverseGeocodeResponse?)->String{
        var strSourceAddress = ""
        
        if var firstResult = response?.firstResult()?.lines{
            
            //Remove Empty Strings from result
            for (index, value) in firstResult.enumerated(){
                if value.isEmpty{
                    firstResult.remove(at: index)
                }
            }
            
            strSourceAddress = firstResult.joined(separator: ", ")
        }
        
        if strSourceAddress.isEmpty{
            strSourceAddress = AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_LOCATION_FOUND
        }
        
        return strSourceAddress
    }
}

extension RiderHomeViewController: FeedbackScreenDelegate{
    func feedbackScreenDidDismiss(){
        self.addNavigationMenuButton()
    }
}

extension RiderHomeViewController: AddPaymentScreenViewDelgate{
    
    func setUpStripeCustomer()
    {
        if let _ = LoginResponseModel.retriveUser()?.stripeProfile?.stripeCustomerId
        {
            //Stripe Id Already Available- Update Stripe Customer
            self.updateStripeCustomer()
        }
        else
        {
            self.presenterAddPayment.sendCustomerIdFecthRequest(withData: AddPaymentScreenRequestModel())
        }
    }
    
    func updateStripeCustomer()->Void{
        self.showLoader()
    
        let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        customerContext.retrieveCustomer { (customer, error) in
            self.hideLoader()
            StripePaymentManager.shared.customer = customer
            if let defaultSource = customer?.defaultSource as? STPSource,defaultSource.type == .card
            {
                 StripePaymentManager.shared.isCardAdded = true
            }
            else
            {
                 StripePaymentManager.shared.isCardAdded = false
            }
           
        }
        
    }

    func cardDeleteSuccessfully(withResponseModel stripeResponseModel:DeleteCardResponseModel){
    }
    
    func customerIdFectchedSuccessfully(withResponseModel stripeResponseModel:CustomerIdResponseModel){
        //Save Stripe Customer Id
        AppDelegate.sharedInstance.userInformation.stripeProfile?.stripeCustomerId = stripeResponseModel.stripeCustomerId
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        //Update Stripe Customer
        self.updateStripeCustomer()
    }
}

extension RiderHomeViewController{
    func initScreenUI(){
        self.initFilterView()
        self.viewFilterOptions.setCornerCircular(6.0)
        
        if AppUtility.isUserLogin(){
            self.lblUserName.text = AppDelegate.sharedInstance.userInformation.firstName?.capitalizeWordsInSentence()
        }
        else{
            self.lblUserName.text = AppConstants.ScreenSpecificConstant.SideMenuScreen.ANONYMOUS_USER_HOME_TITLE.capitalizeWordsInSentence()
        }
        
        self.updateNumberOfAvailableCabs()
        self.initalizeViewWithSavedAddresses()
    }
    
    func initFilterView(){
        //Title for filters
        self.lblLowestPrice.text = AppConstants.ScreenSpecificConstant.RiderHomeScreen.LOWEST_PRICE_TITLE
        self.lblClosestRide.text = AppConstants.ScreenSpecificConstant.RiderHomeScreen.CLOSEST_RIDE_TITLE
        self.lblMostSeats.text = AppConstants.ScreenSpecificConstant.RiderHomeScreen.MOST_SEATS_TITLE
        
        //Price or ride time filter selection
        if UserRideDetail.shared.sortingType == AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_PRICE{
            self.toggleFilter(sender: self.viewLowestPrice)
        }
        else{
            self.toggleFilter(sender: self.viewClosestRide)
        }
        
        //Car with most seats filter selection
        if UserRideDetail.shared.isMostSeatAvailableSelected{
            self.setFilterSelected(withFilter: self.viewMostSeats)
        }
        else{
            self.setFilterUnselected(withFilter: self.viewMostSeats)
        }
    }
    
    func updateNumberOfAvailableCabs(){
        if oldValue == 1 {
            self.lblNumberOfRidesAvailable.text = "\(oldValue) ride"
        }
        else{
            self.lblNumberOfRidesAvailable.text = "\(oldValue) rides"
        }
    }
    
    func openAddressShortcuts(){
        self._iLayoutConstraintClosedAddressSelectionView.isActive = false
        self.hideFilterOptionViewWithAnimation()
        
        UIView.animate(withDuration: 0.5 , delay: 0, options: .curveEaseOut, animations: {
            self.viewScreenContent.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.viewPlaces.bringSubview(toFront: self.imgViewCloseAddressView)
            self.imgViewCloseAddressView.isHidden = false
        })
    }
    
    func closeAddressShortcuts(){
        self._iLayoutConstraintClosedAddressSelectionView.isActive = true
        
        UIView.animate(withDuration: 0.5 , delay: 0, options: .curveEaseOut, animations: {
            self.viewScreenContent.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.imgViewCloseAddressView.isHidden = true
            self.viewPlaces.sendSubview(toBack: self.imgViewCloseAddressView)
            self.showFilterOptionViewWithAnimation()
        })
    }
    
    func showFilterOptionViewWithAnimation(){
        UIView.animate(withDuration: 0.2 , delay: 0, options: .curveEaseOut, animations: {
            self.viewFilterOptions.alpha = 1.0
        }, completion: { _ in
        })
    }
    
    func hideFilterOptionViewWithAnimation(){
        UIView.animate(withDuration: 0.0 , delay: 0, options: .curveEaseOut, animations: {
            self.viewFilterOptions.alpha = 0.0
        }, completion: { _ in
        })
    }
    
    func toggleFilter(sender: UIView){
        switch sender {
        case self.viewLowestPrice:
            UserRideDetail.shared.sortingType = AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_PRICE
            self.setFilterSelected(withFilter: self.viewLowestPrice)
            self.setFilterUnselected(withFilter: self.viewClosestRide)
        case self.viewClosestRide:
            UserRideDetail.shared.sortingType = AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_DISTANCE
            self.setFilterSelected(withFilter: self.viewClosestRide)
            self.setFilterUnselected(withFilter: self.viewLowestPrice)
        default:
            break
        }
    }
    
    func setFilterSelected(withFilter sender: UIView){
        let color = UIColor(red: 198/255.0, green: 66/255.0, blue: 66/255.0, alpha: 1.0)
        sender.backgroundColor = color
    }
    
    func setFilterUnselected(withFilter sender: UIView){
        let color = UIColor.appDarkThemeColor()
        sender.backgroundColor = color
    }
    
    func showSearchPlaceScreenWithTitle(title: String){
        
        //Prepare and show search screen
        let vcSearch = UIViewController.getViewController(GooglePlaceSearchViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        vcSearch.bind(titleSearchHeader: title)
        vcSearch.modalPresentationStyle = .overCurrentContext
        vcSearch.delegate = self
        self.present(vcSearch, animated: true, completion: nil)
        
        //Hide Menu Button When Showing Search screen
        self.hideMenuButton()
    }
    
    func getSourceAddressFromCurrentLocationCoordinate(coordinate: CLLocationCoordinate2D){
        self.showLoader()
        
        if ReachabilityManager.shared.isNetworkAvailable
        {
            GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (response: GMSReverseGeocodeResponse?, error: Error?) in
                
                self.hideLoader()
                self.updateSourceAddress(withResponseData: response)
                
            })
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
        }
    }
    
    func updateSourceAddress(withResponseData data: GMSReverseGeocodeResponse?){
        if let _ = data{
            
            if let _ = data?.firstResult(){
                let firstResult = data?.firstResult()
                self.selectedSourceAddress = SelectedPlaceModel(title: firstResult!.lines!.joined(separator: ", "), coordinate: firstResult!.coordinate)
            }
            else{
                self.selectedSourceAddress = nil
            }
        }
        
        if let _ = self.selectedDestinationAddress{
            self.moveToCabSelectionScreen()
        }
    }
    
    func handleAddAdressViewLayouts(){
        if AppUtility.isUserLogin(){

            if let _ = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
                self.viewAddHome.isHidden = true
                self._iLayoutConstraintHomeAddressTrailingSpaceToView.isActive = true
            }
            else{
                self.viewAddHome.isHidden = false
                self._iLayoutConstraintHomeAddressTrailingSpaceToView.isActive = false
            }
            
            
            if let _ = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
                self.viewAddWork.isHidden = true
                self._iLayoutConstraintWorkAddressTrailingSpaceToView.isActive = true
            }
            else{
                self.viewAddWork.isHidden = false
                self._iLayoutConstraintWorkAddressTrailingSpaceToView.isActive = false
            }
            
            if let _ = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.recentlyVisited{
                self._iLayoutConstaintHeightForLastVisited.constant = 60
                self.viewLastVisit.isHidden = false
            }
            else{
                //hide last visited
                self._iLayoutConstaintHeightForLastVisited.constant = 0
                self.viewLastVisit.isHidden = true
            }
        }
    }
    
    func initalizeViewWithSavedAddresses(){
        
        if AppUtility.isUserLogin(){
            
            //Home Address
            if let home = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
                self.lblHomeAddress.text = home.address!
            }
            
            //Work Address
            if let work = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
                
                self.lblWorkAddress.text = work.address!
            }
            
            //Last Visited
            if let lastVisited = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.recentlyVisited{
                //show last visited
                self.lblLastVisitedAddress.text = lastVisited.address!
            }
        }
        
    }
    
    func getSourceAddressAndMoveToCabSelectionScreen(){
        if let coordinates = self.userCurrentLocationCoordinate{
            self.getSourceAddressFromCurrentLocationCoordinate(coordinate: coordinates)
        }
        else{
            AppUtility.presentToastWithMessage("Please select a valid source address")
        }
    }
}

extension RiderHomeViewController: GooglePlaceSearchDelegate{
    func placeSelected(withDetail place: SelectedPlaceModel){
        self.selectedDestinationAddress = place
        self.lblDestination.text = place.title
        
        self.performAddressSelection(withAddressData: place)
    }
    
    func placeSearchScreenDidDismiss(){
        self.addNavigationMenuButton()
    }
}
