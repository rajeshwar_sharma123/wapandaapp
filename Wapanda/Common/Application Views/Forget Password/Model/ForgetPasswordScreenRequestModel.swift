//
//  ForgetPasswordScreenRequestModel.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
struct ForgetPasswordScreenRequestModel {
    
    var email                 : String!
    var phoneNumber                 : String!
    var countryCodeExt                 : String!
}
