//
//  DriverInvoiceViewModel.swift
//  Wapanda
//
//  Created by Vipul Sharma on 03/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//


import Foundation

struct DriverInvoiceViewModel {

    // MARK: Properties

    public var routeImageId: String = ""
    public var toAddress: String  = ""
    public var fromAddress: String = ""
    public var status: String = ""
    public var id: String = ""
    public var agreedPrice: Double = 0.0

    public var distanceTravelled: Int = 0
    public var endTime: String = ""
    public var startTime: String = ""
    public var duration: Int = 0
    public var stopAddress: String = ""
    public var payment: Payment?


}
