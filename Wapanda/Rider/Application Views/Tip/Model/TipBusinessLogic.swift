
//Note :- This class contains Tip Buisness Logic

class TipBusinessLogic {
    
    
    deinit {
        print("TipBusinessLogic deinit")
    }

    /**
     This method is used for perform feedback With Valid Inputs constructed into a TipRequestModel
     
     - parameter inputData: Contains info for Tip
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performTip(withTipRequestModel feedbackRequestModel: TipRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForfeedback()
        TipApiRequest().makeAPIRequest(withReqFormData: feedbackRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForfeedback() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_TRIP_STATUS, message  : AppConstants.ErrorMessages.INVALID_TRIP_STATUS)
        errorResolver.registerErrorCode(ErrorCodes.PAYMENT_FAILED, message  : AppConstants.ErrorMessages.PAYMENT_FAILED)

        return errorResolver
    }
}
