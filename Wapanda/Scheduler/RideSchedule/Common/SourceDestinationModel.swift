import UIKit

struct SelectedCabModel {
    var name: String = ""
    var id: String = ""
    var icon: String = ""
    var estPrice: Double = 0
}

class SourceDestinationModel {
    
    var source: (lat: Double, long: Double, address: String)
    var destination: (lat: Double, long: Double, address: String)
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        source = builder.source
        destination = builder.destination
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder {
        
        //MARK:- Builder class Properties
        var source: (lat: Double, long: Double, address: String) = (lat: 0.0, long: 0.0, address: "")
        var destination: (lat: Double, long: Double, address: String) = (lat: 0.0, long: 0.0, address: "")

        /**
         This method is used for setting latitude, longitude and address of source
         - parameter lat: latitude of source
         - parameter long: longitude of source
         - parameter address: latitude of source
         
         - returns: returning Builder Object
         */
        
        func setSource(lat: Double, long: Double, address: String) -> Builder{
            source.lat = lat
            source.long = long
            source.address = address
            return self
        }
        
        /**
         This method is used for setting latitude, longitude and address of destination
         
         - parameter lat: latitude of destination
         - parameter long: longitude of destination
         - parameter address: latitude of destination

         - returns: returning Builder Object
         */
        func setDestination(lat: Double, long: Double, address: String) -> Builder{
            destination.lat = lat
            destination.long = long
            destination.address = address
            return self
        }
        
        func build() -> SourceDestinationModel{
            return SourceDestinationModel(builderObject: self)
        }
    }
}
