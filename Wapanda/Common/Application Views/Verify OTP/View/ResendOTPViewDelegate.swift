
//Notes:-

protocol ResendOTPViewDelegate:BaseViewProtocol {
    func resendEmailOTPSuccessfully() -> Void
    func showToastWithMessage(_ message:String) -> Void
}
