

//Notes:- This class is used for constructing DriverInfo Service Request Model

class DriverInfoRequestModel {
    
    //MARK:- DriverInfoRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var driverId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.driverId = builder.driverId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var driverId: String!
        
        /**
         This method is used for setting first Name
         
         - parameter userName: String parameter that is going to be set on first Name
         
         - returns: returning Builder Object
         */
        func setDriverId(_ id:String) -> Builder{
            self.driverId = id
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of DriverInfoRequestModel
         and provide DriverInfoForm1ViewViewRequestModel object.
         
         -returns : DriverInfoRequestModel
         */
        func build()->DriverInfoRequestModel{
            return DriverInfoRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting DriverInfo end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.DRIVER_INFO, self.driverId)
    }
    
}
