//
//  Localization.swift
//
//  Created by Daffolapmac on 02/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Localization: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let time = "time"
    static let moneyview = "moneyview"
    static let geoloc = "geoloc"
  }

  // MARK: Properties
  public var time: String?
  public var moneyview: Moneyview?
  public var geoloc: Geoloc?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    time <- map[SerializationKeys.time]
    moneyview <- map[SerializationKeys.moneyview]
    geoloc <- map[SerializationKeys.geoloc]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = time { dictionary[SerializationKeys.time] = value }
    if let value = moneyview { dictionary[SerializationKeys.moneyview] = value.dictionaryRepresentation() }
    if let value = geoloc { dictionary[SerializationKeys.geoloc] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.time = aDecoder.decodeObject(forKey: SerializationKeys.time) as? String
    self.moneyview = aDecoder.decodeObject(forKey: SerializationKeys.moneyview) as? Moneyview
    self.geoloc = aDecoder.decodeObject(forKey: SerializationKeys.geoloc) as? Geoloc
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(time, forKey: SerializationKeys.time)
    aCoder.encode(moneyview, forKey: SerializationKeys.moneyview)
    aCoder.encode(geoloc, forKey: SerializationKeys.geoloc)
  }

}
