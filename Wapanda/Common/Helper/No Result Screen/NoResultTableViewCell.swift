//
//  NoResultTableViewCell.swift
//  Wapanda
//
//  Created by daffomac-31 on 12/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class NoResultTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(title: String){
        self.lblMessage.text = title
    }
}
