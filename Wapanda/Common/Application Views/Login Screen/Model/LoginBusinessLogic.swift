//
//  LoginBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class LoginBusinessLogic {
    
    init(){
        print("LoginBusinessLogic init \(self)")
    }
    deinit {
        print("LoginBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform Login With Valid Inputs(Email , password) constructed into a LoginRequestModel
     
     - parameter inputData: Contains info for Sign In
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performLogin(withLoginRequestModel LoginRequestModel: LoginRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForLogin()
        LoginAPIRequest().makeAPIRequest(withReqFormData: LoginRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForLogin() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_LOGIN, message: AppConstants.ErrorMessages.INVALID_LOGIN_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.PHONE_NOT_VERIFIED, message: AppConstants.ErrorMessages.PHONE_NOT_VERIFIED)
        errorResolver.registerErrorCode(ErrorCodes.EMAIL_NOT_VERIFIED, message: AppConstants.ErrorMessages.EMAIL_NOT_VERIFIED)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message: AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        
        
        return errorResolver
    }
}
