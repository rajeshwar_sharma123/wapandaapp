
import UIKit

protocol UserRideDetailProtocol {
    func sourceDestinationUpdated()
    func selectedCabChanged()
}

class UserRideDetail {

    static let shared = UserRideDetail()
    private init() { }
    var delegates: [UserRideDetailProtocol] = [UserRideDetailProtocol]()
    var sourceDestination: SourceDestinationModel? {
        didSet{
            
            for delegate in delegates {
                delegate.sourceDestinationUpdated()
            }
        }
    }
    
    var selectedCabs = [SelectedCabModel]() {
        didSet{
            for delegate in delegates {
                delegate.selectedCabChanged()
            }
        }
    }

    var sortingType = AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_PRICE
    var isMostSeatAvailableSelected = false
}
