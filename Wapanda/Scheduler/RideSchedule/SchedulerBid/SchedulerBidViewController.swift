import UIKit
import CoreLocation
class SchedulerBidViewController: BaseViewController, SorceAndDestinationViewDelegate, SchedulerBidViewControllerDelegate, SelectCabDelegate, UserRideDetailProtocol, SelectCabViewControllerDelegate {
   
    
    
    @IBOutlet weak var noCabsAvailableView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var sourceAndDestinationView: SourceAndDestinationView!
    @IBOutlet weak var viewBoundary: UIView!
    @IBOutlet weak var lblRangeForThisRide: UILabel!
    @IBOutlet weak var lblRange: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var selectCabView: SelectCabView!
    @IBOutlet weak var lblRangeLowHigh: UILabel!
    
    
    
    fileprivate var selectedCabs: [RideList] = []
    private var sortingTypeSelected: String = AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_PRICE
    fileprivate var rideDetail: [VehicleListResponseModel]?
   fileprivate var carTyepeDetail: VehicleListResponseModel?
    fileprivate var presenter: SchedulerBidPresenter?
    fileprivate var prices = [Double]()
    fileprivate let FARE_INCREMENT = 0.25
    fileprivate var sourceDestination : SourceDestinationModel?
    fileprivate var selectedFare: Double = 0.0
    fileprivate var selectedSourceAddress: SelectedPlaceModel!
    fileprivate var selectedDestinationAddress: SelectedPlaceModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        
        customizeNavigationBarWithTitle(navigationTitle: "", color: UIColor.schedulerNavColor() , isTranslucent: false)
        customizeNavigationBackButton()
        lblRangeForThisRide.text = "RANGE FOR THIS RIDE".uppercased()
        let borderColor = UIColor(red: 194/255.0, green:  201/255.0, blue:  255/255.0, alpha:0.5)
        viewBoundary.setBorderToView(withWidth: 1.2, color: borderColor)
        viewBoundary.setCornerCircular(6)
        sourceAndDestinationView.delegate = self
       // getRideDetail()

        selectCabView.delegate = self
        presenter = SchedulerBidPresenter(delegate: self)
        setInitialSourceAndDestinationTitle()
        if let sourceDestination = UserRideDetail.shared.sourceDestination{
            presenter?.getAllVehicleTypes(latFrom: selectedSourceAddress.coordinate.latitude,
                                         lngFrom: selectedSourceAddress.coordinate.longitude,
                                         latTo: selectedDestinationAddress.coordinate.latitude,
                                         lngTo: selectedDestinationAddress.coordinate.longitude)
        }
        removeSelectorFromPickerView()
        addLocationManagerDelegate()
        sourceDestinationUpdated()
    }
    
    func addLocationManagerDelegate() {
        
        let delegates = UserRideDetail.shared.delegates
        for (index, delegate) in delegates.enumerated() {
            
            if delegate is SchedulerBidViewController {
                UserRideDetail.shared.delegates.remove(at: index)
                break
            }
        }
        UserRideDetail.shared.delegates.append(self)
    }
    
    func selectedCabChanged() {
        guard let selectedRideInfo = UserRideDetail.shared.selectedCabs.first else { return }
        selectCabView.bindData(cabInfo: selectedRideInfo)
    }
    
    func sourceDestinationUpdated() {
        
        guard let sourceDestination = UserRideDetail.shared.sourceDestination else { return }
        sourceAndDestinationView.bind(source: sourceDestination.source.address, destination: sourceDestination.destination.address)
    }
    func setInitialSourceAndDestinationTitle(){
        if let sourceDestination = UserRideDetail.shared.sourceDestination {            
            selectedSourceAddress = SelectedPlaceModel(title: sourceDestination.source.address, coordinate: CLLocationCoordinate2D(latitude: sourceDestination.source.lat, longitude: sourceDestination.source.long))
            
            selectedDestinationAddress = SelectedPlaceModel(title: sourceDestination.destination.address, coordinate: CLLocationCoordinate2D(latitude: sourceDestination.destination.lat, longitude: sourceDestination.destination.long))
        }
    }
    func getRideDetail() {
        guard let sourceDestination = UserRideDetail.shared.sourceDestination else { return }
        
        if let selectedCab = UserRideDetail.shared.selectedCabs.first {
            presenter?.getCabsList(location: sourceDestination, selectedCab: [selectedCab.id])
        }
        else {
            presenter?.getCabsList(location: sourceDestination)
        }
    }
    
    func updateList(cabType type: String) {
        //getRideDetail()
         self.populateDataOnPicker((UserRideDetail.shared.selectedCabs.first?.name)!)
    }
    
    func btnEditSourceOrDestinationClick() {
        navigateToSeletCabViewController(editAddress: true)
    }
    
    func setLocation(sourceDestination: SourceDestinationModel) {
        self.sourceDestination = sourceDestination
    }
    
    func renderData(data rideData: RideList) {
        
//        noCabsAvailableView.isHidden = true
//        rideDetail = rideData
//
//        guard let selectedRideInfo = UserRideDetail.shared.selectedCabs.first else { return }
//        selectCabView.bindData(cabInfo: selectedRideInfo)
//
//        guard let fairValues = rideDetail?.fairValues else { return }
//
//        let minFare = AppUtility.getFormattedPriceString(withPrice: fairValues.minFare)
//        let maxFare = String(format: "%.2f", fairValues.maxFare)
//        lblRange.text = minFare + "-" + maxFare
//        prices.removeAll()
//
//        var fare = fairValues.minFare
//
//        while (fare <= fairValues.maxFare) {
//            prices.append(fare)
//            fare += FARE_INCREMENT
//        }
//        pickerView.reloadAllComponents()
//
//        let midIndex = Int(prices.count/2)
//        pickerView.selectRow(midIndex, inComponent: 0, animated: false)
//        setLowHighBidRange(price: prices[midIndex])
    }
    
    func noCabsAvailable() {
        noCabsAvailableView.isHidden = false
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        
        if alertMessage == AppConstants.ErrorMessages.AREA_NOT_COVERED || alertMessage == AppConstants.ErrorMessages.STATE_NOT_COVERED {
            noCabsAvailable()
            return
        }
        
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
        
       
    }
    
    private func removeSelectorFromPickerView()
    {
        pickerView.subviews.forEach({
            
            $0.isHidden = $0.frame.height < 2.0
        })
    }
    
    fileprivate func setLowHighBidRange(price: Double) {
        
        guard let fareValues = self.carTyepeDetail?.fairValues else { return }
        
        if price <= fareValues.fairPriceLow {
            lblRangeLowHigh.text = "Your Bid Looks Low"
        }
        else if price >= fareValues.fairPriceHigh {
            lblRangeLowHigh.text = "Your Bid Looks High"
        }
        else{
            lblRangeLowHigh.text = "Your Bid Looks Good"
        }
    }
    
    @IBAction func continueClicked(_ sender: Any) {
        if let vc = UIViewController.getViewControllerFromSchedulingStoryboard(viewController: ScheduleRideTimeViewController.self) {
            vc.selectedFare = selectedFare
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showLoader() {
        self.showLoader(self)
    }
    
    func hideLoader() {
        self.hideLoader(self)
    }
    
    func selectCabClicked() {
        navigateToSeletCabViewController(editAddress: false)
    }
    
    func navigateToSeletCabViewController(editAddress: Bool) {
        if let vc = UIViewController.getViewControllerFromSchedulingStoryboard(viewController: SelectCabViewController.self) {
            vc.delegate = self

            if let estPrice = carTyepeDetail?.estPrice {
                vc.selectedCabPrice = estPrice
            }
            
            if let cabName = carTyepeDetail?.carType{
                let cabType = Type(JSON: [:])
                cabType?.vehicleType = cabName
                vc.selectedCab = cabType
                vc.editAddress = editAddress
            }
            present(vc, animated: true, completion: nil)
        }
    }
}

extension SchedulerBidViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: picker view datasource methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        pickerView.subviews.forEach({
            $0.isHidden = $0.frame.height < 2.0
        })
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.prices.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard row > 0 else { return}
        setLowHighBidRange(price: self.prices[row])
        selectedFare = prices[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel;
        
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            pickerLabel?.frame.size.height = 50
            pickerLabel?.textColor = UIColor.white
            pickerLabel?.font = UIFont(name: "Titillium-Semibold", size: 32)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        
        pickerLabel?.text = "$\(prices[row])"
        
        return pickerLabel!;
    }
    func refreshList(list: [VehicleListResponseModel]) {
        noCabsAvailableView.isHidden = true
        rideDetail = list
        rideDetail?.insert(createAnyCarTypeModel(list),at:0)
        guard let selectedRideInfo = UserRideDetail.shared.selectedCabs.first else { return }
        selectCabView.bindData(cabInfo: selectedRideInfo)
        self.populateDataOnPicker((UserRideDetail.shared.selectedCabs.first?.name)!)
     
    }
    func createAnyCarTypeModel(_ list: [VehicleListResponseModel])-> VehicleListResponseModel
    {
        let anyType =  VehicleListResponseModel(JSON:[:])
        anyType?.carType = "Any"
        anyType?.estPrice = 0.0
        anyType?.fairValues = FairValuesModel(JSON:[:])
        anyType?.fairValues?.fairPriceLow = list.flatMap({ (obj) -> Double? in obj.fairValues?.fairPriceLow }).min()!
        anyType?.fairValues?.fairPriceHigh = list.flatMap({ (obj) -> Double? in obj.fairValues?.fairPriceHigh }).max()!
        anyType?.fairValues?.maxFare = list.flatMap({ (obj) -> Double? in obj.fairValues?.maxFare }).max()!
        anyType?.fairValues?.minFare = list.flatMap({ (obj) -> Double? in obj.fairValues?.minFare }).min()!
        return anyType!
    }
    func getMaxValue(_ carPrices:[IndexArray],_ userPrice: Float)->Float
    {
        var priceArray = carPrices.flatMap { (obj) -> Float? in
            return obj.indexrate
        }
        priceArray.append(userPrice)
        return priceArray.max()!
    }
    func populateDataOnPicker(_ carType:String)
    {
        let standardFairValuesArray = rideDetail?.filter({ (vehicleObj) -> Bool in
            if vehicleObj.carType == carType
            { return true}
            return false
        })
        guard let standardFairValues = standardFairValuesArray,standardFairValues.count > 0 else { return}
        guard let fairValues = standardFairValues[0].fairValues else { return }
        self.carTyepeDetail = standardFairValues[0]
        let minFare = AppUtility.getFormattedPriceString(withPrice: fairValues.minFare)
        let maxFare = String(format: "%.2f", fairValues.maxFare)
        lblRange.text = minFare + "-" + maxFare
        prices.removeAll()
        
        var fare = fairValues.minFare
        
        while (fare <= fairValues.maxFare) {
            prices.append(fare)
            fare += FARE_INCREMENT
        }
        pickerView.reloadAllComponents()
        
        let midIndex = Int(prices.count/2)
        selectedFare = prices[midIndex]
        pickerView.selectRow(midIndex, inComponent: 0, animated: false)
        setLowHighBidRange(price: prices[midIndex])
        
    }
    
//    func pickerView(
    
//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString?{
//
//        let attString = NSAttributedString(string: "$\(prices[row])", attributes: [NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 50)])
//        //            UIFont(name: "Titillium-Semibold", size: 50)!])
//        return attString
//    }
}
