

import UIKit

class CarTypeTableViewCell: UITableViewCell {
    @IBOutlet weak var imgViewCheckMark: UIImageView!
    @IBOutlet weak var viewSelected: UIView!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var imgViewCar: UIImageView!
    @IBOutlet weak var lblPriceRange: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    /// Binds data with cell
    ///
    /// - Parameters:
    ///   - type: string value as car type i.e. standard etc.
    ///   - priceRange: string value denoting price range i.e. $12-14
    ///   - imageId: string value as car image id
    ///   - time: formatted time string in minutes
    func bind(withCarType type: String, priceRange: String, imageId: String, time: String){
        self.imgViewCar.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: imageId))!, placeholderImage: #imageLiteral(resourceName: "ic_standard_car"))
        self.lblCarType.text = type
        self.lblPriceRange.text = priceRange
        self.lblTime.text = time
        self.imgViewCar.getImageViewWithImageTintColor(color: UIColor.appPlaceholderColor())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        if selected{
            self.viewSelected.backgroundColor = UIColor.appThemeColor()
            self.contentView.backgroundColor = UIColor.appThemeColor()
            self.imgViewCheckMark.isHidden = false
        }
        else{
            self.viewSelected.backgroundColor = UIColor.appDarkThemeColor()
            self.contentView.backgroundColor = UIColor.appDarkThemeColor()
            self.imgViewCheckMark.isHidden = true
        }
    }
    
}
