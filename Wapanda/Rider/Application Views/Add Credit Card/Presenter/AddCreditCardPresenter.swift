//
//  AddCreditCardPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
import Stripe
class AddCreditCardPresenter: ResponseCallback{
    
    //MARK:- AddCreditCardPresenter local properties
    private weak var addCreditCardViewDelegate             : AddCreditCardScreenViewDelgate?
    private lazy var addCreditCardBusinessLogic         : AddCreditCardBusinessLogic = AddCreditCardBusinessLogic()
    private weak var textFieldValidationDelegate   : CardTextFieldValidationDelegate?

    //MARK:- Constructor
    init(delegate responseDelegate:AddCreditCardScreenViewDelgate,textFieldValidationDelegate: CardTextFieldValidationDelegate) {
        self.addCreditCardViewDelegate = responseDelegate
        self.textFieldValidationDelegate = textFieldValidationDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.addCreditCardViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error : ErrorModel){
        self.addCreditCardViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.addCreditCardViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendCustomerIdFetchRequest() -> Void{
       
    self.addCreditCardViewDelegate?.showLoader()
        
        
    }
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func validateAddCardRequest(withData addCreditCardRequestModel:AddCreditCardScreenRequestModel) -> Bool{
        guard self.validateInput(withData: addCreditCardRequestModel) else{
            return false
        }
        return true
       // self.addCreditCardViewDelegate?.showLoader()
        
    }
    
    func validateInput(withData addCardRequestModel:AddCreditCardScreenRequestModel) -> Bool {
        var isValid = true
        var isCardNumberEmpty = true
        var isCVVNumberEmpty = true
        if addCardRequestModel.cardNumber == AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING  {
            self.textFieldValidationDelegate?.showErrorMessage(withMessage: "Please enter card number", forTextFields: .CARD_NUMBER)
            isValid = false
        }
        else { isCardNumberEmpty = false }
        if addCardRequestModel.month == "" {
            self.textFieldValidationDelegate?.showErrorMessage(withMessage: "Please select expiry month.", forTextFields: .MONTH)
            isValid = false
        }
        if addCardRequestModel.year == "" {
            self.textFieldValidationDelegate?.showErrorMessage(withMessage: "Please select expiry year.", forTextFields: .YEAR)
            isValid = false
        }
        if addCardRequestModel.cvv == ""  {
            self.textFieldValidationDelegate?.showErrorMessage(withMessage: "Please enter CVV number", forTextFields: .CVV)
            isValid = false
        }
       else { isCVVNumberEmpty = false }
       
        let cardNumberState = STPCardValidator.validationState(forNumber: addCardRequestModel.cardNumber, validatingCardBrand: true)
        if  cardNumberState != .valid && !isCardNumberEmpty
        {
            self.textFieldValidationDelegate?.showErrorMessage(withMessage: "Please enter a valid card number", forTextFields: .CARD_NUMBER)
            isValid = false
        }
        
        let cvvNumberState = STPCardValidator.validationState(forCVC: addCardRequestModel.cvv, cardBrand: addCardRequestModel.brand)
        if cvvNumberState != .valid && !isCVVNumberEmpty
        {
            self.textFieldValidationDelegate?.showErrorMessage(withMessage: "Please enter a valid CVV number", forTextFields: .CVV)
            isValid = false
        }

        return isValid
    }
}
