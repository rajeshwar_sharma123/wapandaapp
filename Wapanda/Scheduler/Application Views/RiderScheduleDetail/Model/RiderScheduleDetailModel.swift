//
//  RiderScheduleDetailModel.swift
//  Wapanda
//
//  Created by Daffodil on 28/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
struct RiderScheduleDetailModel {
    var cancelledBy             : String!
    var cancelReason            : String!
    var tripId                  : String!
}

