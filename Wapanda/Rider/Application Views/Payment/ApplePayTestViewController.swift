//
//  ApplePayTestViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 30/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Stripe

class ApplePayTestViewController: BaseViewController {
    
    var prsenterProcessPayment: ProcessPaymentViewPresenter!
    var appplePayHandler = ApplePayHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appplePayHandler.delegate = self
        prsenterProcessPayment = ProcessPaymentViewPresenter(delegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCloseClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnApplePayClick(_ sender: Any) {
        self.appplePayHandler.handleApplePayRequestWithItem(items: [ApplePayItemModel(label: "Ride", price: 123.44), ApplePayItemModel(label: "Tip", price: 12.34)])
    }
}

//MARK: Base Protocol
extension ApplePayTestViewController{
    func showLoader() {
        super.showLoader(self)
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        self.appplePayHandler.onPaymentProcessingError()
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}


// MARK: - ProcessPaymentViewDelegate
extension ApplePayTestViewController: ProcessPaymentViewDelegate{
    func addTipSuccess(withResponseModel data: AddTipResponseModel) {
        
    }

    func processPaymentSuccess(withResponseModel data: ProcessPaymentResponseModel){
        self.appplePayHandler.onPaymentProcessingSuccess()
    }
}


// MARK: - ApplePayDelegate
extension ApplePayTestViewController: ApplePayDelegate{
    func processPaymentWithSourceId(id: String) {
        
    }
    
    func applePayPaymentSuccess(){
        
    }
    
    func applePayPaymentFailure(){
        
    }
}
