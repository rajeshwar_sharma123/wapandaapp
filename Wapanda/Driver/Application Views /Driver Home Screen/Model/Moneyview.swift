//
//  Moneyview.swift
//
//  Created by Daffolapmac on 02/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Moneyview: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isocode = "isocode"
    static let increment = "increment"
  }

  // MARK: Properties
  public var isocode: String?
  public var increment: Float?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    isocode <- map[SerializationKeys.isocode]
    increment <- map[SerializationKeys.increment]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = isocode { dictionary[SerializationKeys.isocode] = value }
    if let value = increment { dictionary[SerializationKeys.increment] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.isocode = aDecoder.decodeObject(forKey: SerializationKeys.isocode) as? String
    self.increment = aDecoder.decodeObject(forKey: SerializationKeys.increment) as? Float
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(isocode, forKey: SerializationKeys.isocode)
    aCoder.encode(increment, forKey: SerializationKeys.increment)
  }

}
