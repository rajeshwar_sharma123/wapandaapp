
//Notes:- This protocol is used as a interface which is used by ProcessPaymentPresenter to tranfer info to ProcessPaymentViewController

protocol ProcessPaymentViewDelegate:BaseViewProtocol {
    func processPaymentSuccess(withResponseModel data: ProcessPaymentResponseModel)
    func addTipSuccess(withResponseModel data: AddTipResponseModel)
}
