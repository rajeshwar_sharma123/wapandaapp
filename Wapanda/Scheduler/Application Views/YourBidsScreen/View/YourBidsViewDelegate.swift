//
//  YourBidsViewDelegate.swift
//  Wapanda
//
//  Created by Daffodil on 29/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

//Note:- This protocol is used as a interface which is used by YourBidsViewPresenter to tranfer info to YourBidsViewController

protocol YourBidsViewDelegate: BaseViewProtocol {
    
    func updateYourBidsList(withResponseModel upcomingTripList:YourBidsResponseModel)
}
