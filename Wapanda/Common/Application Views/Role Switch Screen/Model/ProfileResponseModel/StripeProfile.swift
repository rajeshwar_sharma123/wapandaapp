//
//  StripeProfile.swift
//
//  Created by  on 27/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class StripeProfile:NSObject,Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stripeCustomerId = "stripeCustomerId"
    static let stripeAccountId = "stripeAccountId"
    static let id = "_id"
    static let chargesEnabled = "chargesEnabled"
  }

  // MARK: Properties
  public var stripeCustomerId: String?
  public var stripeAccountId: String?
  public var id: String?
  public var chargesEnabled: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    stripeCustomerId <- map[SerializationKeys.stripeCustomerId]
    stripeAccountId <- map[SerializationKeys.stripeAccountId]
    id <- map[SerializationKeys.id]
    chargesEnabled <- map[SerializationKeys.chargesEnabled]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stripeCustomerId { dictionary[SerializationKeys.stripeCustomerId] = value }
    if let value = stripeAccountId { dictionary[SerializationKeys.stripeAccountId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.chargesEnabled] = chargesEnabled
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.stripeCustomerId = aDecoder.decodeObject(forKey: SerializationKeys.stripeCustomerId) as? String
    self.stripeAccountId = aDecoder.decodeObject(forKey: SerializationKeys.stripeAccountId) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.chargesEnabled = aDecoder.decodeObject(forKey: SerializationKeys.chargesEnabled) as? Bool
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(stripeCustomerId, forKey: SerializationKeys.stripeCustomerId)
    aCoder.encode(stripeAccountId, forKey: SerializationKeys.stripeAccountId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(chargesEnabled, forKey: SerializationKeys.chargesEnabled)
  }

}
