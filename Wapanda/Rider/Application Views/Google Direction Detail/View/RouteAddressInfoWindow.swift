

import UIKit

class RouteAddressInfoWindow: UIView {

    @IBOutlet weak var lblAddress: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func bind(withLocation address: String){
        self.lblAddress.text = address
    }
}
