//
//  LaunchViewDelegate.swift
//  Wapanda
//
//  Created by Daffolapmac on 19/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

protocol LaunchViewDelgate: BaseViewProtocol {
    func didReceiveLaunchDetails(withLaunchRequestModel:LaunchResponseModel)
}
