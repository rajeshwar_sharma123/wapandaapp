

//Notes:- This class is used for constructing TermsReview Service Request Model

class TermsReviewRequestModel {
    
    //MARK:- TermsReviewRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of TermsReviewRequestModel
         and provide TermsReviewForm1ViewViewRequestModel object.
         
         -returns : TermsReviewRequestModel
         */
        func build()->TermsReviewRequestModel{
            return TermsReviewRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting TermsReview end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return AppConstants.ApiEndPoints.TERMS_REVIEW
    }
    
}
