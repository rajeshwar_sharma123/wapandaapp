//
//  EarningsBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class EarningsBusinessLogic {
    
    init(){
        print("EarningsBusinessLogic init \(self)")
    }
    deinit {
        print("EarningsBusinessLogic deinit \(self)")
    }
    /**
     This method is used for perform sign Up With Valid Inputs constructed into a DriverHomeRequestModel
     
     - parameter inputData: Contains info for DriverHome
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performDriverAvailabiltyRequest(withDriverHomeRequestModel signUpRequestModel: DriverHomeRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        DriverHomeApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    /**
     This method is used for perform Earnings With Valid Inputs(Driver Id) constructed into a EarningsRequestModel
     
     - parameter inputData: Contains info for Driver Rating
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performGetDriveRatingRequest(withEarningsRequestModel EarningsRequestModel: EarningsRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForEarningsList()
        EarningsAPIRequest().makeAPIRequest(withReqFormData: EarningsRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
        /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForEarningsList() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_AUTH, message  : AppConstants.ErrorMessages.INVALID_AUTH)
        return errorResolver
    }
}
