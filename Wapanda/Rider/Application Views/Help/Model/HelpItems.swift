//
//  Items.swift
//
//  Created by Daffodilmac-20 on 10/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class HelpItems: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let answer = "answer"
    static let updatedAt = "updatedAt"
    static let id = "_id"
    static let createdAt = "createdAt"
    static let question = "question"
  }

  // MARK: Properties
  public var answer: String?
  public var updatedAt: String?
  public var id: String?
  public var createdAt: String?
  public var question: String?
    
  public var isExpanded: Bool = false


  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    answer <- map[SerializationKeys.answer]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    question <- map[SerializationKeys.question]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = answer { dictionary[SerializationKeys.answer] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = question { dictionary[SerializationKeys.question] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.answer = aDecoder.decodeObject(forKey: SerializationKeys.answer) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.question = aDecoder.decodeObject(forKey: SerializationKeys.question) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(answer, forKey: SerializationKeys.answer)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(question, forKey: SerializationKeys.question)
  }

}
