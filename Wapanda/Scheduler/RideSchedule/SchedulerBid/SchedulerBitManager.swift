import UIKit
import ObjectMapper

class SchedulerBitManager {

    deinit {
        print("CabSelectionBusinessLogic deinit")
    }
    
    /**
     This method is used for perform sign Up With Valid Inputs constructed into a CabSelectionRequestModel
     
     - parameter inputData: Contains info for CabSelection
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performCabSelection(withCabSelectionRequestModel signUpRequestModel: CabSelectionRequestModel, presenterDelegate:ResponseCallback) -> Void {
        
        //Adding predefined set of errors
        CabSelectionApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: registerErrorForGettingCab(), responseCallback: presenterDelegate)
    }
    /**
     This method is used for perform VehicleInfo With Valid Inputs constructed into a VehicleInfoRequestModel
     
     - parameter inputData: Contains info for vehicle type
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performAllVehicleTypeRequest(withCarMakeRequestModel request: VehicleRequestModel, presenterDelegate: SchedulerBidPresenter) {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForVehicleInfo()
        VehicleAPIRequest().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForGettingCab() -> ErrorResolver {
        
        let errorResolver:ErrorResolver = ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_KEY, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        
        return errorResolver
    }
    
    /**
     This method is used for perform VehicleInfo With Valid Inputs constructed into a VehicleInfoRequestModel
     
     - parameter inputData: Contains info for vehicle type
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performAllVehicleTypeRequest(withCarMakeRequestModel vehicleTypeRequestModel: VehicleTypeRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForVehicleInfo()
        VehicleInfoAPIRequest().makeAPIRequestForAllVehicleType(withReqFormData:vehicleTypeRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForVehicleInfo() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.AREA_NOT_COVERED, message: AppConstants.ErrorMessages.AREA_NOT_COVERED)
        
        errorResolver.registerErrorCode(ErrorCodes.STATE_NOT_COVERED, message: AppConstants.ErrorMessages.STATE_NOT_COVERED)
        
        
        return errorResolver
    }
    
}
