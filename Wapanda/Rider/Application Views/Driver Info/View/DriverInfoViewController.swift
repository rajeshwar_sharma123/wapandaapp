//
// This controller used to show the info for the driver for the ride with option to counter or start the ride
//

import UIKit
import Stripe
enum BID_TYPE{
    case COUNTER
    case CREATE
}

protocol DriverInfoScreenDelegates: class {
    func driverInfoScreenDidDismiss()
}

class DriverInfoViewController: BaseViewController {

    //IBOutlets Driver Info
    @IBOutlet weak var letsGoButton: UIButton!
    @IBOutlet weak var viewDriverPic: UIView!
    @IBOutlet weak var activityPaymentLoader: UIActivityIndicatorView!
    @IBOutlet weak var imgViewDriverPic: UIImageView!
    @IBOutlet weak var imgViewCar: UIImageView!
    @IBOutlet weak var imgViewRating: UIImageView!
    @IBOutlet weak var imgViewBrandImage: UIImageView!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblMakeModel: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewCounterBid: UIView!
    @IBOutlet weak var viewDriverInfo: UIBorderView!
    @IBOutlet weak var viewContent: UIBorderView!
    @IBOutlet weak var btnCounterBid: UIButton!
    
    //IBOutlet Counter Bid
    @IBOutlet weak var lblDriverNameCounterBid: UILabel!
    @IBOutlet weak var lblRatingCounterBid: UILabel!
    @IBOutlet weak var lblMakeModelCounterBid: UILabel!
    @IBOutlet weak var lblCompanyNameCounterBid: UILabel!
    @IBOutlet weak var imgViewRatingCounterBid: UIImageView!
    @IBOutlet weak var pickerCounterPrice: UIPickerView!
    @IBOutlet weak var viewCounterBidBackground: UIView!
    @IBOutlet weak var viewAddPayment: UIView!
    
    var driverBidViewDelegate : DriverBidViewDelegate!
    var riderCounteredPrice : Double!{
        didSet{
            self.updateBidPriceStatus()
        }
    }
    
    //Global Varibales
    var presenterDriverInfo: DriverInfoViewPresenter!
    var presenterCreateBid: CreateBidViewPresenter!
    var driverInfo: DriverInfoResponseModel?
    var rideInfo: RideList!
    var rideDetailModel: CabSelectionViewRequestModel!
    var bidType: BID_TYPE = BID_TYPE.CREATE
    weak var delegate: DriverInfoScreenDelegates?
    var arrCounterBidPrices: [Double] = []

    @IBOutlet weak var lblBidStatus: UILabel!
    //MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.customizeView()
        self.presenterDriverInfo = DriverInfoViewPresenter(delegate: self)
        self.presenterCreateBid = CreateBidViewPresenter(delegate: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(addPaymentScreenDidDismiss(_:)), name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.REFRESH_PAYMENT_OPTION), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setDefaultPayment(false)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.getDriverInfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /// This method is used to initalize screen mendatory data to load
    ///
    /// - Parameter info: ride information with type RideList
    func bind(withRideInfo info: RideList, rideDetailModel: CabSelectionViewRequestModel, delegate: DriverInfoScreenDelegates,driverBidDelegate:DriverBidViewDelegate){
        rideInfo = info
        self.rideDetailModel = rideDetailModel
        self.delegate = delegate
        self.driverBidViewDelegate = driverBidDelegate
    }
    
    //MARK: Custom View Handling 
    
    
    /// Method is used to do view customization on load
    func customizeView(){
        self.viewDriverPic.setCornerCircular(self.viewDriverPic.frame.size.width/2)
        self.imgViewDriverPic.setCornerCircular(self.imgViewDriverPic.frame.size.width/2)
        self.addBlurrEffectOnView()
    }
    @objc fileprivate func addPaymentScreenDidDismiss(_ notification:NSNotification)
    {
        let isContextRefresh = (notification.object as! NSNumber).boolValue
        self.setDefaultPayment(isContextRefresh)
    }
    @objc fileprivate func setDefaultPayment(_ shouldLoadContext:Bool)
    {
        self.activityPaymentLoader.isHidden = false
        self.viewCounterBidBackground.isUserInteractionEnabled = false
        self.viewAddPayment.isUserInteractionEnabled = false
        
        guard let _ = StripePaymentManager.shared.customer else {
            self.getDefaultCardFromNewContext()
            return
        }
        if !shouldLoadContext
        {
            if let defaultSource = StripePaymentManager.shared.customer?.defaultSource as? STPSource,defaultSource.type == .card
            {
                self.initialseDefaultCardView(defaultSource)
            }
            else
            {
                self.initialiseNoPaymentAddedView()
            }
        }
        else
        {
            self.getDefaultCardFromNewContext()
        }

    }
    private func getDefaultCardFromNewContext()
    {
        let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        customerContext.retrieveCustomer { (customer, error) in
            if error != nil
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: (error?.localizedDescription)!)
                self.viewCounterBidBackground.isHidden = true
                self.viewAddPayment.isHidden = false
                return
            }
            StripePaymentManager.shared.customer = customer
            if let defaultSource = customer?.defaultSource as? STPSource,defaultSource.type == .card
            {
                self.initialseDefaultCardView(defaultSource)
            }
            else
            {
                 self.initialiseNoPaymentAddedView()
            }
        }
        
    }
    private func initialseDefaultCardView(_ defaultCard : STPSource)
    {
        self.imgViewBrandImage.image = StripePaymentManager.shared.getBrandImage(basedOnBrandType: (defaultCard.cardDetails?.brand)!)
        self.lblCardNumber.text = defaultCard.cardDetails?.last4
        self.viewCounterBidBackground.isHidden = false
        self.viewAddPayment.isHidden = true
        self.activityPaymentLoader.isHidden = true
        self.viewCounterBidBackground.isUserInteractionEnabled = true
        self.viewAddPayment.isUserInteractionEnabled = true
    }
    private func initialiseNoPaymentAddedView()
    {
        self.imgViewBrandImage.image = StripePaymentManager.shared.getBrandImage(basedOnBrandType: .unknown)
        self.lblCardNumber.text = ""
        self.activityPaymentLoader.isHidden = true
        self.viewCounterBidBackground.isHidden = true
        self.viewAddPayment.isHidden = false
        self.activityPaymentLoader.isHidden = true
        self.viewCounterBidBackground.isUserInteractionEnabled = true
        self.viewAddPayment.isUserInteractionEnabled = true
    
    }
    /// Method adds blurr effect view on screen
    func addBlurrEffectOnView(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 0.8
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubview(toBack: blurEffectView)
    }
    
    
    /// Refresh screen with new data available in screen datasource
    func refreshScreenData(){
        guard self.driverInfo != nil else{return}
        
        //Enable/Disable Counter Bid Button
        
        if (self.rideInfo.cabInfo?.type?.vehicleType?.caseInsensitiveCompare("taxi") == ComparisonResult.orderedSame){
           self.btnCounterBid.backgroundColor = UIColor.appCounterGreyColor()
           self.btnCounterBid.isUserInteractionEnabled = false
        }else{
            self.btnCounterBid.backgroundColor =  UIColor(red: 57/255.0, green: 76/255.0, blue: 211/255.0, alpha: 1.0)
            self.btnCounterBid.isUserInteractionEnabled = true
        }
    
        
        
        //Driver Name
        self.lblDriverName.text = (self.driverInfo?.firstName ?? "").capitalizeWordsInSentence() + " " + (self.driverInfo?.lastName ?? "").capitalizeWordsInSentence()
        //Company Name
        self.lblCompanyName.text  = (self.rideInfo.cabInfo?.companyName ?? "")
        //Make & Model
        self.lblMakeModel.text = (self.rideInfo.cabInfo?.make?.capitalizeWordsInSentence() ?? "") + " " + (self.rideInfo.cabInfo?.model?.capitalizeWordsInSentence() ?? "")
        //Rating
        if let rating = self.driverInfo?.driverProfile?.averageRating{
            self.lblRating.text = String(format: "%.1f", rating)
        }
        //Price
        self.lblPrice.text = String(format: "$%0.2f", self.rideInfo.rideEstimatedFare!)
        //Driver Image
//        self.imgViewDriverPic.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: self.driverInfo!.profileImageFileId))!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
//
        let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: self.driverInfo!.profileImageFileId))!
        
        imgViewDriverPic.setImageWith(URLRequest.init(url: imageUrl), placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"), success: { (request, response, image) in
            self.imgViewDriverPic.image = image
            self.imgViewDriverPic.contentMode = .scaleAspectFit
        }, failure: { (request, response, error) in
            
            self.imgViewDriverPic.image = #imageLiteral(resourceName: "ic_account_placeholder")
            self.imgViewDriverPic.contentMode = .center
        })

        
        //Car Image
        self.imgViewCar.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: (self.rideInfo.cabInfo?.imageId ?? "")))!, placeholderImage: #imageLiteral(resourceName: "ic_car_bg"))
        //Star Image Tint 
        self.imgViewRating.getImageViewWithImageTintColor(color: UIColor.appDarkGrayColor())
    }
    
    /// Method is used to get the driver info data
    private func getDriverInfo(){
        let driverInfoRequestModel = DriverInfoViewRequestModel(driverId: self.rideInfo._id!)
        self.presenterDriverInfo.sendDriverInfoRequest(withDriverInfoViewRequestModel: driverInfoRequestModel)
    }
    
    func dismissScreen(){
        self.dismiss(animated: false) { 
            self.delegate?.driverInfoScreenDidDismiss()
        }
    }
    
    func dismissScreen(withMessage message: String){
        self.dismissScreen()
        AppUtility.presentToastWithMessage(message)
    }
    
    //MARK: Button Actions 
    
    /// This method handle click on lets go button
    ///
    /// - Parameter sender: button reference
    @IBAction func btnLetsGoClick(_ sender: Any) {
        self.bidType = .CREATE
        let requestModel = CreateBidViewRequestModel(latFrom: rideDetailModel.latFrom, lngFrom: rideDetailModel.lngFrom, latTo: rideDetailModel.latTo, lngTo: rideDetailModel.lngTo, driverId: self.driverInfo!.id, riderId: AppDelegate.sharedInstance.userInformation.id, estDistance: rideInfo.rideEstimatedDistance, estDuration: rideInfo.rideEstimatedTime, startTime: Date().getUTCFormateDate(), riderCounterPrice: nil,fromAddress:self.rideDetailModel.sourceAddress,toAddress:self.rideDetailModel.destinationAddress)
        self.presenterCreateBid.sendCreateBidRequest(withCreateBidViewRequestModel: requestModel)
    }
    
    /// This method handle click on counter button
    ///
    /// - Parameter sender: button reference
    @IBAction func btnCounterBidClick(_ sender: Any) {
        self.initialisePickerView()
        self.viewContent.bringSubview(toFront: self.viewCounterBid)
    }
    
    /// This method handle click on add payment button
    ///
    /// - Parameter sender: button reference
    @IBAction func btnAddPaymentMethodClick(_ sender: Any) {
        if let _ = StripePaymentManager.shared.customer?.defaultSource
        {
        let paymentVC = UIViewController.getViewController(AddPaymentScreenViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            paymentVC.isComingFromSideMenu = false
        let navigationController = UINavigationController(rootViewController: paymentVC)
        UIApplication.shared.visibleViewController?.present(navigationController, animated: true)
        }
        else
        {
            let paymentMethodVC = UIViewController.getViewController(PaymentMethodViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            paymentMethodVC.isComingFromSideMenu = false
            let navigationController = UINavigationController(rootViewController: paymentMethodVC)
            UIApplication.shared.visibleViewController?.present(navigationController, animated: true)
        }
    }

    /// This method handle click on cross button
    ///
    /// - Parameter sender: button reference
    @IBAction func btnCrossClick(_ sender: Any) {
        self.dismissScreen()
    }
    @IBAction func driverRatingTapped(_ sender: Any) {
        self.navigateToDriverRatingList()
    }
    
    //MARK:Navigation
    private func navigateToDriverRatingList()
    {
        let driverRatingVC =  UIViewController.getViewController(DriverRatingViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        driverRatingVC.driverInfoModel = self.driverInfo
        self.present(driverRatingVC, animated: true, completion: {})
    }
    
        
}

//MARK: Driver Info View Presenter

extension DriverInfoViewController: DriverInfoViewDelegate{
    
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        if alertMessage == AppConstants.ErrorMessages.DRIVER_UNAVAILABLE{
            self.dismissScreen(withMessage: alertMessage)
        }
        else{
            super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
        }
    }
    
    func driverInfoFetched(withResponseModel data: DriverInfoResponseModel){
        if let driverAvailable = data.driverProfile?.available, driverAvailable{
            driverInfo  = data
            self.refreshScreenData()
        }
        else{
            self.dismissScreen(withMessage: AppConstants.ErrorMessages.DRIVER_UNAVAILABLE)
        }
    }
}

//MARK: Create bid view delegate

extension DriverInfoViewController: CreateBidViewDelegate{
    func bidCreated(withResponseData data: CreateBidResponseModel){
        self.dismiss(animated: true) {
            let objNotificationWaiting = PushNotificationObjectModel(JSON: [:])
            objNotificationWaiting?.biddingDoc = BiddingDoc(JSON: data.dictionaryRepresentation())
            if let _ = self.riderCounteredPrice
            {
             objNotificationWaiting?.biddingDoc?.riderCounterPrice = Float(self.riderCounteredPrice)
            }
            self.driverBidViewDelegate.riderDidCreateBid(objNotificationWaiting!)
        }
    }
}

