//
//  DocumentsListTableViewCell.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/24/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DocumentsListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDocumentTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(WithData title: String){
        self.lblDocumentTitle.text = title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
