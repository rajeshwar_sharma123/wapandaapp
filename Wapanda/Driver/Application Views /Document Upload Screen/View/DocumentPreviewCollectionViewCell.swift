//
//  DocumentPreviewCollectionViewCell.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/21/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DocumentPreviewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewPreview: UIImageView!
    var tapGestureImageViewPreview: UITapGestureRecognizer!
    @IBOutlet weak var imageViewPlaceHolder: UIImageView!
    @IBOutlet weak var btnAddImage: CustomButton!
    @IBOutlet weak var viewBackground: UIView!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewBackground.layer.cornerRadius = self.viewBackground.frame.height/2
    }
    
    func initialiseUploadCellOfCollectionView(_ cell : DocumentPreviewCollectionViewCell,uploadScreenType:UploadScreenType) -> DocumentPreviewCollectionViewCell
    {
        switch uploadScreenType {
        case .Insurance:
            cell.imageViewPlaceHolder.backgroundColor = DocumentsUploadScreenViewController.INSURANCE_SCREEN_PLACEHOLDER_BACKGROUND
            cell.imageViewPlaceHolder.image = nil
            cell.imageViewPreview.isHidden = false
            cell.btnAddImage.isHidden = cell.imageViewPreview.image == nil ? false : true
            cell.imageViewPlaceHolder.isHidden = cell.imageViewPreview.image == nil ? false : true
            
            break
        case .Selfie:
            cell.imageViewPlaceHolder.backgroundColor = DocumentsUploadScreenViewController.SELFIE_SCREEN_PLACEHOLDER_BACKGROUND
            cell.imageViewPlaceHolder.image = DocumentsUploadScreenViewController.SELFIE_SCREEN_PLACEHOLDER_IMAGE
            cell.imageViewPlaceHolder.contentMode = .scaleAspectFit
            cell.imageViewPreview.isHidden = false
            cell.btnAddImage.isHidden = cell.imageViewPreview.image == nil ? false : true
            cell.imageViewPlaceHolder.isHidden = cell.imageViewPreview.image == nil ? false : true
            break
        case .Vehicle:
            cell.imageViewPlaceHolder.backgroundColor = DocumentsUploadScreenViewController.VEHICLE_SCREEN_PLACEHOLDER_BACKGROUND
            cell.imageViewPlaceHolder.image = DocumentsUploadScreenViewController.VEHICLE_SCREEN_PLACEHOLDER_IMAGE
            cell.imageViewPlaceHolder.contentMode = .scaleToFill
            cell.imageViewPreview.isHidden = false
            cell.btnAddImage.isHidden = cell.imageViewPreview.image == nil ? false : true
            cell.imageViewPlaceHolder.isHidden = cell.imageViewPreview.image == nil ? false : true
            break
            
        }
        return cell
        
    }
}
