//
//  ResetPasswordRequestModel.swift
//  Wapanda
//
//  Created by Daffodil on 21/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation


//Notes:- This class is used for constructing OTPPopup Service Request Model

class ResetPasswordRequestModel {
    
    //MARK:- NewPasswordRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    
    internal class Builder{
        
        
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        
        /**
         This method is used for setting current password
         
         - parameter otp: String parameter that is going to be set on current password
         
         - returns: returning Builder Object
         */
        
        func setCurrentPassword(_ currentPassword:String) -> Builder{
            requestBody["oldPassword"] = currentPassword as AnyObject?
            return self
        }
        
        
        /**
         This method is used for setting new password
         
         - parameter email: String parameter that is going to be set on new password
         
         - returns: returning Builder Object
         */
        func setNewPassword(_ newPassword:String)->Builder{
            requestBody["newPassword"] = newPassword as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of ResetPasswordRequestModel
         and provide ResetPasswordRequestModel object.
         
         -returns : ResetPasswordRequestModel
         */
        
        func build()->ResetPasswordRequestModel{
            return ResetPasswordRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Reset Password end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.CHANGE_PASSWORD)
    }
    
}
