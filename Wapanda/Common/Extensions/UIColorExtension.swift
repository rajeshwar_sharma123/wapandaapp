
//Note:- This class contains color code of specific colors

import UIKit


extension UIColor{
    
    static func appThemeColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1.0)
    }

    static func appDarkThemeColor() -> UIColor {
        return UIColor(red: 233/255.0, green: 94/255.0, blue: 94/255.0, alpha: 1.0)
    }
    
    static func appSeperatorColor() -> UIColor {
        return UIColor(red: 240/255.0, green: 242/255.0, blue: 247/255.0, alpha: 1.0)
    }
    
    static func appPlaceholderColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 179.02/255.0, blue: 179.02/255.0, alpha: 1.0)
    }
    static func appDocumentPlaceholderColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 179.02/255.0, blue: 179.02/255.0, alpha: 0.76)
    }
    static func liftCabsMarkerColor() -> UIColor {
        return UIColor(red: 143/255.0, green:  157/255.0, blue:  255/255.0, alpha: 0.76)
    }
    static func gettCabsMarkerColor() -> UIColor {
        return UIColor(red: 255/255.0, green:  183/255.0, blue: 49/255.0, alpha: 1)
    }
    static func uberCabsMarkerColor() -> UIColor {
        return UIColor(red: 224/255.0, green:  139/255.0, blue:  255/255.0, alpha:1)
    }
    static func sliderColor() -> UIColor {
        return UIColor(red: 115/255.0, green:  115/255.0, blue:  115/255.0, alpha:1)
    }
    static func centerMarkColor() -> UIColor {
        return UIColor(red: 166/255.0, green:  166/255.0, blue:  166/255.0, alpha:1)
    }
    static func centerDividerMarkColor() -> UIColor {
        return UIColor(red: 115/255.0, green:  115/255.0, blue:  115/255.0, alpha:1)
    }
    static func appDarkGrayColor() -> UIColor {
        return UIColor(red: 64/255.0, green:  64/255.0, blue:  64/255.0, alpha:1)
    }
    static func appCounterBlueColor() -> UIColor {
        return UIColor(red: 69/255.0, green:  88/255.0, blue:  230/255.0, alpha:1)
    }
    static func appCounterGreyColor() -> UIColor {
        return UIColor(red: 197/255.0, green:  197/255.0, blue:  197/255.0, alpha:1)
    }
    static func driverRatingGreenStatus()->UIColor
    {
        return UIColor(red: 8/255.0, green:  184/255.0, blue:  115/255.0, alpha:1)
    }
    
    static func schedulingThemeColor() -> UIColor {
        return UIColor(red: 20/255.0, green: 34/255.0, blue: 134/255.0, alpha: 1.0)
    }
    
    static func pickerBorderColor() -> UIColor {
        return UIColor(red: 194/255.0, green: 201/255.0, blue: 255/255.0, alpha: 1.0)
    }
    
    static func countDownTimerNormalCircleColor() -> UIColor {
        return UIColor(red: 155/255.0, green: 155/255.0, blue: 155/255.0, alpha: 1.0)
    }
    static func countDownTimerEndCircleColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1.0)
    }
    static func transparentBackgroundColor() -> UIColor {
        return UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.8)
    }
    
    static func tripOpenStatusBackgroundColor() -> UIColor {
        return UIColor(red: 58/255.0, green: 76/255.0, blue: 211/255.0, alpha: 0.20)
    }
    
    static func tripCancelledStatusBackgroundColor() -> UIColor {
        return UIColor(red: 64/255.0, green:  64/255.0, blue:  64/255.0, alpha:0.20)
    }
    static func filterPickerBorderColor() -> UIColor {
        return UIColor(red: 115/255.0, green: 115/255.0, blue: 115/255.0, alpha: 0.5)
    }

    static func tripScheduledStatusBackgroundColor() -> UIColor {
        return UIColor(red: 255/255.0, green: 110/255.0, blue: 110/255.0, alpha: 0.20)
    }
    static func tripOpenStatusTextColor() -> UIColor {
        return UIColor(red: 58/255.0, green: 76/255.0, blue: 211/255.0, alpha: 1)
    }
    
    static func setColorWithRGB(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }

    static func schedulerNavColor() -> UIColor {
        return UIColor(red: 20/255.0, green:  34/255.0, blue:  134/255.0, alpha:1)
    }
}
