//
//  CounterPriceViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 13/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
class CounterPriceViewController: BaseViewController {
    //MARK:- Outlets
    
    @IBOutlet weak var pickerCounterPrice: UIPickerView!
    @IBOutlet weak var buttonSubmit: CustomButton!
    var riderCounteredPrice : Float!{
        didSet{
            self.updateBidPriceStatus()
        }
    }
    var counterPresenter:CounterPricePresenter!
    var counteredPriceDelegate:CounteredPriceViewDelegate!
    var priceObjNotification : PushNotificationObjectModel!
    @IBOutlet weak var lblBidStatus: UILabel!
    var arrCounterBidPrices: [Float] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.counterPresenter = CounterPricePresenter(delegate: self)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.prepareCounterPriceArray()
        self.initialisePickerView()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.removeSelectorFromPickerView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Helper Methods
    
    private func initialisePickerView()
    {
        self.pickerCounterPrice.delegate = self
        self.pickerCounterPrice.dataSource = self
        self.pickerCounterPrice.selectRow(self.getIndexForEstimatedFare(fare: self.riderCounteredPrice), inComponent: 0, animated: true)
        if self.pickerCounterPrice.selectedRow(inComponent: 0) == self.getIndexForEstimatedFare(fare: self.riderCounteredPrice)
        {
            self.buttonSubmit.backgroundColor = UIColor.appCounterGreyColor()
            self.buttonSubmit.isUserInteractionEnabled = false
        }
        else
        {
            self.buttonSubmit.backgroundColor = UIColor.appCounterBlueColor()
            self.buttonSubmit.isUserInteractionEnabled = true
        }
    }
    
    func updateBidPriceStatus(){
        guard self.priceObjNotification != nil else {return}
        
        if (self.riderCounteredPrice > self.priceObjNotification.biddingDoc!.fareValue!.fairValues!.fairPriceLow!) && (self.riderCounteredPrice < self.priceObjNotification.biddingDoc!.fareValue!.fairValues!.fairPriceHigh!){
            self.lblBidStatus.text = AppConstants.ScreenSpecificConstant.CounterBidScreen.BID_LOOKS_GOOD_MESSAGE
        }
        else if self.riderCounteredPrice >= (self.priceObjNotification.biddingDoc!.fareValue!.fairValues!.fairPriceHigh!){
            self.lblBidStatus.text = AppConstants.ScreenSpecificConstant.CounterBidScreen.BID_HIGH_MESSAGE
        }
        else {
            self.lblBidStatus.text = AppConstants.ScreenSpecificConstant.CounterBidScreen.BID_LOW_MESSAGE
        }
    }
    
    func prepareCounterPriceArray(){
        var price = self.riderCounteredPrice
        
        
        while price! <= self.priceObjNotification.biddingDoc!.fareValue!.fairValues!.maximumPrice!{
            self.arrCounterBidPrices.append(price!)
            price = price! + self.priceObjNotification.biddingDoc!.fareValue!.moneyview!.increment!
        }
        
//        while (price! + self.priceObjNotification.biddingDoc!.fareValue!.moneyview!.increment!) <= self.priceObjNotification.biddingDoc!.fareValue!.fairValues!.maximumPrice!{
//            price = price! + self.priceObjNotification.biddingDoc!.fareValue!.moneyview!.increment!
//            self.arrCounterBidPrices.append(price!)
//        }
        
        //In case nothing prepared to show in the list
        if self.arrCounterBidPrices.count == 0{
            self.arrCounterBidPrices.append(price!)
        }
    }
    
    func getIndexForEstimatedFare(fare: Float)->Int{
        
        for (index, price) in self.arrCounterBidPrices.enumerated(){
            if fare == price{
                return index
            }
        }
        
        return 0
    }
    
    private func removeSelectorFromPickerView()
    {
        for view: UIView in pickerCounterPrice.subviews {
            if view.bounds.size.height < 2.0 {
                view.backgroundColor = UIColor.clear
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true) { 
            
        }
    }
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        self.counterPresenter.sendcounterPriceSumbitRequest(withData: CounterPriceScreenRequestModel(driverCounterPrice: Double(self.riderCounteredPrice), bidingId: priceObjNotification.biddingDoc!.id))
    }
    
}
extension CounterPriceViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrCounterBidPrices.count
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.riderCounteredPrice = self.arrCounterBidPrices[row]
        
        if self.pickerCounterPrice.selectedRow(inComponent: 0) == self.getIndexForEstimatedFare(fare: self.priceObjNotification.biddingDoc!.riderCounterPrice!)
        {
            self.buttonSubmit.backgroundColor = UIColor.appCounterGreyColor()
            self.buttonSubmit.isUserInteractionEnabled = false
        }
        else
        {
            self.buttonSubmit.backgroundColor = UIColor.appCounterBlueColor()
            self.buttonSubmit.isUserInteractionEnabled = true
        }

        
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: String(format:"$%0.2f",self.arrCounterBidPrices[row]), attributes: [NSForegroundColorAttributeName:UIColor.white])
    }
}
extension CounterPriceViewController:CounterPriceViewDelegate
{
    func counterPriceSubmittedSuccessful(withResponseModel: DriverCounterResponseModel)
    {
        let objNotificationWaiting = PushNotificationObjectModel(JSON: [:])
        objNotificationWaiting?.biddingDoc = BiddingDoc(JSON: withResponseModel.dictionaryRepresentation())
        objNotificationWaiting?.biddingDoc?.riderCounterPrice = self.priceObjNotification.biddingDoc!.riderCounterPrice!
        self.dismiss(animated: false) {
            self.counteredPriceDelegate.didCounterPrice(objNotificationWaiting!)
        }
    }
    func didChangeRidePrice(_ counterPrice : Double)
    {
    
    
    }
    func showLoader()
    {
        self.showLoader(self)
    }
    
    func hideLoader()
    {
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
        self.dismiss(animated: false) {
        }
    }

}
