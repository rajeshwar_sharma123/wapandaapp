import UIKit

protocol BidDetailViewDelegate: BaseViewProtocol {
    
    func showDetail(bidDetail: BidDetailResponseModel)
    func bidPlaced()
    func showErrorAlert(_ alertTitle: String, alertMessage: String)
//    func driverStatusUpdatedSuccessful(reponse: LoginResponseModel)
}
