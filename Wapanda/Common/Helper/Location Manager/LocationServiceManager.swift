//
//  LocationServiceManager.swift
//  Wapanda
//
//  Created by daffomac-31 on 24/07/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//
import Foundation
import CoreLocation
import UIKit
import JHTAlertController
protocol WelcomeScreenViewDelegate: class {
    func showControllerWithVC(controller: UIViewController)
    func locationUpdated(lat: Double, long: Double)
    func receivedAddressFromLocation(_ address:String)
}


extension WelcomeScreenViewDelegate
{
    func receivedAddressFromLocation(_ address:String){}
}
class LocationServiceManager: NSObject, CLLocationManagerDelegate{
    
    static let sharedInstance = LocationServiceManager()
    var currentLocation: CLLocation?
    var driverLocationUpdateTimer: Timer!
 
    weak var viewDelegate: WelcomeScreenViewDelegate?
    
    private var manager = CLLocationManager()

    private func configureLocationServices(){
        
        self.checkForUserPermissions()
    }
    
    //MARK: Location Manager Delegate
    private func locationManager(manager: CLLocationManager,
                         didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
            manager.delegate = self
        }
        else{
            self.checkForUserPermissions()
        }
    }
    
    func checkForUserPermissions(){
        manager.delegate = self
        
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedAlways:
        manager.startUpdatingLocation()
            break
        case .notDetermined:
            manager.requestAlwaysAuthorization()
        case .authorizedWhenInUse:
        manager.startUpdatingLocation()
            break
        case .restricted, .denied:
            let alertController = JHTAlertController(
                title: AppConstants.ScreenSpecificConstant.WelcomeScreen.TITLE_LOCATION_ACCESS_DISABLED,
                message: AppConstants.ScreenSpecificConstant.WelcomeScreen.MESSAGE_LOCATION_SERVICE_DISABLED,
                preferredStyle: .alert)
            alertController.alertBackgroundColor = .white
            alertController.titleViewBackgroundColor = .white
            
            alertController.messageTextColor = .black
            alertController.titleTextColor = .black

            alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
            alertController.hasRoundedCorners = true
            
            alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
            alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
            let openAction = JHTAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        
                    }
                }
            }
            alertController.addAction(openAction)
            self.viewDelegate!.showControllerWithVC(controller: alertController)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let enableFakeGps = PListUtility.getValue(forKey: AppConstants.PListKeys.ENABLE_FAKE_GPS) as? String

        if enableFakeGps == "true"{
            self.currentLocation = CLLocation(latitude: 40.699926, longitude: -73.9080747)
        }
        else{
            self.currentLocation = locations.last
        }
        
        if !UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            manager.stopUpdatingLocation()
            self.viewDelegate?.locationUpdated(lat: (self.currentLocation?.coordinate.latitude)!, long: (self.currentLocation?.coordinate.longitude)!)
        }
        else
        {
            if let _ = self.driverLocationUpdateTimer
            {}
            else
            {
                self.startDriverUpdateTimer(withtimeInterVal:3)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func startDriverUpdateTimer(withtimeInterVal timeInterVal:TimeInterval)
    {
        if let _ = self.driverLocationUpdateTimer
        {
            self.driverLocationUpdateTimer.invalidate()
            self.driverLocationUpdateTimer = nil
        }
        self.driverLocationUpdateTimer = Timer.scheduledTimer(timeInterval: timeInterVal, target: self, selector: #selector(sendLocationUpdateForDriver), userInfo: nil, repeats: true)
    }
    func stopDriverUpdateTimer()
    {
        if let _ = self.driverLocationUpdateTimer
        {
            self.driverLocationUpdateTimer.invalidate()
        }
        self.driverLocationUpdateTimer = nil
    }
    func sendLocationUpdateForDriver()
    {
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN) && self.currentLocation != nil
        {
            self.viewDelegate?.locationUpdated(lat: (self.currentLocation?.coordinate.latitude)!, long: (self.currentLocation?.coordinate.longitude)!)
        }
    }
    
//    func getFakeGPSCoordinatesFromLocation(location: CLLocation)->CLLocation{
//        let lat = location.coordinate.latitude + 11.0238408
//        let lng = -(180 - location.coordinate.longitude) + 27.9180363
//
//        return CLLocation(latitude: lat, longitude: lng)
//    }
    
}
