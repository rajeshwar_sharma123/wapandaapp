
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct SignUpViewRequestModel {
    
    var firstName                   : String!
    var lastName                    : String?
    var email                       : String?
    var phoneNo                     : String!
    var password                    : String!
    var countryCode                 : String!
    var countryCodeExt              : String!
    var phoneOTP                    : String!
}
