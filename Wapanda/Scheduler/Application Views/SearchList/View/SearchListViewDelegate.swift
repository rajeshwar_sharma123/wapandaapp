import UIKit

protocol SearchListViewDelegate: BaseViewProtocol {
    
    func refreshList(list: [SearchListItemModel])
    func showErrorAlert(_ alertTitle: String, alertMessage: String)
}
