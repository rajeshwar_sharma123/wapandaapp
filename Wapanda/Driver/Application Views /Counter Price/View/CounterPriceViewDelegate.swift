//
//  CounterPriceViewDelegate.swift
//  Wapanda
//
//  Created by Daffolapmac on 14/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

protocol CounterPriceViewDelegate: BaseViewProtocol {
    func counterPriceSubmittedSuccessful(withResponseModel: DriverCounterResponseModel)
    func didChangeRidePrice(_ counterPrice : Double)
  //  func didCounterPrice()
}
protocol CounteredPriceViewDelegate {
    func didCounterPrice(_ objNotification:PushNotificationObjectModel)
}
