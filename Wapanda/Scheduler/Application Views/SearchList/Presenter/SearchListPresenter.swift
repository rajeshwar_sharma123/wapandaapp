import UIKit
import ObjectMapper

class SearchListPresenter: ResponseCallback {
    
    private weak var view: SearchListViewDelegate!
    var savedSearchesList: SearchListResponseModel?
    private var searchListManager = SearchListManager()
    var callInProgress = false
    var itemToDelete = ""
    
    var skip = 0
    var LIMIT = 20
    
    init(delegate: SearchListViewDelegate){
        view = delegate
    }
    
    /**
     This method is used to get saved search by the driver
     */
    func getSavedSearchList(with skip: Int) {
        
        self.skip = skip
        view.showLoader()
        
        callInProgress = true
        let savedSearchListRequest: SearchListRequestModel  = SearchListRequestModel
            .Builder()
            .addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
//            .setSkip(skip)
//            .setLimit(LIMIT)
            .build()
        
        searchListManager.getSavedSearches(with : savedSearchListRequest, presenterDelegate: self)
    }
    
    
    func deleteSearch(searchId: String) {
        
        view.showLoader()
        itemToDelete = searchId
        let savedSearchDeleteReqModel: SavedSearchDeleteReqModel  = SavedSearchDeleteReqModel
            .Builder()
            .addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setSearchId(searchId)
            .build()
        
        searchListManager.deleteSavedSearch(with : savedSearchDeleteReqModel, presenterDelegate: self)
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject: T){
        
        callInProgress = false
        if let response = responseObject as? SearchListResponseModel {
            if skip == 0{
                savedSearchesList = response
            }
            else{
                savedSearchesList?.hasNext = response.hasNext
                savedSearchesList?.items.append(contentsOf: response.items)
            }
            self.view?.refreshList(list: savedSearchesList!.items)
        }
        else if responseObject is SavedSearchDeleteResponseModel {
            deleteSearchFromLocal(id: itemToDelete)
            itemToDelete = ""
        }
        view.hideLoader()
    }
    
    private func deleteSearchFromLocal(id: String) {
        
        for (index, item) in savedSearchesList!.items.enumerated() {
            if item.id == id {
                savedSearchesList!.items.remove(at: index)
                self.view?.refreshList(list: savedSearchesList!.items)
            }
        }
    }
    
    
    func servicesManagerError(error: ErrorModel){
        view?.hideLoader()
        callInProgress = false
        self.view?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
}
