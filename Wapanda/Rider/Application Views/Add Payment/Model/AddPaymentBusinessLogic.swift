//
//  AddPaymentBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class AddPaymentBusinessLogic {
    
    init(){
        print("AddPaymentBusinessLogic init \(self)")
    }
    deinit {
        print("AddPaymentBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform AddPayment With Valid Inputs constructed into a AddPaymentRequestModel
     
     - parameter inputData: Contains info for Code Received From Stripe Connect
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performCustomerIdFetchRequest(withAddPaymentRequestModel bankInfoRequestModel: AddPaymentRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForAddPayment()
        AddPaymentAPIRequest().makeAPIRequestForCustomerId(withReqFormData: bankInfoRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for perform AddPayment With Valid Inputs constructed into a AddPaymentRequestModel
     
     - parameter inputData: Contains info for Code Received From Stripe Connect
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performDeleteSourceRequest(withAddPaymentRequestModel bankInfoRequestModel: AddPaymentRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForAddPayment()
        AddPaymentAPIRequest().makeAPIRequest(withReqFormData: bankInfoRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForAddPayment() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        
        return errorResolver
    }
}
