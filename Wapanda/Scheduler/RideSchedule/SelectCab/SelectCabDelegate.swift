
protocol SelectCabViewDelegate: class {

    func refreshList(list: [VehicleListResponseModel])
    func showErrorAlert(_ errorTitle: String, alertMessage message: String)
    func showLoader()
    func hideLoader()
}
