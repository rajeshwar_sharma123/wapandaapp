//
//  IndexArray.swift
//
//  Created by  on 05/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class IndexArray: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let minutefare = "minutefare"
    static let indexrate = "indexrate"
    static let milefare = "milefare"
    static let basefare = "basefare"
    static let cartype = "cartype"
  }

  // MARK: Properties
  public var minutefare: Float?
  public var indexrate: Float?
  public var milefare: Float?
  public var basefare: Float?
  public var cartype: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    minutefare <- map[SerializationKeys.minutefare]
    indexrate <- map[SerializationKeys.indexrate]
    milefare <- map[SerializationKeys.milefare]
    basefare <- map[SerializationKeys.basefare]
    cartype <- map[SerializationKeys.cartype]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = minutefare { dictionary[SerializationKeys.minutefare] = value }
    if let value = indexrate { dictionary[SerializationKeys.indexrate] = value }
    if let value = milefare { dictionary[SerializationKeys.milefare] = value }
    if let value = basefare { dictionary[SerializationKeys.basefare] = value }
    if let value = cartype { dictionary[SerializationKeys.cartype] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.minutefare = aDecoder.decodeObject(forKey: SerializationKeys.minutefare) as? Float
    self.indexrate = aDecoder.decodeObject(forKey: SerializationKeys.indexrate) as? Float
    self.milefare = aDecoder.decodeObject(forKey: SerializationKeys.milefare) as? Float
    self.basefare = aDecoder.decodeObject(forKey: SerializationKeys.basefare) as? Float
    self.cartype = aDecoder.decodeObject(forKey: SerializationKeys.cartype) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(minutefare, forKey: SerializationKeys.minutefare)
    aCoder.encode(indexrate, forKey: SerializationKeys.indexrate)
    aCoder.encode(milefare, forKey: SerializationKeys.milefare)
    aCoder.encode(basefare, forKey: SerializationKeys.basefare)
    aCoder.encode(cartype, forKey: SerializationKeys.cartype)
  }

}
