//
//  Models.swift
//
//  Created by Daffodil on 22/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Models: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let modelMakeId = "model_make_id"
    static let modelName = "model_name"
  }

  // MARK: Properties
  public var modelMakeId: String?
  public var modelName: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    modelMakeId <- map[SerializationKeys.modelMakeId]
    modelName <- map[SerializationKeys.modelName]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = modelMakeId { dictionary[SerializationKeys.modelMakeId] = value }
    if let value = modelName { dictionary[SerializationKeys.modelName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.modelMakeId = aDecoder.decodeObject(forKey: SerializationKeys.modelMakeId) as? String
    self.modelName = aDecoder.decodeObject(forKey: SerializationKeys.modelName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(modelMakeId, forKey: SerializationKeys.modelMakeId)
    aCoder.encode(modelName, forKey: SerializationKeys.modelName)
  }

}
