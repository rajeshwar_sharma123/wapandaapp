//
//  LaunchResponseModel.swift
//
//  Created by Daffolapmac on 06/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class LaunchResponseModel: NSObject,Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let loginResponseModel = "user"
    static let biddingDoc = "bid"
    static let trip = "trip"
  }

  // MARK: Properties
  public var loginResponseModel: LoginResponseModel?
  public var biddingDoc: BiddingDoc?
  public var trip: Trip?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    loginResponseModel <- map[SerializationKeys.loginResponseModel]
    biddingDoc <- map[SerializationKeys.biddingDoc]
    trip <- map[SerializationKeys.trip]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = loginResponseModel { dictionary[SerializationKeys.loginResponseModel] = value.dictionaryRepresentation() }
    if let value = biddingDoc { dictionary[SerializationKeys.biddingDoc] = value.dictionaryRepresentation() }
    if let value = trip { dictionary[SerializationKeys.trip] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.loginResponseModel = aDecoder.decodeObject(forKey: SerializationKeys.loginResponseModel) as? LoginResponseModel
    self.biddingDoc = aDecoder.decodeObject(forKey: SerializationKeys.biddingDoc) as? BiddingDoc
    self.trip = aDecoder.decodeObject(forKey: SerializationKeys.trip) as? Trip
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(loginResponseModel, forKey: SerializationKeys.loginResponseModel)
    aCoder.encode(biddingDoc, forKey: SerializationKeys.biddingDoc)
    aCoder.encode(trip, forKey: SerializationKeys.trip)
  }

}
