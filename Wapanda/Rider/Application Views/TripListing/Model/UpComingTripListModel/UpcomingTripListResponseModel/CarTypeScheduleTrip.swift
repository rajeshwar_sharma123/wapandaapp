//
//  CarType.swift
//
//  Created by  on 27/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class CarTypeScheduleTrip: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let vehicleType = "vehicleType"
  }

  // MARK: Properties
  public var vehicleType: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    vehicleType <- map[SerializationKeys.vehicleType]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
  }

}
