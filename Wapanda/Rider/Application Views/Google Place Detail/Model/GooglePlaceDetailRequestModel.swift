

//Notes:- This class is used for constructing GooglePlaceDetail Service Request Model
import GooglePlaces
import UIKit

class GooglePlaceDetailRequestModel {
    
    var placeId: String
    var placeClient: GMSPlacesClient
    
    //This is a Private Constrctor instantiating Service Model Request Object
    
    private init(builderObject:Builder){
        
        //Instantiating service Request model Properties with Builder Object property
        self.placeId = builderObject.placeId
        self.placeClient = builderObject.placeClient
    }
    
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var placeId: String = ""
        var placeClient = GMSPlacesClient()
        
        func buildWithPlaceId(placeId: String) -> Builder{
            self.placeId = placeId
            return self
        }
        
        func buildWithPlaceClient(placeClient: GMSPlacesClient) -> Builder{
            self.placeClient = placeClient
            return self
        }
        
        /**
         This method returns the Service request Model
         
         - returns: Returns ServiceRequestModel Object having.
         */
        
        func build() -> GooglePlaceDetailRequestModel{
            return GooglePlaceDetailRequestModel(builderObject: self)
        }
        
    }
}
