//
//  ModelsResponse.swift
//  Wapanda
//
//  Created by Daffodil on 22/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

public final class ModelsResponseModel: Mappable, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let models = "Models"
    }
    
    // MARK: Properties
    public var models: [Models]?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        models <- map[SerializationKeys.models]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = models { dictionary[SerializationKeys.models] = value.map { $0.dictionaryRepresentation() } }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.models = aDecoder.decodeObject(forKey: SerializationKeys.models) as? [Models]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(models, forKey: SerializationKeys.models)
    }
    
}
