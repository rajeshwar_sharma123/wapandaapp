//
//  AddPaymentScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AddPaymentScreenRequestModel {
    
    var customerId         : String!
    var sourceId            : String!
}
