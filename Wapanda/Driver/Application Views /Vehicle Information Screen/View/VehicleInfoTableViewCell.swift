//
//  VehicleInfoTableViewCell.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/23/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class VehicleInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var lblInputTitle: UILabel!
    @IBOutlet weak var txtFieldInput: CustomTextField!
    @IBOutlet weak var lblTextError: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
