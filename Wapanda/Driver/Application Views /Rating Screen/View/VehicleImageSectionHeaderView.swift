//
//  VehicleImageSectionHeaderView.swift
//  Wapanda
//
//  Created by Daffolapmac on 16/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class VehicleImageSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewBackgroundDriverImage: UIView!
    @IBOutlet weak var imageViewVehicle: UIImageView!
    @IBOutlet weak var imageViewDriver: UIImageView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
