

//Notes:- This class is used for constructing SendEmergencyAlert Service Request Model

class SendEmergencyAlertRequestModel {
    
    //MARK:- SendEmergencyAlertRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        /**
         This method is used for setting contact number
         
         - parameter userName: String parameter that is going to be set on Phone
         
         - returns: returning Builder Object
         */
        func setPhone(_ phone:String) -> Builder{
            requestBody["phone"] = phone as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Text
         
         - parameter userName: String parameter that is going to be set on Text
         
         - returns: returning Builder Object
         */
        func setText(_ text:String) -> Builder{
            requestBody["text"] = text as AnyObject?
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of SendEmergencyAlertRequestModel
         and provide SendEmergencyAlertForm1ViewViewRequestModel object.
         
         -returns : SendEmergencyAlertRequestModel
         */
        func build()->SendEmergencyAlertRequestModel{
            return SendEmergencyAlertRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting SendEmergencyAlert end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return AppConstants.ApiEndPoints.SEND_EMERGENCY_ALERT
    }
    
}
