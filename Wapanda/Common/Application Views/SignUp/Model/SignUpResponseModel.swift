//
//  SignUpResponseModel.swift
//  Wapanda
//
//  Created by daffomac-31 on 24/07/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class SignUpResponseModel:Mappable {
    
    var firstName       : String = ""
    var lastName       : String = ""
    var email       : String = ""
    var phone       : String = ""
    var countryCode       : String = ""
    var countryCodeExt       : String = ""
    var _id       : String = ""
    var accountDisabled       : String = ""
    var emailVerified       : String = ""
    var phoneVerified       : String = ""
    var phoneOTP       : String = ""  //Temp
    
    required internal init?(map: Map) {
        mapping(map: map)
    }
    
    init() {
        
    }
    
    internal func mapping(map: Map) {
        firstName         <- map["firstName"]
        lastName         <- map["lastName"]
        email         <- map["email"]
        phone         <- map["phone"]
        countryCode         <- map["countryCode"]
        countryCodeExt         <- map["countryCodeExt"]
        _id         <- map["_id"]
        accountDisabled         <- map["accountDisabled"]
        emailVerified         <- map["emailVerified"]
        phoneOTP         <- map["phoneOTP"]
    }
}
