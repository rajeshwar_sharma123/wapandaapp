//
//  DriverRatingResponseModel.swift
//
//  Created by Daffolapmac on 16/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DriverRatingResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let rateAndReviews = "rateAndReviews"
    static let driverProfile = "driverProfile"
  }

  // MARK: Properties
  public var rateAndReviews: RateAndReviews?
  public var driverProfile: DriverProfile?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    rateAndReviews <- map[SerializationKeys.rateAndReviews]
    driverProfile <- map[SerializationKeys.driverProfile]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = rateAndReviews { dictionary[SerializationKeys.rateAndReviews] = value.dictionaryRepresentation() }
    if let value = driverProfile { dictionary[SerializationKeys.driverProfile] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.rateAndReviews = aDecoder.decodeObject(forKey: SerializationKeys.rateAndReviews) as? RateAndReviews
    self.driverProfile = aDecoder.decodeObject(forKey: SerializationKeys.driverProfile) as? DriverProfile
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(rateAndReviews, forKey: SerializationKeys.rateAndReviews)
    aCoder.encode(driverProfile, forKey: SerializationKeys.driverProfile)
  }

}
