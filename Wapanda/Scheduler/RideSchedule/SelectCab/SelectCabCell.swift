import UIKit

class SelectCabCell: UITableViewCell {
    
    @IBOutlet weak var radioBtnImageView: UIImageView!
    @IBOutlet weak var lblCabType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var cabIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func bindData(cabType: String?, price: Double?, icon: String?, currentlySelected: Bool){
        lblCabType.text = cabType ?? ""
    
        if cabType == "Any"
        {
            lblPrice.text = "--.--"
        }
        else
        {
            lblPrice.text = AppUtility.getFormattedPriceString(withPrice: price ?? 0.0)
        }
        
        
        if let icon = icon, let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: icon))  {
            cabIconImageView.setImageWith(imageUrl, placeholderImage: #imageLiteral(resourceName: "ic_standard_car"))
        }
        else {
            cabIconImageView.image =  #imageLiteral(resourceName: "ic_driver_side").withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            
        }
        
        if currentlySelected {
            makeCellSelected()
        }
        else{
            makeCellDeselected()
        }
    }
    
    
    func makeCellSelected() {
        radioBtnImageView.image =  #imageLiteral(resourceName: "radioBtnSelected")
        self.contentView.backgroundColor = UIColor(red: 69/255, green: 88/255, blue: 230/255, alpha: 1)
    }
    
    func makeCellDeselected() {
        radioBtnImageView.image =  #imageLiteral(resourceName: "radioBtnDeselected")
        self.contentView.backgroundColor = UIColor(red: 52/255, green: 70/255, blue: 200/255, alpha: 1)
    }
}
