import UIKit

class BidsListManager {
    deinit {
        print("BidsListManager deinit")
    }
    
    /**
     This method is used for perform request of bids
     
     - parameter inputData: Contains info for BidsListRequestModel
 
     */
    func getBids(with request: BidsListRequestModel, presenterDelegate: ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForGetBids()
        BidsListService().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for perform save search
     
     - parameter request: Contains info for SaveSearchReqModel
     - parameter presenterDelegate: ResponseCallback object

     */
    func saveSearchPreference(with request: SaveSearchReqModel, presenterDelegate: ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerSaveSearchPreference()
        SaveSearchAPIRequest().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerSaveSearchPreference() -> ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
        return errorResolver
        
        
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForGetBids() -> ErrorResolver {
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        return errorResolver
        
    }
}
