//
//  DocumentViewerScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/24/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class DocumentViewerScreenViewController: BaseViewController {
    
//    var urlString : String!
    fileprivate var dataModel : Item!
    
    @IBOutlet weak var webViewDocuments: UIWebView!
    @IBOutlet weak var lblScreenTitle: UILabel!
    
    // MARK: - Document Viewer Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.intialSetupForView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeScreen(WithItemData data: Item){
        dataModel = data
    }
    
    // MARK: - Helper Methods
    private func intialSetupForView(){
        self.setTitle(title: dataModel.type)
        self.setUpWebView()
        self.setUpNavigationBar()
    }
    
    private func setTitle(title: String){
        self.lblScreenTitle.text = title
    }
    
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle: "")
        self.customizeNavigationBackButton()
    }
    private func setUpWebView(){
        self.webViewDocuments.delegate = self
//        self.webViewDocuments.loadRequest(URLRequest(url: URL(string: self.urlString)!))
        self.webViewDocuments.loadHTMLString(dataModel.content, baseURL: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - UIWebView Delegate Methods
extension DocumentViewerScreenViewController : UIWebViewDelegate
{
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.showLoader(self)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
       self.hideLoader(self)
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
         self.hideLoader(self)
        
    }
    
    
}
