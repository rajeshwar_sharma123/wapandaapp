
//Notes:- This class is used as presenter for SendEmergencyAlertViewPresenter

import Foundation
import ObjectMapper

class SendEmergencyAlertViewPresenter: ResponseCallback{
    
//MARK:- SendEmergencyAlertViewPresenter local properties
    
    private weak var sendEmergencyAlertViewDelegate          : SendEmergencyAlertViewDelegate?
    private lazy var sendEmergencyAlertBusinessLogic         : SendEmergencyAlertBusinessLogic = SendEmergencyAlertBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:SendEmergencyAlertViewDelegate){
        self.sendEmergencyAlertViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        if let _ = responseObject as? CommonResponseModel{
            self.sendEmergencyAlertViewDelegate?.sentEmergencyAlertSucessfully()
        }
        
        self.sendEmergencyAlertViewDelegate?.hideLoader()
    }
    
    func servicesManagerError(error: ErrorModel){
        self.sendEmergencyAlertViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.sendEmergencyAlertViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call SendEmergencyAlert Api.
    
    func sendSendEmergencyAlertRequest(withSendEmergencyAlertViewRequestModel sendEmergencyAlertViewRequestModel:SendEmergencyAlertViewRequestModel){
        self.sendEmergencyAlertViewDelegate?.showLoader()
        
        let viewRequestModel = SendEmergencyAlertRequestModel.Builder()
                                .setPhone(sendEmergencyAlertViewRequestModel.phone)
                                .setText(sendEmergencyAlertViewRequestModel.text)
                                .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                                .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                                .build()
        
        self.sendEmergencyAlertBusinessLogic.performSendEmergencyAlert(withSendEmergencyAlertRequestModel: viewRequestModel, presenterDelegate: self)
    }
}
