

import UIKit

enum CAB_SORTING_TYPE: String{
    case ESTIMATED_FARE = "rideEstimatedFare"
    case ARRIVAL_TIME = "rideArivalTime"
}

protocol RideListingViewDelegate: class {
    func updatedAddress(withAddressData data: CabSelectionViewRequestModel)
}

class RideListingViewController: BaseViewController {
    
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblSortByValue: UILabel!
    @IBOutlet weak var imgViewSortingUpDownArrow: UIImageView!
    @IBOutlet weak var imgViewCarSelected: UIImageView!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var imgViewSelectCarUpDownArrow: UIImageView!
    var sortingTypeSelected: String = UserRideDetail.shared.sortingType
    var viewDataSource: CabSelectionViewRequestModel!
    var cabListDataSource: CabSelectionResponseModel?
    var presenterCabList: CabSelectionViewPresenter!
    var selectedCabs: [RideList] = []
    var refreshControl: UIRefreshControl!
    var isRefreshEnabled = false
    weak var delegate: RideListingViewDelegate?
    var numberOfRidesAvailable: Int = 0

    
    @IBOutlet weak var tblView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = []
        presenterCabList = CabSelectionViewPresenter(delegate: self)
        self.addPullToRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewCustomization()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getCabList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bind(viewModel model: CabSelectionViewRequestModel, selectedCar: [RideList]){
        self.viewDataSource = model
        self.selectedCabs = selectedCar
    }
    
    /// Method used to customize view appearance
    func getCabList(){
        self.presenterCabList.sendCabSelectionRequest(withCabSelectionViewRequestModel: self.viewDataSource, withLoader: true)
    }
    
    func viewCustomization(){
        self.customizeNavigationBar()
        self.tableViewInitalization()
        self.showSourceAndDestinationAddress()
        self.updateSelectedCabView()
        self.updateSelectedSortingOrder(order: self.sortingTypeSelected)
    }
    
    func showSourceAndDestinationAddress(){
        self.lblSource.text = viewDataSource.sourceAddress
        self.lblDestination.text = viewDataSource.destinationAddress
    }
    
    func customizeNavigationBar(){
        self.customizeNavigationBackButton()
        self.customizeNavigationBarWithTitle(navigationTitle: "", color: UIColor.appThemeColor(), isTranslucent: false)
    }
    
    func addPullToRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.appThemeColor()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            self.tblView.refreshControl = refreshControl
        } else {
            self.tblView.backgroundView = refreshControl
        }
    }
    
    func refresh(_ refreshControl: UIRefreshControl) {
        self.isRefreshEnabled = true
        self.getCabList()
    }
    
    func endRefresh(){
        self.refreshControl.endRefreshing()
        self.isRefreshEnabled = false
    }
    
    override func backButtonClick() {
        self.delegate?.updatedAddress(withAddressData: self.viewDataSource)
        super.backButtonClick()
        
    }
    
    /// Method to initalize table view
    func tableViewInitalization(){
        self.tblView.registerTableViewCell(tableViewCell: RideListTableViewCell.self)
        self.tblView.registerTableViewCell(tableViewCell: ThirdPartyRideListTableViewCell.self)

        self.tblView.registerTableViewCell(tableViewCell: NoResultTableViewCell.self)
    }

    //MARK: Actions
    @IBAction func btnSortClick(_ sender: Any) {
        let dataSourceFilterScreen = CabSelectionViewRequestModel(latFrom: self.viewDataSource.latFrom, lngFrom: self.viewDataSource.lngFrom, latTo: self.viewDataSource.latTo, lngTo: self.viewDataSource.lngTo, sourceAddress: self.viewDataSource.sourceAddress, destinationAddress: self.viewDataSource.destinationAddress, cabList: nil, sortingOrder: nil)
        
        let sortingVC = UIViewController.getViewController(RideListFilterViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        sortingVC.delegate = self
        
        sortingVC.bind(withCars: self.selectedCabs, screenTypeSelected: RIDE_FILTER_SCREEN_TYPE.SORT, viewDataSource: dataSourceFilterScreen, sortingOrder: self.sortingTypeSelected)
        sortingVC.modalPresentationStyle = .overCurrentContext
        self.present(sortingVC, animated: false, completion: nil)
    }
    
    @IBAction func btnSelectCabClick(_ sender: Any) {
        let dataSourceFilterScreen = CabSelectionViewRequestModel(latFrom: self.viewDataSource.latFrom, lngFrom: self.viewDataSource.lngFrom, latTo: self.viewDataSource.latTo, lngTo: self.viewDataSource.lngTo, sourceAddress: self.viewDataSource.sourceAddress, destinationAddress: self.viewDataSource.destinationAddress, cabList: nil, sortingOrder: nil)
        let sortingVC = UIViewController.getViewController(RideListFilterViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        sortingVC.delegate = self

        sortingVC.bind(withCars: self.selectedCabs, screenTypeSelected: RIDE_FILTER_SCREEN_TYPE.CAR, viewDataSource: dataSourceFilterScreen, sortingOrder: self.sortingTypeSelected)
        sortingVC.modalPresentationStyle = .overCurrentContext
        self.present(sortingVC, animated: false, completion: nil)
    }
    
    @IBAction func btnEditSourceOrDestinationClick(_ sender: Any) {
        
        let dataSourceFilterScreen = CabSelectionViewRequestModel(latFrom: self.viewDataSource.latFrom, lngFrom: self.viewDataSource.lngFrom, latTo: self.viewDataSource.latTo, lngTo: self.viewDataSource.lngTo, sourceAddress: self.viewDataSource.sourceAddress, destinationAddress: self.viewDataSource.destinationAddress, cabList: nil, sortingOrder: nil)
        let sortingVC = UIViewController.getViewController(RideListFilterViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        sortingVC.delegate = self
        
        sortingVC.bind(withCars: self.selectedCabs, screenTypeSelected: RIDE_FILTER_SCREEN_TYPE.EDIT_ADDRESS, viewDataSource: dataSourceFilterScreen, sortingOrder: self.sortingTypeSelected)
        sortingVC.modalPresentationStyle = .overCurrentContext
        self.present(sortingVC, animated: false, completion: nil)
    }
    
    func updateSelectedCabView(){
        if selectedCabs.count == 0{
            self.imgViewCarSelected.isHidden = true
            self.lblCarType.text = AppConstants.ScreenSpecificConstant.RideListScreen.ALL_CAB_SELECTION_TITLE
        }
        else if selectedCabs.count == 1{
            let selectedCar = selectedCabs.first!
            self.imgViewCarSelected.isHidden = false
            self.imgViewCarSelected.setImageWith(URL(string: AppUtility.getImageURL(fromImageId: selectedCar.cabInfo?.type?.icon ?? ""))!, placeholderImage: #imageLiteral(resourceName: "ic_standard_car"))
            self.imgViewCarSelected.getImageViewWithImageTintColor(color: UIColor.appPlaceholderColor())
            self.lblCarType.text = selectedCar.cabInfo?.type?.vehicleType?.capitalizeWordsInSentence() ?? ""
        }
        else if selectedCabs.count > 1{
            self.imgViewCarSelected.isHidden = true
            self.lblCarType.text = AppConstants.ScreenSpecificConstant.RideListScreen.MULTIPLE_CAB_TITLE
        }
    }
    
    func bindThirdPartyData(withIndexPath indexPath: IndexPath) -> ThirdPartyRideListTableViewCell {

        var rideCount = 0
        if let rides = self.cabListDataSource?.rideList, rides.count > 0{
            rideCount = rides.count
        }

        let cell = tblView.getCell(withCellType: ThirdPartyRideListTableViewCell.self)
        let cellDataSource = cabListDataSource!.thirdpartyestimatorsList![indexPath.row - rideCount]
        let carName = (cellDataSource.thirdpartycartype ?? "-") 
        cell.bind(withCabName: carName, companyName: cellDataSource.thirdpartyname ?? "", price: Double((cellDataSource.thirdpartyestimate?.typicalPrice) ?? 0.0),imgCompany: #imageLiteral(resourceName: "ic_uber"))
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    func updateSelectedSortingOrder(order: String){
        self.sortingTypeSelected = order
        self.lblSortByValue.text = self.sortingTypeSelected
    }
    
    func reloadData(){
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.reloadData()
    }
}

//MARK: UITable View Delegate & DataSource
extension RideListingViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.numberOfRidesAvailable > 0{
            return  self.numberOfRidesAvailable
        }
        else{
            //No Cab Found
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.numberOfRidesAvailable > 0{
            return UITableViewAutomaticDimension
        }
        else{
            //No Cab Found
            return self.tblView.frame.size.height
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.numberOfRidesAvailable > 0{
            return UITableViewAutomaticDimension
        }
        else{
            //No Cab Found
            return self.tblView.frame.size.height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      if self.numberOfRidesAvailable > 0{
        
        if let rides = self.cabListDataSource?.rideList, rides.count > 0{
            
            if indexPath.row < rides.count{        //RideListTableCell
                
                let cell = tableView.getCell(withCellType: RideListTableViewCell.self)
                let cellDataSource = cabListDataSource!.rideList![indexPath.row]
                let carName = (cellDataSource.cabInfo?.make ?? "-") + " " + (cellDataSource.cabInfo?.model ?? "-")
                cell.bind(withCabName: carName, companyName: cellDataSource.cabInfo?.companyName ?? "", rating: cellDataSource.driverProfile?.averageRating ?? 0, time: cellDataSource.rideArivalTime!, price: cellDataSource.rideEstimatedFare!)
                cell.selectionStyle = .none
                return cell
                
            }else{
                    return  self.bindThirdPartyData(withIndexPath: indexPath)     //ThirdPartyRideCell
            }
        }else{
                  return  self.bindThirdPartyData(withIndexPath: indexPath)
         }
        }else{
            //No Cab Found
            let cell = tableView.getCell(withCellType: NoResultTableViewCell.self)
            if selectedCabs.count == 1 || selectedCabs.count > 1{
                cell.bind(title: AppConstants.ScreenSpecificConstant.RideListScreen.SELECT_OTHER_CAB)
            }
            else{
                cell.bind(title: AppConstants.ScreenSpecificConstant.RiderSelectCab.NO_CAB_AVAILABLE_MESSAGE)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      if self.numberOfRidesAvailable > 0 {
        
        if let rides = self.cabListDataSource?.rideList, rides.count > 0{
            
            if indexPath.row < rides.count{
                let selectedRide = rides[indexPath.row]
                let vc = UIViewController.getViewController(DriverInfoViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
                vc.bind(withRideInfo: selectedRide, rideDetailModel: self.viewDataSource, delegate: self,driverBidDelegate: self)
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: false, completion: nil)
                
            }else{
                //HandleTHirdPArtySelection

                var rideCount = 0
                if let rides = self.cabListDataSource?.rideList, rides.count > 0{
                    rideCount = rides.count
                }
                navigateToThirdPartyApp(thirdPartyItem: cabListDataSource!.thirdpartyestimatorsList![indexPath.row - rideCount])
            }
            
        }else{
            //HandleThirdPartySelection

            var rideCount = 0
            if let rides = self.cabListDataSource?.rideList, rides.count > 0{
                rideCount = rides.count
            }
            
            navigateToThirdPartyApp(thirdPartyItem: cabListDataSource!.thirdpartyestimatorsList![indexPath.row - rideCount])
        }
      }
    }
    
    
    private func navigateToThirdPartyApp(thirdPartyItem: Thirdpartyestimators) {
        
        
        let sourceDest = SourceDestinationModel.Builder().setSource(lat: viewDataSource.latFrom, long: viewDataSource.lngFrom, address: viewDataSource.sourceAddress).setDestination(lat: viewDataSource.latTo, long: viewDataSource.lngTo, address: viewDataSource.destinationAddress)
            .build()
        UserRideDetail.shared.sourceDestination = sourceDest
        
        
        if thirdPartyItem.thirdpartyname!.lowercased() == "uber".lowercased() {
            ThirdPartyDeepLink().navigateToUberApp()
        }
        else if thirdPartyItem.thirdpartyname!.lowercased() == "lyft".lowercased() {
            ThirdPartyDeepLink().navigateToLyftApp()
        }
        
    }
}

//MARK: Cab Selection View Delegate
extension RideListingViewController: CabSelectionViewDelegate{
    
    //MARK: Cab Selection Delegate
    func cabFetched(data: CabSelectionResponseModel){
        self.endRefresh()
        self.cabListDataSource = data
        if let rides = self.cabListDataSource?.rideList, rides.count > 0{
            numberOfRidesAvailable = rides.count
        }
        
        if let thirdPartyRides = self.cabListDataSource?.thirdpartyestimatorsList, thirdPartyRides.count > 0{
            numberOfRidesAvailable += thirdPartyRides.count
        }

        self.setMaxiumSeatCabsOrDefault()
        self.reloadData()
    }
    
    func setMaxiumSeatCabsOrDefault(){
        
    }
    
    //MARK: Base View Delegate
    func showLoader() {
        if !self.isRefreshEnabled{
            super.showLoader(self)
        }
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.reloadData()
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}

extension RideListingViewController: RideListFilterProtocol{
    func filterSelected(withSortingType sortingType:String, selectedCars: [RideList], updatedAddress: CabSelectionViewRequestModel){
        
        //Prepare data source with selected data
        self.selectedCabs = selectedCars
        
        //Updated Selected Sorting order for api
        if sortingType == AppConstants.ScreenSpecificConstant.RideListScreen.SORTING_TYPE_DISTANCE{
            self.viewDataSource.sortingOrder = CAB_SORTING_TYPE.ARRIVAL_TIME.rawValue
        }
        else{
            self.viewDataSource.sortingOrder = CAB_SORTING_TYPE.ESTIMATED_FARE.rawValue
        }
        
        //Update address
        self.viewDataSource = updatedAddress
        
        //Show Updated address
        self.showSourceAndDestinationAddress()
        
        //Update selected cab list
        var selectedCarIds: [String] = []
        for car in selectedCars{
            selectedCarIds.append(car.cabInfo!.type!._id!)
        }
        self.viewDataSource.cabList = selectedCarIds
        
        //Update selected cabs
        self.updateSelectedCabView()
        
        //Update selected sorting type
        self.updateSelectedSortingOrder(order: sortingType)
        
        //Get cab list
        self.getCabList()
    }
}

//MARK: Driver info screen delegate
extension RideListingViewController: DriverInfoScreenDelegates{
    func driverInfoScreenDidDismiss(){
        //Refresh cab list
        self.getCabList()
    }
}
