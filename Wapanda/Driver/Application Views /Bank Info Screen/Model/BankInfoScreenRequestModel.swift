//
//  BankInfoScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BankInfoScreenRequestModel {
    
    var clienId         : String!
    var code            : String!
    var clientSecret    : String!
    var grantType       : String!
}
