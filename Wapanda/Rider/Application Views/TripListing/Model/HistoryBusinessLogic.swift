//
//  HistoryBusinessLogic.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 13/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

class HistoryBusinessLogic {
    
    
    deinit {
        print("HistoryBusinessLogic deinit")
    }
    
    /**
     Note:- Perform Help With Valid Inputs constructed into a HelpRequestModel
     
     - parameter inputData: Contains info for Help
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performHistoryRequest(withHistoryRequestModel historyRequestModel: HistoryRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForhistory()
        
        HistoryApiRequest().makeAPIRequest(withReqFormData: historyRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     Registering set of Predefined Error coming from server
     */
    private func registerErrorForhistory() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_AUTH, message  : AppConstants.ErrorMessages.INVALID_AUTH)
        return errorResolver
    }
}
