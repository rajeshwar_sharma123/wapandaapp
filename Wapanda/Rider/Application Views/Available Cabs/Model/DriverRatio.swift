//
//  DriverRatio.swift
//
//  Created by  on 06/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DriverRatio: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let driverRatio = "driverRatio"
    static let driverId = "driverId"
    static let indexRate = "indexRate"
    static let cartype = "cartype"
  }

  // MARK: Properties
  public var driverRatio: Int?
  public var driverId: String?
  public var indexRate: Int?
  public var cartype: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    driverRatio <- map[SerializationKeys.driverRatio]
    driverId <- map[SerializationKeys.driverId]
    indexRate <- map[SerializationKeys.indexRate]
    cartype <- map[SerializationKeys.cartype]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = driverRatio { dictionary[SerializationKeys.driverRatio] = value }
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    if let value = indexRate { dictionary[SerializationKeys.indexRate] = value }
    if let value = cartype { dictionary[SerializationKeys.cartype] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.driverRatio = aDecoder.decodeObject(forKey: SerializationKeys.driverRatio) as? Int
    self.driverId = aDecoder.decodeObject(forKey: SerializationKeys.driverId) as? String
    self.indexRate = aDecoder.decodeObject(forKey: SerializationKeys.indexRate) as? Int
    self.cartype = aDecoder.decodeObject(forKey: SerializationKeys.cartype) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(driverRatio, forKey: SerializationKeys.driverRatio)
    aCoder.encode(driverId, forKey: SerializationKeys.driverId)
    aCoder.encode(indexRate, forKey: SerializationKeys.indexRate)
    aCoder.encode(cartype, forKey: SerializationKeys.cartype)
  }

}
