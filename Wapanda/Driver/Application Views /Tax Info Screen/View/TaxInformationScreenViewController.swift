//
//  TaxInformationScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/20/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//
enum TaxInformation:String {
    case individual = "Individual/Sole Proprietor"
    case partnership = "Partnership/ LLC Information"
}

import UIKit

class TaxInformationScreenViewController: BaseViewController {
    
    //MARK : Local Variables
    var taxInfoPresenter                : TaxInformationPresenter!
    var addTaxInforSkipButton           : Bool = true
    var objProfileTaxInformation        : LoginResponseModel!
    private var taxInfoAlertController  : UIAlertController!
    
    //MARK : IBOutlet Variables
    @IBOutlet weak var lbTaxNumberError: UILabel!
    @IBOutlet weak var txtFieldTaxNumberType: UITextField!
    @IBOutlet weak var lbltaxTypeTitle: UILabel!
    @IBOutlet weak var txtFieldTaxClassification: UITextField!
    @IBOutlet weak var btnDropDown: UIButton!
    var textFieldformatting : TextFieldFormatting!
    var screenMode: SCREEN_MODE = SCREEN_MODE.NEW
    
    
    //MARK : Tax Information Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.objProfileTaxInformation = AppDelegate.sharedInstance.userInformation
        self.intialSetupForView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.setupNavigationBar()
        self.setupActionSheetController()
        self.setupForTaxInfomation()
        self.customizeTextFields()
        self.txtFieldTaxNumberType.delegate = self
        self.taxInfoPresenter = TaxInformationPresenter(delegate: self, textFieldValidationDelegate: self)
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.TaxInformation.STEP_NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
        if addTaxInforSkipButton
        {
            self.addNavigationSkipButton()
        }
    }
    
    /**
     This method is used to set up Tax Information
     */
    private func setupForTaxInfomation(){
        if let taxClassification = objProfileTaxInformation.driverDocs?.taxDoc?.taxClassification
        {
            self.txtFieldTaxNumberType.text = ""
            self.txtFieldTaxClassification.text = taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE ? TaxInformation.individual.rawValue : TaxInformation.partnership.rawValue
            self.lbltaxTypeTitle.text = taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE ? AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_LABEL_TITLE : AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_LABEL_TITLE
            
            self.textFieldformatting = taxClassification == AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE ? .socialSecurityNumber : .taxid
            self.txtFieldTaxNumberType.text = objProfileTaxInformation.driverDocs?.taxDoc?.taxId == nil ?objProfileTaxInformation.driverDocs?.taxDoc?.socialSecurityNumber?.format("nnn-nn-nnnn", oldString: (objProfileTaxInformation.driverDocs?.taxDoc?.socialSecurityNumber)!) : objProfileTaxInformation.driverDocs?.taxDoc?.taxId?.format("nn-nnnnnnn", oldString: (objProfileTaxInformation.driverDocs?.taxDoc?.taxId)!)
        }
        else
        {
            self.txtFieldTaxClassification.text = TaxInformation.individual.rawValue
            self.lbltaxTypeTitle.text = AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_LABEL_TITLE
            self.txtFieldTaxNumberType.text = AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
            self.textFieldformatting = .socialSecurityNumber
        }
        self.customizeTextFields()
    }
    /**
     This method is used for Individual/Sole Proprietor tax setup
     */
    private func setupForIndividualTaxInfo(){
        self.txtFieldTaxClassification.text = TaxInformation.individual.rawValue
        self.lbltaxTypeTitle.text = AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_LABEL_TITLE
        self.txtFieldTaxNumberType.text = AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
        self.textFieldformatting = .socialSecurityNumber
        self.customizeTextFields()
    }
    
    /**
     This method is used for Partnership/ LLC Information Proprietor tax setup
     */
    private func setupForPartnershipTaxInfo(){
        self.txtFieldTaxClassification.text = TaxInformation.partnership.rawValue
        self.lbltaxTypeTitle.text = AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_LABEL_TITLE
        self.txtFieldTaxNumberType.text = AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
        self.textFieldformatting = .taxid
        self.customizeTextFields()
    }
    
    /**
     This method is used to setup ActionSheetController
     */
    private func setupActionSheetController(){
        
        let attributedString = NSAttributedString(string: AppConstants.ScreenSpecificConstant.TaxInformation.ALERT_CONTROLLER_TITLE, attributes: [
            NSFontAttributeName : UIFont.getSanFranciscoMedium(withSize: 14),
            NSForegroundColorAttributeName : UIColor.black
            ])
        
        taxInfoAlertController = UIAlertController(title: AppConstants.ScreenSpecificConstant.TaxInformation.ALERT_CONTROLLER_TITLE, message: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, preferredStyle: .actionSheet)
        taxInfoAlertController.setValue(attributedString, forKey: "attributedTitle")
        taxInfoAlertController.view.tintColor = UIColor.appThemeColor()
        
        let individualAction = UIAlertAction(title: TaxInformation.individual.rawValue, style: .default,handler: { (action:UIAlertAction!) in
            self.setupForIndividualTaxInfo()
        })
        taxInfoAlertController.addAction(individualAction)
        
        let partnershipAction = UIAlertAction(title: TaxInformation.partnership.rawValue, style: .default, handler:{ (action:UIAlertAction!) in
            self.setupForPartnershipTaxInfo()
        })
        taxInfoAlertController.addAction(partnershipAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        taxInfoAlertController.addAction(cancelAction)
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        
        if self.txtFieldTaxClassification.text == TaxInformation.individual.rawValue
        {
            self.txtFieldTaxNumberType.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_PLACEHOLDER_TITLE, attributes:attributes)
        }
        else
        {
            self.txtFieldTaxNumberType.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_PLACEHOLDER_TITLE, attributes:attributes)
        }
        
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func createRequestModelForTaxInformation()->TaxInformationScreenRequestModel{
        var objTaxInfoScreenRequestModel                   = TaxInformationScreenRequestModel()
        objTaxInfoScreenRequestModel.socialSecurityNumber  = self.txtFieldTaxClassification.text == TaxInformation.individual.rawValue ? self.txtFieldTaxNumberType.text?.replacingOccurrences(of: "-", with: "", options: .literal, range: nil) : AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
        objTaxInfoScreenRequestModel.taxId                 = self.txtFieldTaxClassification.text! == TaxInformation.partnership.rawValue ? self.txtFieldTaxNumberType.text?.replacingOccurrences(of: "-", with: "", options: .literal, range: nil) : AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
        objTaxInfoScreenRequestModel.taxClassification     = self.txtFieldTaxClassification.text == TaxInformation.partnership.rawValue ? AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_VALUE : AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_VALUE
        
        return objTaxInfoScreenRequestModel
    }


    //MARK : IBOutlest Action Methods
    @IBAction func submitButtonTapped(_ sender: CustomButton) {
        self.taxInfoPresenter.sendTaxInfoSumbitRequest(withData: self.createRequestModelForTaxInformation())
    }
    @IBAction func dropDownButtonTapped(_ sender: UIButton) {
        present(taxInfoAlertController, animated: true) {}
    }
    
    @IBAction func taxInfoTextFieldChanged(_ sender: UITextField) {
        
        
        
    }
    
    /**
     This method skips the current screen.
     */
    override func skipButtonClick() ->Void {
        self.navigateToInsuranceDocumentViewController()
    }
    
    // MARK: - Navigation
    
    func navigateToInsuranceDocumentViewController(){
        
        let documentsUploadScreenVC = UIViewController.getViewController(DocumentsUploadScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        documentsUploadScreenVC.uploadScreenType = UploadScreenType.Insurance
        documentsUploadScreenVC.numberOfDocuments = 1
//        documentsUploadScreenVC.objProfileUploadDocument = self.objProfileTaxInformation
        self.navigationController?.pushViewController(documentsUploadScreenVC, animated: true)
    }
    
    
    
}
// MARK : UITextField Delegate Methods
extension TaxInformationScreenViewController : UITextFieldDelegate
{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.txtFieldTaxNumberType:
            self.lbTaxNumberError.text = AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
            
            guard let text = textField.text else {
                return true
            }
            let lastText = (text as NSString).replacingCharacters(in: range, with: string) as String
            
            if self.textFieldformatting == .socialSecurityNumber {
                textField.text = lastText.format("nnn-nn-nnnn", oldString: text)
                return false
            }
            else
            {
                textField.text = lastText.format("nn-nnnnnnn", oldString: text)
                return false
            }
        default:
            break
        }
         return true
    }
   
}
extension TaxInformationScreenViewController : TaxInformationScreenViewDelgate
{
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }

    func taxInfoSubmittedSuccessful(withResponseModel taxResponseModel: TaxDoc) {
        if let _ = self.objProfileTaxInformation.driverDocs
        {
            self.objProfileTaxInformation.driverDocs?.taxDoc = taxResponseModel
        }
        else
        {
            self.objProfileTaxInformation.driverDocs = DriverDocs(JSON: [:])
            self.objProfileTaxInformation.driverDocs?.taxDoc = taxResponseModel
        }
        AppDelegate.sharedInstance.userInformation = self.objProfileTaxInformation
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        if self.screenMode == SCREEN_MODE.EDITING{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.navigateToInsuranceDocumentViewController()
        }
    }



}
extension TaxInformationScreenViewController : TextFieldValidationDelegate
{
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType)
    {
       self.lbTaxNumberError.text = message
    }
}
