//
//  SelectedPlaceModel.swift
//  Wapanda
//
//  Created by daffomac-31 on 28/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import GoogleMaps

struct SelectedPlaceModel {
    var title: String!
    var coordinate: CLLocationCoordinate2D!
}
