

//Notes:- This class is used for constructing GoogleDirectionDetail Service Request Model
import UIKit

class GoogleDirectionDetailRequestModel {
    
    //MARK:- NewPasswordRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    var sourceLat: Double!
    var sourceLng: Double!
    var destinationLat: Double!
    var destinationLng: Double!
    var wayPoint: (lat: Double, lng: Double)?
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        
        self.sourceLat = builder.sourceLat
        self.sourceLng = builder.sourceLng
        self.destinationLat = builder.destinationLat
        self.destinationLng = builder.destinationLng
        self.wayPoint = builder.wayPoint
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        var sourceLat: Double!
        var sourceLng: Double!
        var destinationLat: Double!
        var destinationLng: Double!
        var wayPoint: (lat: Double, lng: Double)?
        
        
        /**
         This method is used for setting otp
         
         - parameter otp: String parameter that is going to be set on otp
         
         - returns: returning Builder Object
         */
        
        func setSourceLatitude(_ sourceLat:Double) -> Builder{
            self.sourceLat = sourceLat
            return self
        }
        
        /**
         This method is used for setting otp
         
         - parameter otp: String parameter that is going to be set on otp
         
         - returns: returning Builder Object
         */
        
        func setSourceLongitude(_ sourceLong:Double) -> Builder{
            self.sourceLng = sourceLong
            return self
        }
        
        /**
         This method is used for setting otp
         
         - parameter otp: String parameter that is going to be set on otp
         
         - returns: returning Builder Object
         */
        
        func setDestinationLatitude(_ destinationLat:Double) -> Builder{
            self.destinationLat = destinationLat
            return self
        }
        
        /**
         This method is used for setting otp
         
         - parameter otp: String parameter that is going to be set on otp
         
         - returns: returning Builder Object
         */
        
        func setDestinationLongitude(_ destinationlong:Double) -> Builder{
            self.destinationLng = destinationlong
            return self
        }
        
        /**
         This method is used for setting way point
         
         - parameter otp: String parameter that is going to be set on otp
         
         - returns: returning Builder Object
         */
        
        func setWayPoint(_ lat:Double?, lng: Double?) -> Builder{
            if let _ = lat, let _ = lng{
                self.wayPoint = (lat!, lng!)
            }
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of NewPasswordRequestModel
         and provide NewPasswordRequestModel object.
         
         -returns : NewPasswordRequestModel
         */
        
        func build()->GoogleDirectionDetailRequestModel{
            return GoogleDirectionDetailRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting OTPPopup end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        if let waypoints = self.wayPoint{
            return  String(format: AppConstants.ApiEndPoints.GOOGLE_DIRECTION_WITH_WAYPOINT_API, self.sourceLat, self.sourceLng, self.destinationLat, self.destinationLng, waypoints.lat, waypoints.lng)
        }
        else{
            return  String(format: AppConstants.ApiEndPoints.GOOGLE_DIRECTION_API, self.sourceLat, self.sourceLng, self.destinationLat, self.destinationLng)
        }
    }
}
