import UIKit
import ObjectMapper

class VehicleEstimatedPriceResponseModel: Mappable {
    
    var items: [VehicleListResponseModel] = [VehicleListResponseModel]()

    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    required init?(map: Map){
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        items <- map["list"]
    }

}
