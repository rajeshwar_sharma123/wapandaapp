

import UIKit

class RideListTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTimeInMinutes: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblCabName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgViewRating: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    /// Binds the data with cell
    ///
    /// - Parameters:
    ///   - driverName: string value as driver name
    ///   - companyName: string value as company name
    ///   - rating: string value as driver rating
    ///   - time: double value in miliseconds
    ///   - price: double value as price
    func bind(withCabName cabName: String, companyName: String, rating: Double, time: UIntMax, price: Double){
        self.lblCabName.text = cabName.capitalizeWordsInSentence()
        self.lblTimeInMinutes.text = AppUtility.getTimeInMinutes(fromTimeInMS: time).uppercased()
        self.lblPrice.text = String(format: "$%0.2f", price)
        self.lblRating.text = rating > 0 ? "\(String(format: "%0.1f", rating))" : "-"
        self.lblCompanyName.text = companyName
    }
}
