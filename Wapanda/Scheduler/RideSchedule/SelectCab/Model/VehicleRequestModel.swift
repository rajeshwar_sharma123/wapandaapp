import UIKit

class VehicleRequestModel {
    
    //MARK:- AuctionDetailRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        /**
         This method is used for setting source latitude
         
         - parameter latFrom: String parameter that is going to be set on latFrom
         
         - returns: returning Builder Object
         */
        func setLatFrom(_ latFrom: Float)->Builder{
            requestBody["latFrom"] = latFrom as AnyObject?
            return self
        }
        
        /**
         This method is used for setting source longitude
         
         - parameter lngFrom: String parameter that is going to be set on lngFrom
         
         - returns: returning Builder Object
         */
        func setLngFrom(_ lngFrom: Float)->Builder{
            requestBody["lngFrom"] = lngFrom as AnyObject?
            return self
        }
        
        /**
         This method is used for setting destination latitude
         
         - parameter lngTo: Float parameter that is going to be set on lngTo
         
         - returns: returning Builder Object
         */
        func setLatTo(_ latTo: Float)->Builder{
            requestBody["latTo"] = latTo as AnyObject?
            return self
        }
        

        
        /**
         This method is used for setting source longitude
         
         - parameter lngFrom: String parameter that is going to be set on lngFrom
         
         - returns: returning Builder Object
         */
        func setLngTo(_ lngTo: Float)->Builder{
            requestBody["lngTo"] = lngTo as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of AuctionDetailRequestModel
         and provide AuctionDetailRequestModel object.
         
         -returns : ScheduleRideTimeRequestModel
         */
        func build() -> VehicleRequestModel {
            return VehicleRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting auction detail end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  "/requestcabs/all"
    }

}
