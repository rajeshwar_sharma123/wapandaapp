//
//  StripePaymentManager.swift
//  Wapanda
//
//  Created by daffomac-31 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import Stripe
import AFNetworking
class StripePaymentManager{
    
    static let shared = StripePaymentManager()
    var customer: STPCustomer?
    var isCardAdded = true
    
    var isContextRefreshed = false

    
    func refreshCustomerContext()
    {

        let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        customerContext.retrieveCustomer { (customer, error) in
      
            if let _ = customer
            {
                StripePaymentManager.shared.customer = customer
                if let defaultSource = (customer?.defaultSource as? STPSource),defaultSource.type == .card
                {
                    StripePaymentManager.shared.isCardAdded = true
                }
                else
                {
                    StripePaymentManager.shared.isCardAdded = false
                }
                self.isContextRefreshed = true
            }
        }
    }
    
    
    func refreshCustomerContextWithCompletionBlock(completion: @escaping (Bool)->())
    {
        let customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        customerContext.retrieveCustomer { (customer, error) in
            
            if let _ = customer
            {
                StripePaymentManager.shared.customer = customer
                if let defaultSource = (customer?.defaultSource as? STPSource),defaultSource.type == .card
                {
                    StripePaymentManager.shared.isCardAdded = true
                    completion(true)

                }
                else
                {
                    StripePaymentManager.shared.isCardAdded = false
                    completion(false)
                }
                self.isContextRefreshed = true
            }else{
                completion(false)
            }
        }
    }

     func getBrandImage(basedOnBrandType brand:STPCardBrand)->UIImage
    {
        switch brand {
        case .visa:
            return #imageLiteral(resourceName: "stp_card_visa.png")
        case .amex:
            return #imageLiteral(resourceName: "stp_card_amex.png")
        case .dinersClub:
            return #imageLiteral(resourceName: "stp_card_diners.png")
        case .discover:
            return #imageLiteral(resourceName: "stp_card_discover.png")
        case .JCB:
            return #imageLiteral(resourceName: "stp_card_jcb.png")
        case .masterCard:
            return #imageLiteral(resourceName: "stp_card_mastercard.png")
        case .unknown:
            return #imageLiteral(resourceName: "stp_card_unknown.png")
        }
    }
    
}
class StripeAPIClient:NSObject,STPEphemeralKeyProvider
{
    static let sharedClient = StripeAPIClient()
    
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        let url = AppConstants.URL.BASE_URL + AppConstants.ApiEndPoints.CUSTOMER_SESSION
        let manager = AFHTTPSessionManager()
        manager.requestSerializer =  AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.responseSerializer.acceptableContentTypes = Set([ "application/json"])
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer.setValue(AppConstants.APIRequestHeaders.API_KEY_VALUE, forHTTPHeaderField: AppConstants.APIRequestHeaders.API_KEY)
        manager.requestSerializer.setValue(UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN), forHTTPHeaderField: "Authorization")
        manager.get(url, parameters: [:], progress: nil, success: { (dataTask, responseObject) in
            completion((responseObject as? [String: AnyObject])?["stripeCustomerSession"] as? [AnyHashable : Any], nil)
        }) { (responseObject, error) in
            completion(nil, error)
        }
    }
}
