//
//  CancelRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class CancelRequestModel {
    
    //MARK:- CancelRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var tripId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.tripId = builder.tripId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var tripId: String!
        /**
         This method is used for setting User id
         
         - parameter biddingId: String parameter that is going to be set on biddingId
         
         - returns: returning Builder Object
         */
        func setTripId(_ tripId:String)->Builder{
            self.tripId = tripId
            return self
        }
        /**
         This method is used for setting cancelledBy
         
         - parameter basePrice: String parameter that is going to be set on cancelledBy
         
         - returns: returning Builder Object
         */
        func setCancelledBy(_ cancelledBy:String) -> Builder{
            requestBody["cancelledBy"] = cancelledBy as AnyObject?
            return self
        }
        /**
         This method is used for setting cancelledBy
         
         - parameter basePrice: String parameter that is going to be set on cancelledBy
         
         - returns: returning Builder Object
         */
        func setCancelReason(_ cancelReason:String) -> Builder{
            requestBody["cancelReason"] = cancelReason as AnyObject?
            return self
        }
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->CancelRequestModel{
            return CancelRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting login end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.CANCEL_TRIP, self.tripId)
    }
    
}
