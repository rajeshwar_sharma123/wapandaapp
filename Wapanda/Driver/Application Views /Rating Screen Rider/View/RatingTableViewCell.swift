//
//  RatingTableViewCell.swift
//  Wapanda
//
//  Created by Daffolapmac on 16/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import HCSStarRatingView
class RatingTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFeedback: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet var ratingView: UIView!
    var driverRatingView : HCSStarRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
            driverRatingView = HCSStarRatingView(frame: self.ratingView.bounds)
            driverRatingView.maximumValue = 5
            driverRatingView.minimumValue = 1
            driverRatingView.spacing = 0.1
            driverRatingView.starBorderColor = UIColor.clear
            driverRatingView.filledStarImage = #imageLiteral(resourceName: "ic_rating_star_filled")
            driverRatingView.emptyStarImage = #imageLiteral(resourceName: "ic_rating_star_empty")
            self.ratingView.addSubview(driverRatingView)
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
