//
//  Items.swift
//
//  Created by Daffodilmac-20 on 13/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class HistoryItems: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let routeImageId = "routeImageId"
    static let vehicle = "vehicle"
    static let stops = "stops"
    static let toAddress = "toAddress"
    static let driver = "driver"
    static let paid = "paid"
    static let paymentId = "paymentId"
    static let fromAddress = "fromAddress"
    static let estDistance = "estDistance"
    static let pickupByTime = "pickupByTime"
    static let rider = "rider"
    static let fromLoc = "fromLoc"
    static let agreedPrice = "agreedPrice"
    static let duration = "duration"
    static let estDuration = "estDuration"
    static let endLoc = "endLoc"
    static let createdAt = "createdAt"
    static let startLoc = "startLoc"
    static let status = "status"
    static let id = "_id"
    static let distanceTravelled = "distanceTravelled"
    static let updatedAt = "updatedAt"
    static let toLoc = "toLoc"
    static let endTime = "endTime"
    static let startTime = "startTime"
    static let payment = "payment"

  }

  // MARK: Properties
  public var routeImageId: String?
  public var vehicle: Vehicle?
  public var stops: [Stops]?
  public var toAddress: String?
  public var driver: Driver?
  public var paid: Bool? = false
  public var paymentId: String?
  public var fromAddress: String?
  public var estDistance: Int?
  public var pickupByTime: String?
  public var rider: Rider?
  public var fromLoc: FromLoc?
  public var agreedPrice: Double?
  public var duration: Int?
  public var estDuration: Int?
  public var endLoc: EndLoc?
  public var createdAt: String?
  public var startLoc: StartLoc?
  public var status: String?
  public var id: String?
  public var distanceTravelled: Int?
  public var updatedAt: String?
  public var toLoc: ToLoc?
  public var endTime: String?
  public var startTime: String?
  public var payment: Payment?


  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    routeImageId <- map[SerializationKeys.routeImageId]
    vehicle <- map[SerializationKeys.vehicle]
    stops <- map[SerializationKeys.stops]
    toAddress <- map[SerializationKeys.toAddress]
    driver <- map[SerializationKeys.driver]
    paid <- map[SerializationKeys.paid]
    paymentId <- map[SerializationKeys.paymentId]
    fromAddress <- map[SerializationKeys.fromAddress]
    estDistance <- map[SerializationKeys.estDistance]
    pickupByTime <- map[SerializationKeys.pickupByTime]
    rider <- map[SerializationKeys.rider]
    fromLoc <- map[SerializationKeys.fromLoc]
    agreedPrice <- map[SerializationKeys.agreedPrice]
    duration <- map[SerializationKeys.duration]
    estDuration <- map[SerializationKeys.estDuration]
    endLoc <- map[SerializationKeys.endLoc]
    createdAt <- map[SerializationKeys.createdAt]
    startLoc <- map[SerializationKeys.startLoc]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    distanceTravelled <- map[SerializationKeys.distanceTravelled]
    updatedAt <- map[SerializationKeys.updatedAt]
    toLoc <- map[SerializationKeys.toLoc]
    endTime <- map[SerializationKeys.endTime]
    startTime <- map[SerializationKeys.startTime]
    payment <- map[SerializationKeys.payment]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = routeImageId { dictionary[SerializationKeys.routeImageId] = value }
    if let value = vehicle { dictionary[SerializationKeys.vehicle] = value.dictionaryRepresentation() }
    if let value = stops { dictionary[SerializationKeys.stops] = value.map { $0.dictionaryRepresentation() } }
    if let value = toAddress { dictionary[SerializationKeys.toAddress] = value }
    if let value = driver { dictionary[SerializationKeys.driver] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.paid] = paid
    if let value = paymentId { dictionary[SerializationKeys.paymentId] = value }
    if let value = fromAddress { dictionary[SerializationKeys.fromAddress] = value }
    if let value = estDistance { dictionary[SerializationKeys.estDistance] = value }
    if let value = pickupByTime { dictionary[SerializationKeys.pickupByTime] = value }
    if let value = rider { dictionary[SerializationKeys.rider] = value.dictionaryRepresentation() }
    if let value = fromLoc { dictionary[SerializationKeys.fromLoc] = value.dictionaryRepresentation() }
    if let value = agreedPrice { dictionary[SerializationKeys.agreedPrice] = value }
    if let value = duration { dictionary[SerializationKeys.duration] = value }
    if let value = estDuration { dictionary[SerializationKeys.estDuration] = value }
    if let value = endLoc { dictionary[SerializationKeys.endLoc] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = startLoc { dictionary[SerializationKeys.startLoc] = value.dictionaryRepresentation() }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = distanceTravelled { dictionary[SerializationKeys.distanceTravelled] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = toLoc { dictionary[SerializationKeys.toLoc] = value.dictionaryRepresentation() }
    if let value = endTime { dictionary[SerializationKeys.endTime] = value }
    if let value = startTime { dictionary[SerializationKeys.startTime] = value }
    if let value = payment { dictionary[SerializationKeys.payment] = value.dictionaryRepresentation() }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.routeImageId = aDecoder.decodeObject(forKey: SerializationKeys.routeImageId) as? String
    self.vehicle = aDecoder.decodeObject(forKey: SerializationKeys.vehicle) as? Vehicle
    self.stops = aDecoder.decodeObject(forKey: SerializationKeys.stops) as? [Stops]
    self.toAddress = aDecoder.decodeObject(forKey: SerializationKeys.toAddress) as? String
    self.driver = aDecoder.decodeObject(forKey: SerializationKeys.driver) as? Driver
    self.paid = aDecoder.decodeBool(forKey: SerializationKeys.paid)
    self.paymentId = aDecoder.decodeObject(forKey: SerializationKeys.paymentId) as? String
    self.fromAddress = aDecoder.decodeObject(forKey: SerializationKeys.fromAddress) as? String
    self.estDistance = aDecoder.decodeObject(forKey: SerializationKeys.estDistance) as? Int
    self.pickupByTime = aDecoder.decodeObject(forKey: SerializationKeys.pickupByTime) as? String
    self.rider = aDecoder.decodeObject(forKey: SerializationKeys.rider) as? Rider
    self.fromLoc = aDecoder.decodeObject(forKey: SerializationKeys.fromLoc) as? FromLoc
    self.agreedPrice = aDecoder.decodeObject(forKey: SerializationKeys.agreedPrice) as? Double
    self.duration = aDecoder.decodeObject(forKey: SerializationKeys.duration) as? Int
    self.estDuration = aDecoder.decodeObject(forKey: SerializationKeys.estDuration) as? Int
    self.endLoc = aDecoder.decodeObject(forKey: SerializationKeys.endLoc) as? EndLoc
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.startLoc = aDecoder.decodeObject(forKey: SerializationKeys.startLoc) as? StartLoc
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.distanceTravelled = aDecoder.decodeObject(forKey: SerializationKeys.distanceTravelled) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.toLoc = aDecoder.decodeObject(forKey: SerializationKeys.toLoc) as? ToLoc
    self.endTime = aDecoder.decodeObject(forKey: SerializationKeys.endTime) as? String
    self.startTime = aDecoder.decodeObject(forKey: SerializationKeys.startTime) as? String
    self.payment = aDecoder.decodeObject(forKey: SerializationKeys.payment) as? Payment

  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(routeImageId, forKey: SerializationKeys.routeImageId)
    aCoder.encode(vehicle, forKey: SerializationKeys.vehicle)
    aCoder.encode(stops, forKey: SerializationKeys.stops)
    aCoder.encode(toAddress, forKey: SerializationKeys.toAddress)
    aCoder.encode(driver, forKey: SerializationKeys.driver)
    aCoder.encode(paid, forKey: SerializationKeys.paid)
    aCoder.encode(paymentId, forKey: SerializationKeys.paymentId)
    aCoder.encode(fromAddress, forKey: SerializationKeys.fromAddress)
    aCoder.encode(estDistance, forKey: SerializationKeys.estDistance)
    aCoder.encode(pickupByTime, forKey: SerializationKeys.pickupByTime)
    aCoder.encode(rider, forKey: SerializationKeys.rider)
    aCoder.encode(fromLoc, forKey: SerializationKeys.fromLoc)
    aCoder.encode(agreedPrice, forKey: SerializationKeys.agreedPrice)
    aCoder.encode(duration, forKey: SerializationKeys.duration)
    aCoder.encode(estDuration, forKey: SerializationKeys.estDuration)
    aCoder.encode(endLoc, forKey: SerializationKeys.endLoc)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(startLoc, forKey: SerializationKeys.startLoc)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(distanceTravelled, forKey: SerializationKeys.distanceTravelled)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(toLoc, forKey: SerializationKeys.toLoc)
    aCoder.encode(endTime, forKey: SerializationKeys.endTime)
    aCoder.encode(startTime, forKey: SerializationKeys.startTime)
    aCoder.encode(payment, forKey: SerializationKeys.payment)

  }

}
