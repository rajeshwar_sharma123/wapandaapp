//
//  SocketManager.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import CoreLocation
import SocketIO

enum CAB_MOVEMENT : String {
    case enter = "enter"
    case exit  =  "exit"
    case move  =  "move"
}
class SocketManager: NSObject {
    static let shared = SocketManager()
    
    //MARK:- Socket Instances
    var driverSocket = SocketIOClient(socketURL: URL(string: AppConstants.URL.SOCKET_URL)!, config: [.forcePolling(true),.forceNew(true),.log(false),.nsp(AppConstants.ApiEndPoints.DRIVER_SOCKET)])
    
    var riderSocket = SocketIOClient(socketURL: URL(string: AppConstants.URL.SOCKET_URL)!, config: [.forcePolling(true),.forceNew(true),.log(false),.nsp(AppConstants.ApiEndPoints.RIDER_SOCKET)])
    
    var tripSocket : SocketIOClient!
    //MARK:- Initializer and Deinitializer
    private override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(updateDriverLocation(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.UPDATE_DRIVER_LOCATION), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,name: Notification.Name(rawValue: AppConstants.NSNotificationNames.UPDATE_DRIVER_LOCATION), object: nil)
    }
    
    //MARK:- Trip Socket Helper Methods
    func initTripSocket(withTripId tripId:String)
    {
        SocketManager.shared.tripSocket = SocketIOClient(socketURL: URL(string: AppConstants.URL.SOCKET_URL)!, config: [.forcePolling(true),.forceNew(true),.log(true),.nsp(AppConstants.ApiEndPoints.TRIP_SOCKET),.connectParams(["tripId":tripId])])
        self.connectToTripSocket()
    }
    func connectToTripSocket()
    {
        self.initialiseTripListenerEvents()
        SocketManager.shared.tripSocket.connect()
    }
    
    private func initialiseTripListenerEvents()
    {
        SocketManager.shared.tripSocket.on(clientEvent: .connect) {data, ack in
            //print("socket connected")
            self.authoriseRider()
        }
        SocketManager.shared.tripSocket.on(AppConstants.SocketEvents.DRIVER_UPDATE) {data, ack in
            
            let cabUpdate = TripDriverUpdateModel(JSONString: self.getJsonStringFor(dictionary: data[0]))
            NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.SOCKET_TRIP_UPDATE), object:cabUpdate)

        }
        SocketManager.shared.tripSocket.on(AppConstants.SocketEvents.API_ERROR) {data, ack in
            NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.SOCKET_API_ERROR), object:nil)
            //print("api error")
        }
        SocketManager.shared.tripSocket.on(clientEvent: .disconnect) {data, ack in
            NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.SOCKET_DISCONNECT), object:nil)
            print("rider socket disconnect")
            //SocketManager.shared.driverSocket.reconnect()
        }
        SocketManager.shared.tripSocket.on(clientEvent: .error) {data, ack in
            NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.SOCKET_ERROR), object:nil)
            print("rider error \(data[0])")
        }
    }
    func authoriseRider()
    {
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN)
        {
            SocketManager.shared.tripSocket.emit(AppConstants.SocketEvents.DRIVER_AUTHORIZATION, with: [UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)])
        }
    }
    //MARK:- Driver Socket Helper Methods
    func connectToDriverSocket()
    {
        self.initialiseDriverListenerEvents()
        SocketManager.shared.driverSocket.connect()
    }
    private func initialiseDriverListenerEvents()
    {
        SocketManager.shared.driverSocket.on(clientEvent: .connect) {data, ack in
            //print("socket connected")
            self.authoriseDriver()
        }
        
        SocketManager.shared.driverSocket.on(AppConstants.SocketEvents.API_ERROR) {data, ack in
            //print("api error")
        }
        SocketManager.shared.driverSocket.on(AppConstants.SocketEvents.DRIVER_AUTHORIZATION) {data, ack in
            print("auth ")
        }

        SocketManager.shared.driverSocket.on(clientEvent: .disconnect) {data, ack in
           // print("driver socket disconnect")
            //SocketManager.shared.driverSocket.reconnect()      
        }
        
        SocketManager.shared.driverSocket.on(clientEvent: .error) {data, ack in
           // SocketManager.shared.driverSocket.disconnect()
            print("driver error")
        }
    }
    func authoriseDriver()
    {
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN)
        {
        SocketManager.shared.driverSocket.emit(AppConstants.SocketEvents.DRIVER_AUTHORIZATION, with: [UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)])
        }
    }
    @objc func updateDriverLocation(_ notication:CLLocation)
    {
        let updatedLocation = notication
        SocketManager.shared.driverSocket.emit(AppConstants.SocketEvents.DRIVER_UPDATE,["lat": updatedLocation.coordinate.latitude,"lng": updatedLocation.coordinate.longitude, "cabInfo": [:]])
    }
    
    //MARK:- Rider Socket Helper Methods
    func connectToRiderSocket(withLat lat:Double,andLong long:Double)
    {
        self.initialseRiderListenerEvents(withLat: lat, andLong: long)
        
        SocketManager.shared.riderSocket.connect()
    }
    private func initialseRiderListenerEvents(withLat lat:Double,andLong long:Double)
    {
        SocketManager.shared.riderSocket.on(clientEvent: .connect) {data, ack in
            //print("socket connected")
            self.sendRiderLocation(withLat: lat, andLong: long)
        }
        SocketManager.shared.riderSocket.on(AppConstants.SocketEvents.DRIVER_UPDATE) {data, ack in
            
            let cabUpdate = Cabs(JSONString: self.getJsonStringFor(dictionary: data[0]))
            let movement = (cabUpdate?.movement)!
            switch movement {
                
            case CAB_MOVEMENT.enter.rawValue:
                NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.CAB_ENTERS), object: cabUpdate)
                break
            case CAB_MOVEMENT.exit.rawValue:
                NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.CAB_EXITS), object: cabUpdate)
                break
            case CAB_MOVEMENT.move.rawValue:
                NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.UPDATE_CAB_LOCATION), object: cabUpdate)
                break
            default:
                break
                
            }
            
        }
        SocketManager.shared.riderSocket.on(AppConstants.SocketEvents.API_ERROR) {data, ack in
            NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.SOCKET_API_ERROR), object:nil)
            //print("api error")
        }
        SocketManager.shared.riderSocket.on(clientEvent: .disconnect) {data, ack in
            NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.SOCKET_DISCONNECT), object:nil)
            print("rider socket disconnect")
             //SocketManager.shared.driverSocket.reconnect()
        }
        SocketManager.shared.riderSocket.on(clientEvent: .error) {data, ack in
            NotificationCenter.default.post(name: Notification.Name(AppConstants.NSNotificationNames.SOCKET_ERROR), object:nil)
            print("rider error \(data[0])")
            //SocketManager.shared.driverSocket.disconnect()
        }
    }
     func sendRiderLocation(withLat lat:Double,andLong long:Double)
    {
        SocketManager.shared.riderSocket.emit(AppConstants.SocketEvents.LAT_LONG,["lat": lat,"lng": long])
    }
    private func getJsonStringFor(dictionary:Any) -> String {
        
        do {
            let data = try JSONSerialization.data(withJSONObject:dictionary, options:[])
            let dataString = String(data: data, encoding: String.Encoding.utf8)!
            return dataString
            
        } catch {
            
        }
        return ""
    }
   
}

