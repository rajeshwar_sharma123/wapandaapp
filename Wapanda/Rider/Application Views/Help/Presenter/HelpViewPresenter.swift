//
//  HelpViewPresenter.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//


//Notes:- This class is used as presenter for ProcessPaymentViewPresenter

import Foundation
import ObjectMapper

class HelpViewPresenter: ResponseCallback{
    
    //MARK:- HelpViewPresenter properties
    
    private weak var helpViewDelegate   : HelpViewDelegate?
    private lazy var helpBusinessLogic  : HelpBusinessLogic = HelpBusinessLogic()

    
    //MARK:- Constructor
    
    init(delegate responseDelegate:HelpViewDelegate){
        self.helpViewDelegate = responseDelegate
    }
    
 
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        
        self.helpViewDelegate?.hideLoader()
        if responseObject is HelpViewResponseModel
        {
        self.helpViewDelegate?.dataFetchedSuccessfully(withResponseModel: responseObject as! HelpViewResponseModel)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.helpViewDelegate?.hideLoader()
        self.helpViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    
    //MARK:- Methods to make decision and call Feedback Api.
    
    func makeHelpApiRequest(){
        self.helpViewDelegate?.showLoader()
        let requestModel = HelpViewRequestModel.Builder().addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE).build()
        self.helpBusinessLogic.performHelpRequest(withHelpRequestModel: requestModel, presenterDelegate: self)
        
    }
    
}
