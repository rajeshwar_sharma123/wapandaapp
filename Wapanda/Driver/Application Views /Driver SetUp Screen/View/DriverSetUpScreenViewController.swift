//
//  DriverSetUpScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/26/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

enum DocumentsType:Int {
    case taxInfomation = 0
    case insuranceInformation = 1
    case profilePhoto = 2
    case vehiclePhoto = 3
    case vehicleInfo = 4
    case bankInformation = 5
    case documentReview = 6
}

import UIKit

class DriverSetUpScreenViewController: BaseViewController {
    
    //MARK: Local Variables
    let documentTypes = [AppConstants.ScreenSpecificConstant.DriverSetup.TAX_INFORMATION,
                         AppConstants.ScreenSpecificConstant.DriverSetup.INSURANCE_INFORMATION,
                         AppConstants.ScreenSpecificConstant.DriverSetup.PROFILE_PHOTO,
                         AppConstants.ScreenSpecificConstant.DriverSetup.VEHICLE_PHOTO,AppConstants.ScreenSpecificConstant.DriverSetup.VEHICLE_INFO,
                         AppConstants.ScreenSpecificConstant.DriverSetup.BANK_INFORMATION,
                         AppConstants.ScreenSpecificConstant.DriverSetup.DOCUMENT_REVIEW]
  
    
    var userDetails = AppDelegate.sharedInstance.userInformation
    var shouldShowVerifyScreeen = true
    var documentStatus: [Bool] = []
    var screenTitle : String!
    //MARK: IBOutlet
    @IBOutlet weak var tableViewDocumentsType: UITableView!
    
    @IBOutlet weak var buttonContinue: CustomButton!
    // MARK: - Terms Review Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpNavigationBar()
        self.updateDocumentStatus()
        self.tableViewDocumentsType.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.customizeNavigationBarWithTitle(navigationTitle: self.screenTitle,withFontSize:14,andColor:UIColor.white)
    }
    // MARK: - Helper Methods
    private func intialSetupForView(){
        self.setUpTableView()
        self.setUpNavigationBar()
    }
    
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle: self.screenTitle,withFontSize:20,andColor:UIColor.white)
        self.customizeNavigationBackButton()
    }
    
    private func setUpTableView(){
        self.tableViewDocumentsType.delegate = self
        self.tableViewDocumentsType.dataSource = self
        self.tableViewDocumentsType.estimatedRowHeight = 60
        self.tableViewDocumentsType.tableFooterView = UIView()
    }
    // MARK: - IBOutlet Action Methods
    @IBAction func continueButtonTapped(_ sender: CustomButton) {
        self.moveToHomeScreen()
    }
    
    private func updateDocumentStatus(){
        userDetails = AppDelegate.sharedInstance.userInformation
        var isTaxDocAvailable = false
        var isInsuranceDocAvailable = false
        
        if let taxDocument = userDetails?.driverDocs?.taxDoc
        {
            if let socialSecurityNumber = taxDocument.socialSecurityNumber,socialSecurityNumber != ""
            {
                isTaxDocAvailable = true
            }
            else if let taxId = taxDocument.taxId,taxId != ""
            {
                isTaxDocAvailable = true
            }
        }
        if let insuranceDocument = userDetails?.driverDocs?.insuranceDoc
        {
            if let imageIds = insuranceDocument.imageIds,imageIds.count != 0
            {
                isInsuranceDocAvailable = true
            }
        }
        
        documentStatus = [
            isTaxDocAvailable,
            isInsuranceDocAvailable,
            userDetails?.profileImageFileId != nil ? true: false,
            userDetails?.driverDocs?.vehicleDoc?.imageId != nil ? true: false,
            userDetails?.driverDocs?.vehicleDoc?.make != nil ? true: false,
            (userDetails?.stripeProfile?.stripeAccountId != nil) ? true: false,
            true
        ]
        
        if documentStatus.contains(false)
        {
            self.buttonContinue.setTitle(AppConstants.ScreenSpecificConstant.DriverSetup.CONTINUE_ANYWAY_TITLE, for: .normal)
        }
        else
        {
            self.buttonContinue.setTitle(AppConstants.ScreenSpecificConstant.DriverSetup.CONTINUE_TITLE, for: .normal)
        }
    }

    
    // MARK: - Navigation
    
    func moveToHomeScreen(){
        
        let driverHomeVC = UIViewController.getViewController(DriverHomeViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        if AppUtility.isUserLogin(){
            //driverHomeVC.objUserProfile = 
            if let isApproved = AppDelegate.sharedInstance.userInformation.driverProfile?.approved, !isApproved
            {
                driverHomeVC.shouldShouldShowVerifyScreeen = shouldShowVerifyScreeen
            }
        }
        self.navigationController?.pushViewController(driverHomeVC, animated: true)
    }
    
    func navigateToTaxInfoController(){
        let taxInfoVC = UIViewController.getViewController(TaxInformationScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        taxInfoVC.addTaxInforSkipButton = false
        taxInfoVC.screenMode = .EDITING
        self.navigationController?.pushViewController(taxInfoVC, animated: true)
    }
    
    func navigateToInsuranceDocController(){
        let insuranceVC = UIViewController.getViewController(DocumentsUploadScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        insuranceVC.addUploadScreenSkipButton = false
        insuranceVC.numberOfDocuments = 2
        insuranceVC.uploadScreenType = UploadScreenType.Insurance
        insuranceVC.setScreenMode(mode: .EDITING)
        self.navigationController?.pushViewController(insuranceVC, animated: true)
    }
    
    func navigateToSelfieController(){
        let selfieVC = UIViewController.getViewController(DocumentsUploadScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        selfieVC.addUploadScreenSkipButton = false
        selfieVC.numberOfDocuments = 1
        selfieVC.uploadScreenType = UploadScreenType.Selfie
        selfieVC.setScreenMode(mode: .EDITING)
        self.navigationController?.pushViewController(selfieVC, animated: true)
    }
    
    func navigateToVehicleInfoController(){
        let vehicleVC = UIViewController.getViewController(DocumentsUploadScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        vehicleVC.addUploadScreenSkipButton = false
        vehicleVC.numberOfDocuments = 1
        vehicleVC.uploadScreenType = UploadScreenType.Vehicle
        vehicleVC.setScreenMode(mode: .EDITING)
        self.navigationController?.pushViewController(vehicleVC, animated: true)
    }
    
    func navigateToVehicleDocController(){
        let vehicleVC = UIViewController.getViewController(VehicleInformationViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        vehicleVC.addVehicleInfoScreenSkipButton = false
        vehicleVC.setScreenMode(mode: .EDITING)
        self.navigationController?.pushViewController(vehicleVC, animated: true)
    }
    func navigateToReviewController(){
        
        let termsVC = UIViewController.getViewController(TermsReviewScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        termsVC.setScreenMode(mode: .EDITING)
        self.navigationController?.pushViewController(termsVC, animated: true)
    }
    
    func navigateToBankInfoScreen(){
        let VC = UIViewController.getViewController(BankInfoScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        VC.screenMode = .EDITING
        VC.addTaxInforSkipButton = false
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
// MARK: - UITableView Delegate Methods
extension DriverSetUpScreenViewController : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTypeTableViewCell", for: indexPath) as! DocumentTypeTableViewCell
        cell.bindData(WithTitle: documentTypes[indexPath.row], isDocUploaded: self.documentStatus[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPathRow = indexPath.row
        switch indexPathRow {
        case DocumentsType.taxInfomation.rawValue:
            self.navigateToTaxInfoController()
            break
        case DocumentsType.insuranceInformation.rawValue:
            self.navigateToInsuranceDocController()
            break
        case DocumentsType.profilePhoto.rawValue:
            self.navigateToSelfieController()
            break
        case DocumentsType.vehiclePhoto.rawValue:
            self.navigateToVehicleInfoController()
            break
        case DocumentsType.vehicleInfo.rawValue:
            self.navigateToVehicleDocController()
            break
        case DocumentsType.bankInformation.rawValue:
            self.navigateToBankInfoScreen()
            break
        case DocumentsType.documentReview.rawValue:
            self.navigateToReviewController()
            break
         default:
            break
        }
        
       
        self.tableViewDocumentsType.deselectRow(at: indexPath, animated: true)
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
}
