//
//  ToLoc.swift
//
//  Created by Daffodilmac-20 on 13/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ToLoc: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let type = "type"
    static let coordinates = "coordinates"
    static let id = "_id"
  }

  // MARK: Properties
  public var type: String?
  public var coordinates: [Float]?
  public var id: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    type <- map[SerializationKeys.type]
    coordinates <- map[SerializationKeys.coordinates]
    id <- map[SerializationKeys.id]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
    self.coordinates = aDecoder.decodeObject(forKey: SerializationKeys.coordinates) as? [Float]
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(coordinates, forKey: SerializationKeys.coordinates)
    aCoder.encode(id, forKey: SerializationKeys.id)
  }

}
