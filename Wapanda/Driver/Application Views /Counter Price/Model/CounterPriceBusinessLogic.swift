//
//  CounterPriceBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class CounterPriceBusinessLogic {
    
    init(){
        print("CounterPriceBusinessLogic init \(self)")
    }
    deinit {
        print("CounterPriceBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform CounterPrice With Valid Inputs constructed into a CounterPriceRequestModel
     
     - parameter inputData: Contains info for Counter Price
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performCounterPrice(withCounterPriceRequestModel CounterPriceRequestModel: CounterPriceRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForCounterPrice()
        CounterPriceAPIRequest().makeAPIRequest(withReqFormData: CounterPriceRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForCounterPrice() ->ErrorResolver{
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode (ErrorCodes.BIDDING_CLOSED, message  : AppConstants.ErrorMessages.BIDDING_CLOSED)
        errorResolver.registerErrorCode (ErrorCodes.BIDDING_EXPIRED, message  : AppConstants.ErrorMessages.BIDDING_EXPIRED)
        errorResolver.registerErrorCode (ErrorCodes.ALREADY_COUNTERED, message  : AppConstants.ErrorMessages.ALREADY_COUNTERED)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_COUNTER_PRICE, message  : AppConstants.ErrorMessages.INVALID_COUNTER_PRICE)
        
        return errorResolver
    }
}
