//
//  BiddingUnderwayViewController.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 14/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import Stripe
protocol BiddingUnderwayViewControllerDelegate {
    func updateUpcomingTripList()
}

class BiddingUnderwayViewController: BaseViewController {
    @IBOutlet weak var pandaImageView: UIImageView!
    @IBOutlet weak var topMessageLabel: UILabel!
    @IBOutlet weak var countDownTimer: SRCountdownTimer!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var counteredPriceLabel: UILabel!
    @IBOutlet weak var timerCountLabel: UILabel!
    @IBOutlet weak var priceHikeLabel: UILabel!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var acceptPriceButton: CustomButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cardView: UIBorderView!
    
    @IBOutlet weak var bidCurrentStatusLabel: UILabel!
    
    
    var waitingPresenter : WaitingPresenter!
    var isDriver = true
    var counterTotalTime : TimeInterval = 60
    var biddingUnderwayViewPresenter : BiddingUnderwayViewPresenter!
    var scheduleRideTimeResponseModel: ScheduleRideTimeResponseModel!
    var fromScheduleCheck: Bool! = false
    var isDriverSchedule: Bool! = false
    var auctionId: String!
    var riderFare: Double! = 0
    var delegate: BiddingUnderwayViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
        self.biddingUnderwayViewPresenter = BiddingUnderwayViewPresenter(delegate: self)
        self.biddingUnderwayViewPresenter.sendAuctionDetailFetchRequest(withAuctionId: self.auctionId, shouldShowLoader: true)
        countDownTimer.tag = 1
        //self.startCounterTimer(self.counterTotalTime)
    }
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        if isDriverSchedule{
            acceptPriceButton.isHidden = true
            cardView.isHidden = true
        }else{
            acceptPriceButton.isHidden = false
            cardView.isHidden = false
            enableAcceptPriceAndCancelButton(false, alpha: 0.5)
        }
        if let defaultSource = StripePaymentManager.shared.customer?.defaultSource as? STPSource
        {
            cardNumberLabel.text = defaultSource.cardDetails!.last4
            cardImageView.image = StripePaymentManager.shared.getBrandImage(basedOnBrandType: defaultSource.cardDetails!.brand)
        }
        self.setupNavigationBar()
        initialiseCounterTimer()
    }
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, color: UIColor.schedulingThemeColor(),isTranslucent: false)
        self.customizeNavigationBackButton()
    }
    
    func initialiseCounterTimer()
    {
        self.countDownTimer.lineWidth = 2.0
        self.countDownTimer.backgroundColor = UIColor.clear
        self.countDownTimer.lineColor = UIColor.white
        self.countDownTimer.delegate = self
        
    }
    func startCounterTimer(_ counterTotalTime:TimeInterval){
        self.countDownTimer.elapsedTime = 0
        // self.countDownTimer.fireInterval = 0.01
        self.countDownTimer.start(beginingValue: Int(counterTotalTime))
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        showErrorAlertWithCancelAndConfirmButtons(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.CANCEL_Bid_CONFIRMATION, VC: self)
        
    }
    
    @IBAction func acceptPriceButtonAction(_ sender: Any) {
        self.biddingUnderwayViewPresenter.sendAcceptAuctionRequest(withAuctionId: auctionId, shouldShowLoader: true)
    }
    
    // Overide back button to redirect user as required
    override func backButtonClick() ->Void {
        stopCountDownTimer()
        if fromScheduleCheck{
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func errorAlertConfirmButtonClicked() {
        stopCountDownTimer()
        self.biddingUnderwayViewPresenter.sendCancelAuctionRequest(withAuctionId: auctionId, shouldShowLoader: true)
    }
    
    // This method is used to enable to disable AcceptPrice Button
    func enableAcceptPriceAndCancelButton(_ enable: Bool, alpha: CGFloat){
        acceptPriceButton.isEnabled = enable
        acceptPriceButton.alpha = alpha
    }
    
    // This method is used to enable to disable  Cancel Button
    func disabelCancelButton(_ enable: Bool, alpha: CGFloat){
        cancelButton.isEnabled = enable
        cancelButton.alpha = alpha
    }
    
    func stopCountDownTimer(){
        countDownTimer.tag = 2
        countDownTimer.end()
    }
    
}

extension BiddingUnderwayViewController: SRCountdownTimerDelegate
{
    func timerDidUpdateCounterValue(newValue: Int){
        self.timerCountLabel.text="\(newValue)"
        if (newValue < 15){
            self.countDownTimer.lineColor = UIColor.countDownTimerEndCircleColor()
        }
    }
    
    func timerDidStart(){
        self.timerCountLabel.isHidden = false
        self.countDownTimer.lineColor = UIColor.countDownTimerNormalCircleColor()
        self.countDownTimer.trailLineColor = UIColor.white
    }
    func timerDidPause(){}
    func timerDidResume(){}
    func timerDidEnd(){
        
        if countDownTimer.tag == 1{
            self.timerCountLabel.isHidden = true
            self.biddingUnderwayViewPresenter.sendAuctionDetailFetchRequest(withAuctionId:self.auctionId, shouldShowLoader: true)
        }
    }
    
}

// MARK: - BiddingUnderwayViewDelegate Methods
extension BiddingUnderwayViewController: BiddingUnderwayViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        
        if alertMessage == AppConstants.ErrorMessages.AUCTION_CLOSED{
            AppUtility.presentToastWithMessage(AppConstants.ErrorMessages.BID_CLOSED)
            return
        }
        AppUtility.presentToastWithMessage(alertMessage)
        self.startCounterTimer(self.counterTotalTime)
        counteredPriceLabel.text = "No bid"
        
    }
    
    
    func updateAuctionDetail(withResponseModel auctionDetail:AuctionDetailResponseModel){
        if auctionDetail.lowestQuote != nil{
            enableAcceptPriceAndCancelButton(true, alpha: 1)
            counteredPriceLabel.text = AppUtility.getFormattedPriceString(withPrice: auctionDetail.lowestQuote!.price!)
            showDifferenceInPrice(auctionDetail.lowestQuote!.price!)
            
        }
        self.startCounterTimer(self.counterTotalTime)
    }
    
    func updateDriverAuctionDetail(withResponseModel auctionDetail: BidDetailResponseModel) {
        if auctionDetail.detail?.lowestQuote != nil{
            enableAcceptPriceAndCancelButton(true, alpha: 1)
            counteredPriceLabel.text = AppUtility.getFormattedPriceString(withPrice: auctionDetail.detail!.lowestQuote!.price!)
            showDifferenceInPrice(auctionDetail.detail!.lowestQuote!.price!)
            
            if auctionDetail.detail!.status == TripStatus.Accepted.rawValue{
                if AppDelegate.sharedInstance.userInformation.id == auctionDetail.detail!.selectedDriverId{
                    topMessageLabel.text = "BIDDING ACCEPTED"
                    bidCurrentStatusLabel.text = TripStatus.Accepted.rawValue
                    disabelCancelButton(false, alpha: 0.5)
                }
            }
        }
        self.startCounterTimer(self.counterTotalTime)
    }
    
    func showDifferenceInPrice(_ loewstBid: Double){
        let difference = loewstBid - riderFare
        var priceHike = AppUtility.getFormattedPriceString(withPrice: abs(difference))
        if difference > 0{
            priceHike =  "+\(priceHike)"
        }else if difference < 0{
          priceHike = "-\(priceHike)"
        }else{
            
        }
        priceHikeLabel.text = priceHike
    }
    
    func updateAcceptOrCancelAuction(withResponseModel response: CommonResponseModel) {
        print(response)
        if fromScheduleCheck{
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }else{
            delegate.updateUpcomingTripList()
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
}
