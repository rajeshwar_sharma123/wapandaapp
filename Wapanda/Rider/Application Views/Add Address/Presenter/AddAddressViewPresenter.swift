
//Notes:- This class is used as presenter for AddAddressViewPresenter

import Foundation
import ObjectMapper

class AddAddressViewPresenter: ResponseCallback{
    
//MARK:- AddAddressViewPresenter local properties
    
    private weak var addAddressViewDelegate          : AddAddressViewDelegate?
    private lazy var addAddressBusinessLogic         : AddAddressBusinessLogic = AddAddressBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:AddAddressViewDelegate){
        self.addAddressViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.addAddressViewDelegate?.hideLoader()
        if let user = responseObject as? LoginResponseModel{
            self.addAddressViewDelegate?.addAddressSuccessfully(withData: user)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.addAddressViewDelegate?.hideLoader()
        self.addAddressViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call addAddress Api.
    
    func sendAddAddressRequest(withAddAddressViewRequestModel addAddressViewRequestModel:AddAddressViewRequestModel){
        
        self.addAddressViewDelegate?.showLoader()
        
        let addAddressRequestModel = AddAddressRequestModel.Builder()
                                        .setAddressType(addAddressViewRequestModel.addressType)
                                        .setAddress(addAddressViewRequestModel.lat, lng: addAddressViewRequestModel.lng, address: addAddressViewRequestModel.address)
                                        .setRiderId(AppDelegate.sharedInstance.userInformation.id!)
                                        .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                                        .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                                        .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                                        .build()
        
        self.addAddressBusinessLogic.performAddAddress(withAddAddressRequestModel: addAddressRequestModel, presenterDelegate: self)
    }
}
