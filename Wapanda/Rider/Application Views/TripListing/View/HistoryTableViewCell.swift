//
//  HistoryTableViewCell.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    //MARK:- Properties/Outlets
    @IBOutlet weak var imgViewRoute: UIImageView!
    @IBOutlet weak var lblBillAmount: UILabel!
    @IBOutlet weak var lblVisitedDate: UILabel!
    @IBOutlet weak var lblVisitedPlace: UILabel!
    @IBOutlet weak var statusImgView: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    //MARK:- Helper Methods
    
    //Bind Cell Data
    func bindData(withResponse response: HistoryResponseModel,withIndexPath indexPath: IndexPath){
        
        let item = response.items?[indexPath.row]
        
        //Set Route Image
        if let routeID = item?.routeImageId {
            let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: routeID))!
            imgViewRoute.setImageWith(imageUrl, placeholderImage: #imageLiteral(resourceName: "map_placeholder"))
            
        }
        else
        {
            imgViewRoute.image = #imageLiteral(resourceName: "map_placeholder")
        }
        
        //Set bill amount
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
        lblBillAmount.text = AppUtility.getFormattedPriceString(withPrice: Double(item?.payment?.stripeAccountAmount ?? 0.00))
        }
        else
        {
            lblBillAmount.text = AppUtility.getFormattedPriceString(withPrice: Double(item?.payment?.amountPayable ?? 0.00))
        }
        lblVisitedPlace.text = item?.vehicle?.carType?.vehicleType?.capitalizeWordsInSentence() ?? ""
        
        if let startTime = item?.startTime{
            lblVisitedDate.text =  AppUtility.getFormattedTimeDuration(fromTimeStamp: startTime)

        }else{
            lblVisitedDate.text =  ""

        }
        
        
        if item?.status?.caseInsensitiveCompare("CANCELLED") == ComparisonResult.orderedSame{
            statusImgView.isHidden = false
            lblStatus.isHidden = false
            lblBillAmount.text = "$0.00"
            
        }else{
            statusImgView.isHidden = true
            lblStatus.isHidden = true
          
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
