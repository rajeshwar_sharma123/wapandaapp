
//Notes:- This protocol is used as a interface which is used by UpdateUserProfilePresenter to tranfer info to UpdateUserProfileViewController

protocol UpdateUserProfileViewDelegate:BaseViewProtocol {
    func userProfileUpdatedSuccessfully(withResponse data: LoginResponseModel)
}
