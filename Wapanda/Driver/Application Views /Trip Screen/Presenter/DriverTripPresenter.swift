//
//  DriverTripPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps
import Kingfisher
class DriverTripPresenter: ResponseCallback{
    
    //MARK:- DriverTripPresenter local properties
    private weak var driverTripViewDelegate             : DriverTripScreenViewDelgate?
    private lazy var driverTripBusinessLogic         : DriverTripBusinessLogic = DriverTripBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:DriverTripScreenViewDelgate) {
        self.driverTripViewDelegate = responseDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.driverTripViewDelegate?.hideLoader()
        if responseObject is Trip
        {
            self.driverTripViewDelegate?.rideStartedSuccessfully(withResponseModel: responseObject as! Trip)
        }
        if responseObject is DriverTripResponseModel
        {
            self.driverTripViewDelegate?.rideEndedSuccessfully(withResponseModel:responseObject as! DriverTripResponseModel)
        }
        if responseObject is CancelResponseModel
        {
            self.driverTripViewDelegate?.didCancelTrip()
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.driverTripViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.driverTripViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendRideStartRequest(withStartRideModel requestModel: DriverTripScreenRequestModel) -> Void{
        
        self.driverTripViewDelegate?.showLoader()
        
        var rideStartRequestModel : DriverTripRequestModel!
        
        
            rideStartRequestModel = DriverTripRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setTripId(requestModel.tripId).setLatFrom(requestModel.latFrom).setLngFroms(requestModel.lngFrom).setStartTime(requestModel.startTime).build()
        
            self.driverTripBusinessLogic.performStartRideRequest(withDriverTripRequestModel: rideStartRequestModel, presenterDelegate: self)
    }
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendRideEndRequest(withStartRideModel requestModel: DriverTripScreenRequestModel) -> Void{
        
        self.driverTripViewDelegate?.showLoader()
        
        var rideStartRequestModel : DriverTripRequestModel!
        rideStartRequestModel = DriverTripRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setTripId(requestModel.tripId).setLatTo(requestModel.latFrom).setLngTo(requestModel.lngFrom).build()
        
        self.driverTripBusinessLogic.performEndRideRequest(withDriverTripRequestModel: rideStartRequestModel, presenterDelegate: self)
    }
    func update(cabMarker:GMSMarker,withNewLocation newLocation:CLLocationCoordinate2D)
    {
        
        let lastCLLocation = CLLocation(latitude: cabMarker.position.latitude, longitude: cabMarker.position.longitude)
        let newCLLocation = CLLocation(latitude: newLocation.latitude, longitude: newLocation.longitude)
        let _ = self.driverTripViewDelegate?.updateMarkerAtPosition(withPoints: CLLocationCoordinate2D(latitude: newCLLocation.coordinate.latitude, longitude: newCLLocation.coordinate.longitude), marker: cabMarker,bearing: self.getBearingBetweenTwoLocation(oldLocation: lastCLLocation, newLocation: newCLLocation))
        
    }
    func sendCancelRideRequest(withTripId tripId: String,andCancelby cancelBy:String) -> Void{
        
        self.driverTripViewDelegate?.showLoader()
        
        var cancelRequestModel : CancelRequestModel!
        cancelRequestModel = CancelRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setCancelledBy(cancelBy).setCancelReason("Any").setTripId(tripId).build()
        
        self.driverTripBusinessLogic.performCancelRequest(withCancelRequestModel: cancelRequestModel, presenterDelegate: self)
    }
    //MARK:- Bearing Calculating methods
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoLocation(oldLocation : CLLocation, newLocation : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: oldLocation.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: oldLocation.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: newLocation.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: newLocation.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }}
