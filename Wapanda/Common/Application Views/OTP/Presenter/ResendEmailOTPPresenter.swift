
//Notes:- This class is used as presenter for ResendEmailOTPPresenter

import Foundation
import ObjectMapper

class ResendEmailOTPPresenter: ResponseCallback{
    
//MARK:- OTPPopupViewPresenter local properties
    private weak var resendOTPViewDelegate          : ResendEmailOTPViewDelegate?
    private var resendOTPBusinessLogic              : ResendEmailOTPBusinessLogic!
    
//MARK:- Constructor
    init(delegate responseDelegate:ResendEmailOTPViewDelegate){
        self.resendOTPViewDelegate = responseDelegate
        self.resendOTPBusinessLogic = ResendEmailOTPBusinessLogic()
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.resendOTPViewDelegate?.hideLoader()
        self.resendOTPViewDelegate?.resendEmailOTPSuccessfully()
    }
    
    func servicesManagerError(error : ErrorModel){
        self.resendOTPViewDelegate?.hideLoader()
        self.resendOTPViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- API calls
    
    func resendEmailOTP(withEmail email:String){
        
        self.resendOTPViewDelegate?.showLoader()

        let otpPopupRequestModel:ResendEmailOTPRequestModel = ResendEmailOTPRequestModel.Builder()
            .setEmail(email)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.resendOTPBusinessLogic.performResendOTP(withResendEmailOTPRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
}
