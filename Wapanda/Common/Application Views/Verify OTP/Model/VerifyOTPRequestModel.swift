

//Notes:- This class is used for constructing OTPPopup Service Request Model

class VerifyOTPRequestModel {
    
    //MARK:- VerifyOTPRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var entryType : EntryType!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.entryType     = builder.entryType
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var entryType : EntryType!
        
        /**
         This method is used for setting otp
         
         - parameter otp: String parameter that is going to be set on otp
         
         - returns: returning Builder Object
         */
        
        func setOTP(_ otp:String) -> Builder{
            requestBody["emailOTP"] = otp as AnyObject?
            return self
        }
       
        
        /**
         This method is used for setting email
         
         - parameter email: String parameter that is going to be set on email
         
         - returns: returning Builder Object
         */
        func setEmail(_ email:String)->Builder{
            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for setting countryCodeExt
         
         - parameter countryCodeExt: String parameter that is going to be set on country code extension
         
         - returns: returning Builder Object
         */
        func setCountryCodeExt(_ countryCodeExt:String)->Builder{
            requestBody["countryCodeExt"] = countryCodeExt as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Phone Number
         
         - parameter phone: String parameter that is going to be set on country code extension
         
         - returns: returning Builder Object
         */
        func setPhone(_ phone:String)->Builder{
            requestBody["phone"] = phone as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Phone OTP
         
         - parameter phoneOTP: String parameter that is going to be set on Phone OTP
         
         - returns: returning Builder Object
         */
        func setPhoneOTP(_ phoneOTP:String)->Builder{
            requestBody["phoneOTP"] = phoneOTP as AnyObject?
            return self
        }
        /**
         This method is used for setting Phone OTP
         
         - parameter phoneOTP: String parameter that is going to be set on Phone OTP
         
         - returns: returning Builder Object
         */
        func setEntryType(_ entryType:EntryType)->Builder{
            self.entryType = entryType
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of VerifyOTPRequestModel
         and provide VerifyOTPRequestModel object.
         
         -returns : VerifyOTPRequestModel
         */
        
        func build()->VerifyOTPRequestModel{
            return VerifyOTPRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Phone OTP Popup end point
     
     -returns: String containg end point
     */
    func getPhoneVerifyEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.VERIFY_PHONE_OTP)
    }
    /**
     This method is used for getting Email OTP Popup end point
     
     -returns: String containg end point
     */
    func getEmailVerifyEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.VERIFY_EMAIL_OTP)
    }

    
}
