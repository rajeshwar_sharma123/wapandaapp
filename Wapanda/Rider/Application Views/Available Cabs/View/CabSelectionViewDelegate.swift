
//Notes:- This protocol is used as a interface which is used by CabSelectionPresenter to tranfer info to CabSelectionViewController

protocol CabSelectionViewDelegate:BaseViewProtocol {
    func cabFetched(data: CabSelectionResponseModel)
}
