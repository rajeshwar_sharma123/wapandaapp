//
//  RideScheduleTimeBussinessLogic.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 12/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

//Note :- This class contains ScheduleRideTime Buisness Logic

class ScheduleRideTimeBussinessLogic {
    
    
    deinit {
        print("FeedbackBusinessLogic deinit")
    }
    
    /**
     This method is used for perform scheduleRideTime With Valid Inputs constructed into a ScheduleRideTimeRequestModel
     
     - parameter inputData: Contains info for ScheduleRideTimeRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performScheduleRideTime(withScheduleRideTimeRequestModel scheduleRideTimeRequestModel: ScheduleRideTimeRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForfeedback()
        ScheduleRideTimeApiRequest().makeAPIRequest(withReqFormData: scheduleRideTimeRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForfeedback() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        return errorResolver
        
      
    }
}
