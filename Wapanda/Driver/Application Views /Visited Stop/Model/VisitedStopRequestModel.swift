

//Notes:- This class is used for constructing VisitedStop Service Request Model

class VisitedStopRequestModel {
    
    //MARK:- VisitedStopRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var tripId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.tripId = builder.tripId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var tripId: String!
        
        /**
         This method is used for setting trip id
         
         - parameter userName: String parameter that is going to be set on trip id
         
         - returns: returning Builder Object
         */
        func setTripId(_ tripId:String) -> Builder{
            self.tripId = tripId
            return self
        }
        
        /**
         This method is used for setting Stop id
         
         - parameter userName: String parameter that is going to be set on Stop id
         
         - returns: returning Builder Object
         */
        func setStopId(_ stopId:String) -> Builder{
            requestBody["stopId"] = stopId as AnyObject?
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of VisitedStopRequestModel
         and provide VisitedStopForm1ViewViewRequestModel object.
         
         -returns : VisitedStopRequestModel
         */
        func build()->VisitedStopRequestModel{
            return VisitedStopRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting VisitedStop end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.VISITED_STOP, self.tripId)
    }
    
}
