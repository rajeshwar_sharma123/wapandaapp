 //
//  LaunchPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class LaunchPresenter: ResponseCallback{
    
    //MARK:- LaunchPresenter local properties
    private weak var launchViewDelegate             : LaunchViewDelgate?
    private lazy var launchBusinessLogic         : LaunchBusinessLogic = LaunchBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:LaunchViewDelgate) {
        self.launchViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        if responseObject is LaunchResponseModel
        {
            self.launchViewDelegate?.didReceiveLaunchDetails(withLaunchRequestModel : responseObject as! LaunchResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        _ = error.getErrorPayloadInfo()
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendLaunchDataRequest() -> Void{
    if AppUtility.isUserLogin()
    {
        var launchRequestModel : LaunchRequestModel!
        launchRequestModel = LaunchRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).build()
        self.launchBusinessLogic.performLaunch(withLaunchRequestModel: launchRequestModel, presenterDelegate: self)
        }
    }

}
