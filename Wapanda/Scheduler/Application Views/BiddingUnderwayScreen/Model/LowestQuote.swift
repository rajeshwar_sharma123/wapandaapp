//
//  LowestQuote.swift
//
//  Created by Daffolapmac on 14/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class LowestQuote: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fairPrice = "fairPrice"
    static let maximumPrice = "maximumPrice"
    static let id = "_id"
    static let createdAt = "createdAt"
    static let updatedAt = "updatedAt"
    static let driverId = "driverId"
    static let minimumPrice = "minimumPrice"
    static let typicalPrice = "typicalPrice"
    static let price = "price"
    static let fairPriceHigh = "fairPriceHigh"
    static let fairPriceLow = "fairPriceLow"
  }

  // MARK: Properties
  public var fairPrice: Int?
  public var maximumPrice: Int?
  public var id: String?
  public var createdAt: String?
  public var updatedAt: String?
  public var driverId: String?
  public var minimumPrice: Int?
  public var typicalPrice: Int?
  public var price: Double?
  public var fairPriceHigh: Int?
  public var fairPriceLow: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    fairPrice <- map[SerializationKeys.fairPrice]
    maximumPrice <- map[SerializationKeys.maximumPrice]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    updatedAt <- map[SerializationKeys.updatedAt]
    driverId <- map[SerializationKeys.driverId]
    minimumPrice <- map[SerializationKeys.minimumPrice]
    typicalPrice <- map[SerializationKeys.typicalPrice]
    price <- map[SerializationKeys.price]
    fairPriceHigh <- map[SerializationKeys.fairPriceHigh]
    fairPriceLow <- map[SerializationKeys.fairPriceLow]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fairPrice { dictionary[SerializationKeys.fairPrice] = value }
    if let value = maximumPrice { dictionary[SerializationKeys.maximumPrice] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    if let value = minimumPrice { dictionary[SerializationKeys.minimumPrice] = value }
    if let value = typicalPrice { dictionary[SerializationKeys.typicalPrice] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = fairPriceHigh { dictionary[SerializationKeys.fairPriceHigh] = value }
    if let value = fairPriceLow { dictionary[SerializationKeys.fairPriceLow] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.fairPrice = aDecoder.decodeObject(forKey: SerializationKeys.fairPrice) as? Int
    self.maximumPrice = aDecoder.decodeObject(forKey: SerializationKeys.maximumPrice) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.driverId = aDecoder.decodeObject(forKey: SerializationKeys.driverId) as? String
    self.minimumPrice = aDecoder.decodeObject(forKey: SerializationKeys.minimumPrice) as? Int
    self.typicalPrice = aDecoder.decodeObject(forKey: SerializationKeys.typicalPrice) as? Int
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Double
    self.fairPriceHigh = aDecoder.decodeObject(forKey: SerializationKeys.fairPriceHigh) as? Int
    self.fairPriceLow = aDecoder.decodeObject(forKey: SerializationKeys.fairPriceLow) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(fairPrice, forKey: SerializationKeys.fairPrice)
    aCoder.encode(maximumPrice, forKey: SerializationKeys.maximumPrice)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(driverId, forKey: SerializationKeys.driverId)
    aCoder.encode(minimumPrice, forKey: SerializationKeys.minimumPrice)
    aCoder.encode(typicalPrice, forKey: SerializationKeys.typicalPrice)
    aCoder.encode(price, forKey: SerializationKeys.price)
    aCoder.encode(fairPriceHigh, forKey: SerializationKeys.fairPriceHigh)
    aCoder.encode(fairPriceLow, forKey: SerializationKeys.fairPriceLow)
  }

}
