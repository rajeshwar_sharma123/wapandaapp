

//Notes:- This class is used for constructing OTPPopup Service Request Model

class NewPasswordRequestModel {
    
    //MARK:- NewPasswordRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()

        
        /**
         This method is used for setting otp
         
         - parameter otp: String parameter that is going to be set on otp
         
         - returns: returning Builder Object
         */
        
        func setOTP(_ otp:String) -> Builder{
            requestBody["otp"] = otp as AnyObject?
            return self
        }
        
        /**
         This method is used for setting email
         
         - parameter email: String parameter that is going to be set on email
         
         - returns: returning Builder Object
         */
        func setNewPassword(_ newPassword:String)->Builder{
            requestBody["newPassword"] = newPassword as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of NewPasswordRequestModel
         and provide NewPasswordRequestModel object.
         
         -returns : NewPasswordRequestModel
         */
        
        func build()->NewPasswordRequestModel{
            return NewPasswordRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting OTPPopup end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.RESET_PASSWORD)
    }
    
}
