//
//  WorkInProgressViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 16/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class WorkInProgressViewController: UIViewController {

    @IBOutlet weak var crossButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupScreenUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func setupScreenUI(){
        self.customizeNavigationBar()
    }

    func customizeNavigationBar(){
        if let _ = self.navigationController
        {
             self.crossButton.isHidden = true
        self.customizeNavigationBarWithTitle(navigationTitle: "")
        self.customizeNavigationBackButton()
        }
        else
        {
            self.crossButton.isHidden = false
        }
    }

}
