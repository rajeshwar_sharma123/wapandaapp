
//Note :- This class contains OTPPopup Buisness Logic

class ResendPhoneOTPBusinessLogic {
    
    /**
     This method is used for perform OTP With Valid Inputs(OTP text) constructed into a ResendPhoneOTPRequestModel
     
     - parameter inputData: Contains info for OTPPopup
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performResendOTP(withResendPhoneOTPRequestModel otpPopupRequestModel: ResendPhoneOTPRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForOTPPopup()
        ResendPhoneOTPApiRequest().makeAPIRequest(withReqFormData: otpPopupRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForOTPPopup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.PHONE_NOT_REGISTERED, message  : AppConstants.ErrorMessages.PHONE_NOT_REGISTERED)
        
        return errorResolver
    }
}
