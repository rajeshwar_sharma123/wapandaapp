//
//  AddCreditCardScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import Stripe
struct AddCreditCardScreenRequestModel {
    
    var cardNumber      : String!
    var cvv             : String!
    var month           : String!
    var year            : String!
    var brand           : STPCardBrand!
}
