//
//  CreditCardTableViewCell.swift
//  Wapanda
//
//  Created by Daffolapmac on 31/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class CreditCardTableViewCell: UITableViewCell {
    @IBOutlet weak var labelCardNumber: UILabel!
    @IBOutlet weak var labelValidThrough: UILabel!
    @IBOutlet weak var imageViewBrand: UIImageView!
    @IBOutlet weak var buttonDefault: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
