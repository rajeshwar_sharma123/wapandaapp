/**
 This class handle the view creation for signup form2 screen
 */

import UIKit
import FRHyperLabel

class SignUpForm2ViewController: BaseViewController, TextFieldValidationDelegate, BaseViewProtocol, UITextFieldDelegate{
    
    //MARK: Screen Outlets
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var lblErrorFirstName: UILabel!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var lblErrorLastName: UILabel!
    @IBOutlet weak var lblTermsAndCondition: FRHyperLabel!
    
    //Global Varibales
    fileprivate var preRegistrationOTPPresenter: PreRegistrationOTPPresenter!
    fileprivate var signUpViewPresenter: SignUpViewPresenter!
    fileprivate var signUpViewRequestModel : SignUpViewRequestModel?


    //MARK: View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set initial UI Elements
        self.setupInitialUI()
        
        //Initialize Presenter
        self.preRegistrationOTPPresenter = PreRegistrationOTPPresenter(delegate: self)
        self.signUpViewPresenter = SignUpViewPresenter(delegate: self, textFieldValidationDelegate: self)
    }
    
    /**
     This method initalizes class with prefilled values in model from form 1 screen
     */
    func initializeViewWithSignUpRequestModel(model: SignUpViewRequestModel){
        self.signUpViewRequestModel = model
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Initial View Methods
    
    /**
     This function setup the Initial UI elements
     - returns :void
     */
    private func setupInitialUI() ->Void{
        //Customize navigation bar style
        self.setupNavigationBar()
        
        //Customize the text field properties
        self.customizeTextFields()
        
        //Customize Terms and condition string
        self.customizeTermsAndConditionText()
    }
    
    func customizeTermsAndConditionText(){
        
        
        let attributes = [NSForegroundColorAttributeName: UIColor.white,
                          NSFontAttributeName: UIFont.getSanFranciscoRegular(withSize: 16.0)]
        
        lblTermsAndCondition.attributedText = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.TERMS_AND_CONDITION_TEXT, attributes: attributes)
        
        //Step 2: Define a selection handler block
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            self.showTermsAndConditions()
        }
        
        //Step 3: Add link substrings
    lblTermsAndCondition.setLinksForSubstrings([AppConstants.ScreenSpecificConstant.SingUpScreen.TERMS_OF_SERVICE_TEXT], withLinkHandler: handler)
        
       
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
                            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
                            NSFontAttributeName : font
                        ]
        self.txtFieldFirstName.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.FIRST_NAME_PLACEHOLDER_TEXT, attributes:attributes)
        self.txtFieldLastName.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.LAST_NAME_PLACEHOLDER_TEXT, attributes:attributes)
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    private func setupNavigationBar() ->Void{
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.SingUpScreen.FORM2_NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
    }
    
    //MARK: Base View Delegate
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    //MARK: UIText Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text ?? "") + string

        switch textField
        {
        case self.txtFieldFirstName:
            self.lblErrorFirstName.text = ""
            
            if !str.isNameValid(){
                return false
            }
        case self.txtFieldLastName:
            self.lblErrorLastName.text = ""
            
            if !str.isNameValid(){
                return false
            }
        default:
            break
        }
        
        return true
    }
    
    //MARK: Text Field Validation Delelgate
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType)
    {
        switch forTextFields
        {
            case TextFieldsType.FirstName:
                self.showValidationMessageForFirstNameWithMessage(msg: message)
            default: break
        }
    }

    //Validation Helper Methods
    private func showValidationMessageForFirstNameWithMessage(msg: String){
        self.lblErrorFirstName.text = msg
    }
    
    //MARK: Action handling
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    @IBAction func btnNextClick(_ sender: Any) {
        
        self.signUpViewRequestModel = self.createSignUpViewRequestModelWithTextFieldInput()
        let validateForm2Inputs = self.signUpViewPresenter.validateAllSignUpForm2Inputs(withData: self.signUpViewRequestModel!)
        
        if validateForm2Inputs{
            self.preRegistrationOTPPresenter.requestOTP(withEmail: self.signUpViewRequestModel?.email, phone: self.signUpViewRequestModel!.phoneNo, diallingCode: self.signUpViewRequestModel!.countryCodeExt)
        }
    }
    
    
    // This method is used to show terms and condition screen
    func showTermsAndConditions(){
        let objNewRequestVC = UIViewController.getViewController(StripeWebViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        objNewRequestVC.screenTitle = AppConstants.ScreenSpecificConstant.SingUpScreen.TERMS_OF_SERVICE_TEXT
        objNewRequestVC.urlString = AppConstants.URL.TERMS_AND_CONDITION_URL
        self.navigationController?.pushViewController(objNewRequestVC, animated: true)
    }
    
    //MARK: Screen helper methods
    /**
     This method is used to get the SignUpViewRequestModel from the current input in text fields for email signup
     
     - returns : SignUpViewRequestModel with current data in textFields
     */
    func createSignUpViewRequestModelWithTextFieldInput() -> SignUpViewRequestModel {
        
        signUpViewRequestModel?.firstName = self.txtFieldFirstName.text?.getWhitespaceTrimmedString()
        signUpViewRequestModel?.lastName = self.txtFieldLastName.text?.getWhitespaceTrimmedString()
        
        return signUpViewRequestModel!
    }
    
    //MARK: Navigation
    
    /**
     This function is used to make navigation to OTP Screen
     */
    func navigateToOTPVerificationScreenWithSignupModel(_ model: SignUpViewRequestModel){
        let OTPVerificationVC = UIViewController.getViewController(viewController: PhoneOTPVerificationViewController.self)
        OTPVerificationVC.initializeViewWithSignUpRequestModel(model: model)
        self.navigationController?.pushViewController(OTPVerificationVC, animated: true)
    }
}

//MARK: Pre-Registration OTP View Delegate
extension SignUpForm2ViewController: PreRegistrationOTPViewDelegate{
    
    func showToastWithMessage(_ message: String) {
        AppUtility.presentToastWithMessage(message)
    }

    
    func preRegistrationOTPSentSuccessfully() -> Void{
        print("Pre-Registration OTP Sent!")
        
        //Move To Phone OTP Screen
        self.navigateToOTPVerificationScreenWithSignupModel(self.signUpViewRequestModel!)
    }
}

extension SignUpForm2ViewController: SignUpViewDelegate{
    func signUpSuccessful(withResponseModel: LoginResponseModel){
        
    }
}
