//
//  YourBidsViewModel.swift
//  Wapanda
//
//  Created by Daffodil on 29/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
struct YourBidsViewModel {
    var limit                      : Int!
    var skip                       : Int!
    var status                     : String!
    var userType                   :UserType!
}
