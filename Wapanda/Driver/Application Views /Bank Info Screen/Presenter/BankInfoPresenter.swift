//
//  BankInfoPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class BankInfoPresenter: ResponseCallback{
    
    //MARK:- BankInfoPresenter local properties
    private weak var bankInfoViewDelegate             : BankInfoScreenViewDelgate?
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?
    private lazy var bankInfoBusinessLogic         : BankInfoBusinessLogic = BankInfoBusinessLogic()
    private var shouldShowLoaderOnScreen = true
    //MARK:- Constructor
    init(delegate responseDelegate:BankInfoScreenViewDelgate) {
        self.bankInfoViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        
        self.bankInfoViewDelegate?.hideLoader()
        if responseObject is StripeAuthenticationResponseModel {
            self.bankInfoViewDelegate?.stripeAuthenticationSuccessfull(withResponseModel: responseObject as! StripeAuthenticationResponseModel)
        }
        if responseObject is DriverStripeAPIConnectionResponseModel
        {
        self.bankInfoViewDelegate?.driverAPIStripeConnectionSuccessfull(withResponseModel: responseObject as! DriverStripeAPIConnectionResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.bankInfoViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.bankInfoViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendStripeAuthRequest(withData bankScreenRequestModel:BankInfoScreenRequestModel) -> Void{
        self.bankInfoViewDelegate?.showLoader()
        
        var bankRequestModel : BankInfoRequestModel!
            bankRequestModel = BankInfoRequestModel.Builder()
            .setCode(bankScreenRequestModel.code)
            .setClientId(bankScreenRequestModel.clienId)
            .setClientSecret(bankScreenRequestModel.clientSecret)
            .setGrantType(bankScreenRequestModel.grantType)
            .build()
        
    self.bankInfoBusinessLogic.performStripeAuthentication(withBankInfoRequestModel: bankRequestModel, presenterDelegate: self)
    }
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendDriverConnectRequest(withAuthCode stripeUserId:String,_ shouldShowLoader:Bool) -> Void{
        if shouldShowLoader
        {
            self.shouldShowLoaderOnScreen = shouldShowLoader
            self.bankInfoViewDelegate?.showLoader()
        }
        var bankRequestModel : BankInfoRequestModel!
        bankRequestModel = BankInfoRequestModel.Builder()
            .addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setAuthCode(stripeUserId)
            .build()
        
        self.bankInfoBusinessLogic.performBankInfo(withBankInfoRequestModel: bankRequestModel, presenterDelegate: self)
    }
}
