
//Note :- This class contains SendEmergencyAlert Buisness Logic

class SendEmergencyAlertBusinessLogic {
    
    
    deinit {
        print("SendEmergencyAlertBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a SendEmergencyAlertRequestModel
     
     - parameter inputData: Contains info for SendEmergencyAlert
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performSendEmergencyAlert(withSendEmergencyAlertRequestModel signUpRequestModel: SendEmergencyAlertRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        SendEmergencyAlertApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
        errorResolver.registerErrorCode(ErrorCodes.EMERGENCY_CONTACT_NOT_ADDED, message  : AppConstants.ErrorMessages.EMERGENCY_CONTACT_NOT_ADDED)
        
        
        return errorResolver
    }
}
