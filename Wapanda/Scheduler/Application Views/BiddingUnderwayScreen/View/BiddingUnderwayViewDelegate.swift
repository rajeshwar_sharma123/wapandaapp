//
//  BiddingUnderwayViewDelegate.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 14/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
//Notes:- This protocol is used as a interface which is used by BiddingUnderwayViewPresenter to tranfer info to BiddingUnderwayViewController

protocol BiddingUnderwayViewDelegate: BaseViewProtocol{
    
     func updateAuctionDetail(withResponseModel auctionDetail:AuctionDetailResponseModel)
    func updateDriverAuctionDetail(withResponseModel auctionDetail:BidDetailResponseModel)
    
     func updateAcceptOrCancelAuction(withResponseModel response:CommonResponseModel)
    
    
    
    
}
