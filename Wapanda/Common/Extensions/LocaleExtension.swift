/*
 This class is used to add extension for locale
 */

import Foundation

extension Locale{
    
    /**
     This function is used to get International dialling code from country code.
     
     - parameter code: text to be set on the label
     
     - returns: Dialling code string
     */
    
    static func diallingCodeFromCountryCode(code: String)->String{
        
        if let path = Bundle.main.path(forResource: "country", ofType: "json"){
            
            let countryData = try! Data(contentsOf: URL(fileURLWithPath: path))
            let jsonData = try! JSONSerialization.jsonObject(with: countryData, options: JSONSerialization.ReadingOptions.allowFragments) as! Array<Dictionary<String, String>>
            
            for element in jsonData{
                if element["code"] == code{
                    return element["dial_code"]!
                }
            }
        }
        
        return ""
    }
    
    static func countryCode()->String{
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            return countryCode
        }
        return ""
    }
}
