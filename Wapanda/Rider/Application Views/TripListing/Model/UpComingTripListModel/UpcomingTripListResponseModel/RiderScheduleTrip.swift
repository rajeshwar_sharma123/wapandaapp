//
//  Rider.swift
//
//  Created by  on 27/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RiderScheduleTrip:NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "_id"
    static let email = "email"
    static let emailVerified = "emailVerified"
    static let profileImageFileId = "profileImageFileId"
    static let phone = "phone"
    static let lastName = "lastName"
    static let firstName = "firstName"
  }

  // MARK: Properties
  public var id: String?
  public var email: String?
  public var emailVerified: Bool? = false
  public var profileImageFileId: String?
  public var phone: String?
  public var lastName: String?
  public var firstName: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    email <- map[SerializationKeys.email]
    emailVerified <- map[SerializationKeys.emailVerified]
    profileImageFileId <- map[SerializationKeys.profileImageFileId]
    phone <- map[SerializationKeys.phone]
    lastName <- map[SerializationKeys.lastName]
    firstName <- map[SerializationKeys.firstName]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    dictionary[SerializationKeys.emailVerified] = emailVerified
    if let value = profileImageFileId { dictionary[SerializationKeys.profileImageFileId] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.emailVerified = aDecoder.decodeObject(forKey: SerializationKeys.emailVerified) as? Bool
    self.profileImageFileId = aDecoder.decodeObject(forKey: SerializationKeys.profileImageFileId) as? String
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(emailVerified, forKey: SerializationKeys.emailVerified)
    aCoder.encode(profileImageFileId, forKey: SerializationKeys.profileImageFileId)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
  }

}
