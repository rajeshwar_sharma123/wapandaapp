
import UIKit
import IQKeyboardManager
import GooglePlaces
import AFNetworking

//MARK: Search Destination Address Handling...
extension RiderHomeViewController {

    func loadSetupSearchScreen() {

        // Do any additional setup after loading the view.
        self.presenterGooglePlaces = GooglePlaceViewPresenter(delegate: self)
        self.registerTableViewCells()
        self.customizeScreenUI()
        self.didAppearViewSettingSearchScreen()
    }
    
    func customizeScreenUI(){
        self.shadowViewHome.addShadow()
        self.shadowViewWork.addShadow()
        self.shadowViewLastVisited.addShadow()
        self.bgViewSearch.addShadow()
        self.handleLastVisitedShortcut()
    }
    
    func handleLastVisitedShortcut(){
        if AppUtility.isUserLogin(){
            if let _ = AppDelegate.sharedInstance.userInformation?.riderProfile?.addresses?.recentlyVisited{
                self.lastVisitedWidthLayoutConstraint.constant = 64.0
                self.viewLastVisited.isHidden = false
            }
            else{
                self.lastVisitedWidthLayoutConstraint.constant = 0.0
                self.viewLastVisited.isHidden = true
            }
        }
        else{
            self.lastVisitedWidthLayoutConstraint.constant = 0.0
            self.viewLastVisited.isHidden = true
        }
    }

    func didAppearViewSettingSearchScreen() {
        self.showSearchButton()
        self.setViewFrameMin()
        self.setViewMin()
        self.initalizeUserAddresess()
        
        self.txtFieldEnterDestination.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    func textFieldDidChange(){
       // self.startTimer()
        self.startSearchForPlaces()
    }
    
    func endTimer(){
        if let _ = timerEditingChanged{
            self.timerEditingChanged?.invalidate()
            self.timerEditingChanged = nil
        }
    }

    func setViewMin(){
        self.isMinimized = true
    }
    
    func setViewMax(){
        self.isMinimized = false
    }
    
    func setViewFrameMin(){
        self.viewDestinationSearch.frame = CGRect(x: 0, y: ScreenSize.size.height-150, width: ScreenSize.size.width, height: ScreenSize.size.height)
        self.viewDestinationSearch.translatesAutoresizingMaskIntoConstraints = true
    }
    
    func setViewFrameToMax(){
        self.viewDestinationSearch.frame = CGRect(x: 0, y: -30, width: ScreenSize.size.width, height: ScreenSize.size.height)
        self.viewDestinationSearch.translatesAutoresizingMaskIntoConstraints = true
    }
    
    func registerTableViewCells(){
        self.tableView.register(UINib(nibName: String(describing: AddressSearchResultTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: AddressSearchResultTableViewCell.self))
        self.tableView.register(UINib(nibName: String(describing: ImageViewWithTitleTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ImageViewWithTitleTableViewCell.self))
    }
    
    func showOptionBar(){
        self.viewOptions.isHidden = false
    }
    
    func hideOptionBar(){
        self.viewOptions.isHidden = true
    }
    
    func endEditing(){
        self.view.endEditing(true)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.hideBlurrEffect()
        self.minimizeAddressSearchView()
    }
    
    func showSearchButton(){
        self.btnBack.isUserInteractionEnabled = false
        self.btnBack.setImage(#imageLiteral(resourceName: "ic_location_hint"), for: .normal)
    }
    
    func showBackButton(){
        self.btnBack.isUserInteractionEnabled = true
        self.btnBack.setImage(#imageLiteral(resourceName: "ic_chevron_left"), for: .normal)
    }
    
    func initalizeUserAddresess(){
        
        if AppUtility.isUserLogin(){
            
            if let home = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
                home.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME
                self.dataSourceUserAddresses.append(home)
            }
            
            if let work = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
                work.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_WORK
                self.dataSourceUserAddresses.append(work)
            }
            
            if let recent = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.recentlyVisited{
                recent.address_type_title = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT
                self.dataSourceUserAddresses.append(recent)
            }
        }
    }
    
    func clearUserAddressess(){
        self.dataSourceUserAddresses = []
    }
}

//MARK: UITableView Delegate & Datasource...
extension RiderHomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource.count == 0{
            return self.dataSourceUserAddresses.count
        }
        else{
            return self.dataSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if dataSource.count == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageViewWithTitleTableViewCell.self)) as! ImageViewWithTitleTableViewCell

            let selectedAddress = self.dataSourceUserAddresses[indexPath.row]
            
            if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT{
                cell.bind(withTitle: selectedAddress.address!)
            }
            else if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME{
                cell.bind(withTitle: selectedAddress.address_type_title!,andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_home"))
            }
            else{
                cell.bind(withTitle: selectedAddress.address_type_title!, andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_work"))
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddressSearchResultTableViewCell.self)) as! AddressSearchResultTableViewCell
            let selectedAddress = self.dataSource[indexPath.row]
            cell.bind(withTitle: selectedAddress.attributedPrimaryText , WithStateAndCountry: selectedAddress.attributedSecondaryText ?? NSAttributedString(string: ""))
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
        
        if indexPath.row == 0{
            
            cell.roundCorners([.topLeft, .topRight], radius: 10.0)
        }
        else if indexPath.row == (self.dataSource.count - 1){
            
            cell.roundCorners([.bottomLeft, .bottomRight], radius: 10.0)
            
        }
        else if self.dataSource.count == 0 && indexPath.row == (self.dataSourceUserAddresses.count - 1){
            cell.roundCorners([.bottomLeft, .bottomRight], radius: 10.0)
        }
        
        //When we have only 1 object
        if self.dataSource.count == 0 && self.dataSourceUserAddresses.count == 1{
            cell.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10.0)
        }
        else if self.dataSource.count == 1 && self.dataSourceUserAddresses.count == 0{
            cell.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10.0)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataSource.count == 0{
           
            self.selectedDestinationAddressTitle = self.dataSourceUserAddresses[indexPath.row].address
            self.txtFieldEnterDestination.text = self.selectedDestinationAddressTitle
            self.handleUserAddressSelection(withAddress: self.dataSourceUserAddresses[indexPath.row])
        }
        else{
            
            //Get Place Detail
            let selectedPlace = self.dataSource[indexPath.row]
            if ReachabilityManager.shared.isNetworkAvailable
                {
            self.selectedDestinationAddressTitle = self.dataSource[indexPath.row].attributedFullText.string
            self.txtFieldEnterDestination.text = self.selectedDestinationAddressTitle
            self.placeDetailPresenter.sendGooglePlaceDetailRequest(withGooglePlaceDetailViewRequestModel: GooglePlaceDetailViewRequestModel(placeId: selectedPlace.placeID ?? ""))
            }
            else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
            }
        }
    }
    
    func performAddressSelection(withAddressData address: SelectedPlaceModel){
        self.selectedDestinationAddress = address
        
        switch addDestinationTypeSelected {
            
            case ADD_ADDRESS_TYPE.HOME:
                self.performAddHomeAddress(withAddressData: address)
            case ADD_ADDRESS_TYPE.WORK:
                self.performAddWorkAddress(withAddressData: address)
            case ADD_ADDRESS_TYPE.LAST_VISITED:
                //something might go here....
                break
            default:
                self.onDestinationSelection(withAddress: address)
                break
        }
    }
    
    func handleUserAddressSelection(withAddress address: Address){
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedDestinationAddressTitle, coordinate: CLLocationCoordinate2D(latitude: address.lat!, longitude: address.lng!))
        self.performAddressSelection(withAddressData: model)
    }
}

//MARK: Add Address Work ...
extension RiderHomeViewController{
    
    //MARK: Add address for shortcuts i.e. Home/Work
    func performAddWorkAddress(withAddressData address: SelectedPlaceModel){
        let addAddressRequestModel = AddAddressViewRequestModel(addressType: AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_WORK, lat: address.coordinate.latitude, lng: address.coordinate.longitude, address: address.title)
        self.presenterAddAddress.sendAddAddressRequest(withAddAddressViewRequestModel: addAddressRequestModel)
    }
    
    func performAddHomeAddress(withAddressData address: SelectedPlaceModel){
        let addAddressRequestModel = AddAddressViewRequestModel(addressType: AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_HOME, lat: address.coordinate.latitude, lng: address.coordinate.longitude, address: address.title)
        self.presenterAddAddress.sendAddAddressRequest(withAddAddressViewRequestModel: addAddressRequestModel)
    }
    
    func onDestinationSelection(withAddress address: SelectedPlaceModel){
        
//        self.dismissAddressSearchViewSettings()
        
        self.getSourceAddressAndMoveToCabSelectionScreen()
    }
    
    func dismissAddressSearchViewSettings(){
        //Handle View
        self.hideBlurrEffect()
        self.minimizeAddressSearchView()
        self.setViewMin()
        self.clearSearchResults()
        self.addDestinationTypeSelected = .NONE
    }
}

//MARK: Add Address View Delegate ...
extension RiderHomeViewController: AddAddressViewDelegate{
    func addAddressSuccessfully(withData user: LoginResponseModel){
        
        //Update User Detail
        AppDelegate.sharedInstance.userInformation = user
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        //Fetch And Update Addresses
        self.initalizeViewWithSavedAddresses()
        
        //Perform Destination Selection For Ride
        self.getSourceAddressAndMoveToCabSelectionScreen()
    }
}

//MARK: UITextField Delegate ...
extension RiderHomeViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if AppUtility.isUserLogin(){
            self.showSearchAddressView()
        }
        else{
            //Move To Signup/Login Screen
            self.navigateToUserWelcomeScreen()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.startSearchForPlaces()
        return true
    }
    
    //MARK: Helper Methods
    func showSearchAddressView(){
        if isMinimized{
            self.hideOptionBar()
            self.maximizeAddressSearchView()
            self.setViewMax()
        }
    }
    
    func startSearchForPlaces(){
        if let searchString = self.txtFieldEnterDestination.text, !searchString.isEmpty{
            let requestModel = GooglePlaceViewRequestModel(searchString: self.txtFieldEnterDestination.text?.getWhitespaceTrimmedString() ?? "", userLocationCoordinate: self.userCurrentLocationCoordinate)
            if ReachabilityManager.shared.isNetworkAvailable
            {
                   self.presenterGooglePlaces.sendGooglePlaceRequest(withGooglePlaceViewRequestModel: requestModel)
            }
            else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
            }
         
        }
        else{
            self.clearSearchResults()
        }
    }
    
    
    func clearSearchResults(){
        self.dataSource = []
        self.tableView.reloadData()
    }
}

//MARK: Google Place Delegate ...
extension RiderHomeViewController: GooglePlaceViewDelegate{
    
    func showSearchList(list: [GMSAutocompletePrediction]){
        self.dataSource = list
        self.tableView.reloadData()
    }
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
}

//MARK: Google Place Detail Delegate ...
extension RiderHomeViewController: GooglePlaceDetailViewDelegate{
    
    func placeDetailWithData(data: GMSPlace){
        
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedDestinationAddressTitle, coordinate: data.coordinate)
        self.performAddressSelection(withAddressData: model)
    }
}

//MARK: Cab Selection View Handling
extension RiderHomeViewController: CabSelectionBottomSheetViewDelegate{
    
    func prepareScreenToShowStartSelection(){
//        self.viewDestinationSearch.isHidden = true
        self.viewScreenContent.isHidden = true
        self.overlayView.isHidden = true
    }
    
    func performCabSelection(data: CabSelectionViewRequestModel){
        self.prepareScreenToShowStartSelection()
        
        if isCabSelectionScreenNotPresent{
            self.presentCabSelectionBottomSheetScreen(withViewDataSource: data)
        }
        else{
            self.updateCabSelectionView()
        }
    }
    
    func presentCabSelectionBottomSheetScreen(withViewDataSource data: CabSelectionViewRequestModel){
        self.isCabSelectionScreenNotPresent = false
        
        cabSelectionVC = UIViewController.getViewController(CabSelectionBottomSheetViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        cabSelectionVC!.modalPresentationStyle = .overCurrentContext
        cabSelectionVC!.bind(withDelegate: self, viewDataModel: data)
        cabSelectionVC?.view.backgroundColor = UIColor.clear
        
        let navController = UINavigationController(rootViewController: cabSelectionVC!)
        navController.modalPresentationStyle = .overCurrentContext
        navController.view.backgroundColor = UIColor.clear
        navController.isNavigationBarHidden = true
        
        self.present(navController, animated: true, completion: nil)
    }
    
    func updateCabSelectionView(){
        cabSelectionVC!.getCabs()
    }
    
    func createCabSelectionViewRequestModel(latFrom: Double, lngFrom: Double, latTo: Double, lngTo: Double, titleSource: String, titleDestination: String)->CabSelectionViewRequestModel{
        
        var dataSource = CabSelectionViewRequestModel()
        dataSource.sourceAddress = titleSource
        dataSource.destinationAddress = titleDestination
        
        dataSource.latFrom = latFrom
        dataSource.lngFrom = lngFrom
        dataSource.latTo = latTo
        dataSource.lngTo = lngTo
        
        return dataSource
    }
    
    //MARK: Cab Selection View Delegate
    func viewDidDismiss(){
        self.isCabSelectionScreenNotPresent = true
        self.showScrollUpButton()
    }
    
    func pushVC(withVC vc: UIViewController){
            cabSelectionVC?.hideBottomSheet()
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func drawRoute(withSource source: SelectedPlaceModel?, withDestination destination: SelectedPlaceModel?){
        
        self.selectedSourceAddress = source
        self.selectedDestinationAddress = destination
        
        if let _ = self.selectedSourceAddress, let _ = self.selectedDestinationAddress{
            self.getDirection()
        }
        else{
            AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderHomeScreen.NO_ROUTE_AVAILABLE_MESSAGE)
        }
    }
    
    func showScrollUpButton(){
        self.btnScrollUpCabView.isHidden = false
    }
    
    func hideScrollUpButton(){
        self.btnScrollUpCabView.isHidden = true
    }
    
    func showSideMenuButton(){
        self.addNavigationMenuButton()
    }
    
    func hideSideMenuButton(){
        self.hideMenuButton()
    }
}
