import UIKit

class BidDetailManager {
    
    deinit {
        print("BidDetailManager deinit")
    }
    
    /**
     This method is used for perform request of bids
     
     - parameter inputData: Contains info for BidsListRequestModel
     
     */
    func getBidDetail(with request: BidDetailRequestModel, presenterDelegate: ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForBidDetail()
        BidDetailAPIRequest().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for perform request of bids
     
     - parameter inputData: Contains info for BidsListRequestModel
     
     */
    func quoteDriverPrice(with request: PlaceBidRequestModel, presenterDelegate: ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerErrorForPlaceBid()
        PlaceBidAPIRequest().makeAPIRequest(withReqFormData: request, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForPlaceBid() -> ErrorResolver {
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
        errorResolver.registerErrorCode(ErrorCodes.AUCTION_CLOSED, message  : AppConstants.ErrorMessages.BID_CLOSED)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        return errorResolver
        
    }
    
    /**
     To add the possible error code that be possible while requesting for the bid detail
     */
    private func registerErrorForBidDetail() -> ErrorResolver {
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND_AUCTION)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        return errorResolver
        
    }

}
