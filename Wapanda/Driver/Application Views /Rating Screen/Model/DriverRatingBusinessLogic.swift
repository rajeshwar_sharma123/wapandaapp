//
//  DriverRatingBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class DriverRatingBusinessLogic {
    
    init(){
        print("DriverRatingBusinessLogic init \(self)")
    }
    deinit {
        print("DriverRatingBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform DriverRating With Valid Inputs(Driver Id) constructed into a DriverRatingRequestModel
     
     - parameter inputData: Contains info for Driver Rating
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performGetDriveRatingRequest(withDriverRatingRequestModel DriverRatingRequestModel: DriverRatingRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForDriverRatingList()
        DriverRatingAPIRequest().makeAPIRequest(withReqFormData: DriverRatingRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
        /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForDriverRatingList() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_AUTH, message  : AppConstants.ErrorMessages.INVALID_AUTH)
        return errorResolver
    }
}
