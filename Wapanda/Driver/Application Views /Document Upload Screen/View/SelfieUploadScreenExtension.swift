//
//  SelfieUploadScreenExtension.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/21/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
extension DocumentsUploadScreenViewController
{
    static let SELFIE_SCREEN_DESCRIPTION = "Take a clear photo of your face. Riders will see this when you’re en route to pick them up."
    static let SELFIE_SCREEN_TITLE = "Selfie Time"
    static let SELFIE_SCREEN_STEP = "Step 3 of 7"
    static let SELFIE_SCREEN_PLACEHOLDER_IMAGE = #imageLiteral(resourceName: "selfie_placeholder")
    static let SELFIE_SCREEN_PLACEHOLDER_BACKGROUND = UIColor.clear
    
    func navigateToVehicleInfoController(){
        
        let vehicleUploadVC = UIViewController.getViewController(DocumentsUploadScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
         //vehicleUploadVC.objProfileUploadDocument = self.objProfileUploadDocument
         vehicleUploadVC.uploadScreenType = UploadScreenType.Vehicle
        self.navigationController?.pushViewController(vehicleUploadVC, animated: true)
    }
    
    func initialiseSelfieUploadCellOfCollectionView(_ cell : DocumentPreviewCollectionViewCell,index:Int) -> DocumentPreviewCollectionViewCell
    {
        cell.imageViewPlaceHolder.backgroundColor = DocumentsUploadScreenViewController.SELFIE_SCREEN_PLACEHOLDER_BACKGROUND
         cell.imageViewPlaceHolder.image = DocumentsUploadScreenViewController.SELFIE_SCREEN_PLACEHOLDER_IMAGE
        cell.imageViewPreview.isHidden = false
        cell.btnAddImage.isHidden = false
        cell.imageViewPlaceHolder.isHidden = false
        cell.imageViewPreview.clipsToBounds = true
        cell.imageViewPlaceHolder.contentMode = .scaleAspectFit
        if let image = self.documentScreenRequestModel.addedImage
        {
            cell.btnAddImage.isHidden = true
            cell.imageViewPlaceHolder.isHidden = true
            cell.imageViewPreview.image = image
            return cell
        }
        guard let _ = self.objProfileUploadDocument.profileImageFileId else
        {
            return cell
        }
        cell.btnAddImage.isHidden = true
         cell.imageViewPlaceHolder.isHidden = false
        cell.imageViewPreview.kf.indicatorType = .activity
        cell.imageViewPreview.kf.setImage(with: URL(string:"\(AppConstants.URL.BASE_URL)/files/\(String(describing: (self.objProfileUploadDocument.profileImageFileId!)))?api_key=\(AppConstants.APIRequestHeaders.API_KEY_VALUE)"), placeholder: UIImage(), options: nil, progressBlock:nil) { (image, error, cacheType, url) in
             cell.imageViewPlaceHolder.isHidden = true
            }

        return cell
        
    }
    
    
}
