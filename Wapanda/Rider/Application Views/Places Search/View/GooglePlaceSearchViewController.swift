//
//  GooglePlaceSearchViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 17/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManager

protocol GooglePlaceSearchDelegate: class{
    func placeSelected(withDetail place: SelectedPlaceModel)
    func placeSearchScreenDidDismiss()
}

class GooglePlaceSearchViewController: BaseViewController {
    var searchString: String = ""
    var presenterGooglePlaces: GooglePlaceViewPresenter!
    @IBOutlet var tblViewSearchResult: UITableView!
    @IBOutlet var txtFieldAddress: UITextField!
    var dataSource: [GMSAutocompletePrediction] = []
    var searchReultDelegateAndDataSource: SearchAddressTableViewDataSourceAndDelegate!
    @IBOutlet weak var viewTop: UIView!
    weak var delegate: GooglePlaceSearchDelegate?
    var selectedPlace: SelectedPlaceModel?
    var placeholderText = "Search"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.searchReultDelegateAndDataSource = SearchAddressTableViewDataSourceAndDelegate()
        self.searchReultDelegateAndDataSource.delegate = self
        
        self.presenterGooglePlaces = GooglePlaceViewPresenter(delegate: self)
        self.txtFieldAddress.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        self.registerTableViewCells()
        
        self.tblViewSearchResult.setCornerCircular(10.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtFieldAddress.placeholder = placeholderText
//        self.viewTop.addShadow()
        self.addBlurrEffectOnView()
        self.setFirstResponderInitial()
    }
    
    func bind(titleSearchHeader: String){
        self.placeholderText = titleSearchHeader
    }
    
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnableAutoToolbar = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addBlurrEffectOnView(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha = 0.7
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubview(toBack: blurEffectView)
    }
    
    func setFirstResponderInitial(){
        self.txtFieldAddress.becomeFirstResponder()
    }

    @IBAction func btnBackClick(_ sender: Any) {
        self.dismissScreen()
    }
    
    func dismissScreen(){
        self.dismiss(animated: true) { 
            self.delegate?.placeSearchScreenDidDismiss()
        }
    }
    
    @IBAction func btnDoneClick(_ sender: Any) {
        if let _ = self.selectedPlace, self.txtFieldAddress.text == self.selectedPlace?.title{
            self.delegate?.placeSelected(withDetail: self.selectedPlace!)
        }
        
        self.dismissScreen()
    }
}

extension GooglePlaceSearchViewController{
    
    func registerTableViewCells(){
        self.tblViewSearchResult.registerTableViewCell(tableViewCell: ImageViewWithTitleTableViewCell.self)
        self.tblViewSearchResult.registerTableViewCell(tableViewCell: AddressSearchResultTableViewCell.self)
        self.tblViewSearchResult.delegate = self.searchReultDelegateAndDataSource
        self.tblViewSearchResult.dataSource = self.searchReultDelegateAndDataSource
    }
    
    //MARK: View Customization Helper Methods
    func initialViewCustomization(){
    }
}

//MARK: UITextField Delegate ...
extension GooglePlaceSearchViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //Clear data source
        self.clearSearchResults()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.startSearchForPlaces()
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let userEnteredString = textField.text
        
        let newString = (userEnteredString! as NSString).replacingCharacters(in: range, with: string) as NSString
        
        self.searchString = newString as String
        
        print(newString)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.searchString = ""

        return true
    }
    
    //MARK: Helper Methods
    func textFieldDidChange(){
        self.startSearchForPlaces()
    }
    
    func startSearchForPlaces(){
        
        if !searchString.isEmpty{
            let requestModel = GooglePlaceViewRequestModel(searchString: searchString.getWhitespaceTrimmedString(), userLocationCoordinate: LocationServiceManager.sharedInstance.currentLocation?.coordinate)
            self.presenterGooglePlaces.sendGooglePlaceRequest(withGooglePlaceViewRequestModel: requestModel)
        }
        else{
            self.clearSearchResults()
        }
    }
    
    func clearSearchResults(){
        self.dataSource = []
    }
}

//MARK: Google Place Delegate ...
extension GooglePlaceSearchViewController: GooglePlaceViewDelegate{
    
    func showSearchList(list: [GMSAutocompletePrediction]){
        self.searchReultDelegateAndDataSource.bind(dataSourceSearchResults: list, userAddress: [], searchText: self.searchString)
        self.registerTableViewCells()
        self.tblViewSearchResult.reloadData()
    }
}

extension GooglePlaceSearchViewController: SearchAddressTableViewDelegate{
    func addressSelected(withAddress address: SelectedPlaceModel){
        self.txtFieldAddress.text = address.title
        self.selectedPlace = address
    }
}

//MARK: BaseViewProtocol Methods
extension GooglePlaceSearchViewController{
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
