//
//  CounterPriceRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class CounterPriceRequestModel {
    
    //MARK:- CounterPriceRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var biddingId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.biddingId = builder.biddingID
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var biddingID: String!
        /**
         This method is used for setting TaxClassification
         
         - parameter taxClassification: String parameter that is going to be set on TaxClassification
         
         - returns: returning Builder Object
         */
        func setDriverCounterPrice(_ driverCounterPrice:Double) -> Builder{
            requestBody["driverCounterPrice"] = driverCounterPrice as AnyObject?
            return self
        }
        
        /**
         This method is used for setting User id
         
         - parameter biddingID: String parameter that is going to be set on biddingID
         
         - returns: returning Builder Object
         */
        func setUserId(_ biddingID:String)->Builder{
            self.biddingID = biddingID
            return self
        }

        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->CounterPriceRequestModel{
            return CounterPriceRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting login end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.DRIVER_COUNTER_PRICE, self.biddingId)
    }
    
}
