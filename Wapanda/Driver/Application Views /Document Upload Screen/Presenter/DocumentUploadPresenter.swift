//
//  DocumentUploadPresenter.swift
//  Wapanda
//


import Foundation
import ObjectMapper

class DocumentUploadPresenter: ResponseCallback{
    
    //MARK:- DocumentUploadPresenter local properties
    private weak var documentUploadViewDelegate     : DocumentUploadDelegate?
    private lazy var documentUploadBusinessLogic         : DocumentUploadBusinessLogic = DocumentUploadBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:DocumentUploadDelegate) {
        self.documentUploadViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.documentUploadViewDelegate?.hideLoader()
        
        if let _ = responseObject as? DocumentUploadResponseModel{
        self.documentUploadViewDelegate?.documentUploadedSuccessfully(withResponseModel: responseObject as! DocumentUploadResponseModel)
        }
        
        if let _ = responseObject as? InsuranceDoc{
            self.documentUploadViewDelegate?.insuranceDocUpdatedSuccessfully(withResponseModel: responseObject as! InsuranceDoc)
        }
        
        if let _ = responseObject as? VehicleDoc{
            self.documentUploadViewDelegate?.vehicleDocUpdatedSuccessfully(withResponseModel: responseObject as! VehicleDoc)
        }
        
        if let _ = responseObject as? LoginResponseModel{
            self.documentUploadViewDelegate?.selfieUploadedSuccessfully(withResponseModel: responseObject as! LoginResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.documentUploadViewDelegate?.hideLoader()
        
        _ = error.getErrorPayloadInfo()
        self.documentUploadViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    
    
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendInsuranceInfoSumbitRequest(withData documentScreenRequestModel:DocumentUploadScreenRequestModel) -> Void{
        
        self.documentUploadViewDelegate?.showLoader()
        let docUploadModel =  DocumentUploadRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setImageIds(documentScreenRequestModel.imageIds as! [String]).setUserId(AppDelegate.sharedInstance.userInformation.id!).setEndPoint(forType: .Insurance).build()
        self.documentUploadBusinessLogic.updateDriverDocuments(withImage: docUploadModel, presenterDelegate: self, returningClass: InsuranceDoc.self)
    }
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendSelfieSumbitRequest(withData documentScreenRequestModel:DocumentUploadScreenRequestModel) -> Void{
        
        
        self.documentUploadViewDelegate?.showLoader()
        let docUploadModel =  DocumentUploadRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setProfileImageFileId(documentScreenRequestModel.profileImageFileId).setUserId(AppDelegate.sharedInstance.userInformation.id!).setEndPoint(forType: .Selfie).build()
        self.documentUploadBusinessLogic.updateDriverDocuments(withImage: docUploadModel, presenterDelegate: self, returningClass: LoginResponseModel.self)

    }
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendVehicleInfoSumbitRequest(withData documentScreenRequestModel:DocumentUploadScreenRequestModel) -> Void{
        
        self.documentUploadViewDelegate?.showLoader()
        let docUploadModel =  DocumentUploadRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setImageId(documentScreenRequestModel.imageId).setUserId(AppDelegate.sharedInstance.userInformation.id!).setEndPoint(forType: .Vehicle).build()
        self.documentUploadBusinessLogic.updateDriverDocuments(withImage: docUploadModel, presenterDelegate: self, returningClass: VehicleDoc.self)
    }
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendUploadImageRequest(withImage image:Data) -> Void{
        
        self.documentUploadViewDelegate?.showLoader()
        let docUploadModel = DocumentUploadRequestModel.Builder().addRequestHeader(key: "Content-Type", value: "multipart/form-data").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setEndPoint(forType: .Upload).build()
        docUploadModel.imageData = image
        self.documentUploadBusinessLogic.uploadDocument(withImage: docUploadModel, presenterDelegate: self)
        
       
    }
}
