//
//  DriverTripScreenViewDelgate.swift
//  Wapanda
//
//  Created by Daffolapmac on 05/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//


import UIKit
import CoreLocation
import GoogleMaps
protocol DriverTripScreenViewDelgate: BaseViewProtocol {
    
    func rideStartedSuccessfully(withResponseModel:Trip)
    func rideEndedSuccessfully(withResponseModel:DriverTripResponseModel)
    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker,bearing:Double)
    func didCancelTrip()
    func didReceiveErrorOnCancelTrip(_ errorMessage:String)
    func didReceiveTripSnapShot(_ rideImage:UIImage)

}
