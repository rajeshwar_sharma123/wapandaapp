//
//  Items.swift
//
//  Created by on 16/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class RatingItems: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let ratedOn = "ratedOn"
    static let review = "review"
    static let rating = "rating"
  }

  // MARK: Properties
  public var ratedOn: String?
  public var review: String?
  public var rating: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    ratedOn <- map[SerializationKeys.ratedOn]
    review <- map[SerializationKeys.review]
    rating <- map[SerializationKeys.rating]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = ratedOn { dictionary[SerializationKeys.ratedOn] = value }
    if let value = review { dictionary[SerializationKeys.review] = value }
    if let value = rating { dictionary[SerializationKeys.rating] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.ratedOn = aDecoder.decodeObject(forKey: SerializationKeys.ratedOn) as? String
    self.review = aDecoder.decodeObject(forKey: SerializationKeys.review) as? String
    self.rating = aDecoder.decodeObject(forKey: SerializationKeys.rating) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(ratedOn, forKey: SerializationKeys.ratedOn)
    aCoder.encode(review, forKey: SerializationKeys.review)
    aCoder.encode(rating, forKey: SerializationKeys.rating)
  }

}
