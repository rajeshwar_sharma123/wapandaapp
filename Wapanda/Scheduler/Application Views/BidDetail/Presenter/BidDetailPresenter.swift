import UIKit
import ObjectMapper
import MapKit

class BidDetailPresenter: ResponseCallback {
    
    private weak var view: BidDetailViewDelegate!
    private var bidDetailManager = BidDetailManager()
    
    init(delegate: BidDetailViewDelegate){
        view = delegate
    }
    
    /**
     This method is used to get bids request available to driver
     - parameter params: user filter parameters
     */
    func getBidsDetail(id: String) {
        
        view.showLoader()
        
        let bidsRequestModel: BidDetailRequestModel  = BidDetailRequestModel
            .Builder()
            .addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setBidId(bidId: id)
            .build()
        
        bidDetailManager.getBidDetail(with : bidsRequestModel, presenterDelegate: self)
    }
    

    func addDriverQuote(bidId: String, price: Float, fareValues: FairValuesModel) {

        view?.showLoader()

        let request = PlaceBidRequestModel.Builder()
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setPrice(price: price)
            .setTypicalPrice(typicalPrice: Float(fareValues.typicalPrice))
            .setFairPrice(fairPrice: Float(fareValues.fairPrice))
            .setFairPriceLow(fairPriceLow: Float(fareValues.fairPriceLow))
            .setFairPriceHigh(fairPriceHigh: Float(fareValues.fairPriceHigh))
            .setMinimumPrice(minimumPrice: Float(fareValues.minFare))
            .setMaximumPrice(maximumPrice: Float(fareValues.maxFare))
            .setBidId(bidId: bidId)
            .build()

        bidDetailManager.quoteDriverPrice(with : request, presenterDelegate: self)
    }
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject: T){
        
        view.hideLoader()
        if let response = responseObject as? BidDetailResponseModel{
            self.view?.showDetail(bidDetail: response)
        } else {
            self.view?.bidPlaced()
        }
    }
    
    
    func servicesManagerError(error: ErrorModel){
        view?.hideLoader()
        self.view?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
}
