//
//  RiderScheduleDetailCell.swift
//  Wapanda
//
//  Created by Daffodil on 27/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class RiderScheduleDetailCell: UITableViewCell {
    
    @IBOutlet weak var mapImgView: UIImageView!
    @IBOutlet weak var driverImgView: UIImageView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var totalBillLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var driverRatingLabel: UILabel!
    @IBOutlet weak var plateNumLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var cancelView: UIView!
    
    @IBOutlet weak var contactView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.driverImgView.setCornerCircular(self.driverImgView.bounds.size.width/2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
