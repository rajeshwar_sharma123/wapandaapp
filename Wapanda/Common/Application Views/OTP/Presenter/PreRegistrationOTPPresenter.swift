
//Notes:- This class is used as presenter for PreRegistrationOTPPresenter

import Foundation
import ObjectMapper

class PreRegistrationOTPPresenter: ResponseCallback{
    
//MARK:- OTPPopupViewPresenter local properties
    private weak var preRegistrationOTPViewDelegate          : PreRegistrationOTPViewDelegate?
    private var preRegistrationOTPBusinessLogic              : PreRegistrationOTPBusinessLogic!
    
//MARK:- Constructor
    init(delegate responseDelegate:PreRegistrationOTPViewDelegate){
        self.preRegistrationOTPViewDelegate = responseDelegate
        self.preRegistrationOTPBusinessLogic = PreRegistrationOTPBusinessLogic()
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.preRegistrationOTPViewDelegate?.hideLoader()
        self.preRegistrationOTPViewDelegate?.preRegistrationOTPSentSuccessfully()
        self.preRegistrationOTPViewDelegate?.showToastWithMessage(AppConstants.ToastMessages.OTP_SENT)
    }
    
    func servicesManagerError(error : ErrorModel){
        self.preRegistrationOTPViewDelegate?.hideLoader()
        self.preRegistrationOTPViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- API calls
    
    func requestOTP(withEmail email:String?, phone: String, diallingCode: String){
        
        self.preRegistrationOTPViewDelegate?.showLoader()

        let otpPopupRequestModel:PreRegistrationOTPRequestModel = PreRegistrationOTPRequestModel.Builder()
            .setEmail(email)
            .setPhone(phone)
            .setDiallingCode(diallingCode)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.preRegistrationOTPBusinessLogic.performPreRegistrationOTP(withPreRegistrationOTPRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
}
