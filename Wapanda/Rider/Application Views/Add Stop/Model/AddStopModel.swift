//
//  AddStopModel.swift
//
//  Created by daffomac-31 on 26/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class AddStopModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stop = "stop"
    static let visited = "visited"
    static let id = "_id"
    static let address = "address"

  }

  // MARK: Properties
  public var stop: Stop?
  public var visited: Bool? = false
  public var id: String?
  public var address: String?


  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    stop <- map[SerializationKeys.stop]
    visited <- map[SerializationKeys.visited]
    id <- map[SerializationKeys.id]
    address <- map[SerializationKeys.address]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stop { dictionary[SerializationKeys.stop] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.visited] = visited
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.stop = aDecoder.decodeObject(forKey: SerializationKeys.stop) as? Stop
    self.visited = aDecoder.decodeBool(forKey: SerializationKeys.visited)
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String

  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(stop, forKey: SerializationKeys.stop)
    aCoder.encode(visited, forKey: SerializationKeys.visited)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(address, forKey: SerializationKeys.address)
  }

}
