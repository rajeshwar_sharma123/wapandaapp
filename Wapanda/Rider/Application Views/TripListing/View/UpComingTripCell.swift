//
//  UpComingTripCell.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 16/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class UpComingTripCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var driverTimeLabel: UILabel!
    @IBOutlet weak var yourBidLabel: UILabel!
    @IBOutlet weak var bidStatusLabel: UILabel!
    @IBOutlet weak var bidStatusView: UIBorderView!
    @IBOutlet weak var currentBidView: UIBorderView!
    @IBOutlet weak var currentBidLabel: UILabel!
    @IBOutlet weak var bidAcceptanceView: UIView!
    @IBOutlet weak var bidAcceptanceLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var transparentView: UIView!
    
    @IBOutlet weak var yourBidStaticLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindYourBidsDataWithCell(yourBids: YourBidsResponseModel, indexPath: IndexPath){
        self.selectionStyle = .none
        let item = yourBids.items?[indexPath.row]
        self.dateLabel.text = AppUtility.getTripDateFromDateString((item?.pickupDateTime)!)
        self.driverTimeLabel.text = AppUtility.getDriverApproxDriveTime(item!.estDuration!,endMsg: "DRIVE APPROX.")
        self.sourceLabel.text = item?.fromAddress
        self.destinationLabel.text = item?.toAddress
        
        if item?.lowestQuote?.price != nil{
            self.currentBidLabel.text = AppUtility.getFormattedPriceString(withPrice: (item?.lowestQuote?.price)!)
        }else{
            self.currentBidLabel.text = "$ --.--"
        }
        self.transparentView.isHidden = true
        self.bidAcceptanceLabel.text = TripStatus.CurrentBid.rawValue
        //self.bidStatusLabel.text = item?.status
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
           setDriverBidsStatusDetail(yourBids: yourBids, indexPath: indexPath)
        }else{
            setRiderBidsStatusDetail(yourBids: yourBids, indexPath: indexPath)
        }
        let size = self.bidStatusLabel.text?.getStingSize("Titillium-Regular", fontSize: 12)
        self.bidStatusLabel.frame.size.width = size!.width + 10
        self.bidStatusView.frame.size.width = self.bidStatusLabel.frame.size.width

    }
    
    func setRiderBidsStatusDetail(yourBids: YourBidsResponseModel, indexPath: IndexPath){
        let item = yourBids.items?[indexPath.row]
        self.bidStatusLabel.text = item?.status
        self.yourBidLabel.text = AppUtility.getFormattedPriceString(withPrice: (item?.riderSelectedFare)!)
        if item?.status == TripStatus.Open.rawValue{
            self.bidStatusView.backgroundColor = UIColor.tripOpenStatusBackgroundColor()
            self.bidStatusLabel.textColor = UIColor.tripOpenStatusTextColor()
            
        }else{
            self.bidStatusView.backgroundColor = UIColor.tripCancelledStatusBackgroundColor()
            self.bidStatusLabel.textColor = UIColor.appDarkGrayColor()
            self.transparentView.isHidden = false
        }
    }
    
    func setDriverBidsStatusDetail(yourBids: YourBidsResponseModel, indexPath: IndexPath){
        let item = yourBids.items?[indexPath.row]
        if item?.lowestQuote?.price != nil{
            
            if item?.yourBid?.price != nil{
                 self.yourBidLabel.text = AppUtility.getFormattedPriceString(withPrice: (item?.yourBid?.price)!)
            }else{
                 self.yourBidLabel.text = AppUtility.getFormattedPriceString(withPrice: (item?.lowestQuote?.price)!)
            }
            
        }
//        if item?.status == TripStatus.Accepted.rawValue{
//            self.bidStatusLabel.text = item?.status
//            self.bidStatusView.backgroundColor = UIColor.tripScheduledStatusBackgroundColor()
//            self.bidStatusLabel.textColor = UIColor.appThemeColor()
//            self.bidAcceptanceLabel.text = TripStatus.AcceptedCamelCase.rawValue
//            self.bidStatusLabel.text = TripStatus.Scheduled.rawValue
//
//            if item?.tripId != nil && item?.tripId?.status == TripStatus.Cancelled.rawValue{
//                self.bidStatusView.backgroundColor = UIColor.tripCancelledStatusBackgroundColor()
//                self.bidStatusLabel.textColor = UIColor.appDarkGrayColor()
//                self.transparentView.isHidden = false
//                self.bidAcceptanceLabel.text = TripStatus.CurrentBid.rawValue
//                self.bidStatusLabel.text = item?.tripId?.status
//            }
//
//        }
        
        if item?.status == TripStatus.Open.rawValue{
            self.bidStatusLabel.text = item?.status
            self.bidStatusView.backgroundColor = UIColor.tripOpenStatusBackgroundColor()
            self.bidStatusLabel.textColor = UIColor.tripOpenStatusTextColor()
            
        }else if item?.status == TripStatus.Cancelled.rawValue{
            self.bidStatusView.backgroundColor = UIColor.tripCancelledStatusBackgroundColor()
            self.bidStatusLabel.textColor = UIColor.appDarkGrayColor()
            self.transparentView.isHidden = false
            self.bidStatusLabel.text = TripStatus.Cancelled.rawValue
        }
        else{
            self.bidStatusView.backgroundColor = UIColor.tripCancelledStatusBackgroundColor()
            self.bidStatusLabel.textColor = UIColor.appDarkGrayColor()
            self.transparentView.isHidden = false
             self.bidStatusLabel.text = TripStatus.Closed.rawValue
            
        }
    }
    
   
    func bindDataWithCell(upcomingTripListResponse: UpcomingTripListResponseModel, indexPath: IndexPath){
        self.selectionStyle = .none
        let item = upcomingTripListResponse.items?[indexPath.row]
        self.dateLabel.text = AppUtility.getTripDateFromDateString((item?.pickupByTime)!)
        self.driverTimeLabel.text = AppUtility.getDriverApproxDriveTime(item!.estDuration!,endMsg: "DRIVE APPROX.")
        self.sourceLabel.text = item?.fromAddress
        self.destinationLabel.text = item?.toAddress
        setUpcomingTripDetail(upcomingTripListResponse: upcomingTripListResponse, indexPath: indexPath)
    }
  

    func setUpcomingTripDetail(upcomingTripListResponse: UpcomingTripListResponseModel, indexPath: IndexPath){
        let item = upcomingTripListResponse.items?[indexPath.row]
        //self.yourBidLabel.text = AppUtility.getFormattedPriceString(withPrice: (item?.agreedPrice)!)
        self.bidStatusLabel.text = item?.status
        self.currentBidLabel.text = AppUtility.getFormattedPriceString(withPrice: (item?.agreedPrice)!)
        
        self.transparentView.isHidden = true
        self.bidStatusLabel.text = TripStatus.Scheduled.rawValue
        self.bidStatusLabel.text = TripStatus.Scheduled.rawValue
        
        self.bidStatusLabel.textColor = UIColor.appThemeColor()
        self.bidAcceptanceLabel.text = TripStatus.AcceptedCamelCase.rawValue
        self.bidStatusView.backgroundColor = UIColor.tripScheduledStatusBackgroundColor()
        let size = self.bidStatusLabel.text?.getStingSize("Titillium-Regular", fontSize: 12)
        self.bidStatusLabel.frame.size.width = size!.width + 10
        self.bidStatusView.frame.size.width = self.bidStatusLabel.frame.size.width
        
        self.yourBidLabel.isHidden = true
        self.yourBidStaticLabel.isHidden = true
        self.bidStatusView.frame.origin.x = 35
        
    }

}
