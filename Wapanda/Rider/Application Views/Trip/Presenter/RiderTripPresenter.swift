
//Notes:- This class is used as presenter for RiderTripPresenter

import Foundation
import ObjectMapper
import GoogleMaps
class RiderTripPresenter: ResponseCallback{
    
    //MARK:- RiderTripPresenter local properties
    
    private weak var riderTripViewDelegate          : RiderTripViewDelegate?
    private lazy var cancelBusinessLogic         : CancelBusinessLogic = CancelBusinessLogic()
    
    //MARK:- Constructor
    
    init(delegate responseDelegate:RiderTripViewDelegate){
        self.riderTripViewDelegate = responseDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.riderTripViewDelegate?.hideLoader()
        if responseObject is CancelResponseModel
        {
            self.riderTripViewDelegate?.didCancelTrip()
        }
        
    }
    
    func servicesManagerError(error: ErrorModel){
        self.riderTripViewDelegate?.hideLoader()
        self.riderTripViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }

    //MARK : Helper Methods
    
    func update(cabMarker:GMSMarker,withNewLocation newLocation:TripDriverUpdateModel)
    {
       
        let lastCLLocation = CLLocation(latitude: cabMarker.position.latitude, longitude: cabMarker.position.longitude)
        let newCLLocation = CLLocation(latitude: Double((newLocation.driverLoc?.coordinates![1])!), longitude: Double((newLocation.driverLoc?.coordinates?[0])!))
        let _ = self.riderTripViewDelegate?.updateMarkerAtPosition(withPoints: CLLocationCoordinate2D(latitude: Double((newLocation.driverLoc?.coordinates![1])!), longitude: Double((newLocation.driverLoc?.coordinates?[0])!)), marker: cabMarker,bearing: self.getBearingBetweenTwoLocation(oldLocation: lastCLLocation, newLocation: newCLLocation))
        
    }
    
    func sendCancelRideRequest(withTripId tripId: String,andCancelby cancelBy:String) -> Void{
        
        self.riderTripViewDelegate?.showLoader()
        
        var cancelRequestModel : CancelRequestModel!
        
        
        cancelRequestModel = CancelRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setCancelledBy(cancelBy).setCancelReason("Any").setTripId(tripId).build()
        
        self.cancelBusinessLogic.performCancelRequest(withCancelRequestModel: cancelRequestModel, presenterDelegate: self)
    }
    //MARK:- Bearing Calculating methods
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoLocation(oldLocation : CLLocation, newLocation : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: oldLocation.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: oldLocation.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: newLocation.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: newLocation.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
}
