
//Notes:- This protocol is used as a interface which is used by UpdatedProfilePresenter to tranfer info to UpdatedProfileViewController

protocol UpdatedProfileViewDelegate:BaseViewProtocol {
    func updatedProfileFetchedSuccessfully(WithResponseModel data: LoginResponseModel)
}
