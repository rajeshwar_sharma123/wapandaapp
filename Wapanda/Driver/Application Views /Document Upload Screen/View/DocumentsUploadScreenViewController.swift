 //
 //  DocumentsUploadScreenViewController.swift
 //  Wapanda
 //
 //  Created by Daffomac-23 on 7/21/17.
 //  Copyright © 2017 Wapanda. All rights reserved.
 //
 
 enum UploadScreenType{
    case Insurance
    case Selfie
    case Vehicle
 }
 
 enum SCREEN_MODE{
    case NEW
    case EDITING
 }
 
 
 import UIKit
 import Kingfisher
 import MobileCoreServices
 import JHTAlertController
 import AVFoundation
 
 
 class DocumentsUploadScreenViewController: BaseViewController {
    
    internal var currentImage:UIImage!
    
    //MARK:- Local Variables
    var uploadScreenPresenter                : DocumentUploadPresenter!
    var documentScreenRequestModel = DocumentUploadScreenRequestModel()
    var addUploadScreenSkipButton : Bool = true
    var uploadScreenType:UploadScreenType!
    var objProfileUploadDocument:LoginResponseModel!
    var numberOfDocuments = 1
    var currentIndexSelected = 0
    var numberOfImagesUploadRequest = 0
    var shouldUpdateProfile = true
    var numberOfImagesUploadSuccessfull = 0
    private var imageUploadAlertController : UIAlertController!
    private let imagePicker = PKCCrop()
    var screenMode = SCREEN_MODE.NEW
    
    //MARK:- IBOutlets
    @IBOutlet weak var lblSteps: UILabel!
    @IBOutlet weak var lblUploadTitle: UILabel!
    @IBOutlet weak var pageControlDocuments: UIPageControl!
    @IBOutlet weak var lblUploadDespcription: UILabel!
    @IBOutlet weak var collectionViewPreview: UICollectionView!
    @IBOutlet weak var lblDescriptionText: UILabel!
    
    //MARK:- Documents Upload Screen Life Cycle Methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if shouldUpdateProfile
        {
            self.objProfileUploadDocument = AppDelegate.sharedInstance.userInformation
            self.intialSetupForView()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.shouldUpdateProfile = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Helper Methods
    
    /**
     This method is used for initial setups
     */
    internal func intialSetupForView(){
        self.setUpInitialDocumentView()
        self.setTitleForScreen()
        self.setDescriptionForScreen()
        self.setUpNavigationBar()
        self.setupActionSheetController()
        self.setUpPageContol()
        self.setUpImageCropType()
        self.setUpCollectionView()
        self.uploadScreenPresenter = DocumentUploadPresenter(delegate: self)
    }
    
    func setScreenMode(mode: SCREEN_MODE){
        self.screenMode = mode
    }
    
    private func setUpInitialDocumentView()
    {
        switch uploadScreenType! {
        case .Insurance:
            guard let _ = self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds else
            {
                return
            }
            self.documentScreenRequestModel.imageIds = (self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds)!
            self.numberOfDocuments = self.documentScreenRequestModel.imageIds.count == AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD ? AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD : self.documentScreenRequestModel.imageIds.count + 1
            break
        case .Selfie:
            guard let _ = self.objProfileUploadDocument.profileImageFileId else
            {
                return
            }
            self.documentScreenRequestModel.imageId = self.objProfileUploadDocument.profileImageFileId
            break
        case .Vehicle:
            guard let _ = self.objProfileUploadDocument.driverDocs?.vehicleDoc?.id else
            {
                return
            }
            self.documentScreenRequestModel.imageId = self.objProfileUploadDocument.driverDocs?.vehicleDoc?.id
            break
            
        }
        
        
    }
    
    private func setUpImageCropType()
    {
        self.imagePicker.delegate = self
        switch uploadScreenType! {
        case .Insurance:
            PKCCropManager.shared.cropType = .freeRateAndMargin
            break
        case .Selfie:
            PKCCropManager.shared.cropType = .rateAndNoneMarginCircle
            break
        case .Vehicle:
            PKCCropManager.shared.cropType = .freeRateAndMargin
            break
            
        }
    }
    /**
     This method is used for set up navigation bar view
     */
    private func setUpNavigationBar()
    {
        self.customizeNavigationBarWithTitle(navigationTitle:self.getStepNumberForScreen())
        self.customizeNavigationBackButton()
        if addUploadScreenSkipButton
        {
            self.addNavigationSkipButton()
        }
    }
    /**
     This method is used for set up navigation bar view
     */
    private func setUpPageContol()
    {
        self.pageControlDocuments.isHidden = self.numberOfDocuments == 1 ? true : false
        self.pageControlDocuments.currentPage = self.currentIndexSelected
        self.pageControlDocuments.numberOfPages = self.numberOfDocuments
    }
    /**
     This method is used for set up collection view
     */
    private func setUpCollectionView(){
        self.collectionViewPreview.delegate = self
        self.collectionViewPreview.dataSource = self
        self.collectionViewPreview.isPagingEnabled = true
        self.collectionViewPreview.scrollsToTop = true
    }
    
    /**
     This method is used to set title corresponding to the current screen.
     */
    private func setTitleForScreen()
    {
        switch uploadScreenType! {
        case .Insurance:
            self.lblUploadTitle.text = DocumentsUploadScreenViewController.INSURANCE_SCREEN_TITLE
            break
        case .Selfie:
            self.lblUploadTitle.text = DocumentsUploadScreenViewController.SELFIE_SCREEN_TITLE
            break
        case .Vehicle:
            self.lblUploadTitle.text = DocumentsUploadScreenViewController.VEHICLE_SCREEN_TITLE
            break
            
        }
    }
    
    /**
     This method is used to set description corresponding to the current screen.
     */
    private func setDescriptionForScreen()
    {
        switch uploadScreenType! {
        case .Insurance:
            self.lblDescriptionText.text = DocumentsUploadScreenViewController.INSURANCE_SCREEN_DESCRIPTION
            break
        case .Selfie:
            self.lblDescriptionText.text = DocumentsUploadScreenViewController.SELFIE_SCREEN_DESCRIPTION
            break
        case .Vehicle:
            self.lblDescriptionText.text = DocumentsUploadScreenViewController.VEHICLE_SCREEN_DESCRIPTION
            break
        }
    }
    
    /**
     This method is used to set step number corresponding to the current screen.
     */
    private func getStepNumberForScreen() -> String
    {
        switch uploadScreenType! {
        case .Insurance:
            return DocumentsUploadScreenViewController.INSURANCE_SCREEN_STEP
        case .Selfie:
            return DocumentsUploadScreenViewController.SELFIE_SCREEN_STEP
        case .Vehicle:
            return DocumentsUploadScreenViewController.VEHICLE_SCREEN_STEP
        }
    }
    
    /**
     This method is used to setup ActionSheetController
     */
    private func setupActionSheetController(){
        
        imageUploadAlertController = UIAlertController(title: "Take Photo from :", message: "", preferredStyle: .actionSheet)
        imageUploadAlertController.view.tintColor = UIColor.appThemeColor()
        
        let individualAction = UIAlertAction(title: "Camera", style: .default,handler: { (action:UIAlertAction!) in
            self.shouldUpdateProfile = false
            self.presentImagePickerView(with: UIImagePickerControllerSourceType.camera)
            self.imagePicker.photoCrop()
        })
        imageUploadAlertController.addAction(individualAction)
        
        let partnershipAction = UIAlertAction(title: "Gallery", style: .default, handler:{ (action:UIAlertAction!) in
            self.shouldUpdateProfile = false
            self.presentImagePickerView(with: UIImagePickerControllerSourceType.photoLibrary)
            self.imagePicker.photoCrop()
        })
        imageUploadAlertController.addAction(partnershipAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        imageUploadAlertController.addAction(cancelAction)
    }
    

    private func presentImagePickerView(with sourceType:UIImagePickerControllerSourceType)
    {
        if sourceType == .camera
        {
            if uploadScreenType == UploadScreenType.Vehicle || uploadScreenType == UploadScreenType.Insurance{
                self.imagePicker.cameraCrop(withDirection: .back)
                
            }else{
                self.imagePicker.cameraCrop(withDirection: .front)

            }
        }
        else
        {
            self.imagePicker.photoCrop()
        }
    }
    
    internal func updateInsuranceDocModel(withImage image:UIImage,andIndex index:Int)
    {
        self.documentScreenRequestModel.addedImages[self.currentIndexSelected] = image
        
        guard let _ = self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds
            else{
                self.numberOfDocuments = self.documentScreenRequestModel.addedImages.filter{$0 != nil}.count == AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD ? AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD : self.documentScreenRequestModel.addedImages.filter{$0 != nil}.count + 1
                self.setUpPageContol()
                self.collectionViewPreview.reloadData()
                return
        }
        
        guard index < (self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds?.count)!
            else{
                self.numberOfDocuments = self.documentScreenRequestModel.imageIds.filter{$0 != nil}.count + self.documentScreenRequestModel.addedImages.filter{$0 != nil}.count == AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD ? AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD : (self.documentScreenRequestModel.imageIds.filter{$0 != nil}.count + self.documentScreenRequestModel.addedImages.filter{$0 != nil}.count) + 1
                self.setUpPageContol()
                self.collectionViewPreview.reloadData()

                return
        }
        self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds?[self.currentIndexSelected] = ""
        self.documentScreenRequestModel.imageIds[self.currentIndexSelected] = nil
        self.numberOfDocuments = self.documentScreenRequestModel.imageIds.filter{$0 != nil}.count + self.documentScreenRequestModel.addedImages.filter{$0 != nil}.count == AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD ? AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD : (self.documentScreenRequestModel.imageIds.filter{$0 != nil}.count + self.documentScreenRequestModel.addedImages.filter{$0 != nil}.count) + 1
        self.setUpPageContol()
        self.collectionViewPreview.reloadData()
    }
    
    //MARK :- IBOutlet Action Methods
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        
        switch uploadScreenType! {
        case .Insurance:
            self.documentScreenRequestModel.addedImages = self.documentScreenRequestModel.addedImages.filter { $0 != nil }
            guard self.documentScreenRequestModel.addedImages.count == 0 else
            {
                self.numberOfImagesUploadRequest = 0
                self.numberOfImagesUploadSuccessfull = 0
                for index in 0..<self.documentScreenRequestModel.addedImages.count
                {
                    if let image = self.documentScreenRequestModel.addedImages[index]
                    {
                        self.currentIndexSelected = index
                        self.numberOfImagesUploadRequest = numberOfImagesUploadRequest + 1
                        self.uploadScreenPresenter.sendUploadImageRequest(withImage: UIImageJPEGRepresentation(image,1.0)!)
                        
                        
                    }
                }
                self.documentScreenRequestModel.addedImages =  [UIImage?](repeating: nil, count:AppConstants.ScreenSpecificConstant.UploadScreen.NUMBER_OF_DOCUMENTS_UPLOAD)
                return
            }
            
            guard let _ = self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds,objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds?.count != 0 else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ValidationErrors.SELECT_INSURANCE)
                return
            }
            
            //NAVIGATION
            if self.screenMode == .EDITING{
                self.popViewController()
            }
            else{
                self.navigateToSelfieController()
            }
            
            break
        case .Selfie:
            guard self.documentScreenRequestModel.addedImage == nil else
            {
                self.uploadScreenPresenter.sendUploadImageRequest(withImage: UIImageJPEGRepresentation(self.documentScreenRequestModel.addedImage,1.0)!)
                return
            }
            guard let _ = self.objProfileUploadDocument.profileImageFileId else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ValidationErrors.SELECT_SELFIE)
                return
            }
            
            //NAVIGATION
            if self.screenMode == .EDITING{
                self.popViewController()
            }
            else{
                self.navigateToVehicleInfoController()
            }
            
        case .Vehicle:
            guard self.documentScreenRequestModel.addedImage == nil else
            {
                self.uploadScreenPresenter.sendUploadImageRequest(withImage: UIImageJPEGRepresentation(self.documentScreenRequestModel.addedImage,1.0)!)
                return
            }
            guard let _ = self.objProfileUploadDocument.driverDocs?.vehicleDoc?.id else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ValidationErrors.SELECT_VEHICLE)
                return
            }
            
          guard let _ =  self.objProfileUploadDocument.driverDocs?.vehicleDoc?.imageId else
            {
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ValidationErrors.SELECT_VEHICLE)
                return
            }
            
            //NAVIGATION
            if self.screenMode == .EDITING{
                self.popViewController()
            }
            else{
                self.navigateToVehicleController()
            }
            
            break
        }
    }
    
    @IBAction func addPhotoButtonTapped(_ sender: Any) {
        if sender is UITapGestureRecognizer
        {
            self.currentIndexSelected = ((sender as! UITapGestureRecognizer).view?.tag)!
        }
        else
        {
            self.currentIndexSelected = (sender as! UIButton).tag
        }
        self.shouldUpdateProfile = false
        present(imageUploadAlertController, animated: true) {}
        
    }
    
    override func skipButtonClick() {
        self.shouldUpdateProfile = true
        switch uploadScreenType! {
        case .Insurance:
            self.navigateToSelfieController()
            break
        case .Selfie:
            self.navigateToVehicleInfoController()
            break
        case .Vehicle:
            self.navigateToVehicleController()
            break
            
        }
    }
    
    func popViewController(){
        self.navigationController?.popViewController(animated: true)
    }
 }
 
 // MARK :- UICollectionView Delegate Methods
 
 extension DocumentsUploadScreenViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
 {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfDocuments
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentPreviewCollectionViewCell", for: indexPath as IndexPath) as! DocumentPreviewCollectionViewCell
        cell.btnAddImage.tag        = indexPath.row
        cell.imageViewPreview.tag   = indexPath.row
        cell.btnAddImage.addTarget(self, action:#selector(addPhotoButtonTapped(_:)), for: .touchUpInside)
        cell.tapGestureImageViewPreview = UITapGestureRecognizer(target: self, action:#selector(addPhotoButtonTapped(_:)))
        cell.imageViewPreview.addGestureRecognizer(cell.tapGestureImageViewPreview)
        
        switch uploadScreenType! {
        case .Insurance:
            return self.initialiseInsuranceUploadCellOfCollectionView(cell, index: indexPath.row)
        case .Selfie:
            return self.initialiseSelfieUploadCellOfCollectionView(cell, index: indexPath.row)
        case .Vehicle:
            return self.initialiseVehicleUploadCellOfCollectionView(cell, index: indexPath.row)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collectionViewPreview.frame.width, height: self.collectionViewPreview.frame.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {}
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = self.collectionViewPreview.frame.size.width
        let currentPage: Float = Float(self.collectionViewPreview.contentOffset.x / pageWidth)
        if 0.0 != fmodf(currentPage, 1.0) {
            self.pageControlDocuments.currentPage = Int(currentPage) + 1
            
        }
        else {
            self.pageControlDocuments.currentPage = Int(currentPage)
        }
        self.currentIndexSelected = self.pageControlDocuments.currentPage
    }
    
 }
 extension DocumentsUploadScreenViewController: PKCCropDelegate{
    
    func pkcCropAccessPermissionsDenied() {
        
    }
    func pkcCropAccessPermissionsDenied(_ type: UIImagePickerControllerSourceType) {
        let alertController = JHTAlertController(
            title: type == .camera ? AppConstants.ScreenSpecificConstant.UploadScreen.TITLE_CAMERA_ACCESS_DISABLED : AppConstants.ScreenSpecificConstant.UploadScreen.TITLE_GALLERY_ACCESS_DISABLED,
            message: type == .camera ? AppConstants.ScreenSpecificConstant.UploadScreen.MESSAGE_CAMERA_SERVICE_DISABLED : AppConstants.ScreenSpecificConstant.UploadScreen.MESSAGE_GALLERY_SERVICE_DISABLED,
            preferredStyle: .alert)
        alertController.alertBackgroundColor = .white
        alertController.titleViewBackgroundColor = .white
        
        alertController.messageTextColor = .black
        alertController.titleTextColor = .black
        
        alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
        alertController.hasRoundedCorners = true
        
        alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
        alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
        let openAction = JHTAlertAction(title: "Open Settings", style: .default) { (action) in
           if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
//                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
//                } else {
//                    // Fallback on earlier versions
                //}
            }
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func pkcCropController() -> UIViewController {
        return self
    }
    func pkcCropImage(_ image: UIImage) {
        switch uploadScreenType! {
        case .Insurance:
            self.updateInsuranceDocModel(withImage:image,andIndex:self.currentIndexSelected)
            // self.objProfileUploadDocument.driverDocs?.insuranceDoc?.imageIds
            break
        case .Selfie: fallthrough
        case .Vehicle:
            self.documentScreenRequestModel.addedImage = image
            break
        }
        self.collectionViewPreview.reloadData()
    }
 }
 extension DocumentsUploadScreenViewController: DocumentUploadDelegate{
    
    func documentUploadedSuccessfully(withResponseModel objDocModel:DocumentUploadResponseModel)
    {
        switch uploadScreenType! {
        case .Insurance:
            self.numberOfImagesUploadSuccessfull = self.numberOfImagesUploadSuccessfull + 1
            self.documentScreenRequestModel.imageIds = self.documentScreenRequestModel.imageIds.filter{$0 != nil}
            self.documentScreenRequestModel.imageIds.append(objDocModel.id!)
            if self.numberOfImagesUploadRequest == self.numberOfImagesUploadSuccessfull
            {
                
                self.uploadScreenPresenter.sendInsuranceInfoSumbitRequest(withData: documentScreenRequestModel)
            }
            break
        case .Selfie:
            documentScreenRequestModel.profileImageFileId = objDocModel.id!
            self.uploadScreenPresenter.sendSelfieSumbitRequest(withData: documentScreenRequestModel)
            break
        case .Vehicle:
            documentScreenRequestModel.imageId = objDocModel.id!
            self.uploadScreenPresenter.sendVehicleInfoSumbitRequest(withData: documentScreenRequestModel)
            break
            
        }
    }
    func insuranceDocUpdatedSuccessfully(withResponseModel objInsurance:InsuranceDoc)
    {
        
        
        if let _ = self.objProfileUploadDocument.driverDocs
        {
            self.objProfileUploadDocument.driverDocs?.insuranceDoc = objInsurance
        }
        else
        {
            self.objProfileUploadDocument.driverDocs = DriverDocs(JSON: [:])
            self.objProfileUploadDocument.driverDocs?.insuranceDoc = objInsurance
        }
        AppDelegate.sharedInstance.userInformation = self.objProfileUploadDocument
        AppDelegate.sharedInstance.userInformation.saveUser()
        self.hideLoader()
        
        //NAVIGATION
        if (self.screenMode == SCREEN_MODE.EDITING){
            self.popViewController()
        }
        else{
            self.navigateToSelfieController()
        }
    }
    func vehicleDocUpdatedSuccessfully(withResponseModel objVehicelModel:VehicleDoc)
    {
        if let _ = self.objProfileUploadDocument.driverDocs
        {
            self.objProfileUploadDocument.driverDocs?.vehicleDoc = objVehicelModel
        }
        else
        {
            self.objProfileUploadDocument.driverDocs = DriverDocs(JSON: [:])
            self.objProfileUploadDocument.driverDocs?.vehicleDoc = objVehicelModel
        }
        AppDelegate.sharedInstance.userInformation = self.objProfileUploadDocument
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        //NAVIGATION
        if (self.screenMode == SCREEN_MODE.EDITING){
            self.popViewController()
        }
        else{
            
        self.navigateToVehicleController()
        }
        
    }
    func selfieUploadedSuccessfully(withResponseModel objProfileModel:LoginResponseModel)
    {
        self.objProfileUploadDocument.profileImageFileId = objProfileModel.profileImageFileId
        AppDelegate.sharedInstance.userInformation = self.objProfileUploadDocument
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        //NAVIGATION
        if (self.screenMode == SCREEN_MODE.EDITING){
            self.popViewController()
        }
        else{
            self.navigateToVehicleInfoController()
        }
    }
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.documentScreenRequestModel = DocumentUploadScreenRequestModel()
        self.intialSetupForView()
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
 }
