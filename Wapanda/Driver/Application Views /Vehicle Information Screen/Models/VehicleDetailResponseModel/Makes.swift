//
//  Makes.swift
//
//  Created by Daffodil on 22/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Makes: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let makeIsCommon = "make_is_common"
    static let makeDisplay = "make_display"
    static let makeCountry = "make_country"
    static let makeId = "make_id"
  }

  // MARK: Properties
  public var makeIsCommon: String?
  public var makeDisplay: String?
  public var makeCountry: String?
  public var makeId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    makeIsCommon <- map[SerializationKeys.makeIsCommon]
    makeDisplay <- map[SerializationKeys.makeDisplay]
    makeCountry <- map[SerializationKeys.makeCountry]
    makeId <- map[SerializationKeys.makeId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = makeIsCommon { dictionary[SerializationKeys.makeIsCommon] = value }
    if let value = makeDisplay { dictionary[SerializationKeys.makeDisplay] = value }
    if let value = makeCountry { dictionary[SerializationKeys.makeCountry] = value }
    if let value = makeId { dictionary[SerializationKeys.makeId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.makeIsCommon = aDecoder.decodeObject(forKey: SerializationKeys.makeIsCommon) as? String
    self.makeDisplay = aDecoder.decodeObject(forKey: SerializationKeys.makeDisplay) as? String
    self.makeCountry = aDecoder.decodeObject(forKey: SerializationKeys.makeCountry) as? String
    self.makeId = aDecoder.decodeObject(forKey: SerializationKeys.makeId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(makeIsCommon, forKey: SerializationKeys.makeIsCommon)
    aCoder.encode(makeDisplay, forKey: SerializationKeys.makeDisplay)
    aCoder.encode(makeCountry, forKey: SerializationKeys.makeCountry)
    aCoder.encode(makeId, forKey: SerializationKeys.makeId)
  }

}
