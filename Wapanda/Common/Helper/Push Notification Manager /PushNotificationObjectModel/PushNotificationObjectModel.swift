//
//  PushNotificationObjectModel.swift
//
//  Created by  on 19/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class PushNotificationObjectModel: NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let biddingDoc = "biddingDoc"
    static let eventName = "eventName"
    static let driverDoc = "driverDoc"
    static let notification = "notification"
    static let riderDoc = "riderDoc"
    static let userDoc = "userDoc"
    static let trip = "trip"
  }

  // MARK: Properties
  public var biddingDoc: BiddingDoc?
  public var eventName: String?
  public var driverDoc: DriverDocs?
  public var notification: WPNotification?
  public var riderDoc: RiderDoc?
  public var userDoc: UserDoc?
  public var trip: Trip?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }
    
    public override init() {
        super.init()
    }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    biddingDoc <- map[SerializationKeys.biddingDoc]
    eventName <- map[SerializationKeys.eventName]
    driverDoc <- map[SerializationKeys.driverDoc]
    notification <- map[SerializationKeys.notification]
    riderDoc <- map[SerializationKeys.riderDoc]
    userDoc <- map[SerializationKeys.userDoc]
    trip <- map[SerializationKeys.trip]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = biddingDoc { dictionary[SerializationKeys.biddingDoc] = value.dictionaryRepresentation() }
    if let value = eventName { dictionary[SerializationKeys.eventName] = value }
    if let value = driverDoc { dictionary[SerializationKeys.driverDoc] = value.dictionaryRepresentation() }
    if let value = notification { dictionary[SerializationKeys.notification] = value.dictionaryRepresentation() }
    if let value = riderDoc { dictionary[SerializationKeys.riderDoc] = value.dictionaryRepresentation() }
     if let value = trip { dictionary[SerializationKeys.trip] = value.dictionaryRepresentation() }
     if let value = userDoc { dictionary[SerializationKeys.userDoc] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.biddingDoc = aDecoder.decodeObject(forKey: SerializationKeys.biddingDoc) as? BiddingDoc
    self.eventName = aDecoder.decodeObject(forKey: SerializationKeys.eventName) as? String
    self.driverDoc = aDecoder.decodeObject(forKey: SerializationKeys.driverDoc) as? DriverDocs
    self.notification = aDecoder.decodeObject(forKey: SerializationKeys.notification) as? WPNotification
    self.riderDoc = aDecoder.decodeObject(forKey: SerializationKeys.riderDoc) as? RiderDoc
    self.userDoc = aDecoder.decodeObject(forKey: SerializationKeys.userDoc) as? UserDoc
    self.trip = aDecoder.decodeObject(forKey: SerializationKeys.trip) as? Trip
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(biddingDoc, forKey: SerializationKeys.biddingDoc)
    aCoder.encode(eventName, forKey: SerializationKeys.eventName)
    aCoder.encode(driverDoc, forKey: SerializationKeys.driverDoc)
    aCoder.encode(notification, forKey: SerializationKeys.notification)
    aCoder.encode(riderDoc, forKey: SerializationKeys.riderDoc)
    aCoder.encode(userDoc, forKey: SerializationKeys.userDoc)
    aCoder.encode(trip, forKey: SerializationKeys.trip)
  }

}
