
//Note :- This Class is used for Update tax info Service means it is used for handling Update tax Api

import UIKit

class AddPaymentAPIRequest: ApiRequestProtocol {

    //MARK:- local properties
    var apiRequestUrl:String!
    
    //MARK:- Helper methods
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: AddPaymentRequestModel which contains Request header and request body for the Tax Information api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeAPIRequest(withReqFormData reqFromData: AddPaymentRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        self.apiRequestUrl = reqFromData.getEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        
         ServiceManager.sharedInstance.requestDELETEWithURL(self.apiRequestUrl, andRequestDictionary: reqFromData.requestBody, requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: DeleteCardResponseModel.self)
    }
    
    func makeAPIRequestForCustomerId(withReqFormData reqFromData: AddPaymentRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        self.apiRequestUrl = AppConstants.URL.BASE_URL+reqFromData.getCustomerIdEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        
        ServiceManager.sharedInstance.requestGETWithURL(self.apiRequestUrl, requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: CustomerIdResponseModel.self)
    }
    /**
     This method is used to know that whether the api request is in progress or not
     
     - returns: Boolean value either true or false
     */
    func isInProgress() -> Bool {
        return ServiceManager.sharedInstance.isInProgress(self.apiRequestUrl)
    }
    
    /**
     This method is used to cancel the particular API request
     */
    func cancel() -> Void{
        ServiceManager.sharedInstance.cancelTaskWithURL(self.apiRequestUrl)
    }

}
