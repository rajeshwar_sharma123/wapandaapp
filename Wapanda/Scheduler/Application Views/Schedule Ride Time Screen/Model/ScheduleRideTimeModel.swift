//
//  RideScheduleTimeModel.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 12/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
struct ScheduleRideTimeModel {
    var latFrom                    : Float!
    var lngFrom                    : Float!
    var latTo                      : Float!
    var lngTo                      : Float!
    var pickupDateTime             : String!
    var fromAddress                : String!
    var toAddress                  : String!
    var carType                    : String!
    var riderSelectedFare          : Float!
}

