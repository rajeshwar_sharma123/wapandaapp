//
//  AcceptAuctionRequestModel.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 15/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//


import Foundation
class AcceptAuctionRequestModel {
    
    //MARK:- AcceptAuctionRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var auctionId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.auctionId = builder.auctionId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var auctionId: String!
        
        /**
         This method is used for setting auctionId
         
         - parameter auctionId: String parameter that is going to be set on auctionId
         
         - returns: returning Builder Object
         */
        func setAuctionId(_ auctionId: String)->Builder{
            self.auctionId = auctionId
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of AcceptAuctionRequestModel
         and provide AcceptAuctionRequestModel object.
         
         -returns : AcceptAuctionRequestModel
         */
        func build()->AcceptAuctionRequestModel{
            return AcceptAuctionRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting accept auction end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.ACCEPT_AUCTION, self.auctionId)
    }
    
}
