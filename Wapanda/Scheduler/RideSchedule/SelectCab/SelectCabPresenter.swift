import UIKit
import ObjectMapper

class SelectCabPresenter: ResponseCallback {
    
    private weak var view: SelectCabViewDelegate!
    private var manager = SchedulerBitManager()
    
    init(delegate: SelectCabViewDelegate){
        view = delegate
    }
    
    /**
     This method is used to make Car Make List request to business layer with valid Request model
     - returns : Void
     */
    func getAllVehicleTypes(latFrom: Double, lngFrom: Double, latTo: Double, lngTo: Double) -> Void{

        view.showLoader()
        
        let vehicleTypesRequestModel : VehicleRequestModel = VehicleRequestModel.Builder()
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setLatTo(Float(latTo))
            .setLngTo(Float(lngTo))
            .setLatFrom(Float(latFrom))
            .setLngFrom(Float(lngFrom))
            .build()
        
        SelectCabManager().performAllVehicleTypeRequest(withCarMakeRequestModel: vehicleTypesRequestModel, presenterDelegate: self as ResponseCallback)
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject: T){
        
        view.hideLoader()
        
        if let response = responseObject as? VehicleEstimatedPriceResponseModel {
            
            self.view?.refreshList(list: response.items)
        }
    }
    
    
    func servicesManagerError(error: ErrorModel){
        view.hideLoader()
        self.view?.hideLoader()
        self.view?.refreshList(list: [])
        self.view?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
}
