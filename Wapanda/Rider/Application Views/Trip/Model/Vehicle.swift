//
//  Vehicle.swift
//
//  Created by daffomac-31 on 29/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class Vehicle: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carType = "carType"
    static let id = "_id"
    static let carPlateNumber = "carPlateNumber"
    static let year = "year"
    static let imageId = "imageId"
    static let make = "make"
    static let model = "model"
  }

  // MARK: Properties
  public var carType: CarType?
  public var id: String?
  public var carPlateNumber: String?
  public var year: Int?
  public var imageId: String?
  public var make: String?
  public var model: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    carType <- map[SerializationKeys.carType]
    id <- map[SerializationKeys.id]
    carPlateNumber <- map[SerializationKeys.carPlateNumber]
    year <- map[SerializationKeys.year]
    imageId <- map[SerializationKeys.imageId]
    make <- map[SerializationKeys.make]
    model <- map[SerializationKeys.model]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carType { dictionary[SerializationKeys.carType] = value.dictionaryRepresentation() }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = carPlateNumber { dictionary[SerializationKeys.carPlateNumber] = value }
    if let value = year { dictionary[SerializationKeys.year] = value }
    if let value = imageId { dictionary[SerializationKeys.imageId] = value }
    if let value = make { dictionary[SerializationKeys.make] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? CarType
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.carPlateNumber = aDecoder.decodeObject(forKey: SerializationKeys.carPlateNumber) as? String
    self.year = aDecoder.decodeObject(forKey: SerializationKeys.year) as? Int
    self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.imageId) as? String
    self.make = aDecoder.decodeObject(forKey: SerializationKeys.make) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(carPlateNumber, forKey: SerializationKeys.carPlateNumber)
    aCoder.encode(year, forKey: SerializationKeys.year)
    aCoder.encode(imageId, forKey: SerializationKeys.imageId)
    aCoder.encode(make, forKey: SerializationKeys.make)
    aCoder.encode(model, forKey: SerializationKeys.model)
  }

}
