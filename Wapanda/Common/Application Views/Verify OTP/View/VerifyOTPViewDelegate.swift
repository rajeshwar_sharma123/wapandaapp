
//Notes:-

protocol VerifyOTPViewDelegate:BaseViewProtocol {
    func otpVerifiedSuccessfully() -> Void
    func showToastWithMessage(_ message:String) -> Void
}
