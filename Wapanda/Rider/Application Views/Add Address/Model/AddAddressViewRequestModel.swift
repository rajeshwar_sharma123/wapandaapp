
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct AddAddressViewRequestModel {
    
    var addressType                   : String!
    var lat                           : Double!
    var lng                          : Double!
    var address                       : String!
}
