
//Notes:- This protocol is used as a interface which is used by AddStopPresenter to tranfer info to AddStopViewController

protocol AddStopViewDelegate:BaseViewProtocol {
    func addStopSuccessfully(withTripModel data: Trip)
}
