//
//  TextFieldTypes.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
enum TextFieldsType{
    case EmailOrPhone
    case EmailAddress
    case PhoneNumber
    case Password
    case FirstName
    case LastName
    case OTP
    case OldPassword
    case NewPassword
    case CurrentPassword
    case Indiviual
    case Partnership
    case All  //all text fields for current view
}
