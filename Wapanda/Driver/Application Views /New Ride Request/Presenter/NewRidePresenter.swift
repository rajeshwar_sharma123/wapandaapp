//
//  NewRidePresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class NewRidePresenter: ResponseCallback{
    
    //MARK:- NewRidePresenter local properties
    private weak var newRideViewDelegate             : NewRideScreenViewDelgate?
    private lazy var newRideBusinessLogic         : NewRideBusinessLogic = NewRideBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:NewRideScreenViewDelgate) {
        self.newRideViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.newRideViewDelegate?.hideLoader()
        if responseObject is NewRideResponseModel
        {
        self.newRideViewDelegate?.newRideSubmittedSuccessful(withResponseModel: responseObject as! NewRideResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.newRideViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.newRideViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendAcceptRideRequest(withRideRequestModel newRideRequestModel: NewRideScreenRequestModel) -> Void{
        
        self.newRideViewDelegate?.showLoader()
        
        var acceptRequestModel : NewRideRequestModel!
        
        
            acceptRequestModel = NewRideRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setBiddingId(newRideRequestModel.biddingId).setLatitude(newRideRequestModel.lat).setLongitude(newRideRequestModel.lng).build()
        
            self.newRideBusinessLogic.performNewRideRequest(withNewRideRequestModel: acceptRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData taxScreenRequestModel:NewRideScreenRequestModel) -> Bool {
        
        return true
    }
}
