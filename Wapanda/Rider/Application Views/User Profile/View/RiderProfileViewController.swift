//
//  UserProfileViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 10/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import JHTAlertController

class RiderProfileViewController: BaseViewController, UpdateUserProfileViewDelegate {
    
    enum SCREEN_MODE{
        case EDIT
        case SAVE
    }
    
    @IBOutlet weak var viewBorderFirstName: UIView!
    @IBOutlet weak var viewBorderLastName: UIView!

    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewBorderEmail: UIView!
    @IBOutlet weak var txtFieldEmail: UITextField!
    
    @IBOutlet weak var lblErrorEmail: UILabel!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblErrorFirstName: UILabel!
    @IBOutlet weak var lblErrorLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblHomeAddress: UILabel!
    @IBOutlet weak var lblWorkAddress: UILabel!
    @IBOutlet weak var imgViewRider: UIImageView!
    @IBOutlet weak var viewEditProfileImage: UIBorderView!
    var screenMode = SCREEN_MODE.SAVE
    var selectedAddressTitle: String!
    var viewRequestModel: UpdateUserProfileViewRequestModel!
    var presenterUpdateUserProfile: UpdateUserProfileViewPresenter!
    var imageUploadAlertController : UIAlertController!
    let imagePicker: PKCCrop = PKCCrop()
    var uploadImagePresenter: DocumentUploadPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initView()
        self.setupNavigationBar()
        self.prepareScreenForSave()
        self.presenterUpdateUserProfile = UpdateUserProfileViewPresenter(delegate: self)
        self.setupActionSheetController()
        self.uploadImagePresenter = DocumentUploadPresenter(delegate: self)
        self.setUpImageCropType()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    private func setupNavigationBar() ->Void{
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.RiderProfile.RIDER_PROFILE_TITLE, color: UIColor.appThemeColor(), isTranslucent: false)
        self.customizeNavigationBackButton()
        self.addNavigationRightButtonWithTitle(title: AppConstants.ScreenSpecificConstant.Common.EDIT_BUTTON_TITLE)
    }
    
    override func backButtonClick() {
        if screenMode == .EDIT{
            //Move Back to save screen
            self.initView()
            self.prepareScreenForSave()
        }
        else{
            //Go back to home
            super.backButtonClick()
        }
    }
    
    func initView(){
        
        self.viewBorderFirstName.layer.borderWidth = 1.0
        self.viewBorderFirstName.layer.borderColor = UIColor.appSeperatorColor().cgColor
        
        self.viewBorderLastName.layer.borderWidth = 1.0
        self.viewBorderLastName.layer.borderColor = UIColor.appSeperatorColor().cgColor
        
        self.viewBorderEmail.layer.borderWidth = 1.0
        self.viewBorderEmail.layer.borderColor = UIColor.appSeperatorColor().cgColor
        
        self.txtFieldFirstName.text = AppDelegate.sharedInstance.userInformation.firstName ?? ""
        self.txtFieldLastName.text = AppDelegate.sharedInstance.userInformation.lastName ?? ""
        
         self.txtFieldEmail.text = AppDelegate.sharedInstance.userInformation.email ?? ""
        
        self.lblFirstName.text = AppDelegate.sharedInstance.userInformation.firstName ?? ""
        self.lblLastName.text = AppDelegate.sharedInstance.userInformation.lastName ?? ""
        
        self.lblEmail.text = AppDelegate.sharedInstance.userInformation.email ?? ""

        
        self.lblHomeAddress.text = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home?.address ?? "Add"
        self.lblWorkAddress.text = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work?.address ?? "Add"
        self.lblPhone.text = "+" + (AppDelegate.sharedInstance.userInformation.phone ?? "")
        self.lblEmail.text = AppDelegate.sharedInstance.userInformation.email ?? ""
        
        if let imgId = AppDelegate.sharedInstance.userInformation.profileImageFileId{
            let url = AppUtility.getImageURL(fromImageId: imgId)
            self.imgViewRider.setImageWith(URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_emergency_contact"))
        }
        
        //Pre-fill request model
        self.prepareInitalDataSource()
    }
    
    private func setUpImageCropType()
    {
        self.imagePicker.delegate = self
        PKCCropManager.shared.cropType = .rateAndNoneMarginCircle
    }

    
    func prepareInitalDataSource(){
        viewRequestModel = UpdateUserProfileViewRequestModel()
        viewRequestModel.firstName = AppDelegate.sharedInstance.userInformation.firstName
        viewRequestModel.lastName = AppDelegate.sharedInstance.userInformation.lastName
        
        viewRequestModel.email = AppDelegate.sharedInstance.userInformation.email

        viewRequestModel.profileImageFileId = AppDelegate.sharedInstance.userInformation.profileImageFileId
        
        if let work = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
            viewRequestModel.addresses = []
            
            let workAddress = UserAddress(lat: work.lat!, lng: work.lng!, address: work.address!)
            let address = UserAddresses(addresType: AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_WORK, address: workAddress)
            viewRequestModel.addresses!.append(address)
        }
        
        if let home = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
            
            if viewRequestModel.addresses == nil{
                viewRequestModel.addresses = []
            }
            
            let homeAddress = UserAddress(lat: home.lat!, lng: home.lng!, address: home.address!)
            let address = UserAddresses(addresType: AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_HOME, address: homeAddress)
            viewRequestModel.addresses!.append(address)
        }
    }
    
    func prepareDataSourceOnSaveClick(){
        viewRequestModel.firstName = self.txtFieldFirstName.text?.getWhitespaceTrimmedString()
        viewRequestModel.lastName = self.txtFieldLastName.text?.getWhitespaceTrimmedString()
        viewRequestModel.email = self.txtFieldEmail.text?.getWhitespaceTrimmedString()

    }
    
    private func prepareScreenForEdit(){
        self.lblFirstName.isHidden = true
        self.lblLastName.isHidden = true
        
//        self.lblEmail.isHidden = true
//        viewEmail.isUserInteractionEnabled = true
//        self.viewEmail.bringSubview(toFront: self.viewBorderEmail)

        self.viewFirstName.bringSubview(toFront: self.viewBorderFirstName)
        self.viewLastName.bringSubview(toFront: self.viewBorderLastName)
        self.screenMode = .EDIT
        self.viewEditProfileImage.isHidden = false
        self.addNavigationRightButtonWithTitle(title: AppConstants.ScreenSpecificConstant.Common.SAVE_BUTTON_TITLE)
    }
    
    func saveClick(){
        self.prepareDataSourceOnSaveClick()
        self.presenterUpdateUserProfile.sendUpdateUserProfileRequest(withUpdateUserProfileViewRequestModel: self.viewRequestModel)
    }
    
    func prepareScreenForSave(){
        self.lblErrorFirstName.text = ""
        self.lblFirstName.isHidden = false
        self.lblLastName.isHidden = false
        self.lblEmail.isHidden = false
        

        
        viewEmail.isUserInteractionEnabled = false
        self.lblFirstName.isHidden = false
        self.lblLastName.isHidden = false
        self.lblEmail.isHidden = false
        self.viewEmail.bringSubview(toFront: self.lblEmail)

        self.viewFirstName.bringSubview(toFront: self.lblFirstName)
        self.viewLastName.bringSubview(toFront: self.lblLastName)
        self.view.endEditing(true)
        self.screenMode = .SAVE
        self.viewEditProfileImage.isHidden = true
        self.addNavigationRightButtonWithTitle(title: AppConstants.ScreenSpecificConstant.Common.EDIT_BUTTON_TITLE)
    }

    override func rightButtonClick() {
        if self.screenMode == .EDIT{
            self.saveClick()
        }
        else{
            self.prepareScreenForEdit()
        }
    }
    
    @IBAction func btnSignOutClick(_ sender: Any) {
        self.logout()
    }
    
    @IBAction func btnHomeClick(_ sender: Any) {
        self.selectedAddressTitle = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_HOME
        self.showSearchPlaceScreenWithTitle(title: AppConstants.ScreenSpecificConstant.RiderProfile.SEARCH_HOME_ADDRESS_TITLE)
    }
    
    @IBAction func btnWorkClick(_ sender: Any) {
        self.selectedAddressTitle = AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_WORK
        self.showSearchPlaceScreenWithTitle(title: AppConstants.ScreenSpecificConstant.RiderProfile.SEARCH_WORK_ADDRESS_TITLE)
    }

    @IBAction func btnChangeProfilePic(_ sender: Any) {
        self.view.endEditing(true)
        
        present(imageUploadAlertController, animated: true) {}
    }
    
    func showSearchPlaceScreenWithTitle(title: String){
        let vcSearch = UIViewController.getViewController(GooglePlaceSearchViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        vcSearch.bind(titleSearchHeader: title)
        vcSearch.modalPresentationStyle = .overCurrentContext
        vcSearch.delegate = self
        self.present(vcSearch, animated: true, completion: nil)
    }
    
    func changeAddress(address: SelectedPlaceModel, addressType: String){
        var addressEdited = false
        
        if let addresses = self.viewRequestModel.addresses{
            //Replace existing address with new
            for object in addresses{
                if object.addressType == addressType{
                    addressEdited = true
                    let newAddress = UserAddress(lat: Double(address.coordinate.latitude), lng: Double(address.coordinate.longitude), address: address.title)
                    object.address = newAddress
                }
            }
        }
        
        if !addressEdited{
            //Add new address
            self.viewRequestModel.addresses = []
            let homeAddress = UserAddress(lat: Double(address.coordinate.latitude), lng: Double(address.coordinate.longitude), address: address.title)
            let address = UserAddresses(addresType: addressType, address: homeAddress)
            viewRequestModel.addresses!.append(address)
        }
    }
    
    func userProfileUpdatedSuccessfully(withResponse data: LoginResponseModel){
        AppDelegate.sharedInstance.userInformation = data
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        self.initView()
        self.prepareScreenForSave()
        
       AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderProfile.PROFILE_UPDATE_SUCCESS_MESSAGE)
    
       //Send Notification
       let notificationName = Notification.Name(AppConstants.NSNotificationNames.USER_PROFILE_UPDATED)
       NotificationCenter.default.post(name: notificationName, object: nil)
    }
}

//MARK: BaseViewProtocol Methods
extension RiderProfileViewController{
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}


extension RiderProfileViewController: GooglePlaceSearchDelegate{
    func placeSelected(withDetail place: SelectedPlaceModel){
        self.changeAddress(address: place, addressType: self.selectedAddressTitle)
        
        if self.selectedAddressTitle == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_HOME{
            self.lblHomeAddress.text = place.title
        }
        else{
            self.lblWorkAddress.text = place.title
        }
        
        self.saveClick()
    }
    
    func placeSearchScreenDidDismiss(){
    }
}

extension RiderProfileViewController{
    /**
     This method is used to setup ActionSheetController
     */
    func setupActionSheetController(){
        
        imageUploadAlertController = UIAlertController(title: "Take Photo from :", message: "", preferredStyle: .actionSheet)
        imageUploadAlertController.view.tintColor = UIColor.appThemeColor()
        
        let individualAction = UIAlertAction(title: "Camera", style: .default,handler: { (action:UIAlertAction!) in
            self.presentImagePickerView(with: UIImagePickerControllerSourceType.camera)
        })
        imageUploadAlertController.addAction(individualAction)
        
        let partnershipAction = UIAlertAction(title: "Gallery", style: .default, handler:{ (action:UIAlertAction!) in
            self.presentImagePickerView(with: UIImagePickerControllerSourceType.photoLibrary)
        })
        imageUploadAlertController.addAction(partnershipAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        imageUploadAlertController.addAction(cancelAction)
    }
    
    func presentImagePickerView(with sourceType:UIImagePickerControllerSourceType)
    {
        if sourceType == .camera
        {
            self.imagePicker.cameraCrop(withDirection: .front)
        }
        else
        {
            self.imagePicker.photoCrop()
        }
    }
}

extension RiderProfileViewController: PKCCropDelegate{
    
    func pkcCropAccessPermissionsDenied() {
        
    }
    func pkcCropAccessPermissionsDenied(_ type: UIImagePickerControllerSourceType) {
        
        DispatchQueue.main.async {

        let alertController = JHTAlertController(
            
            title: type == .camera ? AppConstants.ScreenSpecificConstant.UploadScreen.TITLE_CAMERA_ACCESS_DISABLED : AppConstants.ScreenSpecificConstant.UploadScreen.TITLE_GALLERY_ACCESS_DISABLED,
            message: type == .camera ? AppConstants.ScreenSpecificConstant.UploadScreen.MESSAGE_CAMERA_SERVICE_DISABLED : AppConstants.ScreenSpecificConstant.UploadScreen.MESSAGE_GALLERY_SERVICE_DISABLED,
            preferredStyle: .alert)
        alertController.alertBackgroundColor = .white
        alertController.titleViewBackgroundColor = .white
        
        alertController.messageTextColor = .black
        alertController.titleTextColor = .black
        
        alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
        alertController.hasRoundedCorners = true
        
        alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
        alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
        let openAction = JHTAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                //                if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                //                } else {
                //                    // Fallback on earlier versions
                //}
            }
        }
        alertController.addAction(openAction)
        
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func pkcCropController() -> UIViewController {
        return self
    }
    func pkcCropImage(_ image: UIImage) {
        self.uploadImagePresenter.sendUploadImageRequest(withImage: UIImageJPEGRepresentation(image, 1.0)!)

    }
}

extension RiderProfileViewController: DocumentUploadDelegate{
    
    func documentUploadedSuccessfully(withResponseModel objDocModel: DocumentUploadResponseModel) {
        self.viewRequestModel.profileImageFileId = objDocModel.id!
        
        if let imgId = objDocModel.id{
            let url = AppUtility.getImageURL(fromImageId: imgId)
            self.imgViewRider.setImageWith(URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_emergency_contact"))
        }

    }
    
    func insuranceDocUpdatedSuccessfully(withResponseModel objInsurance: InsuranceDoc) {
    }
    
    func vehicleDocUpdatedSuccessfully(withResponseModel objVehicelModel:VehicleDoc){
    }
    
    func selfieUploadedSuccessfully(withResponseModel objProfileModel:LoginResponseModel){
    }
}

extension RiderProfileViewController: UITextFieldDelegate, TextFieldValidationDelegate{
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType){
        switch forTextFields {
        case .FirstName:
            self.lblErrorFirstName.text = message
            break
        case .LastName:
            self.lblErrorLastName.text = message
            break
        case .EmailAddress:
            self.lblErrorEmail.text = message
            break
        default:
            break
        }
    }
    
    //MARK: UITextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField{
        case self.txtFieldFirstName:
            self.lblErrorFirstName.text = ""
        case self.txtFieldLastName:
            self.lblErrorLastName.text = ""
        case self.txtFieldEmail:
            self.lblErrorEmail.text = ""
        default:
            break
        }
        
        if string == "" || textField.text!.characters.count <= 25{
            return true
        }
        else{
            if textField == self.txtFieldEmail{
                return true
            }else{
                return false
                
            }
        }
    }
}




