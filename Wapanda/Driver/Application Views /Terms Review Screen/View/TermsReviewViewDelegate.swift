
//Notes:- This protocol is used as a interface which is used by TermsReviewPresenter to tranfer info to TermsReviewViewController

protocol TermsReviewViewDelegate:BaseViewProtocol {
    func termsReviewListFetchedSuccessfully(WithResponseModel data: TermsReviewResponseModel)
}
