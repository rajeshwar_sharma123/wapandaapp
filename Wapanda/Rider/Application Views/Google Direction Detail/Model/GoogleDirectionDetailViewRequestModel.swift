
//Notes:- This model is used as a model that holds signup properties from signup view controller.
import CoreLocation

struct GoogleDirectionDetailViewRequestModel {
    var source                        : CLLocationCoordinate2D!
    var destination                   : CLLocationCoordinate2D!
    var wayPoint                      : CLLocationCoordinate2D?
}
