
//Notes:- This protocol is used as a interface which is used by WapandaPresenter to tranfer info to WapandaViewController
import GooglePlaces

protocol GoogleDirectionDetailViewDelegate:BaseViewProtocol {
    func directionDetailWithData(data: GoogleDirectionResponse)
}
