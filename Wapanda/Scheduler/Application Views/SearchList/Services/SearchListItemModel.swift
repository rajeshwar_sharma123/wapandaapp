import UIKit
import ObjectMapper

class SearchListItemModel: Mappable {
    
    var fareAbove: Double?
    var pickupDateTime = ""
    var destAddress = ""
    var driverId = ""
    var isDeleted = false
    var title = ""
    var id = ""
    var lat: Double?
    var long: Double?
    var destLoc: [Double] = [Double]()
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        
        fareAbove <- map["fareAbove"]
        pickupDateTime <- map["pickupDateTime"]
        destAddress <- map["destAddress"]
        driverId <- map["driverId"]
        isDeleted <- map["isDeleted"]
        title <- map["title"]
        id <- map["_id"]
        destLoc <- map["destLoc.coordinates"]
        
        if destLoc.count == 2{
            lat = destLoc[0]
            long = destLoc[1]
        }
    }

}
