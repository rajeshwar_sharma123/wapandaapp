//
//  TaxInformationRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class TaxInformationRequestModel {
    
    //MARK:- TaxInformationRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var userId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.userId = builder.userID
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var userID: String!
        /**
         This method is used for setting TaxClassification
         
         - parameter taxClassification: String parameter that is going to be set on TaxClassification
         
         - returns: returning Builder Object
         */
        func setTaxClassification(_ taxClassification:String) -> Builder{
            requestBody["taxClassification"] = taxClassification as AnyObject?
            return self
        }
        
        /**
         This method is used for setting SocialSecurityNumber
         
         - parameter socialSecurityNumber: String parameter that is going to be set on SocialSecurityNumber
         
         - returns: returning Builder Object
         */
        func setSocialSecurityNumber(_ socialSecurityNumber:String)->Builder{
            requestBody["socialSecurityNumber"] = socialSecurityNumber as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Tax Id
         
         - parameter taxId: String parameter that is going to be set on Tax Id
         
         - returns: returning Builder Object
         */
        func setTaxId(_ taxId:String)->Builder{
            requestBody["taxId"] = taxId as AnyObject?
            return self
        }
        /**
         This method is used for setting User id
         
         - parameter userID: String parameter that is going to be set on userID
         
         - returns: returning Builder Object
         */
        func setUserId(_ userID:String)->Builder{
            self.userID = userID
            return self
        }

        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->TaxInformationRequestModel{
            return TaxInformationRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting login end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.DRIVER_TAX_INFO, self.userId)
    }
    
}
