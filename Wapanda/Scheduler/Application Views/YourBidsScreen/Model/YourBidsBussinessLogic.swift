//
//  YourBidsBussinessModel.swift
//  Wapanda
//
//  Created by Daffodil on 29/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
//Note :- This class contains YourBidsBussinessLogic Buisness Logic

class YourBidsBussinessLogic {
    
    
    deinit {
        print("YourBidsBussinessLogic deinit")
    }
    
    /**
     This method is used for perform auctionDetail With Valid Inputs constructed into a YourBidsRequestModel
     
     - parameter inputData: Contains info for YourBidsRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performYourBidsList(withYourBidsRequestModel yourBidsRequestModel: YourBidsRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerError()
        YourBidsAPIRequest().makeAPIRequest(withReqFormData: yourBidsRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerError() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ScreenSpecificErrorMessage.UpcomingTripList.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.ACCESS_DENIED, message  : AppConstants.ErrorMessages.ACCESS_DENIED)
        return errorResolver
    }
    
}

