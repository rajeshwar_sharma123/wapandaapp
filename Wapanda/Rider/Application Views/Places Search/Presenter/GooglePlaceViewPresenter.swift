
//Notes:- This class is used as presenter for GooglePlaceViewPresenter

import Foundation
import ObjectMapper
import GooglePlaces

class GooglePlaceViewPresenter: ResponseCallbackGeneral{
    
//MARK:- GooglePlaceViewPresenter local properties
    
    private weak var googlePlaceViewDelegate          : GooglePlaceViewDelegate?
    private lazy var googlePlaceBusinessLogic         : GooglePlaceBusinessLogic = GooglePlaceBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:GooglePlaceViewDelegate){
        self.googlePlaceViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:AnyObject>(responseObject : T){
        if let result = responseObject as? [GMSAutocompletePrediction]{
            self.googlePlaceViewDelegate?.showSearchList(list: result)
        }
        else{
            self.googlePlaceViewDelegate?.showSearchList(list: [])
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.googlePlaceViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call googlePlace Api.
    
    func sendGooglePlaceRequest(withGooglePlaceViewRequestModel googlePlaceViewRequestModel:GooglePlaceViewRequestModel){
        
        guard !googlePlaceViewRequestModel.searchString.isEmpty else {return}
        
        var bounds: GMSCoordinateBounds? = nil
        if let userLocation = googlePlaceViewRequestModel.userLocationCoordinate{
            bounds = GMSCoordinateBounds(coordinate: userLocation, coordinate: userLocation)
        }
        
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        
        let requestModel = GooglePlaceRequestModel.Builder()
                        .buildWithBounds(bounds: bounds)
                        .buildWithQuery(queryString: googlePlaceViewRequestModel.searchString)
                        .buildWithFilter(filter: filter).build()
        
        self.googlePlaceBusinessLogic.performGooglePlace(withGooglePlaceRequestModel: requestModel, presenterDelegate: self)
    }
}
