//
//  CancelPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class CancelPresenter: ResponseCallback{
    
    //MARK:- CancelPresenter local properties
    private weak var cancelViewDelegate             : CancelScreenViewDelgate?
    private lazy var cancelBusinessLogic         : CancelBusinessLogic = CancelBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:CancelScreenViewDelgate) {
        self.cancelViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.cancelViewDelegate?.hideLoader()
        if responseObject is CancelResponseModel
        {
            self.cancelViewDelegate?.didCancelTrip()
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.cancelViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.cancelViewDelegate?.didReceiveErrorOnCancelTrip( error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendCancelRideRequest(withTripId tripId: String,andCancelby cancelBy:String) -> Void{
        
        self.cancelViewDelegate?.showLoader()
        
        var cancelRequestModel : CancelRequestModel!
        
        
            cancelRequestModel = CancelRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setCancelledBy(cancelBy).setCancelReason("Any").setTripId(tripId).build()

            self.cancelBusinessLogic.performCancelRequest(withCancelRequestModel: cancelRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData taxScreenRequestModel:CancelScreenRequestModel) -> Bool {
        
        return true
    }
}
