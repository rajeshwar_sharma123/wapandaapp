
import UIKit
import MBProgressHUD
import JHTAlertController

class BaseViewController: UIViewController  {
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var noInternetVC: NoInternetViewController?
    var loader = CustomLoader()
    let bgView = UIView()
    var devicePresenter:DeviceRegisterScreenPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.internetRechableNotification), name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.INTERNET_RECHABLE_NOTIFICATION), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.internetUnreachableNotification), name: NSNotification.Name(rawValue: AppConstants.NSNotificationNames.INTERNET_UNREACHABLE_NOTIFICATION), object: nil)
        
        bgView.frame =  CGRect(x: 0, y: 0, width: ScreenSize.size.width, height: ScreenSize.size.height)
        bgView.backgroundColor = UIColor.darkGray
        bgView.alpha = 0.5
        self.devicePresenter = DeviceRegisterScreenPresenter(delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func internetRechableNotification(){
        if let vc = UIApplication.shared.visibleViewController as? NoInternetViewController{
           vc.dismiss(animated: true, completion: nil)
            self.setInternetViewDismissed()
        }
    }
    
    func setInternetViewDismissed(){
        self.noInternetVC = nil
    }
    
    
    func internetUnreachableNotification(){
        if let _ = UIApplication.shared.visibleViewController as? NoInternetViewController{
        }
        else{
        noInternetVC = UIViewController.getViewController(NoInternetViewController.self,storyboard: UIStoryboard.Storyboard.Main.object)
        UIApplication.shared.visibleViewController?.present(noInternetVC!, animated: true, completion: nil)
        }
    }
    
    /**
     This Method is used for showing loader
     
     - parameter VC: This object contains ViewController Object reference
     */
    
    func showLoader(_ VC : AnyObject?){
        
        let topMostController = VC
    
        UIApplication.shared.windows.first!.addSubview(self.bgView)

        DispatchQueue.main.async(execute: {
            self.loader.showLoader(OnView: UIApplication.shared.windows.first!)
//             MBProgressHUD.showAdded(to: (VC?.view)!, animated: true)
        })
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    /**
     This Method is used for hiding loader
     
     - parameter VC: This object contains ViewController Object reference
     */
    
    
    func hideLoader(_ VC : AnyObject?){
        
        self.bgView.removeFromSuperview()

        
        if(UIApplication.shared.isIgnoringInteractionEvents == true){
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        DispatchQueue.main.async(execute: {
//            MBProgressHUD.hide(for: VC!.view, animated: true)
            self.loader.hideLoader()
        })
    }
    
    /**
     This Method is used for showing Error Alert
     
     - parameter alertTitle:   This parameter contain Alert Title
     - parameter alertMessage: This parameter contain Alert Message that need to be shown
     - parameter VC:           ViewConroller On Which we have to show error alert
     */
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String,VC : AnyObject?){
        
        if alertMessage == AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION
        {
         noInternetVC = UIViewController.getViewController(NoInternetViewController.self,storyboard: UIStoryboard.Storyboard.Main.object)
         VC!.present(noInternetVC!, animated: true, completion: nil)
        }
        else if alertMessage == AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED
        {
            let unverified = UIViewController.getViewController(VerifyInProcessViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
            VC!.present(unverified, animated: true, completion: nil)
        }
        else if alertMessage == AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE
        {
            self.showErrorAlertForAccountDissable(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE, VC: VC)
        }
        else
        {
        if #available(iOS 8, *) {
            let alertController = JHTAlertController(title: alertTitle , message:
                alertMessage, preferredStyle: .alert)
            alertController.alertBackgroundColor = .white
            alertController.titleViewBackgroundColor = .white
            
            alertController.messageTextColor = .black
            alertController.titleTextColor = .black
            
            alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
            alertController.hasRoundedCorners = true
            
            alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
            alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
            alertController.addAction(JHTAlertAction(title: "OK", style: .cancel,handler: nil))
            
            VC!.present(alertController, animated: true, completion: nil)
            
        }
        else {
            let alertView = UIAlertView(title: alertTitle, message: alertMessage, delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK")
            alertView.show()
        }
        }
    }
    
    
    /**
     Show error Alert with cancel and Confirm button and callback
     
     - parameter alertTitle:   This parameter contain Alert Title
     - parameter alertMessage: This parameter contain Alert Message that need to be shown
     - parameter VC:           ViewConroller On Which we have to show error alert

     -returns : Void
     */
    func showErrorAlertWithCancelAndConfirmButtons(_ alertTitle: String, alertMessage: String,VC : AnyObject?){
        
        if #available(iOS 8, *) {
            let alertController = JHTAlertController(title: alertTitle , message:
                alertMessage, preferredStyle: .alert)
            alertController.alertBackgroundColor = .white
            alertController.titleViewBackgroundColor = .white
            
            alertController.messageTextColor = .black
            alertController.titleTextColor = .black
            
            alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
            alertController.hasRoundedCorners = true
            
            alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
            alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
            alertController.addAction(JHTAlertAction(title: "No", style: .default,handler:  { Void in
            }))
            alertController.addAction(JHTAlertAction(title: "Yes", style: .default,handler: { Void in
                self.errorAlertConfirmButtonClicked()
            }))

            VC!.present(alertController, animated: true, completion: nil)
        }
    }
    
    

    func showErrorAlertForAccountDissable(_ alertTitle: String, alertMessage: String,VC : AnyObject?){
        
        if #available(iOS 8, *) {
            let alertController = JHTAlertController(title: alertTitle , message:
                alertMessage, preferredStyle: .alert)
            alertController.alertBackgroundColor = .white
            alertController.titleViewBackgroundColor = .white
            
            alertController.messageTextColor = .black
            alertController.titleTextColor = .black
            
            alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
            alertController.hasRoundedCorners = true
            
            alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
            alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
            alertController.addAction(JHTAlertAction(title: "Ok", style: .default,handler:  { Void in
                self.logOutUser()
            }))
            VC!.present(alertController, animated: true, completion: nil)
        }
    }
    /**
     This Method is used for showing Error Alert with click handler
     
     - parameter alertTitle:   This parameter contain Alert Title
     - parameter alertMessage: This parameter contain Alert Message that need to be shown
     - parameter VC:           ViewConroller On Which we have to show error alert
     
     -returns : Void
     */
    
    func showErrorAlertWithHandler(_ alertTitle: String, alertMessage: String,VC : AnyObject?, clickHandler: @escaping () -> Void){
        
        if #available(iOS 8, *) {
            let alertController = JHTAlertController(title: alertTitle , message:
                alertMessage, preferredStyle: .alert)
            alertController.alertBackgroundColor = .white
            alertController.titleViewBackgroundColor = .white
            
            alertController.messageTextColor = .black
            alertController.titleTextColor = .black
            
            alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
            alertController.hasRoundedCorners = true
            
            alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
            alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
            alertController.addAction(JHTAlertAction(title: "Dismiss", style: .default,handler:  { Void in
                clickHandler()
//                self.dismiss(animated: true, completion: nil)
            }))
            VC!.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    func errorAlertConfirmButtonClicked(){
        
    }
    
    func errorAlertHandler(){
       self.dismiss(animated: true, completion: nil)
    }

    func navigatedToSpecifiedController<T:UIViewController>(viewController ofType:T.Type,storyboard:UIStoryboard)
    {
        var popped=false;
        let toViewController = storyboard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
        
        if let _ =  self.navigationController
        {
            for viewController in ((self.navigationController)?.viewControllers)!
            {
                if viewController.isKind(of: T.self)
                {
                    popped=true;
                    (self.navigationController)?.popToViewController(viewController, animated: true)
                    break
                }
            }
            
            if !popped {
                (self.navigationController)?.pushViewController(toViewController,animated:true);
            }
        }
        else
        {
            
            (self.navigationController)?.pushViewController(toViewController,animated:true);
        }
    }
    func getImageURL(_ imageId:String)->URL
    {
        return URL(string: "\(AppConstants.URL.BASE_URL)/files/\(imageId)?api_key=\(AppConstants.APIRequestHeaders.API_KEY_VALUE)")!
    }
     func readJson(_ fileName:String) -> [String: Any] {
        do {
            if let file = Bundle.main.url(forResource: fileName, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    }
    //MARK: Log out functionality
    
    /// This method is used to direct direct the user to Login screen if he perform any opera
     func logOutUser()
    {
        if AppUtility.isUserLogin(){
            self.deRegisterDeviceToken()
            self.devicePresenter.sendDriverOfflineRequest()
            UserDefaultUtility.removeAllUserDefault()
            SocketManager.shared.riderSocket.disconnect()
            SocketManager.shared.driverSocket.disconnect()
            LocationServiceManager.sharedInstance.stopDriverUpdateTimer()
        }
    
        AppInitialViewHandler.sharedInstance.setupInitialViewController()
    }
    
     func logout(){
        
        
        let alertController = JHTAlertController(title: AppConstants.ScreenSpecificConstant.Common.LOGOUT_TITLE , message:
            AppConstants.ScreenSpecificConstant.Common.LOGOUT_MESSAGE, preferredStyle: .alert)
        
        alertController.alertBackgroundColor = .white
        alertController.titleViewBackgroundColor = .white
        
        alertController.messageTextColor = .black
        alertController.titleTextColor = .black
        
        alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
        alertController.hasRoundedCorners = true
        
        alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
        alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
        
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.NO_TITLE, style: .cancel, handler: nil))
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.YES_TITLE, style: .default, handler: { (alertAction) in
            self.logOutUser()
        }))
        
        AppDelegate.sharedInstance.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
        
    }

    
    
     func registerDeviceToken()
    {
        if UserDefaultUtility.objectAlreadyExist(AppConstants.UserDefaultKeys.DEVICE_TOKEN)
        {
        self.devicePresenter.sendDeviceRegisterRequest(withData: DeviceRegisterScreenRequestModel(type: "ios", pushId: UserDefaultUtility.retrieveStringWithKey(AppConstants.UserDefaultKeys.DEVICE_TOKEN)))
        }
    }
     func deRegisterDeviceToken()
    {
        self.devicePresenter.sendDeviceDeregisterRequest()
        
    }
    func initialseAlertController(withTitle title:String,andMessage message:String)->JHTAlertController
    {
        let alertController = JHTAlertController(title: title , message:
            message, preferredStyle: .alert)
        
        alertController.alertBackgroundColor = .white
        alertController.titleViewBackgroundColor = .white
        
        alertController.messageTextColor = .black
        alertController.titleTextColor = .black
        
        alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
        alertController.hasRoundedCorners = true
        
        alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
        alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
        return alertController
    }
}
extension BaseViewController : DeviceRegisterDelegate
{
    func deRegisterDeviceSuccessfull(withResponseModel loginResponseModel: DeviceRegisterResponseModel) {
        
    }
    func registerDeviceSuccessfull(withResponseModel loginResponseModel: DeviceRegisterResponseModel) {
        
    }
    func driverOffilineSuccessful(withResponseModel taxResponseModel:LoginResponseModel)
     {
    
    }
    
}
