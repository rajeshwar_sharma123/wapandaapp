//
//  RiderTripViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 29/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import JHTAlertController

class RiderTripViewController: BaseViewController{
    
    @IBOutlet weak var _iLayoutConstraintClosedBottomSheet: NSLayoutConstraint!
    @IBOutlet weak var _iLayoutConstraintOpenBottomSheet: NSLayoutConstraint!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDriverRating: UILabel!
    @IBOutlet weak var lblCabMakeModel: UILabel!
    @IBOutlet weak var lblCabNumber: UILabel!
    @IBOutlet weak var viewGoogleMap: GMSMapView!
    @IBOutlet weak var imgViewRating: UIImageView!
    @IBOutlet weak var imgViewCancel: UIImageView!
    @IBOutlet weak var lblCancel: UILabel!
    @IBOutlet weak var imgViewContact: UIImageView!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var viewRiderImageBackground: UIView!
    @IBOutlet weak var imageViewRiderImage: UIImageView!
    
    var isTripStarted = false
    var isFirstTime = true
    var userCurrentLocationCoordinate: CLLocationCoordinate2D?
    var destinationLocationCoordinate: CLLocationCoordinate2D?
    var driverLocationCoordinate: CLLocationCoordinate2D?
    var cabMarker : GMSMarker!
    var destinationInfoWindowMarker: InfoWindow!
    var destinationAddressInfoWindowMarker: RouteAddressInfoWindow!
    var presenterGoogleDirection:GoogleDirectionDetailViewPresenter!
    var presenterCancelTrip:CancelPresenter!
    var presenterRiderTrip:RiderTripPresenter!
    var pushNotificationModel:PushNotificationObjectModel!
    
    var shouldShowLoader = true

    var polyline: GMSPolyline?
    @IBOutlet weak var viewETA: UIView!
    @IBOutlet weak var lblETATripDestination: UILabel!
    @IBOutlet weak var _iETAViewHeightContraintLayout: NSLayoutConstraint!
    
    var sendEmergencyRequestPresenter: SendEmergencyAlertViewPresenter!
    var wayPointMarkerAdded = false
    var presenterAddStop: AddStopViewPresenter!
    @IBOutlet weak var btnCancelTrip: UIButton!
    var markerAddStop: GMSMarker?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.presenterRiderTrip = RiderTripPresenter(delegate: self)
        //Add Stop Presenter
        self.presenterAddStop = AddStopViewPresenter(delegate: self)
        //Google Map initial customization
        self.viewGoogleMap.customizeMapAppearance()
        self.viewGoogleMap.delegate = self
        //Google Direction Settings
        self.googleDirectionSettingOnLoad()
        //Fetch Current location
        LocationServiceManager.sharedInstance.viewDelegate = self
        LocationServiceManager.sharedInstance.checkForUserPermissions()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setNotificationObject(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_STARTED_TRIP), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(setNotificationObjectForDriverArrived(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_ARRIVED), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(driverCancelledTrip(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.DRIVER_CANCELLED_TRIP), object: nil)
        self.populateTripData()
        if self.pushNotificationModel.trip?.status == TRIP_STATUS.CONFIRMED.rawValue
        {
            self.populateTripData()
        }
        else
        {
            self.initialiseStartTripView()
            
        }
        //Socket Manage
        self.initialiseTripSocket()
        //Hide ETA View For Start trip
        self.hideETAViewForStartTrip()
        self.setCornerRadiusOfDriverProfileImage()
    }
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.customizeViewAppearance()
        self.customizeNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    private func setCornerRadiusOfDriverProfileImage()
    {
        self.viewRiderImageBackground.layoutIfNeeded()
        
        self.imageViewRiderImage.clipsToBounds = true
        self.viewRiderImageBackground.clipsToBounds = true
        
        self.imageViewRiderImage.layer.cornerRadius = self.imageViewRiderImage.frame.width/2
        self.viewRiderImageBackground.layer.cornerRadius = self.viewRiderImageBackground.frame.width/2
    }
    /// This method is used to customize the navigation
    func customizeNavigationBar(){
        self.customizeNavigationBarWithTitleImage(image: #imageLiteral(resourceName: "ic_panda_navigation"))
        self.addNavigationMenuButton()
    }
    
    func customizeViewAppearance(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.imgViewRating.getImageViewWithImageTintColor(color: UIColor.appDarkGrayColor())
        
        if let stops = self.pushNotificationModel.trip?.stop, stops.count > 0 {
            self.disabledAddStop()
        }
        else{
            self.enableAddStop()
        }
    }
    private func initialiseTripSocket()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(updateCabLocation(_:)), name: Notification.Name(rawValue: AppConstants.NSNotificationNames.SOCKET_TRIP_UPDATE), object: nil)
        
        SocketManager.shared.riderSocket.disconnect()
        SocketManager.shared.initTripSocket(withTripId: (self.pushNotificationModel.trip?.id)!)
        
    }
    func setNotificationObject(_ objNotification:Notification)
    {
        self.pushNotificationModel = objNotification.object as! PushNotificationObjectModel
        self.initialiseStartTripView()
    }
    
    func setNotificationObjectForDriverArrived(_ objNotification:Notification){
        self.pushNotificationModel = objNotification.object as! PushNotificationObjectModel
        self.showDriverArrivedMessage()
    }
    
    func showDriverArrivedMessage(){
        AppUtility.showCustomToastMessage(withMessage: "", message: AppConstants.ScreenSpecificConstant.RiderTripScreen.DRIVER_ARRIVING_MESSAGE)
    }
    func driverCancelledTrip(_ notication:Notification)
    {
        let alertController = self.initialseAlertController(withTitle: AppConstants.ErrorMessages.ALERT_TITLE, andMessage: "Driver cancelled the ride")
        alertController.addAction(JHTAlertAction(title:"Ok", style: .default, handler: { (alertAction) in
               alertController.dismiss(animated: false, completion: {})
            AppInitialViewHandler.sharedInstance.setupInitialViewController()


        }))
        self.present(alertController, animated: true, completion: nil)

    }
    @objc func updateCabLocation(_ notication:Notification)
    {
        let cabUpdateObj = notication.object as! TripDriverUpdateModel
       
        if let _ = self.cabMarker
        {
            self.presenterRiderTrip.update(cabMarker: self.cabMarker, withNewLocation: cabUpdateObj)
            
        }
        
        if isTripStarted{
            self.showETAViewForStartTrip()
        }
        
        self.driverLocationCoordinate = CLLocationCoordinate2D(latitude: Double((cabUpdateObj.driverLoc?.coordinates![1])!), longitude: Double((cabUpdateObj.driverLoc?.coordinates?[0])!))
        self.getDirection(self.driverLocationCoordinate!)
//        self.viewGoogleMap.updateCameraLocation(lat: self.driverLocationCoordinate!.latitude, long: self.driverLocationCoordinate!.longitude,zoom: 15.0)
     
    }
    func initialiseStartTripView()
    {
        self.isTripStarted = true
        self.imgViewCancel.image = #imageLiteral(resourceName: "ic_add_stop")
        self.imgViewContact.image = #imageLiteral(resourceName: "ic_emergency")
        self.lblCancel.text = "Add Stop"
        self.lblContact.text = "Emergency"
        self.destinationLocationCoordinate = CLLocationCoordinate2D(latitude: Double((self.pushNotificationModel.trip?.endLoc?.coordinates![1])!), longitude: Double((self.pushNotificationModel.trip?.endLoc?.coordinates![0])!))
        self.viewGoogleMap.clear()
        self.cabMarker = nil
        self.polyline = nil
    }
    private func showETAViewForStartTrip(){
        self._iETAViewHeightContraintLayout.constant = 44.0
        self.viewETA.backgroundColor = UIColor.appDarkGrayColor()
    }
    
    private func hideETAViewForStartTrip(){
        self._iETAViewHeightContraintLayout.constant = 0.0
    }
    
    private func populateTripData()
    {
        self.destinationLocationCoordinate = CLLocationCoordinate2D(latitude: Double((self.pushNotificationModel.trip?.startLoc?.coordinates![1])!), longitude: Double((self.pushNotificationModel.trip?.startLoc?.coordinates![0])!))
        self.driverLocationCoordinate = CLLocationCoordinate2D(latitude: Double((self.pushNotificationModel.trip?.driver?.driverProfile?.lastLoc?.coordinates![1])!), longitude: Double((self.pushNotificationModel.trip?.driver?.driverProfile?.lastLoc?.coordinates![0])!))
        self.lblCabMakeModel.text = "\(self.pushNotificationModel.trip?.vehicle?.make ?? "") \(self.pushNotificationModel.trip?.vehicle?.model ?? "")"
        self.lblCabNumber.text = self.pushNotificationModel.trip?.vehicle?.carPlateNumber?.uppercased() ?? ""
        self.lblDriverName.text = self.pushNotificationModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? ""
        if let imageId = self.pushNotificationModel.trip?.driver?.profileImageFileId{
            let imgURL = AppUtility.getImageURL(fromImageId: imageId)
            self.imageViewRiderImage.setImageWith(URL(string: imgURL)!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
        }
        if let driverRating = self.pushNotificationModel.trip?.driver?.driverProfile?.averageRating
        {
            self.lblDriverRating.text = String(format: "%.1f", driverRating)
        }
        self.getDirection(self.driverLocationCoordinate!)
    }
    //MARK: Navigation Selector
    override func menuButtonClick() ->Void {
        super.menuButtonClick()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSplitFareClick(_ sender: Any) {
    }
    
    @IBAction func btnContactDriverClick(_ sender: Any) {
        
        if self.isTripStarted{
            //Emergency service
            self.performEmergencyService()
        }
        else{
            //Contact Driver
            self.performContactDriver()
        }
        
    }
    
    private func performContactDriver(){
        
        let contactVC = UIViewController.getViewController(viewController: ContactViewController.self)
        contactVC.modalPresentationStyle = .overCurrentContext
        
        let name = "\(self.pushNotificationModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")"
        let phone = self.pushNotificationModel.trip?.driver?.phone ?? ""
        var imageId = ""
        if let imgId = self.pushNotificationModel.trip?.driver?.profileImageFileId{
            imageId = imgId
        }
        contactVC.bindScreen(withName: name, phoneNumber: phone, imgId: imageId,pushModelObject:self.pushNotificationModel)
        self.present(contactVC, animated: true, completion: nil)
    }
    
    private func performEmergencyService(){
        if let emergencyContactNumber = AppDelegate.sharedInstance.userInformation.emergencyContact{
            //Perform  emergency call and message
            self.connectWithEmergency(withNumber: emergencyContactNumber)
        }
        else{
            //Open Add Emergency contact screen
            self.AddEmergencyContact()
        }
    }
    
    func connectWithEmergency(withNumber number: String){
        //Send Emergency Alert message
        self.sendEmergencyAlertMessage(onPhoneNumber: number)
        
        //Open phone dialler with emergency number
        AppUtility.callNumber(phoneNumber: number)
    }
    
    private func sendEmergencyAlertMessage(onPhoneNumber number: String){
        self.sendEmergencyRequestPresenter = SendEmergencyAlertViewPresenter(delegate: self)
        let modelEmergencyRequest = SendEmergencyAlertViewRequestModel(phone: number, text: "Emergency!!!")
        self.sendEmergencyRequestPresenter.sendSendEmergencyAlertRequest(withSendEmergencyAlertViewRequestModel: modelEmergencyRequest)
    }
    
    /// Show emergency for user
    private func AddEmergencyContact(){
        let emergencyVC = UIViewController.getViewController(EmergencyContactViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        emergencyVC.delegate = self
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(emergencyVC, animated: true)
    }
    
    @IBAction func btnCancelTripClick(_ sender: Any) {
        
        if self.isTripStarted{
            //Add Stop- when ride started 
            self.performAddStop()
        }
        else{
            //Cancel trip - while driver coming for pickup case
            self.performCancelRide()
        }
        
    }
    
    @IBAction func driverInfoTapped(_ sender: Any) {
        self.navigateToDriverRatingList()
    }
    
    //MARK:Navigation
    private func navigateToDriverRatingList()
    {
        let driverRatingVC =  UIViewController.getViewController(DriverRatingViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        driverRatingVC.driverInfoModel = self.getDriverInfoModel()
        self.present(driverRatingVC, animated: true, completion: {})
    }
    private func getDriverInfoModel() -> DriverInfoResponseModel
    {
        let driverInfoModel = DriverInfoResponseModel(JSON: [:])
        driverInfoModel?.firstName = "\(self.pushNotificationModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")"
        driverInfoModel?.id = "\(self.pushNotificationModel.trip?.driver?.id ?? "")"
        driverInfoModel?.profileImageFileId = "\(self.pushNotificationModel.trip?.driver?.profileImageFileId ?? "")"
        driverInfoModel?.driverProfile = self.pushNotificationModel.trip?.driver?.driverProfile
        driverInfoModel?.cabInfo = Cab(JSON: (self.pushNotificationModel.trip?.vehicle?.dictionaryRepresentation())!)
        return driverInfoModel!
    }
    private func performCancelRide(){
        self.shouldShowLoader = true
        let alertController = self.initialseAlertController(withTitle: AppConstants.ErrorMessages.ALERT_TITLE, andMessage: AppConstants.ErrorMessages.CANCEL_RIDE_CONFIRMATION)
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.NO_TITLE, style: .cancel, handler: nil))
        alertController.addAction(JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.YES_TITLE, style: .default, handler: { (alertAction) in
            if let tripId = self.pushNotificationModel.trip?.id
            {
                self.presenterRiderTrip.sendCancelRideRequest(withTripId:tripId, andCancelby: "RIDER")
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func performAddStop(){
        if let stops = pushNotificationModel.trip?.stop, stops.count > 0{
        }
        else{
            self.showAlertForOnAddStop()
        }
    }
    
    private func showAlertForOnAddStop(){
        let alertActionCancel = JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.Cancel_TITLE, style: .cancel, handler: nil)
        let alertActionOk = JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.OK_TITLE, style: .default, handler: { (alertAction) in
            self.showSearchPlaceScreen()
        })
        AppUtility.showCustomAlert(title: AppConstants.ErrorMessages.ALERT_TITLE, message: AppConstants.ScreenSpecificConstant.RiderTripScreen.ADD_STOP_ALERT_MESSAGE, presentingVC: self, actionItems: [alertActionCancel, alertActionOk])
    }
    
    private func showSearchPlaceScreen(){
        
        self.hideMenuButton()
        
        let googlePlaceSearchScreen = UIViewController.getViewController(GooglePlaceSearchViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        googlePlaceSearchScreen.modalPresentationStyle = .overCurrentContext
        googlePlaceSearchScreen.delegate = self
        self.present(googlePlaceSearchScreen, animated: true, completion: nil)
    }
    
    
    func enableAddStop(){
        self.imgViewCancel.alpha = 1.0
        self.lblCancel.alpha = 1.0
        self.btnCancelTrip.isUserInteractionEnabled = true
    }
    
    func disabledAddStop(){
        self.imgViewCancel.alpha = 0.5
        self.lblCancel.alpha = 0.5
        self.btnCancelTrip.isUserInteractionEnabled = false
    }
    
    @IBAction func myLocationTapped(_ sender: Any) {
        if let currentLocation = self.userCurrentLocationCoordinate{
            self.viewGoogleMap.updateCameraLocation(lat: currentLocation.latitude, long: currentLocation.longitude, zoom: 15.0)
        }
    }
    
    @IBAction func onbottomSheetTapGestureRecognition(_ sender: UITapGestureRecognizer) {
        self.bottomSheetViewHandler()
    }
    
    @IBAction func onBottomSheetSwipeGestureRecognition(_ sender: Any) {
        self.bottomSheetViewHandler()
    }
    
    func bottomSheetViewHandler(){
        self._iLayoutConstraintClosedBottomSheet.isActive = !self._iLayoutConstraintClosedBottomSheet.isActive
        self._iLayoutConstraintOpenBottomSheet.isActive = !self._iLayoutConstraintOpenBottomSheet.isActive
        UIView.animate(withDuration: 0.2 , delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            
        })
    }
    
    func hideBottomSheet(){
        self._iLayoutConstraintClosedBottomSheet.isActive = true
        self._iLayoutConstraintOpenBottomSheet.isActive = false
        UIView.animate(withDuration: 0.2 , delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            
        })
    }
    
    func showFeedbackScreen(){
        let feedbackVC = UIViewController.getViewController(FeedbackViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        
        //Prepare feedback screen model
        let driverName = (self.pushNotificationModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")
        let driverImageId = (self.pushNotificationModel.trip?.driver?.profileImageFileId ?? "")
        let requestModel = FeedbackViewRequestModel(driverName: driverName, driverImageId: driverImageId, tripId: (self.pushNotificationModel.trip?.id ?? ""), rate: 5, reivew: nil,pushObjModel:self.pushNotificationModel)
        
        feedbackVC.bind(withRequestModel: requestModel)
        feedbackVC.modalPresentationStyle = .overCurrentContext
        self.present(feedbackVC, animated: true, completion: {})
    }
}

//MARK: Map View Delegate
extension RiderTripViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
            }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
            self.hideBottomSheet()
    }
    
}
extension RiderTripViewController: WelcomeScreenViewDelegate{
    func showControllerWithVC(controller: UIViewController){
        self.present(controller, animated: true, completion: nil)
    }
    
    func locationUpdated(lat: Double, long: Double){
        if isFirstTime
        {
            self.userCurrentLocationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            if let currentLocation = self.userCurrentLocationCoordinate{
                self.viewGoogleMap.updateCameraLocation(lat: currentLocation.latitude, long: currentLocation.longitude, zoom: 15.0)
            }
            self.isFirstTime = false
        }
        
    }
}
extension RiderTripViewController: GoogleDirectionDetailViewDelegate{
    
    func googleDirectionSettingOnLoad(){
        self.presenterGoogleDirection = GoogleDirectionDetailViewPresenter(delegate: self)
    }
    
    func getDirection(_ cabLocation:CLLocationCoordinate2D){
        if ReachabilityManager.shared.isNetworkAvailable
        {
            var wayPoint: CLLocationCoordinate2D?
            
            if let wayPoints = self.pushNotificationModel.trip?.stop?.first, !wayPoints.visited!{
                wayPoint = CLLocationCoordinate2D(latitude: Double(wayPoints.stop!.coordinates!.last!), longitude: Double(wayPoints.stop!.coordinates!.first!))
            }
            
            self.presenterGoogleDirection.sendGoogleDirectionDetailRequest(withGoogleDirectionDetailViewRequestModel: GoogleDirectionDetailViewRequestModel(source: cabLocation, destination: self.destinationLocationCoordinate!, wayPoint: wayPoint), shouldShowLoader: self.shouldShowLoader)
        }
        else
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ErrorMessages.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, VC: self)
        }
        
    }
    
    //MARK: Direction View Delegate
    func directionDetailWithData(data: GoogleDirectionResponse){
        self.shouldShowLoader = false
        
        if let stop = self.pushNotificationModel.trip?.stop?.first, !stop.visited!{
            if self.markerAddStop == nil{
                self.markerAddStop = self.viewGoogleMap.addMarkerWithCoordinates(coordinate:  CLLocationCoordinate2D(latitude: Double(stop.stop!.coordinates!.last!), longitude: Double(stop.stop!.coordinates!.first!)), markerImage: #imageLiteral(resourceName: "ic_destination"))
            }
        }
        
        if let routes = data.routes, routes.count > 0{
            self.routeAavailableWithData(data: data)
        }
        else{
            //No Route Available Settings
            self.noRouteAvailableSettings()
        }
    }
    
    func routeAavailableWithData(data: GoogleDirectionResponse){
        
        if let polyline = self.polyline{
            
            //Update Route
            self.polyline = self.viewGoogleMap.updateRoute(withPolyline: polyline, newPolyLinePoints: data.routes!.first!.overviewPolyline!.points!)
            
            //Update Time
            
            if self.isTripStarted{
                //Update ETA View Here...
                self.lblETATripDestination.text = AppUtility.getTimeInMinutes(fromTimeInMS: UIntMax(data.routes!.first!.legs!.first!.duration!.value!*1000))
            }
            else{
                //Update Info View Here...
                let destinationTextString = AppUtility.getTimeInMinutes(fromTimeInMS: UIntMax(data.routes!.first!.legs!.first!.duration!.value!*1000)) + " Away"
                self.destinationInfoWindowMarker.bind(withTitle: destinationTextString)
            }
        }
        else{
            //Draw Inital Route
            self.polyline = self.viewGoogleMap.drawRoute(withOverViewPolyline: data.routes!.first!.overviewPolyline!.points!)
            self.addMarkersForSourceAndDestination(withSource: CLLocationCoordinate2D(latitude: Double(data.routes!.first!.legs!.first!.startLocation!.lat!), longitude: Double(data.routes!.first!.legs!.first!.startLocation!.lng!)), withDestination: CLLocationCoordinate2D(latitude: Double(data.routes!.first!.legs!.first!.endLocation!.lat!), longitude: Double(data.routes!.first!.legs!.first!.endLocation!.lng!)), cabReachingTimeText: data.routes!.first!.legs!.first!.duration!.text!)
        }
        
    }
    
    
    func noRouteAvailableSettings(){
        self.addMarkersForSourceAndDestination(withSource: self.driverLocationCoordinate!, withDestination: self.destinationLocationCoordinate!, cabReachingTimeText: "")
    }
    
    
    func prepareScreenToDrawRoute(){
        self.viewGoogleMap.clear()
    }
    
    func addMarkersForSourceAndDestination(withSource source: CLLocationCoordinate2D, withDestination destination: CLLocationCoordinate2D, cabReachingTimeText text: String){
        self.cabMarker = self.viewGoogleMap.addMarkerWithCoordinates(coordinate: source, markerImage: #imageLiteral(resourceName: "ic_car"))
        
        if self.isTripStarted{
            self.destinationAddressInfoWindowMarker = self.viewGoogleMap.addCustomInfoViewMarker(withCoordinates: destination, infoWindowView: self.getAddressInfoNibForTripDestination()) as! RouteAddressInfoWindow
        }
        else{
            self.destinationInfoWindowMarker = self.viewGoogleMap.addCustomInfoViewMarker(withCoordinates: destination, infoWindowView: self.getInfoNibForDestination()) as! InfoWindow
        }
    }
    
    func getInfoNibForDestination()->InfoWindow{
        let nib = Bundle.main.loadNibNamed("InfoWindow", owner: self, options: nil)?.first as! InfoWindow
        return nib
    }
    
    func getAddressInfoNibForTripDestination()->RouteAddressInfoWindow{
        let nib = Bundle.main.loadNibNamed("RouteAddressInfoWindow", owner: self, options: nil)?.first as! RouteAddressInfoWindow
        nib.bind(withLocation: self.pushNotificationModel.trip!.toAddress!)
        return nib
    }
    
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}

extension RiderTripViewController : CancelScreenViewDelgate
{
    func didCancelTrip() {
        SocketManager.shared.tripSocket.disconnect()
        AppInitialViewHandler.sharedInstance.setupInitialViewController()
    }
    func didReceiveErrorOnCancelTrip(_ errorMessage: String) {
        let alertController = self.initialseAlertController(withTitle: AppConstants.ErrorMessages.ALERT_TITLE, andMessage: errorMessage)
        alertController.addAction(JHTAlertAction(title:"OK", style: .default, handler: { (alertAction) in
            SocketManager.shared.tripSocket.disconnect()
            alertController.dismiss(animated: false, completion: {
                
            })
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
            
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
extension RiderTripViewController : RiderTripViewDelegate
{
    func updateMarkerAtPosition(withPoints coordinate: CLLocationCoordinate2D, marker: GMSMarker,bearing:Double)
    {
        let _ = self.viewGoogleMap.updateMarkerAtPosition(withPoints: coordinate, marker: marker, bearing: bearing)
    }
}


// MARK: - SendEmergencyAlertView Delegate Methods
extension RiderTripViewController: SendEmergencyAlertViewDelegate{
    func sentEmergencyAlertSucessfully(){
        print("Emergency sms sent successfully.")
    }
}

extension RiderTripViewController: EmergencyContactDelegate{
    func emergencyContactAdded(withNumber number: String){
        self.navigationController?.popViewController(animated: true)
        self.connectWithEmergency(withNumber: number)
    }
}


// MARK: - GooglePlaceSearchDelegate - Add stop
extension RiderTripViewController: GooglePlaceSearchDelegate{
    
    func placeSelected(withDetail place: SelectedPlaceModel){
        
        self.presenterAddStop.sendAddStopRequest(withAddStopViewRequestModel: AddStopViewRequestModel(lat: place.coordinate.latitude, lng: place.coordinate.longitude, tripId: self.pushNotificationModel.trip!.id!,address: place.title))
        }
    
    func placeSearchScreenDidDismiss() {
        self.addNavigationMenuButton()
    }
}

extension RiderTripViewController: AddStopViewDelegate{
    func addStopSuccessfully(withTripModel data: Trip){
        self.disabledAddStop()
        self.pushNotificationModel.trip = data
        
        if let _ = self.driverLocationCoordinate{
            self.getDirection(self.driverLocationCoordinate!)
        }
    }
    
    func handleScreenOnNextStopNotification(withReponseModel model: PushNotificationObjectModel){
        if let _ = self.driverLocationCoordinate{
            self.pushNotificationModel = model
            self.getDirection(self.driverLocationCoordinate!)
            let _  = self.viewGoogleMap.removeMarker(marker: self.markerAddStop!)
            self.markerAddStop = nil
            AppUtility.showCustomAlert(title: AppConstants.ErrorMessages.ALERT_TITLE, message: AppConstants.ScreenSpecificConstant.RiderTripScreen.VISITED_STOP_ALERT_MESSAGE, presentingVC: self)
        }
        
    }
}


