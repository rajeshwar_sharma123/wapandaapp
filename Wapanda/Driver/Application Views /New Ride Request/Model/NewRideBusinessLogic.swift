//
//  NewRideBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class NewRideBusinessLogic {
    
    init(){
        print("NewRideBusinessLogic init \(self)")
    }
    deinit {
        print("NewRideBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform NewRide With Valid Inputs constructed into a NewRideRequestModel
     
     - parameter inputData: Contains info for New Ride Request
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performNewRideRequest(withNewRideRequestModel NewRideRequestModel: NewRideRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForNewRide()
        NewRideAPIRequest().makeAPIRequest(withReqFormData: NewRideRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForNewRide() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
         errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
        errorResolver.registerErrorCode(ErrorCodes.BIDDING_CLOSED, message: AppConstants.ErrorMessages.BIDDING_CLOSED)
        errorResolver.registerErrorCode(ErrorCodes.BIDDING_EXPIRED, message: AppConstants.ErrorMessages.BIDDING_EXPIRED)
        errorResolver.registerErrorCode(ErrorCodes.DRIVER_OFFLINE, message: AppConstants.ErrorMessages.DRIVER_OFFLINE)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_DRIVER, message: AppConstants.ErrorMessages.INVALID_DRIVER)
        errorResolver.registerErrorCode(ErrorCodes.DRIVER_ON_TRIP, message: AppConstants.ErrorMessages.DRIVER_ON_TRIP)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_BIDDING, message: AppConstants.ErrorMessages.ALREADY_BIDDING)

        return errorResolver
    }
}
