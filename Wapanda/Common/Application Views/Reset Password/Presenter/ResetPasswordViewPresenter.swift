//
//  ResetPasswordViewPresenter.swift
//  Wapanda
//
//  Created by Daffodil on 21/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

//Notes:- This class is used as presenter for NewPasswordPresenter

import Foundation
import ObjectMapper

class ResetPasswordViewPresenter: ResponseCallback{
    
    
    //MARK:- ResetPasswordViewPresenter local properties
    private weak var resetPasswordViewDelegate          : ResetPasswordViewDelegate?
    private var resetPasswordBusinessLogic              : ResetPasswordBusinessLogic!
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?
    
    
    //MARK:- Constructor
    init(delegate responseDelegate:ResetPasswordViewDelegate,textFieldValidationDelegate:TextFieldValidationDelegate){
        
        self.resetPasswordViewDelegate = responseDelegate
        self.textFieldValidationDelegate = textFieldValidationDelegate
        self.resetPasswordBusinessLogic = ResetPasswordBusinessLogic()
    }
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        
        self.resetPasswordViewDelegate?.hideLoader()
        
        if let data = responseObject as? ResetPasswordResponseModel{
            self.resetPasswordViewDelegate?.passwordChangedSuccessfully(withData: data)
        }

    }
    
    
    func servicesManagerError(error : ErrorModel){
        self.resetPasswordViewDelegate?.hideLoader()
        self.resetPasswordViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }

    //MARK:- API calls
    
    func sendChangePasswordRequest(withData resetPasswordRequestModel:ResetPasswordViewModel){
        
        guard validateInput(withData: resetPasswordRequestModel) else {
            return
        }
        
        self.resetPasswordViewDelegate?.showLoader()
        
        let changePasswordRequestModel = ResetPasswordRequestModel.Builder()
            .setCurrentPassword(resetPasswordRequestModel.currentPassword).setNewPassword(resetPasswordRequestModel.newPassword).addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE).addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.resetPasswordBusinessLogic.performPasswordChangeRequest(withResetPasswordRequestModel: changePasswordRequestModel, presenterDelegate: self)
    }
    
    
    //TextField Validation
    
    func validateInput(withData resetPasswordViewModel:ResetPasswordViewModel) -> Bool {
        
        //All fields not empty
        guard self.validateAllFieldsEmpty(withData: resetPasswordViewModel) else{
            //All fields empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: "", forTextFields: .All)
            return false
        }
        
        
        //New Password validation
        guard !resetPasswordViewModel.newPassword.isEmpty else {
            //password empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.NEW_PASSWORD, forTextFields: .NewPassword)
            return false
        }
        guard resetPasswordViewModel.newPassword!.isPasswordValid() else{
            //New password not valid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.INVALID_PASSWORD, forTextFields: .NewPassword)
            return false
        }
        
        //Current Password validation
        guard !resetPasswordViewModel.currentPassword.isEmpty else {
            //password empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.CURRENT_PASSWORD, forTextFields: .CurrentPassword)
            return false
        }
        
        return true
    }
    
    
    /**
     Validate that all fields are empty or not
     - parameter loginRequestModel : LoginScreenRequestModel
     */
    func validateAllFieldsEmpty(withData resetPasswordViewModel:ResetPasswordViewModel) -> Bool{
        guard !resetPasswordViewModel.newPassword.isEmptyString() || !resetPasswordViewModel.currentPassword.isEmptyString() else {
            return false
        }
        return true
    }
}


