//
//  RideScheduleTimeViewPresenter.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 10/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
class ScheduleRideTimeViewPresenter: ResponseCallback{

    //MARK:- ScheduleRideTimeViewPresenter local properties
    
    private weak var rideScheduleTimeViewDelegate: ScheduleRideTimeViewDelegate?
    private lazy var rideScheduleTimeBussinessLogic         : ScheduleRideTimeBussinessLogic = ScheduleRideTimeBussinessLogic()
    //MARK:- Constructor
    
    init(delegate responseDelegate:ScheduleRideTimeViewDelegate){
        self.rideScheduleTimeViewDelegate = responseDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.rideScheduleTimeViewDelegate?.hideLoader()
   self.rideScheduleTimeViewDelegate?.rideScheduleTimeSubmittedSuccessfully(withResponseModel: responseObject as! ScheduleRideTimeResponseModel)
    }
    
    func servicesManagerError(error : ErrorModel){
       self.rideScheduleTimeViewDelegate?.hideLoader()
        self.rideScheduleTimeViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        
    }
    //MARK:- Methods to call server
    
    /**
     This method is used to send schedule ride time request to business layer
     - returns : Void
     */
    func sendScheduleRideTimeRequest(withData scheduleRideTimeModel:ScheduleRideTimeModel) -> Void{
        
        self.rideScheduleTimeViewDelegate?.showLoader()
        
        let scheduleRideTimeRequestModel = ScheduleRideTimeRequestModel.Builder()
           
            .setLatFrom(scheduleRideTimeModel.latFrom)
            .setLngFrom(scheduleRideTimeModel.lngFrom)
            .setLatTo(scheduleRideTimeModel.latTo)
            .setLngTo(scheduleRideTimeModel.lngTo)
            .setPickupDateTime(scheduleRideTimeModel.pickupDateTime)
            .setFromAddress(scheduleRideTimeModel.fromAddress)
            .setToAddress(scheduleRideTimeModel.toAddress)
            .setCarType(scheduleRideTimeModel.carType ?? "")
            .setRiderSelectedFare(scheduleRideTimeModel.riderSelectedFare)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.rideScheduleTimeBussinessLogic.performScheduleRideTime(withScheduleRideTimeRequestModel: scheduleRideTimeRequestModel, presenterDelegate: self)
        
    }
    
    
}
