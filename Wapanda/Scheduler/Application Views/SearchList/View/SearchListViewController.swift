import UIKit

protocol SavedSearchDelegate {
    func savedSearchesClicked(params: BidRequestUserInput)
}

class SearchListViewController: BaseViewController {


    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var searchTableView: UITableView!
    var searchList = [SearchListItemModel]()
    var presenter: SearchListPresenter?
    var delegate: SavedSearchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    func initView() {
        customizeNavigationBackButton()
        customizeNavigationBarWithTitle(navigationTitle: "", color: UIColor.appDarkGrayColor() , isTranslucent: false)
        presenter = SearchListPresenter(delegate: self)
        presenter?.getSavedSearchList(with: 0)
        registerCells()
    }
    
    func getCurrentDateWithFilteredTime(filterDate: Date) -> Date {
        var calendar = NSCalendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        calendar.locale = Locale(identifier:"en_US_POSIX")
        var components = calendar.dateComponents([Calendar.Component.hour, Calendar.Component.minute, Calendar.Component.second], from: filterDate)
        let todaysComponents = calendar.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: Date())
        components.day = todaysComponents.day
        components.month = todaysComponents.month
        components.year = todaysComponents.year
        return calendar.date(from: components)!
    }
}

extension SearchListViewController: SearchListViewDelegate {
    
    func showLoader() {
        showLoader(self)
    }
    
    func hideLoader() {
        hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func refreshList(list: [SearchListItemModel]) {
        searchList = list
        searchTableView.reloadData()
        lblNoDataFound.isHidden = searchList.count != 0
    }
}
