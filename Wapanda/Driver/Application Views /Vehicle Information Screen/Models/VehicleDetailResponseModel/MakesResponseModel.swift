//
//  MakesResponseModel.swift
//
//  Created by Daffodil on 22/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class MakesResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let makes = "Makes"
  }

  // MARK: Properties
  public var makes: [Makes]?


  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    makes <- map[SerializationKeys.makes]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = makes { dictionary[SerializationKeys.makes] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.makes = aDecoder.decodeObject(forKey: SerializationKeys.makes) as? [Makes]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(makes, forKey: SerializationKeys.makes)
  }

}
