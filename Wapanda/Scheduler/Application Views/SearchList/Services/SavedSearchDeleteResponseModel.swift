import UIKit
import ObjectMapper

class SavedSearchDeleteResponseModel: Mappable {
    var status = ""
    var message = ""
    var code = 0
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        
        status <- map["status"]
        message <- map["message"]
        code <- map["code"]
    }
}
