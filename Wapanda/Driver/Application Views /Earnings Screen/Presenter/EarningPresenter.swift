//
//  EarningsPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps
import Kingfisher
class EarningsPresenter: ResponseCallback{
    
    //MARK:- EarningsPresenter local properties
    private weak var earningsViewDelegate             : EarningsScreenViewDelgate?
    private lazy var earningsBusinessLogic         : EarningsBusinessLogic = EarningsBusinessLogic()
    //MARK:- Constructor
    init(delegate responseDelegate:EarningsScreenViewDelgate) {
        self.earningsViewDelegate = responseDelegate
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.earningsViewDelegate?.hideLoader()
        if responseObject is EarningResponseModel
        {
            self.earningsViewDelegate?.didReceiveEarningsList(withResponseModel: responseObject as! EarningResponseModel)
        }
        if responseObject is LoginResponseModel
        {
            self.earningsViewDelegate?.driverStatusUpdatedSuccessful(withResponseModel: responseObject as! LoginResponseModel)
        }
    }
    
    func servicesManagerError(error : ErrorModel){
        self.earningsViewDelegate?.hideLoader()
        _ = error.getErrorPayloadInfo()
        self.earningsViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendDriverOnlineRequest(withDriverHomeViewRequestModel driverHomeViewRequestModel:DriverHomeViewRequestModel){
        
        self.earningsViewDelegate?.showLoader()
        
        let driverHomeRequestModel = DriverHomeRequestModel.Builder()
            .setAvailable(driverHomeViewRequestModel.available)
            .setDriverId(AppDelegate.sharedInstance.userInformation.id!)
            .setLatitude(driverHomeViewRequestModel.lat)
            .setLongitude(driverHomeViewRequestModel.lng)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.earningsBusinessLogic.performDriverAvailabiltyRequest(withDriverHomeRequestModel: driverHomeRequestModel, presenterDelegate: self)
    }
    func sendDriverOfflineRequest(withDriverHomeViewRequestModel driverHomeViewRequestModel:DriverHomeViewRequestModel){
        
        self.earningsViewDelegate?.showLoader()
        
        let driverHomeRequestModel = DriverHomeRequestModel.Builder()
            .setAvailable(driverHomeViewRequestModel.available)
            .setDriverId(AppDelegate.sharedInstance.userInformation.id!)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.earningsBusinessLogic.performDriverAvailabiltyRequest(withDriverHomeRequestModel: driverHomeRequestModel, presenterDelegate: self)
    }
    func sendEarningsRequest(withDriverInfoModel requestModel: EarningsScreenRequestModel) -> Void{
        
        self.earningsViewDelegate?.showLoader()
        
        var earningsRequestModel : EarningsRequestModel!
        
            earningsRequestModel = EarningsRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setFromMonth(requestModel.fromMonth).setToWeekDate(requestModel.toWeekDate).setCurrentDate(requestModel.currentDate).setFromWeekDate(requestModel.fromWeekDate).build()
                
            self.earningsBusinessLogic.performGetDriveRatingRequest(withEarningsRequestModel: earningsRequestModel, presenterDelegate: self)
    }
}
