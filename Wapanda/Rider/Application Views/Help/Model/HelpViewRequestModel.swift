//
//  HelpViewRequestModel.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation


//Notes:- This class is used for constructing Help Service Request Model

class HelpViewRequestModel {
    
    //MARK:- HelpViewRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        
        /**
         This method is used for setting TaxClassification
         
         - parameter taxClassification: String parameter that is going to be set on TaxClassification
         
         - returns: returning Builder Object
         */
        func setSkip(_ skip:Int) -> Builder{
            requestBody["skip"] = skip as AnyObject?
            return self
        }
        
        /**
         This method is used for setting SocialSecurityNumber
         
         - parameter socialSecurityNumber: String parameter that is going to be set on SocialSecurityNumber
         
         - returns: returning Builder Object
         */
        func setLimit(_ limit:Int)->Builder{
            requestBody["limit"] = limit as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of HelpViewRequestModel
         and provide HelpViewViewRequestModel object.
         
         -returns : HelpViewRequestModel
         */
        func build()->HelpViewRequestModel{
            return HelpViewRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting UpdateUser end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            return String(format:AppConstants.ApiEndPoints.HELP,"DRIVER")
        }
        else
        {
             return String(format:AppConstants.ApiEndPoints.HELP,"RIDER")
        }
    }
    
}
