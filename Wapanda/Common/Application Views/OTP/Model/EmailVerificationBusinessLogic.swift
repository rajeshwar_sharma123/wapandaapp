
//Note :- This class contains OTPPopup Buisness Logic

class EmailVerificationBusinessLogic {
    
    /**
     This method is used for perform OTP With Valid Inputs(OTP text) constructed into a EmailVerificationRequestModel
     
     - parameter inputData: Contains info for OTPPopup
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performOTPVerification(withEmailVerificationRequestModel otpPopupRequestModel: EmailVerificationRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForOTPPopup()
        EmailVerificationApiRequest().makeAPIRequest(withReqFormData: otpPopupRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForOTPPopup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT)
        errorResolver.registerErrorCode (ErrorCodes.INVALID_OTP, message  : AppConstants.ErrorMessages.INVALID_OTP)
         errorResolver.registerErrorCode (ErrorCodes.NOT_REGISTERED, message  : AppConstants.ErrorMessages.NOT_REGISTERED)
        return errorResolver
    }
}
