
//Notes:- This class is used as presenter for DriverHomeViewPresenter

import Foundation
import ObjectMapper

class DriverHomeViewPresenter: ResponseCallback{
    
//MARK:- DriverHomeViewPresenter local properties
    
    private weak var driverHomeViewDelegate          : DriverHomeViewDelegate?
    private lazy var driverHomeBusinessLogic         : DriverHomeBusinessLogic = DriverHomeBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:DriverHomeViewDelegate){
        self.driverHomeViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.driverHomeViewDelegate?.hideLoader()
    
        if responseObject is AreaPriceResponseModel{
            self.driverHomeViewDelegate?.updateAreaBasePrice(withResponseModel: responseObject as! AreaPriceResponseModel)
        }
        if let user = responseObject as? LoginResponseModel{
            self.driverHomeViewDelegate?.driverStatusUpdatedSuccessful(withResponseModel: user)
        }
        if responseObject is LaunchResponseModel
        {
            self.driverHomeViewDelegate?.didReceiveLaunchDetails(withLaunchRequestModel : responseObject as! LaunchResponseModel)
        }
        if responseObject is DriverStripeAPIConnectionResponseModel
        {
            self.driverHomeViewDelegate?.driverAPIStripeConnectionSuccessfull(withResponseModel: responseObject as! DriverStripeAPIConnectionResponseModel)
        }
        
    }
    
    func servicesManagerError(error: ErrorModel){
        self.driverHomeViewDelegate?.hideLoader()
        self.driverHomeViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call driverHome Api.
    
    func sendDriverOnlineRequest(withDriverHomeViewRequestModel driverHomeViewRequestModel:DriverHomeViewRequestModel){
        
        self.driverHomeViewDelegate?.showLoader()
        
        let driverHomeRequestModel = DriverHomeRequestModel.Builder()
                                        .setAvailable(driverHomeViewRequestModel.available)
                                        .setDriverId(AppDelegate.sharedInstance.userInformation.id!)
                                        .setLatitude(driverHomeViewRequestModel.lat)
                                        .setLongitude(driverHomeViewRequestModel.lng)
                                        .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                                        .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                                        .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                                        .build()
        
        self.driverHomeBusinessLogic.performDriverAvailabiltyRequest(withDriverHomeRequestModel: driverHomeRequestModel, presenterDelegate: self)
    }
    func sendDriverOfflineRequest(withDriverHomeViewRequestModel driverHomeViewRequestModel:DriverHomeViewRequestModel){
        
        self.driverHomeViewDelegate?.showLoader()
        
        let driverHomeRequestModel = DriverHomeRequestModel.Builder()
            .setAvailable(driverHomeViewRequestModel.available)
            .setDriverId(AppDelegate.sharedInstance.userInformation.id!)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.driverHomeBusinessLogic.performDriverAvailabiltyRequest(withDriverHomeRequestModel: driverHomeRequestModel, presenterDelegate: self)
    }
    func sendDriverUpdatePriceRequest(withDriverHomeViewRequestModel driverHomeViewRequestModel:DriverHomeViewRequestModel){
        
        self.driverHomeViewDelegate?.showLoader()
        
        let driverHomeRequestModel = DriverHomeRequestModel.Builder()
            .setOngoingRate(driverHomeViewRequestModel.ongoingRate)
            .setDriverId(AppDelegate.sharedInstance.userInformation.id!)
            .setRatio(driverHomeViewRequestModel.ratio)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        self.driverHomeBusinessLogic.performDriverAvailabiltyRequest(withDriverHomeRequestModel: driverHomeRequestModel, presenterDelegate: self)
    }
    
    func sendAreabasePriceFetchRequest(withAreaViewRequestModel areaViewRequestModel:AreaBasePriceViewRequestModel,withLoader shouldShowLoader:Bool){
        
        if AppUtility.checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN){
            
            if shouldShowLoader
            {
                self.driverHomeViewDelegate?.showLoader()
            }
            let areaRequestModel = AreaBasePriceRequestModel.Builder()
                .setLatitude(areaViewRequestModel.latitude)
                .setLongitude(areaViewRequestModel.longitude).setDriverId(AppDelegate.sharedInstance.userInformation.id!)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                .build()
            
            self.driverHomeBusinessLogic.performAreaBasePriceRequest(withAreaBasePriceRequestModel: areaRequestModel, presenterDelegate: self)
        }
        
    }
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendDriverConnectRequest(withAuthCode stripeUserId:String,_ shouldShowLoader:Bool) -> Void{
            self.driverHomeViewDelegate?.showLoader()
        
        var bankRequestModel : BankInfoRequestModel!
        bankRequestModel = BankInfoRequestModel.Builder()
            .addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setAuthCode(stripeUserId)
            .build()
        
        self.driverHomeBusinessLogic.performBankInfo(withBankInfoRequestModel: bankRequestModel, presenterDelegate: self)
    }
    func sendLaunchDataRequest() -> Void{
        
        var launchRequestModel : LaunchRequestModel!
        launchRequestModel = LaunchRequestModel.Builder().addRequestHeader(key: "Content-Type", value:"application/json").addRequestHeader(key: "Authorization", value:UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).build()
        self.driverHomeBusinessLogic.performLaunch(withLaunchRequestModel: launchRequestModel, presenterDelegate: self)
    }

}
