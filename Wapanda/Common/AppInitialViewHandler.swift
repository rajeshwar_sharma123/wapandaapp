
//Notes:- This class is used to set initial view controller for the app.

import UIKit
import LGSideMenuController

enum InitialViewControllerMode{
    case withSideMenu
    case withoutSideMenu
}

class AppInitialViewHandler: NSObject {

//MARK:- AppInitialViewHandler local properties.

    static let sharedInstance = AppInitialViewHandler()
    
//MARK:- AppInitialViewHandler init.
    private override init(){
        super.init()
    }
    
//MARK:- Helper methods
    
    /**
     Setup initial view controller for development.
     */
    func setupInitialViewController(){
        
        var mode :InitialViewControllerMode = .withoutSideMenu

        let welcomeScreenVC = UIViewController.getViewController(viewController: WelcomeScreenViewController.self)
        
        let riderHome = UIViewController.getViewController(RiderHomeViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        
        let driverHome = UIViewController.getViewController(DriverHomeViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        
        let sideMenuVC = UIViewController.getViewController(viewController: SideMenuViewController.self)
        
        let driverWelcomeVC = UIViewController.getViewController(DriverWelcomeScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        
        var navigationController: UINavigationController?
        
        mode = .withSideMenu

        
        if checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN){
            
            //Check for location permissions
            AppDelegate.sharedInstance.userInformation = LoginResponseModel.retriveUser()
            
            if checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_DRIVER)
            {
//                mode = .withoutSideMenu

                if (AppDelegate.sharedInstance.userInformation.driverProfile?.approved != nil && AppDelegate.sharedInstance.userInformation.driverProfile!.approved!) ||  (AppDelegate.sharedInstance.userInformation.driverDocs?.canApprove != nil && AppDelegate.sharedInstance.userInformation.driverDocs!.canApprove!)
                {
                  //  driverHome.objUserProfile = AppDelegate.sharedInstance.userInformation
                    navigationController = UINavigationController(rootViewController: driverHome)
                }
                else{
                    driverWelcomeVC.objDriverWelcomeProfile = AppDelegate.sharedInstance.userInformation
                    navigationController = UINavigationController(rootViewController: driverWelcomeVC)
                }
            }
            else{
                navigationController = UINavigationController(rootViewController: riderHome)
            }
        }
        else{
//            mode = .withoutSideMenu
        
            
            navigationController = UINavigationController(rootViewController: welcomeScreenVC)
        }
        
        //Show with side menu or not
        if (mode == .withSideMenu){
            let sideMenuController = LGSideMenuController(rootViewController: navigationController, leftViewController: nil, rightViewController: sideMenuVC)
            sideMenuController.isRightViewSwipeGestureEnabled = false
            sideMenuController.rightViewWidth = ScreenSize.size.width
            sideMenuController.rightViewLayerShadowColor = .clear
            sideMenuController.rightViewPresentationStyle = .slideAbove
            AppDelegate.sharedInstance.kMainViewController = sideMenuController
            AppDelegate.sharedInstance.window?.rootViewController = sideMenuController
            
        }else{
            AppDelegate.sharedInstance.window?.rootViewController = navigationController
        }
        
        
        AppDelegate.sharedInstance.window?.makeKeyAndVisible()
    }
    
    
    
    func setupInitialViewControllerForNonLoggedUser(){
    
        var navigationController: UINavigationController?
       
        
        let riderHome = UIViewController.getViewController(RiderHomeViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        
        let driverHome = UIViewController.getViewController(DriverHomeViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        
        let sideMenuVC = UIViewController.getViewController(viewController: SideMenuViewController.self)
        

        
        
        if checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.NON_LOGGED_IS_DRIVER){
            navigationController = UINavigationController(rootViewController: driverHome)
            
        }else{
            navigationController = UINavigationController(rootViewController: riderHome)
            
        }
        
        let sideMenuController = LGSideMenuController(rootViewController: navigationController, leftViewController: nil, rightViewController: sideMenuVC)
        sideMenuController.isRightViewSwipeGestureEnabled = false
        sideMenuController.rightViewWidth = ScreenSize.size.width
        sideMenuController.rightViewLayerShadowColor = .clear
        sideMenuController.rightViewPresentationStyle = .slideAbove
        AppDelegate.sharedInstance.kMainViewController = sideMenuController
        AppDelegate.sharedInstance.window?.rootViewController = sideMenuController
    
    
    }
    
    
    
    /**
     * This method is used for checking particular in User Default
     * parameter : String Object
     * @returns  : Bool
     */
    
    fileprivate func checkKeyInUserDefault(key:String) -> Bool {
        if(UserDefaultUtility.retrieveBoolForKey(key) == true) {
            
            if (UserDefaultUtility.objectAlreadyExist(key) == true){
                return true
            }
            return false
        }
        return false
    }
}
