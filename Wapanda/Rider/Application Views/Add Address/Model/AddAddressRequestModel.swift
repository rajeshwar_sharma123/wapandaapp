

//Notes:- This class is used for constructing AddAddress Service Request Model

class AddAddressRequestModel {
    
    //MARK:- AddAddressRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var riderId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.riderId = builder.riderId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var riderId: String!
        
        /**
         This method is used for setting address type
         
         - parameter userName: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setAddressType(_ type:String) -> Builder{
            requestBody["addressType"] = type as AnyObject?
            return self
        }
        
        /**
         This method is used for setting address
         
         - parameter userName: String parameter that is going to be set on address
         
         - returns: returning Builder Object
         */
        func setAddress(_ lat:Double, lng: Double, address: String) -> Builder{
            let dictAddress: [String: AnyObject] = ["lat": lat as AnyObject, "lng": lng as AnyObject, "address": address as AnyObject]
                
            requestBody["address"] = dictAddress as AnyObject?
            return self
        }
        
        /**
         This method is used for setting address type
         
         - parameter userName: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setRiderId(_ id:String) -> Builder{
            self.riderId = id
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of AddAddressRequestModel
         and provide AddAddressForm1ViewViewRequestModel object.
         
         -returns : AddAddressRequestModel
         */
        func build()->AddAddressRequestModel{
            return AddAddressRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting AddAddress end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.UPDATE_RIDER, self.riderId)
    }
    
}
