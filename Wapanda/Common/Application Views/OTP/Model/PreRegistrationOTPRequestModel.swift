

//Notes:- This class is used for constructing PreRegistrationOTP Service Request Model

class PreRegistrationOTPRequestModel {
    
    //MARK:- PreRegistrationOTPRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()

        /**
         This method is used for setting phone
         
         - parameter phone: String parameter that is going to be set on phone
         
         - returns: returning Builder Object
         */
        
        func setPhone(_ phone:String) -> Builder{
            requestBody["phone"] = phone as AnyObject?
            return self
        }
        
        /**
         This method is used for setting code
         
         - parameter code: String parameter that is going to be set on code
         
         - returns: returning Builder Object
         */
        
        func setDiallingCode(_ code:String) -> Builder{
            requestBody["countryCodeExt"] = code as AnyObject?
            return self
        }
        
        /**
         This method is used for setting email
         
         - parameter phone: String parameter that is going to be set on email
         
         - returns: returning Builder Object
         */
        
        func setEmail(_ email:String?) -> Builder{
            guard let checkedEmail = email, !checkedEmail.isEmpty else {return self}
            
            requestBody["email"] = email as AnyObject?
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of PreRegistrationOTPRequestModel
         and provide PreRegistrationOTPRequestModel object.
         
         -returns : PreRegistrationOTPRequestModel
         */
        
        func build()->PreRegistrationOTPRequestModel{
            return PreRegistrationOTPRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting OTPPopup end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  AppConstants.ApiEndPoints.PRE_REGISTRATION_OTP
    }
    
}
