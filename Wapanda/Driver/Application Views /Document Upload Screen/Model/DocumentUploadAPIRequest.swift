
//Note :- This Class is used for Update tax info Service means it is used for handling Update tax Api

import UIKit
import ObjectMapper
class DocumentUploadAPIRequest: ApiRequestProtocol {

    //MARK:- local properties
    var apiRequestUrl:String!
    
    //MARK:- Helper methods
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: TaxInformationRequestModel which contains Request header and request body for the Document Upload api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeAPIRequest(withReqFormData reqFromData: DocumentUploadRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        self.apiRequestUrl = AppConstants.URL.BASE_URL+reqFromData.getEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        
        
        ServiceManager.sharedInstance.requestPOSTWithURL(self.apiRequestUrl, andRequestDictionary: reqFromData.requestBody, requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: TaxDoc.self)
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: TaxInformationRequestModel which contains Request header and request body for the Document upload api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeAPIRequest<T:Mappable>(withReqFormData reqFromData: DocumentUploadRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback,returningClass:T.Type) {
        
        self.apiRequestUrl = AppConstants.URL.BASE_URL+reqFromData.getEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        
        
        ServiceManager.sharedInstance.requestPUTWithURL(self.apiRequestUrl, andRequestDictionary: reqFromData.requestBody, requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: T.self)
    }

    
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: TaxInformationRequestModel which contains Request header and request body for the Document upload api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeAPIRequest(withImageData reqFromData: DocumentUploadRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        self.apiRequestUrl = AppConstants.URL.BASE_URL+reqFromData.getEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        
        ServiceManager.sharedInstance.requestMultipartGetRequestWithURL(self.apiRequestUrl, andRequestDictionary: reqFromData.requestBody, requestHeader: reqFromData.requestHeader,imageData:reqFromData.imageData, responseCallBack: responseWrapper, returningClass: DocumentUploadResponseModel.self)
    }
    
    /**
     This method is used to know that whether the api request is in progress or not
     
     - returns: Boolean value either true or false
     */
    func isInProgress() -> Bool {
        return ServiceManager.sharedInstance.isInProgress(self.apiRequestUrl)
    }
    
    /**
     This method is used to cancel the particular API request
     */
    func cancel() -> Void{
        ServiceManager.sharedInstance.cancelTaskWithURL(self.apiRequestUrl)
    }

}
