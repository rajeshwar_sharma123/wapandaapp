//
//  BiddingDoc.swift
//
//  Created by  on 19/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class BiddingDoc: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carType = "carType"
    static let endLoc = "endLoc"
    static let createdAt = "createdAt"
    static let fairPriceTypical = "fairPriceTypical"
    static let closeBy = "closeBy"
    static let startLoc = "startLoc"
    static let driverCountered = "driverCountered"
    static let riderId = "riderId"
    static let fairPriceHigh = "fairPriceHigh"
    static let success = "success"
    static let driverRate = "driverRate"
    static let fairPriceLow = "fairPriceLow"
    static let closed = "closed"
    static let id = "_id"
    static let driverCounterPrice = "driverCounterPrice"
    static let estDistance = "estDistance"
    static let updatedAt = "updatedAt"
    static let driverId = "driverId"
    static let riderCountered = "riderCountered"
    static let agreedPrice = "agreedPrice"
    static let estPrice = "estPrice"
    static let estDuration = "estDuration"
    static let riderCounterPrice = "riderCounterPrice"
    static let toAddress = "toAddress"
    static let fromAddress = "fromAddress"
    static let fareValue = "fareValue"
  }

  // MARK: Properties

  public var carType: String?
  public var endLoc: EndLocation?
  public var createdAt: String?
  public var fairPriceTypical: Float?
  public var closeBy: String?
  public var startLoc: StartLocation?
  public var driverCountered: Bool? = false
  public var riderId: String?
  public var fairPriceHigh: Float?
  public var success: Bool? = false
  public var driverRate: Int?
  public var fairPriceLow: Float?
  public var closed: Bool? = false
  public var id: String?
  public var driverCounterPrice: Float?
  public var estDistance: Int?
  public var updatedAt: String?
  public var driverId: String?
  public var riderCountered: Bool? = false
  public var agreedPrice: Float?
  public var estPrice: Float?
  public var estDuration: Int?
  public var riderCounterPrice: Float?
  public var toAddress: String?
  public var fromAddress: String?
  public var fareValue: FareValue?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    carType <- map[SerializationKeys.carType]
    endLoc <- map[SerializationKeys.endLoc]
    createdAt <- map[SerializationKeys.createdAt]
    fairPriceTypical <- map[SerializationKeys.fairPriceTypical]
    closeBy <- map[SerializationKeys.closeBy]
    startLoc <- map[SerializationKeys.startLoc]
    driverCountered <- map[SerializationKeys.driverCountered]
    riderId <- map[SerializationKeys.riderId]
    fairPriceHigh <- map[SerializationKeys.fairPriceHigh]
    success <- map[SerializationKeys.success]
    driverRate <- map[SerializationKeys.driverRate]
    fairPriceLow <- map[SerializationKeys.fairPriceLow]
    closed <- map[SerializationKeys.closed]
    id <- map[SerializationKeys.id]
    driverCounterPrice <- map[SerializationKeys.driverCounterPrice]
    estDistance <- map[SerializationKeys.estDistance]
    updatedAt <- map[SerializationKeys.updatedAt]
    driverId <- map[SerializationKeys.driverId]
    riderCountered <- map[SerializationKeys.riderCountered]
    agreedPrice <- map[SerializationKeys.agreedPrice]
    estPrice <- map[SerializationKeys.estPrice]
    estDuration <- map[SerializationKeys.estDuration]
    riderCounterPrice <- map[SerializationKeys.riderCounterPrice]
    fromAddress <- map[SerializationKeys.fromAddress]
    toAddress <- map[SerializationKeys.toAddress]
    fareValue <- map[SerializationKeys.fareValue]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = endLoc { dictionary[SerializationKeys.endLoc] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = fairPriceTypical { dictionary[SerializationKeys.fairPriceTypical] = value }
    if let value = closeBy { dictionary[SerializationKeys.closeBy] = value }
    if let value = startLoc { dictionary[SerializationKeys.startLoc] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.driverCountered] = driverCountered
    if let value = riderId { dictionary[SerializationKeys.riderId] = value }
    if let value = fairPriceHigh { dictionary[SerializationKeys.fairPriceHigh] = value }
    dictionary[SerializationKeys.success] = success
    if let value = driverRate { dictionary[SerializationKeys.driverRate] = value }
    if let value = fairPriceLow { dictionary[SerializationKeys.fairPriceLow] = value }
    dictionary[SerializationKeys.closed] = closed
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = driverCounterPrice { dictionary[SerializationKeys.driverCounterPrice] = value }
    if let value = estDistance { dictionary[SerializationKeys.estDistance] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    dictionary[SerializationKeys.riderCountered] = riderCountered
    if let value = agreedPrice { dictionary[SerializationKeys.agreedPrice] = value }
    if let value = estPrice { dictionary[SerializationKeys.estPrice] = value }
    if let value = estDuration { dictionary[SerializationKeys.estDuration] = value }
    if let value = riderCounterPrice { dictionary[SerializationKeys.riderCounterPrice] = value }
    if let value = fromAddress { dictionary[SerializationKeys.fromAddress] = value }
    if let value = toAddress { dictionary[SerializationKeys.toAddress] = value }
    if let value = fareValue { dictionary[SerializationKeys.fareValue] = value.dictionaryRepresentation() }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.endLoc = aDecoder.decodeObject(forKey: SerializationKeys.endLoc) as? EndLocation
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.fairPriceTypical = aDecoder.decodeObject(forKey: SerializationKeys.fairPriceTypical) as? Float
    self.closeBy = aDecoder.decodeObject(forKey: SerializationKeys.closeBy) as? String
    self.startLoc = aDecoder.decodeObject(forKey: SerializationKeys.startLoc) as? StartLocation
    self.driverCountered = aDecoder.decodeBool(forKey: SerializationKeys.driverCountered)
    self.riderId = aDecoder.decodeObject(forKey: SerializationKeys.riderId) as? String
    self.fairPriceHigh = aDecoder.decodeObject(forKey: SerializationKeys.fairPriceHigh) as? Float
    self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
    self.driverRate = aDecoder.decodeObject(forKey: SerializationKeys.driverRate) as? Int
    self.fairPriceLow = aDecoder.decodeObject(forKey: SerializationKeys.fairPriceLow) as? Float
    self.closed = aDecoder.decodeBool(forKey: SerializationKeys.closed)
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.driverCounterPrice = aDecoder.decodeObject(forKey: SerializationKeys.driverCounterPrice) as? Float
    self.estDistance = aDecoder.decodeObject(forKey: SerializationKeys.estDistance) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.driverId = aDecoder.decodeObject(forKey: SerializationKeys.driverId) as? String
    self.riderCountered = aDecoder.decodeBool(forKey: SerializationKeys.riderCountered)
    self.agreedPrice = aDecoder.decodeObject(forKey: SerializationKeys.agreedPrice) as? Float
    self.estPrice = aDecoder.decodeObject(forKey: SerializationKeys.estPrice) as? Float
    self.estDuration = aDecoder.decodeObject(forKey: SerializationKeys.estDuration) as? Int
    self.riderCounterPrice = aDecoder.decodeObject(forKey: SerializationKeys.riderCounterPrice) as? Float
    self.fromAddress = aDecoder.decodeObject(forKey: SerializationKeys.fromAddress) as? String
    self.toAddress = aDecoder.decodeObject(forKey: SerializationKeys.toAddress) as? String
    self.fareValue = aDecoder.decodeObject(forKey: SerializationKeys.fareValue) as? FareValue

  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(endLoc, forKey: SerializationKeys.endLoc)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(fairPriceTypical, forKey: SerializationKeys.fairPriceTypical)
    aCoder.encode(closeBy, forKey: SerializationKeys.closeBy)
    aCoder.encode(startLoc, forKey: SerializationKeys.startLoc)
    aCoder.encode(driverCountered, forKey: SerializationKeys.driverCountered)
    aCoder.encode(riderId, forKey: SerializationKeys.riderId)
    aCoder.encode(fairPriceHigh, forKey: SerializationKeys.fairPriceHigh)
    aCoder.encode(success, forKey: SerializationKeys.success)
    aCoder.encode(driverRate, forKey: SerializationKeys.driverRate)
    aCoder.encode(fairPriceLow, forKey: SerializationKeys.fairPriceLow)
    aCoder.encode(closed, forKey: SerializationKeys.closed)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(driverCounterPrice, forKey: SerializationKeys.driverCounterPrice)
    aCoder.encode(estDistance, forKey: SerializationKeys.estDistance)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(driverId, forKey: SerializationKeys.driverId)
    aCoder.encode(riderCountered, forKey: SerializationKeys.riderCountered)
    aCoder.encode(agreedPrice, forKey: SerializationKeys.agreedPrice)
    aCoder.encode(estPrice, forKey: SerializationKeys.estPrice)
    aCoder.encode(estDuration, forKey: SerializationKeys.estDuration)
    aCoder.encode(riderCounterPrice, forKey: SerializationKeys.riderCounterPrice)
    aCoder.encode(fromAddress, forKey: SerializationKeys.fromAddress)
    aCoder.encode(toAddress, forKey: SerializationKeys.toAddress)
    aCoder.encode(fareValue, forKey: SerializationKeys.fareValue)
  }

}
