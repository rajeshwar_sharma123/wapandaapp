//
//  WaitingScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct WaitingScreenRequestModel {
    
    var success   = true
    var message   = "Ride accepted."
}
