//
//  HistoryViewPresenter+UpcomingTrip.swift
//  Wapanda
//
//  Created by Daffodil on 23/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
extension HistoryViewPresenter{
    /**
     This method is used to send upcoming trip list request to business layer
     - returns : Void
     */
    func sendUpcomingTripList(withData scheduleRideTimeModel: UpcomingTripListModel) -> Void{
        
        self.historyViewDelegate?.showLoader()
        
        var upcomingTripListRequestModel: UpcomingTripListRequestModel!
        if scheduleRideTimeModel.status != ""{
            upcomingTripListRequestModel = UpcomingTripListRequestModel.Builder()
                .setLimit(scheduleRideTimeModel.limit)
                .setSkip(scheduleRideTimeModel.skip)
                .setUserType(scheduleRideTimeModel.userType)
                .setOrder(1)
                .setSortingType(withType: "pickupByTime")
                .setStatus(scheduleRideTimeModel.status)
                .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                .build()
        }else{
            upcomingTripListRequestModel = UpcomingTripListRequestModel.Builder()
                .setLimit(scheduleRideTimeModel.limit)
                .setSkip(scheduleRideTimeModel.skip)
                .setUserType(scheduleRideTimeModel.userType)
                .setOrder(1)
                .setSortingType(withType: "pickupByTime")
                .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                .build()
        }
        self.upcomingTripListBussinessLogic.performUpcomingTripList(withUpcomingTripListRequestModel: upcomingTripListRequestModel, presenterDelegate: self)
        
    }
}
