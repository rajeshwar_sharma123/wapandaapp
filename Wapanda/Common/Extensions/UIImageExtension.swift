//
//  UIImageExtension.swift
//  Wapanda
//
//  Created by daffomac-31 on 06/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

extension UIImageView{
     func getImageViewWithImageTintColor(color: UIColor){
        self.tintColor = color
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
    }
    
    func getImageViewWithOutImageTintColor(color: UIColor){
        self.tintColor = color
        self.image = self.image!.withRenderingMode(.alwaysOriginal)
        
    }
}
