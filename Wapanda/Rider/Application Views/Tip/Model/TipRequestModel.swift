

//Notes:- This class is used for constructing Tip Service Request Model

class TipRequestModel {
    
    //MARK:- TipRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        /**
         This method is used for setting trip id
         
         - parameter tripId: trip id as string parameter
         
         - returns: returning Builder Object
         */
        func setTripId(_ tripId:String) -> Builder{
            self.requestBody["tripId"] = tripId as AnyObject
            return self
        }
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of TipRequestModel
         and provide TipForm1ViewViewRequestModel object.
         
         -returns : TipRequestModel
         */
        func build()->TipRequestModel{
            return TipRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Tip end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.PAYMENT_SUCCES)
    }
    
}
