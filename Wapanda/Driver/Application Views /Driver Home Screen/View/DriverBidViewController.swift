//
//  DriverBidViewController.swift
//  Wapanda
//
//  Created by Daffolapmac on 20/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
extension BaseViewController: DriverBidViewDelegate
{
    func driverDidAcceptedRide(_ notification: PushNotificationObjectModel) {
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
        let driverTripVC = UIViewController.getViewController(DriverTripViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
            driverTripVC.pushNotificationModel = notification
        self.navigationController?.pushViewController(driverTripVC, animated: false)
        }
        else {
            let riderTripVC = UIViewController.getViewController(RiderTripViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            riderTripVC.pushNotificationModel = notification
            self.navigationController?.pushViewController(riderTripVC, animated: true)
        }
    }
    func driverDidCounterRide(_ notification: PushNotificationObjectModel) {
        let waitingVC = UIViewController.getViewController(WaitingViewController.self, storyboard: UIStoryboard.Storyboard.Main.object)
         waitingVC.objNotificationWaiting = notification
         waitingVC.driverBidDelegate = self
         waitingVC.isDriver = true
        self.present(waitingVC, animated: true, completion: nil)
    }
    
    func riderDidCreateBid(_ notification: PushNotificationObjectModel)
    {
        let waitingVC = UIViewController.getViewController(WaitingViewController.self, storyboard: UIStoryboard.Storyboard.Main.object)
        waitingVC.objNotificationWaiting = notification
         waitingVC.isDriver = false
         waitingVC.driverBidDelegate = self
        waitingVC.counterTotalTime = 60
        self.present(waitingVC, animated: true, completion: nil)
    }
    
    func riderDidAcceptBid(_ notification: PushNotificationObjectModel)
    {
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            let driverTripVC = UIViewController.getViewController(DriverTripViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
            driverTripVC.pushNotificationModel = notification
            self.navigationController?.pushViewController(driverTripVC, animated: true)
        }
        else {
            let riderTripVC = UIViewController.getViewController(RiderTripViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
            riderTripVC.pushNotificationModel = notification
            self.navigationController?.pushViewController(riderTripVC, animated: true)
        }
    }
    
    func riderDidRejectBid(_ notification: PushNotificationObjectModel)
    {
       AppUtility.presentToastWithMessage("Rider rejected your request.")
    }

}
