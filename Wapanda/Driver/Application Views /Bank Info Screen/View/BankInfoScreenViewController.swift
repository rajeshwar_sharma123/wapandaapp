//
//  BankInfoScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import OAuthSwift
class BankInfoScreenViewController: BaseViewController {
    
    //MARK : Local Variables
    var addTaxInforSkipButton           : Bool = true
    @IBOutlet weak var labelDescription: UILabel!
    var screenMode: SCREEN_MODE = SCREEN_MODE.NEW
    var presenterBankInfo : BankInfoPresenter!
    var shouldUpdateProfile = true
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var txtFieldRoutingNumber: UITextField!
    @IBOutlet weak var txtfieldName: UITextField!
    @IBOutlet weak var stripeConnectButton: CustomButton!
     var objProfileBankInfo:LoginResponseModel!
    //MARK : Tax Information Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenterBankInfo = BankInfoPresenter(delegate: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if shouldUpdateProfile
        {
            self.objProfileBankInfo = AppDelegate.sharedInstance.userInformation
        }
        
        self.intialSetupForView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.shouldUpdateProfile = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.setupBankInfoView()
        self.setupNavigationBar()
    }
    
    /**
     This method is used to setup initial Bank Info View
     */
    private func setupBankInfoView(){
        if let _ = self.objProfileBankInfo.stripeProfile?.stripeAccountId
        {
            self.stripeConnectButton.setTitle("Update", for: .normal)
            self.labelDescription.text = "Stripe Account connected"
        }
        else
        {
            self.stripeConnectButton.setTitle("Connect", for: .normal)
             self.labelDescription.text = "Connect to stripe to receive payments"
            
        }
        
    }
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.BankInformation.STEP_NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
        if addTaxInforSkipButton
        {
            self.addNavigationSkipButton()
        }
    }
    
    //MARK : IBOutlest Action Methods
    @IBAction func submitButtonTapped(_ sender: CustomButton) {

        let objNewRequestVC = UIViewController.getViewController(StripeWebViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        objNewRequestVC.delegate = self
        objNewRequestVC.urlString = "\(AppConstants.ApiEndPoints.STRIPE_CONNECT_AUTH)?response_type=code&client_id=\(PListUtility.getValue(forKey: AppConstants.PListKeys.STRIPE_STANDARD_CLIENT_ID) as! String)&scope=read_write"
        self.navigationController?.pushViewController(objNewRequestVC, animated: true)
    }
    /**
     This method skips the current screen.
     */
    override func skipButtonClick() ->Void {
        self.navigateToTermsReviewController()
    }
    
    // MARK: - Navigation
    
    func navigateToTermsReviewController(){
        
        let termsReviewVC = UIViewController.getViewController(TermsReviewScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        self.navigationController?.pushViewController(termsReviewVC, animated: true)
    }

}
extension BankInfoScreenViewController:StripeWebViewDelegate
{
    func didCompleteStripeConnection(withResponseModel stripeConnectRespone: StripeConnectionResponseModel) {
        if stripeConnectRespone.error != nil
        {
            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: stripeConnectRespone.errorDescription ?? AppConstants.ErrorMessages.SOME_ERROR_OCCURED)
        }
        else
        {
                self.presenterBankInfo.sendDriverConnectRequest(withAuthCode: stripeConnectRespone.code!, true)
        }
    }
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }

}
extension BankInfoScreenViewController:BankInfoScreenViewDelgate
{
    func driverAPIStripeConnectionSuccessfull(withResponseModel connectResponseModel: DriverStripeAPIConnectionResponseModel) {
        self.objProfileBankInfo.stripeProfile?.stripeAccountId = connectResponseModel.id
        self.objProfileBankInfo.stripeProfile?.chargesEnabled = connectResponseModel.chargesEnabled
        AppDelegate.sharedInstance.userInformation = self.objProfileBankInfo
        AppDelegate.sharedInstance.userInformation.saveUser()
                if screenMode == .NEW{
                    self.navigateToTermsReviewController()
                }
                else{
                    self.navigationController?.popViewController(animated: true)
                }
    }
    func stripeAuthenticationSuccessfull(withResponseModel stripeResponseModel: StripeAuthenticationResponseModel) {
        if screenMode == .NEW{
            self.navigateToTermsReviewController()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }

       
    }

}
