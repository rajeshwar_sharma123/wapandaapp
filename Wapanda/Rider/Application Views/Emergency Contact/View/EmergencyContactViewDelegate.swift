
//Notes:- This protocol is used as a interface which is used by EmergencyContactPresenter to tranfer info to EmergencyContactViewController

protocol EmergencyContactViewDelegate:BaseViewProtocol {
    func emergencyContactUpdatedSuccessfully(withUpdatedUserProfile profile: LoginResponseModel)
}
