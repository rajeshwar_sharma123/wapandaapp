//
//  HelpViewDelegate.swift
//  Wapanda
//
//  Created by Daffodilmac-20 on 09/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation


//Note:- This protocol is used as a interface which is used by HelpViewPresenter to tranfer info to HelpViewController

protocol HelpViewDelegate:BaseViewProtocol {
    func dataFetchedSuccessfully(withResponseModel responseData: HelpViewResponseModel)
}
