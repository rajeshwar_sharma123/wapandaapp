/**
    This class handles the OTP Verification process for phone and email
 */

import UIKit
import Foundation

class EmailOTPVerificationViewController:BaseViewController{
    
    //Screen Outlets
    @IBOutlet weak var lblNumberWithDots: UILabel!
    @IBOutlet weak var txtFieldOTPCode: UITextField!
    @IBOutlet weak var lblErrorOTPCode: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    
    //Global Variables
    fileprivate var emailVerificationPresenter: EmailVerificationPresenter!
    fileprivate var presenterResendEmailOTP : ResendEmailOTPPresenter!
    
    fileprivate var signUpResponseModel : LoginResponseModel?
    fileprivate var timerEnableResendOTP: Timer!
    
    //MARK: Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Setup Initial UI
        self.setupInitialUI()
        
        //Prepare resend phone OTP presenter
        self.presenterResendEmailOTP = ResendEmailOTPPresenter(delegate: self)

        //Email Verification presenter
        self.emailVerificationPresenter = EmailVerificationPresenter(delegate: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //Disable Resend OTP for 60 seconds 
        self.disableResendOTP()
        self.startTimerForEnableOTP()
    }
    
    /**
     This method initalizes class with prefilled values in model from form 2 screen
     */
    func initializeViewWithSignUpRequestModel(model: LoginResponseModel){
        self.signUpResponseModel = model
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Initial View Methods
    
    /**
     This function setup the Initial UI elements
     - returns :void
     */
    private func setupInitialUI() ->Void{
        //Customize navigation bar style
        self.setupNavigationBar()
        
        //Customize the text field properties
        self.customizeTextFields()
        
        //Set Screen Message
        self.setScreenMessage()
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        self.txtFieldOTPCode.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.SingUpScreen.OTP_PLACEHOLDER_TEXT, attributes:attributes)
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    private func setupNavigationBar() ->Void{
        self.customizeNavigationBackButton()
        self.addNavigationSkipButton()
    }
    
    @objc private func enableResendOTP(){
        self.btnResend.isEnabled = true
        self.btnResend.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    private func disableResendOTP(){
        self.btnResend.isEnabled = false
        self.btnResend.titleLabel?.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    private func startTimerForEnableOTP(){
      
        timerEnableResendOTP = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(enableResendOTP), userInfo: nil, repeats: false)
    }
    
    //MARK: Button Actions
    
    @IBAction func btnNextClick(_ sender: Any) {
        self.emailVerificationPresenter.verifyEmail(withOTP: self.txtFieldOTPCode.text ?? "", email: self.signUpResponseModel!.email!, userID: self.signUpResponseModel!.id!)
    }
    
    @IBAction func btnResendClick(_ sender: Any) {
        
        //Disable Resend button for next 60 seconds
        self.disableResendOTP()
        self.startTimerForEnableOTP()
        
        //Resend OTP
        self.resendEmailOTP()
    }
    
    func resendEmailOTP(){
        self.presenterResendEmailOTP.resendEmailOTP(withEmail: self.signUpResponseModel!.email!)
    }
    
    //MARK: Screen Helper Methods
    
    private func setScreenMessage(){
        
        //Get Email
        guard let email = self.signUpResponseModel?.email else {return}
        
        //Make hint text for email id
        let hintEmail = AppConstants.ScreenSpecificConstant.SingUpScreen.HINT_EMAIL_TEXT + "@" + email.components(separatedBy: "@").last!
        
        //Set Hint text
        self.lblNumberWithDots.text = hintEmail
    }
    
    //MARK: Base View Delegate
    
    /**
     This function shows the activity for the screen
     - returns :void
     */
    func showLoader(){
        super.showLoader(self)
    }
    
    /**
     This function hide the activity
     - returns :void
     */
    func hideLoader(){
        super.hideLoader(self)
    }
    
    /**
     This function show error in alert on screen
     - returns :void
     */
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
    func showToastWithMessage(_ message:String) -> Void{
        AppUtility.presentToastWithMessage(message)
    }
    
    //MARK: Navigation
    
    /**
     This function navigate to the Switch Role screen
     - returns :void
     */
    func navigateToSwitchUserScreen(){
        let switchUserScreen = UIViewController.getViewController(viewController: SwitchRoleScreenViewController.self)
        switchUserScreen.initializeScreenWithDataAs(hideBackButton: true)
        self.navigationController?.pushViewController(switchUserScreen, animated: true)
    }
    
    //MARK: Navigation handling
    
    override func skipButtonClick() ->Void {
        self.navigateToSwitchUserScreen()
    }
}

extension EmailOTPVerificationViewController: ResendEmailOTPViewDelegate{
    
    func resendEmailOTPSuccessfully() -> Void{
        navigateToSwitchUserScreen()
    }
}

//MARK: Text Field Delegate

extension EmailOTPVerificationViewController: TextFieldValidationDelegate, UITextFieldDelegate {
    
    //MARK: Validation Delegate
    
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType)
    {
        switch forTextFields {
        case .OTP:
            self.lblErrorOTPCode.text = message
        default:
            break
        }
    }
    
    //MARK: UITextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField{
        case self.txtFieldOTPCode:
            self.lblErrorOTPCode.text = ""
            
            let str = (textField.text! + string)
            if (str.characters.count <= 6){
                return true
            }
            else{
                return false
            }
        default:
            break
        }
        
        return true
    }

}

//MARK: Email Verification Delegate

extension EmailOTPVerificationViewController: EmailVerificationViewDelegate{
    func emailVerifiedSuccessfully() -> Void{
        self.navigateToSwitchUserScreen()
    }
}






