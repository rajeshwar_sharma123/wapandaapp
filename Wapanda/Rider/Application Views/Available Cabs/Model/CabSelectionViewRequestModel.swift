
//Notes:- This model is used as a model that holds signup properties from signup view controller.
import GooglePlaces
import GoogleMaps

struct CabSelectionViewRequestModel {
    
    var latFrom: Double!
    var lngFrom: Double!
    var latTo: Double!
    var lngTo: Double!
    var sourceAddress: String!
    var destinationAddress: String!
    
    //In case of sorted list
    var cabList: [String]? //Cab ids
    var sortingOrder: String? //CAB_SORTING_ORDER raw value
}
