//
//  NewRideRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class NewRideRequestModel {
    
    //MARK:- NewRideRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var biddingId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.biddingId = builder.biddingId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var biddingId: String!
        /**
         This method is used for setting User id
         
         - parameter biddingId: String parameter that is going to be set on biddingId
         
         - returns: returning Builder Object
         */
        func setBiddingId(_ biddingId:String)->Builder{
            self.biddingId = biddingId
            return self
        }
        
        /**
         This method is used for setting User id
         
         - parameter biddingId: String parameter that is going to be set on biddingId
         
         - returns: returning Builder Object
         */
        func setLatitude(_ latitude:Double)->Builder{
            requestBody["lat"] = latitude as AnyObject
            return self
        }
        /**
         This method is used for setting User id
         
         - parameter biddingId: String parameter that is going to be set on biddingId
         
         - returns: returning Builder Object
         */
        func setLongitude(_ longitude:Double)->Builder{
            requestBody["lng"] = longitude as AnyObject
            return self
        }
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->NewRideRequestModel{
            return NewRideRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting login end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.DRIVER_ACCEPT_RIDE, self.biddingId)
    }
    
}
