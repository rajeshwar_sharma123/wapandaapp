//
//  SearchAddressTableViewDataSourceAndDelegate.swift
//  Wapanda
//
//  Created by daffomac-31 on 19/09/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces

protocol SearchAddressTableViewDelegate: BaseViewProtocol {
    func addressSelected(withAddress address: SelectedPlaceModel)
}

class SearchAddressTableViewDataSourceAndDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var dataSource: [GMSAutocompletePrediction] = []
    var dataSourceUserAddresses: [Address] = []
    var placeDetailPresenter: GooglePlaceDetailViewPresenter!
    var selectedAddressTitle: String!
    var searchString = ""
    weak var delegate: SearchAddressTableViewDelegate?
    
    override init() {
        super.init()
        placeDetailPresenter = GooglePlaceDetailViewPresenter(delegate: self)
    }
    
    func bind(dataSourceSearchResults: [GMSAutocompletePrediction], userAddress: [Address], searchText: String){
        self.dataSource = dataSourceSearchResults
        self.dataSourceUserAddresses = userAddress
        self.searchString = searchText
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.isHidden = false
        if dataSource.count == 0 && !searchString.isEmpty{
            return 1
        }
        else if dataSource.count == 0{
            return self.dataSourceUserAddresses.count
        }
        else{
            return self.dataSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if dataSource.count == 0 && !searchString.isEmpty{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddressSearchResultTableViewCell.self)) as! AddressSearchResultTableViewCell
            cell.bind(withTitle: NSAttributedString(string: AppConstants.ScreenSpecificConstant.RideListScreen.LOCATION_NOT_FOUND) , WithStateAndCountry: NSAttributedString(string: AppConstants.ScreenSpecificConstant.RideListScreen.LOCATION_NOT_FOUND_MSG))
            cell.selectionStyle = .none
            return cell
        }
        else if dataSource.count == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageViewWithTitleTableViewCell.self)) as! ImageViewWithTitleTableViewCell
            
            let selectedAddress = self.dataSourceUserAddresses[indexPath.row]
            
            if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_RECENT{
                cell.bind(withTitle: selectedAddress.address!)
            }
            else if selectedAddress.address_type_title == AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_SCREEN_HOME{
                cell.bind(withTitle: selectedAddress.address_type_title!,andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_home"))
            }
            else{
                cell.bind(withTitle: selectedAddress.address_type_title!, andDescription: selectedAddress.address!, img: #imageLiteral(resourceName: "ic_work"))
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddressSearchResultTableViewCell.self)) as! AddressSearchResultTableViewCell
            let selectedAddress = self.dataSource[indexPath.row]
            cell.bind(withTitle: selectedAddress.attributedPrimaryText , WithStateAndCountry: selectedAddress.attributedSecondaryText ?? NSAttributedString(string: ""))
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataSource.count == 0 && !searchString.isEmpty{
            //Do nothing while tap on no location cell
        }
        else if dataSource.count == 0{
            self.selectedAddressTitle = self.dataSourceUserAddresses[indexPath.row].address
            self.handleUserAddressSelection(withAddress: self.dataSourceUserAddresses[indexPath.row])
            tableView.isHidden = true
        }
        else{
            
            //Get Place Detail
            let selectedPlace = self.dataSource[indexPath.row]
            self.selectedAddressTitle = selectedPlace.attributedFullText.string
            self.placeDetailPresenter.sendGooglePlaceDetailRequest(withGooglePlaceDetailViewRequestModel: GooglePlaceDetailViewRequestModel(placeId: selectedPlace.placeID ?? ""))
            tableView.isHidden = true
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func handleUserAddressSelection(withAddress address: Address){
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedAddressTitle, coordinate: CLLocationCoordinate2D(latitude: address.lat!, longitude: address.lng!))
        
        self.onAddressSelection(withAddress: model)
    }
    
    func onAddressSelection(withAddress address: SelectedPlaceModel){
        print("Selected Address: \(address)")
        self.delegate?.addressSelected(withAddress: address)
    }
}

//MARK: Google Place Detail Delegate ...
extension SearchAddressTableViewDataSourceAndDelegate: BaseViewProtocol, GooglePlaceDetailViewDelegate{
    
    func placeDetailWithData(data: GMSPlace){
        
        //Perform operation on destination selection
        let model = SelectedPlaceModel(title: self.selectedAddressTitle, coordinate: data.coordinate)
        self.onAddressSelection(withAddress: model)
    }
    
    func showLoader(){
        self.delegate?.showLoader()
    }
    
    func hideLoader(){
        self.delegate?.hideLoader()
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.delegate?.showErrorAlert(alertTitle, alertMessage: alertMessage)
    }
}
