//
//  DriverTripRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class DriverTripRequestModel {
    
    //MARK:- DriverTripRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var tripId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.tripId = builder.tripId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var tripId: String!
        /**
         This method is used for setting User id
         
         - parameter tripId: String parameter that is going to be set on tripId
         
         - returns: returning Builder Object
         */
        func setTripId(_ tripId:String)->Builder{
            self.tripId = tripId
            return self
        }
        /**
         This method is used for setting latFrom
         
         - parameter id: Double parameter that is going to be set on latFrom
         
         - returns: returning Builder Object
         */
        func setLatFrom(_ latFrom:Double) -> Builder{
            requestBody["latFrom"] = latFrom as AnyObject?
            return self
        }  /**
         This method is used for setting lngFrom
         
         - parameter id: Double parameter that is going to be set on lngFrom
         
         - returns: returning Builder Object
         */
        func setLngFroms(_ lngFrom:Double) -> Builder{
            requestBody["lngFrom"] = lngFrom as AnyObject?
            return self
        }
        /**
         This method is used for setting latFrom
         
         - parameter id: Double parameter that is going to be set on latFrom
         
         - returns: returning Builder Object
         */
        func setLatTo(_ latTo:Double) -> Builder{
            requestBody["latTo"] = latTo as AnyObject?
            return self
        }  /**
         This method is used for setting lngFrom
         
         - parameter id: Double parameter that is going to be set on lngFrom
         
         - returns: returning Builder Object
         */
        func setLngTo(_ lngTo:Double) -> Builder{
            requestBody["lngTo"] = lngTo as AnyObject?
            return self
        }
        /**
         This method is used for setting startTime
         
         - parameter distance: String parameter that is going to be set on startTime
         
         - returns: returning Builder Object
         */
        func setStartTime(_ startTime:String) -> Builder{
            requestBody["startTime"] = startTime as AnyObject?
            return self
        }
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->DriverTripRequestModel{
            return DriverTripRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Start Trip end point
     
     -returns: String containg end point
     */
    func getStartTripEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.START_TRIP, self.tripId)
    }
    /**
     This method is used for getting End Trip end point
     
     -returns: String containg end point
     */
    func getEndTripEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.END_TRIP, self.tripId)
    }
    
}
