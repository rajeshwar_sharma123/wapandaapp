
//Notes:- This class is used as presenter for ProcessPaymentViewPresenter

import Foundation
import ObjectMapper

class ProcessPaymentViewPresenter: ResponseCallback{
    
//MARK:- ProcessPaymentViewPresenter local properties
    
    private weak var processPaymentViewDelegate          : ProcessPaymentViewDelegate?
    private lazy var processPaymentBusinessLogic         : ProcessPaymentBusinessLogic = ProcessPaymentBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:ProcessPaymentViewDelegate){
        self.processPaymentViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.processPaymentViewDelegate?.hideLoader()
        if let model = responseObject as? ProcessPaymentResponseModel{
            self.processPaymentViewDelegate?.processPaymentSuccess(withResponseModel: model)
        }
        if let model = responseObject as? AddTipResponseModel{
            self.processPaymentViewDelegate?.addTipSuccess(withResponseModel: model)
        }
        
        
    }
    
    func servicesManagerError(error: ErrorModel){
        self.processPaymentViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        self.processPaymentViewDelegate?.hideLoader()
    }
    
//MARK:- Methods to make decision and call ProcessPayment Api.
    
    func sendProcessPaymentRequest(withProcessPaymentViewRequestModel processPaymentViewRequestModel:ProcessPaymentViewRequestModel){
        
        self.processPaymentViewDelegate?.showLoader()
        
        let requestModel = ProcessPaymentRequestModel.Builder()
                            .setPaymentId(processPaymentViewRequestModel.paymentId).setSourceId(processPaymentViewRequestModel.sourceId)
                            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
                            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
                            .build()
        
        self.processPaymentBusinessLogic.performProcessPayment(withProcessPaymentRequestModel: requestModel, presenterDelegate: self)
    }
    
    func sendAddTipRequest(withTipPercent tipPercent:Double,andPaymentId paymentId:String){
        
        self.processPaymentViewDelegate?.showLoader()
        
        let requestModel = ProcessPaymentRequestModel.Builder()
            .setPaymentId(paymentId)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN)).setTipPercent(tipPercent)
            .build()
        
        self.processPaymentBusinessLogic.performAddTip(withProcessPaymentRequestModel: requestModel, presenterDelegate: self)
    }
}
