

//Notes:- This class is used for constructing DriverHome Service Request Model

class DriverHomeRequestModel {
    
    //MARK:- DriverHomeRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var driverId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.driverId = builder.driverId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var driverId: String!
        
        /**
         This method is used for setting base price
         
         - parameter basePrice: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setAvailable(_ available:Bool) -> Builder{
            requestBody["available"] = available as AnyObject?
            return self
        }
        
        /**
         This method is used for setting base price
         
         - parameter basePrice: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setLongitude(_ lng:Float) -> Builder{
            requestBody["lng"] = lng as AnyObject?
            return self
        }
        
        /**
         This method is used for setting base price
         
         - parameter basePrice: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setLatitude(_ lat:Float) -> Builder{
            requestBody["lat"] = lat as AnyObject?
            return self
        }
        
        /**
         This method is used for setting base price
         
         - parameter basePrice: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setOngoingRate(_ ongoingRate:Float) -> Builder{
            requestBody["ongoingRate"] = ongoingRate as AnyObject?
            return self
        }
        /**
         This method is used for setting base price
         
         - parameter basePrice: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setRatio(_ ratio:Float) -> Builder{
            requestBody["ratio"] = ratio as AnyObject?
            return self
        }

        /**
         This method is used for setting address type
         
         - parameter userName: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setDriverId(_ id:String) -> Builder{
            self.driverId = id
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of DriverHomeRequestModel
         and provide DriverHomeForm1ViewViewRequestModel object.
         
         -returns : DriverHomeRequestModel
         */
        func build()->DriverHomeRequestModel{
            return DriverHomeRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting DriverHome end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.UPDATE_DRIVER, self.driverId)
    }
    
}
