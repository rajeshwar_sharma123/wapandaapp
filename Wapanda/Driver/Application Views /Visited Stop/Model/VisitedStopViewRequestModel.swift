
//Notes:- This model is used as a model that holds signup properties from visited stop view controller.

struct VisitedStopViewRequestModel {
    
    var tripId                   : String!
    var stopId                   : String!
}
