import UIKit

protocol SchedulerBidViewControllerDelegate: class {
    
    func hideLoader()
    func showLoader()
    func showErrorAlert(_ alertTitle: String, alertMessage: String)
    func renderData(data: RideList)
    func refreshList(list: [VehicleListResponseModel])
    func noCabsAvailable()
}
