//
//  SwitchRoleScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/20/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class SwitchRoleScreenViewController: BaseViewController {
    
    fileprivate var hideBackButton: Bool! = false
    internal var isDriverSelected = false
    var updatedProfilePresenter : UpdatedProfileViewPresenter!
    
    
    // MARK: - SwitchRoleScreenViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helper methods
    
    
    func initializeScreenWithDataAs(hideBackButton: Bool){
        self.hideBackButton = hideBackButton
    }
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.updatedProfilePresenter = UpdatedProfileViewPresenter(delegate: self)
        self.setupNavigationBar()
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: "")
        
        if !hideBackButton{
            self.customizeNavigationBackButton()
        }
        else{
            self.hideNavigationBar()
        }
    }
    
    // MARK: - IBOutlets Action Methods
    @IBAction func driverOptionTapped(_ sender: UIButton) {
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN)
        {
        UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER, value: true)
        self.isDriverSelected = true
        self.checkForUpdtedUserProfile()
        }
        else
        {
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.NON_LOGGED_IS_DRIVER, value: true)
            self.navigateToDriverHomeScreen()
        }
    }
    
    @IBAction func riderOptionTapped(_ sender: UIButton) {
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN)
        {
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER, value: false)
            self.isDriverSelected = false
            self.checkForUpdtedUserProfile()
        }
        else
        {
            UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.NON_LOGGED_IS_DRIVER, value: false)
            self.navigateToRiderHomeScreen()
        }
       
    }
    
    func checkForUpdtedUserProfile(){
//        if AppUtility.isUserLogin(){
//            //self.updatedProfilePresenter.getUpdatedProfileList()
//        }
//        else{
            self.performNavigationFromScreen()
        //}
    }
    
    // MARK: - Navigation
    
    /// Source of navigation from screen to driver or rider screen with all possible options
    func performNavigationFromScreen(){
        
        if AppUtility.isUserLogin(){
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }
        else{
            if self.isDriverSelected{
                self.navigateToDriverHomeScreen()
            }
            else{
                self.navigateToRiderHomeScreen()
            }
        }
    }
    
    func navigateToDriverWelcomeViewController(){
        
        let driverWelcomeVC = UIViewController.getViewController(DriverWelcomeScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        driverWelcomeVC.objDriverWelcomeProfile = LoginResponseModel.retriveUser()
        self.navigationController?.pushViewController(driverWelcomeVC, animated: true)
    }
    
    func navigateToRiderHomeScreen(){
        
        let rolesVC = UIViewController.getViewController(RiderHomeViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
        self.navigationController?.pushViewController(rolesVC, animated: true)
    }
    
    func navigateToDriverHomeScreen(){
        let driverHomeVC = UIViewController.getViewController(DriverHomeViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
         if AppUtility.isUserLogin(){
      //  driverHomeVC.objUserProfile = AppDelegate.sharedInstance.userInformation
        }
        self.navigationController?.pushViewController(driverHomeVC, animated: true)

    }
    
}
extension SwitchRoleScreenViewController : UpdatedProfileViewDelegate
{
    func updatedProfileFetchedSuccessfully(WithResponseModel data: LoginResponseModel) {
        AppDelegate.sharedInstance.userInformation = data
        AppDelegate.sharedInstance.userInformation.saveUser()
        self.performNavigationFromScreen()
    }
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}
