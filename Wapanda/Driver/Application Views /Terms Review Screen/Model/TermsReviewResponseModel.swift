//
//  TermsReviewResponseModel.swift
//  Wapanda
//
//  Created by daffomac-31 on 02/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class TermsReviewResponseModel:Mappable {
    
    var skip       : String = ""
    var limit       : String = ""
    var total_count       : String = ""
    var items       : [Item] = []
    var hasNext       : String = ""
    var hasPrev       : String = ""
    var item_count       : String = ""
    
    required internal init?(map: Map) {
        mapping(map: map)
    }
    
    init() {
        
    }
    
    internal func mapping(map: Map) {
        skip         <- map["skip"]
        limit         <- map["limit"]
        total_count         <- map["total_count"]
        items         <- map["items"]
        hasNext         <- map["hasNext"]
        hasPrev         <- map["hasPrev"]
        item_count         <- map["item_count"]
    }
}
