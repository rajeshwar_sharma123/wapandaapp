//
//  UpdateUserProfileViewRequestModel.swift
//
//  Created by daffomac-31 on 10/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class UpdateUserProfileViewRequestModel: NSObject, Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastName = "lastName"
    static let profileImageFileId = "profileImageFileId"
    static let firstName = "firstName"
    static let addresses = "addresses"
    static let email = "email"

  }

  // MARK: Properties
  public var lastName: String?
  public var email: String?

  public var profileImageFileId: String?
  public var firstName: String?
  public var addresses: [UserAddresses]?

  public override init() {
  }
    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lastName <- map[SerializationKeys.lastName]
    profileImageFileId <- map[SerializationKeys.profileImageFileId]
    firstName <- map[SerializationKeys.firstName]
    email <- map[SerializationKeys.email]

    addresses <- map[SerializationKeys.addresses]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = profileImageFileId { dictionary[SerializationKeys.profileImageFileId] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }

    if let value = addresses { dictionary[SerializationKeys.addresses] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.profileImageFileId = aDecoder.decodeObject(forKey: SerializationKeys.profileImageFileId) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String

    self.addresses = aDecoder.decodeObject(forKey: SerializationKeys.addresses) as? [UserAddresses]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(profileImageFileId, forKey: SerializationKeys.profileImageFileId)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(email, forKey: SerializationKeys.email)

    aCoder.encode(addresses, forKey: SerializationKeys.addresses)
  }

}
