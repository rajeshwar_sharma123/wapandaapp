
//Note :- This class contains AddStop Buisness Logic

class AddStopBusinessLogic {
    
    
    deinit {
        print("AddStopBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs(Email , password) construceted into a AddStopRequestModel
     
     - parameter inputData: Contains info for AddStop
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performAddStop(withAddStopRequestModel addStopRequestModel: AddStopRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        AddStopApiRequest().makeAPIRequest(withReqFormData: addStopRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_ADDED_STOP, message  : AppConstants.ErrorMessages.ALREADY_ADDED_STOP)
        errorResolver.registerErrorCode(ErrorCodes.TRIP_NOT_STARTED_YET, message  : AppConstants.ErrorMessages.TRIP_NOT_STARTED_YET)
        errorResolver.registerErrorCode(ErrorCodes.ACCESS_DENIED, message  : AppConstants.ErrorMessages.ACCESS_DENIED)
        
        


        return errorResolver
    }
}
