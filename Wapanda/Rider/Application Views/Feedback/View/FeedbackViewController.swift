//
//  FeedbackViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 11/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import HCSStarRatingView

protocol FeedbackScreenDelegate: class {
    func feedbackScreenDidDismiss()
}

class FeedbackViewController: BaseViewController {
    
    @IBOutlet var _iWriteFeedbackHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var _iFeedbackViewHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var imgViewDriverImage: UIImageView!
    @IBOutlet var ratingView: HCSStarRatingView!
    @IBOutlet var txtViewReview: UITextView!
    @IBOutlet var lblWriteReview: UILabel!
    @IBOutlet var viewDriverImageBackground: UIView!
    var viewRequestModel: FeedbackViewRequestModel!
    var presenterSubmitFeedback: FeedbackViewPresenter!
    weak var delegate: FeedbackScreenDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.viewCustomization()
        self.presenterSubmitFeedback = FeedbackViewPresenter(delegate: self)
    }
    
    
    /// This method is used to customize the view on load
    func viewCustomization(){
        //Image & Image Bg View Customization
        self.imgViewDriverImage.clipsToBounds = true
        self.viewDriverImageBackground.clipsToBounds = true
        self.imgViewDriverImage.layer.cornerRadius = self.imgViewDriverImage.frame.width/2
        self.viewDriverImageBackground.layer.cornerRadius = self.viewDriverImageBackground.frame.width/2
        
        //Set Driver Name
        self.lblDriverName.text = viewRequestModel.driverName
        
        //Set Driver Image
        let imageUrl = URL(string: AppUtility.getImageURL(fromImageId: (self.viewRequestModel.driverImageId ?? "")))
        self.imgViewDriverImage.setImageWith(imageUrl!, placeholderImage: #imageLiteral(resourceName: "ic_account_placeholder"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// This function is used to initalize required data for screen
    ///
    /// - Parameter tripId: tripId as string parameter
    func bind(withRequestModel viewModel: FeedbackViewRequestModel){
        self.viewRequestModel = viewModel
    }
    
    /// This method is handler for finish click
    ///
    /// - Parameter sender: sender reference object
    @IBAction func finishClick(_ sender: Any) {
        self.prepareFeedbackViewRequest()
        self.presenterSubmitFeedback.sendFeedbackRequest(withFeedbackViewRequestModel: self.viewRequestModel)
    }
    
    func prepareFeedbackViewRequest(){
        self.viewRequestModel.rate = Int(self.ratingView.value)
        if !self.txtViewReview.text.isEmpty{
            self.viewRequestModel.reivew = self.txtViewReview.text
        }
    }
    
    
    /// This method is used to handle click on write review button
    ///
    /// - Parameter sender: sender reference object
    @IBAction func writeReviewClick(_ sender: Any) {
        self.prepareViewForWriteReview()
    }
    
    /// This method is used to handle click on driver info to show his rating
    ///
    /// - Parameter sender: sender reference object
    @IBAction func driverInfoTapped(_ sender: Any) {
        self.navigateToDriverRatingList()
    }
    
    //MARK:Navigation
    private func navigateToDriverRatingList()
    {
        let driverRatingVC =  UIViewController.getViewController(DriverRatingViewController.self, storyboard: UIStoryboard.Storyboard.Driver.object)
        driverRatingVC.driverInfoModel = self.getDriverInfoModel()
        self.present(driverRatingVC, animated: true, completion: {})
    }
    private func getDriverInfoModel() -> DriverInfoResponseModel
    {
        let driverInfoModel = DriverInfoResponseModel(JSON: [:])
        driverInfoModel?.firstName = "\(self.viewRequestModel.pushObjModel.trip?.driver?.firstName?.capitalizeWordsInSentence() ?? "")"
        driverInfoModel?.id = "\(self.viewRequestModel.pushObjModel.trip?.driver?.id ?? "")"
        driverInfoModel?.profileImageFileId = "\(self.viewRequestModel.pushObjModel.trip?.driver?.profileImageFileId ?? "")"
        driverInfoModel?.driverProfile = self.viewRequestModel.pushObjModel.trip?.driver?.driverProfile
        driverInfoModel?.cabInfo = Cab(JSON: (self.viewRequestModel.pushObjModel.trip?.vehicle?.dictionaryRepresentation())!)
        return driverInfoModel!
    }

    
    
    /// This method is used to prepare the view for write a review
    private func prepareViewForWriteReview(){
        self._iFeedbackViewHeightLayoutConstraint.isActive = false
        self._iWriteFeedbackHeightLayoutConstraint.constant = 100.0
        self.txtViewReview.becomeFirstResponder()
        self.view.bringSubview(toFront: self.txtViewReview)
        self.lblWriteReview.isHidden = true
        self.txtViewReview.isUserInteractionEnabled = true
        self.view.layoutIfNeeded()
    }

}


// MARK: - UITextViewDelegate Methods
extension FeedbackViewController: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            self.keyboardReturnKeyClick()
        }
        
        return textView.text.characters.count + (text.characters.count - range.length) <= 500
    }
    
    func keyboardReturnKeyClick(){
        self.txtViewReview.resignFirstResponder()
    }
}

extension FeedbackViewController: FeedbackViewDelegate{
    func feedbackSubmittedSucessfully() {
        self.dismiss(animated: true) { 
            self.delegate?.feedbackScreenDidDismiss()
        }
    }
    
    func showLoader() {
        super.showLoader(self)
    }
    
    func hideLoader() {
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle: String, alertMessage: String) {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}


