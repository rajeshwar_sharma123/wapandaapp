import UIKit
import ObjectMapper
import MapKit

class BidsListPresenter: ResponseCallback {

    private weak var view: BidsListViewDelegate!
    var bidsList: BidsListResponseModel?
    private var bidsListManager = BidsListManager()
    var callInProgress = false
    
    var skip = 0
    var LIMIT = 20
    
    init(delegate: BidsListViewDelegate){
        view = delegate
    }
    
    /**
     This method is used to get bids request available to driver
     - parameter params: user filter parameters
     */
    func getBidsList(with params: BidRequestUserInput, skip: Int, showLoader: Bool = false) {
        
        self.skip = skip
        if showLoader {
            view.showLoader()
        }
        
        callInProgress = true
        let bidsRequestModel: BidsListRequestModel  = BidsListRequestModel
            .Builder()
            .addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setLatitude(params.latitude)
            .setLongitude(params.longitude)
            .setSortOrder(params.sortOrder)
            .setSortKey(params.sortKey)
            .setSkip(skip)
            .setLimit(LIMIT)
            .setFare(params.fare)
            .setPickupTime(params.pickupTime)
            .build()
        
        bidsListManager.getBids(with : bidsRequestModel, presenterDelegate: self)
    }
    
    
    func updateDriverAvailabilitySataus(isAvailable: Bool, driverLocation: CLLocationCoordinate2D) {
        
        view?.showLoader()
        
        let driverHomeRequestModel = DriverHomeRequestModel.Builder()
            .setAvailable(isAvailable)
            .setDriverId(AppDelegate.sharedInstance.userInformation.id!)
            .setLatitude(Float(driverLocation.latitude))
            .setLongitude(Float(driverLocation.longitude))
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        
        DriverHomeBusinessLogic().performDriverAvailabiltyRequest(withDriverHomeRequestModel: driverHomeRequestModel, presenterDelegate: self)
    }
    
    /**
     This method is used to make Car Make List request to business layer with valid Request model
     - returns : Void
     */
    func saveSearch(with params: SaveSearchUserInput) {
        
        view.showLoader()
        
        let request: SaveSearchReqModel  = SaveSearchReqModel
            .Builder()
            .addRequestHeader(key: "Content-Type", value:"application/json")
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .setDestLoc(params.destLoc)
            .setTitle(params.title)
            .setFare(params.fare)
            .setPickupDateTime(params.pickupDateTime)
            .setDestAddress(params.destinationAddress)
            .build()
        
        bidsListManager.saveSearchPreference(with : request, presenterDelegate: self)
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject: T){
        
        callInProgress = false
        if let response = responseObject as? BidsListResponseModel{
            if skip == 0{
                bidsList = response
            }
            else{
                bidsList?.hasNext = response.hasNext
                bidsList?.items.append(contentsOf: response.items)
            }
            self.view?.refreshList(list: bidsList!.items)
        }
        else if let _ = responseObject as? SaveSearchResponseModel{
            self.view?.searchSaved()
        }
        else if let response = responseObject as? LoginResponseModel{
            self.view?.driverStatusUpdatedSuccessful(reponse: response)
        }
        view.hideLoader()
    }
    
    
    func servicesManagerError(error: ErrorModel){
        view?.hideLoader()
        callInProgress = false
        self.view?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
}
