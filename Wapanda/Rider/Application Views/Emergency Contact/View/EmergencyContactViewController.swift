//
//  EmergencyContactViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 13/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//  

import UIKit
import Contacts
import ContactsUI
import JHTAlertController

protocol EmergencyContactDelegate: class {
    func emergencyContactAdded(withNumber number: String)
}

class EmergencyContactViewController: BaseViewController {
    
    @IBOutlet weak var imgViewDial: UIImageView!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    var contactHandler = ContactsHandler.sharedInstance
    var emergencyContactPresenter: EmergencyContactViewPresenter!
    var uploadImagePresenter: DocumentUploadPresenter!
    var viewRequestModel: EmergencyContactViewRequestModel!
    weak var delegate: EmergencyContactDelegate?
    var selectedContact: CNContact!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.customizeView()
        self.emergencyContactPresenter = EmergencyContactViewPresenter(delegate: self)
        self.uploadImagePresenter = DocumentUploadPresenter(delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool){
    }
    
    func bind(withViewData data: EmergencyContactViewRequestModel){
        self.viewRequestModel = data
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func customizeView(){
        self.imgViewDial.setCornerCircular(75.0)
        self.btnSubmit.setCornerCircular(10.0)
        self.customizeNavigationBar()
        self.showScreenData()
    }
    
    private func showScreenData(){
        if let _ = viewRequestModel{
            self.prepareScreenWithUserData()
        }
        else{
            self.prepareScreenForNoContact()
        }
    }
    
    private func customizeNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: "Emergency", color: UIColor.appThemeColor(), isTranslucent: false)
        self.customizeNavigationBackButton()
    }
    
    override func backButtonClick() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func prepareScreenWithUserData(){
        
        //Set Contact Name
        self.lblFirst.text = self.viewRequestModel.emergencyContactName
        
        //Set Contact Number
        self.lblSecond.text = self.viewRequestModel.emergencyContact
        
        //Set Image
        let imageId = self.viewRequestModel.emergencyContactImageId ?? ""
        let ImageURL = URL(string: AppUtility.getImageURL(fromImageId: imageId))
        self.imgViewDial.setImageWith(ImageURL!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_emergency_contact"))
        
        //Change Submit Button title 
        self.btnSubmit.setTitle("Change", for: .normal)
    }
    
    private func prepareScreenForNoContact(){
        self.lblFirst.text = AppConstants.ScreenSpecificConstant.EmergencyContactScreen.NO_CONTACT_ADDED_MESSAGE
        self.lblSecond.text = ""
        self.imgViewDial.image = #imageLiteral(resourceName: "ic_add_emergency_contact")
        
        //Change Submit Button title
        self.btnSubmit.setTitle("Add Contact", for: .normal)
    }
    
    @IBAction func btnSubmitClick(_ sender: Any) {
        self.addContact()
        
    }
    
    private func addContact(){
        contactHandler.delegate = self
        contactHandler.requestForAccess { (status) in
            if status{
                self.contactHandler.openAddressbook(vc: self)
            }
        }
    }
    
    func addEmegencyContact(){
        self.emergencyContactPresenter.sendEmergencyContactRequest(withEmergencyContactViewRequestModel: self.viewRequestModel)
    }
    
    func uploadImageWithData(data: Data){
        self.uploadImagePresenter.sendUploadImageRequest(withImage: data)
    }
}

extension EmergencyContactViewController: ContactViewDelegate{
    func contactPickerDidSelectContact(withContact contact: CNContact) {
        
        if let numbers = contact.phoneNumbers as? [CNLabeledValue<CNPhoneNumber>]{
            if numbers.count > 1{
                self.selectedContact = contact
                
                let actionSheet = UIAlertController(title: "SELECT", message: "", preferredStyle: .actionSheet)
                
                for number in numbers{
                    if let numberDigits = number.value.value(forKey: "digits") as? String{
                        let action = UIAlertAction(title: numberDigits, style: .default, handler: numberSelectionHandler)
                        actionSheet.addAction(action)
                    }
                }
                
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
                Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false
                    , block: { (completion) in
                        self.present(actionSheet, animated: true, completion: nil)
                })
            }
            else if numbers.count == 1{
                if let numberValue = contact.phoneNumbers.first?.value, let number = numberValue.value(forKey: "digits") as? String{
                    
                    let countryCode = numberValue.value(forKey: "countryCode") as! String
                    let name: String = CNContactFormatter.string(from: contact, style: .fullName) ?? "\(String(describing: numberValue))"
                    let imageData = contact.imageData
                    
                    self.updateEmergencyContact(withNumberDigits: number, countryCode: countryCode, name: name, imageData: imageData)
                }
                else{
                    AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.EmergencyContactScreen.INVALID_NUMBER_MESSAGE)
                }
            }
            else{
                AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.EmergencyContactScreen.INVALID_NUMBER_MESSAGE)

            }
        }
        else{
            AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.EmergencyContactScreen.INVALID_NUMBER_MESSAGE)
        }
        
    }
    
    func numberSelectionHandler(alert: UIAlertAction){
        let countryCode = self.getCountryCodeForContact(withDigits: alert.title!)
        let name: String = CNContactFormatter.string(from: self.selectedContact, style: .fullName) ?? "\(String(describing: alert.title))"
        let imageData = self.selectedContact.imageData
        
        self.updateEmergencyContact(withNumberDigits: alert.title!, countryCode: countryCode, name: name, imageData: imageData)
    }
    
    func getCountryCodeForContact(withDigits digit: String)->String{
        if let numbers = self.selectedContact.phoneNumbers as? [CNLabeledValue<CNPhoneNumber>]{
            for number in numbers{
                if (number.value.value(forKey: "digits") as! String) == digit{
                    return number.value.value(forKey: "countryCode") as! String
                }
            }
        }
        
        return "1"
    }
    
    func updateEmergencyContact(withNumberDigits number: String, countryCode: String, name: String, imageData: Data?){
        
        let diallingCode = Locale.diallingCodeFromCountryCode(code: countryCode.uppercased())
        let formattedDiallingCode = AppUtility.getFormattedDiallingCodeFromCode(code: diallingCode)
        
        print("Name: \(name), CountryCode: \(countryCode), Phone: \(number), ImageData:\(String(describing: imageData))")
        
        //Update Model
        self.viewRequestModel = EmergencyContactViewRequestModel(emergencyContact: formattedDiallingCode+number, userId: AppDelegate.sharedInstance.userInformation.id, emergencyContactName: name, emergencyContactImageId: nil)
        
        if let imageData = imageData{
            self.uploadImageWithData(data: imageData)
        }
        else{
            self.addEmegencyContact()
        }
    }
    
    func contactPickerDismissed() {
        
    }
    
    func deviceContactAccessDenied(){
        self.view.endEditing(true)
        
        DispatchQueue.main.async {
            let goToSettings = JHTAlertAction(title: AppConstants.ScreenSpecificConstant.Common.SETTINGS, style: .default, handler: { (action) in
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            })
            
            AppUtility.showCustomAlert(title: AppConstants.ScreenSpecificConstant.Common.ACCESS_DENIED, message: AppConstants.ScreenSpecificConstant.Common.CONTACT_ACCESS_PERMISSION_MESSAGE, presentingVC: self, actionItems: [goToSettings])
        }
    }
}

//MARK: BaseViewProtocol Methods
extension EmergencyContactViewController{
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}

extension EmergencyContactViewController: EmergencyContactViewDelegate{
    func emergencyContactUpdatedSuccessfully(withUpdatedUserProfile profile: LoginResponseModel){
        //Update user data
        AppDelegate.sharedInstance.userInformation = profile
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        //Update UI 
        self.prepareScreenWithUserData()
        
        //Updat with added number
        self.delegate?.emergencyContactAdded(withNumber: profile.emergencyContact!)
    }
}

extension EmergencyContactViewController: DocumentUploadDelegate{

    func documentUploadedSuccessfully(withResponseModel objDocModel: DocumentUploadResponseModel) {
        self.viewRequestModel.emergencyContactImageId = objDocModel.id
        self.addEmegencyContact()
    }
    
    func insuranceDocUpdatedSuccessfully(withResponseModel objInsurance: InsuranceDoc) {
    }
    
    func vehicleDocUpdatedSuccessfully(withResponseModel objVehicelModel:VehicleDoc){
    }
    
    func selfieUploadedSuccessfully(withResponseModel objProfileModel:LoginResponseModel){
    }
}
