
//Notes:- This class is used as presenter for DriverInfoViewPresenter

import Foundation
import ObjectMapper

class DriverInfoViewPresenter: ResponseCallback{
    
//MARK:- DriverInfoViewPresenter local properties
    
    private weak var driverInfoViewDelegate          : DriverInfoViewDelegate?
    private lazy var driverInfoBusinessLogic         : DriverInfoBusinessLogic = DriverInfoBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:DriverInfoViewDelegate){
        self.driverInfoViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.driverInfoViewDelegate?.hideLoader()
        if responseObject is DriverInfoResponseModel
        {
            self.driverInfoViewDelegate?.driverInfoFetched(withResponseModel: responseObject as!    DriverInfoResponseModel)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.driverInfoViewDelegate?.hideLoader()
        self.driverInfoViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call driverInfo Api.
    
    func sendDriverInfoRequest(withDriverInfoViewRequestModel driverInfoViewRequestModel:DriverInfoViewRequestModel){
        
        self.driverInfoViewDelegate?.showLoader()
        
        var requestModel : DriverInfoRequestModel!

        requestModel = DriverInfoRequestModel.Builder()
            .setDriverId(driverInfoViewRequestModel.driverId)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.driverInfoBusinessLogic.performDriverInfo(withDriverInfoRequestModel: requestModel, presenterDelegate: self)
    }
}
