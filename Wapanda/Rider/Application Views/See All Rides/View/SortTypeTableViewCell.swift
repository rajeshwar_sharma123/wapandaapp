

import UIKit

class SortTypeTableViewCell: UITableViewCell {
    @IBOutlet weak var viewSelected: UIBorderView!
    @IBOutlet weak var lblSortingType: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    /// Binds data with cell
    ///
    /// - Parameter type: string value as car type
    func bind(withSortingType type: String){
        self.lblSortingType.text = type
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        if selected{
            self.viewSelected.backgroundColor = UIColor.white
            self.contentView.backgroundColor = UIColor.appThemeColor()
        }
        else{
            self.viewSelected.backgroundColor = UIColor.appDarkThemeColor()
            self.contentView.backgroundColor = UIColor.appDarkThemeColor()
        }
    }
    
}
