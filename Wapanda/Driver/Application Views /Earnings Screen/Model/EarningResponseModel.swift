//
//  EarningResponseModel.swift
//
//  Created by Daffolapmac on 23/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class EarningResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let monthToDateEarning = "monthToDateEarning"
    static let weekToDateEarning = "weekToDateEarning"
    static let weeklyEarnings = "weeklyEarnings"
    static let totalEarningTillNow = "totalEarningTillNow"
    static let lastTripAmount = "lastTripAmount"
  }

  // MARK: Properties
  public var monthToDateEarning: Float?
  public var weekToDateEarning: Float?
  public var weeklyEarnings: [WeeklyEarnings]?
  public var totalEarningTillNow: Float?
  public var lastTripAmount: Float?
    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    monthToDateEarning <- map[SerializationKeys.monthToDateEarning]
    weekToDateEarning <- map[SerializationKeys.weekToDateEarning]
    weeklyEarnings <- map[SerializationKeys.weeklyEarnings]
    totalEarningTillNow <- map[SerializationKeys.totalEarningTillNow]
    lastTripAmount <- map[SerializationKeys.lastTripAmount]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = monthToDateEarning { dictionary[SerializationKeys.monthToDateEarning] = value }
    if let value = weekToDateEarning { dictionary[SerializationKeys.weekToDateEarning] = value }
    if let value = weeklyEarnings { dictionary[SerializationKeys.weeklyEarnings] = value.map { $0.dictionaryRepresentation() } }
    if let value = totalEarningTillNow { dictionary[SerializationKeys.totalEarningTillNow] = value }
     if let value = lastTripAmount { dictionary[SerializationKeys.lastTripAmount] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.monthToDateEarning = aDecoder.decodeObject(forKey: SerializationKeys.monthToDateEarning) as? Float
    self.weekToDateEarning = aDecoder.decodeObject(forKey: SerializationKeys.weekToDateEarning) as? Float
    self.weeklyEarnings = aDecoder.decodeObject(forKey: SerializationKeys.weeklyEarnings) as? [WeeklyEarnings]
    self.totalEarningTillNow = aDecoder.decodeObject(forKey: SerializationKeys.totalEarningTillNow) as? Float
     self.lastTripAmount = aDecoder.decodeObject(forKey: SerializationKeys.lastTripAmount) as? Float
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(monthToDateEarning, forKey: SerializationKeys.monthToDateEarning)
    aCoder.encode(weekToDateEarning, forKey: SerializationKeys.weekToDateEarning)
    aCoder.encode(weeklyEarnings, forKey: SerializationKeys.weeklyEarnings)
    aCoder.encode(totalEarningTillNow, forKey: SerializationKeys.totalEarningTillNow)
     aCoder.encode(lastTripAmount, forKey: SerializationKeys.lastTripAmount)
  }

}
