//
//  DriverProfileViewController.swift
//  Wapanda
//
//  Created by daffomac-31 on 22/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import JHTAlertController

class DriverProfileViewController: BaseViewController, UpdateUserProfileViewDelegate {
    
    enum SCREEN_MODE{
        case EDIT
        case SAVE
    }
    
    @IBOutlet weak var lblVehicleNo: UILabel!
    @IBOutlet weak var lblMake: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
    
    @IBOutlet weak var viewBorderEmail: UIView!
    @IBOutlet weak var viewBorderFirstName: UIView!
    @IBOutlet weak var viewBorderLastName: UIView!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewLastName: UIView!
    
    @IBOutlet weak var lblErrorEmail: UILabel!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblErrorFirstName: UILabel!
    @IBOutlet weak var lblErrorLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var imgViewRider: UIImageView!
    @IBOutlet weak var imgViewVehicle: UIImageView!
    @IBOutlet weak var viewEditProfileImage: UIBorderView!
    var screenMode = SCREEN_MODE.SAVE
    var selectedAddressTitle: String!
    var viewRequestModel: UpdateUserProfileViewRequestModel!
    var presenterUpdateUserProfile: UpdateUserProfileViewPresenter!
    var imageUploadAlertController : UIAlertController!
    let imagePicker = PKCCrop()
    var uploadImagePresenter: DocumentUploadPresenter!
    @IBOutlet weak var _iBottomLayoutSavedVehicleInfo: NSLayoutConstraint!
    @IBOutlet weak var viewSavedVehicleInformation: UIView!
    @IBOutlet weak var viewEditVehicleInformation: UIView!
    @IBOutlet weak var viewContentScrollView: UIView!
    @IBOutlet weak var lblEditVehicleInformation: UILabel!
    
    @IBOutlet weak var _iBottomLayoutEditVehicleInfo: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.prepareScreenForSave()
        self.presenterUpdateUserProfile = UpdateUserProfileViewPresenter(delegate: self)
        self.setupActionSheetController()
        self.uploadImagePresenter = DocumentUploadPresenter(delegate: self)
        self.setUpImageCropType()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initView()
        self.setupNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     This function setup the Navigation bar
     - returns :void
     */
    private func setupNavigationBar() ->Void{
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.RiderProfile.RIDER_PROFILE_TITLE, color: UIColor.appThemeColor(), isTranslucent: false)
        self.customizeNavigationBackButton()
        
        if self.screenMode == .SAVE{
            self.addNavigationRightButtonWithTitle(title: AppConstants.ScreenSpecificConstant.Common.EDIT_BUTTON_TITLE)
        }
        else{
            self.addNavigationRightButtonWithTitle(title: AppConstants.ScreenSpecificConstant.Common.SAVE_BUTTON_TITLE)
        }
    }
    
    override func backButtonClick() {
        if screenMode == .EDIT{
            //Move Back to save screen
            self.initView()
            self.prepareScreenForSave()
        }
        else{
            //Go back to home
            super.backButtonClick()
        }
    }
    
    func initView(){
        
        self.viewBorderFirstName.layer.borderWidth = 1.0
        self.viewBorderFirstName.layer.borderColor = UIColor.appSeperatorColor().cgColor
        
        self.viewBorderLastName.layer.borderWidth = 1.0
        self.viewBorderLastName.layer.borderColor = UIColor.appSeperatorColor().cgColor
        
        
        self.viewBorderEmail.layer.borderWidth = 1.0
        self.viewBorderEmail.layer.borderColor = UIColor.appSeperatorColor().cgColor
        
        self.initPersonInformation()
        
        self.initVehicleInformation()
        
        self.prepareEditDocumentLabelText()
        
        //Pre-fill request model
        self.prepareInitalDataSource()
    }
    
    func initPersonInformation(){
        
        self.txtFieldFirstName.text = AppDelegate.sharedInstance.userInformation.firstName ?? ""
        self.txtFieldLastName.text = AppDelegate.sharedInstance.userInformation.lastName ?? ""
        self.txtFieldEmail.text = AppDelegate.sharedInstance.userInformation.email ?? ""

        
        self.lblFirstName.text = AppDelegate.sharedInstance.userInformation.firstName ?? ""
        self.lblLastName.text = AppDelegate.sharedInstance.userInformation.lastName ?? ""
        
        self.lblPhone.text = "+" + (AppDelegate.sharedInstance.userInformation.phone ?? "")
        self.lblEmail.text = AppDelegate.sharedInstance.userInformation.email ?? ""
        
        if let imgId = AppDelegate.sharedInstance.userInformation.profileImageFileId{
            let url = AppUtility.getImageURL(fromImageId: imgId)
            self.imgViewRider.setImageWith(URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_emergency_contact"))
        }
        
        self.imgViewRider.setCornerCircular(46.0)
    }
    
    func initVehicleInformation(){
        self.imgViewVehicle.setCornerCircular(46.0)
        
        self.lblVehicleNo.text = AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.carPlateNumber ?? ""
        self.lblMake.text = AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.make ?? ""
        self.lblModel.text = AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.model ?? ""
        
        if let year = AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.year{
            self.lblYear.text = "\(year)"
        }
        
        self.lblServiceName.text = AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.companyName ?? ""
        self.lblCarType.text = AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.carType?.vehicleType ?? ""
        
        if let imgId = AppDelegate.sharedInstance.userInformation.driverDocs?.vehicleDoc?.imageId{
            let url = AppUtility.getImageURL(fromImageId: imgId)
            self.imgViewVehicle.setImageWith(URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_emergency_contact"))
        }
    }
    
    func prepareEditDocumentLabelText(){
        self.lblEditVehicleInformation.numberOfLines = 0
        let blueColor = UIColor(red: 69/255.0, green: 88/255.0, blue: 230/255.0, alpha: 1.0)
        let greyColor = UIColor(red: 64/255.0, green: 64/255.0, blue: 64/255.0, alpha: 1.0)

      //  let attributes = [NSForegroundColorAttributeName: blueColor, NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue,NSFontAttributeName: UIFont.getSanFranciscoBold(withSize: 16.0)] as [String : Any]
        
        
      
          let  attributes = [NSForegroundColorAttributeName: blueColor, NSUnderlineStyleAttributeName: NSUnderlineStyle.styleThick.rawValue,NSBaselineOffsetAttributeName:0,NSFontAttributeName: UIFont.getSanFranciscoBold(withSize: 16.0)] as [String : Any]
        
          let attributesForEdit = [NSForegroundColorAttributeName: greyColor,NSFontAttributeName: UIFont.getSanFranciscoRegular(withSize: 16.0)] as [String : Any]
        
        let editRange = NSString(string: AppConstants.ScreenSpecificConstant.DriverProfile.EDIT_DOCUMENT_LABEL_STRING).range(of: AppConstants.ScreenSpecificConstant.DriverProfile.EDIT_LABEL_STRING)
        
        let documentsTextRange = NSString(string: AppConstants.ScreenSpecificConstant.DriverProfile.EDIT_DOCUMENT_LABEL_STRING).range(of: AppConstants.ScreenSpecificConstant.DriverProfile.DOCUMENT_LABEL_STRING)
        

        
        let mutableText = NSMutableAttributedString(string: AppConstants.ScreenSpecificConstant.DriverProfile.EDIT_DOCUMENT_LABEL_STRING)
    
    
        
        mutableText.addAttributes(attributesForEdit, range: editRange)
        mutableText.addAttributes(attributes, range: documentsTextRange)
        self.lblEditVehicleInformation.attributedText = mutableText
    }

    
    private func setUpImageCropType()
    {
        self.imagePicker.delegate = self
        PKCCropManager.shared.cropType = .rateAndNoneMarginCircle
    }
    
    
    func prepareInitalDataSource(){
        
        viewRequestModel = UpdateUserProfileViewRequestModel()
        viewRequestModel.firstName = AppDelegate.sharedInstance.userInformation.firstName
        viewRequestModel.lastName = AppDelegate.sharedInstance.userInformation.lastName
        
        viewRequestModel.email = AppDelegate.sharedInstance.userInformation.email

        viewRequestModel.profileImageFileId = AppDelegate.sharedInstance.userInformation.profileImageFileId
        
        if let work = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.work{
            viewRequestModel.addresses = []
            
            let workAddress = UserAddress(lat: work.lat!, lng: work.lng!, address: work.address!)
            let address = UserAddresses(addresType: AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_WORK, address: workAddress)
            viewRequestModel.addresses!.append(address)
        }
        
        if let home = AppDelegate.sharedInstance.userInformation.riderProfile?.addresses?.home{
            
            if viewRequestModel.addresses == nil{
                viewRequestModel.addresses = []
            }
            
            let homeAddress = UserAddress(lat: home.lat!, lng: home.lng!, address: home.address!)
            let address = UserAddresses(addresType: AppConstants.ScreenSpecificConstant.RiderHomeScreen.ADDRESS_TITLE_HOME, address: homeAddress)
            viewRequestModel.addresses!.append(address)
        }
    }
    
    func prepareDataSourceOnSaveClick(){
        viewRequestModel.firstName = self.txtFieldFirstName.text?.getWhitespaceTrimmedString()
        viewRequestModel.lastName = self.txtFieldLastName.text?.getWhitespaceTrimmedString()
        viewRequestModel.email = self.txtFieldEmail.text?.getWhitespaceTrimmedString()

    }
    
    private func prepareScreenForEdit(){
        self.lblFirstName.isHidden = true
        self.lblLastName.isHidden = true
//        self.lblEmail.isHidden = true

        viewFirstName.isUserInteractionEnabled = true
        viewLastName.isUserInteractionEnabled = true
        
//        viewEmail.isUserInteractionEnabled = true

        
//        self.viewEmail.bringSubview(toFront: self.viewBorderEmail)

        self.viewFirstName.bringSubview(toFront: self.viewBorderFirstName)
        self.viewLastName.bringSubview(toFront: self.viewBorderLastName)
      

        self.screenMode = .EDIT
        self.viewEditProfileImage.isHidden = false
        self.addNavigationRightButtonWithTitle(title: AppConstants.ScreenSpecificConstant.Common.SAVE_BUTTON_TITLE)
        self._iBottomLayoutSavedVehicleInfo.isActive = false
        self._iBottomLayoutEditVehicleInfo.isActive = true
        self.viewContentScrollView.bringSubview(toFront: self.viewEditVehicleInformation)
        self.viewEditVehicleInformation.isHidden = false
        self.viewSavedVehicleInformation.isHidden = true
    }
    
    func saveClick(){
        self.prepareDataSourceOnSaveClick()
        self.presenterUpdateUserProfile.sendUpdateUserProfileRequest(withUpdateUserProfileViewRequestModel: self.viewRequestModel)
    }
    
    func prepareScreenForSave(){
        
        viewEmail.isUserInteractionEnabled = false
        viewFirstName.isUserInteractionEnabled = false
        viewLastName.isUserInteractionEnabled = false
        self.lblErrorFirstName.text = ""
        self.lblFirstName.isHidden = false
        self.lblLastName.isHidden = false
        self.lblEmail.isHidden = false

        
        self.lblErrorLastName.text = ""
        self.lblErrorEmail.text = ""

        
        self.viewEmail.bringSubview(toFront: self.lblEmail)
        
        self.viewFirstName.bringSubview(toFront: self.lblFirstName)
        self.viewLastName.bringSubview(toFront: self.lblLastName)
        
        
        self.view.endEditing(true)
        self.screenMode = .SAVE
        self.viewEditProfileImage.isHidden = true
        self.addNavigationRightButtonWithTitle(title: AppConstants.ScreenSpecificConstant.Common.EDIT_BUTTON_TITLE)
        self._iBottomLayoutSavedVehicleInfo.isActive = true
        self._iBottomLayoutEditVehicleInfo.isActive = false
        self.viewContentScrollView.bringSubview(toFront: self.viewSavedVehicleInformation)
        self.viewEditVehicleInformation.isHidden = true
        self.viewSavedVehicleInformation.isHidden = false
    }
    
    override func rightButtonClick() {
        if self.screenMode == .EDIT{
            self.saveClick()
        }
        else{
            self.prepareScreenForEdit()
        }
    }
    
    @IBAction func btnSignOutClick(_ sender: Any) {
        self.logout()
    }
    
    @IBAction func btnChangeProfilePic(_ sender: Any) {
        self.view.endEditing(true)
        
        present(imageUploadAlertController, animated: true) {}
    }
    
    @IBAction func tapOnEditVehicleLabel(_ sender: UITapGestureRecognizer) {
        let text = self.lblEditVehicleInformation.text ?? ""
        let documentsTextRange = (text as NSString).range(of: AppConstants.ScreenSpecificConstant.DriverProfile.DOCUMENT_LABEL_STRING)
        
        if sender.didTapAttributedTextInLabel(label: self.lblEditVehicleInformation, inRange: documentsTextRange){
            print("Document Clicked")
            self.driverDocumentClicked()
        }
    }
    
    func userProfileUpdatedSuccessfully(withResponse data: LoginResponseModel){
        AppDelegate.sharedInstance.userInformation = data
        AppDelegate.sharedInstance.userInformation.saveUser()
        
        self.initView()
        self.prepareScreenForSave()
        
        AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.RiderProfile.PROFILE_UPDATE_SUCCESS_MESSAGE)
    }
    
    func driverDocumentClicked(){
        let docSetUpVC = UIViewController.getViewController(DriverSetUpScreenViewController.self,storyboard: UIStoryboard.Storyboard.Driver.object)
        docSetUpVC.screenTitle = "Documents"
        docSetUpVC.shouldShowVerifyScreeen = false
        UIApplication.shared.visibleViewController?.navigationController?.pushViewController(docSetUpVC, animated: true)
    }
}

//MARK: BaseViewProtocol Methods
extension DriverProfileViewController{
    func showLoader(){
        super.showLoader(self)
    }
    
    func hideLoader(){
        super.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
}


extension DriverProfileViewController{
    /**
     This method is used to setup ActionSheetController
     */
    func setupActionSheetController(){
        
        imageUploadAlertController = UIAlertController(title: "Take Photo from :", message: "", preferredStyle: .actionSheet)
        imageUploadAlertController.view.tintColor = UIColor.appThemeColor()
        
        let individualAction = UIAlertAction(title: "Camera", style: .default,handler: { (action:UIAlertAction!) in
            self.presentImagePickerView(with: UIImagePickerControllerSourceType.camera)
        })
        imageUploadAlertController.addAction(individualAction)
        
        let partnershipAction = UIAlertAction(title: "Gallery", style: .default, handler:{ (action:UIAlertAction!) in
            self.presentImagePickerView(with: UIImagePickerControllerSourceType.photoLibrary)
        })
        imageUploadAlertController.addAction(partnershipAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        imageUploadAlertController.addAction(cancelAction)
    }
    
    func presentImagePickerView(with sourceType:UIImagePickerControllerSourceType)
    {
        if sourceType == .camera
        {
            self.imagePicker.cameraCrop(withDirection: .front)
        }
        else
        {
            self.imagePicker.photoCrop()
        }
    }
}

extension DriverProfileViewController: PKCCropDelegate{
    
    func pkcCropAccessPermissionsDenied() {
        
    }
    func pkcCropAccessPermissionsDenied(_ type: UIImagePickerControllerSourceType) {
        
        DispatchQueue.main.async {
            
            let alertController = JHTAlertController(
                
                title: type == .camera ? AppConstants.ScreenSpecificConstant.UploadScreen.TITLE_CAMERA_ACCESS_DISABLED : AppConstants.ScreenSpecificConstant.UploadScreen.TITLE_GALLERY_ACCESS_DISABLED,
                message: type == .camera ? AppConstants.ScreenSpecificConstant.UploadScreen.MESSAGE_CAMERA_SERVICE_DISABLED : AppConstants.ScreenSpecificConstant.UploadScreen.MESSAGE_GALLERY_SERVICE_DISABLED,
                preferredStyle: .alert)
            alertController.alertBackgroundColor = .white
            alertController.titleViewBackgroundColor = .white
            
            alertController.messageTextColor = .black
            alertController.titleTextColor = .black
            
            alertController.setAllButtonBackgroundColors(to: UIColor.appThemeColor())
            alertController.hasRoundedCorners = true
            
            alertController.titleFont = UIFont.getSanFranciscoMedium(withSize: 20)
            alertController.messageFont = UIFont.getSanFranciscoRegular(withSize: 16)
            let openAction = JHTAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    //                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    //                } else {
                    //                    // Fallback on earlier versions
                    //}
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func pkcCropController() -> UIViewController {
        return self
    }
    func pkcCropImage(_ image: UIImage) {
        self.uploadImagePresenter.sendUploadImageRequest(withImage: UIImageJPEGRepresentation(image, 1.0)!)
        
    }
}


extension DriverProfileViewController: DocumentUploadDelegate{
    
    func documentUploadedSuccessfully(withResponseModel objDocModel: DocumentUploadResponseModel) {
        self.viewRequestModel.profileImageFileId = objDocModel.id!
        
        if let imgId = objDocModel.id{
            let url = AppUtility.getImageURL(fromImageId: imgId)
            self.imgViewRider.setImageWith(URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_emergency_contact"))
        }
        
    }
    
    func insuranceDocUpdatedSuccessfully(withResponseModel objInsurance: InsuranceDoc) {
    }
    
    func vehicleDocUpdatedSuccessfully(withResponseModel objVehicelModel:VehicleDoc){
    }
    
    func selfieUploadedSuccessfully(withResponseModel objProfileModel:LoginResponseModel){
    }
}

extension DriverProfileViewController: UITextFieldDelegate, TextFieldValidationDelegate{
    
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType){
        
        switch forTextFields {
        case .FirstName:
            self.lblErrorFirstName.text = message
            break
        case .LastName:
            self.lblErrorLastName.text = message
            break
        case .EmailAddress:
            self.lblErrorEmail.text = message
            break
        default:
            break
        }
    }
    
    //MARK: UITextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField{
        case self.txtFieldFirstName:
            self.lblErrorFirstName.text = ""
        case self.txtFieldLastName:
            self.lblErrorLastName.text = ""
        case self.txtFieldEmail:
            self.lblErrorEmail.text = ""
        default:
            break
        }
        
            if string == "" || textField.text!.characters.count <= 25{
                return true
        }
        else{
                if textField == self.txtFieldEmail{
                    return true
                }else{
                    return false

                }
            }
        }
}





