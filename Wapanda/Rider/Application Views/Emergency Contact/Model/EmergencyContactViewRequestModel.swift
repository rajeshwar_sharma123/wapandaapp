
//Notes:- This model is used as a model that holds signup properties from emergency contact view controller.

struct EmergencyContactViewRequestModel {
    
    var emergencyContact                   : String!  //Number along with country code
    var userId: String! //User id
    var emergencyContactName: String!
    var emergencyContactImageId: String?
}
