//
//  WapandaCustomerContext.swift
//  Wapanda
//
//  Created by Daffolapmac on 31/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import Stripe

class WapandaCustomer: STPCustomer {
    var wapandaSources: [STPSourceProtocol] = []
    var wapandaDefaultSource: STPSourceProtocol? = nil
    var wapandaShippingAddress: STPAddress?
    
    override init() {
    }
    
    override var sources: [STPSourceProtocol] {
        get {
            return wapandaSources
        }
        set {
            wapandaSources = newValue
        }
    }
    
    override var defaultSource: STPSourceProtocol? {
        get {
            return wapandaDefaultSource
        }
        set {
            wapandaDefaultSource = newValue
        }
    }
    
    override var shippingAddress: STPAddress {
        get {
            return wapandaShippingAddress!
        }
        set {
            wapandaShippingAddress = newValue
        }
    }
}

class WapandaCustomerContext: STPCustomerContext {
    
    let customer = WapandaCustomer()
    
    
    override func retrieveCustomer(_ completion: STPCustomerCompletionBlock? = nil) {
        if let completion = completion {
            completion(customer, nil)
        }
    }
    
    override func attachSource(toCustomer source: STPSourceProtocol, completion: @escaping STPErrorBlock) {
        if let token = source as? STPToken, let card = token.card {
            customer.sources.append(card)
        }
        completion(nil)
    }
    
    override func selectDefaultCustomerSource(_ source: STPSourceProtocol, completion: @escaping STPErrorBlock) {
        if customer.sources.contains(where: { $0.stripeID == source.stripeID }) {
            customer.defaultSource = source
        }
        completion(nil)
    }
    
    func updateCustomer(withShippingAddress shipping: STPAddress, completion: STPErrorBlock?) {
        customer.shippingAddress = shipping
        if let completion = completion {
            completion(nil)
        }
    }
    
    func detachSource(fromCustomer source: STPSourceProtocol, completion: STPErrorBlock?) {
        if let index = customer.sources.index(where: { $0.stripeID == source.stripeID }) {
            customer.sources.remove(at: index)
        }
        if let completion = completion {
            completion(nil)
        }
    }
}

