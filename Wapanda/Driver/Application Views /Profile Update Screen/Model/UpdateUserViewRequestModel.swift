
//Notes:- This model is used as a model that holds signup properties from signup view controller.

struct UpdateUserViewRequestModel {
    
    var agreeTermsAndConditions  : Bool! = false
    var userId                   : String!
    
}
