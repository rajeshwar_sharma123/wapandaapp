//
//  BankInfoRequestModel
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class BankInfoRequestModel {
    
    //MARK:- BankInfoRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var stripeUserId: String!
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.stripeUserId = builder.stripeUserId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var stripeUserId: String!
        
        /**
         This method is used for setting Code 
         
         - parameter grantType: String parameter
         - returns: returning Builder Object
         */
        func setCode(_ code:String) -> Builder{
            requestBody["code"] = code as AnyObject?
            return self
        }
        /**
         This method is used for setting Grant Type
         
         - parameter grantType: String parameter
         - returns: returning Builder Object
         */
        func setGrantType(_ grantType:String) -> Builder{
            requestBody["grant_type"] = grantType as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Client ID of Stripe Account
         
         - parameter clientId: String parameter
         
         - returns: returning Builder Object
         */
        func setClientId(_ clientId:String)->Builder{
            requestBody["client_id"] = clientId as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Client Secret of Stripe Account
         
         - parameter clientSecret: String parameter
         
         - returns: returning Builder Object
         */
        func setClientSecret(_ clientSecret:String)->Builder{
            requestBody["client_secret"] = clientSecret as AnyObject?
            return self
        }
        /**
         This method is used for setting User id
         
         - parameter stripeUserId: String parameter that is going to be set on stripeUserId
         
         - returns: returning Builder Object
         */
        func setAuthCode(_ stripeUserId:String)->Builder{
            self.stripeUserId = stripeUserId
            return self
        }

        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->BankInfoRequestModel{
            return BankInfoRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Connect Driver Stripe account to API end point
     
     -returns: String containg end point
     */
    func getConnectEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.CONNECT_DRIVER_ACCOUNT, self.stripeUserId)
    }
    
    /**
     This method is used for getting auth stripe end point
     
     -returns: String containg end point
     */
    func getStripeAuthEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.AUTH_STRIPE_CODE)
    }
    
}
