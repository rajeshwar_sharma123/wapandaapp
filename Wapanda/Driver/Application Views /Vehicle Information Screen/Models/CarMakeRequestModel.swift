//
//  CarMakeRequestModel.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/24/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation

enum ParameterType:String{
    
    case Make = "getMakes"
    case Model = "getModels"
    case Year = "getYears"
}



class CarMakeRequestModel  {
    
    //MARK:- CarMakeRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var userId: String!
    var year: Int!
    var paramType : ParameterType = .Year
    var make: String = ""
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.year = builder.year
        self.paramType = builder.paramType
        self.make = builder.make
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var year: Int = 0
        var paramType : ParameterType = .Year
        var make = ""

        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used for setting the year
         
         - parameter skip: Int parameter that is going to be set on year
         
         - returns: returning Builder Object
         */
        func setYear(_ year:Int)->Builder{
            self.year = year
            return self
        }
        
        
        /**
         This method is used for setting the year
         
         - parameter skip: Int parameter that is going to be set on year
         
         - returns: returning Builder Object
         */
        func setMake(_ make:String)->Builder{
            self.make = make
            return self
        }
        
        /**
         This method is used for setting the parameter type
         
         - parameter skip: Int parameter that is going to be set on year
         
         - returns: returning Builder Object
         */
        func setType(_ type:ParameterType)->Builder{
            self.paramType = type
            return self
        }
        
        /**
         This method is used to set properties in upper class of LoginRequestModel
         and provide LoginRequestModel object.
         
         -returns : LoginRequestModel
         */
        func build()->CarMakeRequestModel{
            return CarMakeRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting login end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.CAR_MAKE_REQUEST_BY_YEAR,self.paramType.rawValue,self.year,self.make)

    }
    
}
