//
//  Type.swift
//
//  Created by  on 8/25/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Type: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let _id = "_id"
    static let vehicleType = "vehicleType"
    static let price = "price"
    static let icon = "icon"
    static let isActive = "isActive"
  }

  // MARK: Properties
  public var _id: String?
  public var vehicleType: String?
  public var price: Float?
  public var icon: String?
  public var isActive: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    _id <- map[SerializationKeys._id]
    vehicleType <- map[SerializationKeys.vehicleType]
    price <- map[SerializationKeys.price]
    icon <- map[SerializationKeys.icon]
    isActive <- map[SerializationKeys.isActive]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = _id { dictionary[SerializationKeys._id] = value }
    if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = icon { dictionary[SerializationKeys.icon] = value }
    if let value = isActive { dictionary[SerializationKeys.isActive] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self._id = aDecoder.decodeObject(forKey: SerializationKeys._id) as? String
    self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Float
    self.icon = aDecoder.decodeObject(forKey: SerializationKeys.icon) as? String
    self.isActive = aDecoder.decodeObject(forKey: SerializationKeys.isActive) as? Bool
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(_id, forKey: SerializationKeys._id)
    aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
    aCoder.encode(price, forKey: SerializationKeys.price)
    aCoder.encode(icon, forKey: SerializationKeys.icon)
    aCoder.encode(isActive, forKey: SerializationKeys.isActive)
  }

}
