//
//  VehicleInfoScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct VehicleInfoScreenRequestModel {
    var type: ParameterType = .Make
    var year                : Int = 0
    var make                : String = ""
    var makeId:                 String = ""
    var model               : String = ""
    var companyName         : String = ""
    var carType             : String = ""
    var carTypeId             : String = ""
    var carPlateNumber      : String = ""
}
