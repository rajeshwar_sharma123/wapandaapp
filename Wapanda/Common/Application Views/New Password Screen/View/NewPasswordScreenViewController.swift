//
//  NewPasswordScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/9/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class NewPasswordScreenViewController: BaseViewController {
    
    //MARK:- NewPasswordViewController local properties.
    var newPasswordPresenter : NewPasswordPresenter!
    var verifiedOtp : String!
    //MARK:- IBOutlets
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    @IBOutlet weak var btnNewPassword: CustomButton!
    @IBOutlet weak var lblPasswordError: UILabel!
    
    
    //MARK:- View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
        self.newPasswordPresenter = NewPasswordPresenter(delegate: self,textFieldValidationDelegate: self)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction for Buttons
    @IBAction func newPasswordButtonTapped(_ sender: UIButton) {
        //       self.navigateToUserOptionViewController()
        self.newPasswordPresenter.sendNewPasswordRequest(withData: self.createNewPasswordScreenRequestModelWithTextFieldInput())
    }
    
    @IBAction func showHidePasswordButtonTapped(_ sender: UIButton) {
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
        self.btnShowHidePassword.setImage(self.txtPassword.isSecureTextEntry ? #imageLiteral(resourceName: "ic_visibility_off_white"): #imageLiteral(resourceName: "ic_visibility_white"), for: .normal)
        
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.txtPassword.delegate = self
        self.setupNavigationBar()
        self.customizeTextFields()
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: "")
        self.customizeNavigationBackButton()
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        self.txtPassword.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.NewPasswordScreen.PASSWORD_PLACEHOLDER_TEXT, attributes:attributes)
    }
    
    /**
     This method is used to get the newPasswordViewRequestModel from the current input in text fields
     
     -returns : EmailNewPasswordViewRequestModel with current data in textFields
     */
    func createNewPasswordScreenRequestModelWithTextFieldInput() -> NewPasswordScreenRequestModel {
        var newPasswordScreenRequestModel = NewPasswordScreenRequestModel()
        newPasswordScreenRequestModel.otp = verifiedOtp
        newPasswordScreenRequestModel.newPassword = self.txtPassword.text
        return newPasswordScreenRequestModel
    }
    
    
    // MARK: - Navigation
    
    func navigateToLoginViewController(){
        let loginVC = UIViewController.getViewController(viewController: LoginScreenViewController.self)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
    
    // MARK: - Error Methods
    
    /**
     This method is used to show error on all the text fields with default messages.
     */
    func showAllTextFieldError(){
        self.lblPasswordError.text = AppConstants.ValidationErrors.ENTER_PASSWORD
        self.lblPasswordError.isHidden = false
    }
    
    /**
     This method is used to show error on password text fields
     
     -parameter message: String the message that is needed to diaplay as error.
     */
    func showPasswordTextFieldError(withMessage message:String) {
        
        self.lblPasswordError.text = message
        self.lblPasswordError.isHidden = false
    }
    
}

// MARK : UITextField Delegate Methods
extension NewPasswordScreenViewController : UITextFieldDelegate,TextFieldValidationDelegate
{
    func showErrorMessage(withMessage message: String, forTextFields: TextFieldsType) {
        switch forTextFields {
        case .Password:
            self.lblPasswordError.isHidden = false
            self.lblPasswordError.text = message
        default:
            break
        }
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.txtPassword:
            self.lblPasswordError.isHidden = true
            return true
        default:
            self.lblPasswordError.isHidden = true
            return true
        }
    }
}

// MARK: - NewPasswordViewDelegate Methods
extension NewPasswordScreenViewController : NewPasswordViewDelegate
{
    func showToastWithMessage(_ message: String) {
        
    }
    func passwordChangedSuccessfully()
    {
        self.hideLoader(self)
        self.navigateToLoginViewController()
    }

    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
}
