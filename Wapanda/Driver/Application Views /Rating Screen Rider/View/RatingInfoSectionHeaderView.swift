//
//  RatingInfoSectionHeaderView.swift
//  
//
//  Created by Daffolapmac on 16/10/17.
//
//

import UIKit

class RatingInfoSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var labelRatingName: UILabel!
    @IBOutlet weak var labelVehicleServiceName: UILabel!
    @IBOutlet weak var labelVehicleNumber: UILabel!
    @IBOutlet weak var labelRatingRating: UILabel!

    @IBOutlet weak var imageViewstar: UIImageView!
    /*
     Only override draw() if you perform custom drawing.
     An empty implementation adversely affects performance during animation.*/
    override func draw(_ rect: CGRect) {
        
    }
 
    func addBottomShadow()
    {
        let gradient = CAGradientLayer()
        let stopColor = UIColor.gray.cgColor
        let startColor = UIColor.white.cgColor
        
        gradient.frame = CGRect(x: 0, y: self.bounds.height, width: self.bounds.width, height: 15)
        gradient.colors = [stopColor,startColor]
        gradient.locations = [0.0,0.8]
        
        self.layer.addSublayer(gradient)
    }
    func removeBottomShadow()
    {
         self.layer.sublayers?.forEach { if $0.isKind(of: CAGradientLayer.self) {  $0.removeFromSuperlayer()} }
    }
}
