

//Notes:- This class is used for constructing CabSelection Service Request Model

class CabSelectionRequestModel {
    
    //MARK:- CabSelectionRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var sortingOrder: String?
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.sortingOrder = builder.sortingOrder
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var sortingOrder: String?
        
        /**
         This method is used for setting source coordinates
         
         - parameter lat: String parameter that is going to be set on lat
         - parameter lng: String parameter that is going to be set on lng
         
         - returns: returning Builder Object
         */
        func setSourceCoordinate(lat:Double, lng: Double) -> Builder{
            requestBody["latFrom"] = lat as AnyObject?
            requestBody["lngFrom"] = lng as AnyObject?
            return self
        }
        
        /**
         This method is used for setting destination coordinates
         
         - parameter lat: String parameter that is going to be set on lat
         - parameter lng: String parameter that is going to be set on lng
         
         - returns: returning Builder Object
         */
        func setDestinationCoordinate(lat:Double, lng: Double) -> Builder{
            requestBody["latTo"] = lat as AnyObject?
            requestBody["lngTo"] = lng as AnyObject?
            return self
        }
        
        
        /// This method is used to set car tyes for filter list
        ///
        /// - Parameter carTypes: array of car type ids
        /// - Returns: Builder Object
        func setCarTypes(carTypes: [String]?)->Builder{
            requestBody["carTypes"] = carTypes as AnyObject?
            return self
        }
        
        
        /// This method is used to set sorting order for sorted list
        ///
        /// - Parameter order: i.e. CAB_SORTING_TYPE raw value
        /// - Returns: Builder Object
        func setSortingOrder(order: String?)->Builder{
            self.sortingOrder = order
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of CabSelectionRequestModel
         and provide CabSelectionForm1ViewViewRequestModel object.
         
         -returns : CabSelectionRequestModel
         */
        func build()->CabSelectionRequestModel{
            return CabSelectionRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting CabSelection end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        if let sortingOrder = self.sortingOrder{
            return AppConstants.ApiEndPoints.REQUEST_CAB + "?sort=\(sortingOrder)&order=1"
        }
        else{
            return AppConstants.ApiEndPoints.REQUEST_CAB
        }
    }
    
}
