//
//  WaitingBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class WaitingBusinessLogic {
    
    init(){
        print("WaitingBusinessLogic init \(self)")
    }
    deinit {
        print("WaitingBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform Waiting With Valid Inputs constructed into a WaitingRequestModel
     
     - parameter inputData: Contains info for Ride Accept Request
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performRideAcceptRequest(withWaitingRequestModel WaitingRequestModel: WaitingRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForWaiting()
        WaitingAPIRequest().makeAPIRequest(withReqFormData: WaitingRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    func performRideRejectRequest(withWaitingRequestModel WaitingRequestModel: WaitingRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForWaiting()
        WaitingAPIRequest().makeRejectAPIRequest(withReqFormData: WaitingRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForWaiting() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.BIDDING_CLOSED, message: AppConstants.ErrorMessages.BIDDING_CLOSED)
        errorResolver.registerErrorCode(ErrorCodes.BIDDING_EXPIRED, message: AppConstants.ErrorMessages.BIDDING_EXPIRED)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_DRIVER, message: AppConstants.ErrorMessages.INVALID_DRIVER)
         errorResolver.registerErrorCode(ErrorCodes.INVALID_RIDER, message: AppConstants.ErrorMessages.INVALID_RIDER)
        
        return errorResolver
    }
}
