//
//  Item.swift
//  Wapanda
//
//  Created by daffomac-31 on 02/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class Item:Mappable {
    
    var content       : String = ""
    var type       : String = ""
    var createdAt       : String = ""
    var updatedAt       : String = ""
    var _id       : String = ""
    
    required internal init?(map: Map) {
        mapping(map: map)
    }
    
    init() {
        
    }
    
    internal func mapping(map: Map) {
        content         <- map["content"]
        type         <- map["type"]
        createdAt         <- map["createdAt"]
        updatedAt         <- map["updatedAt"]
        _id         <- map["_id"]
    }
}

