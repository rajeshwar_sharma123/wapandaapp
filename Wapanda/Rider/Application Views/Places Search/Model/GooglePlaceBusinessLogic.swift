
//Note :- This class contains GooglePlace Buisness Logic

class GooglePlaceBusinessLogic {
    
    
    deinit {
        print("GooglePlaceBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a GooglePlaceRequestModel
     
     - parameter inputData: Contains info for GooglePlace
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performGooglePlace(withGooglePlaceRequestModel requestModel: GooglePlaceRequestModel, presenterDelegate:ResponseCallbackGeneral) ->Void {
        
        //Adding predefined set of errors
        GooglePlaceServiceManager().makeRequest(withReqFormData: requestModel, responseCallback: presenterDelegate)
    }

}
