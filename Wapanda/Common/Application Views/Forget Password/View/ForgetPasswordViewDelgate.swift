//
//  ForgetPasswordViewDelgate.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//
enum EntryType
{
    case Email
    case Phone
}

import Foundation

//Notes:- This protocol is used as a interface which is used by ForgetPasswordViewPresenter to tranfer info to ForgetPasswordViewController

protocol ForgetPasswordViewDelegate: BaseViewProtocol{
    
    func otpSentSuccessfully(to type:EntryType, withResponseModel forgetPasswordResponseModel:ForgetPasswordResponseModel)
}

