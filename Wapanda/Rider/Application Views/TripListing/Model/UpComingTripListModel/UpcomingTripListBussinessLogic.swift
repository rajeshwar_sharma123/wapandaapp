//
//  UpcomingTripListBussinessLogic.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 19/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

//
//  BiddingUnderwayBusinessLogic.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 14/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
//Note :- This class contains UpcomingTripListRequestModel Buisness Logic

class UpcomingTripListBussinessLogic {
    
    
    deinit {
        print("UpcomingTripListBussinessLogic deinit")
    }
    
    /**
     This method is used for perform auctionDetail With Valid Inputs constructed into a UpcomingTripListRequestModel
     
     - parameter inputData: Contains info for UpcomingTripListRequestModel
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performUpcomingTripList(withUpcomingTripListRequestModel upcomingTripListRequestModel: UpcomingTripListRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = self.registerError()
        UpcomingTripListAPIRequest().makeAPIRequest(withReqFormData: upcomingTripListRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
  
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerError() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver()
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message  : AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ScreenSpecificErrorMessage.UpcomingTripList.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.ACCESS_DENIED, message  : AppConstants.ErrorMessages.ACCESS_DENIED)
         return errorResolver
    }
    
}

