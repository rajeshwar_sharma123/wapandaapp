import UIKit

class BidsListRequestModel {
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var skip: Int?
    var limit: Int?
    var sortOrder: Int?
    var sortKey: String?

    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        skip = builder.skip
        limit = builder.limit
        sortOrder = builder.sortOrder
        sortKey = builder.sortKey
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var skip: Int?
        var limit: Int?
        var sortOrder: Int?
        var sortKey: String?
        
        /**
         This method is used for setting longitude
         
         - parameter long: String parameter that is going to be set on longitude
         
         - returns: returning Builder Object
         */
        func setLongitude(_ long: Double?) -> Builder{
            if let lng = long {
                requestBody["lng"] = lng as AnyObject
            }
            return self
        }
        
        /**
         This method is used for setting latitude
         
         - parameter long: String parameter that is going to be set on latitude
         
         - returns: returning Builder Object
         */
        func setLatitude(_ lat: Double?) -> Builder{
            if let lat = lat {
                requestBody["lat"] = lat as AnyObject
            }
            return self
        }
        
        /**
         This method is used for setting Pick Up Date Time
         
         - parameter userName: String parameter that is going to be set on Stop id
         
         - returns: returning Builder Object
         */
        func setPickupTime(_ time: Double?) -> Builder{
            if let pickupDateTime = time {
                requestBody["pickupDateTime"] = pickupDateTime as AnyObject
            }
            return self
        }
        
        /**
         This method is used for setting fare
         
         - parameter userName: String parameter that is going to be set on fare
         
         - returns: returning Builder Object
         */
        func setFare(_ fare: Float?) -> Builder{
            if let fare = fare {
                requestBody["fare"] = fare as AnyObject
            }
            return self
        }
        
        
        /**
         This method is used for setting TaxClassification
         
         - parameter taxClassification: String parameter that is going to be set on TaxClassification
         
         - returns: returning Builder Object
         */
        func setSkip(_ skip: Int) -> Builder{
            self.skip = skip
            return self
        }
        
        /**
         This method is used for setting SocialSecurityNumber
         
         - parameter socialSecurityNumber: String parameter that is going to be set on SocialSecurityNumber
         
         - returns: returning Builder Object
         */
        func setLimit(_ limit:Int)->Builder{
            self.limit = limit
            return self
        }
        
        /**
         This method is used for setting Tax Id
         
         - parameter taxId: String parameter that is going to be set on Tax Id
         
         - returns: returning Builder Object
         */
        func setSortKey(_ key: String?) -> Builder {
            if let sortKey = key{
                self.sortKey = sortKey
            }
            return self
        }
        
        /**
         This method is used for setting Tax Id
         
         - parameter taxId: String parameter that is going to be set on Tax Id
         
         - returns: returning Builder Object
         */
        func setSortOrder(_ order: Int?)->Builder{
            if let sortOrder = order{
                self.sortOrder = sortOrder
            }
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of BidsListRequestModel
         and provide BidsListRequestModel object.
         
         -returns : VisitedStopRequestModel
         */
        func build() -> BidsListRequestModel{
            return BidsListRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting VisitedStop end point
     
     -returns: String containg end point
     */
    func getEndPoint() -> String{
        var url = "/auctions/filter?"
        if let skip = self.skip {
            url += "skip=\(skip)"
        }
        if let limit = self.limit {
            url += "&limit=\(limit)"
        }
        if let sortKey = self.sortKey {
            url += "&sort=\(sortKey)"
        }
        if let sortOrder = self.sortOrder {
            url += "&order=\(sortOrder)"
        }
        return url
    }
}
