//
//  NewRideResponseModel.swift
//
//  Created by  on 19/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class NewRideResponseModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let success = "success"
    static let message = "message"
    static let trip = "trip"
  }

  // MARK: Properties
  public var success: Bool? = false
  public var message: String?
  public var trip: Trip?


  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    success <- map[SerializationKeys.success]
    message <- map[SerializationKeys.message]
    trip <- map[SerializationKeys.trip]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.success] = success
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = trip { dictionary[SerializationKeys.trip] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.trip = aDecoder.decodeObject(forKey: SerializationKeys.trip) as? Trip
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(success, forKey: SerializationKeys.success)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(trip, forKey: SerializationKeys.trip)
  }

}
