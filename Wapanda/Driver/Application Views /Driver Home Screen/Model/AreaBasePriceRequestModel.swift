

//Notes:- This class is used for constructing AreaBasePrice Service Request Model

class AreaBasePriceRequestModel {
    
    //MARK:- AreaBasePriceRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var driverId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.driverId = builder.driverId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var driverId: String!
        
        /**
         This method is used for setting base price
         
         - parameter basePrice: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setLatitude(_ latitude:Double) -> Builder{
            requestBody["latitude"] = latitude as AnyObject?
            return self
        }
        
        /**
         This method is used for setting base price
         
         - parameter basePrice: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setLongitude(_ longitude:Double) -> Builder{
            requestBody["longitude"] = longitude as AnyObject?
            return self
        }
        
        /**
         This method is used for setting address type
         
         - parameter userName: String parameter that is going to be set on address type
         
         - returns: returning Builder Object
         */
        func setDriverId(_ id:String) -> Builder{
            self.driverId = id
            return self
        }
        
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of AreaBasePriceRequestModel
         and provide AreaBasePriceForm1ViewViewRequestModel object.
         
         -returns : AreaBasePriceRequestModel
         */
        func build()->AreaBasePriceRequestModel{
            return AreaBasePriceRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting AreaBasePrice end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return  String(format: AppConstants.ApiEndPoints.AREA_ONGOING_PRICE_DRIVER, self.driverId,self.requestBody["latitude"] as! Double,self.requestBody["longitude"] as! Double)
    }
    
}
