
//Note :- This class contains CreateBid Buisness Logic

class CreateBidBusinessLogic {
    
    
    deinit {
        print("CreateBidBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a CreateBidRequestModel
     
     - parameter inputData: Contains info for CreateBid
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performCreateBid(withCreateBidRequestModel createBidRequestModel: CreateBidRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        CreateBidApiRequest().makeAPIRequest(withReqFormData: createBidRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.NOT_FOUND, message  : AppConstants.ErrorMessages.NOT_FOUND)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.ACCOUNT_DISABLED_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_ON_TRIP, message  : AppConstants.ErrorMessages.ALREADY_ON_TRIP)

        errorResolver.registerErrorCode(ErrorCodes.ALREADY_BIDDING, message  : AppConstants.ErrorMessages.ALREADY_BIDDING)
        errorResolver.registerErrorCode(ErrorCodes.DRIVER_ON_TRIP, message  : AppConstants.ErrorMessages.DRIVER_ON_TRIP)
        errorResolver.registerErrorCode(ErrorCodes.DRIVER_OFFLINE, message  : AppConstants.ErrorMessages.DRIVER_OFFLINE)
        errorResolver.registerErrorCode(ErrorCodes.ACCOUNT_UNVERIFIED, message  : AppConstants.ErrorMessages.ACCOUNT_UNVERIFIED)
    
        
        return errorResolver
    }
}
