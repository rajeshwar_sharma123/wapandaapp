

//Notes:- This class is used for constructing Launch Service Request Model

class LaunchRequestModel {
    
    //MARK:- LaunchRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of LaunchRequestModel
         and provide LaunchForm1ViewViewRequestModel object.
         
         -returns : LaunchRequestModel
         */
        func build()->LaunchRequestModel{
            return LaunchRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting Launch end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER)
        {
            return String(format:AppConstants.ApiEndPoints.LAUNCH_APP,"DRIVER")
        }
        else
        {
            return String(format:AppConstants.ApiEndPoints.LAUNCH_APP,"RIDER")
        }
    }
    
}
