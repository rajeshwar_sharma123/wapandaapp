
//Notes:- This class is used as presenter for CabSelectionViewPresenter

import Foundation
import ObjectMapper

class CabSelectionViewPresenter: ResponseCallback{
    
//MARK:- CabSelectionViewPresenter local properties
    
    private weak var cabSelectionViewDelegate          : CabSelectionViewDelegate?
    private lazy var cabSelectionBusinessLogic         : CabSelectionBusinessLogic = CabSelectionBusinessLogic()

//MARK:- Constructor
    
    init(delegate responseDelegate:CabSelectionViewDelegate){
        self.cabSelectionViewDelegate = responseDelegate
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.cabSelectionViewDelegate?.hideLoader()
        if let response = responseObject as? CabSelectionResponseModel{
            self.cabSelectionViewDelegate?.cabFetched(data: response)
        }
    }
    
    func servicesManagerError(error: ErrorModel){
        self.cabSelectionViewDelegate?.hideLoader()
        self.cabSelectionViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- Methods to make decision and call cabSelection Api.
    
    func sendCabSelectionRequest(withCabSelectionViewRequestModel cabSelectionViewRequestModel:CabSelectionViewRequestModel,withLoader shouldShowLoader:Bool){
        if shouldShowLoader
        {
            self.cabSelectionViewDelegate?.showLoader()
        }
        var requestModel: CabSelectionRequestModel!
        
        requestModel = CabSelectionRequestModel.Builder()
            .setSourceCoordinate(lat: cabSelectionViewRequestModel.latFrom, lng: cabSelectionViewRequestModel.lngFrom)
            .setDestinationCoordinate(lat: cabSelectionViewRequestModel.latTo, lng: cabSelectionViewRequestModel.lngTo)
            .setCarTypes(carTypes: cabSelectionViewRequestModel.cabList)
            .setSortingOrder(order: cabSelectionViewRequestModel.sortingOrder)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE).addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON).build()
        
        self.cabSelectionBusinessLogic.performCabSelection(withCabSelectionRequestModel: requestModel, presenterDelegate: self)
    }
}
