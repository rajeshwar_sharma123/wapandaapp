//
//  BiddingUnderwayAPIRequest.swift
//  Wapanda
//
//  Created by Daffolapmac-33 on 14/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation


class BiddingUnderwayAPIRequest: ApiRequestProtocol {
    
    //MARK:- local properties
    var apiRequestUrl:String!
    
    //MARK:- Helper methods
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: BiddingUnderwayRequestModel which contains Request header and request body for the auctionDetail api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    
    func makeAPIRequest(withReqFormData reqFromData: AuctionDetailRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
    }
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: BiddingUnderwayRequestModel which contains Request header and request body for the auctionDetail api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeAPIRequestToGetAuctionDetail(withReqFormData reqFromData: AuctionDetailRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        self.apiRequestUrl = AppConstants.URL.BASE_URL+reqFromData.getEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        
         if UserDefaultUtility.retrieveBoolForKey(AppConstants.UserDefaultKeys.IS_DRIVER){
           
            ServiceManager.sharedInstance.requestGETWithURL(self.apiRequestUrl,requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: BidDetailResponseModel.self)
            
        }else{
            ServiceManager.sharedInstance.requestGETWithURL(self.apiRequestUrl,requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: AuctionDetailResponseModel.self)
            
        }
        
        
    }
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: AcceptAuctionRequestModel which contains Request header and request body for the accept auction api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeAPIRequestToAcceptAuction(withReqFormData reqFromData: AcceptAuctionRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        self.apiRequestUrl = AppConstants.URL.BASE_URL+reqFromData.getEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        //requestPUTWithURL
        //ServiceManager.sharedInstance.requestGETWithURL(self.apiRequestUrl,requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: CommonResponseModel.self)
        
        ServiceManager.sharedInstance.requestPUTWithURL(self.apiRequestUrl, andRequestDictionary: reqFromData.requestBody, requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: CommonResponseModel.self)
        
    }
    
    
    /**
     This method is used to make an api request to service manager
     
     - parameter reqFromData: CancelAuctionRequestModel which contains Request header and request body for the accept auction api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeAPIRequestToCancelAuction(withReqFormData reqFromData: CancelAuctionRequestModel, errorResolver: ErrorResolver, responseCallback: ResponseCallback) {
        
        self.apiRequestUrl = AppConstants.URL.BASE_URL+reqFromData.getEndPoint()
        
        let responseWrapper = ResponseWrapper(errorResolver: errorResolver, responseCallBack: responseCallback)
        
        ServiceManager.sharedInstance.requestPUTWithURL(self.apiRequestUrl, andRequestDictionary: reqFromData.requestBody, requestHeader: reqFromData.requestHeader, responseCallBack: responseWrapper, returningClass: CommonResponseModel.self)
    }
    
    
    /**
     This method is used to know that whether the api request is in progress or not
     
     - returns: Boolean value either true or false
     */
    func isInProgress() -> Bool {
        return ServiceManager.sharedInstance.isInProgress(self.apiRequestUrl)
    }
    
    /**
     This method is used to cancel the particular API request
     */
    func cancel() -> Void{
        ServiceManager.sharedInstance.cancelTaskWithURL(self.apiRequestUrl)
    }
}

