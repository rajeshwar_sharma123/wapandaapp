//
//  ForgotPasswordPresenter.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/9/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

class ForgetPasswordScreenPresenter: ResponseCallback{
    
    //MARK:- ForgetPasswordScreenPresenter local properties
    private weak var forgetPasswordViewDelegate             : ForgetPasswordViewDelegate?
    private var forgetPasswordBusinessForgetPassword                 : ForgetPasswordBusinessLogic?
    private var entryType : EntryType!
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?
    
    //MARK:- Constructor
    init(delegate responseDelegate:ForgetPasswordViewDelegate,textFieldValidationDelegate: TextFieldValidationDelegate) {
        self.forgetPasswordViewDelegate = responseDelegate
        self.textFieldValidationDelegate = textFieldValidationDelegate
        forgetPasswordBusinessForgetPassword = ForgetPasswordBusinessLogic()
    }
    
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.forgetPasswordViewDelegate?.hideLoader()
        self.forgetPasswordViewDelegate?.otpSentSuccessfully(to: self.entryType, withResponseModel: responseObject as! ForgetPasswordResponseModel)
    }
    
    func servicesManagerError(error : ErrorModel){
        self.forgetPasswordViewDelegate?.hideLoader()
        self.forgetPasswordViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send forgetPassword request to business layer with valid Request model
     - returns : Void
     */
    func sendForgetPasswordRequest(withData forgetPasswordViewRequestModel:ForgetPasswordScreenRequestModel) -> Void{
        
        guard self.validateInput(withData: forgetPasswordViewRequestModel) else{
            return
        }
        
        self.forgetPasswordViewDelegate?.showLoader()
        
        var forgetPasswordRequestModel : ForgetPasswordRequestModel!
        
        if forgetPasswordViewRequestModel.email.isValidEmailId() {
            self.entryType = .Email
            forgetPasswordRequestModel = ForgetPasswordRequestModel.Builder()
                .setUserName(forgetPasswordViewRequestModel.email)
                .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .build()
        }else{
             self.entryType = .Phone
            let userName = "\(forgetPasswordViewRequestModel.countryCodeExt!)\(forgetPasswordViewRequestModel.phoneNumber!)"
            forgetPasswordRequestModel = ForgetPasswordRequestModel.Builder()
                .setUserName(userName)
                .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
                .build()
        }
    
        self.forgetPasswordBusinessForgetPassword?.performForgetPassword(withForgetPasswordRequestModel: forgetPasswordRequestModel, presenterDelegate: self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for forgetPassword view request model(PreForgetPassword Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter forgetPasswordRequestModel: ForgetPasswordScreenRequestModel recieved from ForgetPassword view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData forgetPasswordRequestModel:ForgetPasswordScreenRequestModel) -> Bool {
        
        //All fields not empty
        guard self.validateAllFieldsEmpty(withData: forgetPasswordRequestModel) else{
            //All fields empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: "", forTextFields: .All)
            return false
        }
        
        //email validation
        guard !forgetPasswordRequestModel.email.isEmptyString() || !forgetPasswordRequestModel.phoneNumber.isEmptyString() else{
            //email or phone empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_EMAILID_OR_PHONE_NUMBER, forTextFields: .EmailOrPhone)
            return false
        }
        
        guard forgetPasswordRequestModel.email.isValidEmailId() || forgetPasswordRequestModel.phoneNumber.isPhoneNumberValid() else {
            //email or phone not valid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_EMAILID_OR_PHONE_NUMBER, forTextFields: .EmailOrPhone)
            return false
        }
        
        return true
    }
    
    /**
     Validate that all fields are empty or not
     - parameter forgetPasswordRequestModel : ForgetPasswordScreenRequestModel
     */
    func validateAllFieldsEmpty(withData forgetPasswordRequestModel:ForgetPasswordScreenRequestModel) -> Bool{
        guard !forgetPasswordRequestModel.email.isEmptyString() || !forgetPasswordRequestModel.phoneNumber.isEmptyString()
            else {
            return false
        }
        return true
    }
}

