import UIKit

extension SearchListViewController: UITableViewDelegate, UITableViewDataSource {

    func registerCells() {
        searchTableView.registerTableViewCell(tableViewCell: SearchCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.getCell(withCellType: SearchCell.self)
        cell.bindData(data: searchList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let filter = searchList[indexPath.row]
        
        var serachParam = BidRequestUserInput()
        
        if !filter.pickupDateTime.isEmpty {
            let date = filter.pickupDateTime.getDateFromZoneFormate()
            let filterDate = getCurrentDateWithFilteredTime(filterDate: date)
            serachParam.pickupTime = filterDate.timeIntervalSince1970
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
            dateFormatter.locale = Locale(identifier:"en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            serachParam.pickupTimeUTC = dateFormatter.string(from: filterDate)
        }
        
        if let latitude = filter.lat {
            serachParam.latitude = Double(latitude)
        }
        
        if let longitude = filter.long {
            serachParam.longitude = Double(longitude)
        }
        
        serachParam.addressTitle = filter.destAddress
        
        if let fare = filter.fareAbove {
            serachParam.fare = Float(fare)
        }

        delegate?.savedSearchesClicked(params: serachParam)
        navigationController?.popViewController(animated: true)
    }

    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            presenter?.deleteSearch(searchId: searchList[indexPath.row].id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
