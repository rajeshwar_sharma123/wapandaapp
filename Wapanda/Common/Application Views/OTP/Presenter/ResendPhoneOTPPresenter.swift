
//Notes:- This class is used as presenter for ResendPhoneOTPPresenter

import Foundation
import ObjectMapper

class ResendPhoneOTPPresenter: ResponseCallback{
    
//MARK:- OTPPopupViewPresenter local properties
    private weak var resendOTPViewDelegate          : ResendPhoneOTPViewDelegate?
    private var resendOTPBusinessLogic              : ResendPhoneOTPBusinessLogic!
    
//MARK:- Constructor
    init(delegate responseDelegate:ResendPhoneOTPViewDelegate){
        self.resendOTPViewDelegate = responseDelegate
        self.resendOTPBusinessLogic = ResendPhoneOTPBusinessLogic()
    }
    
//MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable>(responseObject : T){
        self.resendOTPViewDelegate?.hideLoader()
        self.resendOTPViewDelegate?.resendPhoneOTPSuccessfully()
    }
    
    func servicesManagerError(error : ErrorModel){
        self.resendOTPViewDelegate?.hideLoader()
        self.resendOTPViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
    }
    
//MARK:- API calls
    
    func resendPhoneOTP(withPhone phone:String, countryCode: String, diallingCode: String){
        
        self.resendOTPViewDelegate?.showLoader()

        let otpPopupRequestModel:ResendPhoneOTPRequestModel = ResendPhoneOTPRequestModel.Builder()
            .setPhone(phone)
            .setCountryCode(countryCode)
            .setDiallingCode(diallingCode)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        
        self.resendOTPBusinessLogic.performResendOTP(withResendPhoneOTPRequestModel: otpPopupRequestModel, presenterDelegate: self)
    }
}
