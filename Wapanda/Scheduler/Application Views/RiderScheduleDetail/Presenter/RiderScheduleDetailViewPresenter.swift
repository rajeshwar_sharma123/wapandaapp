//
//  RiderScheduleDetailViewPresenter.swift
//  Wapanda
//
//  Created by Daffodil on 28/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper
class RiderScheduleDetailViewPresenter: ResponseCallback{
    
    //MARK:- RiderScheduleDetailViewPresenter local properties
    
    private weak var riderScheduleDetailViewDelegate: RiderScheduleDetailViewDelegate?
    private lazy var riderScheduleDetailBussinessLogic: RiderScheduleDetailBussinessLogic = RiderScheduleDetailBussinessLogic()
    //MARK:- Constructor
    
    init(delegate responseDelegate:RiderScheduleDetailViewDelegate){
        self.riderScheduleDetailViewDelegate = responseDelegate
    }
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Mappable> (responseObject : T){
        self.riderScheduleDetailViewDelegate?.hideLoader()
       
        if responseObject is CancelResponseModel{
            self.riderScheduleDetailViewDelegate?.updateCancelRide(withResponseModel: responseObject as! CancelResponseModel)
        }
        
    }
    
    func servicesManagerError(error : ErrorModel){
        self.riderScheduleDetailViewDelegate?.hideLoader()
        self.riderScheduleDetailViewDelegate?.showErrorAlert(error.getErrorTitle(), alertMessage: error.getErrorMessage())
        
    }
    //MARK:- Methods to call server
    
    
    /**
     This method is used to send reject auction request to bussiness layer
     - returns : Void
     */
    func sendCancelRideRequest(withRideId riderScheduleDetailModel: RiderScheduleDetailModel) -> Void{
       
        self.riderScheduleDetailViewDelegate?.showLoader()
     
        let cancelRiderScheduleRideRequestModel = CancelRiderScheduleRideRequestModel.Builder()
            .setTripId(riderScheduleDetailModel.tripId)
            .setCancelledBy(riderScheduleDetailModel.cancelledBy)
            .addRequestHeader(key:AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.API_KEY, value: AppConstants.APIRequestHeaders.API_KEY_VALUE)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.AUTH_TOKEN, value: UserDefaultUtility.retrieveStringWithKey(AppConstants.APIRequestHeaders.AUTH_TOKEN))
            .build()
        self.riderScheduleDetailBussinessLogic.performCancelRide(withCancelRiderScheduleRideRequestModel: cancelRiderScheduleRideRequestModel, presenterDelegate: self)
    }
    
    
}
