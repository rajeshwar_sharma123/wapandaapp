//
//  AddCreditCardBusinessLogic.swift
//  Wapanda
//
//  Created by Daffomac-23 on 7/19/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
class AddCreditCardBusinessLogic {
    
    init(){
        print("AddCreditCardBusinessLogic init \(self)")
    }
    deinit {
        print("AddCreditCardBusinessLogic deinit \(self)")
    }
    
    /**
     This method is used for perform AddCreditCard With Valid Inputs constructed into a AddCreditCardRequestModel
     
     - parameter inputData: Contains info for Authentication Received from Stripe
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performCustomerIdFetchRequest(withAddCreditCardRequestModel bankInfoRequestModel: AddCreditCardRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForAddCreditCard()
        AddCreditCardAPIRequest().makeAPIRequest(withReqFormData: bankInfoRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    /**
     This method is used for perform AddCreditCard With Valid Inputs constructed into a AddCreditCardRequestModel
     
     - parameter inputData: Contains info for Code Received From Stripe Connect
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    
    func performStripeAuthentication(withAddCreditCardRequestModel bankInfoRequestModel: AddCreditCardRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForAddCreditCard()
        AddCreditCardAPIRequest().makeAPIRequestForStripeAuthentication(withReqFormData: bankInfoRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    
    private func registerErrorForAddCreditCard() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode (ErrorCodes.ACCOUNT_DISABLED, message  : AppConstants.ErrorMessages.INVALID_KEY_MESSAGE)
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        
        return errorResolver
    }
}
