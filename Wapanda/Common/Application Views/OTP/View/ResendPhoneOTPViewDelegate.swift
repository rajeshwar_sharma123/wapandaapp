
//Notes:-

protocol ResendPhoneOTPViewDelegate:BaseViewProtocol {
    func resendPhoneOTPSuccessfully() -> Void
    func showToastWithMessage(_ message:String) -> Void
}
