

//Notes:- This class is used for constructing UpdateUser Service Request Model

class UpdateUserRequestModel {
    
    //MARK:- UpdateUserRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    var userId: String!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
        self.userId = builder.userId
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        var userId: String!
        
        /**
         This method is used for setting profileImageFileId
         
         - parameter userName: String parameter that is going to be profileImageFileId
         
         - returns: returning Builder Object
         */
        func setAgreeTermsAndConditions(_ value:Bool) -> Builder{
            requestBody["agreeTermsAndConditions"] = value as AnyObject?
            return self
        }
        
        /**
         This method is used for setting emergency contact
         
         - parameter contact: String parameter that is going to be emergency contact
         
         - returns: returning Builder Object
         */
        func setEmergencyContact(_ value:String) -> Builder{
            requestBody["emergencyContact"] = value as AnyObject?
            return self
        }
        
        /**
         This method is used for setting Emergency Contact Name
         
         - parameter contact: String parameter that is going to be emergencyContactName
         
         - returns: returning Builder Object
         */
        func setEmergencyContactName(_ value:String) -> Builder{
            requestBody["emergencyContactName"] = value as AnyObject?
            return self
        }
        
        /**
         This method is used for setting emergency contact image id
         
         - parameter contact: String parameter that is going to be emergency contact image id
         
         - returns: returning Builder Object
         */
        func setEmergencyContactImageId(_ value:String?) -> Builder{
            if let id = value{
                requestBody["emergencyContactImageId"] = id as AnyObject?
            }
            return self
        }
        
        /**
         This method is used for setting userID
         
         - parameter userID: String parameter that is going to be userID
         
         - returns: returning Builder Object
         */
        func setUserId(_ userID:String) -> Builder{
            self.userId = userID
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of UpdateUserRequestModel
         and provide UpdateUserForm1ViewViewRequestModel object.
         
         -returns : UpdateUserRequestModel
         */
        func build()->UpdateUserRequestModel{
            return UpdateUserRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting UpdateUser end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return String(format: AppConstants.ApiEndPoints.UPDATE_USER, self.userId)
    }
    
}
