//
//  CancelScreenRequestModel
//
//  Created by  on 7/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CancelScreenRequestModel {
    
    var success   = true
    var message   = "Ride accepted."
}
