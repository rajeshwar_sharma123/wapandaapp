//
//  ResetPasswordViewController.swift
//  Wapanda
//
//  Created by Daffodil on 21/11/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit

class ResetPasswordViewController: BaseViewController {
    
    //MARK:- Properties/IBOutlets
    
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var btnShowHideCurrentPassword: UIButton!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var btnShowHideNewPassword: UIButton!
    @IBOutlet weak var btnSavePassword: CustomButton!
    var resetPasswordPresenter : ResetPasswordViewPresenter!

    @IBOutlet weak var _lblErrorMSgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblErrorMsgCurrentPassword: UILabel!
    @IBOutlet weak var lblErrorMsgNewPassword: UILabel!
    
    //MARK:- ViewLifeCycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.intialViewSetup()
        self.resetPasswordPresenter = ResetPasswordViewPresenter(delegate: self,textFieldValidationDelegate: self)

    }
    
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setup
     */
    private func intialViewSetup(){
        
        self.txtNewPassword.delegate = self
        self.txtCurrentPassword.delegate = self

        self.setupNavigationBar()
        self.customizeTextFields()
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitleImage(image: #imageLiteral(resourceName: "inverted_panda"))
        self.customizeNavigationBackButton()
    }
    
    /**
     This function customizes the text field properties
     - returns :void
     */
    private func customizeTextFields(){
        
        _lblErrorMSgHeightConstraint.constant = 0
        lblErrorMsgNewPassword.isHidden = true
        
        let font = UIFont.getSanFranciscoLight(withSize: 20)
        let attributes = [
            NSForegroundColorAttributeName: UIColor.appPlaceholderColor(),
            NSFontAttributeName : font
        ]
        self.txtNewPassword.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.ResetPasswordScreen.NEW_PASSWORD_PLACEHOLDER_TEXT, attributes:attributes)
        
        self.txtCurrentPassword.attributedPlaceholder = NSAttributedString(string: AppConstants.ScreenSpecificConstant.ResetPasswordScreen.CURRENT_PASSWORD_PLACEHOLDER_TEXT, attributes:attributes)

    }

    
    @IBAction func showHideCurrentPassword(_ sender: UIButton) {
        self.txtCurrentPassword.isSecureTextEntry = !self.txtCurrentPassword.isSecureTextEntry
        self.btnShowHideCurrentPassword.setImage(self.txtCurrentPassword.isSecureTextEntry ? #imageLiteral(resourceName: "ic_visibility_off_white"): #imageLiteral(resourceName: "ic_visibility_white"), for: .normal)
    }
    
    @IBAction func showHideNewPassword(_ sender: UIButton) {
        
        self.txtNewPassword.isSecureTextEntry = !self.txtNewPassword.isSecureTextEntry
       self.btnShowHideNewPassword.setImage(self.txtNewPassword.isSecureTextEntry ? #imageLiteral(resourceName: "ic_visibility_off_white"): #imageLiteral(resourceName: "ic_visibility_white"), for: .normal)
    }

    @IBAction func savePasswordBtnAction(_ sender: UIButton) {
        
        self.resetPasswordPresenter.sendChangePasswordRequest(withData: ResetPasswordViewModel(newPassword: self.txtNewPassword.text ?? "", currentPassword: self.txtCurrentPassword.text ?? ""))

    }
    
    // MARK: - Error Methods
    
    /**
     This method is used to show error on all the text fields with default messages.
     */
    func showAllTextFieldError(){
        self.lblErrorMsgNewPassword.text = AppConstants.ValidationErrors.NEW_PASSWORD
        self.lblErrorMsgCurrentPassword.text =  AppConstants.ValidationErrors.CURRENT_PASSWORD
        _lblErrorMSgHeightConstraint.constant = 16

        self.lblErrorMsgNewPassword.isHidden = false
    }
}





// MARK : UITextField Delegate Methods
extension ResetPasswordViewController : UITextFieldDelegate,TextFieldValidationDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            _lblErrorMSgHeightConstraint.constant = 0
            lblErrorMsgNewPassword.isHidden = true
        
        return true
    }
    
    func showErrorMessage(withMessage message: String, forTextFields: TextFieldsType) {
        
        switch forTextFields {
            
            case .CurrentPassword:
                                _lblErrorMSgHeightConstraint.constant = 16
                                self.lblErrorMsgCurrentPassword.text = message
                                lblErrorMsgNewPassword.isHidden = true
            
            case .NewPassword:
                                self.lblErrorMsgNewPassword.isHidden = false
                                self.lblErrorMsgNewPassword.text = message
                                _lblErrorMSgHeightConstraint.constant = 0
            
            case .All:
                            showAllTextFieldError()
        default:
            break
        }
       
    }
}


// MARK: - NewPasswordViewDelegate Methods
extension ResetPasswordViewController : ResetPasswordViewDelegate
{
    
    func passwordChangedSuccessfully(withData response: ResetPasswordResponseModel) {
                
        AppUtility.presentToastWithMessage(AppConstants.ScreenSpecificConstant.ResetPasswordScreen.PASSWORD_CHANGE_SUCCESS)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            AppInitialViewHandler.sharedInstance.setupInitialViewController()
        }
    }
    
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
}

