
//Note :- This class contains signUp Buisness Logic

class SignUpBusinessLogic {
    
    
    deinit {
        print("SignUpBusinessLogic deinit")
    }

    /**
     This method is used for perform sign Up With Valid Inputs constructed into a SignUpRequestModel
     
     - parameter inputData: Contains info for SignUp
     - parameter success:   Returning Success Response of API
     - parameter failure:   NSError Response Contaons ErrorInfo
     */
    func performSignUp(withSignUpRequestModel signUpRequestModel: SignUpRequestModel, presenterDelegate:ResponseCallback) ->Void {
        
        //Adding predefined set of errors
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //self.registerErrorForSignup()
        SignUpApiRequest().makeAPIRequest(withReqFormData: signUpRequestModel, errorResolver: errorResolver, responseCallback: presenterDelegate)
    }
    
    
    /**
     This method is used for adding set of Predefined Error coming from server
     */
    private func registerErrorForSignup() ->ErrorResolver{
        
        let errorResolver:ErrorResolver = ErrorResolver.registerErrorsForApiRequests() //ErrorResolver()
        
        errorResolver.registerErrorCode(ErrorCodes.INVALID_INPUT, message: AppConstants.ErrorMessages.INVALID_INPUT_MESSAGE)
        errorResolver.registerErrorCode(ErrorCodes.EMAIL_ALREADY_REGISTERED, message: AppConstants.ErrorMessages.EMAIL_ALREADY_REGISTERED)
        errorResolver.registerErrorCode(ErrorCodes.PHONE_ALREADY_REGISTERED, message: AppConstants.ErrorMessages.PHONE_ALREADY_REGISTERED)
        errorResolver.registerErrorCode(ErrorCodes.ALREADY_REGISTERED, message: AppConstants.ErrorMessages.ALREADY_REGISTERED)
        errorResolver.registerErrorCode(ErrorCodes.INVALID_OTP, message: AppConstants.ErrorMessages.INVALID_OTP)
        return errorResolver
    }}
