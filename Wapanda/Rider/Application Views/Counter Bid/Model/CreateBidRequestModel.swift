

//Notes:- This class is used for constructing CreateBid Service Request Model

class CreateBidRequestModel {
    
    //MARK:- CreateBidRequestModel properties
    
    //Note :- Property Name must be same as key used in request API
    var requestBody: [String:AnyObject]!
    var requestHeader: [String:AnyObject]!
    
    init(builderObject builder:Builder){
        //Instantiating service Request model Properties with Builder Object property
        self.requestBody = builder.requestBody
        self.requestHeader = builder.requestHeader
    }
    
    // This inner class is used for setting upper class properties
    internal class Builder{
        //MARK:- Builder class Properties
        //Note :- Property Name must be same as key used in request API
        var requestBody: [String:AnyObject] = [String:AnyObject]()
        var requestHeader: [String:AnyObject] = [String:AnyObject]()
        
        /**
         This method is used for setting driver id
         
         - parameter id: String parameter that is going to be set on driver id
         
         - returns: returning Builder Object
         */
        func setDriverId(_ id:String) -> Builder{
            requestBody["driverId"] = id as AnyObject?
            return self
        }
        
        /**
         This method is used for setting rider id
         
         - parameter id: String parameter that is going to be set on rider id
         
         - returns: returning Builder Object
         */
        func setRiderId(_ id:String) -> Builder{
            requestBody["riderId"] = id as AnyObject?
            return self
        }
        /**
         This method is used for setting driver fromAddress
         
         - parameter id: String parameter that is going to be set on fromAddress
         
         - returns: returning Builder Object
         */
        func setFromAddress(_ fromAddress:String) -> Builder{
            requestBody["fromAddress"] = fromAddress as AnyObject?
            return self
        }  /**
         This method is used for setting toAddress
         
         - parameter id: String parameter that is going to be set on driver toAddress
         
         - returns: returning Builder Object
         */
        func setToAddress(_ toAddress:String) -> Builder{
            requestBody["toAddress"] = toAddress as AnyObject?
            return self
        }
        /**
         This method is used for setting Estimated distance
         
         - parameter distance: String parameter that is going to be set on distance
         
         - returns: returning Builder Object
         */
        func setEstDistance(_ distance:UIntMax) -> Builder{
            requestBody["estDistance"] = distance as AnyObject?
            return self
        }
        
        /**
         This method is used for setting estimated duration
         
         - parameter duration: String parameter that is going to be set on duration
         
         - returns: returning Builder Object
         */
        func setEstDuration(_ duration:UIntMax) -> Builder{
            requestBody["estDuration"] = duration as AnyObject?
            return self
        }
        
        /**
         This method is used for setting source coordinates
         
         - parameter lat: String parameter that is going to be set on lat
         - parameter lng: String parameter that is going to be set on lng
         
         - returns: returning Builder Object
         */
        func setSourceCoordinate(lat:Double, lng: Double) -> Builder{
            requestBody["latFrom"] = lat as AnyObject?
            requestBody["lngFrom"] = lng as AnyObject?
            return self
        }
        
        /**
         This method is used for setting destination coordinates
         
         - parameter lat: String parameter that is going to be set on lat
         - parameter lng: String parameter that is going to be set on lng
         
         - returns: returning Builder Object
         */
        func setDestinationCoordinate(lat:Double, lng: Double) -> Builder{
            requestBody["latTo"] = lat as AnyObject?
            requestBody["lngTo"] = lng as AnyObject?
            return self
        }
        
        /**
         This method is used for setting start time string in UTC format
         
         - parameter time: String parameter that is going to be set start time
         
         - returns: returning Builder Object
         */
        func setStartTime(_ time:String) -> Builder{
            requestBody["startTime"] = time as AnyObject?
            return self
        }
        
        /**
         This method is used for setting rider counter price
         
         - parameter price: String parameter that is going to be set on counter price
         
         - returns: returning Builder Object
         */
        func setRiderCounterPrice(_ price:Double?) -> Builder{
            if let price = price{
                requestBody["riderCounterPrice"] = price as AnyObject?
            }
            return self
        }
        
        /**
         This method is used for adding request Header
         
         - parameter key:   Key of a Header
         - parameter value: Value corresponding to header
         
         - returns: returning Builder object
         */
        func addRequestHeader(key:String , value:String) -> Builder {
            self.requestHeader[key] = value as AnyObject?
            return self
        }
        
        /**
         This method is used to set properties in upper class of CreateBidRequestModel
         and provide CreateBidForm1ViewViewRequestModel object.
         
         -returns : CreateBidRequestModel
         */
        func build()->CreateBidRequestModel{
            return CreateBidRequestModel(builderObject: self)
        }
    }
    
    /**
     This method is used for getting CreateBid end point
     
     -returns: String containg end point
     */
    func getEndPoint()->String{
        return AppConstants.ApiEndPoints.CREATE_BID
    }
    
}
