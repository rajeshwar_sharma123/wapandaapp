import UIKit

protocol PickupTimeViewControllerDelegate {
    func updatePickupTime(_ date: String)
}
class PickupTimeViewController: UIViewController {

    @IBOutlet weak var dateBracketView: UIView!
    @IBOutlet weak var timeBracketView: UIView!
    @IBOutlet weak var datePickerView: UIPickerView!
    @IBOutlet weak var timePickerView: UIPickerView!
    
    //@IBOutlet weak var setTimeButton: UIButton!
    var delegate: PickupTimeViewControllerDelegate?
    var datePickerData: [Any] = [Any]()
    var timePickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        intialSetupForView()
    }

    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        self.setupNavigationBar()
        setPickerProperty()
        setDateAndTimePickerData()
       
        
    }
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, color: UIColor.appDarkGrayColor(),isTranslucent: false)
        self.customizeNavigationBackButton()
    }

    /**
     This method is used to set default prperty of Date and Time Picker
     */
    
    func setPickerProperty(){
        datePickerView.delegate = self
        datePickerView.dataSource = self
        datePickerView.backgroundColor = UIColor.clear
        timePickerView.delegate = self
        timePickerView.dataSource = self
        timePickerView.backgroundColor = UIColor.clear
        dateBracketView.layer.cornerRadius = 6
        dateBracketView.layer.borderColor =  UIColor.filterPickerBorderColor().cgColor
        dateBracketView.layer.borderWidth = 1
        timeBracketView.layer.cornerRadius = 6
        timeBracketView.layer.borderColor = UIColor.filterPickerBorderColor().cgColor
        timeBracketView.layer.borderWidth = 1
    }
    
    /**
     This method is used to set data for Date and Time Picker
     */
    func setDateAndTimePickerData(){
        let today = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE MMM dd"
        datePickerData.append(getDateDetail(today, dateStr: "Today"))
        let maxDate = 13
        for i in 0..<maxDate {
            let date = Calendar.current.date(byAdding: .day, value: i + 1, to: today)
            let dateStr = dateFormatter.string(from: date!)
            //datePickerData.append(dateStr)
            datePickerData.append(getDateDetail(date!, dateStr: dateStr))
        }
        setTimePickerData(dateObj: Date())
    }
    
    func setTimePickerData(dateObj: Date){
        timePickerData.removeAll()
        let today = Date()
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.dateFormat = "dd MM yyyy"
        let futureDateStr = dateFormatter.string(from: dateObj)
        let futureDate = dateFormatter.date(from: futureDateStr)
        let todayDateStr = dateFormatter.string(from: today)
        let todayDate = dateFormatter.date(from: todayDateStr)
        let date: Date!
        if futureDate == todayDate{
            let minutes = calendar.component(.minute, from: today)
            var minuteDiff = 0
            if minutes%10 != 0{
                minuteDiff = 10 - minutes%10
            }
            let extraMinutes = 0 + minuteDiff
            date = Calendar.current.date(byAdding: .minute, value: extraMinutes, to: today)
        }else{
            date = futureDate
        }
        
        var hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        if hour == 0 {
            hour = 24 * 60
        }else{
            hour = 24*60 - hour*60 - minutes
        }
        let minuteInterval:Int = 10
        var minuteIncrease: Int = 0
        let loopIterate: Float =  Float(hour)/Float(minuteInterval)
        let count = Int(loopIterate.rounded())
        
        for _ in 0..<count {
            let dateMinute = Calendar.current.date(byAdding: .minute, value: minuteIncrease, to: date!)
            dateFormatter.locale = Locale(identifier:"en_US_POSIX")
            dateFormatter.dateFormat = "h:mm a"
            let timeStrTmp = dateFormatter.string(from: dateMinute!)
            timePickerData.append(timeStrTmp)
            minuteIncrease = minuteIncrease + minuteInterval
        }
        timePickerView.reloadAllComponents()
    }
    
    
    /**
     This method is used to create dictionary for date detail
     */
    func getDateDetail(_ date:Any,dateStr: String) -> [String:Any]{
        var dateDic: [String:Any] = [String:Any]()
        dateDic["date"] = date
        dateDic["dateStr"] = dateStr
        return dateDic
    }
    
    /**
     This method is used to get the selected Date & Time
     */
    func getPickupDateTime()-> String{
        let dateDic = datePickerData[datePickerView.selectedRow(inComponent: 0)] as! [String:Any]
        let date = dateDic["date"] as! Date
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier:"en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let selectedDate = dateFormatter.string(from: date)
        let selectedTime = timePickerData[timePickerView.selectedRow(inComponent: 0)]
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        let selectedDateTime = "\(selectedDate) \(selectedTime)"
        let dateTmp = dateFormatter.date(from: selectedDateTime)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(identifier:"UTC")
        let selectedDateTimeString = dateFormatter.string(from: dateTmp!)
        return selectedDateTimeString
    }
    
    @IBAction func setTimeButtonAction(_ sender: Any) {
        delegate?.updatePickupTime(getPickupDateTime())
        navigationController?.popViewController(animated: true)
    }
    
}


extension PickupTimeViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    // The number of columns of data
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        pickerView.subviews.forEach({
            $0.isHidden = $0.frame.height < 1.0
        })
        return 1
    }
    
    // The number of rows of data
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if pickerView == datePickerView{
            return datePickerData.count
        }else{
            return timePickerData.count
        }
    }
    
    // The data to return for the row and component (column) that's being passed in
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var title = ""
        if pickerView == datePickerView{
            let dateDic = datePickerData[row] as! [String:Any]
            title = dateDic["dateStr"] as! String
        }else{
            title = timePickerData[row]
        }
        let attributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName:  UIFont.getTitilliumSemibold(withSize: 18)]
        let mutableString = NSMutableAttributedString(string: title, attributes: attributes)
        return mutableString
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView == datePickerView{
            let dateDic = datePickerData[row] as! [String:Any]
            let date = dateDic["date"] as! Date
            setTimePickerData(dateObj: date)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
       }
}


