
//Note :- This Class is used for GooglePlaceDetail Service means it is used for handling GooglePlaceDetail Api

import GooglePlaces

class GooglePlaceDetailServiceManager {
    
    //MARK:- Helper methods
    
    /**
     This method is used make an api request to service manager
     
     - parameter reqFromData: GooglePlaceDetailRequestModel which contains Request header and request body for the signup api call
     - parameter errorResolver: ErrorResolver contains all error handling with posiible error codes
     - parameter responseCallback: ResponseCallback used to throw callback on recieving response
     */
    func makeRequest(withReqFormData reqFromData: GooglePlaceDetailRequestModel, responseCallback: ResponseCallbackGeneral) {
        
        reqFromData.placeClient.lookUpPlaceID(reqFromData.placeId) { (place: GMSPlace?, error: Error?) in
            if let _ = error{
                let errorModel = ErrorModel()
                errorModel.setErrorTitle((error?.localizedDescription)!)
                responseCallback.servicesManagerError(error: errorModel)
            }
            else if let _ = place{
                responseCallback.servicesManagerSuccessResponse(responseObject: place as AnyObject)
            }
        }
    }
}

