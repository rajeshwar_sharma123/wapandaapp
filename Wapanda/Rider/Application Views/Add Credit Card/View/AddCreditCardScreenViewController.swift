//
//  AddCreditCardScreenViewController.swift
//  Wapanda
//
//  Created by Daffomac-23 on 8/10/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import UIKit
import OAuthSwift
import Stripe

enum CardEntryType:Int
{
    case CARD_NUMBER = 1
    case MONTH = 2
    case YEAR = 3
    case CVV = 4
}
class AddCreditCardScreenViewController: BaseViewController {
    
    //MARK : Local Variables
    var presenterAddCreditCard : AddCreditCardPresenter!
    var customer:STPCustomer!
    var customerContext:STPCustomerContext!
    fileprivate var dropDownPicker : UIPickerView = UIPickerView()
    fileprivate var selectedIndex : Int = 0
    fileprivate var numberOfRows = 0
    fileprivate var cardFormat = "nnnn-nnnn-nnnn-nnnn"
    fileprivate var cvvFormat = "nnn"
    fileprivate var months = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    fileprivate var textFieldformatting : TextFieldFormatting!
    @IBOutlet weak var imageViewBrand: UIImageView!
    @IBOutlet weak var textFieldCardNumber: UITextField!
    @IBOutlet weak var textFieldCvv: UITextField!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var txtFieldMonth: UITextField!
    @IBOutlet weak var txtFieldYear: UITextField!
    var maxCVVLength: Int = 0
    
    @IBOutlet weak var labelCardValidation: UILabel!
    @IBOutlet weak var labelMonthValidation: UILabel!
    @IBOutlet weak var labelYearValidation: UILabel!
    @IBOutlet weak var labelCVVValidation: UILabel!
    var isComingFromSideMenu = false
    
    //MARK : Tax Information Screen Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setupCustomer()
        self.intialSetupForView()
        self.setUpPickerView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
    private func intialSetupForView(){
        
        self.presenterAddCreditCard = AddCreditCardPresenter(delegate: self, textFieldValidationDelegate: self)
        self.setExpiryMonth()
        self.setExpiryYear()
        self.txtFieldMonth.inputView = self.dropDownPicker
        self.txtFieldYear.inputView = self.dropDownPicker
        
        self.textFieldCvv.delegate = self
        self.textFieldCardNumber.delegate = self
        
        self.labelCVVValidation.text = ""
        self.labelCardValidation.text = ""
        self.labelYearValidation.text = ""
        self.labelMonthValidation.text = ""
    }
    
    private func setUpPickerView(){
        self.dropDownPicker.delegate = self
        self.dropDownPicker.dataSource = self
        self.dropDownPicker.backgroundColor = UIColor.white
    }
    
    func setupCustomer(){
        self.showLoader()
        self.customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
        self.customerContext.retrieveCustomer { (customer, error) in
            self.hideLoader()
            if let _ = customer
            {
                StripePaymentManager.shared.customer = customer
            }
            else{
                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: (error?.localizedDescription)!)
            }
        }
    }
    
    /**
     This method is used to setup initial Bank Info View
     */
    private func setupAddCreditCardView(){
        
        
    }
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
        self.customizeNavigationBarWithTitle(navigationTitle: AppConstants.ScreenSpecificConstant.AddCreditCardScreen.NAVIGATION_TITLE)
        self.customizeNavigationBackButton()
    }
    //MARK : IBOutlest Action Methods
    @IBAction func yearDropDownButtonTapped(_ sender: Any) {
        self.txtFieldYear.becomeFirstResponder()
    }
    @IBAction func monthDropDownButtonTapped(_ sender: Any) {
         self.txtFieldMonth.becomeFirstResponder()
    }
    @IBAction func addNewCardButtonTapped(_ sender: Any) {
        var newCardRequest = AddCreditCardScreenRequestModel()
        newCardRequest.brand = STPCardValidator.brand(forNumber:self.textFieldCardNumber.text!)
        newCardRequest.cardNumber = self.textFieldCardNumber.text!.replacingOccurrences(of: "-", with: "", options: .literal, range: nil) 
        newCardRequest.month = self.lblMonth.text!
        newCardRequest.year = self.lblYear.text!
        newCardRequest.cvv = self.textFieldCvv.text!
        
        if self.presenterAddCreditCard.validateAddCardRequest(withData: newCardRequest)
        {
            self.showLoader()
           self.addCardToCustomerAccount(newCardRequest)
        }
    }
    @IBAction func textFieldValueChanged(_ sender: Any) {
        let tag = (sender as! UITextField).tag
        
        switch tag {
        case CardEntryType.CARD_NUMBER.rawValue:
            self.labelCardValidation.text = ""
            let brand = STPCardValidator.brand(forNumber: (sender as! UITextField).text!)
            self.setUpView(forBrand:brand)
            break
        case CardEntryType.CVV.rawValue:
            self.labelCVVValidation.text = ""
            break
            
        default: break
            
        }
    }
    
    @IBAction func textFieldEditingDidBegin(_ sender: Any) {
        let tag = (sender as! UITextField).tag
        self.selectedIndex = tag
        switch tag {
        case CardEntryType.MONTH.rawValue:
            self.numberOfRows = 12
            self.setExpiryMonth()
            break
            
        case CardEntryType.YEAR.rawValue:
            self.numberOfRows = 25
            self.setExpiryYear()
            break
            
        default: break
            
        }
        self.dropDownPicker.reloadAllComponents()
    }
    
    fileprivate func setExpiryMonth()
    {
        self.labelMonthValidation.text = ""
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy"
        let currentYear = formatter.string(from: date)
        
        formatter.dateFormat = "MM"
        let currentMonth = Int(formatter.string(from: date))
       
        if self.lblMonth.text == ""
        {
            if (currentMonth!+1) < 10
            {
                self.lblMonth.text = String(format:"0%d",currentMonth!)
            }
            else
            {
                self.lblMonth.text = String(format:"%d",currentMonth!)
            }
        }
        else
        {
            if self.numberOfRows == 12
            {
            self.dropDownPicker.selectRow(self.months.index(of: self.lblMonth.text!)!, inComponent: 0, animated: true)
            }
        }
        if self.lblYear.text == currentYear
        {
            if (currentMonth!+1) < 10
            {
                self.lblMonth.text = String(format:"0%d",currentMonth!)
            }
            else
            {
                self.lblMonth.text = String(format:"%d",currentMonth!)
            }
        }
        
    }
    private func setExpiryYear()
    {
        self.labelYearValidation.text = ""
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let currentYear = Int(formatter.string(from: date))
        if self.lblYear.text == "SELECT YEAR"
        {
            self.lblYear.text = formatter.string(from: date)
            self.dropDownPicker.selectRow(0, inComponent: 0, animated: true)
        }
        else
        {
            let row = Int(self.lblYear.text!)! - currentYear!
            self.dropDownPicker.selectRow(row, inComponent: 0, animated: true)
        }
        
        self.setExpiryMonth()

    }
    fileprivate func setUpView(forBrand brand:STPCardBrand)
    {
        self.setCVVFormatting(basedOnBrandType: brand)
        self.cardFormat = self.getCardNumberFormatting(basedOnBrandType: brand)
        self.imageViewBrand.image = StripePaymentManager.shared.getBrandImage(basedOnBrandType: brand)
    }
    private func addCardToCustomerAccount(_ newCardRequest : AddCreditCardScreenRequestModel)
    {
        let cardParams = STPCardParams()
        cardParams.number = newCardRequest.cardNumber
        cardParams.expMonth = NumberFormatter().number(from: newCardRequest.month) as! UInt
        cardParams.expYear = NumberFormatter().number(from: newCardRequest.year) as! UInt
        cardParams.cvc = newCardRequest.cvv
                let sourceParams = STPSourceParams.cardParams(
                    withCard: cardParams)
                sourceParams.redirect = ["return_url":"wapandaapp://stripe-redirect"]
        
        
                STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
                    if let errorObj = error {
                        self.hideLoader()
                        self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: errorObj.localizedDescription)
                        return
                    }
                    self.customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
                    self.customerContext.attachSource(toCustomer: source!, completion: { (error) in
                        self.hideLoader()
                        if error != nil {
                            self.hideLoader()
                            self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ScreenSpecificConstant.AddCreditCardScreen.UNABLE_TO_ADD_CARD_MESSAGE)
                            return
                        }
                        if (self.navigationController?.viewControllers.count) == 3 && (self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-3]) is AddPaymentScreenViewController
                        {
                            self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-3])!, animated: true)
                        }
                        else
                        {
                            let paymentVC = UIViewController.getViewController(AddPaymentScreenViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
                            paymentVC.isComingFromSideMenu = self.isComingFromSideMenu
                           // paymentVC.isComingFromSideMenu =
                            // paymentVC.delegateAddPayment = self
                            UIApplication.shared.visibleViewController?.navigationController?.pushViewController(paymentVC, animated: true)
                        }
                    })

                }
        
//        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
//            if let errorObj = error {
//                self.hideLoader()
//                self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: errorObj.localizedDescription)
//                return
//            }
//            self.customerContext = STPCustomerContext(keyProvider: StripeAPIClient.sharedClient)
//            self.customerContext.attachSource(toCustomer: token!, completion: { (error) in
//                self.hideLoader()
//                if error != nil {
//                    self.hideLoader()
//                    self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: AppConstants.ScreenSpecificConstant.AddCreditCardScreen.UNABLE_TO_ADD_CARD_MESSAGE)
//                    return
//                }
//                if (self.navigationController?.viewControllers.count) == 3 && (self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-3]) is AddPaymentScreenViewController
//                {
//                self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-3])!, animated: true)
//                }
//                else
//                {
//                    let paymentVC = UIViewController.getViewController(AddPaymentScreenViewController.self, storyboard: UIStoryboard.Storyboard.Rider.object)
//                   // paymentVC.delegateAddPayment = self
//                    UIApplication.shared.visibleViewController?.navigationController?.pushViewController(paymentVC, animated: true)
//                }
//            })
//        }
        
    }
    
    private func setCVVFormatting(basedOnBrandType brand:STPCardBrand)
    {
        let maxLenghtOfCVV = STPCardValidator.maxCVCLength(for: brand)
        let numberOfDigits = [String](repeating: "n", count: Int(maxLenghtOfCVV))
        self.cvvFormat = String(numberOfDigits.flatMap { $0.characters.first })
        self.textFieldCvv.placeholder = String([String](repeating: "X", count: Int(maxLenghtOfCVV)).flatMap { $0.characters.first })
    }
    private func getCardNumberFormatting(basedOnBrandType brand:STPCardBrand)->String
    {
        var format = AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING
        switch brand {
        case .amex:
            format = "nnnn-nnnnnn-nnnnn"
            break
            
        case .dinersClub:
            format = "nnnn-nnnn-nnnn-nn"
            break
            
        case .visa: fallthrough
        case .discover: fallthrough
        case .JCB: fallthrough
        case .masterCard: fallthrough
        case .unknown:
            format = "nnnn-nnnn-nnnn-nnnn"
            break
        }
       return format
    }
    // MARK: - Navigation
    
    func navigateToTermsReviewController(){
        
    }
    
}
extension AddCreditCardScreenViewController:AddCreditCardScreenViewDelgate
{
    func showLoader()
    {
        super.showLoader(self)
    }
    func hideLoader()
    {
        super.hideLoader(self)
    }
    func showErrorAlert(_ alertTitle : String , alertMessage : String)
    {
        super.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    
}
// MARK:- UIPicker Delegate Methods
extension AddCreditCardScreenViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.numberOfRows
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let date = Date()
        let formatter = DateFormatter()
        switch self.selectedIndex {
        case CardEntryType.MONTH.rawValue:
            self.lblMonth.text = self.months[row]
            break
        case CardEntryType.YEAR.rawValue:
            formatter.dateFormat = "yyyy"
            let currentYear = Int(formatter.string(from: date))
            self.lblYear.text = "\(currentYear!+row)"
            self.setExpiryMonth()
        default: break
            
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        switch self.selectedIndex {
        case CardEntryType.MONTH.rawValue:
            return NSAttributedString(string: self.months[row], attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
            
        case CardEntryType.YEAR.rawValue:
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            let currentYear = Int(formatter.string(from: date))
            return NSAttributedString(string: "\(currentYear!+row)", attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
        default: break
            
        }
        
        return NSAttributedString(string: AppConstants.ScreenSpecificConstant.Common.EMPTY_STRING, attributes: [NSForegroundColorAttributeName:UIColor.appThemeColor()])
    }
}
extension AddCreditCardScreenViewController : CardTextFieldValidationDelegate
{
    func showErrorMessage(withMessage message: String,forTextFields: CardEntryType)
    {
        switch forTextFields {
        case .CARD_NUMBER:
            self.labelCardValidation.text = message
            break
        case .CVV:
            self.labelCVVValidation.text = message
            break
        case .MONTH:
            self.labelMonthValidation.text = message
            break
        case .YEAR:
            self.labelYearValidation.text = message
            break
        }
    }
}
extension AddCreditCardScreenViewController : UITextFieldDelegate
{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case self.textFieldCardNumber:
            guard let text = textField.text else {
                return true
            }
            let lastText = (text as NSString).replacingCharacters(in: range, with: string) as String
            textField.text = lastText.format(self.cardFormat, oldString: text)
            let brand = STPCardValidator.brand(forNumber: (textField.text?.replacingOccurrences(of: "-", with: ""))!)
            self.setUpView(forBrand:brand)
                return false
            
        case self.textFieldCvv:
            

            guard let text = textField.text else {
                return true
            }
            let lastText = (text as NSString).replacingCharacters(in: range, with: string) as String
                textField.text = lastText.format(self.cvvFormat, oldString: text)
            
            
            let brand = STPCardValidator.brand(forNumber: (textField.text?.replacingOccurrences(of: "-", with: ""))!)
            let maxLenghtOfCVV = STPCardValidator.maxCVCLength(for: brand)
            
            print(textField.text!.characters.count,string.characters.count,maxLenghtOfCVV)
            
            if ((textField.text?.characters.count)! <  Int(maxLenghtOfCVV)) {
                textField.textColor = UIColor(red: 198/255, green: 66/255, blue: 66/255, alpha: 1.0)
            }else {
                textField.textColor = UIColor.black
                
            }
            
         
                return false
        default:
            break
        }
        return true
    }
    
}
