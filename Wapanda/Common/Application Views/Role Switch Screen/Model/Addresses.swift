//
//  Addresses.swift
//  Wapanda
//
//  Created by daffomac-31 on 18/08/17.
//  Copyright © 2017 Wapanda. All rights reserved.
//

import Foundation
import ObjectMapper

public final class Addresses:NSObject, Mappable, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let recentlyVisited = "recentlyVisited"
        static let work = "work"
        static let home = "home"
    }
    
    // MARK: Properties
    public var recentlyVisited: Address?
    public var work: Address?
    public var home: Address?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        recentlyVisited <- map[SerializationKeys.recentlyVisited]
        work <- map[SerializationKeys.work]
        home <- map[SerializationKeys.home]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = recentlyVisited { dictionary[SerializationKeys.recentlyVisited] = value }
        if let value = work { dictionary[SerializationKeys.work] = value }
        if let value = home { dictionary[SerializationKeys.home] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.recentlyVisited = aDecoder.decodeObject(forKey: SerializationKeys.recentlyVisited) as? Address
        self.work = aDecoder.decodeObject(forKey: SerializationKeys.work) as? Address
        self.home = aDecoder.decodeObject(forKey: SerializationKeys.home) as? Address
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(recentlyVisited, forKey: SerializationKeys.recentlyVisited)
        aCoder.encode(work, forKey: SerializationKeys.work)
        aCoder.encode(home, forKey: SerializationKeys.home)
    }
    
}
