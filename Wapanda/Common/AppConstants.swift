/**
 Contains all the constants related to the application
 */

import Foundation

struct AppConstants{
    
    struct PListKeys{
        static let KEY_HOCKEY_APP = "KEY_HOCKEY_APP"
        static let BASE_URL_KEY = "BASE_URL"
        static let SOCKET_URL_KEY = "SOCKET_URL"
        static let KEY_GOOGLE_MAP = "KEY_GOOGLE_MAP"
        static let KEY_STRIPE = "KEY_STRIPE"
        static let STRIPE_STANDARD_CLIENT_ID = "STRIPE_STANDARD_CLIENT_ID"
        static let STRIPE_REDIRECT_URI = "STRIPE_REDIRECT_URI"
        static let STRIPE_KEY_PUBLISHABLE = "STRIPE_KEY_PUBLISHABLE"
        static let STRIPE_SECRET_KEY = "STRIPE_SECRET_KEY"
        static let APPLE_PAY_MERCHANT_ID = "APPLE_PAY_MERCHANT_ID"
        static let ENABLE_FAKE_GPS = "ENABLE_FAKE_GPS"
    }
    
    //MARK:This structure contains Api URLs
    
    struct URL{
        static let BASE_URL:String = PListUtility.getValue(forKey: AppConstants.PListKeys.BASE_URL_KEY) as! String
        static let SOCKET_URL:String = PListUtility.getValue(forKey: AppConstants.PListKeys.SOCKET_URL_KEY) as! String
        static let TERMS_AND_CONDITION_URL = "http://wapanda.co/privacy.html"
    }
    
    //MARK:This structure contains Api End Points
    
    struct ApiEndPoints {
        static let USER_LOGIN = "/users/login"
        static let USER_SIGNUP = "/users"
        static let FORGOT_PASSWORD = "/forgotpassword"
        static let REGISTER_DEVICE = "/devices"
        static let DEREGISTER_DEVICE = "/devices/pushIds/%@"
        static let DRIVER_TAX_INFO = "/drivers/%@/docs/tax"
        static let USER_PROFILE_UPDATE = "/users/%@"
        static let DRIVER_INSURANCE_UPDATE = "/drivers/%@/docs/insurance"
        static let DRIVER_VEHCILE_UPDATE = "/drivers/%@/docs/vehicle"
        static let DRIVER_DOCUMENT_UPLOAD = "/files"
        static let VERIFY_PHONE = "/users/%@/verifyphone"
        static let VERIFY_EMAIL = "/users/%@/verifyemail"
        static let VERIFY_EMAIL_OTP = "/verifyemailotp"
        static let VERIFY_PHONE_OTP = "/verifyphoneotp"
        static let RESET_PASSWORD = "/resetpassword"
        static let CHANGE_PASSWORD = "/changepassword"
        static let RESEND_PHONE_OTP = "/sendotp/phone"
        static let RESEND_EMAIL_OTP = "/sendotp/email"
        static let PRE_REGISTRATION_OTP = "/register/requestotp"
        static let TERMS_REVIEW = "/termsandconditions"
        static let UPDATE_USER = "/users/%@"
        static let UPDATE_RIDER = "/riders/%@"
        static let UPDATE_DRIVER = "/drivers/%@"
        static let AREA_PRICE_DRIVER = "/drivers/%@/areaprice?lat=%f&lng=%f"
        static let AREA_ONGOING_PRICE_DRIVER = "/drivers/%@/areaOngoingPrice?lat=%f&lng=%f"
        static let DRIVER_SOCKET = "/api/socket/driver"
        static let RIDER_SOCKET = "/api/socket/map"
        static let TRIP_SOCKET = "/api/socket/trip"
        
        //----Added----
        static let CAR_MAKE_REQUEST_BY_YEAR = "https://www.carqueryapi.com/api/0.3/?cmd=%@&year=%d&make=%@"
        
        static let CAB_LIST = "/nearbydrivers?lat=%f&lng=%f"
        static let REQUEST_CAB = "/requestcabs/filter"
        static let CAR_MAKE_REQUEST = "http://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=\(AppConstants.APIRequestHeaders.EDMUNDS_API_KEY_VALUE)"
        static let ALL_VEHICLE_TYPES = "/vehicletypes?sort=vehicleType"
        static let GOOGLE_DIRECTION_API = "https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=\(PListUtility.getValue(forKey: AppConstants.PListKeys.KEY_GOOGLE_MAP) as! String)"
        static let GOOGLE_DIRECTION_WITH_WAYPOINT_API = "https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&waypoints=%f,%f&key=\(PListUtility.getValue(forKey: AppConstants.PListKeys.KEY_GOOGLE_MAP) as! String)"
        static let DRIVER_INFO = "/drivers/%@"
        static let CREATE_BID = "/biddings"
        static let DRIVER_ACCEPT_RIDE = "/biddings/%@/driver/accept"
        static let DRIVER_COUNTER_PRICE = "/biddings/%@/driver/counter"
        
        static let RIDER_ACCEPT_RIDE = "/biddings/%@/rider/accept"
        static let RIDER_REJECT_RIDE = "/biddings/%@/rider/reject"
        static let CONFIRM_PICK_UP = "/trips/%@/startpickup"
        static let CANCEL_TRIP = "/trips/%@/cancel"
        static let START_TRIP = "/trips/%@/start"
        static let END_TRIP = "/trips/%@/end"
        static let LAUNCH_APP = "/users/me?userType=%@"
        static let GOOLE_STATIC_MAP_API = "https://maps.googleapis.com/maps/api/staticmap?size=1200x600\(MapStyle.parameters)&path=weight:3|color:red|enc:%@&markers=anchor:center|icon:https://goo.gl/RdrFZE|%f,%f&markers=anchor:center|icon:https://goo.gl/RdrFZE|%f,%f&key=\(PListUtility.getValue(forKey: AppConstants.PListKeys.KEY_GOOGLE_MAP) as! String)"
        static let WRITE_REVIEW = "/trips/%@/rateAndReview"
        static let GET_DRIVER_REVIEW = "/drivers/%@/ratesAndReviews"
        static let PAYMENT_SUCCES = "/payments/success"
        static let DRIVER_RATING_LIST = "/drivers/%@/ratesAndReviews"
        static let SEND_EMERGENCY_ALERT = "/users/emergencycontact"
        static let ADD_STOP = "/trips/%@/stops"
        static let STRIPE_CONNECT_AUTH = "https://connect.stripe.com/oauth/authorize"
        static let CONNECT_DRIVER_ACCOUNT = "/payments/connect?code=%@"
        static let AUTH_STRIPE_CODE = "https://connect.stripe.com/oauth/token"
        static let DELETE_SOURCE_STRIPE = "https://api.stripe.com/v1/customers/%@/sources/%@"
        static let VISITED_STOP = "/trips/%@/stopvisited"
        static let PROCESS_PAYMENT = "/payments/process"
        static let ADD_TIP_PERCENT = "/payments/%@/tip"
        static let CUSTOMER_ID = "/payments/customerid"
        static let CUSTOMER_SESSION = "/payments/customersession?stripeApiVersion=2017-08-15"
        static let SCHEDULE_RIDE_AUCTION = "/auctions"
        static let AUCTION_DETAIL = "/auctions/detail/%@/%@"
        static let ACCEPT_AUCTION = "/auctions/accept/%@"
        static let CANCEL_AUCTION = "/auctions/cancel/%@/%@"
        
        static let UPCOMING_BIDS = "/all/auctions/%@?skip=%d&limit=%d&sort=%@&order=%d"
        static let UPCOMING_TRIP = "/trips/upcoming/%@?skip=%d&limit=%d&sort=%@&order=%d"
       
        
        
        static let HELP_RIDER = "/helpQuestions"
        static let UPDATE_USER_PROFILE = "/users/%@/profile"
        static let PAST_TRIPS = "/trips/history/%@?skip=%d&limit=%d&sort=%@&order=%d"
        static let HELP = "/helpQuestions?relatedTo=%@"
        static let EARNINGS_DATA = "/earnings/drivers"
    }
    
    
    //MARK:- This structure contains Error message for various events
    
    struct ValidationErrors{
        static let ENTER_PHONE_NUMBER = "Please enter the phone number"
        static let INVALID_PHONE_NUMBER = "Please enter a valid phone number"
        static let ENTER_EMAILID_OR_PHONE_NUMBER = "Please enter email id or phone number"
        static let ENTER_VALID_EMAILID_OR_PHONE_NUMBER = "Please enter a valid email id or phone number"
        static let ENTER_PASSWORD = "Please enter the password"
        static let CURRENT_PASSWORD = "Please enter the current password"
        static let NEW_PASSWORD = "Please enter the new password"

        static let INVALID_PASSWORD = "Invalid password.(Should be more than 6 digits and less than 25 digits)"
        static let ENTER_EMAIL_ID = "Please enter email id"
        static let INVALID_EMAIL_ID = "Please enter a valid email address"
        static let ENTER_FIRST_NAME = "Please enter first name"
        static let ENTER_VALID_LAST_NAME = "Please enter valid last name"
        static let ENTER_VALID_FIRST_NAME = "Please enter valid first name"
        static let ENTER_VALID_EMAIL = "Please enter valid email address"


        static let ENTER_OTP = "Please enter OTP"
        static let INVALID_OTP = "Please provide a valid pin"
        static let SELECT_SELFIE = "Please enter a selfie image"
        static let SELECT_INSURANCE = "Please select insurance image"
        static let SELECT_VEHICLE = "Please select vehicle image"
        
        //Tax info screen
        static let ENTER_SECURITY_NUMBER = "Please enter the \(AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_PLACEHOLDER_TITLE)"
        static let ENTER_TAX_ID = "Please enter the \(AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_PLACEHOLDER_TITLE)"
        static let ENTER_VALID_SECURITY_NUMBER = "Please enter a valid \(AppConstants.ScreenSpecificConstant.TaxInformation.INDIVIDUAL_PLACEHOLDER_TITLE)"
        static let ENTER_VALID_TAX_ID = "Please enter a valid \(AppConstants.ScreenSpecificConstant.TaxInformation.PARTNERSHIP_PLACEHOLDER_TITLE)"
        
        //Vehicle Info Screen
        static let ENTER_YEAR = "Please select year of the vehicle"
        static let ENTER_MAKE = "Please select make of the vehicle"
        static let ENTER_MODEL = "Please select model of the vehicle"
        static let ENTER_COMPANY = "Please enter company/corporate of the vehicle"
        static let ENTER_PLATE_NUMBER = "Please enter licence plate number of the vehicle"
        static let ENTER_TYPE = "Please select type of the vehicle"
        static let ENTER_VALID_CAR_PLATE_NUMBER = "Limit max 8 characters to input through keyboard"

        static let SELECT_MAKE_FIRST = "Please select make of the vehicle"
        
    }
    
    //MARK:- This structure contains Request header keys
    
    struct APIRequestHeaders {
        
        static let CONTENT_TYPE = "Content-Type"
        static let APPLICATION_JSON = "application/json"
        static let API_KEY = "api_key"
        static let AUTH_TOKEN = "Authorization"
        static let API_KEY_VALUE = "e29e457aa39593afbef47dfde7a558adf12ebdf7f67156621a6026b7e567ca73"
        static let EDMUNDS_API_KEY_VALUE = "e29e457aa39593afbef47dfde7a558adf12ebdf7f67156621a6026b7e567ca73"
    }
    
    //MARK:- This structure contains keys for error handling
    
    struct ErrorHandlingKeys{
        
        static let IS_ERROR = ""
        static let ERROR_KEY = "code"
        static let ERROR_DETAIL = "detail"
    }
    
    //MARK:- This structure contains the error messages corresponding to error code
    
    struct ErrorMessages{
        
        static let ERROR_TITLE = "Alert"
        static let ALERT_TITLE = "Alert"
        static let MESSAGE_TITLE = "Message"
        static let INVALID_KEY_MESSAGE = "Valid api key is required. Please provide a valid api key along with request"
        static let INVALID_INPUT_MESSAGE = "Please provide a valid input"
        static let INVALID_LOGIN_MESSAGE = "Please try again. Invalid login "
        static let SOME_ERROR_OCCURED = "Some error occurred. Please try again"
        static let PLEASE_CHECK_YOUR_INTERNET_CONNECTION = "Please check your internet connection"
        static let REQUEST_TIME_OUT = "Looks like the server is taking to long to respond, please try again in sometime"
        static let EMAIL_ALREADY_REGISTERED = "This email is already registered"
        static let PHONE_ALREADY_REGISTERED = "This phone is already registered"
        static let INVALID_INPUT = "Please provide a valid input"
        static let INVALID_OTP = "Please provide a valid pin"
        static let PHONE_NOT_REGISTERED = "This number is not registered"
        static let EMAIL_NOT_REGISTERED = "This email is not registered"
        static let PHONE_NOT_VERIFIED = "Phone number is not verified."
        static let EMAIL_NOT_VERIFIED = "Email is not verified."
        static let INTERNAL_ERROR = "Oops-something went wrong. Please try again"
        static let ALREADY_REGISTERED = "You are already registered with the credential."
        static let ACCOUNT_DISABLED_MESSAGE = "Your account has been disabled. Please contact the administrator"
        static let ACCOUNT_UNVERIFIED = "Your account is not verified"
        static let INVALID_AUTH = "Valid authentication token is required. Please provide a valid token along with request"
        static let NOT_FOUND = "User profile not found."
        static let NO_CABS_FOUND = "No cars available."
        static let DRIVER_NOT_FOUND = "Driver profile not found."
        static let ALREADY_BIDDING = "Your driver is not available right now, please try later"
        static let DRIVER_ON_TRIP = "Driver is not available right now, please try later"
        static let DRIVER_OFFLINE = "Your driver is not available right now, please try later"
        static let DRIVER_UNAVAILABLE = "Driver is not available right now, please try later"
        
        static let BIDDING_CLOSED = "Bidding is closed, please try again"
        static let BIDDING_EXPIRED = "Your bid has expired, please try again"
        static let INVALID_DRIVER = "Invalid driver."
        static let INVALID_RIDER = "Invalid rider"
        static let ALREADY_COUNTERED = "You already countered this bid"
        static let INVALID_COUNTER_PRICE = "This is an invalid counter price, please try a different amount."
        static let ALREADY_CANCELLED = "Your ride has been cancelled"
        static let CANCEL_RIDE_CONFIRMATION = "Are you sure you want to cancel the ride?"
         static let CANCEL_Bid_CONFIRMATION = "Are you sure you want to cancel the bid?"
        static let INVALID_TRIP_STATUS = "Trip status is not valid"
        static let PAYMENT_FAILED = "Your payment has not been received"
        static let INVALID_INPUT_EMERGENCY_CONTACT = "Please save a valid phone number for emergency"
        static let ALREADY_ADDED_STOP = "Already added stops to trip"
        static let ALREADY_PAID = "ALREADY_PAID"
        
        static let PAYMENT_AUTH_FAILED = "Payment failed. Please try again!"
        
        static let PAYMENT_ALREADY_PAID = "Your payment has already been processed"
        static let PAYMENT_RIDER_NO_SOURCE = "Please add a payment method to process payments"
        static let PAYMENT_DRIVER_NO_ACCOUNT = "Driver is not connected to stripe"
        static let STRIPE_ACCOUNT_IN_USE = "Stripe account already in use"
        static let ALREADY_VISITED_MESSAGE = "Already visited on this stop"
        static let NOT_REGISTERED = "No user is registered."
        static let TRIP_NOT_STARTED_YET = "Trip not yet started"
        static let ACCESS_DENIED = "Cannot access requested resource"
        static let EMERGENCY_CONTACT_NOT_ADDED = "Emergency contact not added"
        static let ALREADY_ON_TRIP = "Your driver is not available right now, please try later"
        static let UNAUTHORIZED = "User login is required"
        static let INVALID_VERIFICATION_CODE = "Please enter a valid code"
        static let INVALID_INPUT_FORMAT = "Invalid input format"
        static let INPUT_TOO_LARGE = "Input too large"
        static let INVALID_KEY = "Valid api key is required. Please provide a valid api key along with request"
        static let LIMIT_REACHED = "Limit is reached"
        static let ALREADY_EXIST = "Data Already exist"
        static let CANNOT_BID_SELF = "You cannot bid yourself"
        static let ALREADY_ENDED = "Trip already ended"
        static let INVALID_IMAGE_ID = "Image not found"
        static let ON_TRIP = "User on trip"
        static let UNKNOWN_DRIVER_LOCATION = "Driver location is not known"
        static let STOP_NOT_FOUND = "Stop not found"
        static let STRIPE_ERROR = "Stripe error occurred"
        static let INVALID_OLD_PASSWORD = "Current password is incorrect"
        
        static let INVALID_WEEK_START_DAY = "Please provide valid start day."
        static let INVALID_WEEK_END_DAY = "Please provide a valid week end day"
        static let INVALID_MONTH_DATE = "Month day is invalid."

        static let READY_FOR_PICKUP = "Are you ready for pick up?"
        static let PICKUP_ALREADY_STARTED = "Pick up already started"
        static let START_PICKUP_NOT_FOUND = "Some error occurred"
        static let START_PICKUP_ALREADY_ON_TRIP = "You can't start another trip"
        
        static let NOT_FOUND_AUCTION = "Some error occurred"
        static let EMPTY_QUOTES_AUCTION_DETAIL_MESSAGE = "No one available for bid now."
        static let NOT_FOUND_AUCTION_BID = "Data not found"
        static let AUCTION_CLOSED = "Auction closed"
        static let BID_CLOSED = "Bid closed"
        static let EMPTY_QUOTES_MESSAGE = "No one available for bid now."
        static let ADDRESS_NOT_FOUND = "Cab not found for selected locations"
        static let PRICING_ENGINE_ERROR = "OOPS! We have encountered an issue. Please try again."
        static let AREA_NOT_COVERED = "Our services are currently not available in this area. Please enter a different location."
        static let STATE_NOT_COVERED = "Our services are currently not available in this area. Please enter a different location."
        static let JSON_REQUEST_SPECIFICATION_ERROR = "OOPS! We have encountered an issue on server. Please try after some time."
        static let WEATHER_SERVICE_ISSUE = "OOPS! We have encountered an issue. Please try again."
        static let PRICING_SERVER_STATE_ERROR = "OOPS! We have encountered an issue. Please try again."
        static let RIDE_TIME_GREATER_THAN_NOW = "You can only schedule one hour from now"
        
    }
    
    struct ScreenSpecificErrorMessage {
        struct UpcomingTripList{
            static let NOT_FOUND = "Data not found"
        }
    }
    
    struct ScreenSpecificConstant {
        struct Common{
            static let BACK_BUTTON_TITLE = "Back"
            static let SKIP_BUTTON_TITLE = "Skip"
            static let EMPTY_STRING = ""
            static let LOGOUT_TITLE = "Logout"
            static let YES_TITLE = "Yes"
            static let OK_TITLE = "Ok"
            static let NO_TITLE = "No"
            static let Cancel_TITLE = "Cancel"
            static let LOGOUT_MESSAGE = "Are you sure you want to logout?"
            static let SETTINGS = "Go To Settings"
            static let ACCESS_DENIED = "Access Denied"
            static let CONTACT_ACCESS_PERMISSION_MESSAGE = "Please allow the app to access your contacts through the Settings"
            static let SAVE_BUTTON_TITLE = "Save"
            static let EDIT_BUTTON_TITLE = "Edit"
            static let PRODUCT_NAME_PRODUCTION = "Wapanda"
        }
        struct UploadScreen {
            static let TITLE_GALLERY_ACCESS_DISABLED = "Gallery Access Disabled"
            static let TITLE_CAMERA_ACCESS_DISABLED = "Gallery Access Disabled"
            static let MESSAGE_GALLERY_SERVICE_DISABLED = "Allow us to access your gallery to pick photo for you."
            static let MESSAGE_CAMERA_SERVICE_DISABLED = "Allow us to access your camera to pick photo for you."
            static let NUMBER_OF_DOCUMENTS_UPLOAD = 2
        }
        struct TermsScreen{
            static let NAVIGATION_TITLE = "Please Review"
            static let STEP_NAVIGATION_TITLE = "Step 7 of 7"
        }
        struct LoginScreen {
            static let NAVIGATION_TITLE = "Login"
            static let EMAIL_OR_PHONE_PLACEHOLDER_TEXT = "Email or Phone"
            static let PASSWORD_PLACEHOLDER_TEXT = "Password"
        }
        struct NewPasswordScreen {
            static let PASSWORD_PLACEHOLDER_TEXT = "Password"
        }
        struct ResetPasswordScreen {
            static let NEW_PASSWORD_PLACEHOLDER_TEXT = "New Password"
            static let CURRENT_PASSWORD_PLACEHOLDER_TEXT = "Current Password"
            static let PASSWORD_CHANGE_SUCCESS = "Your password has been changed successfully"


        }
        struct ForgotPasswordScreen {
            static let EMAIL_OR_PHONE_PLACEHOLDER_TEXT = "Email or Phone"
        }
        struct DriverSetup {
            static let NAVIGATION_TITLE = "Driver Setup"
            static let TAX_INFORMATION = "Tax Infomation"
            static let INSURANCE_INFORMATION = "Insurance Information"
            static let PROFILE_PHOTO = "Profile Photo"
            static let VEHICLE_PHOTO = "Vehicle Photo"
            static let VEHICLE_INFO = "Vehicle Information"
            static let BANK_INFORMATION = "Stripe"
            static let DOCUMENT_REVIEW = "Document Review"
            static let CONTINUE_TITLE = "Continue"
            static let CONTINUE_ANYWAY_TITLE = "Continue Anyway"
        }
        struct TaxInformation{
            static let INDIVIDUAL_LABEL_TITLE = "SOCIAL SECURITY NUMBER"
            static let INDIVIDUAL_PLACEHOLDER_TITLE = "Social Security Number"
            static let PARTNERSHIP_LABEL_TITLE = "TAX ID"
            static let PARTNERSHIP_PLACEHOLDER_TITLE = "Tax Id"
            static let NAVIGATION_TITLE = "Tax Information"
            static let ALERT_CONTROLLER_TITLE = "TAX CLASSIFICATION"
            static let STEP_NAVIGATION_TITLE = "Step 1 of 7"
            static let INDIVIDUAL_VALUE = "soleProprietory"
            static let PARTNERSHIP_VALUE = "partnership"
        }
        struct BankInformation{
            static let NAME_LABEL_PLACEHOLDER = "Enter Your Full Name"
            static let ROUTING_LABEL_PLACEHOLDER = "Enter Your Routing Number"
            static let PASSWORD_LABEL_PLACEHOLDER = "Password"
            static let STEP_NAVIGATION_TITLE = "Step 6 of 7"
        }
        struct VehicleInfo{
            static let STEP_NAVIGATION_TITLE = "Step 5 of 7"
        }
        
        struct SingUpScreen{
            static let FORM1_NAVIGATION_TITLE = "Step 1 of 3"
            static let FORM2_NAVIGATION_TITLE = "Step 2 of 3"
            static let EMAIL_PLACEHOLDER_TEXT = "Enter Your Email"
            static let PHONE_PLACEHOLDER_TEXT = "Enter Your Number"
            static let FIRST_NAME_PLACEHOLDER_TEXT = "Enter First Name"
            static let LAST_NAME_PLACEHOLDER_TEXT = "Enter Last Name"
            static let OTP_PLACEHOLDER_TEXT = "Enter Code"
            static let PASSWORD_PLACEHOLDER_TEXT = "Password"
            static let HINT_PHONE_NUMBER_TEXT = "(•••) •••-••"
            static let HINT_EMAIL_TEXT = "•••••"
            static let OTP_SENT = "Email successfully verified"
            static let TERMS_AND_CONDITION_TEXT = "By Signing up you are agreeing to our Terms of Service."
            static let TERMS_OF_SERVICE_TEXT = "Terms of Service"
        }
        
        struct WelcomeScreen {
            static let TITLE_LOCATION_ACCESS_DISABLED = "Location Access Disabled"
            static let MESSAGE_LOCATION_SERVICE_DISABLED = "Allow \(PListUtility.getValue(forKey: "CFBundleName")) to access this device's location?"
        }
        
        struct RiderHomeScreen{
            static let ADDRESS_TITLE_HOME = "home"
            static let ADDRESS_TITLE_WORK = "work"
            static let ADDRESS_TITLE_RECENT = "recent"
            static let ADDRESS_TITLE_SCREEN_HOME = "Home Address"
            static let ADDRESS_TITLE_SCREEN_WORK = "Work Address"
            static let ADDRESS_TITLE_SCREEN_RECENT = "RECENT"
            static let NO_LOCATION_FOUND = "No Address Found"
            static let CAB_ANIMATION_DURATION = 2.0
            static let NO_ROUTE_AVAILABLE_MESSAGE = "No Route Found"
            static let FETCHING_ADDRESS_TITLE = "Fetching Address..."
            static let NO_SOURCE_ADDRESS = "Please select your pic up address"
            static let TIME_TO_FETCH_CABS = 10 //seconds
            static let LOWEST_PRICE_TITLE = "Lowest\nPrice"
            static let CLOSEST_RIDE_TITLE = "Closest\nRide"
            static let MOST_SEATS_TITLE = "Most\nSeats"
        }
        
        struct DriverHomeScreen{
            static let ONLINE_TITLE = "Go Online"
            static let OFFLINE_TITLE = "Go Offline"
            static let MULTIPLE_FACTOR : Float = 20.0
            static let TIME_TO_FETCH_AREA_PRICE = 60*10//seconds
            static let RANGE_FACTOR : Float = 10.0
            static let MINIMUM_VALUE : Float = 0.0
            static let WAIT_FOR_LOCATTION_MESSAGE = "Unable to fetch your location. Please try again after some time."
            static let POINTS_PER_CENT_FACTOR : Float = 0.15 // 1 dollor will have the scale value of 15 points.
        }
        
        struct RiderSelectCab{
            static let CAR_TYPE_STANDARD_KEY = "standard"
            static let CAR_TÁPE_SUV_KEY = "suv"
            static let CART_TYPE_XL_KEY = "xl"
            static let CAR_REACH_TIME_PLACEHODLER = "--:--"
            static let NO_CAB_AVAILABLE_MESSAGE = "No Cars Available"
        }
        
        struct SideMenuScreen{
            static let ITEM_PAYMENT_TITLE = "Payments"
            static let PAYMENT_SUBTITLE = "SETUP PAYMENT"
            static let ITEM_TRIP_TITLE = "Your Trips"
            static let ITEM_BID_TITLE = "Your Bids"
            static let ITEM_CONNECTION_TITLE = "Connections"
            static let ITEM_HELP_TITLE = "Help"
            static let ITEM_EMERGENCY_TITLE = "Emergency"
            static let ITEM_DRIVER_SIDE_TITLE = "Driver Side"
            static let ITEM_SETTINGS_TITLE = "Reset Password"
            static let ITEM_LOGOUT_TITLE = "Logout"
            static let SETUP_EMERGENCY_TITLE = "SETUP NEEDED"
            static let SETUP_PAYMENT_TITLE = "NEW CARD NEEDED"
            static let ANONYMOUS_USER_TITLE = "Anonymous"
            static let ANONYMOUS_USER_HOME_TITLE = "There"
            static let SWITCH_ROLE_MESSAGE = "Are you sure you want to switch to %@?"

        }
        
        struct SideMenuScreenDriver{
            static let ITEM_PAYMENT_TITLE = "Payments"
            static let ITEM_TRIP_TITLE = "Your Trips"
            static let ITEM_BID_TITLE = "Your Bids"
            static let ITEM_RATING_TITLE = "Ratings"
            static let ITEM_HELP_TITLE = "Help"
            static let ITEM_DOCUMENTS_TITLE = "Documents"
            static let ITEM_RIDER_SIDE_TITLE = "Rider Side"
            static let ITEM_SETTINGS_TITLE = "Reset Password"
            static let ITEM_LOGOUT_TITLE = "Logout"
        }
        
        struct RideListScreen{
            static let MULTIPLE_CAB_TITLE = "Multiple"
            static let SORTING_TYPE_PRICE = "Price"
            static let SORTING_TYPE_DISTANCE = "Distance"
            static let ALL_CAB_SELECTION_TITLE = "All"
            static let SELECT_OTHER_CAB = "This type of cabs are not available. Please select another type"
            static let LOCATION_NOT_FOUND = "Location not found"
            static let LOCATION_NOT_FOUND_MSG = "Please check for spelling mistakes"
        }
        
        struct BidInProgressScreen{
            static let BID_WAITING_MESSAGE = "Your request is processing by driver. Please wait..."
            static let COUNTER_WAITING_MESSAGE = "Bidding is in progress..."
        }
        struct DriverRequestScreen{
            static let TIMER_DURATION = 60
        }
        struct DriverTripScreen {
            static let GOOGLE_MAP_APP_URL_FORMAT = "comgooglemaps://?saddr=%f,%f&daddr=%f,%f&directionsmode=transit"
            static let GOOGLE_MAP_SITE_URL_FORMAT = "https://maps.google.com/?saddr=%f,%f&daddr=%f,%f&directionsmode=transit"
            static let GOOGLE_MAP_APP_DEEP_LINK = "comgooglemaps://"
            static let RIDER_ADDED_STOP_ALERT_MESSAGE = "Rider added a stop"
        }
        struct RiderTripScreen{
            static let DRIVER_ARRIVING_MESSAGE = "Whoa! Your driver is arriving!"
            static let VISITED_STOP_ALERT_MESSAGE = "You have arrived to your stop"
            static let ADD_STOP_ALERT_MESSAGE = "This may change your final price"
        }
        struct EmergencyContactScreen{
            static let NO_CONTACT_ADDED_MESSAGE = "You haven't added any emergency contact yet"
            static let INVALID_NUMBER_MESSAGE = "Please save a valid phone number for emergency"
        }
        struct PaymentMethodScreen{
            static let NAVIGATION_TITLE = "Select Method"
        }
        struct DriverRatingScreen{
            static let NAVIGATION_TITLE = "Ratings"
        }
        struct AddPaymentScreen{
            static let NAVIGATION_TITLE = "Payment"
            static let DELETE_CONFIRM_MESSAGE = "Are you sure you want to delete card ?"
            static let DEFAULT_CARD_CONFIRM_MESSAGE = "Are you sure you want to add this card as default?"
            static let CARD_DELETED_SUCCESS_MESSAGE = "Card has been deleted successfully"
        }
        struct AddCreditCardScreen{
            static let NAVIGATION_TITLE = "Add Card"
            static let UNABLE_TO_ADD_CARD_MESSAGE = "Unable to add card at this moment. Please try again later!"
        }
        struct TipScreen{
            static let NAVIGATION_TITLE = "Payment"
        }
        struct HelpScreen{
            static let NAVIGATION_TITLE = "Help"
        }
        struct DriverInvoice
        {
            static let PENDING_MESSAGE = "(Payment Pending)"
            static let CANCELLED_MESSAGE = "(Cancelled)"
        }
        struct RiderProfile{
            static let RIDER_PROFILE_TITLE = "Profile"
            static let ADD_BUTTON_TITLE = "Add"
            static let SEARCH_HOME_ADDRESS_TITLE = "Enter Home Address"
            static let SEARCH_WORK_ADDRESS_TITLE = "Enter Work Address"
            static let PROFILE_UPDATE_SUCCESS_MESSAGE = "Profile has been updated successfully"
        }
        struct RiderTripListing{
            static let NAVIGATION_TITLE = "Your Trips"
            static let No_Trips = "Currently there is no trip history"
            static let UPCOMING_TITLE = "Upcoming"
            static let HISTORY_TITLE = "History"
            static let No_Schedule_Trip = "There are no trips"
        }
        struct YourBidsScreen{
            static let NAVIGATION_TITLE = "Your Bids"
            static let NO_BIDS = "There are no bids"
            static let ACTIVE_TITLE = "Active"
            static let INACTIVE_TITLE = "In Active"
        }
        
        struct DriverProfile{
            static let EDIT_DOCUMENT_LABEL_STRING = "To Edit Vehicle Information go to Documents"
            static let EDIT_LABEL_STRING = "To Edit Vehicle Information go to "

            static let DOCUMENT_LABEL_STRING = "Documents"
        }
        struct CounterBidScreen{
            static let BID_LOW_MESSAGE = "This Bid Looks Low"
            static let BID_HIGH_MESSAGE = "This Bid Looks High"
            static let BID_LOOKS_GOOD_MESSAGE = "This Bid Looks Good"
        }
    }
    
    
    //MARK:- Common constants
    
    struct Common{
        static let LOGIN_SUCCESSFULL = "Login successful"
        static let LOGOUT_SUCCESSFUL = "Logged out successfully"
    }
    
    //MARK:- NSNotification Names
    struct NSNotificationNames {
        static let UPDATE_DRIVER_LOCATION = "updateDriverLocation"
        static let UPDATE_CAB_LOCATION = "UPDATE_CAB_LOCATION"
        static let SOCKET_API_ERROR = "SOCKET_API_ERROR"
        static let SOCKET_TRIP_UPDATE = "SOCKET_TRIP_UPDATE"
        static let SOCKET_ERROR = "SOCKET_ERROR"
        static let SOCKET_DISCONNECT = "SOCKET_DISCONNECT"
        static let CAB_ENTERS = "CAB_ENTERS"
        static let CAB_EXITS = "CAB_EXITS"
        static let INTERNET_RECHABLE_NOTIFICATION = "INTERNET_RECHABLE"
        static let INTERNET_UNREACHABLE_NOTIFICATION = "INTERNET_UNREACHABLE"
        static let REFRESH_PAYMENT_OPTION = "REFRESH_PAYMENT_OPTION"
        static let GET_CAB_LIST = "GET_CAB_LIST"
        static let USER_PROFILE_UPDATED = "USER_PROFILE_UPDATED"
        static let DRIVER_STARTED_TRIP = "DRIVER_STARTED_TRIP"
        static let DRIVER_ARRIVED = "DRIVER_ARRIVED"
        static let RIDER_CANCELLED_TRIP = "RIDER_CANCELLED_TRIP"
        static let DRIVER_CANCELLED_TRIP = "DRIVER_CANCELLED_TRIP"
        static let DRIVER_INVOICE_HANDLE = "DRIVER_INVOICE_HANDLE"
        static let RIDER_REJECT_EVENT = "RIDER_REJECT_EVENT"
        static let RIDER_ACCEPT_EVENT = "RIDER_ACCEPT_EVENT"
        static let DRIVER_ACCEPT_EVENT = "DRIVER_ACCEPT_EVENT"
        static let RIDER_ADDED_STOP = "RIDER_ADDED_STOP"
        static let CUSTOMER_CONTEXT_REFRESHED = "CUSTOMER_CONTEXT_REFRESHED"

        static let APP_BECOME_ACTIVE_NOTIFICATION = "APP_BECAME_ACTIVE_NOTIF"
    }
    
    //MARK : UserInfo Object Constants
    
    struct UserInfoConstants{
        
        static let USER_ID = "USER_ID"
        static let FIRST_NAME = "FIRST_NAME"
        static let PHONE = "PHONE"
        static let EMAIL = "EMAIL"
        static let COUNTRY_CODE = "COUNTRY_CODE"
        static let PROFILE_IMAGE_ID = "PROFILE_IMAGE_ID"
        static let COUNTRY_CODE_EXTENSION = "COUNTRY_CODE_EXTENSION"
        static let ACCOUNT_DISABLED = "ACCOUNT_DISABLED"
        static let ACCOUNT_UNVERIFIED = "ACCOUNT_UNVERIFIED"
        static let EMAIL_VERIFIED = "EMAIL_VERIFIED"
        static let PHONE_VERIFIED = "PHONE_VERIFIED"
        static let DRIVER_DOC = "DRIVER_DOC"
        static let DRIVER_PROFILE = "DRIVER_PROFILE"
        static let RIDER_PROFILE = "RIDER_PROFILE"
        static let USER_INFO = "USER_INFO"
    }
    
    //MARK:- This structure contains events used to listen or emit data on socket
    
    struct SocketEvents{
        static let DRIVER_AUTHORIZATION = "auth"
        static let DRIVER_UPDATE = "driverUpdate"
        static let API_ERROR = "ApiError"
        static let ENTER = "enter"
        static let EXIT = "exit"
        static let LAT_LONG = "latlng"
    }
    
    //MARK:- Map Style Parameters
    struct MapStyle {
        static let parameters = "&style=element:geometry|color:0xf5f5f5&style=element:labels.icon|visibility:off&style=element:labels.text.fill|color:0x616161&style=element:labels.text.stroke|color:0xf5f5f5&style=feature:administrative.land_parcel|element:labels.text.fill|color:0xbdbdbd&style=feature:landscape|element:geometry.fill|color:0xc7c7c7&style=feature:landscape|element:geometry.stroke|color:0xc7c7c7&style=feature:poi|element:geometry|color:0xeeeeee&style=feature:poi|element:labels.text.fill|color:0x757575&style=feature:poi.park|element:geometry|color:0xe5e5e5&style=feature:poi.park|element:labels.text.fill|color:0x9e9e9e&style=feature:road|element:geometry|color:0xffffff&style=feature:road|element:geometry.fill|color:0xd7d7d7&style=feature:road.arterial|element:labels.text.fill|color:0x757575&style=feature:road.highway|element:geometry|color:0xdadada&style=feature:road.highway|element:labels.text.fill|color:0x616161&style=feature:road.local|element:labels.text.fill|color:0x9e9e9e&style=feature:transit.line|element:geometry|color:0xe5e5e5&style=feature:transit.station|element:geometry|color:0xeeeeee&style=feature:water|element:geometry|color:0xc9c9c9&style=feature:water|element:labels.text.fill|color:0x9e9e9e"
    }
    
    //MARK: Persistent user default data keys
    
    struct UserDefaultKeys{
        static let IS_ALREADY_LOGIN = "isAlreadyLogin"
        static let IS_DRIVER = "isDriver"
        static let NON_LOGGED_IS_DRIVER = "isNonLoggedDriver"
        static let DEVICE_TOKEN = "DEVICE_TOKEN"
        static let IS_ALREADY_LAUNCHED = "IS_ALREADY_LAUNCHED"
    }
    
    //MARK:- Date Formatter constants
    
    struct DateConstants {
        static let DOB_FORMAT = "dd/MM/yyyy"
        static let DOB_FORMAT_FROM_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let TIME_FORMAT_IN_12_HOUR = "hh:mm a"
        
    }
    
    struct ToastMessages{
        static let OTP_SENT = "OTP has been sent."
        static let LOGIN_SUCCESS = "OTP has been sent."
    }
}
